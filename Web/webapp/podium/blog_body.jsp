<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.ConfigurationHelper,
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.PodiumForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.data.FeaturedBlog"%>   

<% 
    PodiumForm podiumForm = (PodiumForm)request.getAttribute("PodiumForm"); 
      
    List blogList = (List)podiumForm.getBlogList();  

   
%>
    <table cellpadding="5">
            <tr>
                <td class="hdr_1" colspan="3" align="left"><h1>Featured Blog</h1>
                </td>
            </tr>   
            <tr>
                <td colspan="3">Add your blog here! Select "Feature My Blog!" at Profile - Contacts - My Blog
                </td>
            </tr>    
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>  
            
        <%
            if (blogList != null && !blogList.isEmpty())
            {  
                //for(int i=0;i<blogList.size();i++)
                for(int i=blogList.size()-1;i>=0;i--)
                {
                    FeaturedBlog featuredBlog = (FeaturedBlog)blogList.get(i);
                   
        %>
            <tr>
                <td width="43%" align="right"><a href="<%=featuredBlog.getBlogLink()%>" target="#"><img src="<%=featuredBlog.getBlogPhotoPath()%>" align="right"></a></td> 
                <td width="2%"></td>
                <td width="55%" align="left" valign="middle"><a href="<%=featuredBlog.getBlogLink()%>" target="#"><%=featuredBlog.getBlogName()%></a>
                    <%
                    if (featuredBlog.getUserId()!= PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY))
                    {
                     %>
                     &nbsp;by<a href="http://profile.<%=ConfigurationHelper.getDomainName()%>/<%=featuredBlog.getUser().getUserName()%>" <%=featuredBlog.getUser().getUserName()%></a>
                      
                     <%
                    }
                    %><br><br><%=featuredBlog.getBlogDescription()%>
                </td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr> 
          
        <%
                } 
          }
          else
          {
        %>   
        
            <tr>
                <td class="hdr_1" colspan="3" align="left">No Blogs
                </td>
            </tr>  
        <%
          } 
        %>   
        </table>