<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.PodiumForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.data.RankingSnapshot"%>   

<% 
    PodiumForm podiumForm = (PodiumForm)request.getAttribute("PodiumForm"); 
    HashMap hashMap = podiumForm.getPodiumHashMap();
    List viewsList = null;
    List votesList = null;
    List invitesList = null;
    List gameList = null;
    
    if ( hashMap != null){
        viewsList = (List)podiumForm.getPodiumHashMap().get(OEightConstants.TOP_VIEWS);
        votesList = (List)podiumForm.getPodiumHashMap().get(OEightConstants.TOP_VOTES);
        invitesList = (List)podiumForm.getPodiumHashMap().get(OEightConstants.TOP_INVITES);
        gameList = (List)podiumForm.getPodiumHashMap().get(OEightConstants.TOP_GAMER);
    } 
   
%>  
    
    <table border="0" cellpadding="0" width="100%">
        <tr>
            <td colspan="2" align="left"><h1>OFFICIAL PODIUM - current month</h1>
            </td>
            <td rowspan="4" width="20%">&nbsp;</td>
        </tr>
        <tr>
          <td><div id="profile_personal2" class="divBoxPodium">
            <table border="0" cellpadding="2" width="100%" height="100%">
              <TR>
                <TD class=inputlabel align=left colSpan=3>&nbsp;</TD>
              </TR>
              <TR class=inputlabel>
                <%--TD width="20%" align=right class=inputlabel><img src="/images/podium/main_vote.gif" width="110" height="110"> </TD>
                <TD width="20%" align=right class=guylabel>&nbsp;</TD--%>
                <TD width="100%" align=left valign="top">
		<TABLE width="100%">
                  <TBODY> 
                      <TR class=inputlabel>
                          <TD colspan="2" align=right class=inputlabel><img src="/images/podium/hdr_votes.gif" width="235" height="24"></TD>
                      </TR>
               
                         
        <%
        
        String[] userName = new String[3]; 
        String[] photoPath = new String[3]; 
        //long[] userId = new long[3]; 
        long[] votes = new long[3];
        if (votesList != null && !votesList.isEmpty())
        {
            System.out.println("Result size" + votesList.size()); 
            for(int i=0;i<votesList.size();i++)
            {
            RankingSnapshot rankingSnapshot = (RankingSnapshot)votesList.get(i);
            userName[i] = rankingSnapshot.getUser().getUserName();
            photoPath[i] = rankingSnapshot.getUser().getUserCountry().getPhotoLargePath();
            //userId[i] = rankingSnapshot.getUserId();
            votes[i] = rankingSnapshot.getVotes(); 
            } 
        }
        else
        { 
            for(int i=0;i<=3;i++)
            { 
                userName[i] = "Not available";
                photoPath[i] = "na.jpg";
                //userId[i] = "Not available";
                votes[i] = 0l; 
            } 
        } 
        %>  
                    <TR>
                      <TD class=oneofthree align=middle width=150>1st<BR>
                          <a 
            href="http://profile.080808.com.my/<%=userName[0]%>"><img 
            src="/vphotos/<%=photoPath[0]%>" width=120></a> </TD>
                        <td>
                            <table align="left">
                                <TR>
                                  <TD align=right>User :</TD>
                                  <TD align=left><%=userName[0]%></TD>
                                </TR>
                                <TR>
                                  <TD align=right>Votes :</TD>
                                  <TD align=left><%=votes[0]%></TD>
                                </TR>
                                <TR>
                                  <TD colSpan=2>&nbsp; </TD>
                                </TR>
                                <TR>
                            </table>
                        </td>
                    </TR>
                    
                      <TD class=oneofthree align=middle width=150>2nd<BR>
                          <A 
            href="http://profile.080808.com.my/<%=userName[1]%>"><IMG 
            src="/vphotos/<%=photoPath[1]%>" width=120> </A></TD>
                        <td>
                            <table align="left">
                                <TR>
                                  <TD align=right>User :</TD>
                                  <TD align=left><%=userName[1]%></TD>
                                </TR>
                                <TR>
                                  <TD align=right>Votes :</TD>
                                  <TD align=left><%=votes[1]%></TD>
                                </TR>
                                <TR>
                                  <TD colSpan=2>&nbsp; </TD>
                                </TR>
                                <TR>
                            </table>
                        </td>
                    </TR> 
                    <TR>
                      <TD class=oneofthree align=middle width=150>3rd<BR>
                          <A 
            href="http://profile.080808.com.my/<%=userName[2]%>"><IMG 
            src="/vphotos/<%=photoPath[2]%>" width=120> </A></TD>
                        <td>
                            <table align="left">
                                <TR>
                                  <TD align=right>User :</TD>
                                  <TD align=left><%=userName[2]%></TD>
                                </TR>
                                <TR>
                                  <TD align=right>Votes :</TD>
                                  <TD align=left><%=votes[2]%></TD>
                                </TR>
                                <TR>
                                  <TD colSpan=2>&nbsp; </TD>
                                </TR>
                                <TR>
                            </table>
                        </td>
                    </TR> 
                    <TR>
                      <TD colSpan=2>&nbsp; </TD>
                    </TR>
                  </TBODY>
                </TABLE>
                   </TD>
              </TR>
            </table>
          </div>
		  </td>
          <td>
		  <div id="profile_personal2" class="divBoxPodium">
            <table border="0" cellpadding="2" width="100%" height="100%">
              <TR>
                <TD class=inputlabel align=left colSpan=3>&nbsp;</TD>
              </TR>
              <TR class=inputlabel>
                <%--TD width="20%" align=right class=inputlabel><img src="/images/podium/main_invite.gif" width="100" height="100"> </TD>
                <TD width="20%" align=right class=guylabel>&nbsp;</TD--%>
                <TD width="100%" align=left valign="top">
                  <TABLE width="100%">
                      <TR class=inputlabel>
                          <TD colspan="2" align=right class=inputlabel><img src="/images/podium/hdr_invites.gif" width="235" height="24"></TD>
                      </TR>
                                       
        <%
         
        long[] invites = new long[3];
        if (invitesList != null && !invitesList.isEmpty())
        {
            System.out.println("Result size" + invitesList.size()); 
            for(int i=0;i<invitesList.size();i++)
            {
            RankingSnapshot rankingSnapshot = (RankingSnapshot)invitesList.get(i);  
            userName[i] = rankingSnapshot.getUser().getUserName();
            photoPath[i] = rankingSnapshot.getUser().getUserCountry().getPhotoLargePath();
            //userId[i] = rankingSnapshot.getUserId();
            invites[i] = rankingSnapshot.getInvitesAccepted(); 
            } 
        }
        else
        { 
            for(int i=0;i<=3;i++)
            { 
                userName[i] = "Not available";
                photoPath[i] = "na.jpg";
                //userId[i] = "Not available";
                invites[i] = 0l; 
            } 
        } 
        %>  
                    <TR>
                      <TD class=oneofthree align=middle width=150>1st<BR>
                          <A 
            href="http://profile.080808.com.my/<%=userName[0]%>"><IMG 
            src="/vphotos/<%=photoPath[0]%>" width=120></A> </TD>
                        <td>
                            <table align="left"> 
                            <TR>
                              <TD align=right>User :</TD>
                              <TD align=left><%=userName[0]%></TD>
                            </TR>
                            <TR>
                              <TD align=right>Accepted Invites :</TD>
                              <TD align=left><%=invites[0]%></TD>
                            </TR>
                            <TR>
                              <TD colSpan=2>&nbsp; </TD>
                            </TR>
                            </table>
                        </td>
                    </TR>
                    <TR>
                      <TD class=oneofthree align=middle width=150>2nd<BR>
                          <A 
            href="http://profile.080808.com.my/<%=userName[1]%>"><IMG 
            src="/vphotos/<%=photoPath[1]%>" width=120> </A></TD>
                        <td>
                            <table align="left"> 
                            <TR>
                              <TD align=right>User :</TD>
                              <TD align=left><%=userName[1]%></TD>
                            </TR>
                            <TR>
                              <TD align=right>Accepted Invites :</TD>
                              <TD align=left><%=invites[1]%></TD>
                            </TR>
                            <TR>
                              <TD colSpan=2>&nbsp; </TD>
                            </TR>
                            </table>
                        </td>
                    </TR>
                    <TR>
                      <TD class=oneofthree align=middle width=150>3rd<BR>
                          <A 
            href="http://profile.080808.com.my/<%=userName[2]%>"><IMG 
            src="/vphotos/<%=photoPath[2]%>" width=120> </A></TD>
                        <td>
                            <table align="left"> 
                            <TR>
                              <TD align=right>User :</TD>
                              <TD align=left><%=userName[2]%></TD>
                            </TR>
                            <TR>
                              <TD align=right>Accepted Invites :</TD>
                              <TD align=left><%=invites[2]%></TD>
                            </TR>
                            <TR>
                              <TD colSpan=2>&nbsp; </TD>
                            </TR>
                            </table>
                        </td>
                    </TR>
                    <TBODY>
                    </TBODY>
                </TABLE></TD>
              </TR>
            </table>
          </div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div id="profile_personal3" class="divBoxPodium">
            <table border="0" cellpadding="2" width="100%" height="100%">
              <TR>
                <TD class=inputlabel align=left colSpan=3>&nbsp;</TD>
              </TR>
              <TR class=inputlabel>
                <%--TD width="20%" align=right class=inputlabel><img src="/images/podium/main_view.gif" width="110" height="110"> </TD>
                <TD width="20%" align=right class=guylabel>&nbsp;</TD--%>
                <TD width="100%" align=left valign="top">
                  <TABLE width="100%">
                      <TR class=inputlabel>
                          <TD colspan="2" align=right class=inputlabel><img src="/images/podium/hdr_views.gif" width="235" height="24"></TD>
                      </TR>
                          <%
         
        long[] pageViews = new long[3];
         if (viewsList != null && !viewsList.isEmpty())
        {
            System.out.println("Result size" + viewsList.size()); 
            for(int i=0;i<viewsList.size();i++)
            {
                RankingSnapshot rankingSnapshot = (RankingSnapshot)viewsList.get(i);
                userName[i] = rankingSnapshot.getUser().getUserName();
                photoPath[i] = rankingSnapshot.getUser().getUserCountry().getPhotoLargePath();
                //userId[i] = rankingSnapshot.getUserId();
                pageViews[i] = rankingSnapshot.getPageView(); 
            } 
        }
        else
        { 
            for(int i=0;i<=3;i++)
            { 
                userName[i] = "Not available";
                photoPath[i] = "na.jpg";
                //userId[i] = "Not available";
                pageViews[i] = 0l; 
            } 
        } 
        %>  
                      <TR>
          <TD class=oneofthree align=middle width=150>1st<BR>
            <A 
            href="http://profile.080808.com.my/<%=userName[0]%>"><IMG 
            src="/vphotos/<%=photoPath[0]%>" width=120> </A></TD>
            <td>
                <table align="left"> 
                <TR>
                  <TD align=right>User :</TD>
                  <TD align=left><%=userName[0]%></TD></TR>
                <TR>
                  <TD align=right>Page Views :</TD>
                  <TD align=left><%=pageViews[0]%></TD></TR>
                <TR>
                  <TD colSpan=2>&nbsp; </TD></TR>
                </table>
            </td>
        </TR>
        <TR>
          <TD class=oneofthree align=middle width=150>2nd<BR>
            <A 
            href="http://profile.080808.com.my/<%=userName[1]%>"><IMG 
            src="/vphotos/<%=photoPath[1]%>" width=120> </A></TD>
            <td>
                <table align="left"> 
                <TR>
                  <TD align=right>User :</TD>
                  <TD align=left><%=userName[1]%></TD></TR>
                <TR>
                  <TD align=right>Page Views :</TD>
                  <TD align=left><%=pageViews[1]%></TD></TR>
                <TR>
                  <TD colSpan=2>&nbsp; </TD></TR>
                </table>
            </td>
        </TR>
        <TR>
          <TD class=oneofthree align=middle width=150>3rd<BR>
            <A 
            href="http://profile.080808.com.my/<%=userName[2]%>"><IMG 
            src="/vphotos/<%=photoPath[2]%>" width=120> </A></TD>
            <td>
                <table align="left"> 
                <TR>
                  <TD align=right>User :</TD>
                  <TD align=left><%=userName[2]%></TD></TR>
                <TR>
                  <TD align=right>Page Views :</TD>
                  <TD align=left><%=pageViews[2]%></TD></TR>
                <TR>
                  <TD colSpan=2>&nbsp; </TD>
                </TR>
                </table>
            </td>
        </TR>
                    <TBODY>
                    </TBODY>
                </TABLE></TD>
              </TR>
            </table>
          </div></td>
          <td><div id="profile_personal4" class="divBoxPodium">
              <table border="0" cellpadding="2" width="100%" height="100%">
                  <TR>
                      <TD class=inputlabel align=center colSpan=3> <cemter>
         <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="280" height="280">
  <param name="movie" value="/ads/280-280.swf">
  <param name="quality" value="high">
  <embed src="/ads/280-280.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="160" height="160"></embed>
</object></cemter>   </TD>
                  </TR>
              </table>
            
          </div></td>
        </tr>
      </table> 