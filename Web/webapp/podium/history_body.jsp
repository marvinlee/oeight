<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.PodiumForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.data.RankingPodium"%>   

<% 
    PodiumForm podiumForm = (PodiumForm)request.getAttribute("PodiumForm"); 
      
    List historyList = (List)podiumForm.getHistoryList(); 
    char snapshotCode = podiumForm.getSnapshotCode();

   
%>

<table border="0" cellpadding="0" width="100%">
        <tr>
          <td><div class="divBox"><center>
            <table width="409" height="391" border="0" align="center" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
                <td colspan="3"><h1>OFFICIAL PODIUM -  
                    <%
                    
                    switch (snapshotCode)
                    {
                    
                    case OEightConstants.PODIUM_TYPE_VOTES:
                        out.println("Most Votes");
                        break;
                    
                    case OEightConstants.PODIUM_TYPE_INVITES:
                        out.println("Most Invites"); 
                        break;
                    
                    case OEightConstants.PODIUM_TYPE_VIEWS:
                        out.println("Most Views"); 
                        break;
                    
                    case OEightConstants.PODIUM_TYPE_GAMER: 
                        out.println("Top Gamer");
                        break; 
                    
                    } 
                    
                    %> 
                Champions</h1></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
<%-- Loop month here START --%>
                     
        <%
            if (historyList != null && !historyList.isEmpty())
            {
                System.out.println("Result size" + historyList.size()); 
                String currentMonth = "";
                RankingPodium rankingPodium = null;
                String[] oEightId = null;
                String[] photoPath = null;
                long[] values = null;
                int count = 0;
                    
                for(int i=0;i<historyList.size();i++)
                {
                    rankingPodium = (RankingPodium)historyList.get(i);
                    
                    String snapShotMonth = DateObjectHelper.getMonthForDisplay(rankingPodium.getSnapshotMonth());
                    
                    
                    if (!currentMonth.equals(snapShotMonth))
                    {
                        currentMonth = snapShotMonth;
                        // First rank is here, reset variables
                        oEightId = new String[3];
                        photoPath = new String[3];
                        values = new long[3];
                        count = 0;
                        
                        // Set first ranking variable
                        oEightId[0] = rankingPodium.getUser().getUserName();
                        photoPath[0] = rankingPodium.getUser().getUserCountry().getPhotoLargePath();
                        values[0] = rankingPodium.getValue();
                        %> 
              <tr>
                <td>&nbsp;</td>
                <td colspan="3"><%=snapShotMonth%></td>
                <td>&nbsp;</td>
              </tr> 
                    <%
                    } 
                    else
                    {
                        // Second and third ranking is here 
                        switch (count){
                        case 1:
                            oEightId[1] = rankingPodium.getUser().getUserName();
                            photoPath[1] = rankingPodium.getUser().getUserCountry().getPhotoLargePath();
                            values[1] = rankingPodium.getValue();
                            break;
                        case 2:
                            oEightId[2] = rankingPodium.getUser().getUserName();
                            photoPath[2] = rankingPodium.getUser().getUserCountry().getPhotoLargePath();
                            values[2] = rankingPodium.getValue();
                        }
                        
                    }
                    count++;
                    
                    // if count is multiple of 3 only do printing
                    if (count%3==0){
                        String photoPodium = "podium2.gif";
                        switch (snapshotCode)
                        {

                            case OEightConstants.PODIUM_TYPE_VOTES:
                                photoPodium = "podium1.gif";
                                break;  
                        }
        %>
              <tr>
                <td>&nbsp;</td> 
                
                <td colspan="3"><img src="/images/podium/<%=photoPodium%>" width="350" height="100"></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td> <a 
            href="http://profile.080808.com.my/<%=oEightId[1]%>"><img 
            src="/vphotos/<%=photoPath[1]%>" width="120px"></a> </td>
                <td align="center"><center>
                    <a 
            href="http://profile.080808.com.my/<%=oEightId[0]%>"><img 
            src="/vphotos/<%=photoPath[0]%>" width="120px"></a>
                </center></td>
                <td><a 
            href="http://profile.080808.com.my/<%=oEightId[2]%>"><img 
            src="/vphotos/<%=photoPath[2]%>" width="120px"></a></td>
                <td>&nbsp;</td>
              </tr>
                <%
                    String valueLabel = "";
                    switch (snapshotCode)
                    {
                    
                    case OEightConstants.PODIUM_TYPE_VOTES:
                        valueLabel = "Total Votes";
                        break;
                    
                    case OEightConstants.PODIUM_TYPE_INVITES:
                        valueLabel = "Total Invites"; 
                        break;
                    
                    case OEightConstants.PODIUM_TYPE_VIEWS:
                        valueLabel = "Total Views"; 
                        break;
                    
                    case OEightConstants.PODIUM_TYPE_GAMER: 
                        valueLabel = "Total Points";
                        break; 
                    
                    } 
                    
                    %>  
              
              <tr>
                <td>&nbsp;</td>
                <td>oEight Id : <%=oEightId[1]%> <br>
                  <%=valueLabel%> : <%=values[1]%></td>
                  <td>oEight Id : <b><%=oEightId[0]%></b> <br>
                  <%=valueLabel%> : <b><%=values[0]%></b></td>
                <td>oEight Id : <%=oEightId[2]%> <br>
                  <%=valueLabel%> : <%=values[2]%></td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
               
              
<%-- Loop month here ENDS --%>
    
        <%
                    }
                } 
          }
          else
          {
        %>   
        
            <tr>
                <td>&nbsp;</td>
                <td colspan="3" align="left">No Results </td>
                <td>&nbsp;</td>
            </tr>  
        <%
          } 
        %>   
        </table></center>
          </div></td>
          <td width="300">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td width="300">&nbsp;</td>
        </tr>
      </table> 