 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants, 
                 com.esmart2u.oeight.member.web.struts.controller.SeasonGreetingForm"%>   

<% 
    SeasonGreetingForm seasonGreetingForm = (SeasonGreetingForm)request.getAttribute("SeasonGreetingForm");  
    String receiverEmail = seasonGreetingForm.getReceiverEmail();  
   
%>

    <html:form name="SeasonGreetingForm" type="com.esmart2u.oeight.member.web.struts.controller.SeasonGreetingForm" method="post" action="/seasons_greetings.do" isRelative="true">
   
    <table width="80%">
        <COL width="20%"> 
        <COL width="40%"> 
	<COL width="40%">
            <tr>
                <td class="hdr_1" colspan="3" align="left">Seasons Greetings - Sent
                </td>
            </tr>   
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>   
            
        <%
            if (StringUtils.hasValue(receiverEmail))
            { 
                        %>  
            <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                    Greeting card has been sent to : <b><%=receiverEmail%></b>
                    <br> 
                    Thank you.
                    <br>
                    <br>
                </td>
            </tr>     
          
        <% 
          }
          else
          {
        %>   
        
            <tr>
                <td class="hdr_1" colspan="3" align="center">No Email Sent.
                </td>
            </tr> 
        <%
          } 
        %>       
            <tr>
                <td>&nbsp;
                </td>
                <td align="center">    
                    <input type="button" name="back" value="Send Another" onclick="formBack('send');">  
                </td>
                <td>&nbsp;
                </td>
            </tr>
        </table>
        <input type="hidden" name="act" value="send">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    