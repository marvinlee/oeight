<style style="text/css">

.lcdstyle{ /*Example CSS to create LCD countdown look
background-color:#383838;*/
color:white;
font: bold 16px MS Sans Serif;
padding: 3px;
}

.lcdstyle sup{ /*Example CSS to create LCD countdown look*/
font-size: 40%
}

</style>

<script type="text/javascript">
 
function cdtime(container, targetdate){
if (!document.getElementById || !document.getElementById(container)) return
this.container=document.getElementById(container)
this.currentTime=new Date()
this.targetdate=new Date(targetdate)
this.timesup=false
this.updateTime()
}

cdtime.prototype.updateTime=function(){
var thisobj=this
this.currentTime.setSeconds(this.currentTime.getSeconds()+1)
setTimeout(function(){thisobj.updateTime()}, 1000) //update time every second
}

cdtime.prototype.displaycountdown=function(baseunit, functionref){
this.baseunit=baseunit
this.formatresults=functionref
this.showresults()
}

cdtime.prototype.showresults=function(){
var thisobj=this


var timediff=(this.targetdate-this.currentTime)/1000 //difference btw target date and current date, in seconds
//alert(timediff)
if (timediff<0){ //if time is up
this.timesup=true
this.container.innerHTML=this.formatresults()
return
}
var oneMinute=60 //minute unit in seconds
var oneHour=60*60 //hour unit in seconds
var oneDay=60*60*24 //day unit in seconds
var dayfield=Math.floor(timediff/oneDay)
var hourfield=Math.floor((timediff-dayfield*oneDay)/oneHour)
var minutefield=Math.floor((timediff-dayfield*oneDay-hourfield*oneHour)/oneMinute)
var secondfield=Math.floor((timediff-dayfield*oneDay-hourfield*oneHour-minutefield*oneMinute))
if (this.baseunit=="hours"){ //if base unit is hours, set "hourfield" to be topmost level
hourfield=dayfield*24+hourfield
dayfield="n/a"
}
else if (this.baseunit=="minutes"){ //if base unit is minutes, set "minutefield" to be topmost level
minutefield=dayfield*24*60+hourfield*60+minutefield
dayfield=hourfield="n/a"
}
else if (this.baseunit=="seconds"){ //if base unit is seconds, set "secondfield" to be topmost level
var secondfield=timediff
dayfield=hourfield=minutefield="n/a"
}
this.container.innerHTML=this.formatresults(dayfield, hourfield, minutefield, secondfield)
setTimeout(function(){thisobj.showresults()}, 1000) //update results every second
}
 
 

function formatresults2(){
if (this.timesup==false){ //if target date/time not yet met
var displaystring="<span class='lcdstyle'>"+arguments[0]+" <sup>days</sup> "+arguments[1]+" <sup>hours</sup> "+arguments[2]+" <sup>mins</sup> "+arguments[3]+" <sup>secs</sup></span>"
}
else{ //else if target date/time met 
var displaystring="Contest Ended, Happy New Year 2008!" //Don't display any text
//alert("Happy New Year!") //Instead, perform a custom alert
}
return displaystring
}

</script> 
<script language="JavaScript" type="text/JavaScript"> 
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->
</script>
<link href="/seasons_greetings/seasons_style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
body {
	background-image: url(http://080808.org.my/seasons_greetings/img/bg.jpg);
}
-->
</style>



<script language="javascript">  

function formBack(action)
{
    document.forms[0].act.value=action;
    document.forms[0].submit(); 
}
 
var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("senderName", "Sender's Name", "M", true);
	fieldList[counter++] = new Array("senderEmail", "Sender's Email", "M", true); 
	fieldList[counter++] = new Array("receiverName", "Recipient's Name", "M", true); 
	fieldList[counter++] = new Array("receiverEmail", "Recipient's Email", "M", true);  

	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);
	var emailMsg = checkEmail(form.senderEmail.value, "Sender's Email");
        if (emailMsg != '')
        {
                emailMsg += "\n"
        }
        emailMsg += checkEmail(form.receiverEmail.value, "Recipient's Email");
	if (emailMsg != '')
	{
		if (errMsg != '')
		{
			errMsg += "\n"
		}
		errMsg += emailMsg
	}

	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	}


		posted = true;
		document.forms[0].submit();
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 