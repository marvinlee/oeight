<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<tiles:insert page="/tiles/empty_template.jsp" flush="true">
<tiles:put name="title" type="string" value="Seasons Greetings from 080808.com.my" />
<tiles:put name="otherBodyTags" type="string" value="bgcolor=\"#383838\""  />  
<tiles:put name="javascript" value="/seasons_greetings/send_js.jsp" /> 
<tiles:put name="body" value="/seasons_greetings/static2_body.jsp" /> 
</tiles:insert>