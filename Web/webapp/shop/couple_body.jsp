<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ShopForm"%>   

 <%
    ShopForm shopForm = (ShopForm)request.getAttribute("ShopForm"); 
     
%>
<%-- This page needs total 3 columns instead of 4--%> 
        <table width="100%">
            <tr>    
                <td width="5%">&nbsp;</td>
                <td valign="top">

                <h1>Lovers Tee Order Form</h1> 

    <html:form name="ShopForm" type="com.esmart2u.oeight.member.web.struts.controller.ShopForm" method="post" scope="request"  action="/shop.do" isRelative="true">
    <div id="shoplayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="inputvalue" >
              <center><a href="/images/shop/lovers/lovers_tee.jpg" mce_href="/images/shop/lovers/lovers_tee.jpg" rel="lightbox"><img src="/images/shop/lovers/lovers_tee_t.jpg" border="1" width="240" height="180" align="center"></a></center>
          </td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr>
           <tr>
                <td class="inputlabel" align="right">Product Code :&nbsp;
                </td>
                <td align="left" class="inputvalue" >
                    couple08
                </td>
            </tr>   
            <tr>
                <td class="inputlabel" align="right">Name :&nbsp;
                </td>
                <td align="left">
                    <html:text name="ShopForm"  styleClass="inputvalue" property="name" size="30" maxlength="100"/>   
                    <html:errors property="name"/>
                </td>
            </tr>   
            <tr>
                <td class="inputlabel" align="right">Girl Design Shirt Size :&nbsp;
                </td>
                <td align="left">
                    <html:radio styleClass="inputvalue" name="ShopForm" property="f" value="M" /> M
                    <html:radio styleClass="inputvalue" name="ShopForm" property="f" value="L" /> L 
                    <html:errors property="girlSize"/>
                </td>
            </tr>   <tr>
                <td class="inputlabel" align="right">Guy Design Shirt Size :&nbsp;
                </td>
                <td align="left">
                    <html:radio styleClass="inputvalue" name="ShopForm" property="m" value="M" /> M
                    <html:radio styleClass="inputvalue" name="ShopForm" property="m" value="L" /> L  
                    <html:radio styleClass="inputvalue" name="ShopForm" property="m" value="XL" /> XL  
                    <html:radio styleClass="inputvalue" name="ShopForm" property="m" value="XXL" /> XXL     
                    <html:errors property="guySize"/>
                </td>
            </tr>  
             <tr>
                <td class="inputlabel" align="right">Contact :&nbsp;
                </td>
                <td align="left">
                    <html:text name="ShopForm"  styleClass="inputvalue" property="contactNo" size="30" maxlength="15"/>   
                    <html:errors property="contactNo"/>
                </td>
            </tr>
             <tr>
                <td class="inputlabel" align="right">Email :&nbsp;
                </td>
                <td align="left">
                    <%=shopForm.getEmail()%>
                    <%--html:text name="ShopForm"  styleClass="inputvalue" property="email" size="30" maxlength="100"/>   
                    <html:errors property="email"/--%>
                </td>
            </tr>  
             <tr>
                <td class="inputlabel" align="right">Address :&nbsp;
                </td>
                <td align="left">
                    <html:textarea name="ShopForm"  styleClass="inputvalue" property="address" cols="30" rows="5" onkeydown="textareaCheck(this, 200)"/>   
                    <html:errors property="address"/>
                </td>
            </tr>  
             <tr>
                <td class="inputlabel" align="right">Price :&nbsp;
                </td>
                <td align="left"> 
                    RM 68.00
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="inputvalue" ><br>
              <b>NOTE :</b><br>
              <b>Name</b> should be same with the account holder name of which payment will be made.<br>
              Stock is limited, we will update this form when a particular size runs out.<br>
              Delivery is made upon payment confirmation, not submission of this order form.<br><br>
              Click "Submit" button below for the confirmation page of your order form.
              </td>
            </tr> 
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr> 
                <td class="inputlabel" align="right">&nbsp;
                </td>
                <td align="left" class="formbuttonsCell" > 
                    <input type="button" name="search" class="formbuttons" value="Submit" onclick="formSubmitTest();">
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div>
        <input type="hidden" name="act" value="couple08Confirm">
        <%--html:hidden name="ShopForm" property="tokenValue"/>
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>"--%> 
    </html:form> 
                </td>
                <td width="5%">&nbsp;</td>
                </tr>
            </table>  