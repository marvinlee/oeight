 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserShop,
                 com.esmart2u.oeight.member.web.struts.controller.ShopForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%>   

<% 
    ShopForm shopForm = (ShopForm)request.getAttribute("ShopForm"); 
      
    List orderList = (List)shopForm.getOrderList(); 

   
%>
<div id="orderLayer" class="divBox">   
    <table width="100%">
        <COL width="20%"> 
        <COL width="40%">
        <COL width="40%"> 
        <tr>
            <td class="lbl" align="center"> 
                
            </td>
            <td align="left" colspan="2"> 
                
            </td>
        </tr>   
        <tr class="inputlabel">  
            <td class="inputlabel" colspan="3" align="left"><span class="inputlabel">My Orders</span>
            </td>
        </tr>   
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>  
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>  
        <tr>
            <td class="inputlabel" >No.
            </td>
            <td class="inputlabel" >Order Reference Number / Details
            </td>
            <td class="inputlabel" >Status
            </td>
        </tr>   
        
        <%
        if (orderList != null && !orderList.isEmpty())
        {  
        for(int i=0;i<orderList.size();i++)
        {
        UserShop userShop = (UserShop)orderList.get(i);
        
        
        %>  
        <tr>
            <td><%=i+1%>
            </td> 
            <td><%=userShop.getShopReferenceNo()%><br>
            <%
            if ("couple08".equals(userShop.getShopCode()))
            {
                String description = userShop.getShopDescription();
                StringTokenizer tokenizer = new StringTokenizer(description,",");
                while (tokenizer.hasMoreTokens())
                {
                    String designSize = tokenizer.nextToken();
                    designSize = designSize.replaceAll("f=","Girl Size :");
                    designSize = designSize.replaceAll("m=","Guy Size :");
                    out.print(designSize+"<br>");
                }
            } 
            %>
            </td> 
            <td><%
                char status = userShop.getShopStatus().charAt(0);
                String statusString = "";
                switch (status)
                {
                case 'N'://ShopForm.NEW:
                case 'F'://ShopForm.NEW_FAIL_NOTIFY:
                case 'S'://ShopForm.NEW_NOTIFIED:
                statusString = "Order Received";
                break;
                case 'C'://ShopForm.PAYMENT_CONFIRMED:
                statusString = "Payment Confirmed";
                break;
                case 'T'://ShopForm.IN_TRANSIT:
                statusString = "Item in Transit";
                break;
                case 'D'://ShopForm.DELIVERED:
                statusString = "Delivered";
                break;  
                } 
                
                %>
                <%=statusString%><br>
                <%
                if (StringUtils.hasValue(userShop.getTrackingNo())){
                %>
                    Tracking No: <%=userShop.getTrackingNo()%>
                <%
                }
                %>
            </td>
        </tr>     
        <tr>
            <td colspan="3"><br>
            </td>
        </tr>  
        <%
        } 
        }
        else
        {
        %>   
        
        <tr>
            <td class="hdr_1" colspan="3" align="left">You have no orders yet
            </td>
        </tr>  
        <%
        } 
        %>       
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>  
</table></div>