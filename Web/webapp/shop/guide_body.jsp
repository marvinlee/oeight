<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%> 
        <table>
            <tr>   

                <td valign="top">

                <h1>Shop Guide</h1> 

    <div id="guidelayer" class="divBox"> 
        <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         <tr bgcolor="#333333">
        <td colspan="2"><span class="infoBar"> Shop Guide </span></td>
        </tr>
      <tr>
        <td colspan="2"><p> This shopping guide is divided into the following sections: <a name="top"></a></p>
          <p><strong> <a href="#order">How To Order</a> </strong></p>
          <p><strong> <a href="#payment">Payment</a> </strong></p>
          <p><strong> <a href="#tracking">Order Tracking</a></strong></p>
          <p><strong> <a href="#delivery">Delivery</a></strong></p>
          <p><strong> <a href="#refund">Refund Policy</a></strong></p>
          <p><strong> <a href="#international">International Order </a></strong></p>
          <p><strong> </strong></p> <br> <br> <br>
          </td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="order" id="order"></a>How To Order</span> </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>  1 - Go to the <a href="/shop.do?act=couple08">Order Now</a> page for the order form. If you are not logged-in, you will be requested to login. <br>
</p>
          <p> 2 - Fill in the t-shirt size, your name, contact information and mailing address. <br>
          </p>
          <p>  3 - Click &quot;Submit&quot; and verify that the information you have provided is correct. <br>
            </p>
          <p> 4 - You will receive an email from us with your order reference id and payment instructions. <br> 
            </p>
          <p>&nbsp; </p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="payment" id="payment"></a>Payment</span> </td> 
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>We currently accept Funds Transfer via Maybank2u.com or Direct Deposit via Kawanku ATM to our Maybank account.</p>
          <p>&nbsp;</p>
          <p><strong><u>Funds Transfer via Maybank2u.com</u> </strong></p>
          <p>Pay to:<strong> E SMART SOLUTIONS</strong></p>
          <p>Account Number : <strong>5122 3132 8102  </strong></p>
          <p>Email : <strong>sales@080808.com.my</strong></p>
          <p>Kindly use the  email address above  for confirmation of your payment. </p>
          <p>&nbsp;</p>
          <p><u><strong>Direct Deposit via Maybank Kawanku ATM </strong></u></p>
          <p>Pay to:<strong> E SMART SOLUTIONS</strong></p>
          <p>Account Number :<strong> 5122 3132 8102 </strong></p>          
          <p>Please  fax (<strong>03-78063194</strong>) or email ( <a href="mailto:sales@080808.com.my">sales@080808.com.my</a> ) us the transaction slip together with your  order reference number for payment confirmation. </p>
          <p>&nbsp;</p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="tracking" id="tracking"></a>Order Tracking</span> </td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>Upon receiving of your payment confirmation, we will 


 send <strong> a confirmation e-mail together with a tracking number </strong>(within 24 hours of payment confirmation)<strong>.</strong></p>
          <p>The tracking number will enable you to track the delivery online. </p>
          <p>&nbsp;</p>
          <p><a href="#top">Back to Top</a></p>          <p></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="delivery" id="delivery"></a>Delivery</span> </td>   
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p> Currently we only deliver within Malaysia and the delivery of item is free of charge.</p>
          <p>We deliver the items in an envelope box via Pos Express and it usually takes 1-3 working days to reach your mailing address.</p>
          <p>In the payment confirmation email, you will be given a tracking number to track the via Pos Express tracking system.</p>
          <p> The postman will drop off <strong>all Pos Express mailings </strong>, so please <strong>ensure that someone is in the house to accept the order. We will not be responsible for lost or misplaced orders. </strong>  </p>
          <p>&nbsp;</p>
          <p><a href="#top">Back to Top</a> </p>          <p></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="refund" id="refund"></a>Refund Policy </span> </td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>By shopping through this website, you are backed up by an unconditional money-back guarantee or free replacement. </p>
          <p>We should be notified of any problem immediately upon receipt of the delivery. We reserve the right to refuse a request for refund if such request is not received by us on a timely basis or we have confirmation of delivery. </p>
          <p>All prices shown and all transactions are in Ringgit Malaysia (RM). </p>          <p>All refund request will be dealt on a case by case basis. </p>
          <p>              <strong> In case of returns</strong>, postage, admin and handling charges will be borne by the customer.
              </li>
</p>
          <p>&nbsp;            </p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="international" id="international"></a>International Order</span> </td>   
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>We currently do not deliver outside  If you have any enquiries, please submit us a <a href="/feedback.do">Feedback</a> for further information. </p>
          <p>&nbsp;</p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="5%">&nbsp;</td>
        <td><p><br> 
          </p>
          </td>
      </tr>
                    </table>
                </div>            </td>
                </tr>
            </table>                    