<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ShopForm"%>   

 <%
    ShopForm shopForm = (ShopForm)request.getAttribute("ShopForm"); 
     
%>
<%-- This page needs total 3 columns instead of 4--%> 
        <table width="100%">
            <tr>    
                <td width="5%">&nbsp;</td>
                <td valign="top">

                <h1>Lovers Tee Order Received</h1> 

     <div id="shoplayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;<br><br></td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="inputvalue" >
             We have received your order and an email has been sent to <%=shopForm.getEmail()%><br>
Within 24 hours of your payment, the payment confirmation email will be sent to you.<br>
You can check your order list <a href="/shop.do?act=orderList">here</a>. 
<br><br><br>
 <b>Note</b> : Please check your "<b>Junk/Spam email</b>" folder. If you find our email there, select the confirmation message and click "<b>Not Junk</b>" or "<b>Not Spam</b>". This will help future messages to get through. 
<br><br><br>                        
          </td>
        </tr>
        
     </table></div> 
                </td>
                <td width="5%">&nbsp;</td>
                </tr>
            </table>  