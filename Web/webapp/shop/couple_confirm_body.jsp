<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ShopForm"%>   

 <%
    ShopForm shopForm = (ShopForm)request.getAttribute("ShopForm"); 
     
%>
<%-- This page needs total 3 columns instead of 4--%> 
        <table width="100%">
            <tr>    
                <td width="5%">&nbsp;</td>
                <td valign="top">

                <h1>Lovers Tee Order Confirmation</h1> 

    <html:form name="ShopForm" type="com.esmart2u.oeight.member.web.struts.controller.ShopForm" method="post" scope="request"  action="/shop.do" isRelative="true">
    <div id="shoplayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="inputvalue" >
              <center><a href="/images/shop/lovers/lovers_tee.jpg" mce_href="/images/shop/lovers/lovers_tee.jpg" rel="lightbox"><img src="/images/shop/lovers/lovers_tee_t.jpg" border="1" width="240" height="180" align="center"></a></center>
          </td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr>
           <tr>
                <td class="inputlabel" align="right">Product Code :&nbsp;
                </td>
                <td align="left" class="inputvalue" >
                    couple08
                </td>
            </tr>   
            <tr>
                <td class="inputlabel" align="right">Name :&nbsp;
                </td>
                <td align="left">
                    <%=shopForm.getName()%>  
                </td>
            </tr>   
            <tr>
                <td class="inputlabel" align="right">Girl Design Shirt Size :&nbsp;
                </td>
                <td align="left">
                    <%=shopForm.getF()%>  
                </td>
            </tr>   <tr>
                <td class="inputlabel" align="right">Guy Design Shirt Size :&nbsp;
                </td>
                <td align="left">
                    <%=shopForm.getM()%>  
                </td>
            </tr>  
             <tr>
                <td class="inputlabel" align="right">Contact :&nbsp;
                </td>
                <td align="left"> 
                    <%=shopForm.getContactNo()%>  
                </td>
            </tr>
             <tr>
                <td class="inputlabel" align="right">Email :&nbsp;
                </td>
                <td align="left">
                    <%=shopForm.getEmail()%> 
                </td>
            </tr>  
             <tr>
                <td class="inputlabel" align="right">Address :&nbsp;
                </td>
                <td align="left">
                    <%=shopForm.getAddress()%> 
                </td>
            </tr>
             <tr>
                <td class="inputlabel" align="right">Price :&nbsp;
                </td>
                <td align="left"> 
                    RM 68.00
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="inputvalue" ><br>
              <br><br> 
              </td>
            </tr> 
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr> 
                <td class="inputlabel" align="right">&nbsp;
                </td>
                <td align="left" class="formbuttonsCell" >  
                    <input type="button" name="back" class="formbuttons" value="Back" onclick="formBack('couple08');">&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="button" name="confirm" class="formbuttons" value="Confirm" onclick="formSubmit();">
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div>
        <input type="hidden" name="act" value="orderReceived"> 
        <input type="hidden" name="shopReferenceNo" value="<%=shopForm.getShopReferenceNo()%>"> 
        </html:form>
                </td>
                <td width="5%">&nbsp;</td>
                </tr>
            </table>  