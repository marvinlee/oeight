<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils,  
                 com.esmart2u.oeight.member.helper.OEightConstants, 
                 com.esmart2u.oeight.member.web.struts.controller.PostForm"%>  

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:write name="PostForm" property="title" filter="true"/></title>
    </head>
    <body>

    <p>Link Submitted Successfully</p>
    
     <div id="link" class="divBox">  
       <table width="80%">
           <COL width="5%"> 
           <COL width="40%"> 
           <COL width="55%">
           <tr>
               <td colspan="3">&nbsp;</td> 
           </tr>
           <tr>
               <td>Thumbnail</td>
               <td class="hdr_1" colspan="2" align="left"><a href="<bean:write name="PostForm" property="url" filter="false"/>" ><h1><bean:write name="PostForm" property="title" filter="true"/></h1></a>
               </td>
           </tr> 
           <tr>
               <td colspan="3"><bean:write name="PostForm" property="description" filter="true"/></td> 
           </tr>
           <tr>
               <td align="left">Submitted by:<bean:write name="PostForm" property="userId" filter="true"/></td> 
               <td>&nbsp;</td> 
               <td align="right"><bean:write name="PostForm" property="datePostedString" filter="false"/></td> 
           </tr>
       </table>
     </div>

    </body>
</html>
