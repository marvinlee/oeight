 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils,  
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserInvites, 
                 com.esmart2u.oeight.member.web.struts.controller.PostForm"%>   

<% 
    PostForm postForm = (PostForm)request.getAttribute("PostForm");  
   
    %>
 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
<script language="javascript">
function formBack(action)
{
    document.forms[0].act.value=action;
    document.forms[0].submit(); 
}

var posted = false; 
function formSubmit()
{

	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
        <%
        if ("Y".equals(postForm.getUrlOk()))
        {
        %>  
	fieldList[counter++] = new Array("title", "Title", "M", true); 
	fieldList[counter++] = new Array("description", "Description", "M", true); 
        <%
        }
        else
        {
        %> 
	fieldList[counter++] = new Array("url", "URL", "M", true);  
        <%
        } 
        %>   


	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);  
         
	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	}


    posted = true; 
    document.forms[0].submit(); 
}    
</script>
<script type="text/javascript" src="/js/check.js"></script>
<script type="text/javascript" src="/js/messages.js"></script>        
        
    </head>
    <body>

    <h1>Share a Link</h1>
    
    <html:form name="PostForm" type="com.esmart2u.oeight.member.web.struts.controller.PostForm" method="post" action="/post.do" isRelative="true">
    <div id="postlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="inputvalue" >Share a link:</td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <%
        if ("Y".equals(postForm.getUrlOk()))
        {
        %>
          <tr>
                <td class="inputlabel" align="right">URL :&nbsp;
                </td>
                <td align="left"> 
                    <a target="_blank" href="<bean:write name="PostForm" property="url" filter="false"/>"><bean:write name="PostForm" property="url" filter="false"/></a>
                </td>
            </tr> 
            <tr>
                <td class="inputlabel" align="right">Title :&nbsp;
                </td>
                <td align="left">
                    <html:text name="PostForm"  styleClass="inputvalue" property="title" size="100" maxlength="100"/>   
                    <html:errors property="title"/>
                </td>
            </tr>
             <tr>
                <td class="inputlabel" align="right">Description :&nbsp;
                </td>
                <td align="left">  
                    <html:textarea name="PostForm"  styleClass="inputvalue" property="description" cols="30" rows="5" onkeydown="textareaCheck(this, 200)"/>   
                    <html:errors property="description"/>
                    <input type="hidden" name="act" value="submitted">
                </td>
            </tr> 
        <%
        }
        else
        {
        %> 
          <tr>
                <td class="inputlabel" align="right">URL :&nbsp;
                </td>
                <td align="left">
                    <html:text name="PostForm"  styleClass="inputvalue" property="url" size="100" maxlength="200"/>   
                    <html:errors property="url"/>
                    <input type="hidden" name="act" value="checked">
                </td>
            </tr>  
        <%
        } 
        %>   
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr> 
                <td class="inputlabel" align="right">&nbsp;
                </td>
                <td align="left" class="formbuttonsCell" > 
                    <html:hidden name="PostForm" property="urlOk"/>
                    <input type="button" name="search" class="formbuttons" value="Submit" onclick="formSubmit();">
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div>
    </html:form>
    </body>
</html>
