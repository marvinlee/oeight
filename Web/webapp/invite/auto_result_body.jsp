<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserInvites,
                 com.esmart2u.solution.base.helper.contactlist.ContactImpl,
                 com.esmart2u.oeight.member.web.struts.controller.InviteForm"%>   

<% 
    InviteForm inviteForm = (InviteForm)request.getAttribute("InviteForm"); 
      
    List membersEmailList = (List)inviteForm.getMembersEmailList(); 
    List nonMembersEmailList = (List)inviteForm.getNonMembersEmailList(); 

   
    %>
  
      <div id="inviteLayer" class="divBox">   
        <table width="400">
            <COL width="20%"> 
            <COL width="40%">
            <COL width="40%"> 
            <tr>
                <td class="lbl" align="center"> 
                    
                </td>
                <td align="left" colspan="2"> 
                    
                </td>
            </tr>   
            
            <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br><br>
                </td>
            </tr>  
            
            
            <%
            if (membersEmailList != null && !membersEmailList.isEmpty())
            { 
            %>
               
            <tr>
                <td class="inputlabel" colspan="3" align="left">Buddies Requested
                </td>
            </tr>  
            
            <%
            for(int i=0;i<membersEmailList.size();i++)
            {
            ContactImpl contact = (ContactImpl)membersEmailList.get(i);
            
            
            %>  
            <tr>
                <td>&nbsp;</td>
                <td colspan="2" align="left">
                    <%--=contact.getUserName()--%>
                    <a href='http://profile.080808.com.my/<%=contact.getUserName()%>'><img src="/vthumb/<%=contact.getPhotoSmallPath()%>"  width="38px"><br><b><%=contact.getUserName()%></b></a>
                </td>
            </tr>     
            
            <%
            } 
            }
            else
            {
            %>   
            
            <%--tr>
                <td class="hdr_1" colspan="3" align="left">No Buddy Request Sent
                </td>
            </tr--%>  
            <%
            } 
            %>   
             <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br><br><br><br><br>
                </td>
            </tr>   
         
            <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br><br>
                </td>
            </tr>     
            <tr>
                <td class="inputlabel" colspan="3" align="left">Invites Sent
                </td>
            </tr>  
            
            
            <%
            if (nonMembersEmailList != null && !nonMembersEmailList.isEmpty())
            { 
            for(int i=0;i<nonMembersEmailList.size();i++)
            {
            UserInvites userInvite = (UserInvites)nonMembersEmailList.get(i);
            
            
            %>  
            <tr>
                <td>&nbsp;</td>
                <td colspan="2" align="left"><%=userInvite.getEmail()%>
                </td>
            </tr>     
            
            <%
            } 
            }
            else
            {
            %>   
            
            <tr>
                <td class="hdr_1" colspan="3" align="left">No Invites Sent
                </td>
            </tr>  
            <%
            } 
            %>   
            <tr>
                <td class="hdr_1" colspan="3" align="left">&nbsp;
                </td>
            </tr>  
           <tr class="formbuttonsCell"> 
                <td class="formbuttonsCell" align="left"><a href="/invite.do?act=auto">Search Again</a>
                </td>
                <td class="formbuttonsCell" colspan="2" align="right">
                </td>
            </tr>
    </table></div>
