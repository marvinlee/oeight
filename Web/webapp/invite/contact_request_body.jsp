 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="java.util.*"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.contactlist.ContactImpl,
                 com.esmart2u.oeight.member.web.struts.controller.InviteForm,
                 com.esmart2u.solution.base.helper.PropertyManager" %>
<% 
    InviteForm inviteForm = (InviteForm)request.getAttribute("InviteForm"); 
      
    List membersList = (List)inviteForm.getMembersEmailList(); 
    List nonMembersList = (List)inviteForm.getNonMembersEmailList(); 
    
%>
    
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="4">
        <table width="100%">
            <tr>  
    
    <td width="5%">&nbsp;</td>
 
    <td width="80%" valign="top">

    <h1>Search and Invite Friends</h1>
    
    <html:form name="InviteForm" type="com.esmart2u.oeight.member.web.struts.controller.InviteForm" method="post" action="/invite.do" isRelative="true">
        <div id="inviteLayer" class="divBox">  
            <table width="80%">
                <COL width="20%"> 
                <COL width="40%"> 
                <COL width="40%">
              
                <tr>
                    <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br> 
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td class="inputlabel" colspan="2" align="left">  
                        <html:errors property="inviteRequest"/> 
                        <br> 
                        Select contacts who have join as your buddy and invite others after submission:<br> <br> 
                    </td>
                </tr> 
 

             <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                    <br>
                        <table border="0">
                            <tr>
                                <td class="inputlabel" align="left">Email</td>
                                <td class="inputlabel" align="left">Password</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="emailUser" maxlength="100">&nbsp;<select name="provider"><option value=""></option><option value="hotmail.com">@hotmail.com</option><option value="yahoo.com">@yahoo.com</option><option value="gmail.com">@gmail.com</option></select>&nbsp;
                                </td>
                                <td><input type="password" name="password" maxlength="100">&nbsp;<br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                <i>Your password is not stored and is used for this search only.</i></td> 
                            </tr>
                        </table>
                    <br>  
                </td>
            </tr>  
            
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="center"> 
                        <br><center>
                        <input type="button" class="formbuttons" name="ok" value="Submit" onclick="formSubmit();">                          
                        </center>
                        <br>   
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        <br>
                        <br>  
                    </td>
                </tr> 
               
                
        </table></div>
        <input type="hidden" name="act" value="contactSelect">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    <br><br>  

    </html:form> 