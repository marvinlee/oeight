 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
  
    <html:form name="InviteForm" type="com.esmart2u.oeight.member.web.struts.controller.InviteForm" method="post" action="/invite.do" isRelative="true">
        <div id="inviteLayer" class="divBox">   
            <table width="400">
                <COL width="40%"> 
                <COL width="20%">
                <COL width="40%"> 
                <tr>
                    <td class="lbl" align="center"> 
                        
                    </td>
                    <td align="left" colspan="2"> 
                        
                    </td>
                </tr>   
                <tr class="formbuttonsCell">
                    <td class="hdr_1" align="left"><h1>Invite Friends</h1>
                    </td>
                    <td class="formbuttonsCell" colspan="2" align="right"><span class="formbuttonsCell"><a href="/invite.do?act=history">Invitation History</a></span>
                    </td>
                </tr>
                <tr>
                    <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br><br>
                    </td>
                </tr>  
                <tr>
                    <td class="hdr_m" align="left">&nbsp;
                    </td>
                    <td colspan="2" align="left"> 
                        <span class="inputlabel" >Email addresses :</span>&nbsp;<br>
                        <html:textarea styleClass="inputvalue" name="InviteForm" property="emails" cols="50" rows="5" onkeydown="textareaCheck(this, 1500)"/>   
                        <html:errors property="emails"/> <br>
                        You can seperate emails addresses with spaces, comma (,) or semi-colon (;)
                    </td>
                </tr>
                <%--tr>
                <td class="lbl" align="right">My message :&nbsp;
                </td>
                <td align="left">
                        <html:textarea name="InviteForm" property="message" cols="50" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <html:errors property="message"/>  
                </td>
            </tr--%> 
           
                <tr>
                    <td>&nbsp;
                    </td>
                    <td colspan="2" align="right">    
                        <input class="formbuttons" type="button" name="send" value="Send" onclick="formSubmit();">  
                    </td>
                </tr> 
                <tr>
                    <td colspan="3">&nbsp;
                    </td>
                </tr>
        </table></div> 
        <input type="hidden" name="act" value="invited">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
