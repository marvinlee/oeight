 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserInvites,
                 com.esmart2u.oeight.member.web.struts.controller.InviteForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%>   

<% 
    InviteForm inviteForm = (InviteForm)request.getAttribute("InviteForm"); 
      
    List historyList = (List)inviteForm.getHistoryList(); 

   
%>
<div id="inviteLayer" class="divBox">   
    <table width="400">
        <COL width="20%"> 
        <COL width="40%">
        <COL width="40%"> 
        <tr>
            <td class="lbl" align="center"> 
                
            </td>
            <td align="left" colspan="2"> 
                
            </td>
        </tr>   
        <tr class="formbuttonsCell"> 
            <td class="formbuttonsCell" align="left"><a href="/invite.do?act=main">Invite Friends</a>
            </td>
            <td class="formbuttonsCell" colspan="2" align="right"><span class="formbuttonsCell">Invitation History</span>
            </td>
        </tr>   
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>   
        <tr>
            <td class="inputlabel" colspan="3" align="right">Total Sent :&nbsp;<span class="inputvalue"><%=inviteForm.getTotalInvitesSent()%></span>
            </td>
        </tr>   
        <tr>
            <td class="inputlabel" colspan="3" align="right">Total Accepted :&nbsp;<span class="inputvalue"><%=inviteForm.getTotalInvitesAccepted()%></span>
            </td>
        </tr>      
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>  
        <tr>
            <td class="inputlabel" >No.
            </td>
            <td class="inputlabel" >Email Address
            </td>
            <td class="inputlabel" >Status
            </td>
        </tr>   
        
        <%
        if (historyList != null && !historyList.isEmpty())
        {
        System.out.println("Result size" + historyList.size());  
        for(int i=0;i<historyList.size();i++)
        {
        UserInvites userInvite = (UserInvites)historyList.get(i);
        
        
        %>  
        <tr>
            <td><%=i+1%>
            </td> 
            <td><%=userInvite.getEmail()%>
            </td> 
            <td><%
                char status = userInvite.getStatus();
                String statusString = "";
                switch (status)
                {
                case OEightConstants.MAIL_STATUS_NEW:
                case OEightConstants.MAIL_STATUS_SENT:
                statusString = "Sent";
                break;
                case OEightConstants.MAIL_STATUS_SEND_FAILED:
                statusString = "Sending Failed";
                break;
                case OEightConstants.MAIL_STATUS_ACCEPTED:
                statusString = "Accepted - " + DateObjectHelper.getPrintedDate(userInvite.getDateAccepted());
                break;  
                } 
                
                %>
                <%=statusString%>
            </td>
        </tr>     
        
        <%
        } 
        }
        else
        {
        %>   
        
        <tr>
            <td class="hdr_1" colspan="3" align="left">No Results
            </td>
        </tr>  
        <%
        } 
        %>       
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>  
</table></div>