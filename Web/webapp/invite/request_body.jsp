 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                com.esmart2u.solution.base.helper.PropertyManager" %>
    
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="4">
        <table width="100%">
            <tr>  
    
    <td width="5%">&nbsp;</td>
 
    <td width="80%" valign="top">

    <h1>Validation Email</h1>
    
    <html:form name="InviteForm" type="com.esmart2u.oeight.member.web.struts.controller.InviteForm" method="post" action="/invite.do" isRelative="true">
        <div id="inviteLayer" class="divBox">  
            <table width="80%">
                <COL width="20%"> 
                <COL width="40%"> 
                <COL width="40%">
                <%--tr>
                <td class="hdr_1" align="left"><nobr>Request Invite</nobr>
                </td>
                <td class="hdr_1" colspan="2" align="right">
                </td>
            </tr--%> 
                <tr>
                    <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br> 
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left">  
                        <br> 
                        Enter your email below to regenerate email and validate your profile.<br> 
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        <br><br>
                        Email address :&nbsp; 
                        <html:text name="InviteForm" styleClass="inputvalue" property="emails" size="20" maxlength="50"/> 
                        <html:errors property="emails"/> 
                        <input type="button" class="formbuttons" name="send" value="Submit" onclick="formSubmit();"> 
                        <br>   
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        <br>
                        <br>  
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        <br>
                        <br>  
                        <%
                        //String friendsterProfile = (String)PropertyManager.getValue(PropertyConstants.SYSTEM_FRIENDSTER_PROFILE);
                        %> 
                        <%--Check out our official Friendster profile at <a href="<%=friendsterProfile%>" target="#"><%=friendsterProfile%></a>.<br>
                        If you have a Friendster account, add us as your friend!<br><br>
                        2) Check if any of your friends are friends of our <b>luckyEight</b> (<a href="<%=friendsterProfile%>"><%=friendsterProfile%></a>)<br>
                        &nbsp;&nbsp;&nbsp;and request invite from them. Help them to get 'Most Invites'.
                        <a href="http://www.friendster.com/affiliate.php?aff_id=48225160&link_id=2&count=click" target="#"><script type="text/Javascript"> var aff_id=48225160; var link_id=2; var greeting='Join me on Friendster!'; var photo='http://photos-160.friendster.com/e1/photos/06/15/48225160/1_453970908';</script> <script type="text/JavaScript" src="http://www.friendster.com/superfriendster.js"></script></a>
                    --%></td>
                </tr>  
                <%--tr>
                    <td>&nbsp;
                    </td>
                    <td align="center">    
                        <input type="button" name="send" value="Submit" onclick="formSubmit();"> 
                        <br><br>   
                    </td>
                    <td>&nbsp;
                    </td>
                </tr--%>
                
        </table></div>
        <input type="hidden" name="act" value="requestSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    <br><br> <center>
        <div style="width:565px;height:355px" align="center">
            <nobr><a href="/"><img src="/images/login/part3.gif"><img src="/images/login/part4.gif"></a></nobr>
        </div></center>

    </html:form> 