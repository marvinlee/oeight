<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="java.util.*" %>
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.InviteForm" %>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.EmailProviderComboHelper" %>
   <% 
    InviteForm inviteForm = (InviteForm)request.getAttribute("InviteForm");
    Vector providerList = EmailProviderComboHelper.getProviderList();
    %>
    
    <html:form name="InviteForm" type="com.esmart2u.oeight.member.web.struts.controller.InviteForm" method="post" action="/invite.do" isRelative="true">
        <div id="inviteLayer" class="divBox">   
            <table width="400">
                <COL width="40%"> 
                <COL width="20%">
                <COL width="40%"> 
                <tr>
                    <td class="lbl" align="center"> 
                        
                    </td>
                    <td align="left" colspan="2"> 
                        
                    </td>
                </tr>   
                <tr class="formbuttonsCell">
                    <td class="hdr_1" align="left"><h1>Invite Friends</h1>
                    </td>
                    <td class="formbuttonsCell" colspan="2" align="right"><span class="formbuttonsCell"><a href="/invite.do?act=history">Invitation History</a></span>
                    </td>
                </tr>
                <tr>
                    <td align="left">&nbsp;</td>
                    <td class="inputlabel" colspan="2" align="left">  
                        <html:errors property="inviteRequest"/> 
                        <br> 
                        Select contacts to send invites after submission:<br> <br> 
                    </td>
                </tr> 
                <tr>
                    <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br><br>
                    </td>
                </tr>  
                <tr>
                    <td class="hdr_m" align="left">&nbsp;
                    </td>
                    <td colspan="2" align="left"> 
                       <br>
                        <table border="0">
                            <tr>
                                <td class="inputlabel" align="left">Email</td>
                                <td class="inputlabel" align="left">Password</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="emailUser" maxlength="100">&nbsp;
                                    <select name="provider">
                                         <%
                                    for(int i=0;i<providerList.size();i++)
                                    {  
                                        String[] provider = (String[])providerList.get(i);
                                        out.print("<option value='" + provider[0] +"'"); 
                                        if (provider[0].equals(inviteForm.getProvider()))
                                            out.print(" selected ");
                                        out.print(">");
                                        out.println(provider[1] + "</option>");
                                    }
                                    %></select>&nbsp;
                                </td>
                                <td><input type="password" name="password" maxlength="100"  onkeydown="if(event.keyCode==13)formSubmit();" >&nbsp;<br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                <i>Your password is not stored and is used for this search only.</i></td> 
                            </tr>
                        </table>
                        <br>  
                    </td>
                </tr> 
           
                <tr>
                    <td>&nbsp;
                    </td>
                    <td colspan="2" align="right">    
                        <input class="formbuttons" type="button" name="send" value="Submit" onclick="formSubmit();">  
                    </td>
                </tr> 
                <tr>
                    <td colspan="3">&nbsp;<br><br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;<a href="/invite.do?act=main">Send Invites Manually</a>
                    </td>
                </tr>
        </table></div> 
        <input type="hidden" name="act" value="autoSelect">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
