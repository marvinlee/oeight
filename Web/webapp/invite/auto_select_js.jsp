<SCRIPT LANGUAGE="Javascript">
function formBack(action)
{
    document.forms[0].act.value=action;
    document.forms[0].submit(); 
}
var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}
  
	posted = true;
	document.forms[0].submit();
}

function toggleSelectMembers(exby)
{
    var userNames = document.forms[0].membersUserNameString;
    if (userNames.length > 1){ 
        for (i = 0; i < userNames.length; i++)
            userNames[i].checked = exby.checked? true:false;
    }else{ 
        userNames.checked = exby.checked? true:false;
    } 
}

function toggleSelectNonMembers(exby)
{
    var emails = document.forms[0].nonMembersEmailString;
    if (emails.length > 1){ 
        for (i = 0; i < emails.length; i++)
            emails[i].checked = exby.checked? true:false;
    }else{ 
        emails.checked = exby.checked? true:false;
    } 
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 