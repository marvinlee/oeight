 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserInvites,
                 com.esmart2u.oeight.member.web.struts.controller.InviteForm"%>   

<% 
    InviteForm inviteForm = (InviteForm)request.getAttribute("InviteForm"); 
      
    List addressList = (List)inviteForm.getAddressList(); 

   
%>
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="4">
        <table width="100%">
            <tr>  
    
    <td width="5%">&nbsp;</td>
 
    <td width="80%" valign="top">

    <h1>Validation Email - Sent</h1>

    <html:form name="InviteForm" type="com.esmart2u.oeight.member.web.struts.controller.InviteForm" method="post" action="/invite.do" isRelative="true">
        <div id="inviteLayer" class="divBox">  
            <table width="80%">
                <COL width="20%"> 
                <COL width="40%"> 
                <COL width="40%">
                <%--tr>
                <td class="hdr_1" colspan="3" align="left">Invite - Sent
                </td>
            </tr--%>   
                <tr>
                    <td colspan="3">&nbsp;
                    </td>
                </tr>   
                
                <%
                if (addressList != null && !addressList.isEmpty())
                {
                System.out.println("Result size" + addressList.size());  
                for(int i=0;i<addressList.size();i++)
                {
                UserInvites userInvite = (UserInvites)addressList.get(i);
                
                
                %>  
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        A validation link has been sent to your email address at : <b><%=userInvite.getEmail()%></b>
                        <br>
                        Please check your email and click on the link to validate your profile.
                        <br><br><br>
                        <b>Note</b> : Please check your "<b>Junk/Spam email</b>" folder. If you find our email there, select the confirmation message and click "<b>Not Junk</b>" or "<b>Not Spam</b>". This will help future messages to get through. 
                        <br>
                    </td>
                </tr>    
                <tr>
                    <td colspan="3">&nbsp;
                    </td>
                </tr>     
                
                <%
                } 
                }
                else
                {
                %>   
                
                <tr>
                    <td class="hdr_1" colspan="3" align="center">No Email Sent.
                    </td>
                </tr>     
                <tr>
                    <td>&nbsp;
                    </td>
                    <td align="center">    
                        <input class="formbuttons" type="button" name="back" value="Back" onclick="formSubmit();">  
                    </td>
                    <td>&nbsp;
                    </td>
                </tr> 
                <tr>
                    <td colspan="3">&nbsp;
                    </td>
                </tr>   
                <%
                } 
                %>   
        </table></div>
        <input type="hidden" name="act" value="request">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form> 