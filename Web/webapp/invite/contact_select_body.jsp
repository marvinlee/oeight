 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@taglib uri="/WEB-INF/struts-html-el.tld" prefix="html-el"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="java.util.*"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.contactlist.ContactImpl,
                 com.esmart2u.oeight.member.web.struts.controller.InviteForm,
                 com.esmart2u.solution.base.helper.PropertyManager" %>
<%
    InviteForm inviteForm = (InviteForm)request.getAttribute("InviteForm"); 
      
    List membersList = (List)inviteForm.getMembersEmailList(); 
    List nonMembersList = (List)inviteForm.getNonMembersEmailList(); 
    
%>
    
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="4">
        <table width="100%">
            <tr>  
    
    <td width="5%">&nbsp;</td>
 
    <td width="80%" valign="top">

    <h1>Search and Invite Friends</h1>
    
    <html:form name="InviteForm" type="com.esmart2u.oeight.member.web.struts.controller.InviteForm" method="post" action="/invite.do" isRelative="true">
        <div id="inviteLayer" class="divBox">  
            <table width="80%">
                <COL width="20%"> 
                <COL width="40%"> 
                <COL width="40%">
              
                <tr>
                    <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br> 
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left">  
                        <html:errors property="inviteRequest"/>  <br> 
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td class="inputlabel" colspan="2" align="left">   
                        <br> 
                        <h1>Add contacts who have join as your buddy and invite others:</h1><br> <br> 
                    </td>
                </tr> 

                <%
                if (membersList != null && !membersList.isEmpty())
                { 

                %>  
                <tr>
                <td colspan="3">
                        <%--input type="button" class="formbuttons" name="Delete" value="Delete" onclick="formDelete();"--%>
                        <table>
                        <tr>
                            <td width="10%" class="inputlabel"><input type="checkbox" name="selectAllMembers" onclick="toggleSelectMembers(this);"></td>
                            <td width="30%" class="inputlabel">Member</td>
                            <td width="60%" class="inputlabel">Email</td>
                        </tr>
                        <logic:iterate id="contact" name="InviteForm" property="membersEmailList" type="com.esmart2u.solution.base.helper.contactlist.ContactImpl">
                        <bean:define id="emailAddress" name="contact" property="emailAddress" type="java.lang.String"/> 
                        <bean:define id="userName" name="contact" property="userName" type="java.lang.String"/>    
                        <bean:define id="photoSmallPath" name="contact" property="photoSmallPath" type="java.lang.String"/>    
                        <tr>
                            <td><html-el:checkbox name="InviteForm" property="membersUserNameString" value="${userName}"/><%--bean:write name="messageIdString" /--%></td>
                            <td><a href='http://profile.080808.com.my/<bean:write name="userName"/>'><img src="/vthumb/<bean:write name="photoSmallPath" />"  width="38px"><br><b><bean:write name="userName" filter="true"/></b></a></td>
                            <td><bean:write name="emailAddress" filter="true"/></td>   
                            </td>
                        </tr>
                    </logic:iterate> 
                    </table>
            <script>toggleSelectMembers(document.forms[0].selectAllMembers)</script>
            </td>
            </tr>     

            <%
                        }
            else
            {
            %>   

            <tr>
                <td colspan="3"><br><br><br> 
                </td>
            </tr>     
            <tr>
                <td class="hdr_1" colspan="3" align="left">No members found
                </td>
            </tr>  
            <%
            } 
            %>  

             <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                    <br>
                    <br>  
                </td>
            </tr> 
            

                <%
                if (nonMembersList != null && !nonMembersList.isEmpty())
                { 

                %>  
                <tr>
                <td colspan="3">
                        <%--input type="button" class="formbuttons" name="Delete" value="Delete" onclick="formDelete();"--%>
                        <table>
                        <tr>
                            <td width="10%" class="inputlabel"><input type="checkbox" name="selectAllNonMembers" onclick="toggleSelectNonMembers(this);"></td>
                            <td width="30%" class="inputlabel">Name</td>
                            <td width="60%" class="inputlabel">Email</td>
                        </tr>
                        <logic:iterate id="contact" name="InviteForm" property="nonMembersEmailList" type="com.esmart2u.solution.base.helper.contactlist.ContactImpl">
                        <bean:define id="emailAddress" name="contact" property="emailAddress" type="java.lang.String"/> 
                        <tr>
                            <td><html-el:checkbox name="InviteForm" property="nonMembersEmailString" value="${emailAddress}"/><%--bean:write name="messageIdString" /--%></td>
                            <td><logic:present name="contact" property="name">
                                    <bean:define id="name" name="contact" property="name" type="java.lang.String"/>       
                                    <bean:write name="name" filter="true"/> 
                                </logic:present>
                            </td>
                            <td><bean:write name="emailAddress" filter="true"/></td>   
                            </td>
                        </tr>
                    </logic:iterate> 
                    </table>
            <script>toggleSelectNonMembers(document.forms[0].selectAllNonMembers)</script>
            </td>
            </tr>     

            <%
                        }
            else
            {
            %>   

            <tr>
                <td colspan="3"><br><br><br> 
                </td>
            </tr>     
            <tr>
                <td class="hdr_1" colspan="3" align="left">No available contacts found
                </td>
            </tr>  
            <%
            } 
            %>  
            
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        <br><br>
                        <%
                            if ( (membersList != null && !membersList.isEmpty()) || (nonMembersList != null && !nonMembersList.isEmpty()) )
                            {
                        %>
                        <input type="button" class="formbuttons" name="ok" value="Submit" onclick="formSubmit();"> 
                           <%
                            }
                            else
                            {
                            %>  
                        <input type="button" class="formbuttons" name="send" value="Search Again" onclick="formBack('contactRequest');">  
                            <%
                            } 
                            %>  
                        <br>   
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        <br>
                        <br>  
                    </td>
                </tr> 
               
                
        </table></div>
        <input type="hidden" name="act" value="contactResult">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    <br><br>  

    </html:form> 