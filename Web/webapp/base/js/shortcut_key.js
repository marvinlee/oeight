var isIE = (navigator.appVersion.indexOf("MSIE") != -1)? true : false;
var isNS = (navigator.appName == "Netscape")? true : false;
var isNS4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion, 10) == 4)? true : false;

//isIE = document.all;
//isNS = !document.all && document.getElementById;
//isNS4 = document.layers;

//var isNS4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion, 10) == 4)? true : false;
//var isNS6AndUp = (navigator.appName == "Netscape" && parseInt(navigator.appVersion, 10) >= 5)? true : false;

//if (isNS4) document.captureEvents(Event.KEYDOWN);
//if (isNS4) document.captureEvents(Event.KEYUP);

// to lock mouse's right click
if (isIE || isNS)
{
	document.oncontextmenu = right;
}
else
{
	document.captureEvents(Event.MOUSEDOWN);
	document.onmousedown = function right(e)
	{
		if (isNS4)
		{
			if (e.which == 2 ||
				e.which == 3 ||
				e.which == 5 ||
				e.which == 10 ||
				e.which == 11 ||
				e.which == 12 ||
				e.which == 20 ||
				e.which == 25)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}

document.onkeydown = function checkKeyDown(e)
{
	var keys = new Array();
	keys["f112"] = 'f1';
	keys["f113"] = 'f2';
	keys["f114"] = 'f3';
	keys["f115"] = 'f4';
	keys["f116"] = 'f5';
	keys["f117"] = 'f6';
	keys["f118"] = 'f7';
	keys["f119"] = 'f8';
	keys["f120"] = 'f9';
	keys["f121"] = 'f10';
	keys["f122"] = 'f11';
	keys["f123"] = 'f12';
	keys["f27"]  = 'Escape';

	// for IE only
	if (isIE)
	{
		// capture and remap F-key
		if (event && keys["f" + event.keyCode])
		{
			event.keyCode = 505;
		}

		if (event && event.keyCode == 505)
		{
			// must return false or the browser will execute old code
			return false;
		}
		else
		{
			// suppressing ctrl, alt, shift
			if (event.ctrlKey)
			{
				// capturing Ctrl-O
				if (event.keyCode == 79)
				{
					event.keyCode = 505;
				}

				// kill the shortcut event
				event.cancelBubble = true;
				event.returnValue = false;
				return false;
			}
			else if (event.altKey)
			{
				// capturing Alt-Home
				if (event.keyCode == 36)
				{
					alert("Alt-Home is locked.");
				}

				// kill the shortcut event
				event.cancelBubble = true;
				event.returnValue = false;
				return false;
			}
			else if (event.shiftKey)
			{
				// capturing Shift-Insert
				if (event.keyCode == 45 ||    // Insert
					event.keyCode == 46)      // Delete
				{
					// kill the shortcut event
					event.cancelBubble = true;
					event.returnValue = false;
					return false;
				}
			}
		}
	}
	else if (isNS6AndUp)
	{
		// capture and remap F-key
		if (e && keys["f" + e.which])
		{
			e.which = 505;

			// kill the shortcut event
			e.cancelBubble = true;
			e.preventDefault();
			e.stopPropagation();
			return false;
		}

		if (e && e.which == 505)
		{
			// must return false or the browser will execute old code
			return false;
		}
		else
		{
			// suppressing browser shortcuts like Ctrl-N(new Page), Ctrl-B(Organize favs)...
			if (e.ctrlKey ||
				e.altKey)
			{
				e.which = 505;

				// kill the shortcut event
				e.cancelBubble = true;
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
		}
	}

//	var oEvent = (isIE)? event : e;
//	var intKeyCode = (isIE)? oEvent.keyCode : oEvent.which;
//	var bCtrlKey = (isNS4)? (oEvent.modifiers && Event.CONTROL_MASK)? true : false : (oEvent.ctrlKey)? true : false;
//	var bAltKey = (isNS4)? (oEvent.modifiers && Event.ALT_MASK)? true : false : (oEvent.altKey)? true : false;

//	if (isNS4 && bCtrlKey)
//	{
//		if (intKeyCode == 0)
//		{
//			return false;
//		}
//	}

//	if (isNS4 && bAltKey)
//	{
//		if (intKeyCode == 0)
//		{
//			return false;
//		}
//	}

//	if ((isIE || isNS6AndUp) && intKeyCode == 122)
//	{
//		if (isIE)
//		{
//			event.returnValue = false;
//			event.cancelBubble = true;
//		}
//		else if (isNS6AndUp)
//		{
//			e.preventDefault();
//			e.stopPropagation();
//		}
//		return false;
//	}

//	var oRegExp = (isNS4)? new RegExp(/37|39|67|86|97|98|101|104|105|110|56/) : new RegExp(/37|39|65|66|67|69|72|73|78|86|87|104/);

//	if (bCtrlKey && oRegExp.test(intKeyCode))
//	{
//		if (isIE)
//		{
//			event.returnValue = false;
//			event.cancelBubble = true;
//		}
//		else if (isNS6AndUp)
//		{
//			e.cancelBubble = true;
//			e.preventDefault();
//			e.stopPropagation();
//		}
//		return false;
//	}

//	if (bAltKey && oRegExp.test(intKeyCode))
//	{
//		if (isIE)
//		{
//			event.returnValue = false;
//			event.cancelBubble = true;
//		}
//		else if (isNS6AndUp)
//		{
//			e.cancelBubble = true;
//			e.preventDefault();
//			e.stopPropagation();
//		}
//		return false;
//	}
}