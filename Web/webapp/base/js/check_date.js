//                                                   #############
//                                                   ## WARNING ##
//                                                   #############
//
// WARNING:                 This file has been deprecated since 25/02/2004 by wkchee. Please use date.js.
//                          DO NOT APPEND THIS FILE ANYMORE UNLESS BUG FIXES!
//                          CHANGES MUST BE AT LEAST MADE IN date.js
//
// Purpose:                 JS validation for date input
//
// Change log :
//   ? (siripong)           First creation.
//   ? (tylaw)              Added function checkLaterCurrentDate.
//   20031218 (wcloh)       Modified function dateSelectedValidate.
//                            - Only perform checking if day, month, year value if not null.
//   20040202 (wcloh)       Added checkDateRangeAfter to check date range later.

alert("[esmart2u] DEPRECATED. Please change to use date.js");

function checkLeapYear(intYear, dateName)
{
	var validNum = "0123456789";

	for (var i = 0; i < intYear.length; i++)
	{
		var temp = "" + intYear.substring(i, i + 1);
		if (validNum.indexOf(temp) == "-1")
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
	}

	for (var i = 0; i < intYear.length; i++)
	{
		var temp = "" + intYear.substring(i, i + 1);
		if (validNum.indexOf(temp) == "-1")
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
	}

	if ((intYear % 4 == 0) && ((intYear % 100 != 0) || (intYear % 400 == 0)))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function dateSelectedValidate(fromDay, fromMonth, fromYear, dateName, strLocale)
{
	var validNum = "0123456789";

//	remarked by waichin, 18/12/2003

//	if (fromDay.length == 0)
//		return generateValidateMessage(invalidDateFormat, [dateName]);

//	if (fromMonth.length == 0)
//		return generateValidateMessage(invalidDateFormat, [dateName]);

//	if (fromYear.length == 0)
//		return generateValidateMessage(invalidDateFormat, [dateName]);

	fromDay = trim(fromDay);
	fromMonth = trim(fromMonth);
	fromYear = trim(fromYear);

	// first, check year by Locale
	if (fromYear != "")    // added by waichin, 18/12/2003, so that can cater for the senario where no year value required for date input
	{
		var intYear = parseInt(fromYear);
		if (isNaN(intYear))
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
		else
		{
			if (strLocale.substr(0, 2) == "th")
			{
				intYear = intYear - 543;
				fromYear = "" + intYear;
			}
		}

		for (var i = 0; i < fromYear.length; i++)
		{
			var temp = "" + fromYear.substring(i, i + 1);
			if (validNum.indexOf(temp) == "-1")
			{
				return generateValidateMessage(invalidDateFormat, [dateName]);
			}
			else
			{
				if (parseInt(fromYear) < 1000)
				{
					return generateValidateMessage(invalidDateFormat, [dateName]);
				}
			}
		}
	}

	if (fromMonth != "")    // added by wkchee, 25/02/2004
	{
		if (fromMonth != "01" && fromMonth != "02" && fromMonth != "03" && fromMonth != "04" &&
			fromMonth != "05" && fromMonth != "06" && fromMonth != "07" && fromMonth != "08" &&
			fromMonth != "09" && fromMonth != "10" && fromMonth != "11" && fromMonth != "12")
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
	}

	if (fromDay != "")    // added by waichin, 18/12/2003
	{
		for (var i = 0; i < fromDay.length; i++)
		{
			var temp = "" + fromDay.substring(i, i + 1);
			if (validNum.indexOf(temp) == "-1")
			{
				return generateValidateMessage(invalidDateFormat, [dateName]);
			}
		}

		if (fromDay < 1 || fromDay > 31)    // added checking fromDay > 31 by wkchee, 25/02/2004
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
	}

	// added by wkchee, 25/02/2004
	if (fromDay != "" && fromMonth != "")
	{
		if (fromMonth == "04" || fromMonth == "06" || fromMonth == "09" || fromMonth == "11")
		{
			if (fromDay < 1 || fromDay > 30)
			{
				return generateValidateMessage(invalidDateFormat, [dateName]);
			}
		}
		else if (fromMonth == "01" || fromMonth == "03" || fromMonth == "05" || fromMonth == "07" ||
			fromMonth == "08" || fromMonth == "10" || fromMonth == "12")
		{
			if (fromDay < 1 || fromDay > 31)
			{
				return generateValidateMessage(invalidDateFormat, [dateName]);
			}
		}

		if (fromYear != "")
		{
			if (fromMonth == "02")
			{
				if (checkLeapYear(parseInt(fromYear), dateName))
				{
					if (fromDay < 1 || fromDay > 29)
					{
						return generateValidateMessage(invalidDateFormat, [dateName]);
					}
				}
				else
				{
					if (fromDay < 1 || fromDay > 28)
					{
						return generateValidateMessage(invalidDateFormat, [dateName]);
					}
				}
			}
		}
		else
		{
			if (fromMonth == "02")
			{
				if (fromDay < 1 || fromDay > 29)
				{
					return generateValidateMessage(invalidDateFormat, [dateName]);
				}
			}
		}
	}

//	remarked by wkchee, 25/02/2004

//	if (fromMonth != "" && fromYear != "")    // added by waichin, 18/12/2003
//	{
//		if (fromMonth == "02" || fromMonth == 'Feb')
//		{
//			if (checkLeapYear(parseInt(fromYear), dateName))
//			{
//				if (fromDay > 29)
//				{
//					return generateValidateMessage(invalidDateFormat, [dateName]);
//				}
//			}
//			else
//			{
//				if (fromDay > 28)
//				{
//					return generateValidateMessage(invalidDateFormat, [dateName]);
//				}
//			}
//		}
//	}
//
//	if (fromMonth != "")    // added by waichin, 18/12/2003
//	{
//		if ((fromMonth == "04") || (fromMonth == "06") || (fromMonth == "09") || (fromMonth == "11") ||
//			(fromMonth == "Apr") || (fromMonth == "Jun") || (fromMonth == "Sep") || (fromMonth == "Nov"))
//		{
//			if (fromDay > 30)
//			{
//				return generateValidateMessage(invalidDateFormat, [dateName]);
//			}
//		}
//	}
//
//	if (fromMonth != "" && fromYear != "")    // added by waichin, 18/12/2003
//	{
//		if ((fromMonth == "01") || (fromMonth == "03") || (fromMonth == "05") || (fromMonth == "07") ||
//			(fromMonth == "08") || (fromMonth == "10") || (fromMonth == "12") || (fromMonth == "Jan") ||
//			(fromMonth == "Mar") || (fromMonth == "May") || (fromMonth == "Jul") || (fromMonth == "Aug") ||
//			(fromMonth == "Oct") || (fromMonth == "Dec"))
//		{
//			if (fromDay > 31)
//			{
//				return generateValidateMessage(invalidDateFormat, [dateName]);
//			}
//		}
//	}

	return "";
}

function checkDateRange(fromDay, fromMonth, fromYear, toDay, toMonth, toYear, fromDateName, toDateName, strLocale)
{
	// first, check year by Locale
	var intFromYear = parseInt(fromYear);
	var intToYear = parseInt(toYear);

	if (isNaN(intFromYear))
	{
		return generateValidateMessage(invalidDateFormat, [fromDateName]);
	}
	if (isNaN(intToYear))
	{
		return generateValidateMessage(invalidDateFormat, [toDateName]);
	}
	if (strLocale.substr(0, 2) == "th")
	{
		intFromYear = intFromYear - 543;
		intToYear = intToYear - 543;
		fromYear = "" + intFromYear;
		toYear = "" + intToYear;
	}

	if (fromMonth == "02" || fromMonth == "Feb")
	{
		if (checkLeapYear(parseInt(fromYear), fromDateName))
		{
			if (fromDay > 29)
			{
				return generateValidateMessage(invalidDateFormat, [fromDateName]);
			}
		}
		else
		{
			if (fromDay > 28)
			{
				return generateValidateMessage(invalidDateFormat, [fromDateName]);
			}
		}
	}

	if (toMonth == "02" || toMonth == "Feb")
	{
		if (checkLeapYear(parseInt(toYear), toDateName))
		{
			if (toDay > 29)
			{
				return generateValidateMessage(invalidDateFormat, [toDateName]);
			}
		}
		else
		{
			if (toDay > 28)
			{
				return generateValidateMessage(invalidDateFormat, [toDateName]);
			}
		}
	}

	if ((fromMonth == "04") || (fromMonth == "06") || (fromMonth == "09") || (fromMonth == "11") ||
		(fromMonth == "Apr") || (fromMonth == "Jun") || (fromMonth == "Sep") || (fromMonth == "Nov"))
	{
		if (fromDay > 30)
		{
			return generateValidateMessage(invalidDateFormat, [fromDateName]);
		}
	}
	else if ((toMonth == "04") || (toMonth == "06") || (toMonth == "09") || (toMonth == "11") ||
		(toMonth == "Apr") || (toMonth == "Jun") || (toMonth == "Sep") || (toMonth == "Nov"))
	{
		if (toDay > 30)
		{
			return generateValidateMessage(invalidDateFormat, [toDateName]);
		}
	}

	var fromDate = new Date(fromYear, fromMonth - 1, fromDay)
	var toDate = new Date(toYear, toMonth -1, toDay);
	if (fromDate > toDate)
	{
		return generateValidateMessage(dateTooLate, [fromDateName, toDateName]);
	}
	return "";
}

function checkLaterCurrentDate(fromDay, fromMonth, fromYear, fromDateName, toDateName, strLocale)
{
	alert("[esmart2u] DEPRECATED. Please change your javascript method from checkLaterCurrentDate to checkBeforeCurrentDate.");
	return checkBeforeCurrentDate(fromDay, fromMonth, fromYear, fromDateName, toDateName, strLocale);
}

function checkBeforeCurrentDate(fromDay, fromMonth, fromYear, fromDateName, toDateName, strLocale)
{
	var ua = navigator.userAgent.toLowerCase();
	var isNS = ((this.isGecko) ? (ua.indexOf("netscape") != -1) : ((ua.indexOf("mozilla") != -1) && (ua.indexOf("spoofer") == -1) && (ua.indexOf("compatible") == -1) && (ua.indexOf("opera") == -1) && (ua.indexOf("webtv") == -1) && (ua.indexOf("hotjava") == -1)));

	var today = new Date();
	var todayYear = today.getYear();
	// check for the NS browser.
	if (isNS)
		todayYear = todayYear + 1900;

	if (strLocale.substr(0, 2) == "th")
	{
		todayYear = todayYear + 543;
	}

	return checkDateRange(fromDay, fromMonth, fromYear, today.getDate(), today.getMonth() + 1, todayYear , fromDateName, toDateName, strLocale);
}

function checkAfterCurrentDate(toDay, toMonth, toYear, fromDateName, toDateName, strLocale)
{
	var ua = navigator.userAgent.toLowerCase();
	var isNS = ((this.isGecko) ? (ua.indexOf("netscape") != -1) : ((ua.indexOf("mozilla") != -1) && (ua.indexOf("spoofer") == -1) && (ua.indexOf("compatible") == -1) && (ua.indexOf("opera") == -1) && (ua.indexOf("webtv") == -1) && (ua.indexOf("hotjava") == -1)));

	var today = new Date();
	var todayYear = today.getYear();
	// check for the NS browser.
	if (isNS)
		todayYear = todayYear + 1900;

	if (strLocale.substr(0, 2) == "th")
	{
		todayYear = todayYear + 543;
	}

	var result = checkDateRange(today.getDate(), today.getMonth() + 1, todayYear, toDay, toMonth, toYear, fromDateName, toDateName, strLocale);
	if (result != '')
	{
		result = generateValidateMessage(dateTooEarly, [toDateName, fromDateName]);
	}
	return result;
}