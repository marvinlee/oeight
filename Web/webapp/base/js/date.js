// Purpose:                 JS validation for date input.
//
// Change log :
//   20040302 (wkchee)      Enhancement on date checking againts server system date instead of client date.
//                          Some functions checking againts client date are deprecated.
//   20040225 (wkchee)      Migrate check_data.js to data.js with some deprecated methods moved from validation.js
//                          Enhancement on date validation to accept the following input pattern:
//                            dd
//                            mm
//                            yyyy
//                            dd-mm
//                            mm-yyyy
//                            dd-mm-yyyy
//   20040202 (wcloh)       Added checkDateRangeAfter to check date range later.
//   20031218 (wcloh)       Modified function dateSelectedValidate.
//                            - Only perform checking if day, month, year value if not null.
//   ? (tylaw)              Added function checkLaterCurrentDate.
//   ? (siripong)           First creation.

/**
 * Check if date is valid.
 */
function isDate(day, month, year)
{
	// checks if date passed is valid
	// will accept dates in following format:
	// isDate(dd,mm,ccyy), or
	// isDate(dd,mm) - which defaults to the current year, or
	// isDate(dd) - which defaults to the current month and year.
	// Note, if passed the month must be between 1 and 12, and the
	// year in ccyy format.
	var today = new Date();
	year = (!year)? y2k(today.getYear()) : year;
	month = (!month)? today.getMonth() : month - 1;
	if (!day) return false;

	var testDate = new Date(year, month, day);
	if ((y2k(testDate.getYear()) == year) &&
		(month == testDate.getMonth()) &&
		(day == testDate.getDate()))
	{
		return true;
	}
	else
	{
		return false
	}
}

/**
 * Y2K check.
 */
function y2k(number)
{
	return (number < 1000) ? number + 1900 : number;
}

// returns true if the given date is more than or equal to today
function timeDifferenceWithToday(laterDay, laterMonth, laterYear)
{
	var now = new Date();

	// return timeDifference(today, laterDate);
	return timeDifference(now.getDate(), now.getMonth() + 1, now.getYear(), laterDay, laterMonth, laterYear);
}

// returns true if the given later date is more than or equal to earlier date
function timeDifference(earlierDay, earlierMonth, earlierYear, laterDay, laterMonth, laterYear)
{
	var earlierDate = new Date(earlierYear, earlierMonth, earlierDay);
	var laterDate = new Date(laterYear, laterMonth, laterDay);

	// return timeDifference(earlierDate, laterDate);
	var difference = laterDate.getTime() - earlierDate.getTime();
	var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);

	if (daysDifference < 0)
	{
		// laterDate is earlier than earlierDate
		return false;
	}
	else
	{
		return true;
	}
}

function checkLeapYear(intYear, dateName)
{
	var validNum = "0123456789";

	for (var i = 0; i < intYear.length; i++)
	{
		var temp = "" + intYear.substring(i, i + 1);
		if (validNum.indexOf(temp) == "-1")
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
	}

	for (var i = 0; i < intYear.length; i++)
	{
		var temp = "" + intYear.substring(i, i + 1);
		if (validNum.indexOf(temp) == "-1")
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
	}

	if ((intYear % 4 == 0) && ((intYear % 100 != 0) || (intYear % 400 == 0)))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function dateSelectedValidate(fromDay, fromMonth, fromYear, dateName, strLocale)
{
	var validNum = "0123456789";

//	remarked by waichin, 18/12/2003

//	if (fromDay.length == 0)
//		return generateValidateMessage(invalidDateFormat, [dateName]);

//	if (fromMonth.length == 0)
//		return generateValidateMessage(invalidDateFormat, [dateName]);

//	if (fromYear.length == 0)
//		return generateValidateMessage(invalidDateFormat, [dateName]);

	fromDay = trim(fromDay);
	fromMonth = trim(fromMonth);
	fromYear = trim(fromYear);

	// first, check year by Locale
	if (fromYear != "")    // added by waichin, 18/12/2003, so that can cater for the senario where no year value required for date input
	{
		var intYear = parseInt(fromYear);
		if (isNaN(intYear))
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
		else
		{
			if (strLocale.substr(0, 2) == "th")
			{
				intYear = intYear - 543;
				fromYear = "" + intYear;
			}
		}

		for (var i = 0; i < fromYear.length; i++)
		{
			var temp = "" + fromYear.substring(i, i + 1);
			if (validNum.indexOf(temp) == "-1")
			{
				return generateValidateMessage(invalidDateFormat, [dateName]);
			}
			else
			{
				if (parseInt(fromYear) < 1000)
				{
					return generateValidateMessage(invalidDateFormat, [dateName]);
				}
			}
		}
	}

	if (fromMonth != "")    // added by wkchee, 25/02/2004
	{
		if (fromMonth != "01" && fromMonth != "02" && fromMonth != "03" && fromMonth != "04" &&
			fromMonth != "05" && fromMonth != "06" && fromMonth != "07" && fromMonth != "08" &&
			fromMonth != "09" && fromMonth != "10" && fromMonth != "11" && fromMonth != "12")
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
	}

	if (fromDay != "")    // added by waichin, 18/12/2003
	{
		for (var i = 0; i < fromDay.length; i++)
		{
			var temp = "" + fromDay.substring(i, i + 1);
			if (validNum.indexOf(temp) == "-1")
			{
				return generateValidateMessage(invalidDateFormat, [dateName]);
			}
		}

		if (fromDay < 1 || fromDay > 31)    // added checking fromDay > 31 by wkchee, 25/02/2004
		{
			return generateValidateMessage(invalidDateFormat, [dateName]);
		}
	}

	// added by wkchee, 25/02/2004
	if (fromDay != "" && fromMonth != "")
	{
		if (fromMonth == "04" || fromMonth == "06" || fromMonth == "09" || fromMonth == "11")
		{
			if (fromDay < 1 || fromDay > 30)
			{
				return generateValidateMessage(invalidDateFormat, [dateName]);
			}
		}
		else if (fromMonth == "01" || fromMonth == "03" || fromMonth == "05" || fromMonth == "07" ||
			fromMonth == "08" || fromMonth == "10" || fromMonth == "12")
		{
			if (fromDay < 1 || fromDay > 31)
			{
				return generateValidateMessage(invalidDateFormat, [dateName]);
			}
		}

		if (fromYear != "")
		{
			if (fromMonth == "02")
			{
				if (checkLeapYear(parseInt(fromYear), dateName))
				{
					if (fromDay < 1 || fromDay > 29)
					{
						return generateValidateMessage(invalidDateFormat, [dateName]);
					}
				}
				else
				{
					if (fromDay < 1 || fromDay > 28)
					{
						return generateValidateMessage(invalidDateFormat, [dateName]);
					}
				}
			}
		}
		else
		{
			if (fromMonth == "02")
			{
				if (fromDay < 1 || fromDay > 29)
				{
					return generateValidateMessage(invalidDateFormat, [dateName]);
				}
			}
		}
	}

//	remarked by wkchee, 25/02/2004

//	if (fromMonth != "" && fromYear != "")    // added by waichin, 18/12/2003
//	{
//		if (fromMonth == "02" || fromMonth == 'Feb')
//		{
//			if (checkLeapYear(parseInt(fromYear), dateName))
//			{
//				if (fromDay > 29)
//				{
//					return generateValidateMessage(invalidDateFormat, [dateName]);
//				}
//			}
//			else
//			{
//				if (fromDay > 28)
//				{
//					return generateValidateMessage(invalidDateFormat, [dateName]);
//				}
//			}
//		}
//	}
//
//	if (fromMonth != "")    // added by waichin, 18/12/2003
//	{
//		if ((fromMonth == "04") || (fromMonth == "06") || (fromMonth == "09") || (fromMonth == "11") ||
//			(fromMonth == "Apr") || (fromMonth == "Jun") || (fromMonth == "Sep") || (fromMonth == "Nov"))
//		{
//			if (fromDay > 30)
//			{
//				return generateValidateMessage(invalidDateFormat, [dateName]);
//			}
//		}
//	}
//
//	if (fromMonth != "" && fromYear != "")    // added by waichin, 18/12/2003
//	{
//		if ((fromMonth == "01") || (fromMonth == "03") || (fromMonth == "05") || (fromMonth == "07") ||
//			(fromMonth == "08") || (fromMonth == "10") || (fromMonth == "12") || (fromMonth == "Jan") ||
//			(fromMonth == "Mar") || (fromMonth == "May") || (fromMonth == "Jul") || (fromMonth == "Aug") ||
//			(fromMonth == "Oct") || (fromMonth == "Dec"))
//		{
//			if (fromDay > 31)
//			{
//				return generateValidateMessage(invalidDateFormat, [dateName]);
//			}
//		}
//	}

	return "";
}

// DEPRECATED!
// Please refer function compareDate(...)
function checkDateRange(fromDay, fromMonth, fromYear, toDay, toMonth, toYear, fromDateName, toDateName, strLocale)
{
	// first, check year by Locale
	var intFromYear = parseInt(fromYear);
	var intToYear = parseInt(toYear);

	if (isNaN(intFromYear))
	{
		return generateValidateMessage(invalidDateFormat, [fromDateName]);
	}
	if (isNaN(intToYear))
	{
		return generateValidateMessage(invalidDateFormat, [toDateName]);
	}
	if (strLocale.substr(0, 2) == "th")
	{
		intFromYear = intFromYear - 543;
		intToYear = intToYear - 543;
		fromYear = "" + intFromYear;
		toYear = "" + intToYear;
	}

//	remarked by wkchee, 25/02/2004
//	if (fromMonth == "02" || fromMonth == "Feb")
    if (fromMonth == "02")
	{
		if (checkLeapYear(parseInt(fromYear), fromDateName))
		{
			if (fromDay > 29)
			{
				return generateValidateMessage(invalidDateFormat, [fromDateName]);
			}
		}
		else
		{
			if (fromDay > 28)
			{
				return generateValidateMessage(invalidDateFormat, [fromDateName]);
			}
		}
	}

//	remarked by wkchee, 25/02/2004
//	if (toMonth == "02" || toMonth == "Feb")
    if (toMonth == "02")
	{
		if (checkLeapYear(parseInt(toYear), toDateName))
		{
			if (toDay > 29)
			{
				return generateValidateMessage(invalidDateFormat, [toDateName]);
			}
		}
		else
		{
			if (toDay > 28)
			{
				return generateValidateMessage(invalidDateFormat, [toDateName]);
			}
		}
	}

//	remarked by wkchee, 25/02/2004
//	if ((fromMonth == "04") || (fromMonth == "06") || (fromMonth == "09") || (fromMonth == "11") ||
//		(fromMonth == "Apr") || (fromMonth == "Jun") || (fromMonth == "Sep") || (fromMonth == "Nov"))
    if (fromMonth == "04" || fromMonth == "06" || fromMonth == "09" || fromMonth == "11")
	{
		if (fromDay > 30)
		{
			return generateValidateMessage(invalidDateFormat, [fromDateName]);
		}
	}
//	remarked by wkchee, 25/02/2004
//	else if ((toMonth == "04") || (toMonth == "06") || (toMonth == "09") || (toMonth == "11") ||
//		(toMonth == "Apr") || (toMonth == "Jun") || (toMonth == "Sep") || (toMonth == "Nov"))
	else if (toMonth == "04" || toMonth == "06" || toMonth == "09" || toMonth == "11")
	{
		if (toDay > 30)
		{
			return generateValidateMessage(invalidDateFormat, [toDateName]);
		}
	}

	var fromDate = new Date(fromYear, fromMonth - 1, fromDay)
	var toDate = new Date(toYear, toMonth -1, toDay);
	if (fromDate > toDate)
	{
		return generateValidateMessage(dateTooLate, [fromDateName, toDateName]);
	}
	return "";
}

// DEPRECATED!
// We should not use this function to check date as the date is obtained from client side.
function checkLaterCurrentDate(fromDay, fromMonth, fromYear, fromDateName, toDateName, strLocale)
{
	alert("[esmart2u] DEPRECATED. Please change your javascript method from checkLaterCurrentDate to checkBeforeCurrentDate.");
	return checkBeforeCurrentDate(fromDay, fromMonth, fromYear, fromDateName, toDateName, strLocale);
}

// DEPRECATED!
// We should not use this function to check date as the date is obtained from client side.
function checkBeforeCurrentDate(fromDay, fromMonth, fromYear, fromDateName, toDateName, strLocale)
{
	var ua = navigator.userAgent.toLowerCase();
	var isNS = ((this.isGecko) ? (ua.indexOf("netscape") != -1) : ((ua.indexOf("mozilla") != -1) && (ua.indexOf("spoofer") == -1) && (ua.indexOf("compatible") == -1) && (ua.indexOf("opera") == -1) && (ua.indexOf("webtv") == -1) && (ua.indexOf("hotjava") == -1)));

	var today = new Date();
	var todayYear = today.getYear();
	// check for the NS browser.
	if (isNS)
		todayYear = todayYear + 1900;

	if (strLocale.substr(0, 2) == "th")
	{
		todayYear = todayYear + 543;
	}

	return checkDateRange(fromDay, fromMonth, fromYear, today.getDate(), today.getMonth() + 1, todayYear , fromDateName, toDateName, strLocale);
}

// DEPRECATED!
// We should not use this function to check date as the date is obtained from client side.
function checkAfterCurrentDate(toDay, toMonth, toYear, fromDateName, toDateName, strLocale)
{
	var ua = navigator.userAgent.toLowerCase();
	var isNS = ((this.isGecko) ? (ua.indexOf("netscape") != -1) : ((ua.indexOf("mozilla") != -1) && (ua.indexOf("spoofer") == -1) && (ua.indexOf("compatible") == -1) && (ua.indexOf("opera") == -1) && (ua.indexOf("webtv") == -1) && (ua.indexOf("hotjava") == -1)));

	var today = new Date();
	var todayYear = today.getYear();
	// check for the NS browser.
	if (isNS)
		todayYear = todayYear + 1900;

	if (strLocale.substr(0, 2) == "th")
	{
		todayYear = todayYear + 543;
	}

	var result = checkDateRange(today.getDate(), today.getMonth() + 1, todayYear, toDay, toMonth, toYear, fromDateName, toDateName, strLocale);
	if (result != '')
	{
		result = generateValidateMessage(dateTooEarly, [toDateName, fromDateName]);
	}
	return result;
}

/**
 * Use to compare date1 against date2.
 */
function compareDate(fromDay, fromMonth, fromYear, toDay, toMonth, toYear)
{
    var date1 = "" + fromYear + fromMonth + fromDay;
    var date2 = "" + toYear + toMonth + toDay;

    if (date1 > date2)
    {
        return 1;
    }
    else if (date1 < date2)
    {
        return -1;
    }
    else if (date1 == date2)
    {
        return 0;
    }
}