function onTabClick(formName, actionRequest, tabName)
{
	// tabName is not used, useful for dynamic tabs
	form = eval("document." + formName);
	form.action = actionRequest;
	if (form.currentPage != null)
	{
		form.currentPage.value = 1;
	}
	form.submit();
}