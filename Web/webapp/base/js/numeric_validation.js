//                                                   #############
//                                                   ## WARNING ##
//                                                   #############
//
// WARNING:                 This file has been deprecated since 25/02/2004 by wkchee. Please use numeric.js.
//                          DO NOT APPEND THIS FILE ANYMORE UNLESS BUG FIXES!
//                          CHANGES MUST BE AT LEAST MADE IN numeric.js
//
// Dependency:              validate.js
//
// Note:                    1)  To use this file, declare 2 javascript variable strictly known as:
//                              a)  groupingSeparator
//                              b)  monetaryDecimalSeparator
//                          2)  Before using function in this file, ensure that input value has been validated
//
// Change log :
//   20040225 (wkchee)      Fixed bug in number formatting:
//
//   20040210 (wkchee)      Fixed bug in number formatting:
//                              .30 -> .3.00 (bug fixed, now is 0.30)
//                          Fixed bug in removeGroupingSeparator(amount)
//                          Not to remove grouping seperator whenever the amount is not in valid format.
//   20040130 (wkchee)      Added unformat* methods to unformat object. Also fixed the formatting bug
//                          for negative value as well as the generateMaxLimit method.
//   20031208 (danielang)   1)  Added convertToDefaultDecimalSymbol() method prompted by the need of having
//                              locale-sensitive validation
//
//   20031218 (danielang)   1)  Added validateFractionLength() method to impose fraction length checking
//
//   20031231 (danielang)   1)  Created new method removeSign() to convert negative numeric value
//                              to positive
//                          2)  Updated validateNumericInputLength() method to cater for negative input
//                          3)  Updated formatNumeric() to cater for negative input formatting
//
//   20040103 (danielang)   1)  Created new method replaceWithDefaultMonetaryDecimalSeparator() to replace
//                              monetary decimal separator with default value (.)
//                          2)  Made DEFAULT_MONETARY_DECIMAL_SYMBOL global
//
//   20040113 (wkchee)      Perform formatNumeric only when input is not empty
//
//   20040130 (wkchee)      Added unformat* methods to unformat object. Also fixed the formatting bug
//                          for negative value as well as the generateMaxLimit method.

alert("[esmart2u] DEPRECATED. Please change to use numeric.js");

// Format rate input to desired format (rate maximum fraction length is explicitly declared)
// formatRate(HtmlElementObject object, boolean grouped)
function formatRate(amountObj, grouped, length, fieldName)
{
	if (formatRate.arguments.length != 4)
	{
		alert("[esmart2u] DEPRECATED. Please change your javascript method from formatRate(amountObj, grouped) to formatRate(amountObj, grouped, length, fieldName).");
	}
	amountObj.value = formatNumericValue(amountObj.value, grouped, length, rateMaximumFraction, fieldName);
}

function unformatRate(amountObj)
{
	amountObj.value = convertToDefaultDecimalSymbol(amountObj.value);
}

// Format money input to desired format (money maximum fraction length is explicitly declared)
// formatMoney(HtmlElementObject object, boolean grouped)
function formatMoney(amountObj, grouped, length, fieldName)
{
	if (formatMoney.arguments.length != 4)
	{
		alert("[esmart2u] DEPRECATED. Please change your javascript method from formatMoney(amountObj, grouped) to formatMoney(amountObj, grouped, length, fieldName).");
	}
	amountObj.value = formatNumericValue(amountObj.value, grouped, length, moneyMaximumFraction, fieldName);
}

function unformatMoney(amountObj)
{
	amountObj.value = convertToDefaultDecimalSymbol(amountObj.value);
}

function formatInteger(amountObj, grouped, length, fieldName)
{
	if (formatInteger.arguments.length != 4)
	{
		alert("[esmart2u] DEPRECATED. Please change your javascript method from formatInteger(amountObj, grouped) to formatInteger(amountObj, grouped, length, fieldName).");
	}
	amountObj.value = formatNumericValue(amountObj.value, grouped, length, 0, fieldName);
}

function unformatInteger(amountObj)
{
	amountObj.value = convertToDefaultDecimalSymbol(amountObj.value);
}

function formatPercentage(amountObj, grouped, length, fieldName)
{
	if (formatPercentage.arguments.length != 4)
	{
		alert("[esmart2u] DEPRECATED. Please change your javascript method from formatPercentage(amountObj, grouped) to formatPercentage(amountObj, grouped, length, fieldName).");
	}
	amountObj.value = formatNumericValue(amountObj.value, grouped, length, percentageMaximumFraction, fieldName);
}

function unformatPercentage(amountObj)
{
	amountObj.value = convertToDefaultDecimalSymbol(amountObj.value);
}

// Format numeric input to desired format
// formatNumeric(HtmlElementObject object, boolean grouped, int maximumFractionCount)
function formatNumericValue(amount, grouped, length, maximumFractionCount, fieldName)
{
	if (formatNumericValue.arguments.length != 5)
	{
		alert("[esmart2u] DEPRECATED. Please change your javascript method from formatNumericValue(amount, grouped, maximumFractionCount) to formatNumericValue(amount, grouped, length, maximumFractionCount, fieldName).");
		maximumFractionCount = length;
		length = null;
	}

	// stop from formatting for invalid number scope
	if (length != null && length != "")
	{
		if (!validateNumericInputLength(amount, length, maximumFractionCount))
		{
			var errMsg = generateValidateMessage(numberTooLarge, [fieldName, generateMaxLimit(length, maximumFractionCount)]);
			alert(errMsg);
			return "";
		}
	}

	// perform the formatting only when input is valid
	amount = removeWhiteSpace(amount);
	if (amount == "")
	{
		return amount;
	}

	amount = removeGroupingSeparator(amount);
	if (isNaN(amount))
	{
		return amount;
	}

	var number = "";
	var absoluteNumber = "";
	var decimal = "";
	var decimalSeperatorPosition = -1;
	var formattedNumber = "";

	// extract number, decimal, absolute number
	decimalSeperatorPosition = amount.indexOf(monetaryDecimalSeparator);
	if (decimalSeperatorPosition > -1)
	{
		number = amount.substring(0, decimalSeperatorPosition);
		decimal = amount.substring(decimalSeperatorPosition + 1, amount.length);
	}
	else
	{
		number = amount;
	}

	// formatting number
	number = parseIntOnly(number);
	absoluteNumber = removeSign(number);
	for (; grouped && absoluteNumber.length > 3;)
	{
		var temp = number.substring((number.length - 3), number.length);
		formattedNumber = groupingSeparator + temp + formattedNumber;
		number = number.substring(0, (number.length - 3));
		absoluteNumber = absoluteNumber.substring(0, (absoluteNumber.length - 3));
	}
	if (number.length > 0)
	{
		formattedNumber = number + formattedNumber;
	}

	// formatting decimal
	if (decimal.length > maximumFractionCount)
	{
		decimal = decimal.substring(0, maximumFractionCount);
	}
	else
	{
		for (; decimal.length < maximumFractionCount;)
		{
			decimal += "0";
		}
	}

	// combine formatted number and formatted decimal
	if (maximumFractionCount > 0)
	{
		formattedNumber += monetaryDecimalSeparator + decimal;
	}

	return formattedNumber;
}

// Convert decimal symbol to default symbol, that is a dot (.)
// convertToDefaultDecimalSymbol(String number)
function convertToDefaultDecimalSymbol(number)
{
	var convertedNumber = "";

	// perform only grouping separator removal if decimal symbol is already the default symbol
	if (monetaryDecimalSeparator == DEFAULT_MONETARY_DECIMAL_SYMBOL)
	{
		number = removeGroupingSeparator(number);
		return number;
	}
	else
	{
		// first remove the grouping separator
		number = removeGroupingSeparator(number);

		// then replace monetary decimal separator with dot (.)
		for (var i = 0; i < number.length; i++)
		{
			var temp = number.charAt(i);

			if (temp == monetaryDecimalSeparator)
			{
				convertedNumber += DEFAULT_MONETARY_DECIMAL_SYMBOL;
			}
			else
			{
				convertedNumber += temp;
			}
		}
	}

	return convertedNumber;
}

// Validate size of numeric field(s)
// validateNumericSize(HtmlElementObject form, Array array)
function validateNumericSize(form, array)
{
	var fieldName = "";
	var fieldDescription = "";
	var errorMsgCollection = "";
	var errorMsg = "";
	var length = "";
	var precision = "";
	var object = "";

	for (var count = 0; count < array.length; count++)
	{
		fieldName = array[count][0];
		fieldDescription = array[count][1];
		length = array[count][2];
		precision = array[count][3]
		errorMsg = "";

		object = form.elements[fieldName].value;

		if (!validateNumericInputLength(object, length, precision))
		{
			errorMsg = generateValidateMessage(numberTooLarge, [fieldDescription, generateMaxLimit(length, precision)]);
		}
		else if (!validateFractionLength(object, precision))
		{
			errorMsg = generateValidateMessage(fractionTooLarge, [fieldDescription, precision]);
		}

		if (errorMsg != "")
		{
			if (errorMsgCollection != "")
			{
				errorMsgCollection += "\n";
			}

			errorMsgCollection += errorMsg;
		}
	}

	return errorMsgCollection;
}

// Validate length/size of numeric input based on given length and precision provided
// validateNumericInputLength(String amount, int length, int precision)
function validateNumericInputLength(amount, length, precision)
{
	var number;

	amount = removeGroupingSeparator(amount);
	amount = removeSign(amount);

	if (amount.indexOf(monetaryDecimalSeparator) > 0)
	{
		number = amount.substring(0, amount.indexOf(monetaryDecimalSeparator));
	}
	else
	{
		number = amount;
	}

	if (number.length > (length - precision))
	{
		return false;
	}

	return true;
}

// Generate a max limit sample based on indicated length and precision provided
// generateMaxLimit(int length, int precision)
function generateMaxLimit(length, precision)
{
	var maxLimit = "";

	for (var i = 1; i <= length - precision; i++)
	{
		maxLimit = "9" + maxLimit;
		if (i % 3 == 0 && (i < length - precision))
		{
			maxLimit = groupingSeparator + maxLimit;
		}
	}

	// append a monetary decimal separator
	if (maxLimit == "")
	{
		maxLimit = "0";
	}
	maxLimit += monetaryDecimalSeparator;

	for (var i = 1; i <= precision; i++)
	{
		maxLimit += "9";
	}

	return maxLimit;
}

// Validate length/size of numeric input's fraction based on given length and precision provided
// validateFractionLength(String amount, int length, int precision)
function validateFractionLength(amount, precision)
{
	var number;

	if (amount.indexOf(monetaryDecimalSeparator) > 0)
	{
		number = amount.substring((amount.indexOf(monetaryDecimalSeparator) + 1), amount.length);

		if (number.length > precision)
		{
			return false;
		}
	}

	return true;
}

// Removes negative sign from a numeric input
function removeSign(input)
{
	if (input.indexOf("-") >= 0)
	{
		input = input.replace("-", "");
	}
	if (input.indexOf("+") >= 0)
	{
		input = input.replace("+", "");
	}

	return input;
}

function parseIntOnly(integer)
{
	var copy = integer;
	var sign = "";
	var result = "";
	var hasValue = false;

	if (copy == "")
	{
		result = "0";
	}
	else
	{
		var isLeadingZero = true;
		for (; copy.length > 0;)
		{
			var currentChar = copy.charAt(0);
			copy = copy.substring(1);

			if (currentChar == "+" || currentChar == "-")
			{
				if (currentChar == "-")
				{
					sign = currentChar;
				}
			}
			else if (currentChar == "0" && isLeadingZero)
			{
			}
			else if (currentChar == "1" || currentChar == "2" || currentChar == "3" || currentChar == "4" ||
				currentChar == "5" || currentChar == "6" || currentChar == "7" || currentChar == "8" ||
				currentChar == "9" || currentChar == "0")
			{
				isLeadingZero = false;
				hasValue = true;
				result += currentChar;
			}
			else
			{
				result = integer;
				break;
			}
		}
	}

	if (!hasValue)
	{
		result = "0";
	}

	if (result != "0")
	{
		result = sign + result;
	}

	return result;
}