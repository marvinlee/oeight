// Dependency:              (none)

var popupWindow;

// to solve the js bug to format result of 1 + 1.0001
if (!Number.prototype.toFixed)
{
	Number.prototype.toFixed = function(decimals)
	{
		// default to two decimal digits
		var decDigits = (isNaN(decimals)) ? 2 : decimals;
		var k = Math.pow(10, decDigits);
		var fixedNum = Math.round(parseFloat(this) * k) / k;
		var sFixedNum = new String(fixedNum);
		var aFixedNum = sFixedNum.split(".");

		var i = (aFixedNum[1]) ? aFixedNum[1].length : 0;
		// append decimal point if needed
		if ((i == 0) && (decDigits)) { sFixedNum += "."; }
		// append zeros if needed
		while (i < decDigits)
		{
			sFixedNum += "0";
			i++;
		}
		return sFixedNum;
	}
}

function openWindow(url)
{
	popupWindow = window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,copyhistory=no,width=600,height=450');
}

function closeWindow()
{
	if (popupWindow != null)
	{
		popupWindow.close();
	}
}