<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/solution-web.tld" prefix="solution-web" %>

<script language="JavaScript">
<!--
var today = "<bean:message key="common.message.today"/>";
var fieldIsRequired="<bean:message key="common.message.mandatoryField"/>";
var fieldTooShort="<bean:message key="common.message.enterCharacters" />";
var invalidDateFormat="<bean:message key="common.message.invalidDateFormat"/>";
var invalidIntFormat="<bean:message key="common.message.mustIntegerValue"/>";
var invalidFormat="<bean:message key="common.message.notRecognizedFormat"/>";
var invalidNumericFormat="<bean:message key="common.message.mustNumericValue"/>";
var dateTooEarly="<bean:message key="common.message.mustAfter"/>";
var dateTooLate="<bean:message key="common.message.mustBefore"/>";
var numberMustSmallerThan="<bean:message key="common.message.mustSmaller"/>";
var numberMustLargerThan="<bean:message key="common.message.mustLarger"/>";
var numberMustSmallerThanOrEqualTo="<bean:message key="common.message.mustSmallerOrEqual"/>";
var numberMustLargerThanOrEqualTo="<bean:message key="common.message.mustLargerOrEqual"/>";
var numberRange="<bean:message key="common.message.mustBetween"/>";
var invalidEmailFormat="<bean:message key="common.message.invalidEmailFormat"/>";
var invalidPostcodeLength="<bean:message key="common.message.invalidPostcodeLength"/>";
var invalidCharacter="<bean:message key="common.message.notSpecialCharacters"/>";
var invalidRomanAlphabet="<bean:message key="common.message.invalidRomanAlphabet"/>";
var requestSubmittedMessage="<bean:message key="common.message.requestSubmitted"/>";
var fractionTooLarge="<bean:message key="common.message.fractionTooLarge"/>";
var monetaryDecimalSeparator = "<solution-web:numericSymbol type="monetaryDecimalSeparator"/>";

// deprecated
var numberTooSmall="<bean:message key="common.message.mustNotSmaller"/>";
var numberTooLarge="<bean:message key="common.message.mustNotLarger"/>";
-->
</script>