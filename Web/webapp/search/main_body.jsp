<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.SearchForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.member.web.struts.helper.StateComboHelper,
                 com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper,
                 com.esmart2u.oeight.data.UserCountry"%>   

<% 
    SearchForm searchForm = (SearchForm)request.getAttribute("SearchForm"); 
    List resultList = null;
    StringBuffer pagingOutput = null;
    if (searchForm.getListPage() != null){
        resultList = searchForm.getListPage().getThisPageElements();
    }
    HashMap genderMap = new HashMap();
    genderMap.put(PropertyConstants.GENDER_MALE, OEightConstants.GENDER_MALE_DISPLAY);
    genderMap.put(PropertyConstants.GENDER_FEMALE, OEightConstants.GENDER_FEMALE_DISPLAY); 
   
%>
<div id="searchResults" class="divBox">  
    
    <table border="0" cellpadding="0" width="100%">
        <tr class=inputlabel> 
            <td class="hdr_1" colspan="3" align="left"><h1>Search Results</h1>
            </td>
        </tr>  
        
        
        <%
        if (resultList != null && !resultList.isEmpty())
        {
        System.out.println("Result size" + resultList.size()); 
        %>
         <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>   
        <tr class=inputlabel>
             <%
                int currentPageNumber = searchForm.getListPage().getPageNumber();
                int lastPageNumber = searchForm.getListPage().getLastPageNumber(); 
                pagingOutput = new StringBuffer();
                if (currentPageNumber+1 > 2 ){
                    pagingOutput.append("<a href='javascript:browsePage(" + 0 + ")'> First </a>&nbsp;");
                }
                if (searchForm.getListPage().hasPreviousPage())
                {
                    pagingOutput.append("<a href='javascript:browsePage(" + (currentPageNumber -1) + ")'>"+ (currentPageNumber) + "</a>&nbsp;");
                }
                pagingOutput.append((currentPageNumber +1) + "&nbsp;");
                if (searchForm.getListPage().hasNextPage())
                {
                    pagingOutput.append("<a href='javascript:browsePage(" + (currentPageNumber +1) + ")'>"+ (currentPageNumber+2) + "</a>&nbsp;");
                }
                if (currentPageNumber+1 < lastPageNumber){
                    pagingOutput.append("<a href='javascript:browsePage(" + (lastPageNumber) + ")'> Last </a>&nbsp;");
                }
                pagingOutput.append("&nbsp; of " + (lastPageNumber + 1));
                %>
                <td class="formbuttonsCell" colSpan=3> <%=pagingOutput.toString()%></td>
        </tr>
        <tr class=inputlabel>
            <td width="10%" align=right>&nbsp;</td>
            <td width="80%" align=left>
                    <table> 
                    <%
                    for(int i=0;i<resultList.size();i++)
                    {
                    UserCountry userCountry = (UserCountry)resultList.get(i);
                    %>
                    <tr>
                        <td colspan="3">&nbsp;
                        </td>
                    </tr>  
                    <tr class=inputlabel>
                        <td class="lbl" align="center" rowspan="5"> 
                            <%=userCountry.getUser().getUserName()%><br>
                            <a href='http://profile.080808.com.my/<%=userCountry.getUser().getUserName()%>'><img src="/vphotos/<%=userCountry.getPhotoLargePath()%>" width="120px"> </a>
                        </td> 
                    </tr>
                    <tr>
                        <td class="inputlabel" align="right">User :</td>
                        <td class="inputvalue" align="left"><%=userCountry.getName()%> , <%=genderMap.get(userCountry.getGender())%>, <%=DateObjectHelper.getAge(userCountry.getBirthDate().getTime())%></td>
                    </tr>
                    <tr> 
                        <td class="inputlabel" align="right">Email:</td>
                        <td class="inputvalue" align="left">
                            <% String publicEmail = userCountry.getPublicEmail();
                            if (publicEmail == null || "null".equals(publicEmail))
                            {
                            out.println("-");
                            } 
                            %>
                        </td>
                    </tr>
                    <tr> 
                        <td class="inputlabel" align="right">Location:</td>
                        <td class="inputvalue" align="left"><%=userCountry.getCity()%>, <%=StateComboHelper.getStateLabelByCode(userCountry.getState())%>, <%=CountryComboHelper.getCountryLabelByCode(userCountry.getNationality())%></td>     
                    </tr>
                    <tr>
                        <td class="inputlabel" align="right">Date Joined:</td>
                        <td class="inputvalue" align="left">
                            <%
                            Date registeredDate = userCountry.getSystemRegistered();
                            String formattedDate = DateObjectHelper.getPrintedDate(registeredDate);
                            out.print(formattedDate);
                            %>
                        </td> 
                    </tr> 
                    
                    
                    
                    
                    <%--table>
                        <COL width="40%"> 
                        <COL width="60%">
                        <tr>
                            <td align="right">User :</td>
                            <td align="left"><%=userCountry.getName()%> , <%=genderMap.get(userCountry.getGender())%>, <%=DateObjectHelper.getAge(userCountry.getBirthDate().getTime())%></td>
                        </tr>  
                        <tr>
                            <td align="right">Email:</td>
                            <td align="left">
                                <% String publicEmail = userCountry.getPublicEmail();
                                if (publicEmail == null || "null".equals(publicEmail))
                                {
                                    out.println("-");
                                } 
                            %>
                            </td>
                        </tr> 
                        <tr>
                            <td align="right">Location:</td>
                            <td align="left"><%=userCountry.getCity()%>, <%=StateComboHelper.getStateLabelByCode(userCountry.getState())%>, <%=CountryComboHelper.getCountryLabelByCode(userCountry.getNationality())%></td>
                        </tr>
                        <tr>
                            <td align="right">Date Joined:</td>
                            <td align="left">
                                <%
                                    Date registeredDate = userCountry.getSystemRegistered();
                                    String formattedDate = DateObjectHelper.getPrintedDate(registeredDate);
                                    out.print(formattedDate);
                                %>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr--%>
          
                    <%
                    }
                    %> 
                </table>
                </td> 
              <td width="10%">&nbsp;</td>
            </tr>
        <tr class=inputlabel> 
            <td class="formbuttonsCell" colSpan=3><%=pagingOutput.toString()%></td>
        </tr>  
        <%        
        }
        else
        {
        %>   
        
        <tr>
            <td class="hdr_1" colspan="3" align="left">No Results
            </td>
        </tr>  
        <%
        } 
        %>  
        
    </table>
</div>