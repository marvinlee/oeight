<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>  
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.SearchForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.member.web.struts.helper.StateComboHelper,
                 com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper"%>   
 
<% 
    SearchForm searchForm = (SearchForm)request.getAttribute("SearchForm");
     
    if ((searchForm.getGender() != PropertyConstants.GENDER_MALE) && 
            (searchForm.getGender() != PropertyConstants.GENDER_FEMALE))
        searchForm.setGender('B');
    if (!StringUtils.hasValue(searchForm.getNationality()))
    {
        searchForm.setNationality(PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY));
    } 
    Vector countryList = CountryComboHelper.getCountryList();
    Vector stateList = StateComboHelper.getStateList();
%>

    <html:form name="SearchForm" type="com.esmart2u.oeight.member.web.struts.controller.SearchForm" method="get" scope="request"  action="/search.do" isRelative="true">
       <div id="searchFormLayer" class="divBox">  
           <table>
            <tr>
                <td class="hdr_1" colspan="3" align="left"><h1>Search Members</h1>
                </td>
            </tr>  
            <tr>
                <td width="10%" rowspan="16" >&nbsp;</td>
                <td class="inputlabel"  align="right">Name :&nbsp; 
                </td>
                <td align="left" class="inputvalue">
                    <html:text name="SearchForm"  styleClass="inputvalue" property="name" size="30" maxlength="50"/>    
                </td>
            </tr>
            <tr>
                <td class="inputlabel" align="right"><nobr>Public Email :&nbsp;</nobr>
                </td>
                <td align="left">
                    <html:text name="SearchForm" styleClass="inputvalue" property="publicEmail" maxlength="200" size="30" />  
                </td>
            </tr>
            <tr>
                <td class="inputlabel"  align="right">Gender :&nbsp;
                </td>
                <td align="left" class="inputvalue" > 
                    <html:radio name="SearchForm" property="gender" value="M" /> Male
                    <html:radio name="SearchForm" property="gender" value="F" /> Female 
                    <html:radio name="SearchForm" property="gender" value="B" /> Any 
                </td>
            </tr>
            <tr>
                <td class="inputlabel" align="right"><nobr>Age :&nbsp;</nobr>
                </td>
                <td align="left" class="inputvalue" > 
                        <nobr>
                        From : 
                        <html:select name="SearchForm"  styleClass="inputvalue" property="fromAge">
                            <option value="0"></option>
                            <%
                            if (searchForm.getFromAge() == 0)
                            {
                                searchForm.setFromAge(14);
                            }
                            for(int i=14;i<=80;i++)
                            {   
                                out.print("<option value='" + i +"'"); 
                                if (i == searchForm.getFromAge())
                                    out.print(" selected ");
                                out.print(">");
                                out.println(i + "</option>");
                            }
                            %>
                        </html:select>  
                        To : 
                        <html:select name="SearchForm"  styleClass="inputvalue" property="toAge">
                            <option value="0"></option>
                            <%
                            if (searchForm.getToAge() == 0)
                            {
                                searchForm.setToAge(80);
                            }
                            for(int i=16;i<=80;i++)
                            {   
                                out.print("<option value='" + i +"'"); 
                                if (i == searchForm.getToAge())
                                    out.print(" selected ");
                                out.print(">");
                                out.println(i + "</option>");
                            }
                            %>
                        </html:select>   
                        </nobr>
                </td>
            </tr> 
            <tr>
                <td class="inputlabel" align="right">City :&nbsp;
                </td>
                <td align="left">
                    <html:text name="SearchForm"  styleClass="inputvalue" property="city" size="30" maxlength="30"/>   
                </td>
            </tr>
            <tr>
                <td class="inputlabel" align="right">State :&nbsp;
                </td>
                <td align="left">
                     <html:select name="SearchForm"  styleClass="inputvalue" property="state">
                        <option value="NA">Please Select</option>
                        <%
                        for(int i=0;i<stateList.size();i++)
                        {  
                            String[] state = (String[])stateList.get(i);
                            out.print("<option value='" + state[0] +"'"); 
                            if (state[0].equals(searchForm.getState()))
                                out.print(" selected ");
                            out.print(">");
                            out.println(state[1] + "</option>");
                        }
                        %>
                    </html:select>   
                </td>
            </tr>
            <tr>
                <td class="inputlabel" align="right">Country :&nbsp;
                </td>
                <td align="left">
                     <html:select name="SearchForm"  styleClass="inputvalue" property="nationality">
                        <option value="NA">Please Select</option>
                        <%
                        for(int i=0;i<countryList.size();i++)
                        {  
                            String[] country = (String[])countryList.get(i);
                            out.print("<option value='" + country[0] +"'"); 
                            if (country[0].equals(searchForm.getNationality()))
                                out.print(" selected ");
                            out.print(">");
                            out.println(country[1] + "</option>");
                        }
                        %>
                    </html:select>      
                </td>
            </tr>
            <tr>
                <td class="inputlabel" align="right"><nobr>Current Location :&nbsp;</nobr>
                </td>
                <td align="left">
                    <html:text name="SearchForm"  styleClass="inputvalue" property="currentLocation" size="30" maxlength="100"/>   
                </td>
            </tr>
            <tr>
                <td class="inputlabel" align="center" colspan="2">AND &nbsp;
                </td> 
            </tr>
            <tr>
                <td class="inputlabel" align="right">Lifestyle :&nbsp;
                </td>
                <td align="left">
                    <html:text name="SearchForm"  styleClass="inputvalue" property="lifestyle" size="30" maxlength="100"/>   
                </td>
            </tr>
            <tr>
                <td class="inputlabel" align="center" colspan="2">AND &nbsp;
                </td> 
            </tr>
            <tr>
                <td class="inputlabel" align="right">Interest :&nbsp;
                </td>
                <td align="left">
                    <html:text name="SearchForm"  styleClass="inputvalue" property="interest" size="30" maxlength="100"/>   
                </td>
            </tr>
            <tr>
                <td class="inputlabel" align="center" colspan="2">AND &nbsp;
                </td> 
            </tr>
            <tr>
                <td class="inputlabel" align="right">Skills :&nbsp;
                </td>
                <td align="left">
                    <html:text name="SearchForm"  styleClass="inputvalue" property="skills" size="30" maxlength="100"/>   
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr> 
                <td align="center" colspan="2" class="formbuttonsCell" > 
                    <input type="button" name="search" class="formbuttons" value="Search" onclick="browsePage(0);">
                </td>
            </tr>
            
        </table>
        <input type="hidden" name="currentPage" value="<%=searchForm.getCurrentPage()%>">
        <input type="hidden" name="act" value="searchResult">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>"> 
       </div>  
    </html:form> 