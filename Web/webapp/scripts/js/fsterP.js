function xGetElementById(e)
{
	if(typeof(e)=="string"){
		if(document.getElementById) e=document.getElementById(e);
		else if(document.all) e=document.all[e];
		else e=null;
	}
	return e;
}
function resizeFsterPicImg(image, index)
{
	var max = 100;
	//if (!fsterPLoad[index]){ 
		imgObj = xGetElementById(image);
		var ratio = imgObj.width / imgObj.height;
		if (imgObj.width > max)
		{
			imgObj.width = max;
			imgObj.height = max / ratio;
		}
		if(imgObj.height > max)
		{
			imgObj.height = max;
			imgObj.width = imgObj.height * ratio;
		}
		fsterPLoad[index] = true;
	//}
}
function allFsterPicLoaded()
{ 
	var counter = -1;
	var loaded = 0;
	while(counter < fsterPLoad.length)
	{
		counter++;
		if (fsterPLoad[counter]) loaded++;
	} 
	if (loaded == fsterPLoad.length){
		return true;
	}
	else
	{
		return false;
	} 
}