function xGetElementById(e)
{
	if(typeof(e)=="string"){
		if(document.getElementById) e=document.getElementById(e);
		else if(document.all) e=document.all[e];
		else e=null;
	}
	return e;
}
function resizeFsterFrenImg(image, index)
{
	var max = 100;
	//if (!fsterFLoad[index]){ 
		imgObj = xGetElementById(image);
		var ratio = imgObj.width / imgObj.height;
		if (imgObj.width > max)
		{
			imgObj.width = max;
			imgObj.height = max / ratio;
		}
		if(imgObj.height > max)
		{
			imgObj.height = max;
			imgObj.width = imgObj.height * ratio;
		}
		fsterFLoad[index] = true;
	//}
}
function allFsterFrenLoaded()
{ 
	var counter = -1;
	var loaded = 0;
	while(counter < fsterFLoad.length)
	{
		counter++;
		if (fsterFLoad[counter]) loaded++;
	} 
	if (loaded == fsterFLoad.length){
		return true;
	}
	else
	{
		return false;
	} 
}