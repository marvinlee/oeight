<%@page pageEncoding="UTF-8"%>   
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%>
<%@ page import="com.esmart2u.oeight.member.helper.OEightConstants"%>
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ScriptsForm"%>
<%
    ScriptsForm scriptsForm = (ScriptsForm)request.getAttribute("ScriptsForm");
    String widgetHTML = scriptsForm.getWidgetMainHTML();
    out.print("document.write('" + widgetHTML + "')");
    
    // Unable to set referral, so we set to session 
    // ! Not good for server resource
    if (StringUtils.hasValue(scriptsForm.getSessionValue()))
    { 
        System.out.println(">>> Setting session value:" + scriptsForm.getSessionValue());
        String fromKey = OEightConstants.SESSION_CAMPAIGN_INVITE_FROM + OEightConstants.CAMPAIGN_INVITE_FROM_FRIENDSTER;
        scriptsForm.getActionContext().getRequest().getSession().setAttribute(fromKey, scriptsForm.getSessionValue());
    }
%>