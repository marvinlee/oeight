<html>
<head>
<title>080808.com.my</title>
<link rel="stylesheet" href="error_style.css">
</head>
<body bgcolor="#ffffff">
<center>
<table><tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
</table>
   <table width="90%" border="0" cellspacing="0" cellpadding="0">
     <tr>
       <td>
         <table width="100%" border="0" cellspacing="0">
           <tr>
             <td height="30" class="dark_header">OOPS! SERVER UNABLE TO PROCESS REQUEST</td>
           </tr>
           <tr>
             <td colspan="2">
               &nbsp;
	     </td>
           </tr>
           <tr>
             <td height="30" align="right" colspan="2">
               <hr noshade>
             </td>
           </tr>
           <tr><td>&nbsp;</td></tr>
         </table>
       </td>
     </tr>
     <tr>
       <td>
         <table width="100%" border="0" cellspacing="0" align="center">


                    <tr>
                      <td  colspan="2" height="30" class="dark">The page you are looking for does not seems to exist.
                      </td>
                    </tr>
                  
                    <tr>
                      <td class="dark"><ul>
                      <li>Please check the page URL and try again.</li
                      </ul></td>
                    </tr>
          </tr>
        </table>
       </td>
     </tr>
     <tr><td>&nbsp;</td></tr>
     <tr>
       <td height="30" align="right" colspan="2">
          <hr noshade>
       </td>
     </tr>
        <table width="100%" border="0">
            <tr><td height="10%"></td></tr>
            <tr><td height="10%"></td></tr>
            <tr>
                <td align="center">
                <span class="dark_footer">
                 Copyright&nbsp;&copy;&nbsp;2007-2008&nbsp;080808.com.my&nbsp;<br>ALL RIGHTS RESERVED.

                </span>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
   </table>
</center>


</body>
</html>