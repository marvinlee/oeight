 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                com.esmart2u.solution.base.helper.PropertyManager" %>
  
    <td width="15%">&nbsp;</td> 
    <td> 
    <html:form name="LoginForm" type="com.esmart2u.oeight.member.web.struts.controller.LoginForm" method="post" action="/secure/login.do" isRelative="true">
    <br><br><br>
    <div id="resetLayer" class="divBox">   
    <table width="80%">
        <COL width="20%"> 
        <COL width="40%"> 
	<COL width="40%">
            <tr>
                <td class="hdr_1" align="left"><h1>Reset Password</h1>
                </td>
                <td class="hdr_1" colspan="2" align="right">
                </td>
            </tr> 
            <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br> 
                </td>
            </tr> 
            <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                        Trouble logging in?<br>
                        Please provide the email address you use to login here and then click Send. <br>
                        An email with a link to reset your password will be sent to you.
                        <br>
                         
                </td>
            </tr> 
            <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="center"> 
                        <br><br>
                        Email address :&nbsp; 
                        <html:text styleClass="inputvalue" name="LoginForm" property="login" size="20" maxlength="50"/> 
                        <html:errors property="login"/> 
                        <br><br>  
                </td>
            </tr> 
           
            <tr>
                <td align="left">&nbsp;</td>
                <td align="right">  
                    <input class="formbuttons" align="right" type="button" name="send" value="Send" onclick="formSubmit();"> 
                        <br><br>   
                </td>
                <td>&nbsp;
                </td>
            </tr>
            
        </table>   
        </div>
        <input type="hidden" name="act" value="resetSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>  
    </td>
    <td width="15%">&nbsp;</td>                     