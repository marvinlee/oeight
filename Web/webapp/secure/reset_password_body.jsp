 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                com.esmart2u.solution.base.helper.PropertyManager" %>
   
    <td width="15%">&nbsp;</td> 
    <td> 
    <html:form name="LoginForm" type="com.esmart2u.oeight.member.web.struts.controller.LoginForm" method="post" action="/secure/login.do" isRelative="true">
         <br><br><br>
    <div id="resetLayer" class="divBox">    
    <table width="80%">
        <COL width="20%"> 
        <COL width="40%"> 
	<COL width="40%">
            <tr>
                <td class="hdr_1" align="left"><h1>Reset Password</h1>
                </td>
                <td class="hdr_1" colspan="2" align="right">
                </td>
            </tr> 
            <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br> 
                </td>
            </tr> 
            <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                        <html:errors property="resetInvalid"/> <br>
                        Please provide the new password for your login. <br> <br> <br> 
                         
                </td>
            </tr> 
            <tr>
                <td align="left">&nbsp;</td>
                <td align="right" valign="top">
                        Email address :&nbsp; 
                </td>
                <td align="left">  
                        <b><bean:write name="LoginForm" property="login"/></b>
                        <br><br>  
                </td>
            </tr> 
           
            <tr>
                <td align="left">&nbsp;</td>
                <td align="right" valign="top">
                        New Password :&nbsp;
                </td>
                <td align="left">   
                    <html:password styleClass="inputvalue" name="LoginForm" property="password" size="20" maxlength="12"/> 
                    4 to 12 characters
                    <html:errors property="password"/>
                </td>
            </tr>
            <tr>
                <td align="left">&nbsp;</td>
                <td align="right" valign="top">
                        Confirm New Password :&nbsp;
                </td>
                <td align="left">    
                    <html:password styleClass="inputvalue" name="LoginForm" property="confirmPassword" size="20" maxlength="12"/> 
                    4 to 12 characters
                    <html:errors property="confirmPassword"/><html:errors property="confirmPasswordMismatch"/>
                </td>
            </tr>
            <tr class="formbuttonscell">
                <td>&nbsp;
                </td>
                <td colspan="2" align="right" class="formbuttonscell">    
                    <input class="formbuttons" align="right" type="button" name="continue" value="Submit" onclick="formSubmit();"> 
                        <br><br>   
                </td> 
            </tr>
            
        </table></div> 
        <html:hidden name="LoginForm" property="login"/>
        <html:hidden name="LoginForm" property="resetCode"/>
        <input type="hidden" name="act" value="resetPassed">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>  
    </td>
    <td width="15%">&nbsp;</td>    
