<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%> 
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%>  
<%@ page import="com.esmart2u.solution.base.helper.ConfigurationHelper"%>  
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager"%>  
 


<table width="100%">
<tr>  

<td width="15%">&nbsp;</td>

<td valign="top"><br><br><br>

    <%  if ("y".equalsIgnoreCase(request.getParameter("invalid")))
        {
        //<h1 style="color: darkblue">Your session has been ended due to inactivity. Please login again.</h1><br><br>
        %>
        <h1 style="color: darkblue">Please login to continue.</h1><br><br>
    <%
        } 
    %>
    <h1 class="h1_hdr">Login Page</h1> <br><br>

     <%--div id="earthlayer" class="divBox"--%>  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
     
          
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="content_txt">Please login in order to keep track of your friends pledge or submit your blog post.<br><br></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="content_txt">If you do not have one, <a href="/register.do?act=new">join us</a>.<br><br></td>
        </tr>  
  
        <tr>
          <td align="center" colspan="3" class="content_txt">  
             <html:form name="LoginForm" type="com.esmart2u.oeight.member.web.struts.controller.LoginForm" method="post"  action="/secure/login.do" isRelative="true">
            
                <div id="loginlayer" class="divBox"> 
                <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
                    <tr> 
                        <td align="left" class="inputvalue" colspan="2"><img src="/images/login/login_title.gif">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="inputlabel" >
                        Email Address:
                        </td>
                        <td align="left" class="inputvalue" >
                        <!--input type="text" name="login" size="10"/-->
                        <html:text name="LoginForm" styleClass="inputvalue" property="login" size="20" maxlength="50"/>
                             <html:errors property="login"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"  class="inputlabel">
                        Password:
                        </td>
                        <td align="left" >
                        <input type="password" class="inputvalue"  name="password" size="20" maxlength="20" onkeydown="if(event.keyCode==13)formSubmit();" />
                            <html:errors property="password"/>
                            <html:errors property="passwordMismatch"/>
                        </td>
                    </tr> 
                    <tr class="formbuttonsCell" > 
                      <td colspan="2"  class="formbuttonsCell" >
                            <input type="button" class="formbuttons" name="Login" value="Login" onclick="formSubmit();"/>
                        </td> 
                    </tr>
                    <tr>
                        <td align="right" colspan="2"><br></td>
                    </tr> 
                </table>  
                </div>      

                <input type="hidden" name="act" value="earthLogin">    
        
                </html:form>
          </td>
        </tr>   
       
  
        </table>
   
       <%--/div--%> 
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table> 