<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%> 
<%@ page import="java.util.*"%> 
<%@ page import="java.net.URLEncoder"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%>
<%@ page import="com.esmart2u.oeight.member.bo.MembersUpdateBO"%>  
<%@ page import="com.esmart2u.oeight.data.MembersUpdate"%>  
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%> 
<%@ page import="com.esmart2u.solution.base.helper.ConfigurationHelper"%>  
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.MosaicHelper"%>
<%@ page import="com.esmart2u.oeight.data.Mosaic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager"%>  
 
<%
    String query = request.getParameter("q");
    String output = "";
     //URL encode it
        try{
            output = URLEncoder.encode(query.trim());
        }catch (Exception e)
        {
            System.out.println("Error encoding");
        }
        StringBuffer appended = new StringBuffer();
        char doublequotes = 34;
        char space = 43;
        if (output!=null && !"".equals(output)){
            for (int i=0;i<output.length();i++)
            {
                char letter = output.charAt(i);
                if (space == letter)
                {
                    appended.append("%20");
                }
                else if (doublequotes == letter)
                { 
                    appended.append("%22");
                }
                else
                {
                    appended.append(letter);
                }
            }
        }
      String formattedQuery = appended.toString(); 
%>
<%-- This page needs total 5 columns instead of 4--%>
    <td colspan="3">
        <table width="100%">
            <tr  valign="top">   
                <td width="100%" valign="top"> 
                    <h1>Search Powered By <a href="http://www.google.com" target="#">Google</a></h1>
                    <iframe scrolling="no" frameborder="0" src="http://www.google.com.my/search?hl=en&rlz=1G1GGLQ_ENMY248&q=<%=formattedQuery%>&btnG=Search&meta=cr%3DcountryMY" style="border: none; overflow: auto;" width="800" height="1300"></iframe>
                </td>  
            </tr>
        </table>
    </td>                                