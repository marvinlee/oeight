  <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserInvites,
                 com.esmart2u.oeight.member.web.struts.controller.LoginForm"%>   
 
 
    <div id="changeLayer" class="divBox">   
    <table width="80%">
        <COL width="20%"> 
        <COL width="40%"> 
	<COL width="40%">
            <tr>
                <td class="hdr_1" colspan="3" align="left"><h1>Change Password - Success</h1>
                </td>
            </tr>   
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>   
    
            <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                    Your new password has been updated successfully. <br>
                    Please use your new password on your next login.
                    <br>
                    <br>
                </td>
            </tr>     
      
             <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>  
        </table></div>  