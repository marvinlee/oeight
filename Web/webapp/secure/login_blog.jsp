<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.ConfigurationHelper,
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants, 
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.member.bo.FeaturedBO,
                 com.esmart2u.oeight.data.FeaturedBlog"%>    
<% 
            // Get today's blog listing
            List blogList = FeaturedBO.getInstance().getFeaturedBlogs();

   
%>
    <table>
            <tr>
                <td class="hdr_1" align="center"><h1><a href="/podium.do?act=blog">Featured Blog</a></h1>
                </td>
            </tr>   
            <tr>
                <td>Add your blog here! Select "Feature My Blog!" at Profile - Contacts - My Blog
                </td>
            </tr>    
            <tr>
                <td>&nbsp;
                </td>
            </tr>  
            
        <%
            if (blogList != null && !blogList.isEmpty())
            {  
                int startIndex = 0;
                int showCount = 4;
                long random =0;
                int count = 0;
                if (blogList.size() > showCount)
                {
                    random = Math.round(Math.random()*(blogList.size()-1)); 
                    startIndex = Integer.parseInt(""+random);
                    
                } 
                
                //for(int i=0;i<blogList.size();i++)
                while (count++<showCount)
                {
                    //FeaturedBlog featuredBlog = (FeaturedBlog)blogList.get(i);
                    FeaturedBlog featuredBlog = (FeaturedBlog)blogList.get(startIndex);                    
                    startIndex++;
                    if (startIndex >= blogList.size())
                    {
                        startIndex = 0;
                    }
                    
        %>
            <tr>
                <td width="200" align="left"><a href="<%=featuredBlog.getBlogLink()%>" target="#"><img src="<%=featuredBlog.getBlogPhotoPath()%>" align="center" width="180" height="100"></a><br/><a href="<%=featuredBlog.getBlogLink()%>" target="#"><%=featuredBlog.getBlogName()%></a>
                    <%
                    if (featuredBlog.getUserId()!= PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY))
                    {
                     %>
                     &nbsp;by<a href="http://profile.<%=ConfigurationHelper.getDomainName()%>/<%=featuredBlog.getUser().getUserName()%>" <%=featuredBlog.getUser().getUserName()%></a>
                      
                     <%
                    }
                    %><br><%=featuredBlog.getBlogDescription()%>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr> 
          
        <%
                } 
          }
          else
          {
        %>   
        
            <tr>
                <td class="hdr_1" align="left">No Blogs
                </td>
            </tr>  
        <%
          } 
        %>   
        </table>            