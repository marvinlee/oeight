 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserInvites,
                 com.esmart2u.oeight.member.web.struts.controller.LoginForm"%>   
 

    <td width="15%">&nbsp;</td> 
    <td> 
    <html:form name="LoginForm" type="com.esmart2u.oeight.member.web.struts.controller.LoginForm" method="post" action="/secure/login.do" isRelative="true">
         <br><br><br>
    <div id="resetLayer" class="divBox">   
    <table width="80%">
        <COL width="20%"> 
        <COL width="40%"> 
	<COL width="40%">
            <tr>
                <td class="hdr_1" colspan="3" align="left"><h1>Reset Password - Success</h1>
                </td>
            </tr>   
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>   
    
            <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                    Your new password has been updated successfully. <br>
                    Please continue to login with the new password.
                    <br>
                    <br>
                </td>
            </tr>     
      
            <tr>
                <td>&nbsp;
                </td>
                <td align="center">    
                    <input class="formbuttons" type="button" name="continue" value="Continue" onclick="formSubmit();">  
                </td>
                <td>&nbsp;
                </td>
            </tr> 
        </table></div>
        <input type="hidden" name="act" value="form">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>  
    </td>
    <td width="15%">&nbsp;</td>    