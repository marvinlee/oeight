 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                com.esmart2u.solution.base.helper.PropertyManager" %>
    
    <html:form name="LoginForm" type="com.esmart2u.oeight.member.web.struts.controller.LoginForm" method="post" action="/secure/login.do" isRelative="true">
       
    <div id="changeLayer" class="divBox">    
    <table width="80%">
        <COL width="20%"> 
        <COL width="40%"> 
	<COL width="40%">
            <tr>
                <td class="hdr_1" align="left" colspan="3" ><h1>Change Password</h1>
                </td> 
            </tr> 
            <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br> 
                </td>
            </tr>  
            <tr>
                <td align="left">&nbsp;</td>
                <td align="right" valign="top" class="inputlabel">
                        Current Password :&nbsp; 
                </td>
                <td align="left">  
                    <html:password styleClass="inputvalue" name="LoginForm" property="password" size="20" maxlength="12"/> 
                    <html:errors property="existingPasswordMismatch"/>
                </td>
            </tr> 
           
            <tr>
                <td align="left">&nbsp;</td>
                <td align="right" valign="top" class="inputlabel">
                        New Password :&nbsp;
                </td>
                <td align="left">   
                    <html:password styleClass="inputvalue" name="LoginForm" property="newPassword" size="20" maxlength="12"/> 
                    4 to 12 characters
                    <html:errors property="newPassword"/>
                </td>
            </tr>
            <tr>
                <td align="left">&nbsp;</td>
                <td align="right" valign="top" class="inputlabel">
                        Confirm New Password :&nbsp;
                </td>
                <td align="left">    
                    <html:password styleClass="inputvalue" name="LoginForm" property="confirmPassword" size="20" maxlength="12"/> 
                    4 to 12 characters
                    <html:errors property="confirmPassword"/><html:errors property="confirmPasswordMismatch"/>
                </td>
            </tr>
            <tr class="formbuttonscell">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right" class="formbuttonscell">    
                    <input class="formbuttons" align="right" type="button" name="continue" value="Submit" onclick="formSubmit();"> 
                        <br><br>   
                </td> 
            </tr>
            
        </table></div>  
        <input type="hidden" name="act" value="changeSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>   
