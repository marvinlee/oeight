<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%>
<%@ page import="com.esmart2u.solution.base.helper.ConfigurationHelper"%> 
 
<%-- This page needs total 5 columns instead of 4--%>
    <td colspan="3">
        <table width="100%">
            <tr>  
    
    <td width="5%">&nbsp;</td>

    <td width="20%" valign="top">

    <h1>Login Page</h1>
    
     <% 
        
        // Show login and ads only for those not logged-on
        boolean notLoggedIn = (session==null || !StringUtils.hasValue((String)session.getAttribute("userId")));
        if (notLoggedIn)
        { 
    %>
            <%--p>Please login in order to post in 080808 Forum.</p><br><br>
            <p>If you do not have one, <a href="/register.do?act=invited">join here</a>.</p><br><br--%>
            
        <html:form name="LoginForm" type="com.esmart2u.oeight.member.web.struts.controller.LoginForm" method="post"  action="/secure/login.do" isRelative="true">
            
        <div id="loginlayer" class="divBox"> 
        <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
            <tr> 
                <td align="left" class="inputvalue" colspan="2"><img src="/images/login/login_title.gif">
                </td>
            </tr>
            <tr>
                <td align="right" class="inputlabel" >
                Email Address:
                </td>
                <td align="left" class="inputvalue" >
                <!--input type="text" name="login" size="10"/-->
                <html:text name="LoginForm" styleClass="inputvalue" property="login" size="20" maxlength="50"/>
                     <html:errors property="login"/>
                </td>
            </tr>
            <tr>
                <td align="right"  class="inputlabel">
                Password:
                </td>
                <td align="left" >
                <input type="password" class="inputvalue"  name="password" size="20" maxlength="20" onkeydown="if(event.keyCode==13)formSubmit();" />
                    <html:errors property="password"/>
                    <html:errors property="passwordMismatch"/>
                </td>
            </tr> 
            <tr class="formbuttonsCell" > 
              <td colspan="2"  class="formbuttonsCell" >
                    <input type="button" class="formbuttons" name="Login" value="Login" onclick="formSubmit();"/>
                </td> 
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <a href="/register.do?act=new">Join</a> | 
                    <a href="/secure/login.do?act=reset">Login Help</a> </td>
            </tr> 
        </table>  
        </div>      
            
        <input type="hidden" name="act" value="forumLogin">
            
        </html:form> 
        <%
        }// Ends hide for logged on user
        %> 
       <br><br> 
       <div id="adlayer1" class="divBox">            
         <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="280" height="280">
  <param name="movie" value="/ads/280-280.swf">
  <param name="quality" value="high">
  <embed src="/ads/280-280.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="160" height="160"></embed>
</object>  
        </div>
        
  
                </td>
                <td width="5%" valign="top"></td> 
                <td width="60%" align="left">
                <br><br><br>
                <div id="forumSummaryLayer" class="divBox" style="width:480px;height:580px">     
                    <table>
                        <tr>
                            <td colspan="3" class="tourButtonsCell">
                                &nbsp;
                            </td>
                        </tr>
                        <tr> 
                            <td width="15%" valign="bottom"></td> 
                            <td width="292">
                                <p><strong>What's Hot in 080808 Forum? </strong></p>
                                <center>
                                <table>
                                    <tr>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=3&desc=events">Events<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/events.gif"></a></center></td> 
                                        <td width="30%"><br></td>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=4&desc=great%20deals">Great Deals<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/sales.gif"></a></center></td> 
                                    </tr>
                                    <tr><td colspan="3"><br></td></tr>
                                    <tr>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=5&desc=news">News<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/news.gif"></a></center></td> 
                                        <td><br></td>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=6&desc=gossips">Gossips<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/gossip.gif"></a></center></td> 
                                    </tr>
                                    <tr><td colspan="3"><br></td></tr>
                                    <tr>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=7&desc=entertainment">Entertainment<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/movies.gif"></a></center></td> 
                                        <td><br></td>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=8&desc=automotive">Automotive<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/automtv.gif"></a></center></td> 
                                    </tr>
                                    <tr><td colspan="3"><br></td></tr>
                                    <tr>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=9&desc=sports">Sports<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/sports.gif"></a></center></td> 
                                        <td><br></td>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=10&desc=money%20matters">Money Matters<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/money.gif"></a></center></td> 
                                    </tr>
                                    <tr><td colspan="3"><br></td></tr>
                                    <tr>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=11&desc=classifieds">Classifieds<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/classifieds.gif"></a></center></td> 
                                        <td><br></td>
                                        <td><center><a href="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/viewforum.php?f=12&desc=food">Food<br><img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/food.gif"></a></center></td> 
                                    </tr>
                                </table>
                                </center>
                                <p></p>
                                <br> 
                            </td>
                            <td width="76" valign="bottom">&nbsp;</td>
                        </tr>

                </table></div> 

                </td> 

            </tr>
        </table>
    </td>                                
 