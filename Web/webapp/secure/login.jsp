<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<tiles:insert page="/tiles/login_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Login" />
   <tiles:put name="header" value="/tiles/top.jsp" />
   <tiles:put name="menu" value="/tiles/left.jsp" />
   <tiles:put name="javascript" value="/secure/login_js.jsp" />
   <tiles:put name="body" value="/secure/login_body.jsp" />
   <tiles:put name="right" value="/secure/login_blog.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>