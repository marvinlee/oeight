 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserInvites,
                 com.esmart2u.oeight.member.web.struts.controller.LoginForm"%>   

<% 
    LoginForm loginForm = (LoginForm)request.getAttribute("LoginForm");  
    String loginEmail = loginForm.getLogin();  
   
%>

    <td width="15%">&nbsp;</td> 
    <td> 
    <html:form name="LoginForm" type="com.esmart2u.oeight.member.web.struts.controller.LoginForm" method="post" action="/secure/login.do" isRelative="true">
      <br><br><br>
    <div id="resetLayer" class="divBox">   
    <table width="80%">
        <COL width="20%"> 
        <COL width="40%"> 
	<COL width="40%">
            <tr>
                <td class="hdr_1" colspan="3" align="left"><h1>Reset Password</h1>
                </td>
            </tr>   
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>   
            
        <%
            if (StringUtils.hasValue(loginEmail))
            { 
                        %>  
            <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                    The reset password link has been sent to your email address at : <b><%=loginEmail%></b>
                    <br>
                    Please check your email and follow the link to reset your password.
                    <br>
                    <br>
                </td>
            </tr>     
          
        <% 
          }
          else
          {
        %>   
        
            <tr>
                <td class="hdr_1" colspan="3" align="center">Email address not found, please use your login email address.
                </td>
            </tr>     
            <tr>
                <td>&nbsp;
                </td>
                <td align="center">    
                    <input class="formbuttons" type="button" name="back" value="Back" onclick="formSubmit();">  
                </td>
                <td>&nbsp;
                </td>
            </tr>
        <%
          } 
        %>   
        </table></div>
        <input type="hidden" name="act" value="reset">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
    </td>
    <td width="15%">&nbsp;</td>    