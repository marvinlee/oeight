<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.EarthForm,
                 com.esmart2u.oeight.member.web.struts.helper.OEightPledgeHelper"%>   

 <% 
    EarthForm earthForm = (EarthForm)request.getAttribute("EarthForm");  
%>
<%-- This page needs total 3 columns instead of 4--%> 
     <table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://earth.080808.com.my?act=contestBlogForm';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

                <h1 class="h1_hdr">Submit Climate Change Blog Post</h1> <br><br> 

    <html:form name="EarthForm" type="com.esmart2u.oeight.member.web.struts.controller.EarthForm" method="post" scope="request"  action="/earth.do" isRelative="true">
    <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" width="10%">&nbsp;</td>
          <td align="center" width="20%">&nbsp;</td>
          <td align="center" width="70%">&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="content_txt" >Please provide your blog post information:</td>
        </tr>
        
         
             <tr>
                <td class="inputlabel" align="right"> 
                </td>
                <td align="left">
                     <br>
                </td>
            </tr>   
           
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Your Blog Post :&nbsp;
                </td>
                <td align="left">
                    <html:text name="EarthForm"  styleClass="inputvalue" property="blogUrl" maxlength="300" size="80" />   
                    <html:errors property="blogUrl"/> 
                </td>
            </tr>
           
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Your Name :&nbsp;
                </td>
                <td align="left">
                    <html:text name="EarthForm"  styleClass="inputvalue" property="name" maxlength="100" size="50" />   
                    <html:errors property="name"/> 
                </td>
            </tr>
           
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Contact Number :&nbsp;
                </td>
                <td align="left">
                    <html:text name="EarthForm"  styleClass="inputvalue" property="contactNo" maxlength="20" size="20" />   
                    <html:errors property="contactNo"/> 
                </td>
            </tr>
           
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Email :&nbsp;
                </td>
                <td align="left">
                    <html:text name="EarthForm"  styleClass="inputvalue" property="email" maxlength="100" size="50" />   
                    <html:errors property="email"/> 
                </td>
            </tr>
           
            
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr>  
                <td align="center" colspan="3"> 
                    <center><input type="button" name="next" class="formbuttons" value="Submit" onclick="formSubmit();"></center>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div>
        <input type="hidden" name="act" value="contestBlogSubmit">
    </html:form> 
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table>   