<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.EarthForm,
                 com.esmart2u.oeight.member.web.struts.helper.OEightPledgeHelper"%>   

 <% 
    EarthForm earthForm = (EarthForm)request.getAttribute("EarthForm"); 
    
    Vector pledgeList = OEightPledgeHelper.getPledgeList(); 
    System.out.println("pledgeList:" + pledgeList);
    System.out.println("pledgeList sz:" + pledgeList.size());
    
%>
<%-- This page needs total 3 columns instead of 4--%> 
     <table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://earth.080808.com.my?act=pledgeMake';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

                <h1 class="h1_hdr">Pledge for Climate Change</h1> <br><br> 

    <html:form name="EarthForm" type="com.esmart2u.oeight.member.web.struts.controller.EarthForm" method="post" scope="request"  action="/earth.do" isRelative="true">
    <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" width="10%">&nbsp;</td>
          <td align="center" width="20%">&nbsp;</td>
          <td align="center" width="70%">&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="content_txt" >Please select a pledge that suits you best :</td>
        </tr>
        <tr>
          <td align="center" colspan="3" >
                    <html:errors property="pledgeCode"/>&nbsp;</td>
        </tr>
         
             <tr>
                <td class="inputlabel" align="right"> 
                </td>
                <td align="left">
                     <br>
                </td>
            </tr>   
            <%
            String pledgeCategoryCurrent = "";
            for(int i=0;i<pledgeList.size();i++)
            {  
                String[] pledge = (String[])pledgeList.get(i); 
                String pledgeCode = pledge[0];
                if (!StringUtils.hasValue(pledgeCode)) continue;
                String pledgeCategory = pledgeCode.substring(0,pledgeCode.indexOf("."));
                String pledgeNumber = pledgeCode.substring(pledgeCode.indexOf(".")+1,pledgeCode.length());
                String checkedString = "";
                if (pledgeNumber.equals(""+earthForm.getPledgeCode()))
                {
                    checkedString = "checked";
                }
            %>
             <tr>
                 <td>&nbsp;</td>
                <td class="inputlabel" align="right">
                    &nbsp;
                    <%
                        if (!pledgeCategory.equals(pledgeCategoryCurrent))
                        {
                            %><br><br></td><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td>
                            <td class="inputlabel" align="right">
                                <%--img src="http://080808.org.my/earth08/img/<%=pledgeCategory%>_m.gif"><br>--%>
                                <img src="/images/earth/<%=pledgeCategory%>_m.gif"><br>
                            
                             </td><td class="inputlabel" align="center"><% 
                                    out.print(pledgeCategory.toUpperCase()); 
                                    pledgeCategoryCurrent = pledgeCategory;                                  
                                %></td>
                            </tr>
                             <tr>
                                 <td>&nbsp;</td>
                                 <td> 
                     <%
                        }
                    %>
                </td>
                <td align="left"> 
                    <input type="radio" name="pledgeCode" value="<%=pledgeNumber%>" <%=checkedString%>>  <%=pledge[1]%> 
                </td>
            </tr>
            <%
            }
            %>
             <tr>
                <td colspan="3">&nbsp;<br><br><br></td> 
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">My Name :&nbsp;
                </td>
                <td align="left">
                    <html:text name="EarthForm"  styleClass="inputvalue" property="pledgerName" maxlength="50" size="50" />   
                    <html:errors property="name"/>
                    <html:errors property="pledgeFrom"/>
                    <html:errors property="pledgeFromUserId"/>
                </td>
            </tr>
            
            <%-- If coming to this page from somewhere else, show referral--%>
            <%
            if (earthForm.getPledgeFrom() > 0)
            {
            %> 
            <tr>
                 <td>&nbsp;</td>
                <td class="inputlabel" align="right">&nbsp;
                </td>
                <td align="left">
                    <html:checkbox name="EarthForm"  styleClass="inputvalue" property="pledgeFrom" /> 
                      <%
                        if (earthForm.getPledgeFrom() == 1)
                        {
                        %> 
                            Link to my Friendster Profile
                        <%
                        }else  if (earthForm.getPledgeFrom() == 2)
                        {
                        %> 
                            Link to my Facebook Profile
                        <%
                        }
                        %>
                    <%--html:errors property="pledgerName"/--%>
                </td>
            </tr>
            
            <%
            }
            %>
            
            
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr>  
                <td align="center" colspan="3"> 
                    <center><input type="button" name="pledge" class="formbuttons" value="Pledge" onclick="formSubmit();"></center>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div>
        <input type="hidden" name="act" value="pledgeMakeSubmitted">
        <html:hidden name="EarthForm" property="pledgeFromUserId"/>
        <%--html:hidden name="FeedbackForm" property="tokenValue"/>
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>"--%> 
    </html:form> 
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table>   