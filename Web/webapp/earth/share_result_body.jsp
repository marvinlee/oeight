<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.CampaignInvite,
                 com.esmart2u.solution.base.helper.contactlist.ContactImpl,
                 com.esmart2u.oeight.member.web.struts.controller.EarthForm"%>   

<% 
    EarthForm earthForm = (EarthForm)request.getAttribute("EarthForm"); 
       
    List contactEmailList = (List)earthForm.getContactEmailList(); 

   
    %>
      
<table width="100%">
<tr>  

<td width="15%">&nbsp;</td>

<td valign="top"><br><br><br>

<h1 class="h1_hdr">Share with friends</h1><br><br>    
      <div id="inviteLayer" class="divBox">   
        <table width="400">
            <COL width="20%"> 
            <COL width="40%">
            <COL width="40%"> 
            <tr>
                <td class="lbl" align="center"> 
                    
                </td>
                <td align="left" colspan="2"> 
                    
                </td>
            </tr>   
            
            <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br><br>
                </td>
            </tr>   
         
            <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br><br>
                </td>
            </tr>    
            <tr>
                <td class="content_txt" colspan="3" align="left"><b>Invites to pledge will be sent to:</b> <br><br> </td>
            </tr> 
            
            
            <%
            if (contactEmailList != null && !contactEmailList.isEmpty())
            { 
            for(int i=0;i<contactEmailList.size();i++)
            {
            CampaignInvite campaignInvite = (CampaignInvite)contactEmailList.get(i);
            
            
            %>  
            <tr>
                <td>&nbsp;</td>
                <td class="content_txt" colspan="2" align="left"><%=campaignInvite.getEmailAddress()%>
                </td>
            </tr>     
            
            <%
            } 
            }
            else
            {
            %>   
            
            <tr>
                <td class="hdr_1" colspan="3" align="left">No Invites Sent
                </td>
            </tr>  
            <%
            } 
            %>   
            <tr>
                <td class="hdr_1" colspan="3" align="left">&nbsp;
                </td>
            </tr>  
           <tr class="formbuttonsCell"> 
                <td class="formbuttonsCell" align="left"><a href="/earth.do?act=sharePledge">Share More</a>
                </td>
                <td class="formbuttonsCell" colspan="2" align="right">
                </td>
            </tr>
            <tr>
                <td class="hdr_1" colspan="3" align="left">&nbsp;
                </td>
            </tr>  
    </table></div>
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table>   
