<table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://earth.080808.com.my?act=contestBlog';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  
<h1 class="h1_hdr">Contest for bloggers</h1> <br><br>

     <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
      <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="content_txt"><b>OBJECTIVE :</b> A blog post for climate change, an effective one.<br><br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  

            Three bloggers with the most creative and effective blog post on climate change wins!&nbsp;&nbsp;&nbsp;
            </td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  

            Put on your thinking cap and blog it. It pays to blog green.<br><br><br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  

            <b>Terms and conditions:</b><br><br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">   
            1) You just need to be a <a href="http://080808.com.my">080808.com.my</a> member to be eligible for this contest.<br></td>
        </tr>  
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            2) Your climate change blog post content should be as creative as possible to educate </td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">your readers about climate change.<br></td>
        </tr>    
        <%--tr>
          <td align="center" colspan="3" class="content_txt">  
            3) Your blog post should have a backlink to our Climate Change blog at <br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt"><a href="http://earth080808.blogspot.com/2008/06/climate-change-contest.html">http://earth080808.blogspot.com/2008/06/climate-change-contest.html</a>.<br></td>
        </tr--%>    
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            3) In any part of your blog post, there has to be an inclusion of text:<br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt"><b>"In support of Climate Change campaign by 080808.com.my"</b>.<br></td>
        </tr>    
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            4) Submit your blog post information <a href="/earth.do?act=contestBlogForm">here</a>.<br></td>
        </tr>    
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            5) The winners are selected by 080808.com.my and judges decision is final.<br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            It's that simple too.
            </td>
        </tr>   

        <tr>
          <td align="center" colspan="3" class="inputvalue">&nbsp;</td>
        </tr> 
        <tr>
          <td align="center" colspan="3" class="inputvalue">&nbsp;</td>
        </tr> 
       
        </table>
   
       </div> 
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table> 