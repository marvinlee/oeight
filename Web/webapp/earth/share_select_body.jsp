<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@taglib uri="/WEB-INF/struts-html-el.tld" prefix="html-el"%>    
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="java.util.*"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.contactlist.ContactImpl,
                 com.esmart2u.oeight.member.web.struts.controller.EarthForm,
                 com.esmart2u.solution.base.helper.PropertyManager" %>
<%
    EarthForm earthForm = (EarthForm)request.getAttribute("EarthForm"); 
      
    List contactEmailList = (List)earthForm.getContactEmailList();  
    
%>
    
<table width="100%">
<tr>  

<td width="15%">&nbsp;</td>

<td valign="top"><br><br><br>

<h1 class="h1_hdr">Share with friends</h1><br><br>    
    <html:form name="EarthForm" type="com.esmart2u.oeight.member.web.struts.controller.EarthForm" method="post" action="/earth.do" isRelative="true">
    <div id="inviteLayer" class="divBox">  
            <table width="80%">
                <COL width="20%"> 
                <COL width="40%"> 
                <COL width="40%">
              
                <tr>
                    <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br> 
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left">  
                        <html:errors property="shareRequest"/>  <br> 
                    </td>
                </tr> 
                <tr> 
                    <td class="content_txt" colspan="3" align="left">   
                        <b>Select contacts to share pledge for climate change:</b><br> <br> 
                    </td>
                </tr> 

                <%
                if (contactEmailList != null && !contactEmailList.isEmpty())
                { 

                %>  
                   <tr>
                <td colspan="3">
                        <%--input type="button" class="formbuttons" name="Delete" value="Delete" onclick="formDelete();"--%>
                        <table>
                        <tr>
                            <td width="10%" class="content_txt"><input type="checkbox" name="selectAllNonMembers" onclick="toggleSelectNonMembers(this);"></td>
                            <td width="30%" class="content_txt"><b>Name</b></td>
                            <td width="60%" class="content_txt"><b>Email</b></td>
                        </tr>
                        
                        <% int currentCount = 0;%>
                        <logic:iterate id="contact" name="EarthForm" property="contactEmailList" type="com.esmart2u.solution.base.helper.contactlist.ContactImpl">
                        <bean:define id="emailAddress" name="contact" property="emailAddress" type="java.lang.String"/> 
                            
                        <% 
                            if (currentCount % 2 == 0)
                            {
                            %>
                            <tr>
                            <%
                            }else
                            {
                            %>
                            <tr class="pledge_altbar">   
                            <%
                            }
                        %>                  
                            <td class="content_txt"><html-el:checkbox name="EarthForm" property="contactEmailString" value="${emailAddress}"/><%--bean:write name="messageIdString" /--%></td>
                            <td class="content_txt"><logic:present name="contact" property="name">
                                    <bean:define id="name" name="contact" property="name" type="java.lang.String"/>       
                                    <bean:write name="name" filter="true"/> 
                                </logic:present>
                            </td>
                            <td class="content_txt"><bean:write name="emailAddress" filter="true"/></td>   
                            </td>
                        </tr>
                        <% currentCount++; %>
                    </logic:iterate> 
                    </table>
            <script>toggleSelectNonMembers(document.forms[0].selectAllNonMembers)</script>
            </td>
            </tr>     

            <%
                        }
            else
            {
            %>   

            <tr>
                <td colspan="3"><br><br><br> 
                </td>
            </tr>     
            <tr>
                <td class="content_txt" colspan="3" align="left">No contacts found<br><br>
                </td>
            </tr>   
            <tr>
                <td colspan="3" class="content_txt" >&nbsp;<a href="/earth.do?act=shareManual">Input Email Addresses Manually</a>
                </td>
            </tr> 
            <%
            } 
            %>  

             <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="left"> 
                    <br>
                    <br>  
                </td>
            </tr>  
            
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        <br><br>
                        <%
                            if (contactEmailList != null && !contactEmailList.isEmpty()) 
                            {
                        %>
                        <input type="button" class="formbuttons" name="ok" value="Submit" onclick="formSubmit();"> 
                           <%
                            }
                            else
                            {
                            %>  
                        <input type="button" class="formbuttons" name="send" value="Search Again" onclick="formBack('sharePledge');">  
                            <%
                            } 
                            %>  
                        <br>   
                    </td>
                </tr> 
                <tr>
                    <td align="left">&nbsp;</td>
                    <td  colspan="2" align="left"> 
                        <br>
                        <br>  
                    </td>
                </tr> 
               
                
        </table></div>
        <input type="hidden" name="act" value="shareResult">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table>   
