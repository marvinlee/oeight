<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="java.util.*" %>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.EarthForm" %>
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.EmailProviderComboHelper" %>
  <% 
    EarthForm earthForm = (EarthForm)request.getAttribute("EarthForm");
    Vector providerList = EmailProviderComboHelper.getProviderList();
    %>
    
<table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://earth.080808.com.my?act=sharePledge';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

<h1 class="h1_hdr">Share with friends</h1><br><br>    
    <html:form name="EarthForm" type="com.esmart2u.oeight.member.web.struts.controller.EarthForm" method="post" action="/earth.do" isRelative="true">
        <div id="inviteLayer" class="divBox">   
            <table width="400">
                <COL width="40%"> 
                <COL width="20%">
                <COL width="40%"> 
                <tr>
                    <td class="lbl" align="center"> 
                        
                    </td>
                    <td align="left" colspan="2"> 
                        
                    </td>
                </tr>   
                <tr class="formbuttonsCell">
                    <td class="hdr_1" align="left">
                    </td>
                    <td class="formbuttonsCell" colspan="2" align="right">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">&nbsp;</td>
                    <td class="inputlabel" colspan="2" align="left">  
                        <html:errors property="shareRequest"/> 
                        <br> 
                        Select contacts after submission:<br> <br> 
                    </td>
                </tr> 
                <tr>
                    <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br><br>
                    </td>
                </tr>  
                <tr>
                    <td class="hdr_m" align="left">&nbsp;
                    </td>
                    <td colspan="2" align="left"> 
                       <br>
                        <table border="0">
                            <tr>
                                <td class="inputlabel" align="left">Email</td>
                                <td class="inputlabel" align="left">Password</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="emailUser" maxlength="100">&nbsp;
                                    <select name="provider"> 
                                    <%
                                    for(int i=0;i<providerList.size();i++)
                                    {  
                                        String[] provider = (String[])providerList.get(i);
                                        out.print("<option value='" + provider[0] +"'"); 
                                        if (provider[0].equals(earthForm.getProvider()))
                                            out.print(" selected ");
                                        out.print(">");
                                        out.println(provider[1] + "</option>");
                                    }
                                    %>
                                    <%--option value=""></option><option value="hotmail.com">@hotmail.com</option><option value="yahoo.com">@yahoo.com</option><option value="gmail.com">@gmail.com</option--%>
                                    </select>&nbsp;
                                </td>
                                <td><input type="password" name="password" maxlength="100"  onkeydown="if(event.keyCode==13)formSubmit();" >&nbsp;<br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                <i>Your password is not stored and is used for this search only.</i></td> 
                            </tr>
                        </table>
                        <br>  
                    </td>
                </tr> 
           
                <tr>
                    <td>&nbsp;
                    </td>
                    <td colspan="2" align="right">    
                        <input class="formbuttons" type="button" name="send" value="Submit" onclick="formSubmit();">  
                    </td>
                </tr> 
                <tr>
                    <td colspan="3">&nbsp;<br><br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;<a href="/earth.do?act=shareManual">Input Email Addresses Manually</a>
                    </td>
                </tr>
        </table></div> 
        <input type="hidden" name="act" value="shareSelect">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>  
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table>   
