<table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://080808.com.my/earth/ecobutton.jsp';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

     <h1 class="h1_hdr">Eco Button Review</h1> <br><br>

     <div id="earthlayer" class="divBox"> <center> <table width="100%"  border="0" align="center" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top">
        <p><a href="/earth.do?act=contest"><b>ECO BUTTON for lucky winners</b></a></p><br><br>
        <p>The ecobutton<FONT SIZE="-1"><SUP>TM</SUP></FONT> acts as a strong visual reminder and prompt for you to save electricity each time your computer is going to be left idle. </p>
        <p>The <strong>ecobutton<FONT SIZE="-1"><SUP>TM</SUP></FONT> </strong> is illuminated and sits on your table/desk top next to your keyboard. It connects to your computer via a USB cable. <br>
            <br>
          Each time you take a short or long break, a phone call, go for a meeting etc. you simply press the <strong>ecobutton<FONT SIZE="-1"><SUP>TM</SUP></FONT> </strong> and your computer is put into energy saving 'ecomode' which ensures that both your computer and monitor draw only the same nominal power as when they are shut down. <br>
          <br>
          However, with <strong>ecobutton<FONT SIZE="-1"><SUP>TM</SUP></FONT> </strong> by simply pressing any key on your keyboard your computer instantly returns to where you left off. <br>
          <br>
          There's also an additional bonus, each time your computer is put into 'ecomode' the clever <strong>ecobutton<FONT SIZE="-1"><SUP>TM</SUP></FONT> </strong> software records how many carbon units and how much power and money you have saved by using the <strong>ecobutton<FONT SIZE="-1"><SUP>TM</SUP></FONT> </strong>. Over time this can add up to quite a lot (especially if you have many computers running in an office) and you can use this data to help reduce your carbon footprint as well as your energy bills. <br>
          <br>
        </p> 
        <p><a href="http://www.eco-button.com" target="_blank"><b>The Official ECO BUTTON Website</b></a></p><br><br>
    </td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/1.jpg" width="559" height="373"></p>
    <p align="center">The ecobutton </p>
    <p>&nbsp; </p>    <strong> </strong></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/2.jpg" width="561" height="373"></p>
    <p align="center">Unpacked, all lined-up with the PC Energy Saving Device </p>
    <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/3.jpg" width="559" height="373"></p>
    <p align="center">Cute and useful little thing</p>
    <p>&nbsp; </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/4.jpg" width="560" height="372"></p>
    <p align="center"> A group photo with a mobile phone </p>
    <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/5.jpg" width="560" height="372"></p>
      <p align="center">Another group photo for a different perspective its size </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/6.jpg" width="558" height="371"></p>
      <p align="center">Plugged in! (all lighted up.. like a spaceship glowing) </p>
    <p align="center">It's not functioning yet.. need to download software from the internet. </p>    <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/7.jpg" width="560" height="373"></p>
      <p align="center">What say you? Cool, eh? </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/8.jpg" width="559" height="371"></p>
      <p align="center">Notice the 3 sided glow. </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/9.jpg" width="561" height="373"></p>
      <p align="center">First of a sequence of photos? to show that our little button glows and dims. Now showing as 'dead'? </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/10.jpg" width="560" height="372"></p>
      <p align="center">Glowing? </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/11.jpg" width="559" height="371"></p>
      <p align="center">Glowing more?? </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/12.jpg" width="561" height="373"></p>
      <p align="center">Glowing and starting to dim.. </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/13.jpg" width="560" height="373"></p>
    <p align="center"> Teaching how to use from      <a href="http://www.eco-button.com/" target="_blank">www.eco-button.com </a>.... </p>
    <p>&nbsp; </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/14.jpg" width="556" height="372"></p>
      <p align="center">Alright.. I've downloaded and installed the software. (approx 1.6Mb) <br>and I'm ready to demo the baby (see my hand la.. ) </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/15.jpg" width="560" height="373"></p>
      <p align="center">It worked! I think my laptop just went blank within 2 seconds. <br>Not sure what or where my OS went though. </p>
      <p align="center">Sleep or hibernate? </p>      <p>&nbsp;</p>
    <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/16.jpg" width="560" height="373"></p>
      <p align="center">It says that the button works in a few ways, depending on the OS.<br> Some may just require a press of the ecobutton and the PC springs back to life. <br>Others, need to press the 'ON' button gently once. Guess what's mine? </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/17.jpg" width="560" height="373"></p>
      <p align="center">This is what welcomes you after you put the PC back to life. </p>
    <p align="center">Kinda impressive and 'personalised' rather than just dump you back to where you left earlier. </p>    <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/18.jpg" width="560" height="373"></p>
      <p align="center">Close up of the screen. Shows savings in &pound;.. </p>
    <p align="center">Actually, when you first installed the software, you arrive at a configuration page.. <br>to select/input the type of monitor, CPU, etc.. and rate of electricity consumption for each of these equipments. <br> When you do spend time to input truthfully all these details, it would calculate your savings more precisely.</p>    <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/19.jpg" width="560" height="373"></p>
      <p align="center">My savings? not bad for my 5 seconds of 'effort' (blackout)! </p>
    <p align="center">Just imagine if I keep this up? I'll be <strong>$RICH$</strong> in no time! </p>    <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/20.jpg" width="559" height="372"></p>
      <p align="center">To date summary.. nice feature. </p>
    <p align="center">It sure pays to be green!</p>    <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p><img align="center"  src="http://080808.org.my/earth08/img/ecobutton/21.jpg" width="560" height="373"></p>
      <p align="center">Artistic shot? </p>      <p>&nbsp;</p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top"><p align="center"><strong><i>Eco Button reviewed by William Chung</i></strong></p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" valign="top">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
   </center>
       </div> 
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table> 
