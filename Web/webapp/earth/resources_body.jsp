<table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://earth.080808.com.my?act=resources';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

<h1 class="h1_hdr">Resources</h1> <br><br>

     <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
           <td align="center" colspan="3" class="content_txt"><b>International Environmental Organizations</b></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.greenpeace.org/international/">Greenpeace International</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.unep.org/">United Nations Environment Programme</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.campaigncc.org/">Campaign against Climate Change</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.globalclimatecampaign.org/">Global Climate Campaign</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.bbc.co.uk/climate/">BBC - Climate Change</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.worldwildlife.org/">World Wildlife Fund</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.defenders.org">Defenders of Wildlife</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.wrm.org.uy/">World Rainforest Movement</a></td>
        </tr> 
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;<br><br></td>
        </tr>  
        <tr>
           <td align="center" colspan="3" class="content_txt"><b>Organizations supporting environmental protection in Malaysia</b></td>
        </tr> 
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.ecoknights.com/">EcoKnights</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.gecnet.info/">Global Environment Centre</a></td>
        </tr>    
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.cetdem.org.my/">Centre for Environment, Technology and Development, Malaysia</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://mengo.org/">Malaysian Environmental NGO</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.mns.org.my/">Malaysian Nature Society</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.wwforg/">WWF-Malaysia</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://borneoproject.org/">The Borneo Project</a></td>
        </tr> 
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;<br><br></td>
        </tr>   
        <tr>
           <td align="center" colspan="3" class="content_txt"><b>Agencies under Malaysia's Ministry of Natural Resources and Environment</b></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.nre.gov.my/">Ministry of Natural Resources and Environment</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.doe.gov.my/">Department of Environment (DOE)</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.dmpm.nre.gov.my/">Department of Marine Park Malaysia</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.sabah.gov.my/mocet/">Ministry Of Tourism, Culture and Environment</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.wildlife.gov.my/">Department of Wildlife and National Park</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.frim.gov.my/">Forest Research Institute of Malaysia (FRIM)</a></td>
        </tr>  
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;<br><br></td>
        </tr>   
        <tr>
           <td align="center" colspan="3" class="content_txt"><b>Other Climate Change Online Resources</b></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.carbonfootprint.com/evidence.html">Evidence Of Climate Change</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.carbonfootprint.com/faq.html">Questions on Climate Change</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://www.direct.gov.uk/en/Environmentandgreenerliving/Greenerlivingaquickguide/DG_072885">Climate Change : a quick guide : Directgov</a></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"><a target="_blank" href="http://google-latlong.blogspot.com/2008/05/climate-change-in-our-world.html">Google LatLong: Climate change in our world</a></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;<br><br></td>
        </tr>  
        <tr>
           <td align="center" colspan="3" class="content_txt"><b>Links submission</b></td>
        </tr>   
        <tr>
            <td align="center" colspan="3" class="subcontent_txt">If you are an organization for the environment,  please submit your link to us.<br><br></td>
        </tr>  
        <tr>
            <td align="center" colspan="3" class="subcontent_txt"> 
                Please email to :<SCRIPT type="text/javascript">eval(unescape('%64%6f%63%75%6d%65%6e%74%2e%77%72%69%74%65%28%27%3c%61%20%68%72%65%66%3d%22%6d%61%69%6c%74%6f%3a%70%72%40%30%38%30%38%30%38%2e%63%6f%6d%2E%6D%79%22%3e%70%72%20%61%74%20%30%38%30%38%30%38%20%64%6F%74%20%63%6F%6D%20%64%6F%74%20%6D%79%3c%2f%61%3e%27%29%3b'))</SCRIPT><br><br>
            </td>
        </tr>
        </table>
   
       </div> 
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table> 