<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 
 
<tiles:insert page="/tiles/earth_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Climate Change - Resources" />
   <tiles:put name="header" value="/tiles/earth_top.jsp" />
   <tiles:put name="javascript" value="/tiles/empty.jsp" />
   <tiles:put name="menu" value="/earth/earth_menu.jsp" />
   <tiles:put name="body" value="/earth/resources_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/earth_bottom.jsp" /> 
</tiles:insert>