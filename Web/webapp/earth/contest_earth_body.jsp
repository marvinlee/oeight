<table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://earth.080808.com.my?act=contestEarth';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

<h1 class="h1_hdr">Contest for everybody</h1> <br><br>

     <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
    
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="content_txt"><b>OBJECTIVE :</b> Get your friends to pledge for climate change here.<br><br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  

            Three persons with the most friends who pledged wins!&nbsp;&nbsp;&nbsp;

            <a href="/earth.do?act=sharePledge">Share with my friends now</a><br><br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            You can also share with our <a href="/earth.do?act=friendster">for Climate Change</a> Friendster application.<br><br><br></td>
        </tr>   
  
        <tr>
          <td align="center" colspan="3" class="content_txt">  

            <b>Terms and conditions:</b><br><br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">   
            1) You just need to be a <a href="http://080808.com.my">080808.com.my</a> member to be eligible for this contest.<br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            2) <a href="/earth.do?act=sharePledge">Share with your friends</a> so that they make their pledge for climate change too.<br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            3) Our website will keep track of the number of persons who have actually made a pledge from you.<br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            4) The winners are selected based on the highest number of pledge made by their friends.<br></td>
        </tr>   
        <tr>
          <td align="center" colspan="3" class="content_txt">  
            It's that simple.
            </td>
        </tr>   

        <tr>
          <td align="center" colspan="3" class="inputvalue">&nbsp;</td>
        </tr> 
        <tr>
          <td align="center" colspan="3" class="inputvalue">&nbsp;</td>
        </tr> 
        </table>
   
       </div> 
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table> 