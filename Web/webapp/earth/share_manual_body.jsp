 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
     
<table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://earth.080808.com.my?act=shareManual';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

<h1 class="h1_hdr">Share with friends</h1><br><br>     
    <html:form name="EarthForm" type="com.esmart2u.oeight.member.web.struts.controller.EarthForm" method="post" action="/earth.do" isRelative="true">
        <div id="earthLayer" class="divBox">   
            <table width="400">
                <COL width="40%"> 
                <COL width="20%">
                <COL width="40%"> 
                <tr>
                    <td class="lbl" align="center"> 
                        
                    </td>
                    <td align="left" colspan="2"> 
                        
                    </td>
                </tr>   
                <tr>
                    <td class="content_txt" colspan="3" align="left">&nbsp;<br><br><br>
                    </td>
                </tr>  
                <tr>
                    <td class="content_txt" align="left">&nbsp;
                    </td>
                    <td colspan="2" align="left" class="content_txt" > 
                        <span class="content_txt" >Email addresses :</span>&nbsp;<br>
                        <html:textarea styleClass="inputvalue" name="EarthForm" property="emails" cols="50" rows="5" onkeydown="textareaCheck(this, 1500)"/>   
                        <html:errors property="emails"/> <br>
                        You can seperate emails addresses with spaces, comma (,) or semi-colon (;)
                    </td>
                </tr>
                <%--tr>
                <td class="lbl" align="right">My message :&nbsp;
                </td>
                <td align="left">
                        <html:textarea name="InviteForm" property="message" cols="50" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <html:errors property="message"/>  
                </td>
            </tr--%> 
           
                <tr>
                    <td>&nbsp;
                    </td>
                    <td colspan="2" align="right">    
                        <input class="formbuttons" type="button" name="send" value="Send" onclick="formSubmit();">  
                    </td>
                </tr> 
                <tr>
                    <td colspan="3">&nbsp;
                    </td>
                </tr>
        </table></div> 
        <input type="hidden" name="act" value="shareManualSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form> 
    </td>
    <td width="15%">&nbsp;</td>
    </tr>
</table>      
