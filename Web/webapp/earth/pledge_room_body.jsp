<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>  
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.EarthForm,
                 com.esmart2u.oeight.member.web.struts.helper.Page,
                 com.esmart2u.oeight.member.web.struts.helper.OEightPledgeHelper"%>  
<%@ page import="com.esmart2u.oeight.data.OEightPledge"%>  
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.Page"%>  

 <% 
    EarthForm earthForm = (EarthForm)request.getAttribute("EarthForm"); 
    
    List resultList = null;
    StringBuffer pagingOutput = null;
    if (earthForm.getPledgeListPage() != null){
        resultList = earthForm.getPledgeListPage().getThisPageElements();
    } 
%>  
<html:form name="EarthForm" type="com.esmart2u.oeight.member.web.struts.controller.EarthForm" method="get" scope="request"  action="/earth.do" isRelative="true">
   
<%-- This page needs total 3 columns instead of 4--%> 
      <table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://earth.080808.com.my?act=pledgeRoom';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  
                <h1 class="h1_hdr">Pledge Room</h1> <br><br>

     <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr> 
         
   <bean:define id="pledgeListPage" name="EarthForm" property="pledgeListPage" type="com.esmart2u.oeight.member.web.struts.helper.Page"/> 
        <%--bean:define id="thisPageElements" name="pledgeListPage" property="thisPageElements" type="java.util.List"/--%> 
        <logic:present name="pledgeListPage" property="thisPageElements">
        <logic:notEmpty name="pledgeListPage" property="thisPageElements">
              
         <%--tr>
            <td colspan="3">&nbsp;
            </td>
        </tr--%> 
        
        <tr class=inputlabel>
             <%
                int currentPageNumber = earthForm.getPledgeListPage().getPageNumber();
                int lastPageNumber = earthForm.getPledgeListPage().getLastPageNumber(); 
                pagingOutput = new StringBuffer();
                if (currentPageNumber+1 > 2 ){
                    pagingOutput.append("<a href='javascript:browsePage(" + 0 + ")'> First </a>&nbsp;");
                }
                if (earthForm.getPledgeListPage().hasPreviousPage())
                {
                    pagingOutput.append("<a href='javascript:browsePage(" + (currentPageNumber -1) + ")'>"+ (currentPageNumber) + "</a>&nbsp;");
                }
                pagingOutput.append((currentPageNumber +1) + "&nbsp;");
                if (earthForm.getPledgeListPage().hasNextPage())
                {
                    pagingOutput.append("<a href='javascript:browsePage(" + (currentPageNumber +1) + ")'>"+ (currentPageNumber+2) + "</a>&nbsp;");
                }
                if (currentPageNumber+1 < lastPageNumber){
                    pagingOutput.append("<a href='javascript:browsePage(" + (lastPageNumber) + ")'> Last </a>&nbsp;");
                }
                pagingOutput.append("&nbsp; of " + (lastPageNumber + 1));
                %>
                <td class="formbuttonsCell" colSpan=3> <%=pagingOutput.toString()%><a name="pledgeTop" id="pledgeTop"></a></td>
        </tr>
        <tr>
            <td colSpan=3>
                <table cellpadding="3" cellspacing="0">
                    <tr class=inputlabel>
                        <td>No</td>
                        <td>Name</td>
                        <td>&nbsp;</td>
                        <td>Pledge</td>
                        <td>Profile</td> 
                    </tr>
                    <% int currentCount = ((currentPageNumber*100)+1);%>
                    <logic:iterate id="pledge" name="pledgeListPage" property="thisPageElements"  type="com.esmart2u.oeight.data.OEightPledge">
                        
                        <bean:define id="pledgerName" name="pledge" property="pledgerName" type="java.lang.String"/>    
                        
                        <% 
                            if (currentCount % 2 == 0)
                            {
                            %>
                            <tr>
                            <%
                            }else
                            {
                            %>
                            <tr class="pledge_altbar">   
                            <%
                            }
                        %>                          
                            <td class="inputlabel" align="center"><%=currentCount++%></td>
                            <td class="lbl" align="center"> <bean:write name="pledgerName" filter="true"/></td> 
                            <td class="lbl" align="center">  
                                <% String pledgeCategory = OEightPledgeHelper.getPledgeCategoryByCode(""+pledge.getPledgeCode());%>
                                <%--img src="http://080808.org.my/earth08/img/<%=pledgeCategory%>_m.gif">--%>
                                <img src="/images/earth/<%=pledgeCategory%>_s.gif" align="middle">
                            </td> 
                            <td class="lbl" align="center"> 
                                <%=OEightPledgeHelper.getPledgeLabelByCode(""+pledge.getPledgeCode())%>  
                            </td> 
                            <td>
                                <% if (pledge.getPledgeFrom() > 0 && pledge.isShowPledgeFrom())
                                {
                                    switch (pledge.getPledgeFrom())
                                    {
                                        case (EarthForm.PLEDGE_FROM_FRIENDSTER):
                                        out.print("<a href=\"http://profiles.friendster.com/" + pledge.getPledgeFromUserId() +"\" target=\"_blank\">Friendster</a>");
                                        break;
                                        case (EarthForm.PLEDGE_FROM_FACEBOOK):
                                        out.print("<a href=\"http://www.facebook.com/profile.php?id=" + pledge.getPledgeFromUserId() +"\" target=\"_blank\">Facebook</a>"); 
                                        break; 

                                    }
                                
                                }
                                else
                                {
                                    if(pledge.getUserId()> 0 && StringUtils.hasValue(pledge.getUserName()))
                                    {
                                        out.print("<a href=\"http://profile.080808.com.my/" + pledge.getUserName() +"\">" + pledge.getUserName() +"</a>");
                                    }
                                    else
                                    {
                                        out.print("-");
                                    }
                                }
                                %> 
                            </td>
                        </tr>
                        
                    </logic:iterate>
                </table>
        </td></tr>      
        <tr class=inputlabel> 
            <td class="formbuttonsCell" colSpan=3><nobr><%=pagingOutput.toString()%></nobr> 
            </td>
        </tr>      
        <tr class=inputlabel> 
            <td class="hdr_1" align="right"><a href="#pledgeTop"><nobr>Back to Top</nobr></a> 
            </td>
        </tr>  
      
        </logic:notEmpty>
        <logic:empty name="pledgeListPage" property="thisPageElements">
        
        <tr>
            <td class="hdr_1" colspan="3" align="left">No Results
            </td>
        </tr>  
        </logic:empty>
        </logic:present>
         
        <logic:notPresent name="pledgeListPage" property="thisPageElements"> 
        <tr>
            <td class="hdr_1" colspan="3" align="left">No Results
            </td>
        </tr>   
        </logic:notPresent>
        
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
             
     </table></div>
       
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table>
    </td>   
        <input type="hidden" name="currentPage" value="<%=earthForm.getCurrentPage()%>">
        <input type="hidden" name="act" value="pledgeRoom">
        <%--input type="hidden" name="token" value="<%=request.getAttribute("token")%>"--%> 
       </div>  
    </html:form>        
    
    
 <% 
   
    if (earthForm.getPledgeListPage() != null){
        Page pledgeListpage = earthForm.getPledgeListPage();
        pledgeListpage = null;
        earthForm.setPledgeListPage(null);
    } 
%>  