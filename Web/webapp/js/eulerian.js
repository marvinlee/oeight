function addEngine() {
 var base_uri = 'http://www.eulerian.com/misc/eulerian-support-searchplugin/eulerian-support-searchplugin';
 if ((typeof window.sidebar == "object") && (typeof window.sidebar.addSearchEngine == "function")) { 
  window.sidebar.addSearchEngine(
    base_uri+'.src', base_uri+'.png', 'Eulerian Support', '');
 } else {
  alert("Sorry, you need a Mozilla-based browser (such as Firefox) to install a search plugin.");
 } 
}
