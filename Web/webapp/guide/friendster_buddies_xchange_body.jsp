<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3" width="600">
       <TABLE width=600>
        <TBODY>
        <TR>
          <TD width="15%">&nbsp;</TD>
          <TD vAlign=top>
            <H1>Guide</H1>
           <DIV class=divBox id=guidelayer>
            <TABLE borderColor=#999999 cellSpacing=0 cellPadding=5 width="100%" 
            border=0>
              <TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=5 width="100%" border=0>
                    <TBODY>
                    <TR bgColor=#333333>
                      <TD colSpan=2><SPAN class=infoBar>Guide to Add My oEight Buddies xChange to your Friendster<FONT SIZE="-1"><SUP>TM</SUP></FONT></SPAN></TD></TR>

         
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/friendster_buddies_xchange.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr>                       
                    <TR>
                    <TR>
                      <TD colSpan=2>
                        <P>&nbsp;</P>
                        <P>This guide is targeted to our members who has a Friendster profile.</P>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app5/friendster_buddies1.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app5/friendster_buddies1t.jpg" 
                        border=1></a></font></P>
                        <P>&nbsp;  </P>
                        <P>You can check out our sample app at our profile page 
                        : <A href="http://profiles.friendster.com/my080808" 
                        target=#>http://profiles.friendster.com/my080808</A></P>
                        <P>Introducing the latest and by far the hottest Application from 080808.com.my developed on Friendster API: My oEight Buddies xChange</P>

                        <P>080808.com.my is a fan of Friendster community ourselves, having introducing our Friendster profile to our members since the start, we are now a Friendster Fan Profile under the Website category.</P>

                        <P>As a show of support to Friendster, we are glad to develop an application on Friendster API for the easy exchange of buddies or friends!
                        </P>
                        <P>Remember when we say 080808.com.my is more than a mosaic? Yes, it's making friends and making even more friends!
                        </P>
                        <BR><BR><BR>
                        <P><U>How to add My oEight Buddies xChange App to your Friendster profile</U></P>
                        <P>&nbsp;</P></TD></TR> 
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>So, what is Buddies xChange?</SPAN> </TD>
                      <TD>&nbsp;</TD></TR>
                   <TR>
                      <TD>
                        <P>As the name implies, it is actually a way of exchanging or sharing of friends between oEight with others.</P><BR>
                        <P>
                            By adding My oEight Buddies xChange App to your Friendster profile, you will be able to :<br>
                            &nbsp;&nbsp;&nbsp;a) Show your oEight buddies in your Friendster Profile<br>
                            &nbsp;&nbsp;&nbsp;b) Show your Friendster friends in your oEight Profile<br>
                            &nbsp;&nbsp;&nbsp;c) Show your Friendster photos in your oEight Profile<br>
                        </P>
                        <P>
                            Your friends viewing your Friendster profile and clicked on your oEight buddies will be able to view your buddy's oEight Profile page.<br>
                            Similarly, your friends viewing your oEight profile and clicked on your Friendster friends will be able to view their Friendster Profile page.<br>
                            This is what we call Buddies xChange.<br>
                        </P>
                        <P>
                            If you have previously added oEight Apps, you should know the drill by now.<br>
                            Just follow the steps below:<br>
                        </P>
                      <BR><BR></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>1) Login to <A 
                        href="http://www.friendster.com/" 
                        target=_blank>Friendster<FONT 
                        size=-1><SUP>TM</SUP></FONT></A> or Grab This App from 
                        our <A href="http://profiles.friendster.com/my080808" 
                        target=#>profile page</A></SPAN> </TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app5/friendster_buddies2.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app5/friendster_buddies2t.jpg" 
                        border=1></a></font></P>
                      <BR>You can grab this app at our profile page : <A href="http://profiles.friendster.com/my080808" 
                        target=#>http://profiles.friendster.com/my080808</A><BR></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>2) Go to My 
                        oEight Buddies xChange App install page</SPAN></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app5/friendster_buddies3.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app5/friendster_buddies3t.jpg" 
                        border=1></a></font></P>
                        <P>After login to Friendster, go to the App Install page 
                        at <A href="http://widgets.friendster.com/080808_fren" 
                        target=#>http://widgets.friendster.com/080808_fren</A>  
</P></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>3) Login to 
                        080808.com.my </SPAN></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app5/friendster_buddies4.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app5/friendster_buddies4t.jpg" 

                        border=1></a></font></P>
                        <BR>
                        After login with your 080808.com.my account, the Buddies xChange app will be display on your Friendster profile.
                        <P><a href="http://080808.org.my/img/guide/friendster/app5/friendster_buddies2.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app5/friendster_buddies2t.jpg" 
                        border=1></a></P>
                        <BR>
                        Then, your oEight profile will also be updated with friends and photos depending on the privacy preference you have set.
                         <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app5/friendster_buddies5.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app5/friendster_buddies5t.jpg" 
                        border=1></a></font></P></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD> 
                        <P>An example of our oEight profile page at : <a href="http://profile.080808.com.my/guide08">http://profile.080808.com.my/guide08</a> </P>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app5/friendster_buddies6.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app5/friendster_buddies6t.jpg" 
                        border=1></a></font></P>
                        <P>That's it! Start sharing your Buddies xChange App with your friends too. </P>
                        <P></P>
                        <BR>
                        <P>This guide can be found at : <A 
                        href="http://080808.com.my/guide/friendster_buddies_xchange.jsp">http://080808.com.my/guide/friendster_buddies_xchange.jsp</A> 
                        </P>
                        <BR><BR>
                        <P><b>Note</b> :<br>
                            Friendster is a online community operated by Friendster Inc USA. This Application is not endorsed or controlled by Friendster, but is developed using the Friendster API provided by Friendster.<br>
                            By installing this application, you agree with Friendster's Term of Use and 080808 Term of Use.<br>
                        </P> 
                    </TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR> 
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/friendster_buddies_xchange.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD width="5%">&nbsp;</TD>
                      <TD>
                        <P><BR></P></TD></TR></TBODY></TABLE></TD>
                <TD 
        width="15%">&nbsp;</TD></TR></TBODY></TABLE></DIV></TD></TD></TR></TBODY></TABLE>
    </td>                                  


