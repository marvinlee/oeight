<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3" width="600">
        <table width="600">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">

                <h1>Guide</h1> 

                    <div id="guidelayer" class="divBox"> 
                        <table width="100%" border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
                          <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr bgcolor="#333333">
        <td colspan="2"><span class="infoBar"> Guide </span></td>
        </tr>
        
         
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/main.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
        
      <tr>
        <td colspan="2">
          <%--p>You have reached the secret page of 080808.com.my </p>
          <p>This is the cheat-sheet.</p>
          <p>No, not really a cheat-sheat. This guide page is meant to be an explained-all-guide or a frequently asked question manual.</p>
          <p>We do not guarantee you will get rich from reading this but it will serve a better understanding of this website and how to use it to your benefits. </p--%>
          <p>This guide page is meant to be an explain-all-guide or a "frequently asked questions" manual.</p>
          <p>Reading this will ultimately serves you a better understanding of 080808.com.my and how to use it to your benefits. </p>
          <p>This page is divided into the following sections: <a name="top"></a></p>          <p><strong> <a href="#profile">Profile</a> </strong></p>
          <p><strong> <a href="#photos">Photos + Flickr </a> </strong></p>
          <p><strong> <a href="#contact">Contact</a> </strong></p>
          <p><strong> <a href="#gadgets">Gadgets</a> </strong></p>
          <p><strong> <a href="#buddies">Buddies</a> </strong></p>
          <p><strong> <a href="#wishes">Wishes</a> </strong></p>
          <p><strong> <a href="#votes">Votes, mosaic ranking</a> </strong></p>
          <p><strong> <a href="#invites">Invites</a> </strong></p>
          <p><strong> <a href="#views">Page views</a> </strong></p>
          <p><strong> <a href="#blog">Featured Blog</a> </strong></p>
          <p><strong> <a href="#bookmark">Profile Bookmarking</a> </strong></p></td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="profile">Profile</a></span> </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>Ensure that your profile page and content is always up-to-date. Provide information as much as you can, but be careful not to reveal too much of your personal identity. This is even more for teenagers and young adults. </p>
          <p><br>
          </p>
            <p>Setting the profile preference to "Show Profile" in "Profile - Personal Details" will enable everyone being able to see your profile and generate higher interest.</p>
           <p><br>
          </p>
          <p>Your Oeight Id is searchable at the homepage and you can always promote your profile page with the URL ( http://profile.080808.com.my/myself ) in your personal blog or as a signature in your emails. </p>
          <p><br>
          </p>
          <p>Under your "Profile - Contact Details", you can allow other members to visit your personal blog, social networking sites, or other websites. </p>
          <p><br>
          </p>
          <p>However, if you are not comfortable with sharing too much info and rather not be the most popular 080808 member, you can always hide your contact or hide your profile entirely. </p>
          <p><br>
          </p>
          <p>After all, the most special thing here is our 080808 mosaic! </p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="photos" id="photos"></a>Photos + Flickr</span> </td> 
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <p>A picture speaks a thousand words. The key to your profile from the homepage is your mosaic photo. Upload the best photo of yourself which adheres to our Terms of Use under "Profile - Photos". Photos which are ordered from the top (most voted) are also more likely to be viewed. </p>
          <p><br>
          </p>
          <p>Integrate your profile with Flickr if you already have photos in Flickr. If you do not have one, getting an account in Flickr is fairly easy. Just head on to <a href="http://www.flickr.com/" target="#">www.flickr.com </a></p>
          <p><br>
          </p><p>All you need to do to link your Flickr public photos to your 080808 profile page is just provide your Flickr Id only. </p>
          <p><br>
          </p><p>Remember that we will never ask for your password except to login to this website of course. </p>
          <p><br>
          </p><p>This feature is powered by <a href="http://www.flickr.com/" target="#">www.flickr.com</a>. </p>
          <p><br>
          </p><p>More details here: <a href="/guide/flickr.jsp">Guide to Integrate Flickr&#8482;</a> </p> 
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="contact" id="contact"></a>Contact</span> </td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <p>Your "Profile - Contact Details" is a very good place to spur interest on your profile page. It can help you generate more traffic to your personal blog or websites. </p>
           <p><br>
          </p><p>If you have joined social networking sites, you can also provide the relevant Id and your profile page can have an automatic link to your social networking sites. </p>
          <p><br>
          </p>
          <p>Are your friends joining many different social networking sites while you're trying to follow every single one of them? By having one that is able to link to all like 080808.com.my will enable you to check out their profile pages easier. </p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="gadgets" id="gadgets"></a>Widgets</span> </td>   
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>The 080808 Widgets are provided by 080808.com.my, so that you can place it as a widget in your personal web pages. </p>
          <p><br>
          </p><p>On the other hand, you can always promote your profile page with the URL ( http://profile.080808.com.my/myself ) at your own blog or as a signature in your emails. </p>
          <p>&nbsp;</p>
          <p><a href="#top">Back to Top</a> </p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="buddies" id="buddies"></a>Buddies</span> </td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
            <p>Buddies under "Profile - Buddies" are members who have sent you wish or wishes through your profile page. A member who sends you a wish will automatically join the list of your buddies. </p>
          <p><br>
          </p><p> 
          Photos are ordered based on the latest wish received.
          </p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr> 
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="wishes" id="wishes"></a>Wishes</span> </td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p><br>
          Post a wish at your profile page. It can be something for your birthday, for 080808, for exam, for business or for your loved ones.</p>
          <p><br>
          </p><p> It might really turn out that your buddies make your wish come true ;-). </p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="votes" id="votes"></a>Votes, mosaic ranking</span> </td>   
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <p>We have a voting system for 080808.com.my members. Each members are given 10 votes daily and can vote for their own profile once everyday. Voting system will enable popular members to be recognized. </p>
          <p><br>
          </p><p>Most voted members will also be placed at the top of the mosaic in the homepage. </p>
          <p><br>
          </p><p>The top 3 most voted members for the month will also be ranked at the Podium page.</p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="invites" id="invites"></a>Invites</span> </td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <p>Getting too many invites from your friends to join their social networking site? </p>
          <p><br>
          </p>
          <p>Invite them over to 080808.com.my! You will also know their multiple profile pages if they share them under "Profile - Contact Details". </p>
          <p><br>
          </p>
          <%--p><b>IMPORTANT! 080808.com.my will never ask for your personal email password to add friends on your behalf. We would rather take the less travel route instead of risking your personal information.</b></p>
          <p><br>
          </p>
          <p>
          Although we could get more members by asking for your email and password and automatically sending them to your contacts, we would like our members to have a secured experience.</p>
          <p><br>
          </p>
          <p>
          This may not be the best choice for a website like ours, but we believe ultimately our members will appreciate our honesty.<br>
          If you support this effort, kindly invite friends via "Profile - Invites"  </p>
          <p><br>
          </p--%>
          <p>Top 3 members who sent the most accepted invites each month will also be ranked at the Podium page. </p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="views" id="views"></a>Page views</span> </td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <p>The number of times a profile page has been viewed is tracked. However, viewing of own profile page will not be taken into account for ranking. </p>
          <p><br>
          </p>
          <p>The monthly top 3 most viewed profile members will also be ranked at the Podium page.</p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="blog" id="blog"></a>Featured Blog</span> </td>   
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <p>If you have a personal blog or commercial blog you would like us to promote, select the checkbox for "Feature My Blog!" under "Profile - Contact Details". </p>
          <p><br>
          </p><p>If the blog fits our bill, we will put it under 080808 Featured Blogs and so that it can be viewed by all our members. </p>
          <p><br>
          </p><p>This is a great way to promote your site to a wider audience. </p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"> <span class="infoBar"><a name="bookmark" id="bookmark"></a>Profile Bookmarking</span> </td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          <p>Every 080808 profile page includes links to a range of social bookmarking / sharing websites. </p>
          <p><br>
          </p><p>You can share interesting profiles both with friends and people with similar interests. You can also access your bookmarked pages from any computer you happen to be using as the links are saved on the internet. </p>
          <p><br>
          </p><p>You can find out more about social bookmarking on Wikipedia : <a href="http://en.wikipedia.org/wiki/Social_bookmarking" target="#">Social bookmarking - Wikipedia, the free encyclopedia</a></p>
          <p><br>
          </p><p>This feature is powered by  <a href="http://www.addthis.com/" target="#">www.addthis.com</a>.</p>
          <p><a href="#top">Back to Top</a></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
         
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/main.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
        
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="5%">&nbsp;</td>
        <td><p><br> 
          </p>
          </td>
      </tr>
                    </table>
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table></div>
    </td>                             </td>
                </tr>
            </table>
    </td>                                  