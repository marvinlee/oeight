<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3" width="600">
        <table width="600">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">

                <h1>Guide</h1> 

                    <div id="guidelayer" class="divBox"> 
                        <table width="100%" border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
                          <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr bgcolor="#333333">
        <td colspan="2"><span class="infoBar"> Guide to Integrate Flickr&#8482; </span></td>
        </tr> 
         
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/flickr.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
          
          
       <tr>
        <td colspan="2"><p>&nbsp;</p>
          <p>All it takes is just <strong>1</strong> single thing, your Flickr Id! </p>          
          <p>We bet you never know it is so easy to add Flickr photos to your profile, just follow the steps below: </p>
          <p><u>How To Add Your Flickr Photos into your oEight Profile </u></p>
          <p>&nbsp;</p>
          </td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"><span class="infoBar"> 1) Login to 080808.com.my</span> </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p><img src="http://080808.org.my/img/guide/flickr1.gif" width="296" height="168" border="1"></p>
          </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"><span class="infoBar"> 2) Profile -&gt; Update Photos </span></td> 
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p><img src="http://080808.org.my/img/guide/flickr2.gif" width="709" height="526" border="1"></p>
          </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"><span class="infoBar"> 3) Integrate Flickr </span></td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>&nbsp;</p>
          <p><img src="http://080808.org.my/img/guide/flickr3.gif" width="795" height="294" border="1"></p>
          <p></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"><span class="infoBar"> 4) Flickr Id </span></td>   
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>&nbsp;</p>
          <p><img src="http://080808.org.my/img/guide/flickr4.gif" width="605" height="485" border="1"></p>
          <p>&#149;&nbsp; If you know your Flickr Id, just key into the Flickr Id field and click preview.</p>
          <p>&#149;&nbsp; If you do not know your Flickr Id but you have a Yahoo! Account, key in your Flickr or Yahoo! user name here and we will request for your Flickr Id from Yahoo! automatically. (No password required) </p>
          <p>&#149;&nbsp; If you do not have a Flickr Id, you can register for one at <a href="http://www.flickr.com/" target="#">www.flickr.com </a></p>
          <p>&nbsp;</p>
          <p></p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"><span class="infoBar"> 5)&nbsp; Preview  </span></td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p><br>
            </p>
          <p><img src="http://080808.org.my/img/guide/flickr5.gif" width="602" height="485" border="1"></p>
          <p>Your Flickr photos will be automatically displayed. </p>
          <p>* If you notice that the photos are not loaded for a period longer than expected, kindly <strong>Refresh (Ctrl+F5) </strong>on the page. </p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"><span class="infoBar"> 6)&nbsp; Confirm </span></td>   
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>&nbsp;</p>
          <p><img src="http://080808.org.my/img/guide/flickr6.gif" width="601" height="365" border="1"></p>
          <p>Confirm that the photos indeed belong to you. We recommend posting photos belonging to you, and adhere to our Terms of Use. </p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td bgcolor="#333333"><span class="infoBar"> 7)&nbsp; Done! </span></td>  
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><p>&nbsp;</p>
          <p><img src="http://080808.org.my/img/guide/flickr7.gif" width="709" height="530" border="1"></p>
          <p>That's all it takes. Start showing your oEight profile page to your friends. Bet they will be surprised too =) </p>
          <p>We told you it's easy, didn't we? </p>
          <p>&nbsp;</p>
          </td>
      </tr>
         
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/flickr.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="5%">&nbsp;</td>
        <td><p><br> 
          </p>
          </td>
      </tr>
                    </table>
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table></div>
    </td>                             </td>
                </tr>
            </table>
    </td>                                  