<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<tiles:insert page="/tiles/full_mosaic_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Home" />
   <tiles:put name="header" value="/tiles/top_with_wedding_banner.jsp" />
   <tiles:put name="javascript" value="/oeight/javascript.jsp" />
   <tiles:put name="onLoad" value="canvas.sweepToggle('expand',count);" /> 
   <tiles:put name="body" value="/oeight/mosaic.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>