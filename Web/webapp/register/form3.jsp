<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>    
<%-- This page needs total 3 columns instead of 4--%>  
    <td width="5%">&nbsp;</td> 
    <td width="80%" valign="top"> 
    <h1>Registration</h1> 
    <html:form name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm" method="post" scope="session"  action="/register.do" isRelative="true" enctype="multipart/form-data">
        <div id="register1Layer" class="divBox"><table>
                <COL width="5%"> 
                <COL width="25%"> 
                <COL width="70%">
                <tr>
                    <%--td  colspan="3" rowspan="11" width="5%">&nbsp;</td--%>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" colspan="2" align="left">Final Step
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left">&nbsp;
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputvalue" align="right">*oEight Tips :&nbsp; 
                    </td>
                    <td align="left"> 
                        This photo will be displayed on our 080808 mosaic after submission. Photo file can be of BMP, GIF or JPG.          
                        Your photo file size should not exceed 250Kb. We recommend a square photo with at least 160px(width) and 160px(height).<br>
                        You can use any imaging tools, eg MsPaint and resize it with "Stretch / Scale" to reduce the actual size.
                        <%-- If you have problem resizing your photo, please read this guide --%>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left">&nbsp;
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Photo Upload :&nbsp; 
                    </td>
                    <td align="left">
                        <html:file styleClass="inputvalue" name="RegisterForm" property="photoFile"  onchange="preview(this)"/>   
                        <html:errors property="photoPath"/>  
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Photo Preview :&nbsp;
                    </td>
                    <td align="left"> 
                        <img alt="Photo preview" id="previewField" src="/images/spacer.gif"> 
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Photo Description :&nbsp;
                    </td>
                    <td align="left">
                        <html:textarea styleClass="inputvalue" name="RegisterForm" property="photoDescription" cols="50" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <br>
                        <html:errors property="photoDescription"/>  
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left">&nbsp;
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputvalue" colspan="2" align="left">
                         Your registration will complete by clicking on 'Done' or 'Upload Photo Later' below and you confirm that you are over 13 years of age and <br>accept the <a href="javascript:openNewWin('/info/terms_of_use.htm');">Terms Of Use</a>.<br>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><input class="formbuttons" type="button" name="back" value="Back" onclick="formBack('one');">
                        <input class="formbuttons" type="button" name="continue" value="Done" onclick="formSubmit();">
                        <input class="formbuttons" type="button" name="skip" value="Upload Photo Later" onclick="formSkip();">
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;
                    </td> 
                </tr>
                
        </table></div>
        <input type="hidden" name="act" value="three">
        <input type="hidden" name="skip3" value="three">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
            </td>
            <td width="5%">&nbsp;</td>