<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>  
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.RegisterForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.member.web.struts.helper.StateComboHelper,
                 com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper"%>    

<%
    RegisterForm registerForm = (RegisterForm)session.getAttribute("RegisterForm");
    if (!StringUtils.hasValue(registerForm.getNationality()))
    {
        registerForm.setNationality(PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY));
    }
        
    /*System.out.println("RegisterForm gender=" + registerForm.getGender());
    System.out.println("RegisterForm day=" + registerForm.getBirthDay());
    System.out.println("RegisterForm month=" + registerForm.getBirthMonth());
    System.out.println("RegisterForm year=" + registerForm.getBirthYear());
    System.out.println("RegisterForm state=" + registerForm.getState());
    System.out.println("RegisterForm country=" + registerForm.getNationality());*/
    
    
    Vector countryList = CountryComboHelper.getCountryList();
    Vector stateList = StateComboHelper.getStateList();
%> 
<%-- This page needs total 3 columns instead of 4--%> 
    <td width="5%">&nbsp;</td> 
    <td width="80%" valign="top"> 
    <h1>Registration</h1> 
    <html:form name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm" method="get" scope="session"  action="/register.do" isRelative="true">
        <div id="register1Layer" class="divBox"><table>
                <COL width="5%"> 
                <COL width="25%"> 
                <COL width="70%">
            <tr>
                    <%--td  colspan="3" rowspan="11" width="5%">&nbsp;</td--%>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" colspan="2" align="left">Step 2 of 3
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="hdr_1" colspan="2" align="left">&nbsp;
                </td>
            </tr> 
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Name :&nbsp; 
                </td>
                <td align="left">
                    <html:text styleClass="inputvalue" name="RegisterForm" property="name" size="40" maxlength="50"/>   
                    <html:errors property="name"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Gender :&nbsp;
                </td>
                <td align="left">
                    
                    <html:radio styleClass="inputvalue" name="RegisterForm" property="gender" value="M" /> Male
                    <html:radio styleClass="inputvalue" name="RegisterForm" property="gender" value="F" /> Female
                    <!--input type="radio" name="gender" value="M"> Male
                    <input type="radio" name="gender" value="F"> Female  --> 
                    <html:errors property="gender"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Birthday :&nbsp;
                </td>
                <td align="left">
                    <html:select styleClass="inputvalue" name="RegisterForm" property="birthDay">
                        <%
                        Vector daysCombo = DateObjectHelper.getDaysCombo();
                        String[] daysString = (String[])daysCombo.get(0);
                        for(int i=0;i<daysString.length;i++)
                        {
                            out.print("<option value='" + daysString[i] +"'");
                            if (daysString[i].equals(registerForm.getBirthDay()))
                                out.print(" selected ");
                            out.print(">");
                            out.print(daysString[i] + "</option>");
                        }
                        %>
                    </html:select>
                    /
                    <html:select styleClass="inputvalue" name="RegisterForm" property="birthMonth">
                        <%
                        Vector monthsCombo = DateObjectHelper.getMonthsCombo();
                        String[] monthsLabels = (String[])monthsCombo.get(0);
                        String[] monthsValues = (String[])monthsCombo.get(1);
                        for(int i=0;i<monthsLabels.length;i++)
                        {
                            out.print("<option value='" + monthsValues[i] +"'"); 
                            if (monthsValues[i].equals(registerForm.getBirthMonth()))
                                out.print(" selected ");
                            out.print(">");
                            out.print(monthsLabels[i] + "</option>");
                        }
                        %>
                    </html:select>
                    /                    
                    <html:select styleClass="inputvalue" name="RegisterForm" property="birthYear">
                        <%
                        Vector yearsCombo = DateObjectHelper.getYearsCombo();
                        String[] yearsString = (String[])yearsCombo.get(0);
                        for(int i=0;i<yearsString.length;i++)
                        {
                            out.print("<option value='" + yearsString[i] +"'"); 
                            if (yearsString[i].equals(registerForm.getBirthYear()))
                                out.print(" selected ");
                            out.print(">");
                            out.print(yearsString[i] + "</option>");
                        }
                        %>
                    </html:select>
                    <%--<input class="date" type="text" id="birthdayString" name="birthdayString" size="20" maxlength="30" value="<%=request.getSession().getAttribute("birthdayString")%>">
                    <!--html:text style="date" name="RegisterForm" property="birthdayString" size="20" maxlength="30"/-->--%>   (day/month/year) 
                    <html:errors property="birthdayString"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">City :&nbsp;
                </td>
                <td align="left">
                    <html:text styleClass="inputvalue" name="RegisterForm" property="city" size="20" maxlength="30"/>    
                    <html:errors property="city"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">State :&nbsp;
                </td>
                <td align="left">
                     <html:select styleClass="inputvalue" name="RegisterForm" property="state">
                        <option value="NA">Please Select</option>
                        <%
                        for(int i=0;i<stateList.size();i++)
                        {  
                            String[] state = (String[])stateList.get(i);
                            out.print("<option value='" + state[0] +"'"); 
                            if (state[0].equals(registerForm.getState()))
                                out.print(" selected ");
                            out.print(">");
                            out.println(state[1] + "</option>");
                        }
                        %>
                    </html:select>       
                    <html:errors property="state"/>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Country :&nbsp;
                </td>
                <td align="left">
                     <html:select styleClass="inputvalue" name="RegisterForm" property="nationality">
                        <%
                        for(int i=0;i<countryList.size();i++)
                        {  
                            String[] country = (String[])countryList.get(i);
                            out.print("<option value='" + country[0] +"'"); 
                            if (country[0].equals(registerForm.getNationality()))
                                out.print(" selected ");
                            out.print(">");
                            out.println(country[1] + "</option>");
                        }
                        %>
                    </html:select>     
                    <html:errors property="nationality"/> 
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <input class="formbuttons" type="button" name="back" value="Back" onclick="formBack('main');">
                    <input class="formbuttons" type="button" name="continue" value="Continue" onclick="formSubmit();"> 
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;
                </td> 
            </tr> 
        </table></div>
        <input type="hidden" name="act" value="two">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>"> 
    </html:form>     
            </td>
            <td width="5%">&nbsp;</td> 
