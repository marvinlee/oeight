<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*" %>
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.RegisterForm"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.EmailProviderComboHelper" %>

<%
    RegisterForm registerForm = (RegisterForm)session.getAttribute("RegisterForm"); 
    Vector providerList = EmailProviderComboHelper.getProviderList(); 
%>
<%-- This page needs total 3 columns instead of 4--%> 
    <td width="5%">&nbsp;</td> 
    <td width="80%" valign="top"> 
    <h1>Registration</h1> 
 <html:form name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm" method="post" scope="session" action="/register.do" isRelative="true">
        <div id="register1Layer" class="divBox"><table>
                <COL width="5%"> 
                <COL width="25%"> 
                <COL width="70%">
                <tr>
                    <%--td  colspan="3" rowspan="11" width="5%">&nbsp;</td--%>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>   
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" colspan="2" align="left">You are now a registered 080808 member. 
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" colspan="2" align="left">For <b>080808 Wedding</b>, register here : <a href="/wedding.do?act=register" target="_blank">Yes, I wed on 080808!</a>
                        <br><br><br>
                    </td>
                </tr> 
                <%if (PropertyConstants.BOOLEAN_YES != registerForm.getEmailValidated()){%>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left">
                        A validation link has been sent to your email at <b><%=registerForm.getEmailAddress()%></b>, you need to click on the link to validate your profile.<br>                        
                        <br><br>
                    </td>
                </tr>   
                <%}%>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" colspan="2" align="left">Search and invite friends now
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                        Select friends in your contact list who have joined to add as buddy and invite the others:<br><br>
                        <table border="0">
                            <tr>
                                <td class="inputlabel" align="left">Email</td>
                                <td class="inputlabel" align="left">Password</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="emailUser" maxlength="100">&nbsp;
                                <select name="provider">
                                         <%
                                    for(int i=0;i<providerList.size();i++)
                                    {  
                                        String[] provider = (String[])providerList.get(i);
                                        out.print("<option value='" + provider[0] +"'>");
                                        out.println(provider[1] + "</option>");
                                    }
                                    %></select>&nbsp;
                                </td>
                                <td><input type="password" name="password" maxlength="100"  onkeydown="if(event.keyCode==13)formGo('/invite.do','contactSelect');" >&nbsp;<br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                <i>Your password is not stored and is used for this search only.</i></td> 
                            </tr>
                            
                            <tr align="center">
                                <td colspan="2">
                                <input class="formbuttons" type="button" name="search" value="Search" onclick="formGo('/invite.do','contactSelect');"></td> 
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                        <%--You may now proceed to <a href="/profile.do?act=mainEdit">update</a> your profile details to add any additional details to your profile.<br>
                        Photos in the mosaic is ranked by votes. The more votes you get, the higher your photo will rank.<br>--%>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left">
                        <br><br>
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                        IMPORTANT : <br>
                        Only validated profile (non-spam members) can view members profile.<br>
                        Your photo will only appear in the mosaic if you have uploaded a photo.<br>
                        You may vote for yourself once daily. The more votes you get, the higher your photo will rank.<br>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;
                    </td>
                    <td><br><br><br><input class="formbuttons" type="button" name="updateProfile" value="Update Profile" onclick="formGo('/profile.do','mainEdit');">
                        <input class="formbuttons" type="button" name="viewProfile" value="View Profile" onclick="formGo('/profile.do','view');">
                        <%--input class="formbuttons" type="button" name="skip" value="Skip" onclick="formSkip();"--%>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left">
                        <br><br><br>
                    </td>
                </tr> 
                <%--<tr>
                <td class="hdr_m" colspan="2" align="left">Activation :  Kindly check your email, we have sent an activation link to the email address you have provided. 
                &nbsp; 
                </td> 
            </tr>
            <tr>
                <td class="hdr_m" colspan="2" align="left">Share your personal 080808 page with your friends, make your page the most popular and stand-out in the podium. 
                &nbsp; 
                </td> 
            </tr> 
            <tr>
                <td class="lbl" align="right">Email 1 :&nbsp;
                </td>
                <td align="left">
                    <html:text name="RegisterForm" property="emailFwd1" size="40" maxlength="50"/>   
                    <br>
                    <html:errors property="emailFwd1"/> 
                </td>
            </tr>
            <tr>
                <td class="lbl" align="right">Email 2 :&nbsp;
                </td>
                <td align="left">
                    <html:text name="RegisterForm" property="emailFwd2" size="40" maxlength="50"/>   
                    <br>
                    <html:errors property="emailFwd2"/> 
                </td>
            </tr>
            <tr>
                <td class="lbl" align="right">Email 3 :&nbsp;
                </td>
                <td align="left">
                    <html:text name="RegisterForm" property="emailFwd3" size="40" maxlength="50"/>   
                    <br>
                    <html:errors property="emailFwd3"/> 
                </td>
            </tr>
            <tr>
                <td class="lbl" colspan="2" align="left">  Notes:<br>

We respect online privacy and we do not send spam emails. You may opt to send personal email to your friends to refer to your personal page at http://080808.com.my/<OEight Id>
 
                </td> 
            </tr> 
            <tr>
                <td>&nbsp;
                </td>
                <td><input type="button" name="send" value="Send" onclick="formSubmit();">
                </td>
            </tr>--%> 
        </table></div>
        <input type="hidden" name="act" value="four">
        <%--input type="hidden" name="token" value="<%=request.getAttribute("token")%>"--%>
    </html:form>   
            </td>
            <td width="5%">&nbsp;</td>