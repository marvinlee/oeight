 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>    
  
<%-- This page needs total 3 columns instead of 4--%>  
            <td width="5%">&nbsp;</td>
            
            <td width="80%" valign="top">
                
                <h1>Validate Profile</h1>
                
                <!--bean:define id="RegisterForm" name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm"/-->
                <html:form name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm" method="post" scope="session"  action="/register.do" isRelative="true">
                    <!--form name="RegisterForm" method="post" action="/register.do"-->
                    <div id="register1Layer" class="divBox">  
                        <table>  
                            <tr>
                                <td>&nbsp;<br> 
                                    <html:errors property="validateError"/>
                                    <html:errors property="validateSuccess"/><br><br>
                                </td>
                            </tr>
                        </table>
                    </div>
                   
                </html:form>  
            </td>
            <td width="5%">&nbsp;</td> 
