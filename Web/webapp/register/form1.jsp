<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@page import="com.esmart2u.solution.web.struts.service.Recaptcha4JService"%> 
  
<%-- This page needs total 3 columns instead of 4--%>  
            <td width="5%">&nbsp;</td>
            
            <td width="80%" valign="top">
                <h1 style="color: red">Registration is now closed</h1>
                <%--
                <h1>Registration</h1>
                
                <!--bean:define id="RegisterForm" name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm"/-->
                <html:form name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm" method="post" scope="session"  action="/register.do" isRelative="true">
                    <!--form name="RegisterForm" method="post" action="/register.do"-->
                    <div id="register1Layer" class="divBox">  
                        <table> 
                            <COL width="5%"> 
                            <COL width="25%"> 
                            <COL width="70%">
                            <tr>
                                <td  colspan="3" width="5%">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" colspan="2" align="left">Step 1 of 3
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="hdr_m" colspan="2" align="left">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Referrer :&nbsp; 
                                </td>
                                <td align="left">
                                    <bean:write name="RegisterForm" property="referralName" />
                                    <html:errors property="referral"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">oEight Id :&nbsp; 
                                </td>
                                <td align="left">
                                    <html:text styleClass="inputvalue" name="RegisterForm" property="userName" size="20" maxlength="12"/> 
                                    Reserve your own oEight Id.<br> Your personal page will be :
                                    <b>http://profile.080808.com.my/&lt;oEight Id&gt;</b>
                                    <br>
                                    Hint: Username should be between 4 and 12 characters without any special characters. It should not be all numbers.
                                    <br>
                                    <html:errors property="userName"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Email :&nbsp;
                                </td>
                                <td align="left">
                                    <html:text styleClass="inputvalue" name="RegisterForm" property="emailAddress" size="20" maxlength="50"/>
                                    <!--input type="text" name="emailAddress" size="20" maxlength="50"-->
                    Email address is used to login, validate your membership and 
                    to inform you of this project updates, contest or freebies.
                                    <br>
                                    <html:errors property="emailAddress"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Password :&nbsp;
                                </td>
                                <td align="left">
                                    <html:password styleClass="inputvalue" name="RegisterForm" property="password" size="20" maxlength="12"/> 
                                    4 to 12 characters
                                    <html:errors property="password"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Confirm Password :&nbsp;
                                </td>
                                <td align="left">
                                    <html:password styleClass="inputvalue" name="RegisterForm" property="confirmPassword" size="20" maxlength="12"/> 
                                    4 to 12 characters
                                    <html:errors property="confirmPassword"/><html:errors property="confirmPasswordMismatch"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Spam Filter :&nbsp;
                                </td>
                                <td align="left">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">&nbsp; 
                                    <%if (Recaptcha4JService.isEnabled()){%>
                                    <!-- recaptcha block -->
                                    <%= Recaptcha4JService.getReCaptchaInstance().createRecaptchaHtml(null, null) %> 
                                    <%}%><html:errors property="recaptchaResponse"/>
                                    <a href="javascript:Recaptcha.reload()">Refresh words</a>
                                </td> 
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><input type="button" class="formbuttons" name="continue" value="Continue" onclick="formSubmit();">
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">&nbsp;
                                </td>
                            </tr>
                            
                    </table></div>
                    <input type="hidden" name="act" value="one"> 
                    <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
                </html:form>   --%>
            </td>
            <td width="5%">&nbsp;</td> 
