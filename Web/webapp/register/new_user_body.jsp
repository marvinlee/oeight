<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@ page import="java.util.*" %>
<%@page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@page import="com.esmart2u.solution.web.struts.service.Recaptcha4JService"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.RegisterForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.member.web.struts.helper.StateComboHelper,
                 com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper"%>    
  
<%
    RegisterForm registerForm = (RegisterForm)session.getAttribute("RegisterForm");
    if (!StringUtils.hasValue(registerForm.getNationality()))
    {
        registerForm.setNationality(PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY));
    } 
    
    Vector countryList = CountryComboHelper.getCountryList();
    Vector stateList = StateComboHelper.getStateList();
%> 
<%-- This page needs total 3 columns instead of 4--%>  
            <td width="5%">&nbsp;</td>
            
            <td width="80%" valign="top">
                
                <h1 style="color: red">Registration is now closed</h1>
                <%--
                <h1>Registration</h1>
                
                <!--bean:define id="RegisterForm" name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm"/-->
                <html:form name="RegisterForm" type="com.esmart2u.oeight.member.web.struts.controller.RegisterForm" method="post" scope="session"  action="/register.do" isRelative="true" enctype="multipart/form-data">
                    <!--form name="RegisterForm" method="post" action="/register.do"-->
                    <div id="register1Layer" class="divBox">  
                        <table> 
                            <COL width="5%"> 
                            <COL width="25%"> 
                            <COL width="70%">
                            <tr>
                                <td  colspan="3" width="5%">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" colspan="2" align="left">
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="hdr_m" colspan="2" align="left">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Referrer :&nbsp; 
                                </td>
                                <td align="left">
                                    <bean:write name="RegisterForm" property="referralName" />
                                    <html:errors property="referral"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">oEight Id :&nbsp; 
                                </td>
                                <td align="left">
                                    <html:text styleClass="inputvalue" name="RegisterForm" property="userName" size="20" maxlength="12"/> 
                                     Reserve your own oEight Id.<br> Your personal page will be :
                                    <b>http://profile.080808.com.my/&lt;oEight Id&gt;</b>
                                    <br>
                                    Hint: Username should be between 4 and 12 characters without any special characters. It should not be all numbers.
                                    <br>
                                    <html:errors property="userName"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Email :&nbsp;
                                </td>
                                <td align="left">
                                    <html:text styleClass="inputvalue" name="RegisterForm" property="emailAddress" size="20" maxlength="50"/>
                                    <!--input type="text" name="emailAddress" size="20" maxlength="50"-->
                    Email address is used to login, validate your membership and 
                    to inform you of this project updates, contest or freebies.
                                    <br>
                                    <html:errors property="emailAddress"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Password :&nbsp;
                                </td>
                                <td align="left">
                                    <html:password styleClass="inputvalue" name="RegisterForm" property="password" size="20" maxlength="12"/> 
                                    4 to 12 characters
                                    <html:errors property="password"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Confirm Password :&nbsp;
                                </td>
                                <td align="left">
                                    <html:password styleClass="inputvalue" name="RegisterForm" property="confirmPassword" size="20" maxlength="12"/> 
                                    4 to 12 characters
                                    <html:errors property="confirmPassword"/><html:errors property="confirmPasswordMismatch"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">Spam Filter :&nbsp;
                                </td>
                                <td align="left">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">&nbsp; 
                                    <%if (Recaptcha4JService.isEnabled()){%>
                                    <!-- recaptcha block -->
                                    <%= Recaptcha4JService.getReCaptchaInstance().createRecaptchaHtml(null, null) %> 
                                    <%}%><html:errors property="recaptchaResponse"/>
                                </td> 
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">&nbsp;
                                </td>
                                <td align="left">
                                    <a href="javascript:Recaptcha.reload()">Change words</a></td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right" valign="top">&nbsp;
                                </td>
                                <td align="left"><br></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right">Name :&nbsp; 
                                </td>
                                <td align="left">
                                    <html:text styleClass="inputvalue" name="RegisterForm" property="name" size="40" maxlength="50"/>   
                                    <html:errors property="name"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right">Gender :&nbsp;
                                </td>
                                <td align="left">

                                    <html:radio styleClass="inputvalue" name="RegisterForm" property="gender" value="M" /> Male
                                    <html:radio styleClass="inputvalue" name="RegisterForm" property="gender" value="F" /> Female
                                    <!--input type="radio" name="gender" value="M"> Male
                                    <input type="radio" name="gender" value="F"> Female  --> 
                                    <html:errors property="gender"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right">Birthday :&nbsp;
                                </td>
                                <td align="left">
                                    <html:select styleClass="inputvalue" name="RegisterForm" property="birthDay">
                                        <%
                                        Vector daysCombo = DateObjectHelper.getDaysCombo();
                                        String[] daysString = (String[])daysCombo.get(0);
                                        for(int i=0;i<daysString.length;i++)
                                        {
                                            out.print("<option value='" + daysString[i] +"'");
                                            if (daysString[i].equals(registerForm.getBirthDay()))
                                                out.print(" selected ");
                                            out.print(">");
                                            out.print(daysString[i] + "</option>");
                                        }
                                        %>
                                    </html:select>
                                    /
                                    <html:select styleClass="inputvalue" name="RegisterForm" property="birthMonth">
                                        <%
                                        Vector monthsCombo = DateObjectHelper.getMonthsCombo();
                                        String[] monthsLabels = (String[])monthsCombo.get(0);
                                        String[] monthsValues = (String[])monthsCombo.get(1);
                                        for(int i=0;i<monthsLabels.length;i++)
                                        {
                                            out.print("<option value='" + monthsValues[i] +"'"); 
                                            if (monthsValues[i].equals(registerForm.getBirthMonth()))
                                                out.print(" selected ");
                                            out.print(">");
                                            out.print(monthsLabels[i] + "</option>");
                                        }
                                        %>
                                    </html:select>
                                    /                    
                                    <html:select styleClass="inputvalue" name="RegisterForm" property="birthYear">
                                        <%
                                        Vector yearsCombo = DateObjectHelper.getYearsCombo();
                                        String[] yearsString = (String[])yearsCombo.get(0);
                                        for(int i=0;i<yearsString.length;i++)
                                        {
                                            out.print("<option value='" + yearsString[i] +"'"); 
                                            if (yearsString[i].equals(registerForm.getBirthYear()))
                                                out.print(" selected ");
                                            out.print(">");
                                            out.print(yearsString[i] + "</option>");
                                        }
                                        %>
                                    </html:select>
                                       (day/month/year) 
                                    <html:errors property="birthdayString"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right">City :&nbsp;
                                </td>
                                <td align="left">
                                    <html:text styleClass="inputvalue" name="RegisterForm" property="city" size="20" maxlength="30"/>    
                                    <html:errors property="city"/>
                                </td>
                            </tr> 
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right">Country :&nbsp;
                                </td>
                                <td align="left">
                                     <html:select styleClass="inputvalue" name="RegisterForm" property="nationality">
                                        <%
                                        for(int i=0;i<countryList.size();i++)
                                        {  
                                            String[] country = (String[])countryList.get(i);
                                            out.print("<option value='" + country[0] +"'"); 
                                            if (country[0].equals(registerForm.getNationality()))
                                                out.print(" selected ");
                                            out.print(">");
                                            out.println(country[1] + "</option>");
                                        }
                                        %>
                                    </html:select>     
                                    <html:errors property="nationality"/> 
                                </td>
                            </tr>   
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td class="hdr_1" colspan="2" align="left">&nbsp;
                                </td>
                            </tr> 
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right">Photo Upload :&nbsp; 
                                </td>
                                <td align="left">
                                    <html:file styleClass="inputvalue" name="RegisterForm" property="photoFile"  onchange="preview(this)"/>   
                                    <html:errors property="photoPath"/>  
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputvalue" align="right">*oEight Tips :&nbsp; 
                                </td>
                                <td align="left"> 
                                    This photo will be displayed on our 080808 mosaic after submission. Photo file can be of BMP, GIF or JPG.          
                                    Your photo file size should not exceed 250Kb. We recommend a square photo with at least 160px(width) and 160px(height).<br>
                                    You can use any imaging tools, eg MsPaint and resize it with "Stretch / Scale" to reduce the actual size.
                                       </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right">Photo Preview :&nbsp;
                                </td>
                                <td align="left"> 
                                    <img alt="Photo preview" id="previewField" src="/images/spacer.gif"> 
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputlabel" align="right">Photo Description :&nbsp;
                                </td>
                                <td align="left">
                                    <html:textarea styleClass="inputvalue" name="RegisterForm" property="photoDescription" cols="50" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                                    <br>
                                    <html:errors property="photoDescription"/>  
                                </td>
                            </tr>  
                            <tr>
                                <td>&nbsp;</td>
                                <td class="hdr_1" colspan="2" align="left">&nbsp;
                                </td>
                            </tr> 
                            <tr>
                                <td>&nbsp;</td>
                                <td class="inputvalue" colspan="2" align="left">
                                     Your registration will complete by clicking on 'Join' below and you confirm that you are over 13 years of age and <br>accept the <a href="javascript:openNewWin('/info/terms_of_use.htm');">Terms Of Use</a>.<br>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><input type="button" class="formbuttons" name="continue" value="   Join   " onclick="formSubmit();">
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">&nbsp;
                                </td>
                            </tr>
                            
                    </table></div>
                    <input type="hidden" name="act" value="submit">
                    <input type="hidden" name="skip3" value="N"> 
                    <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
                </html:form>     --%>
            </td>
            <td width="5%">&nbsp;</td> 
