<script language="javascript">  
var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("userName", "oEight Id", "M", true);
	fieldList[counter++] = new Array("emailAddress", "Email", "M", true); 
	fieldList[counter++] = new Array("password", "Password", "M", true); 
	fieldList[counter++] = new Array("confirmPassword", "Confirm Password", "M", true); 
	fieldList[counter++] = new Array("name", "Name", "M", true);
	fieldList[counter++] = new Array("gender", "Gender", "M", true); 
	fieldList[counter++] = new Array("birthDay", "Birthday - day", "M", true); 
	fieldList[counter++] = new Array("birthMonth", "Birthday - month", "M", true); 
	fieldList[counter++] = new Array("birthYear", "Birthday - year", "M", true); 
	fieldList[counter++] = new Array("city", "City", "M", true); 
	//fieldList[counter++] = new Array("state", "State", "M", true); 
	fieldList[counter++] = new Array("nationality", "Country", "M", true); 
	//fieldList[counter++] = new Array("photoFile", "Photo Upload", "M", true);    

	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);
	var emailMsg = checkEmail(form.emailAddress.value, "Email")
	if (emailMsg != '')
	{
		if (errMsg != '')
		{
			errMsg += "\n"
		}
		errMsg += emailMsg
	}

	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	}

        if (document.forms[0].photoFile.value == "")
        {
            document.forms[0].skip3.value = "Y";
        }

		posted = true;
		document.forms[0].submit();
}


/***** CUSTOMIZE THESE VARIABLES *****/

  // width to resize large images to
var maxWidth=100;
  // height to resize large images to
var maxHeight=100;
  // valid file types
var fileTypes=["bmp","gif","png","jpg","jpeg"];
  // the id of the preview image tag
var outImage="previewField";
  // what to display when the image is not valid
var defaultPic="spacer.gif";

/***** DO NOT EDIT BELOW *****/

function preview(what){
  var source=what.value;
  var ext=source.substring(source.lastIndexOf(".")+1,source.length).toLowerCase();
  for (var i=0; i<fileTypes.length; i++) if (fileTypes[i]==ext) break;
  globalPic=new Image();
  if (i<fileTypes.length) globalPic.src=source;
  else {
    globalPic.src=defaultPic;
    alert("THAT IS NOT A VALID IMAGE\nPlease load an image with an extention of one of the following:\n\n"+fileTypes.join(", "));
  }
  setTimeout("applyChanges()",200);
}
var globalPic;
function applyChanges(){
  var field=document.getElementById(outImage);
  var x=parseInt(globalPic.width);
  var y=parseInt(globalPic.height);
  if (x>maxWidth) {
    y*=maxWidth/x;
    x=maxWidth;
  }
  if (y>maxHeight) {
    x*=maxHeight/y;
    y=maxHeight;
  }
  field.style.display=(x<1 || y<1)?"none":"";
  field.src=globalPic.src;
  field.width=x;
  field.height=y;
}
// End --> 
 
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 