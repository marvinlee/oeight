<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%>
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.MosaicHelper"%>
<%@ page import="com.esmart2u.oeight.data.Mosaic"%>

<%-- This page needs total 2 columns instead of 4--%>
    <td colspan="4">
<form name="mForm">
        <table width="100%">
            <tr><td   valign="top">
<%-- Content below onwards --%>                

<%if (session!=null && StringUtils.hasValue((String)session.getAttribute("userId"))){%>        
&nbsp;&nbsp;&nbsp;&nbsp;Find the location of photo here : <input class="inputvalue" type="text" id="8id" name="8id" onclick="resetLabel();" onkeydown="if(event.keyCode==13){return false;}" >
<input class="formbuttons" type="button" name="Locate" value="Locate" onclick="locateItId();"> Key in oEightId and click "Locate"
 <%}else
{%> 
&nbsp;&nbsp;&nbsp;&nbsp;View MORE by <a href="/register.do?act=invited">joining</a> us here. Registered members will be able to view photo and profile details.<br><br>
<%}%>
<div id=layerMain style="position:relative;top:0; left:0; ">


<div id="canvas1" class="icongroup1"><br><br><br><center>Loading mosaic...<br><img src="/images/loading.gif"></center>
</div>
<span id="canvas1-title" class="iconspan"></span> 
<%--<div class="eg-bar" onclick="javascript:canvas.sweepToggle('expand',count)" align="right">
	<span id="canvas1-title" class="iconspan"></span>Show more
</div>
<div class="eg-bar" onclick="javascript:canvas.sweepToggleAll('expand',count)" align="right">
	Show all
</div>--%>


<script type="text/javascript">
var count = 1;
var canvas=new switchicon("icongroup1", "div") //Limit scanning of switch contents to just "div" elements
<%--//oeight.setHeader('<img src="minus.gif" />', '<img src="plus.gif" />') //set icon HTML--%>
canvas.setHeader(' ', ' ') //set icon HTML
<%--//oeight.collapsePrevious(true) //Allow only 1 content open at any time--%>
canvas.setPersist(true, 7) //No persistence enabled
canvas.defaultExpanded(0) //Set 1st content to be expanded by default
canvas.init(); 
<%--canvas.sweepToggle('expand',count);--%>
</script>

<%
//Get counter from DB, everytime generate new map and update table
  int totalMosaics= MosaicHelper.getCurrentTotalMosaics();
  List mosaics = MosaicHelper.getCurrentMosaics();  
  String mapPath = "";
  // There is only a single Map file
  for(int i=0;i<mosaics.size();i++){
    Mosaic mosaic = (Mosaic)mosaics.get(i); 
    //out.println("'"+mosaic.getMapFilePath()+"'");
    mapPath = "/map/" + mosaic.getMapFilePath().trim();
 }
%>

<%if (session!=null && StringUtils.hasValue((String)session.getAttribute("userId"))){%>   
<jsp:include page="<%=mapPath%>" flush="true" />
<%}%>

</div><!-- div layerMain -->

<%-- The maxId below to have a counter of total mosaics, so client side cannot exceed --%>

<%
  for(int i=0;i<mosaics.size();i++){
    Mosaic mosaic = (Mosaic)mosaics.get(i);  
    String mosaicPath = "/vmosaic/" + mosaic.getPictureLocalPath().trim();
 %>   
<input type="hidden" name="mosaic<%=i+1%>" value="<%=mosaicPath%>">
<%}%>

<input type="hidden" name="maxId" value="<%=totalMosaics%>">

				</td>  
            </tr>
        </table>
</form>
    </td>                                