
<!-- Lightbox -->
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />  
<script type="text/javascript" src="js/switchcontent.js" ></script> 
<script type="text/javascript" src="js/switchicon.js"></script> 
<script>

function locateIt(){

	var canvasLayer = document.getElementById("canvas1")
	var oriString = canvasLayer.innerHTML.toLowerCase()
	var divIndex = oriString.indexOf("<div id=layer1");
	var appendedString = "<div id=layer1 style='position:absolute; top:0; left:0; width:120; height:120; z-index:1; padding:0px; border: #FF0000 1px solid;'>";
	if (divIndex > 0){
		oriString = canvasLayer.innerHTML.substring(0,divIndex)
	}
	canvasLayer.innerHTML = oriString + appendedString;
	//alert(canvasLayer.innerHTML)
	var coors = findPos("canvas1");
	//var coors = findPos(obj);
	//var x = document.getElementById(lyr);
	var x = document.getElementById("layer1");
	var topOffset = parseInt(coors[1])
	var leftOffset = parseInt(coors[0])
	//x.style.top = topOffset + 'px';
	x.style.top = topOffset + 'px';
	x.style.left = leftOffset + 'px';
	x.focus()
}

function findPos(obj)
{
	var curleft = curtop = 0;
	if (obj.offsetParent) {
		curleft = obj.offsetLeft
		curtop = obj.offsetTop
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		}
	}
	curleft += document.getElementById("x").value
	curtop += document.getElementById("y").value
	//alert("Left"  + curleft + " Top " + curtop);
	return [curleft,curtop];
}

function locateItId()
{  
	var coords
        var keyInId = document.getElementById("8id").value;
        current8id = keyInId;
        var hasValue = true; 
        
        if (keyInId == "" || keyInId.length ==0)
        { 
            hasValue = false;
        }
        
	var mapImg = document.getElementById(keyInId)
	//alert(mapImg)
	//alert(mapImg.coords)
	if (mapImg !="undefined" && mapImg != null && hasValue)
	{ 
                // Try search for the id first
                var canvasLayer = document.getElementById("canvas1")
                var oriString = canvasLayer.innerHTML.toLowerCase()
                var divIndex = oriString.indexOf("<div id=");
                var appendedString = "<div id='layer1' onmouseover='hideLocator()' style='background:transparent;position:absolute; top:0; left:0; width:38; height:38; z-index:1; padding:0px; border: #FF0000 3px solid;'>";
                
                if (divIndex > 0){
                        oriString = canvasLayer.innerHTML.substring(0,divIndex)
                }
                canvasLayer.innerHTML = oriString + appendedString;
		coords = mapImg.coords.split(',')
		var x = document.getElementById("layer1");
		var topOffset = parseInt(coords[1])
		var leftOffset = parseInt(coords[0])
		x.style.top = topOffset + 'px';
		x.style.left = leftOffset + 'px';
		x.style.width = '38px'; 
		x.style.height = '38px'; 
		x.focus()

	}
        else
        { 
            document.getElementById("8id").value = keyInId + " - Not Found"
        }
 
}

var current8id = "undefined";

function resetLabel()
{
    if (current8id != "undefined" && current8id != ""){
        document.getElementById("8id").value = current8id;
    }
}

var locatorLyr = document.getElementById("layer1");
var blinkCount = 0; 
function hideLocator(){  
blinkCount=0
setTimeout("hideText()", 300);
}
function blinkText()
{
var locatorLyr = document.getElementById("layer1");
locatorLyr.style.visibility = 'visible';
setTimeout("hideText()", 700);
blinkCount++
}
function hideText()
{
var locatorLyr = document.getElementById("layer1");
locatorLyr.style.visibility = 'hidden';
blinkCount++
if (blinkCount <5){
setTimeout("blinkText()", 300);}
}

</script>


<style type="text/css">

/*Default style for SPAN icons. Edit if desired: */

.iconspan{
float: right;
margin: 3px;
cursor:hand;
cursor:pointer;
font-weight: bold;
}

/*CSS used to style the examples. Remove if desired: */

.eg-bar{
background-color: #EEF5D3;
vertical-align : top;
horizontal-align : right;
font-weight: bold;
border: 1px solid black;
padding: 1px;
}

div.eg-bar{
//width: 500px;
width: 95%;
}

.icongroup1{
width: 500px;
}

</style>