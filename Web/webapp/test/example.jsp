<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<tiles:insert page="/test/template.jsp" flush="true">
   <tiles:put name="title" type="string" value="Welcome" />
   <tiles:put name="header" value="/test/top.jsp" />
   <tiles:put name="menu" value="/test/left.jsp" />
   <tiles:put name="body" value="/test/content.jsp" />
   <tiles:put name="bottom" value="/test/bottom.jsp" /> 
</tiles:insert>