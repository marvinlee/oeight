<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<table width="670" cellSpacing=0 cellPadding=2 border=1 bgcolor="#FFFFCC">
    <tr>
      <th width=70 bgColor=white align=center class="listbody">Name</th>
	  <th width=70 bgColor=white align=center class="listbody">Group</th>
	  <th width=200 bgColor=white align=center class="listbody">Class</th>
	  <th width=165 bgColor=white align=center class="listbody">Last Run</th>
	  <th width=165 bgColor=white align=center class="listbody">Next Run </th>
	  <th width=165 bgColor=white align=center class="listbody">Cron </th>
	</tr>
	<logic:present name="session_jobList">
          <logic:iterate id="jobList" scope="session" name="session_jobList" indexId="idx">
          <tr>
	          <td class="listbody" align="center" width=70>&nbsp;
                  <a href='job.do?action=Choose&idx=<bean:write name="idx"/>'  class="listbody"> 
                    <bean:write name='jobList' property='jobName'/>
                  </a>
	          </td>
	          <td class="listbody" align="center" width=70>
	          <bean:write name='jobList' property='jobGroup'/>
	          </td>		
	          <td class="listbody" align="center" width=200>
	          <bean:write name='jobList' property='jobClass'/>
	          </td>
	          <td class="listbody" align="center" width=165>
	          <bean:write name='jobList' property='lastRun'/>
	          </td>		
	          <td class="listbody" align="center" width=165>
	          <bean:write name='jobList' property='nextRun'/>
	          </td>	
	          <td class="listbody" align="center" width=165>
	          <bean:write name='jobList' property='cronExpression'/>
	          </td>
	        </tr>
          </logic:iterate>
    </logic:present>
</table>