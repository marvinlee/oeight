<%@ page import="org.apache.struts.action.Action" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html:html locale="true">
<head>
<meta http-equiv="expires" content="Tue, 20 Aug 1996 01:00:00 GMT">
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" href="../styles/x2_style.css">
<script language="javascript">
 	function isEmpty(frmElement)
 	{
		var value = frmElement.value;

		//Remove any beginning or ending spaces
		value = value.replace(/\ /g, "");

		if(value == ""){
		return true;
		}
		return false;
	}

	function confirmReschedule(f)
	{ 	
	    var jobName = f.jobName.value;
		var jobGroup = f.jobGroup.value;
		var jobClass = f.jobClass.value;
		var jobFrequency = f.jobFrequency.value;
		var hour = f.hour.value;	
		var jobNameKey = f.jobNameKey.value;
		var jobGroupKey = f.jobGroupKey.value;	
		var jobClassKey= f.jobClassKey.value;
		
		if((jobName != jobNameKey))
	    {
		    alert("You cannot change the Job Name during reschedule");
		    f.jobName.value = f.jobNameKey.value;
	      	f.jobGroup.focus();
	      	return false;
    	}
    	
    	if((jobGroup != jobGroupKey))
	    {
		    alert("You cannot change the Job Group during reschedule");
		    f.jobGroup.value = f.jobGroupKey.value;
	      	f.jobGroup.focus();
	      	return false;
    	}
    	
    	if((jobClass != jobClassKey))
	    {
		    alert("You cannot change the Job Class during reschedule");
		    f.jobClass.value = f.jobClassKey.value;
	      	f.jobClass.focus();
	      	return false;
    	}
    	
		if ( isEmpty(f.jobName) )
	    {
	      alert("The Job Name: field is required");
	      f.jobName.focus();
	      return false;
	    }
	    if ( isEmpty(f.jobGroup) )
	    {
	      alert("The Job Group: field is required");
	      f.jobGroup.focus();
	      return false;
	    }
	    	        	
	    if ( isEmpty(f.jobClass) )
	    {
	      alert("The Job Class: field is required");
	      f.jobClass.focus();
	      return false;
	    }
	    if ( isEmpty(f.hour) )
	    {
	      alert("The Hour: field is required");
	      f.hour.focus();
	      return false;
	    }
	    if ( isEmpty(f.jobFrequency) )
	    {
	      alert("The Day of week: field is required");
	      f.jobFrequency.focus();
	      return false;
	    }
	    
	     if(!(confirm("Are you sure you want to Reschedule this job?")))
		 {
		    return false;
		 }		 
	   
     	return true; 
 	}

 	function validateAdd(f)
	{ 	
	    var jobName = f.jobName.value;
		var jobGroup = f.jobGroup.value;
		var jobNameKey = f.jobNameKey.value;
		var jobGroupKey = f.jobGroupKey.value;
		var jobClass = f.jobClass.value;
		var jobFrequency = f.jobFrequency.value;
		var hour = f.hour.value;		

		if ( isEmpty(f.jobName) )
	    {
	      alert("The Job Name: field is required");
	      f.jobName.focus();
	      return false;
	    }
	    if ( isEmpty(f.jobGroup) )
	    {
	      alert("The Job Group: field is required");
	      f.jobGroup.focus();
	      return false;
	    }
	    
	    if( (jobName == jobNameKey) && (jobGroup == jobGroupKey))
	    {
		    alert("Another Job with same Name and Group already exists. Please select a different Name and Group combination");
	      	f.jobGroup.focus();
	      	return false;
    	}
	    
	    if ( isEmpty(f.jobClass) )
	    {
	      alert("The Job Class: field is required");
	      f.jobClass.focus();
	      return false;
	    }
	    if ( isEmpty(f.hour) )
	    {
	      alert("The Hour: field is required");
	      f.hour.focus();
	      return false;
	    }
	    if ( isEmpty(f.jobFrequency) )
	    {
	      alert("The Day of week: field is required");
	      f.jobFrequency.focus();
	      return false;
	    }
	    
	    if ( isEmpty(f.triggerName) )
	    {
	      alert("The Trigger Name: field is required");
	      f.triggerName.focus();
	      return false;
	    }
     	return true; 
 	}
 	
	function confirmDelete()
	{
		  if(confirm("Are you sure you want to delete this job?"))
		  {
		    return true;
		  }
		  else
		  {
		    return false;
		  }
	}
</script>
</head>
<body>
<div align="center">
<table width="780" border="0" cellspacing="0" cellpadding="0">
    <tr>
		<td width="150" valign="top" class="backgroundbg">
			<table align="center">
		       <tr><td><a href="job.do">List Jobs</a></td></tr>
		    </table>		    
		</td>
		<!--- Left navigation ends -->
     	<td class="listbody" width="670" valign="top" bgcolor="#FFFFCC">
      	   <table align="center">
				<tr>
					<td><H3>Jobs</H3></td>
				</tr>
				<b><html:errors/></b>
				<!-- Data table starts -->
				<tr>
			 		<td><jsp:include page="joblist.jsp" flush="true"/></td>
				</tr>
		<!-- form -->
				<html:form  action="/scheduler/job" styleId="JobForm" type="com.esmart2u.oeight.admin.scheduler.struts.JobForm" method="post">
					<tr>
						<td>
							<table width="100%" bgcolor="FFFFCC">
							   <tr>
							     <td align="right">Job Name:</td>
					       		 <td align="left"><html:text name="JobForm" property="jobName" size="10" maxlength="11"/>
					       		 	<input type="hidden" name="jobNameKey" value="<bean:write name='JobForm' property='jobName'/>">
								 </td>		
								 <td align="right">Job Group:</td>
					       		 <td align="left"><html:text name="JobForm" property="jobGroup" size="10" maxlength="11"/>
					       		 	<input type="hidden" name="jobGroupKey" value="<bean:write name='JobForm' property='jobGroup'/>">
								 </td>
								<td align="right">Job Class:</td>
					       		 <td align="left"><html:text name="JobForm" property="jobClass" size="30" maxlength="80"/>
					       		 	<input type="hidden" name="jobClassKey" value="<bean:write name='JobForm' property='jobClass'/>">
								 </td>
							  </tr>
							  <tr>
								 <td align="right">Trigger Name:</td>
					       		 <td align="left"><html:text name="JobForm" property="triggerName" size="11" maxlength="10"/>
					       		 	<input type="hidden" name="triggerNameKey" value="<bean:write name='JobForm' property='triggerName'/>">
								 </td>
								 <td align="right">Hour:</td>
					       		 <td align="left">
					       		 		<html:select size="5" multiple="true" styleClass="selectBox" name="JobForm" property="hour">
		                              		<html:option value="0">12:00 AM</html:option>
		                              		<html:option value="1">1:00 AM</html:option>
		                              		<html:option value="2">2:00 AM</html:option>
		                              		<html:option value="3">3:00 AM</html:option>
		                              		<html:option value="4">4:00 AM</html:option>
		                              		<html:option value="5">5:00 AM</html:option>
											<html:option value="6">6:00 AM</html:option>
											<html:option value="7">7:00 AM</html:option>
											<html:option value="8">8:00 AM</html:option>
		                              		<html:option value="9">9:00 AM</html:option>
		                              		<html:option value="10">10:00 AM</html:option>
		                              		<html:option value="11">11:00 AM</html:option>
		                              		<html:option value="12">12:00 PM</html:option>
		                              		<html:option value="13">1:00 PM</html:option>
		                              		<html:option value="14">2:00 PM</html:option>
		                              		<html:option value="15">3:00 PM</html:option>
		                              		<html:option value="16">4:00 PM</html:option>
		                              		<html:option value="17">5:00 PM</html:option>
		                              		<html:option value="18">6:00 PM</html:option>
											<html:option value="19">7:00 PM</html:option>
											<html:option value="20">8:00 PM</html:option>
											<html:option value="21">9:00 PM</html:option>
		                              		<html:option value="22">10:00 PM</html:option>
		                              		<html:option value="23">11:00 PM</html:option>
		                       			</html:select>
								 </td>
								 <td align="right">Minutes:</td>
					       		 <td align="left">
					       		 		<html:select styleClass="selectBox" name="JobForm" property="minutes">
		                              		<html:option value="0">0</html:option>
		                              		<html:option value="1">1</html:option>
		                              		<html:option value="2">2</html:option>
		                              		<html:option value="3">3</html:option>
		                              		<html:option value="4">4</html:option>
		                              		<html:option value="5">5</html:option>
											<html:option value="6">6</html:option>
											<html:option value="7">7</html:option>
											<html:option value="8">8</html:option>
		                              		<html:option value="9">9</html:option>
		                              		<html:option value="10">10</html:option>
		                              		<html:option value="11">11</html:option>
		                              		<html:option value="12">12</html:option>
		                              		<html:option value="13">13</html:option>
											<html:option value="14">14</html:option>
											<html:option value="15">15</html:option>
						                	<html:option value="16">16</html:option>
		                              		<html:option value="17">17</html:option>
		                              		<html:option value="18">18</html:option>
		                              		<html:option value="19">19</html:option>
											<html:option value="20">20</html:option>
											<html:option value="21">21</html:option>
		                              		<html:option value="22">22</html:option>
		                              		<html:option value="23">23</html:option>
		                              		<html:option value="24">24</html:option>
		                              		<html:option value="25">25</html:option>
											<html:option value="26">26</html:option>
											<html:option value="27">27</html:option>
											<html:option value="28">28</html:option>
											<html:option value="29">29</html:option>
											<html:option value="30">30</html:option>
											<html:option value="31">31</html:option>
		                              		<html:option value="32">32</html:option>
		                              		<html:option value="33">33</html:option>
		                              		<html:option value="34">34</html:option>
		                              		<html:option value="35">35</html:option>
											<html:option value="36">36</html:option>
											<html:option value="37">37</html:option>									
											<html:option value="38">38</html:option>
		                              		<html:option value="39">39</html:option>
		                              		<html:option value="40">40</html:option>
											<html:option value="41">41</html:option>
											<html:option value="42">42</html:option>									
											<html:option value="43">43</html:option>
		                              		<html:option value="44">44</html:option>
		                              		<html:option value="45">45</html:option>
											<html:option value="46">46</html:option>
											<html:option value="47">47</html:option>
		                              		<html:option value="48">48</html:option>
											<html:option value="49">49</html:option>
											<html:option value="50">50</html:option>
											<html:option value="51">51</html:option>
											<html:option value="52">52</html:option>
											<html:option value="53">53</html:option>
		                              		<html:option value="54">54</html:option>
		                              		<html:option value="55">55</html:option>
											<html:option value="56">56</html:option>
											<html:option value="57">57</html:option>
		                              		<html:option value="58">58</html:option>
											<html:option value="59">59</html:option>
		                       			</html:select>
								 </td>
							  </tr>
							  <tr>
							     <td align="right">Day of week:</td>
					       		 <td align="left">
					       		 		<html:select size="4" multiple="true" styleClass="selectBox" name="JobForm" property="jobFrequency">
		                              		<html:option value="EVE">Every Day</html:option>
		                              		<html:option value="MON">Monday</html:option>
		                              		<html:option value="TUE">Tuesday</html:option>
		                              		<html:option value="WED">Wednesday</html:option>
		                              		<html:option value="THU">Thursday</html:option>
		                              		<html:option value="FRI">Friday</html:option>
											<html:option value="SAT">Saturday</html:option>
											<html:option value="SUN">Sunday</html:option>
		                       			</html:select>
								 </td>	
								 <td align="right">Minutes Freq:</td>
					       		 <td align="left"><html:text name="JobForm" property="minutesFrequency" size="5" maxlength="2"/>
								 </td>
								<td></td>
						 		<td></td>
							  </tr>
						     </table>
						</td>
					</tr>
					<!-- buttons -->
					<tr>
						<td>
							<table width="100%" bgcolor="FFFFCC">
						       <tr>
						         <td align="center">
						         <input type="submit" value="Add Job" name="action" class="normalButton" onClick="return validateAdd(document.JobForm)" />
						         <input type="submit" value="Delete Job" name="action" class="normalButton" onClick="return confirmDelete(document.JobForm)"/>
						         <input type="submit" value="Reschedule Job" name="action" class="normalButton" onClick="return confirmReschedule(document.JobForm)"/>
						         <input type="submit" value="Refresh List" name="action" class="normalButton"/></td>
						       </tr>
						       <tr><td></td></tr>
						     </table>
						</td>
					</tr>
			</html:form>
	 	</table>			 
	  </td>
   </tr>
</table>
</div>
</body>
</html:html>