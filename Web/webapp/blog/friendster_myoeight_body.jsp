<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3" width="600">
       <TABLE width=600>
        <TBODY>
        <TR>
          <TD width="15%">&nbsp;</TD>
          <TD vAlign=top>
            <H1>Guide</H1>
            <DIV class=divBox id=guidelayer>
            <TABLE borderColor=#999999 cellSpacing=0 cellPadding=5 width="100%" 
            border=0>
              <TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=5 width="100%" border=0>
                    <TBODY>
                    <TR bgColor=#333333>
                      <TD colSpan=2><SPAN class=infoBar>Guide to Add 
                        Friendster<FONT SIZE="-1"><SUP>TM</SUP></FONT> My oEight App</SPAN></TD></TR>
                    <TR>
         
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/friendster_myoeight.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
        
                    <TR>
                      <TD colSpan=2>
                        <P>&nbsp;</P>
                        <P>You've seen many Friendster Apps around, how about 
                        one from your 080808.com.my profile?</P>
                        <P><a href="http://080808.org.my/img/guide/friendster/app1/myoeight.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app1/myoeight.jpg" 
                        border=1></a></P>
                        <P>Adding it is easy.</P>
                        <P>You can check out our sample app at our profile page 
                        : <A href="http://profiles.friendster.com/my080808" 
                        target=#>http://profiles.friendster.com/my080808</A></P>
                        <P>This Friendster App is developed by our team and is 
                        actually your oEight widget. It shows your oEight wish 
                        and your profile statistics like views and votes. When 
                        your friends or visitors clicked on the widget, it will 
                        open a new window and they will be shown your oEight 
                        profile page! </P><br><br><br>
                        <P><U>How to add Friendster My oEight App to your 
                        profile </U></P>
                        <P>&nbsp;</P></TD></TR>
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>1) Login to <A 
                        href="http://www.friendster.com/" 
                        target=_blank>Friendster<FONT SIZE="-1"><SUP>TM</SUP></FONT></A> or Grab This App from our 
                        <A href="http://profiles.friendster.com/my080808" 
                        target=#>profile page</A></SPAN> </TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P><a href="http://080808.org.my/img/guide/friendster/app1/myoeight2.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app1/myoeight2t.jpg" 
                        border=1></a></P><br><br>
                       </TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR> 
                      <TD bgColor=#333333><SPAN class=infoBar>2) Go to My 
                        oEight App install page</SPAN></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P><a href="http://080808.org.my/img/guide/friendster/app1/myoeight3.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app1/myoeight3t.jpg" 
                        border=1></a></P>
                        <P>After login to Friendster, go to the App Install page 
                        at <A href="http://widgets.friendster.com/080808" 
                        target=#>http://widgets.friendster.com/080808</A> 
                    </P></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>3) Login to 
                        080808.com.my </SPAN></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P>&nbsp;</P>
                        <P><a href="http://080808.org.my/img/guide/friendster/app1/myoeight4.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app1/myoeight4t.jpg" 

                        border=1></a></P> <br>
                         <P>This way, your oEight widget will be retrieved, and 
                        sent to your Friendster profile automatically.</P><br><br>
                        <P><a href="http://080808.org.my/img/guide/friendster/app1/myoeight5.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app1/myoeight5t.jpg" 
                        border=1></a></P></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P>Your oEight app will be then be automatically 
                        displayed. Cool isn't it?</P>
                        <P>You're done in just three simple 
                        steps (or less).</P>
                        <P>That's it! Start sharing your oEight app with your 
                        friends too.</P>
                        <P><a href="http://080808.org.my/img/guide/friendster/app1/myoeight1.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app1/myoeight1t.jpg" 
                        border=1></a></P>
                        <P>This guide can be found at : 
                          <a href="http://080808.com.my/guide/friendster_myoeight.jsp">http://080808.com.my/guide/friendster_myoeight.jsp</a> </P>
                        <%--P>You can also find other options of installing this 
                        widget to your own blog or website under &quot;Profile - 
                        Widget&quot; menu. </P--%>
                        <P></P></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>  
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/friendster_myoeight.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD width="5%">&nbsp;</TD>
                      <TD>
                        <P><BR></P></TD></TR></TBODY></TABLE></TD>
                <TD 
        width="15%">&nbsp;</TD></TR></TBODY></TABLE></DIV></TD></TD></TR></TBODY></TABLE>
    </td>                                  