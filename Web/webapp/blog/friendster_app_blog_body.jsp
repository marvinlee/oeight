<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3" width="600">
       <TABLE width=600>
        <TBODY>
        <TR>
          <TD width="15%">&nbsp;</TD>
          <TD vAlign=top>
            <H1>Guide</H1>
           <DIV class=divBox id=guidelayer>
            <TABLE borderColor=#999999 cellSpacing=0 cellPadding=5 width="100%" 
            border=0>
              <TBODY>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=5 width="100%" border=0>
                    <TBODY>
                    <TR bgColor=#333333>
                      <TD colSpan=2><SPAN class=infoBar>Guide to Add Friendster<FONT SIZE="-1"><SUP>TM</SUP></FONT> </font> My oEight Featured Blog App</SPAN></TD></TR>
                      
         
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/friendster_app_blog.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
                    <TR>
                    <TR>
                      <TD colSpan=2>
                        <P>&nbsp;</P>
                        <P>This guide is targeted to our members whose blog is featured on 080808.com.my and has a Friendster profile.</P>
                        <P>*If you would like submit your blog to be featured, remember to select the &ldquo;<strong>Feature My Blog!</strong>&rdquo; option when you update your contact details. Please note that only blogs fulfilling our criteria for public viewing will be featured.</P>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app3/myoeight_blog1.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app3/myoeight_blog1t.jpg" 
                        border=1></a></font></P>
                        <P>&nbsp;  </P>
                        <P>You can check out our sample app at our profile page 
                        : <A href="http://profiles.friendster.com/my080808" 
                        target=#>http://profiles.friendster.com/my080808</A></P>
                        <P>Anybody who visits your Friendster profile and clicking the Featured Blog App will be directed to your blog in another browser window and is a great way to promote more readers to your blog.
Follow the easy steps below to add the App to your Friendster profile! </P><BR><BR><BR>
                        <P><U>How to add My oEight Featured Blog App to your Friendster profile</U></P>
                        <P>&nbsp;</P></TD></TR>
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>1) Login to <A 
                        href="http://www.friendster.com/" 
                        target=_blank>Friendster<FONT 
                        size=-1><SUP>TM</SUP></FONT></A> or Grab This App from 
                        our <A href="http://profiles.friendster.com/my080808" 
                        target=#>profile page</A></SPAN> </TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app3/myoeight_blog2.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app3/myoeight_blog2t.jpg" 
                        border=1></a></font></P>
                      <BR>You can grab this app at our profile page : <A href="http://profiles.friendster.com/my080808" 
                        target=#>http://profiles.friendster.com/my080808</A><BR></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>2) Go to My 
                        oEight Featured Blog App install page</SPAN></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app3/myoeight_blog3.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app3/myoeight_blog3t.jpg" 
                        border=1></a></font></P>
                        <P>After login to Friendster, go to the App Install page 
                        at <A href="http://widgets.friendster.com/080808_blog" 
                        target=#>http://widgets.friendster.com/080808_blog</A>  
</P></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD bgColor=#333333><SPAN class=infoBar>3) Login to 
                        080808.com.my </SPAN></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P>&nbsp;</P>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app3/myoeight_blog4.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app3/myoeight_blog4t.jpg" 

                        border=1></a></font></P>
                        <BR>
                        <P>This way, your blog information will be retrieved, and sent to your Friendster profile automatically.</P><BR><BR>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app3/myoeight_blog5.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app3/myoeight_blog5t.jpg" 
                        border=1></a></font></P></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>
                        <P>Done in 60 seconds? </P>
                         <P>That's it! Start sharing your oEight Featured Blog App with your friends too. </P>
                        <P><font size='2' face='Arial, Helvetica, sans-serif'><a href="http://080808.org.my/img/guide/friendster/app3/myoeight_blog6.jpg" target="_blank"><IMG 
                        src="http://080808.org.my/img/guide/friendster/app3/myoeight_blog6t.jpg" 
                        border=1></a></font></P>
                        <P>This guide can be found at : <A 
                        href="http://080808.com.my/guide/friendster_app_blog.jsp">http://080808.com.my/guide/friendster_app_blog.jsp</A> 
                        </P>
                        <P></P></TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>  
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/guide/friendster_app_blog.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
                    <TR>
                      <TD>&nbsp;</TD>
                      <TD>&nbsp;</TD></TR>
                    <TR>
                      <TD width="5%">&nbsp;</TD>
                      <TD>
                        <P><BR></P></TD></TR></TBODY></TABLE></TD>
                <TD 
        width="15%">&nbsp;</TD></TR></TBODY></TABLE></DIV></TD></TD></TR></TBODY></TABLE>
    </td>                                  


