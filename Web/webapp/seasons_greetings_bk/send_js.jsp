<script language="javascript">  

function formBack(action)
{
    document.forms[0].act.value=action;
    document.forms[0].submit(); 
}
 
var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("senderName", "Sender's Name", "M", true);
	fieldList[counter++] = new Array("senderEmail", "Sender's Email", "M", true); 
	fieldList[counter++] = new Array("receiverName", "Recipient's Name", "M", true); 
	fieldList[counter++] = new Array("receiverEmail", "Recipient's Email", "M", true);  

	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);
	var emailMsg = checkEmail(form.senderEmail.value, "Sender's Email");
        if (emailMsg != '')
        {
                emailMsg += "\n"
        }
        emailMsg += checkEmail(form.receiverEmail.value, "Recipient's Email");
	if (emailMsg != '')
	{
		if (errMsg != '')
		{
			errMsg += "\n"
		}
		errMsg += emailMsg
	}

	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	}


		posted = true;
		document.forms[0].submit();
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 