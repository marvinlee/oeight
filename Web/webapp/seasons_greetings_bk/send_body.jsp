 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                com.esmart2u.solution.base.helper.PropertyManager" %>
  
    <html:form name="SeasonGreetingForm" type="com.esmart2u.oeight.member.web.struts.controller.SeasonGreetingForm" method="post" action="/seasons_greetings.do" isRelative="true">
    
    <table width="80%">
        <COL width="20%"> 
        <COL width="40%"> 
	<COL width="40%">
            <tr>
                <td class="hdr_1" align="left"><nobr>Seasons Greetings</nobr>
                </td>
                <td class="hdr_1" colspan="2" align="right">
                </td>
            </tr> 
            <tr>
                <td class="hdr_m" colspan="3" align="left">&nbsp;<br><br> 
                </td>
            </tr> 
            <tr>
                <td align="left">&nbsp;</td>
                <td  colspan="2" align="center"> 
                        
                        Merry Christmas<br> 
                        and<br>
                        Happy New Year 2008!<br>
                </td>
            </tr> 
            <tr>
                <td colspan="3" align="center"> 
                        <br><br>
                        Sender Name  :&nbsp; 
                        <html:text name="SeasonGreetingForm" property="senderName" size="50" maxlength="100"/> 
                        <html:errors property="senderName"/> 
                        <br>
                        Sender Email  :&nbsp; 
                        <html:text name="SeasonGreetingForm" property="senderEmail" size="20" maxlength="50"/> 
                        <html:errors property="senderEmail"/> 
                        &nbsp; 
                        <html:checkbox name="SeasonGreetingForm" property="subscribe"/> 
                        Subscribe me to site launch update
                        <br>
                        Recipient Name :&nbsp; 
                        <html:text name="SeasonGreetingForm" property="receiverName" size="50" maxlength="100"/> 
                        <html:errors property="receiverName"/> 
                        <br>
                        Recipient Email  :&nbsp; 
                        <html:text name="SeasonGreetingForm" property="receiverEmail" size="20" maxlength="50"/> 
                        <html:errors property="receiverEmail"/> 
                        <br>
                        Message  :&nbsp; 
                        <html:textarea name="SeasonGreetingForm" property="message" cols="50" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <html:errors property="greetingMessage"/> 
                        <br>
                        <br><br>  
                </td>
            </tr> 
           
            <tr>
                <td>&nbsp;
                </td>
                <td align="center">    
                    <input type="button" name="send" value="Send" onclick="formSubmit();"> 
                        <br><br>   
                </td>
                <td>&nbsp;
                </td>
            </tr>
            
        </table> 
        <input type="hidden" name="act" value="sendSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
