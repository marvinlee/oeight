 <%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<tiles:insert page="/tiles/mosaic_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="Seasons Greetings" />
   <tiles:put name="header" value="/tiles/empty.jsp" />
   <tiles:put name="javascript" value="/seasons_greetings/send_js.jsp" /> 
   <tiles:put name="body" value="/seasons_greetings/send_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/empty.jsp" /> 
</tiles:insert>