<script language="javascript">
function formBack(action)
{
    document.forms[0].act.value=action;
    document.forms[0].submit(); 
}


var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("blogUrl", "Blog URL", "M", true);
	fieldList[counter++] = new Array("blogFeed", "Feed URL", "M", true);   


	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);  
        
         
	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	}


		posted = true;
		document.forms[0].submit();
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 