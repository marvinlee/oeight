<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
    <title><tiles:getAsString name="title" ignore="true"/></title> 
<jsp:include page="meta_header.jsp"/>
<tiles:insert attribute="javascript"/>  
<STYLE>
.lcdstyle {
	PADDING-RIGHT: 3px; PADDING-LEFT: 3px; PADDING-BOTTOM: 3px; FONT: bold 16px MS Sans Serif; COLOR: black; PADDING-TOP: 3px
}
.lcdstyle SUP {
	FONT-SIZE: 40%
}
</STYLE>
<SCRIPT type="text/javascript">
function openNewWin(url)
{
 var tbar = "location=no,status=yes,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,width=550";
 var sw = window.open(url,"_blank",tbar,false);
 sw.focus();
}
</SCRIPT>
<link href="/css/style.css" rel="stylesheet" type="text/css">
<link REL="SHORTCUT ICON" href="http://080808.org.my/img/favicon.ico" type="image/x-icon"/>
</head>

<body onload="<tiles:getAsString ignore="true" name="onLoad"/>" <tiles:getAsString ignore="true" name="otherBodyTags"/>>
    <center>
    <div class="mainBox">
        <table border="0" cellpadding="0" cellspacing="0" width="1000" bordercolor="#000000" bgcolor="#FFFFFF">
            <%--tr>
                <td width="100%" colspan="4" valign="top"--%>
                    <tiles:insert attribute="header"/> 
                <%--/td>
            </tr--%> 
            <tr>  
<%--the body tag will be taking 3 columns max
body tag has to specify how many columns it's taking, as the main page is four columns.
if more than four columns are needed, create a new row and then add table inside --%>
                <tiles:insert attribute="body"/>  
            </tr> 
     
            <tiles:insert attribute="bottom"/> 
        </table></div>
    </center>
<jsp:include page="tracker.jsp"/>
</body>

</html>