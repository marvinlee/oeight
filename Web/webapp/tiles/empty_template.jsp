<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
    <title><tiles:getAsString name="title" ignore="true"/></title>
<jsp:include page="meta_header.jsp"/>
<tiles:insert attribute="javascript"/>    
</head>

<body onload="<tiles:getAsString ignore="true" name="onLoad"/>" <tiles:getAsString ignore="true" name="otherBodyTags"/>>
<center>
 <tiles:insert attribute="body"/>
</center>
<jsp:include page="tracker.jsp"/>
</body>

</html>