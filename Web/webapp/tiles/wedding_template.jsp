<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.OEightPledgeHelper" %>
<%@ page import="com.esmart2u.oeight.member.bo.CampaignInviteBO" %>
<%@ page import="com.esmart2u.solution.base.helper.StringUtils" %>
<%@ page import="com.esmart2u.oeight.member.helper.OEightConstants" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html> 
<head>
    <title><tiles:getAsString name="title" ignore="true"/></title>
<jsp:include page="wedding_meta_header.jsp"/>
<tiles:insert attribute="javascript"/>  
<SCRIPT type="text/javascript">
function openNewWin(url)
{
 var tbar = "location=no,status=yes,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,width=550";
 var sw = window.open(url,"_blank",tbar,false);
 sw.focus();
}  
</SCRIPT>
<link href="/css/wedding_style.css" rel="stylesheet" type="text/css">
<link REL="SHORTCUT ICON" href="http://080808.org.my/img/favicon.ico" type="image/x-icon"/>
</head>

<body onload="<tiles:getAsString ignore="true" name="onLoad"/>" <tiles:getAsString ignore="true" name="otherBodyTags"/>>
<center>
 <div class="mainBox">
        <table border="0" cellpadding="0" cellspacing="0" width="1000" bordercolor="#000000" bgcolor="#FFFFFF">
            <tiles:insert attribute="header"/>  
            <tr>
                <td colspan="4" class="w_menubar" width="100%" valign="top" >
                     <tiles:insert attribute="menu"/>
                </td>
            </tr>   
            <tr> 
                <td align="left" colspan="4" class="w_notice">
                    <br>
                </td>
            </tr>  
            <tr> 
                <td colspan="4">
                <tiles:insert attribute="body"/> 
		</td> 
            </tr> 
     
            <tiles:insert attribute="bottom"/> 
        </table></div> 
</center>
<jsp:include page="tracker.jsp"/>
</body>

</html>