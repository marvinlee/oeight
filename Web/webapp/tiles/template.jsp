<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
    <title><tiles:getAsString name="title" ignore="true"/></title>
<jsp:include page="meta_header.jsp"/>
<tiles:insert attribute="javascript"/>   
<STYLE>
.lcdstyle {
	PADDING-RIGHT: 3px; PADDING-LEFT: 3px; PADDING-BOTTOM: 3px; FONT: bold 16px MS Sans Serif; COLOR: black; PADDING-TOP: 3px
}
.lcdstyle SUP {
	FONT-SIZE: 40%
}
</STYLE>
<SCRIPT type="text/javascript"> 
function openNewWin(url)
{
 var tbar = "location=no,status=yes,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,width=550";
 var sw = window.open(url,"_blank",tbar,false);
 sw.focus();
}
</SCRIPT>
<link href="/css/style.css" rel="stylesheet" type="text/css"> 
<link REL="SHORTCUT ICON" href="http://080808.org.my/img/favicon.ico" type="image/x-icon"/>
</head>

<body onload="<tiles:getAsString ignore="true" name="onLoad"/>">
<center>
    <div class="mainBox">
        <table border="0" cellpadding="0" cellspacing="0" width="1000" bordercolor="#000000" bgcolor="#FFFFFF">
            <tr>
                <td width="100%" colspan="4" valign="top">
                    <tiles:insert attribute="header"/> 
                </td>
            </tr> 
 
<%--the body tag will be taking 3 columns max
body tag has to specify how many columns it's taking, as the main page is four columns.
if more than four columns are needed, create a new row and then add table inside --%>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>  
            <tr> 
                <td colspan="4">
                    <%-- This table below can have its own columns number--%>
                    <table border="0" cellpadding="0" width="1000"> 
                        <tr>
                        <td width="25">&nbsp;</td> 
                        <td width="150" valign="top">
                            <%-- Menu can have its own table with a wrapped form --%>
                            <tiles:insert ignore="true" attribute="menu"/>
                        </td>
                        <td width="25">&nbsp;</td> 
                        <td width="600" valign="top"><tiles:insert attribute="body"/> </td> 
                        <%--td width="15%"><tiles:insert attribute="notes"/></td--%> 
                        <td width="200" valign="top"><tiles:insert attribute="notes"/> <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="200" height="400">
                                <param name="movie" value="/images/common/200-400.swf">
                                <param name="quality" value="high">
                                <embed src="/images/common/200-400.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="200" height="400"></embed>
                        </object></td>
                        
                    </table>
                </td> 
            </tr> 
     
            <tiles:insert attribute="bottom"/> 
        </table></div>
            <%--tr>
                <td width="13%" valign="top" align="left"><tiles:insert ignore="true" attribute="menu"/></td>
                <td width="77%" valign="top"><tiles:insert attribute="body"/></td>
                <td width="10%" valign="top"><tiles:insert attribute="notes"/></td>
            </tr>
            <tr>
                <td width="100%" colspan="3" valign="top"><tiles:insert attribute="bottom"/></td>
            </tr>
        </table--%>

<jsp:include page="tracker.jsp"/>
</center>
</body>

</html>