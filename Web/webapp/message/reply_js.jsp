<script language="javascript">  
var posted = false; 
function formSend()
{ 
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array(); 
	fieldList[counter++] = new Array("subject", "Subject", "M", true); 
	fieldList[counter++] = new Array("message", "Message", "M", true);  
 
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);   
         
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	} 

        posted = true; 
        document.forms[0].submit(); 
}    
</script>
<script type="text/javascript" src="/js/check.js"></script>
<script type="text/javascript" src="/js/messages.js"></script>