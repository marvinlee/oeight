 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserMessage,
                 com.esmart2u.oeight.member.vo.MessageVO,
                 com.esmart2u.oeight.member.web.struts.controller.MessageForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%>   

<% 
    MessageForm messageForm = (MessageForm)request.getAttribute("MessageForm"); 
      
    MessageVO messageVO = (MessageVO)messageForm.getMessageVO();  

   
%>
 
    <html:form name="MessageForm" type="com.esmart2u.oeight.member.web.struts.controller.MessageForm" method="post" action="/message.do" isRelative="true">
    <div id="messageLayer" class="divBox">   
        <table width="400">
        <COL width="20%"> 
        <COL width="40%">
        <COL width="40%">   
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>  
        <tr>
            <td colspan="3"><h1>Message</h1></td>
        </tr>   
        
        <%
        if (messageVO != null)
        { 
            Date dateSent = messageVO.getDateSent();
        %>  
        <bean:define id="messageObject" name="MessageForm" property="messageVO" type="com.esmart2u.oeight.member.vo.MessageVO"/>
        <tr> 
            <td colspan="3"><br><br>
                <a href="/message.do?act=list">Back to My Messages</a><br><br>
            </td> 
        </tr> 
        <%
            if (messageVO.getReferredMessageVO() != null)
            {
                MessageVO referredMessageVO = messageVO.getReferredMessageVO();
                Date dateSource = referredMessageVO.getDateSent();
        %> 
        <bean:define id="referredMessageObject" name="messageObject" property="referredMessageVO" type="com.esmart2u.oeight.member.vo.MessageVO"/>
        <tr>
            <td class="inputlabel"></td>
            <td colspan="2"><br>Your previous message to <bean:write name="messageObject" property="senderUserName"/></td> 
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Date Sent</td>
            <td><%=DateObjectHelper.getPrintedTimestamp(dateSource)%><br></td> 
        </tr>  
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Subject</td>
            <td><bean:write name="referredMessageObject" property="subject" filter="true"/><br></td>   
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Message</td>
            <td><bean:write name="referredMessageObject" property="message" filter="true"/><br><br></td>   
        </tr>
        <%
            }
        %>
        <tr>
            <td colspan="3"><br></td> 
        </tr> 
        <tr>
            <td class="inputlabel"></td>
            <td colspan="2"><hr></td> 
        </tr> 
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">From</td>
            <td><a href='http://profile.080808.com.my/<bean:write name="messageObject" property="senderUserName"/>'><img src="/vthumb/<bean:write name="messageObject" property="photoSmallPath" />"  width="38px"><br><b><bean:write name="messageObject" property="senderUserName" filter="true"/><br></td>
         </tr>
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Date Sent</td>
            <td><%=DateObjectHelper.getPrintedTimestamp(dateSent)%><br></td> 
        </tr> 
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Subject</td>
            <td><bean:write name="messageObject" property="subject" filter="true"/><br></td>   
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Message</td>
            <td><bean:write name="messageObject" property="message" filter="true"/><br><br></td>   
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td colspan="2"><hr></td> 
        </tr> 
        <tr> 
            <td colspan="2"></td>
            <td>
                <br> 
                <input type="button" class="formbuttons" value="Reply" onclick="formReply()">
            </td> 
        </tr>
    </td>
    </tr>     
    
    <%
    }
    else
    {
    %>   
    
    <tr>
        <td class="hdr_1" colspan="3" align="left">Message Not Found
        </td>
    </tr>  
    <%
    } 
    %>       
    <tr>
        <td colspan="3">&nbsp;&nbsp;&nbsp; 
        </td>
    </tr>       
    <tr>
        <td colspan="3">&nbsp;
        </td>
    </tr>  
    </table></div>
    <input type="hidden" name="act" value="list"> 
    <html:hidden name="MessageForm" property="to"/>
    <html:hidden name="MessageForm" property="msgId"/>
    </html:form>  