<SCRIPT LANGUAGE="Javascript">
var posted = false;

function formDelete()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}
  
	posted = true;
	document.forms[0].submit();
}

function toggleDelete(exby)
{
    var messages = document.forms[0].messageIdString;
    if (messages.length > 1){ 
        for (i = 0; i < messages.length; i++)
            messages[i].checked = exby.checked? true:false;
    }else{ 
        messages.checked = exby.checked? true:false;
    } 
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 