 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserMessage,
                 com.esmart2u.oeight.member.vo.MessageVO,
                 com.esmart2u.oeight.member.web.struts.controller.MessageForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%>   
<% 
    MessageForm messageForm = (MessageForm)request.getAttribute("MessageForm"); 
      
    MessageVO messageVO = (MessageVO)messageForm.getMessageVO();  

   
%> 
    <html:form name="MessageForm" type="com.esmart2u.oeight.member.web.struts.controller.MessageForm" method="post" action="/message.do" isRelative="true">
    <div id="messageLayer" class="divBox">   
        <table width="400">
        <COL width="20%"> 
        <COL width="40%">
        <COL width="40%">   
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>  
        <tr>
            <td colspan="3"><h1>Reply Message</h1></td>
        </tr>   
         
         <tr> 
            <td colspan="3"><br><br>
                <a href="/message.do?act=list">Back to My Messages</a><br><br>
            </td> 
        </tr>  
        <tr>
            <td colspan="3"><br><br><br></td> 
        </tr> 
         <%
            if (messageVO != null)
            { 
                Date dateSource = messageVO.getDateSent();
        %> 
        <bean:define id="messageObject" name="MessageForm" property="messageVO" type="com.esmart2u.oeight.member.vo.MessageVO"/>
        <tr>
            <td class="inputlabel"></td>
            <td colspan="2"><br>Reply to <bean:write name="messageObject" property="senderUserName"/></td> 
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Date Sent</td>
            <td><%=DateObjectHelper.getPrintedTimestamp(dateSource)%><br></td> 
        </tr>  
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Subject</td>
            <td><bean:write name="messageObject" property="subject" filter="true"/><br></td>   
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Message</td>
            <td><bean:write name="messageObject" property="message" filter="true"/><br><br></td>   
        </tr>
        <%
            }
        %>
        <tr>
            <td class="inputlabel"></td>
            <td colspan="2"><hr>
                <html:errors property="recipient"/>
                <html:errors property="nonceToken"/></td> 
        </tr> 
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">To</td>
            <td><a href='http://profile.080808.com.my/<bean:write name="MessageForm" property="recipientUserName"/>'><img src="/vthumb/<bean:write name="MessageForm" property="photoSmallPath" />"  width="38px"><br><b><bean:write name="MessageForm" property="recipientUserName" filter="true"/><br></td>
         </tr>
       
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Subject</td>
            <td>
                <html:text name="MessageForm"  styleClass="inputvalue" property="subject" size="30" maxlength="100"/>   
                <html:errors property="subject"/>
            </td>   
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Message</td>
            <td><html:textarea name="MessageForm"  styleClass="inputvalue" property="message" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                <html:errors property="message"/></td>   
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td class="inputlabel">Save Sent Message</td>
            <td><html:checkbox name="MessageForm"  styleClass="inputvalue" property="isSaved"/>   
             </td>   
        </tr>
        <tr>
            <td class="inputlabel"></td>
            <td colspan="2"><hr></td> 
        </tr> 
        <tr> 
            <td colspan="2"></td>
            <td>
                <br> 
                <input type="button" class="formbuttons" value="Send" onclick="formSend();">
            </td> 
        </tr>
    </td>
    </tr>     
  
    <tr>
        <td colspan="3">&nbsp;&nbsp;&nbsp; 
        </td>
    </tr>       
    <tr>
        <td colspan="3">&nbsp;
        </td>
    </tr>  
    </table></div>
    <input type="hidden" name="act" value="replySubmitted"> 
    <html:hidden name="MessageForm" property="msgId"/>
    <html:hidden name="MessageForm" property="to"/>
    <html:hidden name="MessageForm" property="nonceToken"/>
    </html:form>  