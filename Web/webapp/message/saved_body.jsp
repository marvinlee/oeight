 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-html-el.tld" prefix="html-el"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserMessage,
                 com.esmart2u.oeight.member.vo.MessageVO,
                 com.esmart2u.oeight.member.web.struts.controller.MessageForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%>   

<% 
    MessageForm messageForm = (MessageForm)request.getAttribute("MessageForm"); 
      
    List messageList = (List)messageForm.getMessageList(); 

   
%>
 
    <html:form name="MessageForm" type="com.esmart2u.oeight.member.web.struts.controller.MessageForm" method="post" action="/message.do" isRelative="true">
    <div id="messageLayer" class="divBox">   
        <table width="400">
        <COL width="20%"> 
        <COL width="40%">
        <COL width="40%">   
        <tr>
            <td colspan="3">&nbsp;
            </td>
        </tr>  
        <tr>
            <td><a href="/message.do?act=list">My Messages</a></td>
            <td>&nbsp;</td>
            <td><h1>Sent Messages</h1></td>
        </tr>   
        
        <%
        if (messageList != null && !messageList.isEmpty())
        { 

        %>  
        <tr>
        <td colspan="3">
                <input type="button" class="formbuttons" name="Delete" value="Delete" onclick="formDelete();">
                <table>
                <tr>
                    <td width="10%" class="inputlabel"><input type="checkbox" name="deleteAll" onclick="toggleDelete(this);"></td>
                    <td width="10%" class="inputlabel">&nbsp;</td>
                    <td width="10%" class="inputlabel">Date</td>
                    <td width="20%" class="inputlabel">Send to</td>
                    <td width="50%" class="inputlabel">Subject</td>
                </tr>
                <logic:iterate id="message" name="MessageForm" property="messageList" type="com.esmart2u.oeight.member.vo.MessageVO">
                <bean:define id="messageIdString" name="message" property="messageIdString" type="java.lang.String"/>
                <bean:define id="subject" name="message" property="subject" type="java.lang.String"/>
                <bean:define id="dateSent" name="message" property="dateSent" type="java.util.Date"/>    
                <bean:define id="isRead" name="message" property="isRead" type="java.lang.Boolean"/>     
                <bean:define id="isReplied" name="message" property="isReplied" type="java.lang.Boolean"/>    
                <bean:define id="recipientUserName" name="message" property="recipientUserName" type="java.lang.String"/>    
                <bean:define id="photoSmallPath" name="message" property="photoSmallPath" type="java.lang.String"/>    
                <tr>
                    <td><html-el:checkbox name="message" property="messageIdString" value="${messageIdString}"/><%--bean:write name="messageIdString" /--%></td>
                    <td> &nbsp;
                    </td>
                    <td><%=DateObjectHelper.getPrintedTimestamp(dateSent)%></td>
                    <td><a href='http://profile.080808.com.my/<bean:write name="recipientUserName"/>'><img src="/vthumb/<bean:write name="photoSmallPath" />"  width="38px"><br><b><bean:write name="recipientUserName" filter="true"/></td>
                    <td><a href="/message.do?act=viewSaved&msgId=<bean:write name="messageIdString" />"><bean:write name="subject" filter="true"/></a></td>   
                    </td>
                </tr>
            </logic:iterate> 
            </table>
    <script>toggleDelete(document.forms[0].deleteAll)</script>
    </td>
    </tr>     
    
    <%
                }
    else
    {
    %>   
    
    <tr>
        <td colspan="3"><br><br><br> 
        </td>
    </tr>  
    <tr>
        <td class="hdr_1" colspan="3" align="left">No Messages
        </td>
    </tr>  
    <%
    } 
    %>       
    <tr>
        <td colspan="3">&nbsp;&nbsp;&nbsp; 
        </td>
    </tr>       
    <tr>
        <td colspan="3">&nbsp;
        </td>
    </tr>  
    </table></div>
    <input type="hidden" name="act" value="deleteSaved">
    <%--html:hidden name="MessageForm" property="wishId" />
    <input type="hidden" name="token" value="<%=request.getAttribute("token")%>"--%>
    </html:form>  