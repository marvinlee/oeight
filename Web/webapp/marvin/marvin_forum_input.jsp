<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ForumForm,
                 com.esmart2u.oeight.data.phpbb.PhpBbForums,
                 com.esmart2u.oeight.data.phpbb.PhpBbTopics"%>   
<html>
<head>
    <title>Forum Add Topic</title> 
<script language="javascript">
function forumDropDownChanged()
{
    document.forms[0].act.value="marvinForumAdd";
    document.forms[0].submit(); 
}

var posted = false; 
function formSubmit()
{

	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array(); 
	fieldList[counter++] = new Array("topicUrl", "Topic Url", "M", true); 
	fieldList[counter++] = new Array("message", "Message", "M", true);  


	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);   
         
	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	}


    posted = true; 
    document.forms[0].submit(); 
}    
</script>    
<script type="text/javascript" src="/js/check.js"></script>
<script type="text/javascript" src="/js/messages.js"></script>
<link href="/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
 <% 
    System.out.println("Here");
    ForumForm forumForm = (ForumForm)request.getAttribute("ForumForm"); 
    
    List forumDropDown = forumForm.getForumDropDown();
    List topicDropDown = forumForm.getTopicDropDown(); 
%> 
        <table width="100%">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">

                <h1>Forum add topic</h1> 

    <html:form name="ForumForm" type="com.esmart2u.oeight.member.web.struts.controller.ForumForm" method="post" scope="request"  action="/marvinForum.do" isRelative="true">
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr> 
            <tr>
                <td class="inputlabel" align="right">Forum :&nbsp;
                </td>
                <td align="left">
                     <html:select name="ForumForm"  styleClass="inputvalue" property="forumId" onchange="forumDropDownChanged();"> 
                        <option value="">Please Select</option>
                        <%
                        for(int i=0;i<forumDropDown.size();i++)
                        {  
                            PhpBbForums forum = (PhpBbForums)forumDropDown.get(i);
                            out.print("<option value='" + forum.getForumId() +"'");  
                            if (forumForm.getForumId() == forum.getForumId())
                            {
                                out.print(" selected ");
                            }
                            out.print(">");
                            out.println(forum.getForumName() + "</option>");
                        }
                        %>
                    </html:select>  
                    <html:errors property="subject"/>
                </td>
            </tr> 
            <tr>
                <td class="inputlabel" align="right">Topic :&nbsp;
                </td>
                <td align="left">
                     <html:select name="ForumForm"  styleClass="inputvalue" property="topicId"> 
                        <%
                        if (topicDropDown != null)
                        {
                            for(int i=0;i<topicDropDown.size();i++)
                            {  
                                PhpBbTopics topic = (PhpBbTopics)topicDropDown.get(i);
                                out.print("<option value='" + topic.getTopicId() +"'");  
                                out.print(">");
                                out.println(topic.getTopicTitle() + "</option>");
                            }
                        }
                        %>
                    </html:select>  
                    <html:errors property="subject"/>
                </td>
            </tr>
          <tr>
                <td class="inputlabel" align="right">Topic Url :&nbsp;
                </td>
                <td align="left">
                    <html:text name="ForumForm"  styleClass="inputvalue" property="topicUrl"/>   
                    <html:errors property="topicUrl"/>
                </td>
            </tr>   
             <tr>
                <td class="inputlabel" align="right">Topic Post :&nbsp;
                </td>
                <td align="left">
                    <html:textarea name="ForumForm"  styleClass="inputvalue" property="topicPost" cols="60" rows="40" />   
                    <html:errors property="topicPost"/>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr> 
                <td class="inputlabel" align="right">&nbsp;
                </td>
                <td align="left" class="formbuttonsCell" > 
                    <input type="button" name="search" class="formbuttons" value="Submit" onclick="formSubmit();">
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table> 
        <input type="hidden" name="act" value="marvinForumAdded"> 
    </html:form> 
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table> 
</body>             