<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.ConfigurationHelper"%>
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ForumForm,
                 com.esmart2u.oeight.data.phpbb.PhpBbForums,
                 com.esmart2u.oeight.data.phpbb.PhpBbTopics,
                 com.esmart2u.oeight.data.ForumTopic"%>   
<html>
<head>
    <title>Forum Topics Refreshed Page</title> 
<script type="text/javascript" src="/js/check.js"></script>
<script type="text/javascript" src="/js/messages.js"></script>
<link href="/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
 <% 
    ForumForm forumForm = (ForumForm)request.getAttribute("ForumForm"); 
    
    HashMap forumMap = forumForm.getForumMap();
%>  <div class="mainBox">
   
        <table border="0" cellpadding="0" cellspacing="0" width="1000" bordercolor="#000000" bgcolor="#FFFFFF">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">

                <h1>Forum add topic</h1> 

      <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"  bgcolor="#FFFFFF"> 
         
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr> 
        
        
        <%
            if (!forumMap.isEmpty())
            {
            
                Set keySet = forumMap.keySet();
                Iterator keyIterator = keySet.iterator();
                while (keyIterator.hasNext())
                {
                    Long forumId = (Long)keyIterator.next();
                    List forumTopicList = (List)forumMap.get(forumId);
                    //Print first topic
                    for(int i=0;i<forumTopicList.size() && i<6;i++)
                    {
                        ForumTopic topic = (ForumTopic)forumTopicList.get(i);
                        if (i==0)
                        { 
                            %>
                            
                            <tr bgcolor="#333333" valign="middle">
                            <td rowspan="2" valign="middle" width="10%">
                                <%
                                if (topic.getForumName().equals("Events"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/events.gif">
                                <%}
                                else if (topic.getForumName().equals("Great Deals"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/sales.gif">
                                <%}
                                else if (topic.getForumName().equals("News"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/news.gif">
                                <%}
                                else if (topic.getForumName().equals("Gossips"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/gossip.gif">
                                <%}
                                else if (topic.getForumName().equals("Entertainment"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/movies.gif">
                                <%}
                                else if (topic.getForumName().equals("Automotive"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/automtv.gif">
                                <%}
                                else if (topic.getForumName().equals("Sports"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/sports.gif">
                                <%}
                                else if (topic.getForumName().equals("Money Matters"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/money.gif">
                                <%}
                                else if (topic.getForumName().equals("Classifieds"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/classifieds.gif">
                                <%}
                                else if (topic.getForumName().equals("Food"))
                                {%>
                                <img src="http://forum.<%=ConfigurationHelper.getDomainName()%>/community/oeight/image/food.gif">
                                <%}
                                
                                %>
                            </td>
                            <td align="left" valign="bottom">
                                <h1>&nbsp;&nbsp;&nbsp;<span style="font-size: 150%; line-height: normal"><a href="<%=topic.getTopicUrl().substring(0,topic.getTopicUrl().lastIndexOf("/"))%>"><%=topic.getForumName()%> Forum</a></span></h1></td>
                            </tr>
                            <tr bgcolor="#333333" valign="middle">
                            <td>&nbsp;</td></tr>
                            <tr>
                            <td colspan="2">
                            <div id="pageheader">
                                <br> 
                                <a href="<%=topic.getTopicUrl()%>"><h2><%=topic.getTopicTitle()%></h2> (<%=topic.getTopicPosterName()%> - <%=DateObjectHelper.getPrintedDate(topic.getTopicTime())%> )</a> 
                            </div> 
                            <br>
                            <%=topic.getTopicPost()%>
                            <br><br>
                            Read the entire topic here : <a href="<%=topic.getTopicUrl()%>"><%=topic.getTopicTitle()%></a>
                            <br><br><br>
                            More topics from <a href="<%=topic.getTopicUrl().substring(0,topic.getTopicUrl().lastIndexOf("/"))%>"><%=topic.getForumName()%></a>
                            <br><br><br>
                            <%
                        }
                        else
                        {
                            %>
                            <%=i%>)  <a href="<%=topic.getTopicUrl()%>"><%=topic.getTopicTitle()%> ( <%=topic.getTopicPosterName()%> - <%=DateObjectHelper.getPrintedDate(topic.getTopicTime())%> )</a><br><br>                           
                            <%
                        }
                         
                        // Last topic closes the forum
                        if (i+1 > forumTopicList.size() || i==5 )  
                        {
                            %>      
                            </td>
                            </tr>
                            <%
                        }
                    }
                    %>
                    <br><br><br><br><br>
                    <%
                }

            }
        %>
             
            <tr> 
                <td class="inputlabel" align="right">&nbsp;
                </td>
                <td align="left" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="formbuttonsCell" ><a href="/marvinForum.do?act=marvinForumAdd">Add more</a>&nbsp;</td>
            </tr>
     </table>   
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table> 
            </div>
</body>             