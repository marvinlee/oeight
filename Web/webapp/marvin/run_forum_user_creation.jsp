 <%@ page import="com.esmart2u.oeight.member.bo.ForumBO"%> 
 <%@ page import="java.util.*"%>
 <%@ page import="com.esmart2u.oeight.data.User"%>
 <%
    ForumBO forumBO = new ForumBO();
    ArrayList statsList = forumBO.createForumUsers();
    if (statsList != null && !statsList.isEmpty())
    {
        out.print("Stats Size:"+ statsList.size()+"<br>");
        ArrayList existList = (ArrayList)statsList.get(0);
        ArrayList addedList = (ArrayList)statsList.get(1);
        out.print("<b>Existing user:"+ existList.size()+"</b><br>");
        for(int i=0; i<existList.size();i++)
        {
            User user = (User)existList.get(i);
            out.print((i+1) + ":" + user.getUserName() + "/" + user.getEmailAddress()+"<br>");
        }
        
        out.print("<b>Added user:"+ addedList.size()+"</b><br>");
        for(int i=0; i<addedList.size();i++)
        {
            User user = (User)addedList.get(i);
            out.print((i+1) + ":" + user.getUserName() + "/" + user.getEmailAddress()+"<br>");
        } 
    }
    else
    {
        out.print("Empty Statistic List");
    }
%>