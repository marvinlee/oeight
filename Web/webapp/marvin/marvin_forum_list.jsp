<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ForumForm,
                 com.esmart2u.oeight.data.phpbb.PhpBbForums,
                 com.esmart2u.oeight.data.phpbb.PhpBbTopics,
                 com.esmart2u.oeight.data.ForumTopic"%>   
<html>
<head>
    <title>Forum Added Topics</title>
<script>
<script language="javascript"> 
var posted = false; 
function formSubmit()
{  
    if (posted)
    {
            alert(requestSubmittedMessage);
            return;
    } 
    posted = true; 
    document.forms[0].submit(); 
}    
</script>    
<script type="text/javascript" src="/js/check.js"></script>
<script type="text/javascript" src="/js/messages.js"></script>
<link href="/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
 <% 
    ForumForm forumForm = (ForumForm)request.getAttribute("ForumForm"); 
    
    HashMap forumMap = forumForm.getForumMap();
%> 
        <table width="100%">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">

                <h1>Forum add topic</h1> 

    <html:form name="ForumForm" type="com.esmart2u.oeight.member.web.struts.controller.ForumForm" method="post" scope="request"  action="/marvinForum.do" isRelative="true">
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr> 
        
        
        <%
            if (!forumMap.isEmpty())
            {
            
                Set keySet = forumMap.keySet();
                Iterator keyIterator = keySet.iterator();
                while (keyIterator.hasNext())
                {
                    Long forumId = (Long)keyIterator.next();
                    List forumTopicList = (List)forumMap.get(forumId);
                    //Print first topic
                    for(int i=0;i<forumTopicList.size() && i<6;i++)
                    {
                        ForumTopic topic = (ForumTopic)forumTopicList.get(i);
                        if (i==0)
                        { 
                            %>
                            <div id="pageheader">
                                <h1><%=topic.getForumName()%></h1><br> 
                                <a href="<%=topic.getTopicUrl()%>"><h2><%=topic.getTopicTitle()%></h2> (<%=topic.getTopicPosterName()%> - <%=DateObjectHelper.getPrintedDate(topic.getTopicTime())%> )</a> 
                            </div> 
                            <br>
                            <%=topic.getTopicPost()%>
                            <br><br>
                            Read the entire topic here : <a href="<%=topic.getTopicUrl()%>"><%=topic.getTopicTitle()%></a>
                            <br><br><br>
                            More topics from <a href="<%=topic.getTopicUrl().substring(0,topic.getTopicUrl().lastIndexOf("/"))%>"><%=topic.getForumName()%></a>
                            <br>
                            <%
                        }
                        else
                        {
                            %>
                            <%=i%>)  <a href="<%=topic.getTopicUrl()%>"><%=topic.getTopicTitle()%> ( <%=topic.getTopicPosterName()%> - <%=DateObjectHelper.getPrintedDate(topic.getTopicTime())%> )</a><br>                            
                            <%
                        }

                    }
                    %>
                    <br><br><br><br><br>
                    <%
                }

            }
        %>
             
            <tr> 
                <td class="inputlabel" align="right">&nbsp;
                </td>
                <td align="left" class="formbuttonsCell" > 
                    <input type="button" name="search" class="formbuttons" value="Confirm and Refresh" onclick="formSubmit();">
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="2" class="formbuttonsCell" ><a href="/marvinForum.do?act=marvinForumAdd">Add more</a>&nbsp;</td>
            </tr>
     </table> 
        <input type="hidden" name="act" value="marvinForumRefresh"> 
    </html:form> 
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table> 
</body>             