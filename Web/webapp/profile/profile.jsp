<%@ page language="java" %>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ProfileForm"%>  
<%@ page import="com.esmart2u.oeight.member.helper.OEightConstants"%>     

<%
    ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");
    String userName = profileForm.getUserName();
    request.setAttribute("profile.userName",userName);
    //value="080808 Profile - <%=userName>"
      
%>
<tiles:insert page="/tiles/mosaic_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Profile"/>
   <tiles:put name="header" value="/tiles/top.jsp" />
   <tiles:put name="javascript" value="/profile/photo_flickr_js.jsp" />
   
<logic:notEqual name="ProfileForm" property="profilePreference" value="<%=Integer.toString(OEightConstants.PROFILE_PREFERENCE_CONTACT)%>">   
<% if (profileForm.isHasFriendsterFriends()){%> 
    <logic:notEmpty name="ProfileForm" property="flickrId">
        <tiles:put name="onLoad" value="onLoadFriendsterFriends();onLoadFriendsterPhotos();fjb.init();" />
    </logic:notEmpty> 
    <logic:empty name="ProfileForm" property="flickrId"> 
        <tiles:put name="onLoad" value="onLoadFriendsterFriends();onLoadFriendsterPhotos();" />
    </logic:empty> 
<% }
    else
    {%> 
    <logic:notEmpty name="ProfileForm" property="flickrId">
        <tiles:put name="onLoad" value="fjb.init();" />
    </logic:notEmpty> 
<%  
    }%>
</logic:notEqual>
<logic:equal name="ProfileForm" property="profilePreference" value="<%=Integer.toString(OEightConstants.PROFILE_PREFERENCE_CONTACT)%>">   
    <logic:notEmpty name="ProfileForm" property="flickrId">
        <tiles:put name="onLoad" value="fjb.init();" />
    </logic:notEmpty> 
</logic:equal>

   <tiles:put name="menu" value="/tiles/empty.jsp" />
   <tiles:put name="body" value="/profile/profile_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>