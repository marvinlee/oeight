<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
  
  <%
  String booleanYes = "" + PropertyConstants.BOOLEAN_YES;
  %>
    <html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true">
        <div id="details" class="divBox">  
            <table width="80%">
                <COL width="5%"> 
                <COL width="40%"> 
                <COL width="55%">
                <tr>
                    <td colspan="3">&nbsp;</td> 
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left"><h1>Profile - Lifestyle</h1>
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                    </td>
                </tr> 
                <%--tr>
                <td class="lbl" align="right">Smoker? :&nbsp;
                </td>
                <td align="left"> 
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:radio name="ProfileForm" property="lifeSmoker" value="Y" /> Yes
                        <html:radio name="ProfileForm" property="lifeSmoker" value="N" /> No 
                        <br>
                        <html:errors property="lifeSmoker"/>
                    </logic:equal>
                </td>
            </tr>
            <tr>
                <td class="lbl" align="right">Drinker? :&nbsp;
                </td>
                <td align="left"> 
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:radio name="ProfileForm" property="lifeDrinker" value="Y" /> Yes
                        <html:radio name="ProfileForm" property="lifeDrinker" value="N" /> No 
                        <br>
                        <html:errors property="lifeDrinker"/>
                    </logic:equal>
                </td>
            </tr--%>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Simple description about myself :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="lifeAboutMe" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="lifeAboutMe"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="lifeAboutMe" filter="true"/> 
                        </logic:notEqual>
                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Who I would be interested to get to know?:&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="lifeToKnow" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="lifeToKnow"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="lifeToKnow" filter="true"/> 
                        </logic:notEqual>
                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Who might be interested in knowing me? :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="lifeKnowMe" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="lifeKnowMe"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="lifeKnowMe" filter="true"/> 
                        </logic:notEqual>
                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="life1" maxlength="100" size="50"/>   
                            <html:errors property="life1"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="life1" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="lifeValue1" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="lifeValue1"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="lifeValue1" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="life2" maxlength="100" size="50"/>   
                            <html:errors property="life2"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="life2" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="lifeValue2" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="lifeValue2"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="lifeValue2" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="life3" maxlength="100" size="50"/>   
                            <html:errors property="life3"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="life3" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="lifeValue3" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="lifeValue3"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="lifeValue3" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="life4" maxlength="100" size="50"/>   
                            <html:errors property="life4"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="life4" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="lifeValue4" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="lifeValue4"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="lifeValue4" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="life5" maxlength="100" size="50"/>   
                            <html:errors property="life5"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="life5" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="lifeValue5" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="lifeValue5"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="lifeValue5" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <input class="formbuttons" type="button" name="save" value="Save" onclick="formSubmit();">
                        </logic:equal>  
                        
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
        </table></div> 
        <input type="hidden" name="act" value="lifestyleSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
