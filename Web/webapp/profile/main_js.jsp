<script language="javascript">
function formBack(action)
{
    document.forms[0].act.value=action;
    document.forms[0].submit(); 
}


var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("name", "Name", "M", true);
	fieldList[counter++] = new Array("gender", "Gender", "M", true); 
	fieldList[counter++] = new Array("birthDay", "Birthday - day", "M", true); 
	fieldList[counter++] = new Array("birthMonth", "Birthday - month", "M", true); 
	fieldList[counter++] = new Array("birthYear", "Birthday - year", "M", true); 
	fieldList[counter++] = new Array("city", "City", "M", true); 
	//fieldList[counter++] = new Array("state", "State", "M", true); 
	fieldList[counter++] = new Array("nationality", "Country", "M", true);   


	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);  
        if (form.publicEmail.value != '')
        {
            var emailMsg = checkEmail(form.publicEmail.value, "Email for display")
            if (emailMsg != '')
            {
                    if (errMsg != '')
                    {
                            errMsg += "\n"
                    }
                    errMsg += emailMsg
            }
         }
         
	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	}


		posted = true;
		document.forms[0].submit();
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 