<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
  
  <%
  String booleanYes = "" + PropertyConstants.BOOLEAN_YES;
  %>
    <html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true">
     <div id="details" class="divBox">  
            <table width="80%">
                <COL width="5%"> 
                <COL width="40%"> 
                <COL width="55%">
                <tr>
                    <td colspan="3">&nbsp;</td> 
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left"><h1>Profile - Interest</h1>
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                    </td>
                </tr> 
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Actor :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestActor" maxlength="200" size="30"/>   
                        <html:errors property="interestActor"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestActor" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Actress :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestActress" maxlength="200" size="30"/>   
                        <html:errors property="interestActress"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestActress" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Singer :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestSinger" maxlength="200" size="30"/>   
                        <html:errors property="interestSinger"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestSinger" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Music :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestMusic" maxlength="200" size="30"/>   
                        <html:errors property="interestMusic"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestMusic" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Bands :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestBand" maxlength="200" size="30"/>   
                        <html:errors property="interestBand"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestBand" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Movies :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestMovies" maxlength="200" size="30"/>   
                        <html:errors property="interestMovies"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestMovies" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Books :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestBooks" maxlength="200" size="30"/>   
                        <html:errors property="interestBooks"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestBooks" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr>
            <tr> 
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Sports :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestSports" maxlength="200" size="30"/>   
                        <html:errors property="interestSports"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestSports" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr> 
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Favourite Online Activity :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interestWebsites" maxlength="200" size="30"/>   
                        <html:errors property="interestWebsites"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestWebsites" filter="true"/> 
                    </logic:notEqual>
                        
                </td>
            </tr> 
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right"> 
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interest1" maxlength="100" size="30"/>   
                        <html:errors property="interest1"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interest1" filter="true"/> 
                    </logic:notEqual> 
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:textarea name="ProfileForm" property="interestValue1" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <html:errors property="interestValue1"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestValue1" filter="true"/> 
                    </logic:notEqual> 
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right"> 
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interest2" maxlength="100" size="30"/>   
                        <html:errors property="interest1"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interest2" filter="true"/> 
                    </logic:notEqual> 
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:textarea name="ProfileForm" property="interestValue2" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <html:errors property="interestValue2"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestValue2" filter="true"/> 
                    </logic:notEqual> 
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right"> 
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interest3" maxlength="100" size="30"/>   
                        <html:errors property="interest3"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interest3" filter="true"/> 
                    </logic:notEqual> 
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:textarea name="ProfileForm" property="interestValue3" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <html:errors property="interestValue3"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestValue3" filter="true"/> 
                    </logic:notEqual> 
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right"> 
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interest4" maxlength="100" size="30"/>   
                        <html:errors property="interest4"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interest4" filter="true"/> 
                    </logic:notEqual> 
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:textarea name="ProfileForm" property="interestValue4" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <html:errors property="interestValue4"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestValue4" filter="true"/> 
                    </logic:notEqual> 
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right"> 
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:text styleClass="inputvalue" name="ProfileForm" property="interest5" maxlength="100" size="30"/>   
                        <html:errors property="interest5"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interest5" filter="true"/> 
                    </logic:notEqual> 
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <html:textarea name="ProfileForm" property="interestValue5" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <html:errors property="interestValue5"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <bean:write name="ProfileForm" property="interestValue5" filter="true"/> 
                    </logic:notEqual> 
                </td>
            </tr>  
            
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        <input class="formbuttons" type="button" name="save" value="Save" onclick="formSubmit();">
                    </logic:equal>  
                    
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>
            
        </table></div> 
        <input type="hidden" name="act" value="interestSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
