<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.StringUtils,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.member.web.struts.controller.ProfileForm"%>     
<%
    ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");
    if (!StringUtils.hasValue(profileForm.getNationality()))
    {
        profileForm.setNationality(PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY));
    }   
    char gender = profileForm.getGender();
    boolean male = gender == PropertyConstants.GENDER_MALE? true : false;
    String imgPostfix = male? "_m": "_f";
    //System.out.println("ProfileForm gender=" + profileForm.getGender());
    //System.out.println("ProfileForm status=" + profileForm.getMaritalStatus());
    //System.out.println("ProfileForm day=" + profileForm.getBirthDay());
    //System.out.println("ProfileForm month=" + profileForm.getBirthMonth());
    //System.out.println("ProfileForm year=" + profileForm.getBirthYear());
    //System.out.println("ProfileForm state=" + profileForm.getState());
    //System.out.println("ProfileForm country=" + profileForm.getNationality()); 
%> 
    <%--html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true"--%>
    <td vAlign=top align=left width="50"></td>
    <td vAlign=top width="700">
        <table width="80%">
            <tr>
            <td width="40%">&nbsp;</td>
            <td width="60%">&nbsp;</td>
            </tr>
            <tr>
                <td class=hdr_1 align=left colSpan=2>
                <h1>Profile Details</h1> </td>
            </tr> 
            <tr>
                <td class="formbuttonsCell" colspan="2" align="right"> 
                    
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://profile.080808.com.my/<bean:write name="ProfileForm" property="userName"/>';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->

                </td>
            </tr>
            <%--tr>
                <td class="lbl" align="right">OEight Id :&nbsp; 
                </td>
                <td align="left">
                    <bean:write name="ProfileForm" property="userName" /> 
                </td>
            </tr--%> 
            <tr class="profileCenter">
                <td class="profileCenter" align="center" valign="top"><center> 
                    <span class="inputlabel"><bean:write name="ProfileForm" property="name" filter="true"/></span>
                    <br>
                    <a href="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" mce_href="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" rel="lightbox"><img src="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" width="240px"></a>
                    <%--img src="/vphotos/20071003141514test1l.jpg" width="240px"--%>
                    <br>
                    <bean:write name="ProfileForm" property="photoDescription" filter="true"/>
                    <br>
                    <span class="inputlabel">Profile Page : <a href='http://profile.080808.com.my/<bean:write name="ProfileForm" property="userName" filter="true"/>'>http://profile.080808.com.my/<bean:write name="ProfileForm" property="userName" filter="true"/></a> 
                    </center><br>
                </td>
                <td align="center" valign="top"> 
		  <br><center>
		  <div id="profile_personal" class="divBox">  
                    <table valign="top">
                        <COL width="40%"> 
                        <COL width="60%"> 
                        <tr>
                            <td align="right">&nbsp;
                            </td>
                            <td align="left">
                                <br><br>
                            </td>
                        </tr>  
                        <tr>
                            <td align="right" class="guylabel" >
                                <bean:write name="ProfileForm" property="userName" filter="true"/>'s Wish:
                            </td>
                            <td align="left" class="inputvalue">
                                <bean:write name="ProfileForm" property="myWish" filter="true"/> 
                                <logic:empty name="ProfileForm" property="myWish">
                                    No wish yet  
                                </logic:empty>
                                &nbsp;&nbsp;
                                 <%
                                    if (profileForm.isViewingOwnProfile())
                                    {
                                    %>
                                         <a href="/profile.do?act=mainEdit">Update Wish</a>
                                    <%
                                    }
                                    %>
                            </td>
                        </tr>  
                        
                        <tr>
                            <td align="right" class="guylabel" > 
                            </td>
                            <td align="left" class="inputvalue">
                                <a href="/wish.do?act=frens&userName=<%=profileForm.getUserName()%>">View My Buddies</a>
                            </td>
                        </tr>  
                        <%
                        //if (!profileForm.isViewingOwnProfile())
                        //{
                        %>
                        <tr>
                            <td align="right" class="guylabel" > 
                            </td>
                            <td align="left" class="inputvalue">
                                <a href="/wish.do?act=list&userName=<%=profileForm.getUserName()%>">Send me a Wish & Be my Buddy!</a>
                            </td>
                        </tr>  
                        <%
                        //}
                        %>
                        
                        <tr>
                            <td align="right">&nbsp;
                            </td>
                            <td align="left">
                                <br><br>
                            </td>
                        </tr>  
                        <tr>
                            <td align="right" class="guylabel" >
                                oEight Id:
                            </td>
                            <td align="left" class="inputvalue">
                                <bean:write name="ProfileForm" property="userName" filter="true"/>
                            </td>
                        </tr>  
                        <tr>
                            <td align="right" class="guylabel" >
                                Views:
                            </td>
                            <td align="left" class="inputvalue">
                                <bean:write name="ProfileForm" property="pageViewCount"/>
                            </td>
                        </tr>  
                        <tr>
                            <td align="right" class="guylabel" >
                                Votes:
                            </td>
                            <td align="left" class="inputvalue">
                                <bean:write name="ProfileForm" property="votesCount"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="guylabel" >
                                Date Joined:
                            </td>
                            <td align="left" class="inputvalue">
                                <%
                                Date registeredDate = profileForm.getSystemRegistered();
                                String formattedDate = DateObjectHelper.getPrintedDate(registeredDate);
                                out.print(formattedDate);
                                %>
                            </td>
                        </tr>
                        
                        <logic:notEmpty name="ProfileForm" property="currentLocation">
                            <tr>
                                <td align="right" class="guylabel"  class="guylabel" >
                                    Current Location:
                                </td>
                                <td align="left" class="inputvalue"> 
                                    <bean:write name="ProfileForm" property="currentLocation" filter="true"/> 
                                </td>
                            </tr>
                        </logic:notEmpty>
                        
                        <tr>
                            <td align="right" class="guylabel" > &nbsp;
                            </td>
                            <td align="left">
                                <br><br><br>
                            </td>
                        </tr>  
                        <tr valign="bottom"> 
                            <td colspan="2" align="center"> 
                                <a href="/profile.do?act=vote&id=<%=profileForm.getUserName()%>" alt="You have <%=profileForm.getVotesLeft()%> vote<%=profileForm.getVotesLeft()>1?"s" : ""%> left">Give <bean:write name="ProfileForm" property="userName" filter="true"/> your vote</a>&nbsp;<img src="images/profile/arrow.gif" width="50" height="20"><html:errors property="vote"/> <br>
                                
                                You have <%=profileForm.getVotesLeft()%> vote<%=profileForm.getVotesLeft()>1?"s" : ""%> left<br>
                                
                                <%--a href="http://del.icio.us/post?url=http://profile.080808.com.my/<%=profileForm.getUserName()%>&title=<%=profileForm.getUserName()%>"  target='#'><img src="../images/badge/delicious.med.gif">&nbsp;Bookmark This</a>&nbsp;<br>
                                
                                <a href="http://digg.com/submit?phase=2&url=profile.080808.com.my/<%=profileForm.getUserName()%>&title=<%=profileForm.getUserName()%>&bodytext=<%=profileForm.getUserName()%>&topic=software" target='#'>
                                <img src="../images/badge/digg-badge.gif" width="80" height="15" alt="Digg!" /></a>&nbsp;<br--%>
                              
                            </td>
                        </tr> 
                        
                        <tr>
                            <td align="right">&nbsp;
                            </td>
                            <td align="left">
                                <br><br>
                            </td>
                        </tr>  
                    </table></div></center>
                </td>
            </tr>
            
            <logic:notEqual name="ProfileForm" property="profilePreference" value="<%=Integer.toString(OEightConstants.PROFILE_PREFERENCE_HIDE)%>">  
                <%-- Display a row if either photo is available, else skip row --%>
                
                <logic:notEmpty name="ProfileForm" property="profilePhotoLargePath">
                    <%-- logic:notEmpty name="ProfileForm" property="shirtPhotoLargePath" --%>
                
                            <tr>
                        <logic:notEmpty name="ProfileForm" property="profilePhotoLargePath"> 
                                <td class="lbl" align="center"> 
                                    <div align="center"><br>
                                        <a href="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" mce_href="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" rel="lightbox"><img src="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" width="240px"></a>
                                        <br>
                                        <span class="lbl"><bean:write name="ProfileForm" property="profilePhotoDescription" filter="true"/></span>
                                    </div>
                                </td>
                        </logic:notEmpty>
                        <logic:empty name="ProfileForm" property="profilePhotoLargePath">  
                                <td>&nbsp;</td> 
                        </logic:empty>
                        <%-- logic:notEmpty name="ProfileForm" property="shirtPhotoLargePath"> 
                                <td class="lbl"  align="center"> 
                                    <div align="center"><br>
                                        <img src="/vphotos/<bean:write name="ProfileForm" property="shirtPhotoLargePath"/>" width="240px">
                                        <br>
                                        <span class="lbl"><bean:write name="ProfileForm" property="shirtPhotoDescription"/></span>
                                    </div>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                        </logic:notEmpty> 
                        <logic:empty name="ProfileForm" property="shirtPhotoLargePath">  
                                <td>&nbsp;</td> 
                        </logic:empty --%>
                                <td>&nbsp;</td>
                            </tr>
                    <%--/logic:notEmpty--%> 
                </logic:notEmpty>
                
                <%--Flickr Row--%>
                <tr>
                  <td class=lbl align=middle><br><br></td>
                  <td>&nbsp;</td></tr>
                <tr>
                  <td class=lbl  align="center" colSpan=2><img src="/images/profile/flickr_logo.gif" width="98" height="26">
                   <%
                    if (profileForm.isViewingOwnProfile())
                    {
                    %>
                         &nbsp;<a href="/profile.do?act=photoEdit">Update My Photos</a>
                    <%
                    }
                    %>
                  </td>
                </tr>
                <logic:notEmpty name="ProfileForm" property="flickrId">
                    <tr>
                        <td class="lbl"  align="left" colspan="2"> 
                            <nobr><a href="http://flickr.com/photos/<%=profileForm.getFlickrId()%>" id="flickrlink" target="#">Check out my flickr photostream</a></nobr>
                        </td>
                    </tr>
                </logic:notEmpty> 
                <logic:empty name="ProfileForm" property="flickrId">
                <tr>
                  <td class="lbl" align="center" colSpan=2>No Flickr Photos</td>
                </tr>
                </logic:empty> 
            <%-- Done With Flickr--%>
            
            
            <%-- Other Details Start--%>
            <tr>
              <td class=lbl  align="center" colSpan=2>&nbsp;</td>
            </tr>
            <tr>
              <td class=lbl  align="center" colSpan=2><span class="hdr_1"><img src="/images/profile/profile_hdr<%=imgPostfix%>.gif"  height="60">
              </span>
                <%
                if (profileForm.isViewingOwnProfile())
                {
                %>
                     &nbsp;<a href="/profile.do?act=mainEdit">Update My Personal Details</a>
                <%
                }
                %>
              </td>
            </tr>
                <tr>
                    <td class="hdr_1" colspan="2" align="left">
                        <div id="profile_personal1" class="divBox">
                        <table border="0" width="100%">
                            <COL width="20%"> 
                            <COL width="80%">
                            <tr>
                                <td class=inputlabel colspan="2" align="left">Personal Details</td>
                            </tr> 
                            
                            <logic:notEmpty name="ProfileForm" property="genderString">
                                <tr>
                                    <td  class="guylabel" align="right">Gender :&nbsp;
                                    </td>
                                    <td align="left">     
                                        <bean:write name="ProfileForm" property="genderString" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="maritalStatusString">
                                <tr>
                                    <td  class="guylabel"  align="right">Status :&nbsp;
                                    </td>
                                    <td align="left">    
                                        <bean:write name="ProfileForm" property="maritalStatusString" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>                        
                            <logic:notEmpty name="ProfileForm" property="birthdayString">
                                <tr>
                                    <td  class="guylabel" align="right">Birthday :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="birthdayString" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="city">
                                <tr>
                                    <td  class="guylabel" align="right">City :&nbsp;
                                    </td>
                                    <td align="left"> 
                                        <bean:write name="ProfileForm" property="city" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>            
                            <logic:notEmpty name="ProfileForm" property="stateString">       
                                <tr>
                                    <td  class="guylabel" align="right">State :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="stateString" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>                        
                            <logic:notEmpty name="ProfileForm" property="nationalityString">
                                <tr>
                                    <td  class="guylabel" align="right">Country :&nbsp;
                                    </td>
                                    <td align="left"> 
                                        <bean:write name="ProfileForm" property="nationalityString" filter="true"/> 
                                    </td>
                                </tr> 
                            </logic:notEmpty>                
                            <logic:notEmpty name="ProfileForm" property="schools">         
                                <tr>
                                    <td  class="guylabel" align="right">Schools :&nbsp;
                                    </td>
                                    <td align="left">        
                                        <bean:write name="ProfileForm" property="schools" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>                
                            <logic:notEmpty name="ProfileForm" property="occupation">
                                <tr>
                                    <td  class="guylabel" align="right">Occupation :&nbsp;
                                    </td>
                                    <td align="left">         
                                        <bean:write name="ProfileForm" property="occupation" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>                
                            <logic:notEmpty name="ProfileForm" property="company">
                                <tr>
                                    <td  class="guylabel" align="right">Company :&nbsp;
                                    </td>
                                    <td align="left">        
                                        <bean:write name="ProfileForm" property="company" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>           
                            <logic:notEmpty name="ProfileForm" property="publicEmail">         
                                <tr>
                                    <td  class="guylabel" align="right">Email Contact :&nbsp;
                                    </td>
                                    <td align="left">                   
                                        <bean:write name="ProfileForm" property="publicEmail"/> 
                                    </td>
                                </tr> 
                            </logic:notEmpty>         
                            
                      </table>
                              </div>
                              </td>
                        </tr>
                            <tr>
                                <td colspan="2" align="left">&nbsp; </td>
                            </tr>
                            
                  <tr>
                      <td class=lbl  align="center" colSpan=2><span class="hdr_1"><img src="/images/profile/lifestyle_hdr<%=imgPostfix%>.gif"  height="60"></span>
                                  <%
                if (profileForm.isViewingOwnProfile())
                {
                %>
                     &nbsp;
                <a href="/profile.do?act=lifestyleEdit">Update My Lifestyle Details</a>
                <%
                }
                %>
                      </td>
                    </tr>
                    <tr>
                      <td class=lbl  align="center" colSpan=2>
                        <div id="profile_personal2" class="divBox">
                          <table border="0" cellpadding="2" width="100%">
                            <COL width="20%"> 
                            <COL width="80%">
                            <tr>
                                <td class="inputlabel" colspan="2" align="left"><span class="hdr_1">Lifestyle</span></td>
                            </tr>  
                            
                            <logic:notEmpty name="ProfileForm" property="lifeAboutMe">
                                <tr>
                                    <td class="guylabel" align="right">Simple description about myself :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="lifeAboutMe" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="lifeToKnow">
                                <tr>
                                    <td class="guylabel" align="right">Who I would be interested to get to know?:&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="lifeToKnow" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="lifeKnowMe">
                                <tr>
                                    <td class="guylabel" align="right">Who might be interested in knowing me? :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="lifeKnowMe" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="life1">
                                <logic:notEmpty name="ProfileForm" property="lifeValue1">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="life1" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue1" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="life2">
                                <logic:notEmpty name="ProfileForm" property="lifeValue2">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="life2" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue2" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="life3">
                                <logic:notEmpty name="ProfileForm" property="lifeValue3">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="life3" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue3" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="life4">
                                <logic:notEmpty name="ProfileForm" property="lifeValue4">
                                    <tr>
                                        <td class="guylabel" align="right">  
                                            <bean:write name="ProfileForm" property="life4" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue4" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="life5">
                                <logic:notEmpty name="ProfileForm" property="lifeValue5">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="life5" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue5" filter="true"/> 
                                        </td>
                                    </tr>   
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                           </table>
                        </div></td>
                    </tr> 
                            
                            <tr>
                                <td colspan="2" align="left">&nbsp; </td>
                            </tr>
                            <tr>
                              <td class=hdr_1 align=left colSpan=2><img src="/images/profile/interests_hdr<%=imgPostfix%>.gif"  height="60">
                                <%
                                if (profileForm.isViewingOwnProfile())
                                {
                                %>
                                     &nbsp;
                                <a href="/profile.do?act=interestEdit">Update My Interests Details</a>
                                <%
                                }
                                %>
                              </td>
                            </tr>
                            <tr>
                                <td class="hdr_1" colspan="2" align="left"><div id="profile_personal3" class="divBox">
                                    <table border="0" cellpadding="2" width="100%">
                                    <COL width="20%"> 
                                    <COL width="80%">
                                      <tr>
                                        <td class=inputlabel align=left colSpan=2>Interests</td>
                                      </tr>
                            
                            
                            <logic:notEmpty name="ProfileForm" property="interestActor">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Actor :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestActor" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestActress">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Actress :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestActress" filter="true"/> 
                                    </td>
                                </tr> 
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestSinger">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Singer :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestSinger" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestMusic">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Music :&nbsp;
                                    </td>
                                    <td align="left"> 
                                        <bean:write name="ProfileForm" property="interestMusic" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestBand">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Bands :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestBand" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestMovies">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Movies :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestMovies" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestBooks">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Books :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestBooks" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestSports">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Sports :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestSports" filter="true"/>  
                                    </td>
                                </tr> 
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestWebsites">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Online Activity :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestWebsites" filter="true"/>  
                                    </td>
                                </tr> 
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interest1">
                                <logic:notEmpty name="ProfileForm" property="interestValue1">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="interest1" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue1" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="interest2">
                                <logic:notEmpty name="ProfileForm" property="interestValue2">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="interest2" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue2" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="interest3">
                                <logic:notEmpty name="ProfileForm" property="interestValue3">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="interest3" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue3" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="interest4">
                                <logic:notEmpty name="ProfileForm" property="interestValue4">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="interest4" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue4" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="interest5">
                                <logic:notEmpty name="ProfileForm" property="interestValue5">
                                    <tr>
                                        <td class="guylabel" align="right">  
                                            <bean:write name="ProfileForm" property="interest5" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue5" filter="true"/> 
                                        </td>
                                    </tr>  
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            
                            </table>
                          </div></td>
                        </tr>
                            
                            <tr>
                                <td colspan="2" align="left">&nbsp; </td>
                            </tr>
                             <tr>
                              <td class=hdr_1 align=left colSpan=2><img src="/images/profile/skills_hdr<%=imgPostfix%>.gif" height="60">
                                  <%
                                if (profileForm.isViewingOwnProfile())
                                {
                                %>
                                     &nbsp;
                                <a href="/profile.do?act=skillsEdit">Update My Skills Details</a>
                                <%
                                }
                                %></td>
                            </tr>
                            <tr>
                              <td class=hdr_1 align=left colSpan=2><div id="profile_personal4" class="divBox">
                                <table border="0" cellpadding="2" width="100%">
                                  <tr>
                                    <td class=inputlabel align=left colSpan=2>Skills</td>
                                  </tr>
              
                            <logic:notEmpty name="ProfileForm" property="skillsExpert">
                                <tr>
                                    <td class="guylabel" align="right">I am expert in :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="skillsExpert" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="skillsGood">
                                <tr>
                                    <td class="guylabel" align="right">I am experienced in :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="skillsGood" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="skillsLearn">
                                <tr>
                                    <td class="guylabel" align="right">I would like to learn :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="skillsLearn" filter="true"/> 
                                        
                                    </td>
                                </tr>  
                            </logic:notEmpty>
                               
                            </table>
                          </div></td>
                        </tr> 
                            
                            <logic:notEqual name="ProfileForm" property="profilePreference" value="<%=Integer.toString(OEightConstants.PROFILE_PREFERENCE_CONTACT)%>">       
                                <tr>
                                    <td colspan="2" align="left">&nbsp; </td>
                                </tr>
                                 <tr>
                                  <td class=hdr_1 align=left colSpan=2><img src="/images/profile/contacts_hdr<%=imgPostfix%>.gif" height="60">
                                <%
                                if (profileForm.isViewingOwnProfile())
                                {
                                %>
                                     &nbsp;
                                <a href="/profile.do?act=contactEdit">Update My Contact Details</a>
                                <%
                                }
                                %></td>
                                </tr>
                                <tr>
                                  <td class=hdr_1 align=left colSpan=2><div id="profile_personal5" class="divBox">
                                <table border="0" cellpadding="2" width="100%">
                                <tr>
                                    <td class=inputlabel align=left colSpan=2>Contact</td>
                                </tr>  
                                
                                <logic:notEmpty name="ProfileForm" property="contactPhone">
                                    <tr>
                                        <td class="guylabel" align="right">Phone :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <bean:write name="ProfileForm" property="contactPhone" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty>           
                                <logic:notEmpty name="ProfileForm" property="contactGoogle">        
                                    <tr>
                                        <td class="guylabel" align="right">My Google id :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <bean:write name="ProfileForm" property="contactGoogle" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty>       
                                <logic:notEmpty name="ProfileForm" property="contactYahoo">            
                                    <tr>
                                        <td class="guylabel" align="right">My Yahoo id :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <bean:write name="ProfileForm" property="contactYahoo" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty>               
                                <logic:notEmpty name="ProfileForm" property="contactMsn">    
                                    <tr>
                                        <td class="guylabel" align="right">My MSN id :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <bean:write name="ProfileForm" property="contactMsn" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty>              
                                <logic:notEmpty name="ProfileForm" property="webFriendster">     
                                    <tr>
                                        <td class="guylabel" align="right">My Friendster :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://profiles.friendster.com/<bean:write name="ProfileForm" property="webFriendster" filter="true"/>" target="_blank">
                                                http://profiles.friendster.com/<bean:write name="ProfileForm" property="webFriendster" filter="true"/>
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="webHi5">
                                    <tr>
                                        <td class="guylabel" align="right">My Hi5 :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.hi5.com/friend/<bean:write name="ProfileForm" property="webHi5" filter="true"/>" target="_blank">
                                                http://www.hi5.com/friend/<bean:write name="ProfileForm" property="webHi5" filter="true"/>
                                            </a>              
                                        </td>
                                    </tr>
                                </logic:notEmpty>      
                                <logic:notEmpty name="ProfileForm" property="webWayn">
                                    <tr>
                                        <td class="guylabel" align="right">My Wayn :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.wayn.com/waynprofile.html?member_key=<bean:write name="ProfileForm" property="webWayn" filter="true"/>" target="_blank"> 
                                                http://www.wayn.com/waynprofile.html?member_key=<bean:write name="ProfileForm" property="webWayn" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>  
                                <logic:notEmpty name="ProfileForm" property="webOrkut">
                                    <tr>
                                        <td class="guylabel" align="right">My Orkut :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.orkut.com/Profile.aspx?uid=<bean:write name="ProfileForm" property="webOrkut" filter="true"/>" target="_blank"> 
                                                http://www.orkut.com/Profile.aspx?uid=<bean:write name="ProfileForm" property="webOrkut" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>   
                                <logic:notEmpty name="ProfileForm" property="webFacebook">
                                    <tr>
                                        <td class="guylabel" align="right">My Facebook :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.facebook.com/profile.php?id=<bean:write name="ProfileForm" property="webFacebook" filter="true"/>" target="_blank"> 
                                                http://www.facebook.com/profile.php?id=<bean:write name="ProfileForm" property="webFacebook" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>   
                                <logic:notEmpty name="ProfileForm" property="webMyspace">
                                    <tr>
                                        <td class="guylabel" align="right">MySpace URL :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.myspace.com/<bean:write name="ProfileForm" property="webMyspace" filter="true"/>" target="_blank"> 
                                                http://www.myspace.com/<bean:write name="ProfileForm" property="webMyspace" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>  
                                <logic:notEmpty name="ProfileForm" property="webBlog">
                                    <tr>
                                        <td class="guylabel" align="right">My Blog :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://<bean:write name="ProfileForm" property="webBlog" filter="true"/>" target="_blank"> 
                                                http://<bean:write name="ProfileForm" property="webBlog" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>  
                                <logic:notEmpty name="ProfileForm" property="web1">
                                    <logic:notEmpty name="ProfileForm" property="webValue1">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web1" filter="true"/> 
                                            </td>
                                            <td align="left">  
                                                <bean:write name="ProfileForm" property="webValue1" filter="true"/> 
                                            </td>
                                        </tr>
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="web2">
                                    <logic:notEmpty name="ProfileForm" property="webValue2">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web2" filter="true"/> 
                                            </td>
                                            <td align="left"> 
                                                
                                                <bean:write name="ProfileForm" property="webValue2" filter="true"/> 
                                            </td>
                                        </tr>
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="web3">
                                    <logic:notEmpty name="ProfileForm" property="webValue3">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web3" filter="true"/> 
                                            </td>
                                            <td align="left">  
                                                <bean:write name="ProfileForm" property="webValue3" filter="true"/> 
                                            </td>
                                        </tr>
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="web4">
                                    <logic:notEmpty name="ProfileForm" property="webValue4">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web4" filter="true"/> 
                                            </td>
                                            <td align="left"> 
                                                
                                                <bean:write name="ProfileForm" property="webValue4" filter="true"/> 
                                            </td>
                                        </tr>
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="web5">
                                    <logic:notEmpty name="ProfileForm" property="webValue5">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web5" filter="true"/> 
                                            </td>
                                            <td align="left">  
                                                <bean:write name="ProfileForm" property="webValue5" filter="true"/> 
                                            </td>
                                        </tr>   
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                            </logic:notEqual>  
                        </table>
                    </td>
                </tr>  
            </logic:notEqual>    
            
            <tr><td colspan="2"><br><br><center>  
                                <%
                                if (!profileForm.isViewingOwnProfile())
                                {
                                %>
                                <a href="/profile.do?act=report&id=<%=profileForm.getUserName()%>">Report Abuse</a>&nbsp;
                                <%
                                }
                                %></center></td></tr>
            </table> 
    </td> 
    <td vAlign=top align=left width="50"></td>
    <%--/html:form--%>    
