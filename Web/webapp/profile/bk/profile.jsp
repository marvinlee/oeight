<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.ProfileForm"%>  

<%
    ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");
    String userName = profileForm.getUserName();
    request.setAttribute("profile.userName",userName);
    //value="080808 Profile - <%=userName>"
      
%>
<tiles:insert page="/tiles/mosaic_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Profile"/>
   <tiles:put name="header" value="/tiles/top.jsp" />
   <tiles:put name="javascript" value="/profile/photo_flickr_js.jsp" />
   <tiles:put name="onLoad" value="fjb.init();" />
   <tiles:put name="menu" value="/tiles/empty.jsp" />
   <tiles:put name="body" value="/profile/profile_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>