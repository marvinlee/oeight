<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.StringUtils,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.ProfileForm"%>    
  
<%
    ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");  
	String booleanYes = "" + PropertyConstants.BOOLEAN_YES;
%>

    <html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true">
        <div id="photoLayer" class="divBox">   
            <table width="400">
                <COL width="20%"> 
                <COL width="40%">
                <COL width="40%"> 
                <tr>
                    <td class="lbl" align="center">  
                    </td>
                    <td align="left" colspan="2">  
                    </td>
                </tr>   
                <tr>
                    <td class="hdr_1" align="left"><a href="/profile.do?act=photoEdit">Photos</a>
                    </td>
                    <td class="formbuttonsCell" colspan="2" align="right">Integrate Flickr
                    </td>
                </tr> 
                <tr>
                    <td colspan="3"><br><br></td>
                </tr>
                <tr>
                    <td class="lbl" align="center"  colspan="3"> 
                        <html:errors property="flickrId"/> 
                    </td>
                </tr>   
                
                <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                    
                    <tr>
                        <td class="hdr_m" colspan="3" align="center">
                            Please put your Flickr Id here : 
                            <html:text styleClass="inputvalue" name="ProfileForm" property="flickrId"/>  <input class="formbuttons" type="button" value="Preview" onclick="formSubmit();"><br> 
                            
                            Example of Flickr Id: <b>12345678@N00</b><br> 
                        </td>
                    </tr> 
                </logic:equal>
                <logic:equal name="ProfileForm" property="requestEdit" value="<%=""+PropertyConstants.BOOLEAN_NO%>">
                    <tr>
                        <td class="lbl" align="center" colspan="3"> 
                            Your Flickr Id : http://flickr.com/photos/<%=profileForm.getFlickrId()%>
                        </td>
                    </tr>               
                </logic:equal> 
                
                <%
                if (StringUtils.hasValue(profileForm.getFlickrId()))
                {
                %>   
                <tr>
                    <td class="lbl" colspan="3" align="center"> 
                        <a href="http://flickr.com/photos/<%=profileForm.getFlickrId()%>" id="flickrlink" target="#">Check out my flickr photostream</a>
                    </td>
                </tr>
                <%
                }
                %>
                
                
                <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                    
                    <tr>
                        <td class="lbl" align="center"  colspan="3"> 
                            <br>&nbsp; 
                        </td>
                    </tr>   
                    
                    <tr>
                        <td>&nbsp;</td>
                        <td class="lbl" align="center" colspan="2"> 
                            If you do not know your Flickr Id, put your Yahoo! Id <br>
                            here: 
                            <html:text styleClass="inputvalue" name="ProfileForm" property="yahooId"/>  <input class="formbuttons" type="button" value="Lookup" onclick="searchFlickrId();"><br>
                            We will lookup the Flickr Id for you.<br>
                            <html:errors property="yahooId"/>  
                        </td> 
                    </tr>  
                </logic:equal>
                
                
                <tr>
                    <td class="lbl" align="center"  colspan="3"> 
                        <br>&nbsp; 
                    </td>
                </tr>     
                <tr>
                    <td class="lbl" align="center"  colspan="3"> 
                        <br>&nbsp; 
                    </td>
                </tr>   
                
                
                <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">                     
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2" align="center">
                            The above are my Flickr photos. &nbsp; &nbsp;  
                            <input class="formbuttons" type="button" name="save" value="Confirm" onclick="formSubmit();"> 
                        </td>
                    </tr>  
                </logic:equal>    
                <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">                     
                     <tr>
                        <td>&nbsp;</td>
                        <td colspan="2" align="center">
                            Back to previous screen
                            <input class="formbuttons" type="button" name="save" value="Back" onclick="formSubmit();"> 
                        </td>
                    </tr>  
                </logic:notEqual>                 
                
                <tr>
                    <td colspan="3"><br> 
                </tr>
        </table></div> 
        <input type="hidden" name="act" value="flickrSubmitted"> 
    </html:form>  