<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
  
  <%
  String booleanYes = "" + PropertyConstants.BOOLEAN_YES;
  %>
    <html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true">
        <div id="details" class="divBox">  
            <table width="80%">
                <COL width="5%"> 
                <COL width="40%"> 
                <COL width="55%">
                <tr>
                    <td colspan="3">&nbsp;</td> 
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left"><h1>Profile - Skills</h1>
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">I am expert in :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="skillsExpert" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="skillsExpert"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="skillsExpert" filter="true"/> 
                        </logic:notEqual>
                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">I am experienced in :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="skillsGood" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="skillsGood"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="skillsGood" filter="true"/> 
                        </logic:notEqual>
                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">I would like to learn :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="skillsLearn" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="skillsLearn"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="skillsLearn" filter="true"/> 
                        </logic:notEqual>
                        
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;
                    </td>
                    <td>
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <input class="formbuttons" type="button" name="save" value="Save" onclick="formSubmit();">
                        </logic:equal>  
                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                
        </table></div> 
        <input type="hidden" name="act" value="skillsSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
