<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.ConfigurationHelper,
                 com.esmart2u.oeight.member.web.struts.controller.ProfileForm"%>
  
<%
    ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");
    String widgetHTML = profileForm.getWidgetMainHTML();
%>
         <div id="widget" class="divBox">  
            <table width="80%">
                <COL width="5%"> 
                <COL width="40%"> 
                <COL width="55%">
                <tr>
                    <td colspan="3" rowspan="11">&nbsp;</td> 
                </tr>
                <tr>
                    <td class="hdr_1" colspan="2" align="left"><h1>Profile - Widget</h1>
                    </td>
                </tr>  
                <tr>
                    <td class="hdr_m" colspan="2" align="left" valign="top"><br><br> 
                    </td>
                </tr> 
                <tr>
                    <td class="inputlabel" colspan="2"  align="right">Promote your 080808 profile with the oEight widget :&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">&nbsp;</td>
                    <td align="left">
                        <%=widgetHTML%>
                    </td>
                </tr>
                  
                <tr>
                    <td class="hdr_m" colspan="2" align="left" valign="top"><br><br> 
                    </td>
                </tr> 
                <tr>
                    <td class="inputlabel" align="right" valign="top">Source code using &lt;iframe&gt; :&nbsp;
                    </td>
                    <td align="left">
                        <textarea id="iframeContent" class=inputvalue" cols="30" rows="5"><%="<iframe scrolling=\"no\" frameborder=\"0\" src=\"http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=main&type=iframe&id="+profileForm.getId()+" width=\"200\" height=\"240\" style=\"border: none; overflow: auto;\"></iframe>"%></textarea>
                        <br>
                        <input class="formbuttons" type="button" name="save1" value="Copy to Clipboard" onclick="copyIframe();"> 
                    </td>
                </tr> 
                 
                <tr>
                    <td class="hdr_m" colspan="2" align="left" valign="top"><br><br> 
                    </td>
                </tr> 
                <tr>
                    <td class="inputlabel" align="right" valign="top">Source code using &lt;script&gt; :&nbsp;
                    </td>
                    <td align="left">
                        <textarea id="scriptContent" class=inputvalue" cols="30" rows="5"><%="<script src=\"http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=main&id="+profileForm.getId()+"\"></script>"%></textarea>
                        <br>
                        <input class="formbuttons" type="button" name="save2" value="Copy to Clipboard" onclick="copyScript();"> 
                    </td>
                </tr>
                
                <tr>
                    <td>&nbsp;<TEXTAREA ID="holdtext" STYLE="display:none;"></TEXTAREA>
                    </td>
                    <td>
                        <%--Not functioning yet
                             <input class="formbuttons" type="button" name="save" value="Copy to Clipboard" onclick="alert('not ready');">
                   --%>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                
        </table></div>   
