<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %> 
  
<%
    //ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");
    String viewerUserEmail = (String)request.getAttribute("viewerUserEmail");
%>

    <%--html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true"--%>
        <div id="details" class="divBox">  
            <table width="80%">
                <COL width="5%"> 
                <COL width="40%"> 
                <COL width="55%">
                <tr>
                    <td colspan="3">&nbsp;</td> 
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left"><h1>Validate Profile</h1>
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                        Your profile has not been validated yet.<br>
                        We have sent you an email with the validation link.<br>
                        If the email has yet to arrive, please take a moment to update your profile <a href="/profile.do?act=mainEdit">here</a><br>
                        <% if (viewerUserEmail != null) {%>
                        or if you need to regenerate the validation link, kindly click <a href="/invite.do?act=requestSubmitted&emails=<%=viewerUserEmail%>">here</a>.
                        <% } %>
                    </td>
                </tr>   
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
        </table></div>  
    <%--/html:form--%>    
