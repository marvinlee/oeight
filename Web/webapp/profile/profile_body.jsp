<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@ page import="java.util.*"%>  
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.StringUtils,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.solution.base.helper.ConfigurationHelper,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.member.web.struts.controller.ProfileForm"%>     
<%
    ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");
    if (!StringUtils.hasValue(profileForm.getNationality()))
    {
        profileForm.setNationality(PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY));
    }   
    char gender = profileForm.getGender();
    boolean male = gender == PropertyConstants.GENDER_MALE? true : false;
    String imgPostfix = male? "_m": "_f";
    String name = profileForm.getName();
    if (name!= null && name.length() > 50)
    {
        name = name.substring(0,50);
        profileForm.setName(name); //let jsp page do character filtering
    }
    String currentLocation = profileForm.getCurrentLocation();
    if (currentLocation != null && currentLocation.length() > 50)
    {
        currentLocation = currentLocation.substring(0,50);
        profileForm.setCurrentLocation(currentLocation); //let jsp page do character filtering
        
    }
    //System.out.println("ProfileForm gender=" + profileForm.getGender());
    //System.out.println("ProfileForm status=" + profileForm.getMaritalStatus());
    //System.out.println("ProfileForm day=" + profileForm.getBirthDay());
    //System.out.println("ProfileForm month=" + profileForm.getBirthMonth());
    //System.out.println("ProfileForm year=" + profileForm.getBirthYear());
    //System.out.println("ProfileForm state=" + profileForm.getState());
    //System.out.println("ProfileForm country=" + profileForm.getNationality()); 
%> 
    <%--html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true"--%>
    <td vAlign=top align=left width="50"></td>
    <td vAlign=top width="700">
        <table width="100%">
            <tr>
            <td width="40%">&nbsp;</td>
            <td width="60%">&nbsp;</td>
            </tr>
            
            <%  
            if (profileForm.isViewingOwnProfile())
            {
                if (profileForm.getValidated() != PropertyConstants.BOOLEAN_YES){ %>
            <tr>
                <td class="inputvalue" align="center"  colspan="2" > 
                    Your profile is not validated yet.<br>You should receive an email containing the validation link after registration.<br> 
                    If you need to regenerate the validation link, please click <a href="/invite.do?act=requestSubmitted&emails=<%=profileForm.getEmailAddress()%>">here</a>.<br><br>                       
                </td>
            </tr> 
            <%  }
            }%>
            
            <tr>
                <td class=hdr_1 align=left colSpan=2>
                <%--h1>Profile Details</h1--%> 
                <img src="/images/profile/profile_hdr.gif" alt="Profile Details">
                </td>
            </tr> 
            <tr>
                <td class="formbuttonsCell" colspan="2" align="right"> 
                    
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://profile.080808.com.my/<bean:write name="ProfileForm" property="userName"/>';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->

                </td>
            </tr>
            <%--tr>
                <td class="lbl" align="right">OEight Id :&nbsp; 
                </td>
                <td align="left">
                    <bean:write name="ProfileForm" property="userName" /> 
                </td>
            </tr--%> 
             <tr class="profileCenter">
              <td colspan="2" align="center" valign="top" class="profileCenter"><table width="100%"  border="1" cellpadding="5" cellspacing="0" bordercolor="#000000">
                <tr align="center">
                    <td bgcolor="#3366CC"><center><font color="white"><b><bean:write name="ProfileForm" property="name" filter="true"/></b></font></center></td>
                  <td rowspan="2" width="380">
                      <span class="guylabel">&nbsp;&nbsp;&nbsp;<bean:write name="ProfileForm" property="userName" filter="true"/>'s Wish:</span><br>
                    <center> <br>
                                <table width="60%" border="1" cellpadding="3" cellspacing="0" bgcolor="#FFFFCC"><tr><td>
                                <bean:write name="ProfileForm" property="myWish" filter="true"/> 
                                <logic:empty name="ProfileForm" property="myWish">
                                    No wish yet  
                                </logic:empty>
                                </td></tr></table>
                                <br>
                                 <%
                                    if (profileForm.isViewingOwnProfile())
                                    {
                                    %>
                                         <a href="/profile.do?act=mainEdit"><img src="/images/profile/upd_wish.gif" width="150" height="20"></a>
                                    <%
                                    }
                                    else{
                                        if (!profileForm.isInBuddyList())
                                        {
                                    %>
                                        <a href="/wish.do?act=requestFren&userName=<%=profileForm.getUserName()%>">Add as buddy</a>
                                    <%
                                        }
                                    } 
                                    %>
                                <br><br><br>
                                <a href="/wish.do?act=list&userName=<%=profileForm.getUserName()%>"><img src="/images/profile/wishes.gif" width="150" height="20"></a><br><br><br>
                                <a href="/wish.do?act=frens&userName=<%=profileForm.getUserName()%>"><img src="/images/profile/buddies.gif" width="150" height="20"></a><br><br><br>                                
                                <% if (profileForm.isViewingOwnProfile() && profileForm.getHasMessage()){%>
                                    <a href="/message.do?act=list">You have new private message</a><br><br><br>
                                <% } %>
                                <br><br>
					  <table>
                        <tr>
                            <td align="right" class="guylabel" >
                                oEight Id:
                            </td>
                            <td align="left" class="inputvalue">
                                <bean:write name="ProfileForm" property="userName" filter="true"/>
                            </td>
                        </tr>  
                        <tr>
                            <td align="right" class="guylabel" >
                                Views:
                            </td>
                            <td align="left" class="inputvalue">
                                <bean:write name="ProfileForm" property="pageViewCount"/>
                            </td>
                        </tr>  
                        <tr>
                            <td align="right" class="guylabel" >
                                Votes:
                            </td>
                            <td align="left" class="inputvalue">
                                <bean:write name="ProfileForm" property="votesCount"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="guylabel" >
                                Date Joined:
                            </td>
                            <td align="left" class="inputvalue">
                                <%
                                Date registeredDate = profileForm.getSystemRegistered();
                                String formattedDate = DateObjectHelper.getPrintedDate(registeredDate);
                                out.print(formattedDate);
                                %>
                            </td>
                        </tr> 
                        
                        <logic:notEmpty name="ProfileForm" property="currentLocation">
                            <tr>
                                <td align="right" class="guylabel" >
                                    Current Location:
                                </td>
                                <td align="left" class="inputvalue"> 
                                    <bean:write name="ProfileForm" property="currentLocation" filter="true"/> 
                                </td>
                            </tr>
                        </logic:notEmpty>
                        
					  </table></center>
                   <br><br><br> 
                    <% if (!profileForm.isViewingOwnProfile()){%>
                    <a href="/message.do?act=new&to=<%=profileForm.getUserName()%>">Send <%=profileForm.getUserName()%> a private message</a>  <b><%--label style="color: red">NEW!</label--%></b>
                    <% } %>
                   <br><br> 
                    <a href="/profile.do?act=vote&id=<%=profileForm.getUserName()%>" alt="You have <%=profileForm.getVotesLeft()%> vote<%=profileForm.getVotesLeft()>1?"s" : ""%> left">Give <bean:write name="ProfileForm" property="userName" filter="true"/> your vote</a>&nbsp;<img src="/images/profile/arrow.gif" width="50" height="20"><html:errors property="vote"/> <br>
                    You have <%=profileForm.getVotesLeft()%> vote<%=profileForm.getVotesLeft()>1?"s" : ""%> left<br><br></td>
                  <td width="53" rowspan="2" align="right" valign="top" bgcolor="#CCCCCC"><img src="/images/profile/id_side.gif" width="53" height="344"></td>
                </tr>
                <tr>
                  <td>
                  <center> 
                    <%--span class="inputlabel"><bean:write name="ProfileForm" property="name" filter="true"/></span--%> 
                    <a href="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" mce_href="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" rel="lightbox"><img src="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" width="240px"></a>
                     
                    <br>
                    <bean:write name="ProfileForm" property="photoDescription" filter="true"/>
                    <br>
                    <span class="inputlabel">Profile Page : <a href='http://profile.080808.com.my/<bean:write name="ProfileForm" property="userName" filter="true"/>'>http://profile.080808.com.my/<bean:write name="ProfileForm" property="userName" filter="true"/></a> 
                    </center><br><br><br> 
                                </td>
                </tr>
              </table></td>
            </tr>
            
            
            <logic:notEqual name="ProfileForm" property="profilePreference" value="<%=Integer.toString(OEightConstants.PROFILE_PREFERENCE_HIDE)%>">  
                <%-- Display a row if either photo is available, else skip row --%>
                
                <logic:notEmpty name="ProfileForm" property="profilePhotoLargePath">
                    <%-- logic:notEmpty name="ProfileForm" property="shirtPhotoLargePath" --%>
                
                            <tr>
                        <logic:notEmpty name="ProfileForm" property="profilePhotoLargePath"> 
                                <td class="lbl" align="center"> 
                                    <div align="center"><br>
                                        <a href="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" mce_href="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" rel="lightbox"><img src="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" width="240px"></a>
                                        <br>
                                        <span class="lbl"><bean:write name="ProfileForm" property="profilePhotoDescription" filter="true"/></span>
                                    </div>
                                </td>
                        </logic:notEmpty>
                        <logic:empty name="ProfileForm" property="profilePhotoLargePath">  
                                <td>&nbsp;</td> 
                        </logic:empty>
                        <%-- logic:notEmpty name="ProfileForm" property="shirtPhotoLargePath"> 
                                <td class="lbl"  align="center"> 
                                    <div align="center"><br>
                                        <img src="/vphotos/<bean:write name="ProfileForm" property="shirtPhotoLargePath"/>" width="240px">
                                        <br>
                                        <span class="lbl"><bean:write name="ProfileForm" property="shirtPhotoDescription"/></span>
                                    </div>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                        </logic:notEmpty> 
                        <logic:empty name="ProfileForm" property="shirtPhotoLargePath">  
                                <td>&nbsp;</td> 
                        </logic:empty --%>
                                <td>&nbsp;</td>
                            </tr>
                    <%--/logic:notEmpty--%> 
                </logic:notEmpty>
               
                <%--Scripted Show Buddies !! Alignment problem>
                <tr>
                  <td class=lbl align=middle><br><br></td>
                  <td>&nbsp;</td></tr>
                <tr>
                  <td colspan="2" --%>  
                <%--
                    String buddiesWidgetHTML = "<script src='http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=buddies&id=" + profileForm.getUserName() +"'></script><br>";
                    out.print(buddiesWidgetHTML);   
                %>  
                  </td>               
                </tr--%>    
                
                <%--Scripted Show Comments --%>
                <%--
                    String wishesWidgetHTML = "<script src='http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=buddies&id=" + profileForm.getUserName() +"'></script><br>";
                    out.print(wishesWidgetHTML);   
                --%>  
                
                <%--Scripted Apps Row--%>
                <logic:notEqual name="ProfileForm" property="profilePreference" value="<%=Integer.toString(OEightConstants.PROFILE_PREFERENCE_CONTACT)%>">   
                <% if (profileForm.isHasFriendsterFriends()){%>
                   
                <tr>
                  <td class=lbl align=middle><br><br></td>
                  <td>&nbsp;</td></tr>
                <tr>
                  <td colspan="2">
                <%
                    String widgetHTML = "<script src='http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=friendsterFriends&id=" + profileForm.getUserName() +"'></script><br>" +
                            "<script src='http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=friendsterPhoto&showMyself=true&id=" + profileForm.getUserName() +"'></script>";
                    out.print(widgetHTML);   
                %>      
                      
                  </td>               
                </tr>
                <% }%>
                </logic:notEqual>
                
                
                <%-- Done With Scripted Apps--%>
                
                
                
                <%--Flickr Row--%>
                <tr>
                  <td class=lbl align=middle><br><br></td>
                  <td>&nbsp;</td></tr>
                <tr>
                  <td class=lbl  align="center" colSpan=2><img src="/images/profile/flickr_logo.gif" width="49" height="13">
                   <%
                    if (profileForm.isViewingOwnProfile())
                    {
                    %>
                         &nbsp;<a href="/profile.do?act=photoEdit">Update My Photos</a>
                    <%
                    }
                    %>
                  </td>
                </tr>
                <logic:notEmpty name="ProfileForm" property="flickrId">
                    <tr>
                        <td class="lbl"  align="left" colspan="2"> 
                            <nobr><a href="http://flickr.com/photos/<%=profileForm.getFlickrId()%>" id="flickrlink" target="#">Check out my flickr photostream</a></nobr>
                        </td>
                    </tr>
                </logic:notEmpty> 
                <logic:empty name="ProfileForm" property="flickrId">
                <tr>
                    <td class="inputlabel" align="center" colSpan=2><b>User Has No Flickr Photos</b></td>
                </tr>
                </logic:empty> 
            <%-- Done With Flickr--%>
            
            
            <%-- Other Details Start--%>
            <tr>
              <td class=lbl  align="center" colSpan=2>&nbsp;</td>
            </tr>
            <tr>
              <td class=lbl  align="center" colSpan=2><span class="hdr_1"><img src="/images/profile/profile_hdr<%=imgPostfix%>.gif"  height="60">
              </span>
                <%
                if (profileForm.isViewingOwnProfile())
                {
                %>
                     &nbsp;<a href="/profile.do?act=mainEdit">Update My Personal Details</a>
                <%
                }
                %>
              </td>
            </tr>
                <tr>
                    <td class="hdr_1" colspan="2" align="left">
                        <div id="profile_personal1" class="divBox">
                        <table border="0" width="100%">
                            <COL width="20%"> 
                            <COL width="80%">
                            <tr>
                                <td class=inputlabel colspan="2" align="left">Personal Details</td>
                            </tr> 
                            
                            <logic:notEmpty name="ProfileForm" property="genderString">
                                <tr>
                                    <td  class="guylabel" align="right">Gender :&nbsp;
                                    </td>
                                    <td align="left">     
                                        <bean:write name="ProfileForm" property="genderString" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="maritalStatusString">
                                <tr>
                                    <td  class="guylabel"  align="right">Status :&nbsp;
                                    </td>
                                    <td align="left">    
                                        <bean:write name="ProfileForm" property="maritalStatusString" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>                        
                            <logic:notEmpty name="ProfileForm" property="birthdayString">
                                <tr>
                                    <td  class="guylabel" align="right">Age :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <%=DateObjectHelper.getAge(profileForm.getBirthDate().getTime())%>
                                        <%--bean:write name="ProfileForm" property="birthdayString" filter="true"/--%> 
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="city">
                                <tr>
                                    <td  class="guylabel" align="right">City :&nbsp;
                                    </td>
                                    <td align="left"> 
                                        <bean:write name="ProfileForm" property="city" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>            
                            <logic:notEmpty name="ProfileForm" property="stateString">       
                                <tr>
                                    <td  class="guylabel" align="right">State :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="stateString" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>                        
                            <logic:notEmpty name="ProfileForm" property="nationalityString">
                                <tr>
                                    <td  class="guylabel" align="right">Country :&nbsp;
                                    </td>
                                    <td align="left"> 
                                        <bean:write name="ProfileForm" property="nationalityString" filter="true"/> 
                                    </td>
                                </tr> 
                            </logic:notEmpty>                
                            <logic:notEmpty name="ProfileForm" property="schools">         
                                <tr>
                                    <td  class="guylabel" align="right">Schools :&nbsp;
                                    </td>
                                    <td align="left">        
                                        <bean:write name="ProfileForm" property="schools" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>                
                            <logic:notEmpty name="ProfileForm" property="occupation">
                                <tr>
                                    <td  class="guylabel" align="right">Occupation :&nbsp;
                                    </td>
                                    <td align="left">         
                                        <bean:write name="ProfileForm" property="occupation" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>                
                            <logic:notEmpty name="ProfileForm" property="company">
                                <tr>
                                    <td  class="guylabel" align="right">Company :&nbsp;
                                    </td>
                                    <td align="left">        
                                        <bean:write name="ProfileForm" property="company" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>           
                            <logic:notEmpty name="ProfileForm" property="publicEmail">         
                                <tr>
                                    <td  class="guylabel" align="right">Email Contact :&nbsp;
                                    </td>
                                    <td align="left">                   
                                        <bean:write name="ProfileForm" property="publicEmail"/> 
                                    </td>
                                </tr> 
                            </logic:notEmpty>         
                            
                      </table>
                              </div>
                              </td>
                        </tr>
                            <tr>
                                <td colspan="2" align="left">&nbsp; </td>
                            </tr>
                            
                  <tr>
                      <td class=lbl  align="center" colSpan=2><span class="hdr_1"><img src="/images/profile/lifestyle_hdr<%=imgPostfix%>.gif"  height="60"></span>
                                  <%
                if (profileForm.isViewingOwnProfile())
                {
                %>
                     &nbsp;
                <a href="/profile.do?act=lifestyleEdit">Update My Lifestyle Details</a>
                <%
                }
                %>
                      </td>
                    </tr>
                    <tr>
                      <td class=lbl  align="center" colSpan=2>
                        <div id="profile_personal2" class="divBox">
                          <table border="0" cellpadding="2" width="100%">
                            <COL width="20%"> 
                            <COL width="80%">
                            <tr>
                                <td class="inputlabel" colspan="2" align="left"><span class="hdr_1">Lifestyle</span></td>
                            </tr>  
                            
                            <logic:notEmpty name="ProfileForm" property="lifeAboutMe">
                                <tr>
                                    <td class="guylabel" align="right">Simple description about myself :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="lifeAboutMe" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="lifeToKnow">
                                <tr>
                                    <td class="guylabel" align="right">Who I would be interested to get to know?:&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="lifeToKnow" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="lifeKnowMe">
                                <tr>
                                    <td class="guylabel" align="right">Who might be interested in knowing me? :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="lifeKnowMe" filter="true"/> 
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="life1">
                                <logic:notEmpty name="ProfileForm" property="lifeValue1">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="life1" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue1" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="life2">
                                <logic:notEmpty name="ProfileForm" property="lifeValue2">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="life2" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue2" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="life3">
                                <logic:notEmpty name="ProfileForm" property="lifeValue3">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="life3" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue3" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="life4">
                                <logic:notEmpty name="ProfileForm" property="lifeValue4">
                                    <tr>
                                        <td class="guylabel" align="right">  
                                            <bean:write name="ProfileForm" property="life4" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue4" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="life5">
                                <logic:notEmpty name="ProfileForm" property="lifeValue5">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="life5" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="lifeValue5" filter="true"/> 
                                        </td>
                                    </tr>   
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                           </table>
                        </div></td>
                    </tr> 
                            
                            <tr>
                                <td colspan="2" align="left">&nbsp; </td>
                            </tr>
                            <tr>
                              <td class=hdr_1 align=left colSpan=2><img src="/images/profile/interests_hdr<%=imgPostfix%>.gif"  height="60">
                                <%
                                if (profileForm.isViewingOwnProfile())
                                {
                                %>
                                     &nbsp;
                                <a href="/profile.do?act=interestEdit">Update My Interests Details</a>
                                <%
                                }
                                %>
                              </td>
                            </tr>
                            <tr>
                                <td class="hdr_1" colspan="2" align="left"><div id="profile_personal3" class="divBox">
                                    <table border="0" cellpadding="2" width="100%">
                                    <COL width="20%"> 
                                    <COL width="80%">
                                      <tr>
                                        <td class=inputlabel align=left colSpan=2>Interests</td>
                                      </tr>
                            
                            
                            <logic:notEmpty name="ProfileForm" property="interestActor">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Actor :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestActor" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestActress">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Actress :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestActress" filter="true"/> 
                                    </td>
                                </tr> 
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestSinger">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Singer :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestSinger" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestMusic">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Music :&nbsp;
                                    </td>
                                    <td align="left"> 
                                        <bean:write name="ProfileForm" property="interestMusic" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestBand">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Bands :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestBand" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestMovies">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Movies :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestMovies" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestBooks">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Books :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestBooks" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestSports">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Sports :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestSports" filter="true"/>  
                                    </td>
                                </tr> 
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interestWebsites">
                                <tr>
                                    <td class="guylabel" align="right">Favourite Online Activity :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="interestWebsites" filter="true"/>  
                                    </td>
                                </tr> 
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="interest1">
                                <logic:notEmpty name="ProfileForm" property="interestValue1">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="interest1" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue1" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="interest2">
                                <logic:notEmpty name="ProfileForm" property="interestValue2">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="interest2" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue2" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="interest3">
                                <logic:notEmpty name="ProfileForm" property="interestValue3">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="interest3" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue3" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="interest4">
                                <logic:notEmpty name="ProfileForm" property="interestValue4">
                                    <tr>
                                        <td class="guylabel" align="right">   
                                            <bean:write name="ProfileForm" property="interest4" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue4" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            <logic:notEmpty name="ProfileForm" property="interest5">
                                <logic:notEmpty name="ProfileForm" property="interestValue5">
                                    <tr>
                                        <td class="guylabel" align="right">  
                                            <bean:write name="ProfileForm" property="interest5" filter="true"/> 
                                        </td>
                                        <td align="left">  
                                            <bean:write name="ProfileForm" property="interestValue5" filter="true"/> 
                                        </td>
                                    </tr>  
                                </logic:notEmpty> 
                            </logic:notEmpty> 
                            
                            </table>
                          </div></td>
                        </tr>
                            
                            <tr>
                                <td colspan="2" align="left">&nbsp; </td>
                            </tr>
                             <tr>
                              <td class=hdr_1 align=left colSpan=2><img src="/images/profile/skills_hdr<%=imgPostfix%>.gif" height="60">
                                  <%
                                if (profileForm.isViewingOwnProfile())
                                {
                                %>
                                     &nbsp;
                                <a href="/profile.do?act=skillsEdit">Update My Skills Details</a>
                                <%
                                }
                                %></td>
                            </tr>
                            <tr>
                              <td class=hdr_1 align=left colSpan=2><div id="profile_personal4" class="divBox">
                                <table border="0" cellpadding="2" width="100%">
                                  <tr>
                                    <td class=inputlabel align=left colSpan=2>Skills</td>
                                  </tr>
              
                            <logic:notEmpty name="ProfileForm" property="skillsExpert">
                                <tr>
                                    <td class="guylabel" align="right">I am expert in :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="skillsExpert" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="skillsGood">
                                <tr>
                                    <td class="guylabel" align="right">I am experienced in :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="skillsGood" filter="true"/>  
                                    </td>
                                </tr>
                            </logic:notEmpty>
                            <logic:notEmpty name="ProfileForm" property="skillsLearn">
                                <tr>
                                    <td class="guylabel" align="right">I would like to learn :&nbsp;
                                    </td>
                                    <td align="left">  
                                        <bean:write name="ProfileForm" property="skillsLearn" filter="true"/> 
                                        
                                    </td>
                                </tr>  
                            </logic:notEmpty>
                               
                            </table>
                          </div></td>
                        </tr> 
                            
                            <logic:notEqual name="ProfileForm" property="profilePreference" value="<%=Integer.toString(OEightConstants.PROFILE_PREFERENCE_CONTACT)%>">       
                                <tr>
                                    <td colspan="2" align="left">&nbsp; </td>
                                </tr>
                                 <tr>
                                  <td class=hdr_1 align=left colSpan=2><img src="/images/profile/contacts_hdr<%=imgPostfix%>.gif" height="60">
                                <%
                                if (profileForm.isViewingOwnProfile())
                                {
                                %>
                                     &nbsp;
                                <a href="/profile.do?act=contactEdit">Update My Contact Details</a>
                                <%
                                }
                                %></td>
                                </tr>
                                <tr>
                                <td class=hdr_1 align=left colSpan=2><div id="profile_personal5" class="divBox">
                                <table border="0" cellpadding="2" width="100%">
                                <tr>
                                    <td class=inputlabel align=left colSpan=2>Contact</td>
                                </tr>  
                                
                                <logic:notEmpty name="ProfileForm" property="contactPhone">
                                    <tr>
                                        <td class="guylabel" align="right">Phone :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <bean:write name="ProfileForm" property="contactPhone" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty>           
                                <logic:notEmpty name="ProfileForm" property="contactGoogle">        
                                    <tr>
                                        <td class="guylabel" align="right">My Google id :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <bean:write name="ProfileForm" property="contactGoogle" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty>       
                                <logic:notEmpty name="ProfileForm" property="contactYahoo">            
                                    <tr>
                                        <td class="guylabel" align="right">My Yahoo id :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <bean:write name="ProfileForm" property="contactYahoo" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty>               
                                <logic:notEmpty name="ProfileForm" property="contactMsn">    
                                    <tr>
                                        <td class="guylabel" align="right">My MSN id :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <bean:write name="ProfileForm" property="contactMsn" filter="true"/> 
                                        </td>
                                    </tr>
                                </logic:notEmpty>              
                                <logic:notEmpty name="ProfileForm" property="webFriendster">     
                                    <tr>
                                        <td class="guylabel" align="right">My Friendster :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://profiles.friendster.com/<bean:write name="ProfileForm" property="webFriendster" filter="true"/>" target="_blank">
                                                http://profiles.friendster.com/<bean:write name="ProfileForm" property="webFriendster" filter="true"/>
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="webHi5">
                                    <tr>
                                        <td class="guylabel" align="right">My Hi5 :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.hi5.com/friend/<bean:write name="ProfileForm" property="webHi5" filter="true"/>" target="_blank">
                                                http://www.hi5.com/friend/<bean:write name="ProfileForm" property="webHi5" filter="true"/>
                                            </a>              
                                        </td>
                                    </tr>
                                </logic:notEmpty>      
                                <logic:notEmpty name="ProfileForm" property="webWayn">
                                    <tr>
                                        <td class="guylabel" align="right">My Wayn :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.wayn.com/waynprofile.html?member_key=<bean:write name="ProfileForm" property="webWayn" filter="true"/>" target="_blank"> 
                                                http://www.wayn.com/waynprofile.html?member_key=<bean:write name="ProfileForm" property="webWayn" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>  
                                <logic:notEmpty name="ProfileForm" property="webOrkut">
                                    <tr>
                                        <td class="guylabel" align="right">My Orkut :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.orkut.com/Profile.aspx?uid=<bean:write name="ProfileForm" property="webOrkut" filter="true"/>" target="_blank"> 
                                                http://www.orkut.com/Profile.aspx?uid=<bean:write name="ProfileForm" property="webOrkut" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>   
                                <logic:notEmpty name="ProfileForm" property="webFacebook">
                                    <tr>
                                        <td class="guylabel" align="right">My Facebook :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.facebook.com/profile.php?id=<bean:write name="ProfileForm" property="webFacebook" filter="true"/>" target="_blank"> 
                                                http://www.facebook.com/profile.php?id=<bean:write name="ProfileForm" property="webFacebook" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>   
                                <logic:notEmpty name="ProfileForm" property="webMyspace">
                                    <tr>
                                        <td class="guylabel" align="right">MySpace URL :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://www.myspace.com/<bean:write name="ProfileForm" property="webMyspace" filter="true"/>" target="_blank"> 
                                                http://www.myspace.com/<bean:write name="ProfileForm" property="webMyspace" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>  
                                <logic:notEmpty name="ProfileForm" property="webBlog">
                                    <tr>
                                        <td class="guylabel" align="right">My Blog :&nbsp;
                                        </td>
                                        <td align="left"> 
                                            <a href="http://<bean:write name="ProfileForm" property="webBlog" filter="true"/>" target="_blank"> 
                                                http://<bean:write name="ProfileForm" property="webBlog" filter="true"/> 
                                            </a>                  
                                        </td>
                                    </tr>
                                </logic:notEmpty>  
                                <logic:notEmpty name="ProfileForm" property="web1">
                                    <logic:notEmpty name="ProfileForm" property="webValue1">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web1" filter="true"/> 
                                            </td>
                                            <td align="left">  
                                                <bean:write name="ProfileForm" property="webValue1" filter="true"/> 
                                            </td>
                                        </tr>
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="web2">
                                    <logic:notEmpty name="ProfileForm" property="webValue2">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web2" filter="true"/> 
                                            </td>
                                            <td align="left"> 
                                                
                                                <bean:write name="ProfileForm" property="webValue2" filter="true"/> 
                                            </td>
                                        </tr>
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="web3">
                                    <logic:notEmpty name="ProfileForm" property="webValue3">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web3" filter="true"/> 
                                            </td>
                                            <td align="left">  
                                                <bean:write name="ProfileForm" property="webValue3" filter="true"/> 
                                            </td>
                                        </tr>
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="web4">
                                    <logic:notEmpty name="ProfileForm" property="webValue4">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web4" filter="true"/> 
                                            </td>
                                            <td align="left"> 
                                                
                                                <bean:write name="ProfileForm" property="webValue4" filter="true"/> 
                                            </td>
                                        </tr>
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                                <logic:notEmpty name="ProfileForm" property="web5">
                                    <logic:notEmpty name="ProfileForm" property="webValue5">
                                        <tr>
                                            <td class="guylabel" align="right">   
                                                <bean:write name="ProfileForm" property="web5" filter="true"/> 
                                            </td>
                                            <td align="left">  
                                                <bean:write name="ProfileForm" property="webValue5" filter="true"/> 
                                            </td>
                                        </tr>   
                                    </logic:notEmpty> 
                                </logic:notEmpty> 
                            </logic:notEqual>  
                        </table></div>
                    </td>
                </tr>  
            </logic:notEqual>    
            
            <tr><td colspan="2"><br><br><center>  
                                <%
                                if (!profileForm.isViewingOwnProfile())
                                {
                                %>
                                <a href="/profile.do?act=report&id=<%=profileForm.getUserName()%>">Report Abuse</a>&nbsp;
                                <%
                                }
                                %></center></td></tr>
            </table> 
    </td> 
    <td vAlign=top align=left width="50"></td>
    <%--/html:form--%>    
