<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.StringUtils,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.ProfileForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper,
                 com.esmart2u.oeight.member.web.struts.helper.StateComboHelper,
                 com.esmart2u.oeight.member.web.struts.helper.ProfilePreferenceComboHelper,
                 com.esmart2u.oeight.member.web.struts.helper.MaritalStatusComboHelper,
                 com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper"%>    
  
<%
    ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");
    if (!StringUtils.hasValue(profileForm.getNationality()))
    {
        profileForm.setNationality(PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY));
    }
        
    System.out.println("ProfileForm gender=" + profileForm.getGender());
    System.out.println("ProfileForm status=" + profileForm.getMaritalStatus());
    System.out.println("ProfileForm day=" + profileForm.getBirthDay());
    System.out.println("ProfileForm month=" + profileForm.getBirthMonth());
    System.out.println("ProfileForm year=" + profileForm.getBirthYear());
    System.out.println("ProfileForm state=" + profileForm.getState());
    System.out.println("ProfileForm country=" + profileForm.getNationality());
    
    
    Vector countryList = CountryComboHelper.getCountryList();
    Vector stateList = StateComboHelper.getStateList();
    Vector maritalStatusList = MaritalStatusComboHelper.getStatusList();
    Vector preferenceList = ProfilePreferenceComboHelper.getPreferenceList();
    String yesString = "" + PropertyConstants.BOOLEAN_YES;
%>

    <html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true">  
        <div id="details" class="divBox">  
            <table width="80%">
                <COL width="5%"> 
                <COL width="40%"> 
                <COL width="55%">
                <tr>
                    <td colspan="3">&nbsp;</td> 
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left"><h1>Personal Details</h1>
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                    </td>
                </tr>
                <%--tr>
                <td class="lbl" align="right">OEight Id :&nbsp; 
                </td>
                <td align="left">
                    <bean:write name="ProfileForm" property="userName" /> 
                </td>
            </tr--%> 
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">My Wish :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="myWish" cols="50" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="myWish"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="myWish" filter="true"/> 
                        </logic:notEqual>
                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Profile Preference :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:select styleClass="inputvalue" name="ProfileForm" property="profilePreference"> 
                                <%
                                for(int i=0;i<preferenceList.size();i++)
                                {  
                                String[] preference = (String[])preferenceList.get(i);
                                out.print("<option value='" + preference[0] +"'"); 
                                if (preference[0].equals(Integer.toString(profileForm.getProfilePreference())))
                                out.print(" selected ");
                                out.print(">");
                                out.println(preference[1] + "</option>");
                                }
                                %>
                            </html:select>       
                            <br>
                            <html:errors property="profilePreference"/>
                        </logic:equal>    
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="profilePreferenceString" filter="true"/> 
                        </logic:notEqual>                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left">&nbsp; 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Name :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:text styleClass="inputvalue"  name="ProfileForm" property="name" maxlength="100" size="50"/> 
                            <html:errors property="name"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="name" filter="true"/> 
                        </logic:notEqual>
                        
                    </td>
                </tr>
                <%--tr>
                <td class="lbl" align="right">NRIC :&nbsp;
                </td>
                <td align="left">
                    <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                        <html:text name="ProfileForm" property="nric" maxlength="12" size="20"/> 
                        <html:errors property="nric"/>
                    </logic:equal>
                    
                    <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                        <bean:write name="ProfileForm" property="nric"/> 
                    </logic:notEqual>
                        
                </td>
            </tr--%>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Gender :&nbsp;
                    </td>
                    <td align="left"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:radio styleClass="inputvalue" name="ProfileForm" property="gender" value="M" /> Male
                            <html:radio styleClass="inputvalue" name="ProfileForm" property="gender" value="F" /> Female 
                            <br>
                            <html:errors property="gender"/>
                        </logic:equal>                
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="genderString" filter="true"/> 
                        </logic:notEqual>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Status :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:select styleClass="inputvalue" name="ProfileForm" property="maritalStatus"> 
                                <%
                                for(int i=0;i<maritalStatusList.size();i++)
                                {  
                                String[] status = (String[])maritalStatusList.get(i);
                                out.print("<option value='" + status[0] +"'"); 
                                if (status[0].equals(profileForm.getMaritalStatus()))
                                out.print(" selected ");
                                out.print(">");
                                out.println(status[1] + "</option>");
                                }
                                %>
                            </html:select>       
                            <br>
                            <html:errors property="maritalStatus"/>
                        </logic:equal>    
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="maritalStatusString" filter="true"/> 
                        </logic:notEqual>                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Birthday :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:select styleClass="inputvalue" name="ProfileForm" property="birthDay">
                                <%
                                Vector daysCombo = DateObjectHelper.getDaysCombo();
                                String[] daysString = (String[])daysCombo.get(0);
                                for(int i=0;i<daysString.length;i++)
                                {
                                out.print("<option value='" + daysString[i] +"'");
                                if (daysString[i].equals(profileForm.getBirthDay()))
                                out.print(" selected ");
                                out.print(">");
                                out.print(daysString[i] + "</option>");
                                }
                                %>
                            </html:select>
                            /
                            <html:select styleClass="inputvalue" name="ProfileForm" property="birthMonth">
                                <%
                                Vector monthsCombo = DateObjectHelper.getMonthsCombo();
                                String[] monthsLabels = (String[])monthsCombo.get(0);
                                String[] monthsValues = (String[])monthsCombo.get(1);
                                for(int i=0;i<monthsLabels.length;i++)
                                {
                                out.print("<option value='" + monthsValues[i] +"'"); 
                                if (monthsValues[i].equals(profileForm.getBirthMonth()))
                                out.print(" selected ");
                                out.print(">");
                                out.print(monthsLabels[i] + "</option>");
                                }
                                %>
                            </html:select>
                            /                    
                            <html:select styleClass="inputvalue" name="ProfileForm" property="birthYear">
                                <%
                                Vector yearsCombo = DateObjectHelper.getYearsCombo();
                                String[] yearsString = (String[])yearsCombo.get(0);
                                for(int i=0;i<yearsString.length;i++)
                                {
                                out.print("<option value='" + yearsString[i] +"'"); 
                                if (yearsString[i].equals(profileForm.getBirthYear()))
                                out.print(" selected ");
                                out.print(">");
                                out.print(yearsString[i] + "</option>");
                                }
                                %>
                            </html:select>    <br>
                            <html:errors property="birthdayString"/>
                        </logic:equal>   
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="birthdayString" filter="true"/> 
                        </logic:notEqual>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">City :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="city" size="20" maxlength="30"/>    
                            <br>
                            <html:errors property="city"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="city" filter="true"/> 
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">State :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:select styleClass="inputvalue" name="ProfileForm" property="state">
                                <option value="NA">Please Select</option>
                                <%
                                for(int i=0;i<stateList.size();i++)
                                {  
                                String[] state = (String[])stateList.get(i);
                                out.print("<option value='" + state[0] +"'"); 
                                if (state[0].equals(profileForm.getState()))
                                out.print(" selected ");
                                out.print(">");
                                out.println(state[1] + "</option>");
                                }
                                %>
                            </html:select>       
                            <br>
                            <html:errors property="state"/>
                        </logic:equal>    
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="stateString" filter="true"/> 
                        </logic:notEqual>                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Country :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:select styleClass="inputvalue" name="ProfileForm" property="nationality">
                                <%
                                for(int i=0;i<countryList.size();i++)
                                {  
                                String[] country = (String[])countryList.get(i);
                                out.print("<option value='" + country[0] +"'"); 
                                if (country[0].equals(profileForm.getNationality()))
                                out.print(" selected ");
                                out.print(">");
                                out.println(country[1] + "</option>");
                                }
                                %>
                            </html:select>     
                            <br>
                            <html:errors property="nationality"/> 
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="nationalityString" filter="true"/> 
                        </logic:notEqual>                         
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Current Location :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="currentLocation" maxlength="100" size="50"/> 
                            <html:errors property="currentLocation"/>
                        </logic:equal>       
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="currentLocation" filter="true"/> 
                        </logic:notEqual>                  
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Subscribe newsletter + friends update :&nbsp;
                    </td>
                    <td align="left"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:radio styleClass="inputvalue" name="ProfileForm" property="subscribe" value="Y" /> Yes
                            <html:radio styleClass="inputvalue" name="ProfileForm" property="subscribe" value="N" /> No
                            <br>
                            <html:errors property="subscribe"/>
                        </logic:equal>                
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="subscribe" filter="true"/> 
                        </logic:notEqual>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Schools :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="schools" maxlength="100" size="50"/> 
                            <html:errors property="schools"/>
                        </logic:equal>            
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="schools" filter="true"/> 
                        </logic:notEqual>                
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Occupation :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="occupation" maxlength="100" size="50"/> 
                            <html:errors property="occupation"/>
                        </logic:equal>            
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="occupation" filter="true"/> 
                        </logic:notEqual>                
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Company :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="company" maxlength="100" size="50"/> 
                            <html:errors property="company"/>
                        </logic:equal>           
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="company" filter="true"/> 
                        </logic:notEqual>                    
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right">Email for display :&nbsp;
                    </td>
                    <td align="left">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="publicEmail" maxlength="200" size="50" />  
                            <html:errors property="publicEmail"/>
                        </logic:equal>                     
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <bean:write name="ProfileForm" property="publicEmail" filter="true"/> 
                        </logic:notEqual>         
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><br><br><br>
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=yesString%>">
                            <input class="formbuttons" type="button" name="save" value="Save" onclick="formSubmit();">
                        </logic:equal>                      
                    </td>
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;
                    </td>
                </tr>
        </table></div> 
        <input type="hidden" name="act" value="mainSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
