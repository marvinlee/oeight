<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="com.esmart2u.solution.base.helper.PropertyConstants" %>
  
  <%
  String booleanYes = "" + PropertyConstants.BOOLEAN_YES;
  %>
    <html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true">
        <div id="details" class="divBox">  
            <table width="80%">
                <COL width="5%"> 
                <COL width="40%"> 
                <COL width="55%">
                <tr>
                    <td colspan="3">&nbsp;</td> 
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_1" colspan="2" align="left"><h1>Profile - Contacts</h1>
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="hdr_m" colspan="2" align="left"> 
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">Phone :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="contactPhone" size="20" maxlength="15"/>    
                            <html:errors property="contactPhone"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="contactPhone" filter="true"/> 
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My Google id :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="contactGoogle" size="20" maxlength="100"/> eg. yourId@gmail.com
                            <html:errors property="contactGoogle"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="contactGoogle" filter="true"/> 
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My Yahoo id :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="contactYahoo" size="20" maxlength="100"/> eg. yourId@yahoo.com    
                            <html:errors property="contactYahoo"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="contactYahoo" filter="true"/> 
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My MSN id :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="contactMsn" size="20" maxlength="100"/> eg. yourId@msn.com    
                            <html:errors property="contactMsn"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="contactMsn" filter="true"/> 
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My Blog :&nbsp; 
                        <logic:equal name="ProfileForm" property="webBlogFeature" value="true">
                            <br>
                            &nbsp;  
                        </logic:equal>   
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                        	<a href="http://<bean:write name="ProfileForm" property="webBlog" filter="true"/>" target="_blank"> 
                                    http://<bean:write name="ProfileForm" property="webBlog" filter="true"/> 
                            </a> &nbsp; &nbsp; &nbsp; 
                            <b><a href="/blog.do?act=update">Manage Blog</a></b>
                        	<%--
                            http://<html:text styleClass="inputvalue" name="ProfileForm" property="webBlog" size="30" maxlength="200"/>    
                            <br>
                            <html:checkbox styleClass="inputvalue" name="ProfileForm" property="webBlogFeature"/> &nbsp; <b>Feature My Blog!</b>
                            <html:errors property="webBlog"/>
                             --%>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <logic:notEmpty name="ProfileForm" property="webBlog">
                                <a href="http://<bean:write name="ProfileForm" property="webBlog" filter="true"/>" target="_blank"> 
                                    http://<bean:write name="ProfileForm" property="webBlog" filter="true"/> 
                                </a>  
                            	<a href="/blog.do">More</a>
                        	<%--
                                <logic:equal name="ProfileForm" property="webBlogFeature" value="true">
                                    <br>
                                    &nbsp; <font size="small"><b>Feature My Blog!</b></font>
                                </logic:equal>   
                             --%>
                            </logic:notEmpty>  
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My Friendster :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <nobr>http://profiles.friendster.com/<html:text styleClass="inputvalue" name="ProfileForm" property="webFriendster" size="20" maxlength="200"/></nobr>
                            <html:errors property="webFriendster"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <logic:notEmpty name="ProfileForm" property="webFriendster">
                                <a href="http://profiles.friendster.com/<bean:write name="ProfileForm" property="webFriendster" filter="true"/>" target="_blank">
                                    http://profiles.friendster.com/<bean:write name="ProfileForm" property="webFriendster" filter="true"/>
                                </a> 
                            </logic:notEmpty>  
                        </logic:notEqual>                   
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My Facebook :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <nobr>http://www.facebook.com/profile.php?id=<html:text styleClass="inputvalue" name="ProfileForm" property="webFacebook" size="20" maxlength="200"/></nobr>    
                            <html:errors property="webFacebook"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <logic:notEmpty name="ProfileForm" property="webFacebook">
                                <a href="http://www.facebook.com/profile.php?id=<bean:write name="ProfileForm" property="webFacebook" filter="true"/>" target="_blank"> 
                                    http://www.facebook.com/profile.php?id=<bean:write name="ProfileForm" property="webFacebook" filter="true"/> 
                                </a>  
                            </logic:notEmpty>  
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">MySpace URL :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <nobr>http://www.myspace.com/<html:text styleClass="inputvalue" name="ProfileForm" property="webMyspace" size="20" maxlength="200"/></nobr>    
                            <html:errors property="webMyspace"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <logic:notEmpty name="ProfileForm" property="webMyspace">
                                <a href="http://www.myspace.com/<bean:write name="ProfileForm" property="webMyspace" filter="true"/>" target="_blank"> 
                                    http://www.myspace.com/<bean:write name="ProfileForm" property="webMyspace" filter="true"/> 
                                </a>  
                            </logic:notEmpty>  
                        </logic:notEqual>                   
                    </td>
                </tr>                
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My Hi5 :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <nobr>http://www.hi5.com/friend/<html:text styleClass="inputvalue" name="ProfileForm" property="webHi5" size="20" maxlength="200"/></nobr>    
                            <html:errors property="webHi5"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <logic:notEmpty name="ProfileForm" property="webHi5">
                                <a href="http://www.hi5.com/friend/<bean:write name="ProfileForm" property="webHi5" filter="true"/>" target="_blank">
                                    http://www.hi5.com/friend/<bean:write name="ProfileForm" property="webHi5" filter="true"/>
                                </a>  
                            </logic:notEmpty>  
                        </logic:notEqual>                   
                    </td>
                </tr>  
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My Wayn :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <nobr>http://www.wayn.com/waynprofile.html?member_key=<html:text styleClass="inputvalue" name="ProfileForm" property="webWayn" size="20" maxlength="200"/></nobr>    
                            <html:errors property="webWayn"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <logic:notEmpty name="ProfileForm" property="webWayn">
                                <a href="http://www.wayn.com/waynprofile.html?member_key=<bean:write name="ProfileForm" property="webWayn" filter="true"/>" target="_blank"> 
                                    http://www.wayn.com/waynprofile.html?member_key=<bean:write name="ProfileForm" property="webWayn" filter="true"/> 
                                </a>  
                            </logic:notEmpty>  
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top">My Orkut :&nbsp;
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <nobr>http://www.orkut.com/Profile.aspx?uid=<html:text styleClass="inputvalue" name="ProfileForm" property="webOrkut" size="20" maxlength="200"/></nobr>    
                            <html:errors property="webOrkut"/>
                        </logic:equal>  
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <logic:notEmpty name="ProfileForm" property="webOrkut">
                                <a href="http://www.orkut.com/Profile.aspx?uid=<bean:write name="ProfileForm" property="webOrkut" filter="true"/>" target="_blank"> 
                                    http://www.orkut.com/Profile.aspx?uid=<bean:write name="ProfileForm" property="webOrkut" filter="true"/> 
                                </a>  
                            </logic:notEmpty>  
                        </logic:notEqual>                   
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="web1" maxlength="100" size="20"/>   
                            <html:errors property="web1"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="web1" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="webValue1" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="webValue1"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="webValue1" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="web2" maxlength="100" size="20"/>   
                            <html:errors property="web2"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="web2" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="webValue2" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="webValue2"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="webValue2" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="web3" maxlength="100" size="20"/>   
                            <html:errors property="web3"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="web3" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="webValue3" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="webValue3"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="webValue3" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="web4" maxlength="100" size="20"/>   
                            <html:errors property="web4"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="web4" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="webValue4" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="webValue4"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="webValue4" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="inputlabel" align="right" valign="top"> 
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:text styleClass="inputvalue" name="ProfileForm" property="web5" maxlength="100" size="20"/>   
                            <html:errors property="web5"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="web5" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                    <td align="left" valign="top">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <html:textarea styleClass="inputvalue" name="ProfileForm" property="webValue5" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                            <html:errors property="webValue5"/>
                        </logic:equal>
                        
                        <logic:notEqual name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <bean:write name="ProfileForm" property="webValue5" filter="true"/> 
                        </logic:notEqual> 
                    </td>
                </tr>   
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <input class="formbuttons" type="button" name="save" value="Save" onclick="formSubmit();">
                        </logic:equal>  
                        
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                
        </table></div> 
        <input type="hidden" name="act" value="contactSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
