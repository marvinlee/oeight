 <%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<tiles:insert page="/tiles/template.jsp" flush="true">
   <tiles:put name="title" type="string" value="Profile Abuse Report" />
   <tiles:put name="header" value="/tiles/top.jsp" />
   <tiles:put name="javascript" value="/profile/javascript.jsp" />
   <tiles:put name="menu" value="/tiles/empty.jsp" />
   <tiles:put name="body" value="/profile/reported_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>