<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 
<%
//Added reason for refreshing a page problem
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "no-cache");
            response.addIntHeader("Expires", -1);
%>
<tiles:insert page="/tiles/template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Photos - Integrate Flickr" />
   <tiles:put name="header" value="/tiles/top.jsp" />
   <tiles:put name="javascript" value="/profile/photo_flickr_js.jsp" />
   <tiles:put name="onLoad" value="fjb.init();" />
   <tiles:put name="menu" value="/profile/menu.jsp" />
   <tiles:put name="body" value="/profile/photo_flickr_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>