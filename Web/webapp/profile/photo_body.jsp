<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.StringUtils,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.member.web.struts.controller.ProfileForm"%>    
  
<%
    ProfileForm profileForm = (ProfileForm)request.getAttribute("ProfileForm");  
	String booleanYes = "" + PropertyConstants.BOOLEAN_YES;
%>

    <html:form name="ProfileForm" type="com.esmart2u.oeight.member.web.struts.controller.ProfileForm" method="post" action="/profile.do" isRelative="true" enctype="multipart/form-data">
        <div id="photoLayer" class="divBox">   
            <table width="400">
                <COL width="40%"> 
                <COL width="20%">
                <COL width="40%"> 
                <tr>
                    <td class="lbl" align="center"> 
                        
                    </td>
                    <td align="left" colspan="2"> 
                        
                    </td>
                </tr>   
                <tr>
                    <td class="hdr_1" align="left"><h1>Photos</h1>
                    </td>
                    <td class="formbuttonsCell" colspan="2" align="right"><a href="/profile.do?act=flickrEdit">Integrate Flickr</a>
                    </td>
                </tr>
                <tr>
                    <td class="hdr_m" colspan="3" align="left"><br><br>
                    </td>
                </tr> 
                 
                <tr>
                    <td class="inputvalue" colspan="3"  align="left">
                        <table width="100%">
                            <tr valign="top">
                                <td width="10%">*Note :&nbsp;</td>
                                <td width="90%"> 
                          This photo will be displayed on our 080808 mosaic after submission. Photo file can be of BMP, GIF or JPG.          
                          Your photo file size should not exceed 250Kb. We recommend a square photo with at least 160px(width) and 160px(height).<br>
                          You can use any imaging tools, eg MsPaint and resize it with "Stretch / Scale" to reduce the actual size.
                        <%-- If you have problem resizing your photo, please read this guide --%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="inputlabel" align="center"> 
                        <br><br><b>Mosaic Photo</b><br>
                        <logic:notEmpty name="ProfileForm" property="photoLargePath">
                            <a href="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" mce_href="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" rel="lightbox"><img src="/vphotos/<bean:write name="ProfileForm" property="photoLargePath"/>" width="240px"></a>  <br>
                            <span class="inputvalue" align="center"><bean:write name="ProfileForm" property="photoDescription" filter="true"/></span>  <br><br> 
                        </logic:notEmpty>
                        <logic:empty name="ProfileForm" property="photoLargePath"><br>
                            Please upload a photo<br>
                        </logic:empty>
                    </td>
                    <td class="inputlabel" align="left" colspan="2">  
                        
                        Change Photo: <html:file styleClass="inputvalue" name="ProfileForm" property="photoFile"/>   
                        <br>
                        <html:errors property="photoPath"/>   
                        <br>
                        
                        <html:textarea styleClass="inputvalue" name="ProfileForm" property="photoDescription" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <br>
                        <html:errors property="photoDescription"/>   
                    </td>
                </tr>  
                <tr>
                    <td class="inputlabel" align="center"> 
                        <br><br><b>Profile Photo</b><br>
                        <logic:notEmpty name="ProfileForm" property="profilePhotoLargePath">
                            <a href="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" mce_href="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" rel="lightbox"><img src="/vphotos/<bean:write name="ProfileForm" property="profilePhotoLargePath"/>" width="240px"></a>  <br>
                            <span class="inputvalue" align="center"><bean:write name="ProfileForm" property="profilePhotoDescription" filter="true"/></span> <br><br> 
                        </logic:notEmpty>
                        <logic:empty name="ProfileForm" property="profilePhotoLargePath"><br>
                            You can upload an additional photo for your profile<br>
                        </logic:empty>
                    </td>
                    <td class="inputlabel" align="left" colspan="2"> 
                        <logic:empty name="ProfileForm" property="profilePhotoLargePath">
                            Upload Photo:
                        </logic:empty>
                        <logic:notEmpty name="ProfileForm" property="profilePhotoLargePath">
                            Change Photo:
                        </logic:notEmpty>
                        <html:file styleClass="inputvalue" name="ProfileForm" property="profilePhotoFile"/>   
                        <br>
                        <html:errors property="profilePhotoPath"/>   
                        <br>
                        
                        <html:textarea styleClass="inputvalue" name="ProfileForm" property="profilePhotoDescription" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <br>
                        <html:errors property="profilePhotoDescription"/>   
                    </td>
                </tr> 
                
                <%-- HIDE TSHIRT PHOTO
                <tr>
                    <td class="lbl" align="center"> 
                        <br><br><b>T-shirt Photo</b><br>
                        <logic:notEmpty name="ProfileForm" property="shirtPhotoLargePath">
                            <img src="/vphotos/<bean:write name="ProfileForm" property="shirtPhotoLargePath"/>" width="240px">  <br>
                            <bean:write name="ProfileForm" property="shirtPhotoDescription"/> <br><br> 
                        </logic:notEmpty>
                        <logic:empty name="ProfileForm" property="shirtPhotoLargePath"><br>
                            Please upload a photo of you in our shirt, exclusively for VIP members<br>
                        </logic:empty>
                    </td>
                    <td align="left" colspan="2">   
                        <logic:empty name="ProfileForm" property="shirtPhotoLargePath">
                            Upload Photo:
                        </logic:empty>
                        <logic:notEmpty name="ProfileForm" property="shirtPhotoLargePath">
                            Change Photo:
                        </logic:notEmpty>
                        <html:file name="ProfileForm" property="shirtPhotoFile"/>   
                        <br>
                        <html:errors property="shirtPhotoPath"/>   
                        <br>
                        
                        <html:textarea name="ProfileForm" property="shirtPhotoDescription" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                        <br>
                        <html:errors property="shirtPhotoDescription"/>   
                    </td>
                </tr>--%>
                <tr class="formbuttonscell">
                    <td>&nbsp;
                    </td>
                    <td class="formbuttonscell" colspan="2">
                        <logic:equal name="ProfileForm" property="requestEdit" value="<%=booleanYes%>">
                            <input class="formbuttons" type="button" name="save" value="Update" onclick="formSubmit();">
                        </logic:equal>                     
                        
                    </td>
                </tr>
                
        </table></div> 
        <input type="hidden" name="act" value="photoSubmitted">
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>    
