<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3" width="600">
        <table width="600">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">
<br><br>
               <h1>080808.com.my launches an online wedding photo mosaic </h1>
        <br>
        <div id="guidelayer" class="divBox">
          <table width="100%" border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0">
            <tr>
              <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
                  <tr bgcolor="#333333">
                    <td colspan="2"><span class="infoBar">080808 Wedding Mosaic</span></td>
                  </tr>
                  <tr bgcolor="#FFFFFF" align="right">
                    <td colspan="2" align="right">
                      <!-- AddThis Bookmark Button BEGIN -->
                      <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/latest/wedding_mosaic_press_release.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script>
                      <script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                      <!-- AddThis Bookmark Button END -->
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2"> <a href="http://080808.com.my/wedding.do"><img src="http://080808.org.my/wedding/img/wedding_banner.gif" width="485" alt="080808 Wedding Mosaic" title="080808 Wedding Mosaic"/></a> <br>
                        <br>
                        <p>The number of weddings and registration of marriages in a single day are expected to peak on 8th August 2008. Many couples would like to choose a special or auspicious date for their wedding date (and anniversary celebration in subsequent years). A special date like 080808 is definitely a not-to-be-missed date as it happens only once in a century. To illustrate this, even the opening ceremony of Beijing Olympics is set on 080808. </p>
                        <p>&nbsp; </p>
                        <p><em>080808.com.my </em> launches an online wedding photo mosaic; the mosaic is an effort to invite &amp; unite couples from around the world registering their marriage/holding their wedding customs on 8th August 2008 in forming a single beautiful &amp; memorable wedding mosaic photo . This initiative aims to become the first in the world '080808 wedding mosaic' and provides 100 lucky couples to showcase their memorable wedding photos on the main wedding mosaic; remaining entries from other couples will be listed in the 080808 weddings listing. (There are no limits to the number of entries <em>080808.com.my </em> will be receiving. We welcome all entries and endeavour to publish all entries received) </p>
                        <p>Site visitors will be able to view 080808 weddings in a single page. By clicking each individual photo on the mosaic, a larger version of the wedding photo will be displayed while enabling site visitors to view the details of the wedding as submitted by the couple. </p>
                        <p>&nbsp; </p>
                        <p>This initiative by <em>080808.com.my </em> is truly in line with its theme to 'make a mark' by enabling 080808 wedding couples to make a special mark for themselves online. </p>
                        <p>&nbsp; </p>
                        <p>Other ongoing initiatives by <em>080808.com.my </em> to 'make a mark' include an environmental pledge campaign. This pledge campaign aims to spread awareness of climate change affecting the world and how we can each play our part in preserving the environment. Since its launch recently, it has successfully attracted hundreds of visitors from across the world to its site and to participate in its Pledge Room. </p></td>
                  </tr>
                  <tr bgcolor="#FFFFFF" align="right">
                    <td colspan="2" align="right">
                      <!-- AddThis Bookmark Button BEGIN -->
                      <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/latest/wedding_mosaic_press_release.jsp';     
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script>
                      <script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                      <!-- AddThis Bookmark Button END -->
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="5%">&nbsp;</td>
                    <td><p><em>For more information, visit <a href="http://080808.com.my/wedding.do">http://080808.com.my/wedding.do</a></em><br>
                    </p></td>
                  </tr>
              </table></td>
              <td width="15%">&nbsp;</td>
            </tr>
          </table></div>
    </td>                          
                </tr>
            </table>
    </td>                                  
