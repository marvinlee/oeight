<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3" width="600">
        <table width="600">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">
<br><br>
               <h1> 080808.com.my launches environmental pledge campaign on climate change</h1>
        <br>
        <div id="guidelayer" class="divBox">
          <table width="100%" border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0">
            <tr>
              <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
                  <tr bgcolor="#333333">
                    <td colspan="2"><span class="infoBar">080808 Climate Change campaign </span></td>
                  </tr>
                  <tr bgcolor="#FFFFFF" align="right">
                    <td colspan="2" align="right">
                      <!-- AddThis Bookmark Button BEGIN -->
                      <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/latest/climate_change_press_release.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script>
                      <script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                      <!-- AddThis Bookmark Button END -->
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2"> <a href="http://earth.080808.com.my/"><img src="http://080808.org.my/earth08/08Climate.jpg" width="485" alt="080808 Climate Change Pledge" title="080808 Climate Change Pledge"/></a> <br>
                        <br>
                        <p>Climate change is not about our day being sunny or not. Climate change is a long term effect on Earth, changing the weather we usually experience and affecting Earth's population. Climate change has long been identified. However, it is until recent years that climate change is known to be the result of human activities rather than due to natural changes in the atmosphere. </p>
                        <p>&nbsp; </p>
                        <p>The main contributor to climate change is greenhouse gases. This primary cause of climate change has a delayed effect, likened as a disease with a long incubation period. There could have already been damages done which is not reversible by now, but if we do not take action now and stop releasing more CO2 (the main ‘greenhouse gas'), the effect could be more devastating. </p>
                        <p>&nbsp; </p>
                        <p>While the list of issues resulted by climate change can be endless, there are also many ways where humans can help adapt to climate change and slow it down. We can take various initiatives at home, from reducing waste to changing the way we transport to work or even joining an environmental organization. </p>
                        <p>&nbsp; </p>
                        <p><em>080808.com.my </em> has created a Pledge Room for everyone to know that even small actions can contribute in helping planet Earth. The environmental campaign aims to spread awareness of climate change affecting the world and how we can each play our part in preserving the environment. Since its launch recently, it has successfully attracted hundreds of visitors from around the world to its site and to participate in its Pledge Room. </p>
                        <p>&nbsp; </p>
                        <p>To encourage further participation in this environmental campaign, <em>080808.com.my </em> is giving out ecobuttons as prizes for lucky winners who share the pledge campaign with their friends or blogs about climate change. </p>
                        <p>&nbsp; </p>
                        <p>This initiative by <em>080808.com.my </em> is truly in line with its theme to 'make a mark', enabling everyone who pledge to make their pledge counts towards a better future. </p></td>
                  </tr>
                  <tr bgcolor="#FFFFFF" align="right">
                    <td colspan="2" align="right">
                      <!-- AddThis Bookmark Button BEGIN -->
                      <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/latest/climate_change_press_release.jsp';     
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script>
                      <script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                      <!-- AddThis Bookmark Button END -->
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="5%">&nbsp;</td>
                    <td><p><em>For more information, visit <a href="http://earth.080808.com.my">http://earth.080808.com.my</a></em><br>
                    </p></td>
                  </tr>
              </table></td>
              <td width="15%">&nbsp;</td>
            </tr>
          </table></div>
    </td>                          
                </tr>
            </table>
    </td>                                  
