<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
 
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3" width="600">
        <table width="600">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">
<br><br>
                <h1>080808.com.my in support of Malaysian Artistes for Unity - Here In My Home</h1><br> 

                    <div id="guidelayer" class="divBox"> 
                        <table width="100%" border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
                          <tr>
    <td><table width="100%" border="0" cellpadding="5" cellspacing="0">
      <tr bgcolor="#333333">
        <td colspan="2"><span class="infoBar"> Here In My Home </span></td>
        </tr>
        
         
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/latest/malaysian_artistes_for_unity.jsp';   
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
        
      <tr>
        <td colspan="2">
             <a href="http://www.malaysianartistesforunity.info" target="_blank"><img src="http://www.malaysianartistesforunity.info/wp-content/uploads/2008/05/mafu_home_animated.gif" width="485" height="72" alt="free download" title="free download"/></a>

             <br><br>
          <p>Now, this is what real Malaysians are about.<br></p>
          <p>Undivided and together, a group of Malaysian artistes created a song for unity.</p>
          <p>080808.com.my supports this spirit and invite everybody to do the same.</p>
          <p>Let's do this, one by one, one by one as ONE.<strong>  
              </td>
        </tr>
      
      <tr>
            <td colspan="2"> <center>
                <strong>The Video: </strong><br>
                <object type="application/x-shockwave-flash" width="400" height="350" 
                wmode="transparent" data="flvplayer.swf?file=himh/himh336_272.flv&autoStart=false"> 
                <param name="movie" value="flvplayer.swf?file=himh/himh336_272.flv&autoStart=false" /> 
                <param name="wmode" value="transparent" /> 
                </object>
                </center>
            </td>
        </tr>  
      <tr>
            <td colspan="2"> <center>
                <strong><h1 style="color: red">NEW!</h1>The Making of The Video: </strong><br>
                <object type="application/x-shockwave-flash" width="400" height="350" 
                wmode="transparent" data="flvplayer.swf?file=himh/making_320X240.flv&autoStart=false"> 
                <param name="movie" value="flvplayer.swf?file=himh/making_320X240.flv&autoStart=false" /> 
                <param name="wmode" value="transparent" /> 
                </object>
                </center>
            </td>
        </tr>

           <tr>
        <td colspan="2">
            <br> <center> 
          <p><strong> <a href="http://www.malaysianartistesforunity.info" target="_blank">Malaysian Artistes for Unity</a> </strong></p><br><br>
          <p><strong> <a href="http://www.malaysianartistesforunity.info/?page_id=19" target="_blank">Song downloads</a> </strong></p> </center></td>
          
        </tr>
        
        <tr> 
            <td colspan="2"> <center> 
                <strong>Words &amp; Music:</strong> Pete Teo<br>
		<strong>Artist: </strong>Malaysian Artistes<br>
		<strong>Song: </strong>HERE IN MY HOME
		<p>Hold on brother hold on<br>
		  The road is long. We&rsquo;re on stony ground<br>
		  But I&rsquo;m strong. You ain&rsquo;t heavy</p>
		<p>Oh there&rsquo;s a misspoken truth that lies<br>
		  Colors don&rsquo;t bind, oh no.<br>
		  What do they know? They speak falsely.</p>
		<p>Chorus:<br>
		  Here in my home<br>
		  I&rsquo;ll tell you what its all about<br>
		  There&rsquo;s just one hope here in my heart<br>
		  One Love undivided<br>
		  That&rsquo;s what it&rsquo;s all about<br>
		  Please won&rsquo;t you fall in one by one by one [with me]?</p>
		<p>Push back sister won&rsquo;t you push back?<br>
		  Love won&rsquo;t wait. Just keep pushing on.<br>
		  Yes I&rsquo;m strong. You ain&rsquo;t heavy.</p>
		<p>Oh don&rsquo;t you worry about that&hellip;<br>
		  What we have the shadows can&rsquo;t deny<br>
		  Don&rsquo;t you know it&rsquo;s now or never?</p>
		<p>Rap:</p>
		<p>[Bahasa Malaysia]<br>
Bertubi asakan berkurun lamanya <br>
Hati ke depan mencari yang sayang <br>
[Years of fears and years of tribulation <br>
The heart keeps searching for that endless devotion] <br>
<br>
[Mandarin]<br> 
shou qian shou da jia yi qi zou, <br>
wo dai biao guo ren kai kou wei lai jiu mei you, you chou <br>
[Hand in hand we'll march like blood brothers <br>
I speak for my people, hope we'll find peace forever] <br>
<br>
[Tamil]<br> 
inthe payanam payanamm yen vettri thaagam <br>
anthee kaana kaalam naam vetri raagam...nanba nanba <br>
[May the road ahead quench my thirst for success <br>
May the road behind echo a song of the blessed] </p>
		<p>[English]<br>
		</p>
		<p>So I will let it be known yes I feel it in my bones<br> 
		 No matter where I roam this is home sweet home<br>
		</p>
		<p>&nbsp;</p>
<p>Sing!</p>
                <br><br><br> 
                <a href="http://www.malaysianartistesforunity.info" target="_blank"><img src="http://www.malaysianartistesforunity.info/wp-content/uploads/2008/05/mafu_animated.gif" width="485" height="72" alt="free download" title="free download"/></a>
<br><br></center> 
            </td> 
        </tr>
      
      <tr bgcolor="#FFFFFF" align="right">
        <td colspan="2" align="right">  
                    <!-- AddThis Bookmark Button BEGIN -->
                    <script type="text/javascript">
                                  addthis_url    = 'http://080808.com.my/latest/malaysian_artistes_for_unity.jsp';     
                                  addthis_title  = document.title;  
                                  addthis_pub    = 'marvinlee';     
                    </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
                    <!-- AddThis Bookmark Button END -->
            </td>
        </tr> 
        
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="5%">&nbsp;</td>
        <td><p><br> 
          </p>
          </td>
      </tr>
                    </table>
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table></div>
    </td>                          
                </tr>
            </table>
    </td>                                  
