<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%>
 
<%-- This page needs total 5 columns instead of 4--%>
    <td colspan="3">
        <table width="100%">
            <tr>  
    
    <td width="5%">&nbsp;</td>

    <td width="20%" valign="top">

    <h1>Login Page</h1>
    
     <% 
        
        // Show login and ads only for those not logged-on
        boolean notLoggedIn = (session==null || !StringUtils.hasValue((String)session.getAttribute("userId")));
        if (notLoggedIn)
        { 
    %>
    
        <html:form name="LoginForm" type="com.esmart2u.oeight.member.web.struts.controller.LoginForm" method="post"  action="/secure/login.do" isRelative="true">
            
        <div id="loginlayer" class="divBox"> 
        <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
            <tr> 
                <td align="left" class="inputvalue" colspan="2"><img src="/images/login/login_title.gif">
                </td>
            </tr>
            <tr>
                <td align="right" class="inputlabel" >
                Email Address:
                </td>
                <td align="left" class="inputvalue" >
                <!--input type="text" name="login" size="10"/-->
                <html:text name="LoginForm" styleClass="inputvalue" property="login" size="20" maxlength="50"/>
                     <html:errors property="login"/>
                </td>
            </tr>
            <tr>
                <td align="right"  class="inputlabel">
                Password:
                </td>
                <td align="left" >
                <input type="password" class="inputvalue"  name="password" size="20" maxlength="20" onkeydown="if(event.keyCode==13)formSubmit();" />
                    <html:errors property="password"/>
                    <html:errors property="passwordMismatch"/>
                </td>
            </tr> 
            <tr class="formbuttonsCell" > 
              <td colspan="2"  class="formbuttonsCell" >
                    <input type="button" class="formbuttons" name="Login" value="Login" onclick="formSubmit();"/>
                </td> 
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <a href="/register.do?act=new">Join</a> | 
                    <a href="/secure/login.do?act=reset">Login Help</a> </td>
            </tr> 
        </table>  
        </div>      
            
        <input type="hidden" name="act" value="login">
            
        </html:form> 
        <%
        }// Ends hide for logged on user
        %> 
       <br><br> 
       <div id="adlayer1" class="divBox">            
         <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="280" height="280">
  <param name="movie" value="/ads/280-280.swf">
  <param name="quality" value="high">
  <embed src="/ads/280-280.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="160" height="160"></embed>
</object>  
        </div>
        
  
                                </td>
                                <td width="5%" valign="top"></td> 
				<td width="60%" align="left">
				<br><br><br>
                                <div id="tourlayer" class="divBox" style="width:480px;height:380px">     
                                    <table>
                                        <tr>
                                            <td colspan="3"   class="tourButtonsCell">
                                                <img src="/images/tour/takeTour.gif" width="295" height="45" align="middle">
                                            </td>
                                        </tr>
                                        <tr> 
                                            <td width="15%" valign="bottom"></td> 
                                            <td width="292">
                                                <p><strong>What </strong></p>
                                                <p>The main objective of this site is a creation of mosaic comprising of many faces on the date of 8 th August 2008. </p>
                                                <p>While we are on the way counting down, we are also allowing members to preview what it actually looks like when we open the doors for members to upload their 080808 photo. </p>
                                                <p>The 080808.com.my homepage will display the mosaic of many thumbnail versions of our members' photo. </p>
                                                <p>There will be a period of time where we allow members to upload their 080808 photo which we will be announcing at a later date. </p>
                                                <br><br><br>
                                            </td>
                                            <td width="76" valign="bottom"><img src="/images/tour/girl.gif" width="76" height="170" align="right"></td>
                                        </tr>
                                        <tr class="tourButtonsCell">
                                            <td colspan="3"  class="tourButtonsCell"><a href="/register.do?act=new"><img src="/images/tour/joinNow.gif" width="200" height="45" border="0" align="middle"></a><a href="/tour.do?act=why"><img src="/images/tour/right.gif" width="48" height="42" border="0"  align="middle"></a> 
                                            </td>
                                        </tr>
                                </table></div> 
				 
				</td> 
   
            </tr>
        </table>
    </td>                                