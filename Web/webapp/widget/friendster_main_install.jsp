<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%>  
<html>
    <head>
        <title>
            Friendster - My oEight App Install Page
        </title>
        <SCRIPT LANGUAGE="Javascript">
var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("login", "Email Address", "M", true);
	fieldList[counter++] = new Array("password", "Password", "M", true); 

	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);
	var emailMsg = checkEmail(form.login.value, "Email Address")
	if (emailMsg != '')
	{
		if (errMsg != '')
		{
			errMsg += "\n"
		}
		errMsg += emailMsg
	}

	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	} 
		posted = true;
		document.forms[0].submit();
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 
<link href="/css/style.css" rel="stylesheet" type="text/css">
<link REL="SHORTCUT ICON" HREF="/images/favicon.ico">
    </head>    
<body bgcolor="#FFFFFF"> 
<br><br><br> 
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="50%" bordercolor="#000000" bgcolor="#FFFFFF">
        <tr><td><center>
                <html:form name="FriendsterForm" type="com.esmart2u.oeight.member.web.struts.controller.FriendsterForm" method="post"  action="/friendster.do" isRelative="true">
                    
                    <h1 style="color: darkblue">To add My oEight App, Login to your 080808.com.my Profile here.</h1><br><br>
                    <html:errors property="friendsterInput"/> 
                    <table border="0" bgcolor="#FFFFFF" bordercolor="#999999" cellpadding="5" cellspacing="0"> 
                        <tr> 
                            <td align="left" class="inputvalue" colspan="2"><img src="/images/login/login_title.gif">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="inputlabel" >
                                Email Address:
                            </td>
                            <td align="left" class="inputvalue" >
                                <!--input type="text" name="login" size="10"/-->
                                <html:text name="FriendsterForm" styleClass="inputvalue" property="login" size="20" maxlength="50"/>
                                <html:errors property="login"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"  class="inputlabel">
                                Password:
                            </td>
                            <td align="left" >
                                <input type="password" class="inputvalue"  name="password" size="20" maxlength="20" onkeydown="if(event.keyCode==13)formSubmit();" />
                                <html:errors property="password"/>
                                <html:errors property="passwordMismatch"/>
                            </td>
                        </tr> 
                        <tr class="formbuttonsCell" > 
                            <td colspan="2"  class="formbuttonsCell" >
                                <input type="button" class="formbuttons" name="Login" value="Add My oEight App" onclick="formSubmit();"/>
                            </td> 
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                            <a href="/register.do?act=new" target="#">Help, I do not have a login</a></td>
                        </tr> 
                    </table>     
                    
                    <input type="hidden" name="act" value="callback1"> 
                    <input type="hidden" name="fromInstallPage" value="y"> 
                    <html:hidden name="FriendsterForm" property="user_id"/>
                    <html:hidden name="FriendsterForm" property="nonce"/>
                    <html:hidden name="FriendsterForm" property="session_key"/>
                    <html:hidden name="FriendsterForm" property="api_key"/>
                    <html:hidden name="FriendsterForm" property="sig"/>
                </html:form>
        </center></td></tr>
    </table> 
</body>
    
    
</html>