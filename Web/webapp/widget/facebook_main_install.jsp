<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%>   
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.FacebookForm"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%>  
<%
     FacebookForm facebookForm = (FacebookForm)request.getAttribute("FacebookForm");
%>
<html>
    <head>
        <title>
            Facebook - My oEight App Install Page
        </title>
        <SCRIPT LANGUAGE="Javascript">
var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("login", "Email Address", "M", true);
	fieldList[counter++] = new Array("password", "Password", "M", true); 

	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList);
	var emailMsg = checkEmail(form.login.value, "Email Address")
	if (emailMsg != '')
	{
		if (errMsg != '')
		{
			errMsg += "\n"
		}
		errMsg += emailMsg
	}

	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	} 
		posted = true;
		document.forms[0].submit();
}

function reloginMyoEight()
{
    if (window.top!=window.self) {window.top.location='http://www.facebook.com/add.php?api_key=<%=facebookForm.getReloginApiKey()%>&ref=mf'};
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 
<link href="/css/facebook_style.css" rel="stylesheet" type="text/css">
<link REL="SHORTCUT ICON" HREF="/images/favicon.ico">
    </head>    
<body bgcolor="#FFFFFF"> 
<br><br><br> 
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="50%" bordercolor="#000000" bgcolor="#FFFFFF">
        <tr><td><center>
                <html:form name="FacebookForm" type="com.esmart2u.oeight.member.web.struts.controller.FacebookForm" method="post"  action="/facebook.do" isRelative="true">
                    
                    <h1 style="color: darkblue">To add My oEight App, Login to your 080808.com.my Profile here.</h1><br><br>
                    <html:errors property="facebookInput"/> 
                    <%
                        if (StringUtils.hasValue(facebookForm.getReloginApiKey()))
                        {
                        %>
                        <br><font color="red">To add this application, <a href="#http://www.facebook.com/add.php?api_key=<%=facebookForm.getReloginApiKey()%>&ref=mf" onclick="reloginMyoEight();">click here.</a></font>
                        <%
                        }
                        else
                        {
                    %>
                    <table border="0" bgcolor="#FFFFFF" bordercolor="#999999" cellpadding="5" cellspacing="1"> 
                        <tr> 
                            <td align="left" class="inputvalue" colspan="2"><img src="/images/login/login_title.gif">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="inputlabel" >
                                Email Address:
                            </td>
                            <td align="left" class="inputvalue" >
                                <!--input type="text" name="login" size="10"/-->
                                <html:text name="FacebookForm" styleClass="inputvalue" property="login" size="20" maxlength="50"/>
                                <html:errors property="login"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"  class="inputlabel">
                                Password:
                            </td>
                            <td align="left" >
                                <input type="password" class="inputvalue"  name="password" size="20" maxlength="20" onkeydown="if(event.keyCode==13)formSubmit();" />
                                <html:errors property="password"/>
                                <html:errors property="passwordMismatch"/>
                            </td>
                        </tr> 
                        <tr class="formbuttonsCell" > 
                            <td colspan="2"  class="formbuttonsCell" >
                                <input type="button" class="formbuttons" name="Login" value="Add My oEight App" onclick="formSubmit();"/>
                            </td> 
                        </tr>
                        <tr>
                            <td align="right" colspan="2">
                            <a href="/register.do?act=new" target="#">Help, I do not have a login</a></td>
                        </tr> 
                    </table>     
                       <%
                        }
                        %>
                    <input type="hidden" name="act" value="callback1"> 
                    <input type="hidden" name="fromInstallPage" value="y"> 
                    <%--html:hidden name="FacebookForm" property="fb_user"/>
                    <html:hidden name="FacebookForm" property="fb_call_id"/>
                    <html:hidden name="FacebookForm" property="fb_session_key"/>
                    <html:hidden name="FacebookForm" property="api_key"/>
                    <html:hidden name="FacebookForm" property="sig"/--%>
                    <html:hidden name="FacebookForm" property="fb_sig_session_key"/>
                    <html:hidden name="FacebookForm" property="fb_sig_user"/>
                    <html:hidden name="FacebookForm" property="fb_sig_api_key"/>
                    <html:hidden name="FacebookForm" property="auth_token"/>
                </html:form>
        </center></td></tr>
    </table> 
</body>
    
    
</html>