<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.FacebookForm,
                 com.esmart2u.oeight.member.web.struts.helper.OEightPledgeHelper"%>    
<%  
    
    FacebookForm facebookForm = (FacebookForm)request.getAttribute("FacebookForm");
    Vector pledgeList = OEightPledgeHelper.getPledgeList();  
    
%>
<html>
    <head>
        <title>
            Facebook - for Climate Change App Install Page
        </title>
        <SCRIPT LANGUAGE="Javascript">
var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("pledgeCode", "Pledge", "M", true); 

	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList); 

	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	} 
        posted = true;
        document.forms[0].submit();
}

function reloginMyoEight()
{
    if (window.top!=window.self) {window.top.location='http://www.facebook.com/add.php?api_key=<%=facebookForm.getReloginApiKey()%>&ref=mf'};
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 
<link href="/css/facebook_style.css" rel="stylesheet" type="text/css">
<link REL="SHORTCUT ICON" HREF="/images/favicon.ico">
    </head>    
<body bgcolor="#FFFFFF"> 
<br><br><br> 
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="50%" bordercolor="#000000" bgcolor="#FFFFFF">
        <tr><td><center>
                <html:form name="FacebookForm" type="com.esmart2u.oeight.member.web.struts.controller.FacebookForm" method="post"  action="/facebook.do" isRelative="true">
                     
                    <h1 style="color: darkblue">Please select a pledge that suits you best :</h1><br><br>
                    <html:errors property="pledgeCode"/> <html:errors property="facebookInput"/> 
                    <%
                        if (StringUtils.hasValue(facebookForm.getReloginApiKey()))
                        {
                        %>
                        <br><font color="red">To add this application, <a href="#http://www.facebook.com/add.php?api_key=<%=facebookForm.getReloginApiKey()%>&ref=mf" onclick="reloginMyoEight();">click here.</a></font>
                        <%
                        }
                        else
                        {
                    %>
                 <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0">  
                    <tr>
                      <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
                    </tr>  
                         <tr>
                            <td class="inputlabel" align="right"> 
                            </td>
                            <td align="left">
                                <br><br><br>
                            </td>
                        </tr>   
                       <%
            String pledgeCategoryCurrent = "";
            for(int i=0;i<pledgeList.size();i++)
            {  
                String[] pledge = (String[])pledgeList.get(i); 
                String pledgeCode = pledge[0];
                if (!StringUtils.hasValue(pledgeCode)) continue;
                String pledgeCategory = pledgeCode.substring(0,pledgeCode.indexOf("."));
                String pledgeNumber = pledgeCode.substring(pledgeCode.indexOf(".")+1,pledgeCode.length());
            %>
             <tr>
                 <td>&nbsp;</td>
                <td class="inputlabel" align="right">
                    &nbsp;
                    <%
                        if (!pledgeCategory.equals(pledgeCategoryCurrent))
                        {
                            %><br><br></td><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td>
                            <td class="inputlabel" align="right">
                                <%--img src="http://080808.org.my/earth08/img/<%=pledgeCategory%>_m.gif"><br>--%>
                                <img src="/images/earth/<%=pledgeCategory%>_m.gif"><br>
                            
                             </td><td class="inputlabel" align="center"><% 
                                    out.print(pledgeCategory.toUpperCase()); 
                                    pledgeCategoryCurrent = pledgeCategory;                                  
                                %></td>
                            </tr>
                             <tr>
                                 <td>&nbsp;</td>
                                 <td> 
                     <%
                        }
                    %>
                </td>
                <td align="left"> 
                    <input type="radio" name="pledgeCode" value="<%=pledgeNumber%>">  <%=pledge[1]%> 
                </td>
            </tr>
            <%
            }
            %>
            

                        <tr>
                          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
                        </tr>
                        <tr> 
                            <td class="inputlabel" align="right">&nbsp;
                            </td>
                            <td class="inputlabel" align="right">&nbsp;
                            </td>
                            <td align="left" class="formbuttonsCell" > 
                                <input type="button" name="pledge" class="formbuttons" value="Pledge" onclick="formSubmit();">
                            </td>
                        </tr>
                        <tr>
                          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
                        </tr>
                 </table>
                       <%
                        }
                        %>
                    <input type="hidden" name="act" value="callback6"> 
                    <input type="hidden" name="fromInstallPage" value="y">  
                    <html:hidden name="FacebookForm" property="fb_sig_session_key"/>
                    <html:hidden name="FacebookForm" property="fb_sig_user"/>
                    <html:hidden name="FacebookForm" property="fb_sig_api_key"/>
                    <html:hidden name="FacebookForm" property="auth_token"/>
                </html:form>
        </center></td></tr>
    </table> 
</body>
    
    
</html>