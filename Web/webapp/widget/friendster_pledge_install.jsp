<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.helper.OEightConstants"%>
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.FriendsterForm,
                 com.esmart2u.oeight.member.web.struts.helper.OEightPledgeHelper"%>    
<%  
    
    FriendsterForm friendsterForm = (FriendsterForm)request.getAttribute("FriendsterForm");
    Vector pledgeList = OEightPledgeHelper.getPledgeList();  
String fromKey = OEightConstants.SESSION_CAMPAIGN_INVITE_FROM + OEightConstants.CAMPAIGN_INVITE_FROM_FRIENDSTER;
String fromReferral = (String)friendsterForm.getActionContext().getRequest().getSession().getAttribute(fromKey);
System.out.println("INSTALL with referral:" + fromReferral);

%>
<html>
    <head>
        <title>
            Friendster - for Climate Change App Install Page
        </title>
        <SCRIPT LANGUAGE="Javascript">
var posted = false;

function formSubmit()
{
	var counter = 0;
	if (posted)
	{
		alert(requestSubmittedMessage);
		return;
	}

	// client side validation on input fields
	fieldList = new Array();
	fieldList[counter++] = new Array("pledgeCode", "Pledge", "M", true); 

	// validation form is included in js file
	var form = document.forms[0];
	var errMsg = validateForm(form, fieldList); 

	// if content is not empty, this indicates there is message to be alerted and processing shall be discontinued
	if (errMsg != '')
	{
		alert(errMsg);
		return;
	} 
        posted = true;
        document.forms[0].submit();
}
</script>
<script type="text/javascript" src="/js/messages.js"></script>
<script type="text/javascript" src="/js/check.js"></script> 
<link href="/css/earth_style.css" rel="stylesheet" type="text/css">
<link REL="SHORTCUT ICON" HREF="/images/favicon.ico">
    </head>    
<body bgcolor="#FFFFFF"> 
<br><br><br> 
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="50%" bordercolor="#000000" bgcolor="#FFFFFF">
        <tr><td><center>
                <html:form name="FriendsterForm" type="com.esmart2u.oeight.member.web.struts.controller.FriendsterForm" method="post"  action="/friendster.do" isRelative="true">
                    
                    <table border="0" bgcolor="#FFFFFF" bordercolor="#999999" cellpadding="5" cellspacing="0"> 
                        <tr> 
                            <td align="left" class="inputvalue" colspan="2">
                                 <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
              <tr>
                <td align="center" colspan="3" class="friendster_hdr" >Make A Mark for Climate Change</td>
              </tr> 
           
		<tr>
		  <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
		</tr> 
        
         
             <tr>
                <td class="inputlabel"  colspan="3" align="right"> 
                <h1 style="color: darkblue">Please select a pledge that suits you best :</h1><br><br>
                    <html:errors property="pledgeCode"/> 
                    <html:errors property="friendsterInput"/>  
                </td>
            </tr>   
          <%
            String pledgeCategoryCurrent = "";
            for(int i=0;i<pledgeList.size();i++)
            {  
                String[] pledge = (String[])pledgeList.get(i); 
                String pledgeCode = pledge[0];
                if (!StringUtils.hasValue(pledgeCode)) continue;
                String pledgeCategory = pledgeCode.substring(0,pledgeCode.indexOf("."));
                String pledgeNumber = pledgeCode.substring(pledgeCode.indexOf(".")+1,pledgeCode.length());
            %>
             <tr>
                 <td>&nbsp;</td>
                <td class="inputlabel" align="right">
                    &nbsp;
                    <%
                        if (!pledgeCategory.equals(pledgeCategoryCurrent))
                        {
                            %><br><br></td><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td>
                            <td class="inputlabel" align="right">
                                <%--img src="http://080808.org.my/earth08/img/<%=pledgeCategory%>_m.gif"><br>--%>
                                <img src="/images/earth/<%=pledgeCategory%>_m.gif"><br>
                            
                             </td><td class="inputlabel" align="center"><% 
                                    out.print(pledgeCategory.toUpperCase()); 
                                    pledgeCategoryCurrent = pledgeCategory;                                  
                                %></td>
                            </tr>
                             <tr>
                                 <td>&nbsp;</td>
                                 <td> 
                     <%
                        }
                    %>
                </td>
                <td align="left"> 
                    <input type="radio" name="pledgeCode" value="<%=pledgeNumber%>">  <%=pledge[1]%> 
                </td>
            </tr>
            <%
            }
            %>
            
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>    
            <tr>
                <td align="center" colspan="3" class="content_txt" ><b>OPTIONAL LOGIN</b></td>
            </tr>   
             <tr>
                <td align="center" colspan="3" class="content_txt" >You only need to login to 080808.com.my if you would like to join the contest.</td>
            </tr>
            <tr>
                <td align="center" colspan="3" class="content_txt" >More details here : <a href="http://080808.com.my/earth.do?act=contestEarth" target="_blank">Climate Change Contest</a></td>
            </tr>
            <tr>
                <td align="center" colspan="3" > <center>
                    <table align="center" border="0" bgcolor="#FFFFFF" bordercolor="#999999" cellpadding="5" cellspacing="0"> 
                        <tr> 
                            <td align="left" class="inputvalue" colspan="2"><img src="/images/login/login_title.gif">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="inputlabel" >
                                Email Address:
                            </td>
                            <td align="left" class="inputvalue" >
                                <!--input type="text" name="login" size="10"/-->
                                <html:text name="FriendsterForm" styleClass="inputvalue" property="login" size="20" maxlength="50"/>
                                <html:errors property="login"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"  class="inputlabel">
                                Password:
                            </td>
                            <td align="left" >
                                <input type="password" class="inputvalue"  name="password" size="20" maxlength="20" onkeydown="if(event.keyCode==13)formSubmit();" />
                                <html:errors property="password"/>
                                <html:errors property="passwordMismatch"/>
                            </td>
                        </tr>  
                        <tr>
                            <td align="right" colspan="2">
                            <a href="/register.do?act=new" target="_blank">Join 080808.com.my</a></td>
                        </tr> 
                    </table> </center>    
                </td>
            </tr>
            <tr> 
                <td align="left" class="inputlabel" colspan="2"  > 
                    &nbsp;
                </td>
                <td class="inputlabel" align="right">&nbsp;<center><input type="button" name="pledge" class="formbuttons" value="Pledge" onclick="formSubmit();"></center>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div> 
                            </td>
                        </tr> 
                    </table>     
                    
                    <input type="hidden" name="act" value="callback6"> 
                    <input type="hidden" name="fromInstallPage" value="y"> 
                    <%--html:hidden name="FriendsterForm" property="next"/--%>
                    <html:hidden name="FriendsterForm" property="user_id"/>
                    <html:hidden name="FriendsterForm" property="nonce"/>
                    <html:hidden name="FriendsterForm" property="session_key"/>
                    <html:hidden name="FriendsterForm" property="api_key"/>
                    <html:hidden name="FriendsterForm" property="sig"/>
                </html:form>
        </center></td></tr>
    </table> 
</body>
    
    
</html>