<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.WeddingForm"%>   

 <% 
    WeddingForm weddingForm = (WeddingForm)request.getAttribute("WeddingForm"); 
  
%>
<%-- This page needs total 3 columns instead of 4--%> 
     <table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://080808.com.my/wedding.do?act=register';
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

                <h1 class="h1_hdr">I wed on 080808, register me for the mosaic!</h1> <br><br> 

    <html:form name="WeddingForm" type="com.esmart2u.oeight.member.web.struts.controller.WeddingForm" method="post" scope="request"  action="/wedding.do" isRelative="true"  enctype="multipart/form-data">
    <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" width="10%">&nbsp;</td>
          <td align="center" width="20%">&nbsp;</td>
          <td align="center" width="70%">&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="3" class="content_txt" >My Wedding Information :</td>
        </tr>
        <tr>
          <td align="center" colspan="3" >
                    <html:errors property="weddingError"/>&nbsp;</td>
        </tr>
         
             <tr>
                <td class="inputlabel" align="right"> 
                </td>
                <td align="left">
                     <br><br>
                </td>
            </tr>   
          
            
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Photo Preview :&nbsp;
                </td>
                <td align="left"> 
                    <img alt="Photo preview" id="previewField" src="/images/spacer.gif"> 
                    <br>
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Couple Name :&nbsp;
                </td>
                <td align="left">
                    <html:text name="WeddingForm"  styleClass="inputvalue" property="coupleName" maxlength="50" size="50" />   
                    <html:errors property="coupleName"/> 
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Description :&nbsp;
                </td>
                <td align="left">
                    <html:textarea styleClass="inputvalue" name="WeddingForm" property="weddingDescription" cols="50" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                    <html:errors property="weddingDescription"/> 
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Photo Upload :&nbsp;
                </td>
                <td align="left"> 
                    <html:file styleClass="inputvalue" name="WeddingForm" property="photoFile"  onchange="preview(this)"/>   
                    <html:errors property="photoPath"/>
                </td>
            </tr> 
           
            
            
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr>  
                <td align="center" colspan="3"> 
                    <center><input type="button" name="reserve" class="formbuttons" value="Reserve my space" onclick="formSubmit();"></center>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div>
        <input type="hidden" name="act" value="registerSubmitted">  
    </html:form> 
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table>   