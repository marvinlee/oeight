<!-- Lightbox -->
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />  
<script language="javascript">
function formBack(action)
{
    document.forms[0].act.value=action;
    document.forms[0].submit(); 
}

function formSubmit()
{
    document.forms[0].submit(); 
}    

function browsePage(pageNumber)
{
    document.forms[0].currentPage.value = pageNumber;
    formSubmit();
}
</script>