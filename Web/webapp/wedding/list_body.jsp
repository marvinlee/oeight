<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>  
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.WeddingForm"%>   
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.Page"%>  

 <% 
    WeddingForm weddingForm = (WeddingForm)request.getAttribute("WeddingForm"); 
    
    List resultList = null;
    StringBuffer pagingOutput = null;
    if (weddingForm.getWeddingListPage() != null){
        resultList = weddingForm.getWeddingListPage().getThisPageElements();
    } 
%>  
<html:form name="WeddingForm" type="com.esmart2u.oeight.member.web.struts.controller.WeddingForm" method="get" scope="request"  action="/wedding.do" isRelative="true">
   
<%-- This page needs total 3 columns instead of 4--%> 
      <table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://080808.com.my/wedding.do?act=list';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  
                <h1 class="h1_hdr">080808 Weddings</h1> <br><br>

     <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr> 
         
   <bean:define id="weddingListPage" name="WeddingForm" property="weddingListPage" type="com.esmart2u.oeight.member.web.struts.helper.Page"/> 
        <%--bean:define id="thisPageElements" name="pledgeListPage" property="thisPageElements" type="java.util.List"/--%> 
        <logic:present name="weddingListPage" property="thisPageElements">
        <logic:notEmpty name="weddingListPage" property="thisPageElements">
              
         <%--tr>
            <td colspan="3">&nbsp;
            </td>
        </tr--%> 
        
        <tr class=inputlabel>
             <%
                int currentPageNumber = weddingForm.getWeddingListPage().getPageNumber();
                int lastPageNumber = weddingForm.getWeddingListPage().getLastPageNumber(); 
                pagingOutput = new StringBuffer();
                if (currentPageNumber+1 > 2 ){
                    pagingOutput.append("<a href='javascript:browsePage(" + 0 + ")'> First </a>&nbsp;");
                }
                if (weddingForm.getWeddingListPage().hasPreviousPage())
                {
                    pagingOutput.append("<a href='javascript:browsePage(" + (currentPageNumber -1) + ")'>"+ (currentPageNumber) + "</a>&nbsp;");
                }
                pagingOutput.append((currentPageNumber +1) + "&nbsp;");
                if (weddingForm.getWeddingListPage().hasNextPage())
                {
                    pagingOutput.append("<a href='javascript:browsePage(" + (currentPageNumber +1) + ")'>"+ (currentPageNumber+2) + "</a>&nbsp;");
                }
                if (currentPageNumber+1 < lastPageNumber){
                    pagingOutput.append("<a href='javascript:browsePage(" + (lastPageNumber) + ")'> Last </a>&nbsp;");
                }
                pagingOutput.append("&nbsp; of " + (lastPageNumber + 1));
                %>
                <td class="formbuttonsCell" colSpan=3> <%=pagingOutput.toString()%><a name="pledgeTop" id="pledgeTop"></a></td>
        </tr>
        <tr>
            <td colSpan=3>
                <table cellpadding="3" cellspacing="0">
                    <tr class=inputlabel align="center">
                        <td width="30px" align="center">No</td>
                        <td width="300px" align="center"><center>Wedding Couple</center></td>
                        <td width="300px" align="center">Description</td> 
                    </tr>
                    <% int currentCount = ((currentPageNumber*8)+1);%>
                    <logic:iterate id="wedding" name="weddingListPage" property="thisPageElements"  type="com.esmart2u.oeight.data.OEightWedding">
                        
                        <bean:define id="coupleName" name="wedding" property="coupleName" type="java.lang.String"/> 
                        <bean:define id="weddingDescription" name="wedding" property="weddingDescription" type="java.lang.String"/>    
                        <bean:define id="photoLargePath" name="wedding" property="photoLargePath" type="java.lang.String"/>    
                        
                        <% 
                            if (currentCount % 2 == 0)
                            {
                            %>
                            <tr>
                            <%
                            }else
                            {
                            %>
                            <tr class="w_altbar">   
                            <%
                            }
                        %>                          
                            <td class="inputlabel" align="center"><%=currentCount++%></td>
                            <td class="lbl" align="center"><center><br>
                            <logic:notEmpty name="photoLargePath">   
                                <a href="/wedphotos/<bean:write name="photoLargePath"/>" mce_href="/wedphotos/<bean:write name="photoLargePath"/>" rel="lightbox"><img src="/wedphotos/<bean:write name="photoLargePath"/>" width="240px"></a> 
                            </logic:notEmpty>
                                <br><b><bean:write name="coupleName" filter="true"/></b>
                            </center>
                            </td> 
                            <td class="lbl" align="center"> <bean:write name="weddingDescription" filter="true"/></td> 
                           
                        </tr>
                        
                    </logic:iterate>
                </table>
        </td></tr>      
        <tr class=inputlabel> 
            <td class="formbuttonsCell" colSpan=3><nobr><%=pagingOutput.toString()%></nobr> 
            </td>
        </tr>      
        <%--tr class=inputlabel> 
            <td class="hdr_1" align="right"><a href="#pledgeTop"><nobr>Back to Top</nobr></a> 
            </td>
        </tr--%>  
      
        </logic:notEmpty>
        <logic:empty name="weddingListPage" property="thisPageElements">
        
        <tr>
            <td class="hdr_1" colspan="3" align="left">No Results
            </td>
        </tr>  
        </logic:empty>
        </logic:present>
         
        <logic:notPresent name="weddingListPage" property="thisPageElements"> 
        <tr>
            <td class="hdr_1" colspan="3" align="left">No Results
            </td>
        </tr>   
        </logic:notPresent>
        
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
             
     </table></div>
       
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table>
    </td>   
        <input type="hidden" name="currentPage" value="<%=weddingForm.getCurrentPage()%>">
        <input type="hidden" name="act" value="list">
        <%--input type="hidden" name="token" value="<%=request.getAttribute("token")%>"--%> 
       </div>  
    </html:form>        
    
    
 <% 
   
    if (weddingForm.getWeddingListPage() != null){
        Page weddingListpage = weddingForm.getWeddingListPage();
        weddingListpage = null;
        weddingForm.setWeddingListPage(null);
    } 
%>  