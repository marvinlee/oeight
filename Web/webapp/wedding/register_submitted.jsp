<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 
 
<tiles:insert page="/tiles/wedding_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Wedding - Registered!" />
   <tiles:put name="header" value="/tiles/wedding_top.jsp" />
   <tiles:put name="javascript" value="/wedding/register_js.jsp" />
   <tiles:put name="menu" value="/wedding/wedding_menu.jsp" />
   <tiles:put name="body" value="/wedding/register_submitted_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/wedding_bottom.jsp" /> 
</tiles:insert>