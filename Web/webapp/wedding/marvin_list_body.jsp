<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>  
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.WeddingForm"%>   
<%@ page import="com.esmart2u.oeight.member.web.struts.helper.Page"%>  

 <% 
    WeddingForm weddingForm = (WeddingForm)request.getAttribute("WeddingForm"); 
    
    List resultList = null;
    StringBuffer pagingOutput = null;
    if (weddingForm.getWeddingListPage() != null){
        resultList = weddingForm.getWeddingListPage().getThisPageElements();
    } 
%>  
<html:form name="WeddingForm" type="com.esmart2u.oeight.member.web.struts.controller.WeddingForm" method="post" scope="request"  action="/wedding.do" isRelative="true">
   
<%-- This page needs total 3 columns instead of 4--%> 
      <table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br>  
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  
                <h1 class="h1_hdr">080808 Weddings</h1> <br><br>

     <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
        </tr> 
         
        <logic:present name="WeddingForm" property="adminWeddingList">
        <logic:notEmpty name="WeddingForm" property="adminWeddingList">
            
           
         <%--tr>
            <td colspan="3">&nbsp;
            </td>
        </tr--%> 
      
        <tr>
            <td colSpan=3>
                <table cellpadding="3" cellspacing="0">
                    <tr class=inputlabel align="center">
                        <td width="30px" align="center">No</td>
                        <td width="300px" align="center"><center>Wedding Couple</center></td>
                        <td width="300px" align="center">Description</td> 
                        <td width="300px" align="center">Action</td> 
                    </tr> 
                    <% int currentCount = 0;%>
                   <logic:iterate id="eachWedding" name="WeddingForm" property="adminWeddingList" type="com.esmart2u.oeight.data.OEightWedding">
                            <bean:define id="weddingId" name="eachWedding" property="weddingId" type="java.lang.Long"/>
                            <bean:define id="coupleName" name="eachWedding" property="coupleName" type="java.lang.String"/>
                            <bean:define id="weddingDescription" name="eachWedding" property="weddingDescription" type="java.lang.String"/>    
                            <bean:define id="photoLargePath" name="eachWedding" property="photoLargePath" type="java.lang.String"/>     
                            <bean:define id="photoSmallPath" name="eachWedding" property="photoSmallPath" type="java.lang.String"/>    
                            <bean:define id="status" name="eachWedding" property="status" type="java.lang.Character"/>    
                            <bean:define id="dateUpdated" name="eachWedding" property="dateUpdated" type="java.util.Date"/>    
                 
                        <% 
                            if (currentCount % 2 == 0)
                            {
                            %>
                            <tr>
                            <%
                            }else
                            {
                            %>
                            <tr class="w_altbar">   
                            <%
                            }
                        %>                          
                            <td class="inputlabel" align="center"><%--=currentCount++--%><bean:write name="weddingId"/><input type="hidden" name="wedStrId" value="<bean:write name="weddingId"/>"></td>
                            <td class="lbl" align="center"><center><br>
                            <logic:notEmpty name="photoLargePath">   
                                <a href="/wedphotos/<bean:write name="photoLargePath"/>" mce_href="/wedphotos/<bean:write name="photoLargePath"/>" rel="lightbox"><img src="/wedphotos/<bean:write name="photoLargePath"/>" width="240px"></a> 
                            </logic:notEmpty>
                                <br><b><bean:write name="coupleName" filter="true"/></b>
                            </center>
                            </td> 
                            <td class="lbl" align="center"> <bean:write name="weddingDescription" filter="true"/><br><bean:write name="dateUpdated" /></td>
                            <td class="lbl" align="center"> <select name="wedStatus">
                                                                <option value="N" <%if (status == 'N') out.print("selected");%> >Normal</option>
                                                                <option value="C" <%if (status == 'C') out.print("selected");%> >Cancel</option>
                                                            </select>
                            </td> 
                           
                        </tr>
                        
                    </logic:iterate>
                </table>
        </td></tr>      
          <tr>  
                <td align="center" colspan="3"> 
                    <center><input type="button" name="update" class="formbuttons" value="Update" onclick="formSubmit();"></center>
                </td>
            </tr>
      
        </logic:notEmpty>
        <logic:empty name="WeddingForm" property="adminWeddingList">
        
        <tr>
            <td class="hdr_1" colspan="3" align="left">No Results
            </td>
        </tr>  
        </logic:empty>
        </logic:present>
         
        <logic:notPresent name="WeddingForm" property="adminWeddingList"> 
        <tr>
            <td class="hdr_1" colspan="3" align="left">No Results
            </td>
        </tr>   
        </logic:notPresent>
        
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
             
     </table></div>
       
                </td>
                <td width="15%">&nbsp;</td>
                </tr>  
            </table>
    </td>    
        <input type="hidden" name="act" value="marvinListUpdate">
        <%--input type="hidden" name="token" value="<%=request.getAttribute("token")%>"--%> 
       </div>  
    </html:form>        
    
  