<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%>
<%@ page import="com.esmart2u.solution.base.helper.ConfigurationHelper"%>
<table border="0" cellpadding="5" cellspacing="0" align="left" width="1000">
    <tr>
        <td class="w_menu">
            <a href="/wedding.do?act=mosaic">The Wedding</a>
        </td>
        <td class="w_menu">
            <a href="/wedding.do?act=about">About</a>
        </td>
        <%if (session!=null && StringUtils.hasValue((String)session.getAttribute("userId"))){%>
         <td class="w_menu">
            <a href="/wedding.do?act=page">My Wedding</a>
        </td> 
        <%}else
        {%> 
         <td class="w_menu">
            <a href="/wedding.do?act=register">Register</a>
        </td> 
        <%}%> 
        <td class="w_menu">
            <a href="/wedding.do?act=resources">Resources</a> 
        </td> 
        <td class="w_menu">
            <a href="http://<%=ConfigurationHelper.getDomainName()%>">080808</a> 
        </td>  
        <td class="w_menu">
            <a href="shop.do?act=lovers" target="_blank">Shop</a> 
        </td> 
        <%if (session!=null && StringUtils.hasValue((String)session.getAttribute("userId"))){%>
         <td class="w_menu">
            <a href="/secure/login.do?act=logout">Logout</a>
        </td> 
        <%}%>
    </tr>
</table>
<%--table border="0" cellpadding="5" cellspacing="0" width="200">
    <tr>
        <td class="e_menu">&nbsp;
        </td> 
    </tr>
</table--%>