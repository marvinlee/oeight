<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%> 
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.StringUtils"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.WeddingForm"%>   

 <% 
    WeddingForm weddingForm = (WeddingForm)request.getAttribute("WeddingForm"); 
  
%>
<%-- This page needs total 3 columns instead of 4--%> 
     <table width="100%">
<tr bgcolor="#FFFFFF" align="right">
<td colspan="2" class="formbuttonsCell" align="right"> <br><br> 
            <!-- AddThis Bookmark Button BEGIN -->
            <script type="text/javascript">
                          addthis_url    = 'http://wedding.080808.com.my?act=register';   
                          addthis_title  = document.title;  
                          addthis_pub    = 'marvinlee';     
            </script><script type="text/javascript" src="http://s7.addthis.com/js/addthis_widget.php?v=12" ></script>
            <!-- AddThis Bookmark Button END -->
    </td>
    <td width="15%">&nbsp;</td>
</tr>  
<tr>   
<td width="15%">&nbsp;</td> 
<td valign="top">  

                <h1 class="h1_hdr">My 080808 Wedding</h1> <br><br> 

    <html:form name="WeddingForm" type="com.esmart2u.oeight.member.web.struts.controller.WeddingForm" method="post" scope="request"  action="/wedding.do" isRelative="true"  enctype="multipart/form-data">
    <div id="earthlayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" width="10%">&nbsp;</td>
          <td align="center" width="20%">&nbsp;</td>
          <td align="center" width="70%">&nbsp;</td>
        </tr>
      
        <tr>
          <td align="center" colspan="3" >
                    <html:errors property="weddingError"/>&nbsp;</td>
        </tr>
        <% if (weddingForm.getPageSavedSuccess()){%>
        <tr>
          <td align="center" colspan="3" >
            <center><h1 style="color: blue">Information updated successfully.<br>Photo will be moderated.</h1></center>
            </td>
        </tr>    
        <%}%> 
        <tr>
          <td align="center" colspan="3" class="content_txt" >My Wedding Information :<br></td>
        </tr>   
             <tr>
             <logic:notEmpty name="WeddingForm" property="photoLargePath">  
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Photo :&nbsp;
                </td>
                <td align="left"><br>
                            <a href="/wedphotos/<bean:write name="WeddingForm" property="photoLargePath"/>" mce_href="/wedphotos/<bean:write name="WeddingForm" property="photoLargePath"/>" rel="lightbox"><img src="/wedphotos/<bean:write name="WeddingForm" property="photoLargePath"/>" width="240px"></a>
                            <br>  
                    </td>
            </logic:notEmpty>
            <logic:empty name="WeddingForm" property="photoLargePath">  
                    <td  colspan="3" >&nbsp;</td> 
            </logic:empty>
            </tr>   

             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Couple Name :&nbsp;
                </td>
                <td align="left">
                    <html:text name="WeddingForm"  styleClass="inputvalue" property="coupleName" maxlength="50" size="50" />   
                    <html:errors property="coupleName"/> 
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Description :&nbsp;
                </td>
                <td align="left">
                    <html:textarea styleClass="inputvalue" name="WeddingForm" property="weddingDescription" cols="50" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                    <html:errors property="weddingDescription"/> 
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Photo Upload :&nbsp;
                </td>
                <td align="left"> 
                    <html:file styleClass="inputvalue" name="WeddingForm" property="photoFile"  onchange="preview(this)"/>   
                    <html:errors property="photoPath"/>
                </td>
            </tr>   
            <tr>
                <td>&nbsp;</td>
                <td class="inputlabel" align="right">Photo Preview :&nbsp;
                </td>
                <td align="left"> 
                    <img alt="Photo preview" id="previewField" src="/images/spacer.gif"> 
                    <br>
                </td>
            </tr>
           
            
            
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr>  
                <td align="center" colspan="3"> 
                    <center><input type="button" name="update" class="formbuttons" value="Update" onclick="formSubmit();"></center>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="3" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div>
        <input type="hidden" name="act" value="pageSave">  
    </html:form> 
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table>   