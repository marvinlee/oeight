 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserWishers,
                 com.esmart2u.oeight.member.vo.WishVO,
                 com.esmart2u.oeight.member.web.struts.controller.WishForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%>   

<% 
    WishForm wishForm = (WishForm)request.getAttribute("WishForm"); 
      
    List wishersList = (List)wishForm.getWishersList(); 

   
%>
 
    <html:form name="WishForm" type="com.esmart2u.oeight.member.web.struts.controller.WishForm" method="post" action="/wish.do" isRelative="true">
    <div id="wishLayer" class="divBox">   
        <table width="400">
        <COL width="20%"> 
        <COL width="40%">
        <COL width="40%">   
        <tr>
            <td colspan="3">&nbsp;<a name="wishers"></a>
            </td>
        </tr>  
        <tr>
            <td colspan="3"><h1>Wishers</h1></td>
        </tr>   
        
        <tr>
        <td> 
        </td> 
        <td colspan="2">
            <DIV id=container>
                <%
                if (wishersList != null && !wishersList.isEmpty())
                { 
                
                %>  
                <logic:iterate id="wish" name="WishForm" property="wishersList" type="com.esmart2u.oeight.member.vo.WishVO">
                <bean:define id="wishIdString" name="wish" property="wishIdString" type="java.lang.String"/>
                <bean:define id="wishContent" name="wish" property="wishContent" type="java.lang.String"/>
                <bean:define id="dateSent" name="wish" property="dateSent" type="java.util.Date"/>    
                <bean:define id="genderString" name="wish" property="genderString" type="java.lang.String"/>    
                <bean:define id="userName" name="wish" property="userName" type="java.lang.String"/>    
                <bean:define id="photoLargePath" name="wish" property="photoLargePath" type="java.lang.String"/>    
                <div style="border: black 4px;">
                    <logic:equal name="genderString" value="F"> 
                        <DIV class=one>
                    </logic:equal>
                    <logic:notEqual name="genderString" value="F"> 
                        <DIV class=two>
                    </logic:notEqual > 
                    <B class=tl><B class=tr></B></B> 
                    
                <P><bean:write name="wishContent" filter="true"/><%--=userWishers.getWishContent()--%></P><B class=bl></B><B class=br><B class=point></B></B></DIV><br><div style="vertical-align:top;"><a href='http://profile.080808.com.my/<bean:write name="userName"/>'><img src="/vphotos/<bean:write name="photoLargePath" />"  width="120px">&nbsp;&nbsp;<b><bean:write name="userName" filter="true"/></b></a>
                on  <%=DateObjectHelper.getPrintedTimestamp(dateSent)%></div><br>
                <input class="formbuttons" type="button" value=Remove onclick="removeWish(<bean:write name="wishIdString"/>)">
            </div><br><br><br>
            
            </logic:iterate>
        </DIV>
    </td>
    </tr>     
    
    <%
                }
    else
    {
    %>   
    
    <tr>
        <td class="hdr_1" colspan="3" align="left">No Wishers Yet
        </td>
    </tr>  
    <%
    } 
    %>       
    <tr>
        <td colspan="3">&nbsp;&nbsp;&nbsp;<a href="#wishers">Back to Top</a>&nbsp;
        </td>
    </tr>       
    <tr>
        <td colspan="3">&nbsp;
        </td>
    </tr>  
    </table></div>
    <html:hidden name="WishForm" property="wishId" />
    <input type="hidden" name="act" value="mainSubmitted">
    <input type="hidden" name="token" value="<%=request.getAttribute("token")%>">
    </html:form>  