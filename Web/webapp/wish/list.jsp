<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<tiles:insert page="/tiles/mosaic_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Profile - Wishes" />
   <tiles:put name="header" value="/tiles/top.jsp" />
   <tiles:put name="javascript" value="/wish/list_js.jsp" />  
   <tiles:put name="body" value="/wish/list_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>