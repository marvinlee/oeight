 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.ConfigurationHelper,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserWishers,
                 com.esmart2u.oeight.member.vo.WishVO,
                 com.esmart2u.oeight.member.web.struts.controller.WishForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%>   

<% 
    WishForm wishForm = (WishForm)request.getAttribute("WishForm");   
%>
<%-- This page needs total 3 columns instead of 4--%>
    <td width="15%">&nbsp;</td>
    <td>
    <table width="100%">
    <tr>  
    
    <td width="30%">&nbsp;</td>
    
    <td width="60%" valign="top" align="center">
   <html:form name="WishForm" type="com.esmart2u.oeight.member.web.struts.controller.WishForm" method="post" action="/wish.do" isRelative="true">
  
   <div id="friendsLayer" class="divBox">   
            <table width="400">
                <COL width="5%"> 
                <COL width="55%">
                <COL width="40%">    
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;</a>
                </td>
            </tr>    
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;<a href="http://profile.<%=ConfigurationHelper.getDomainName() + "/" + wishForm.getUserName()%>">Back to <%=wishForm.getUserName()%>'s Profile</a>  

                </td>
            </tr>  
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>     
            <tr>
                <td>&nbsp;</td>
                <td colspan="2"><h1>Request Add <%=wishForm.getUserName()%></h1></td>
            </tr>   
            <tr align="center"> 
                <td colspan="3">&nbsp;<center>
                <a href='http://profile.<%=ConfigurationHelper.getDomainName()%>/<bean:write name="WishForm" property="userName"/>'><img src="/vthumb/<bean:write name="WishForm" property="photoSmallPath" />"  width="38px" height="38px" alt="<bean:write name="WishForm" property="userName" filter="true"/>"><br><b><bean:write name="WishForm" property="userName" filter="true"/></b></a><br>
                <% if (wishForm.isViewingOwnProfile()){%>
                    <br><font color="red">You cannot add yourself</font><br> 
                  <%}
                else{
                
                    if (wishForm.isHasError())
                    {
                %> 
                        <html:errors property="requestFren"/>  
                    <%}
                    else
                    {
                %> 
                    <br><font color="blue">Add buddy requested</font>
                <%
                    }
                }
                %>
                <br><br><input class="formbuttons" type="button" name="back" value="Back" onclick="formBack('<%=wishForm.getUserName()%>');">
                </center>             
                </td>
            </tr>     
          
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>     
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>  
            </table></div>
            <html:hidden name="WishForm" property="userName" />
    </html:form>    
                <td width="10%">&nbsp;
                </td>
        </tr>
    </table>
    </td>
    <td width="10%">&nbsp;</td>