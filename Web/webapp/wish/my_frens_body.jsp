 <%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>  
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>   
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>   
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>   
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.solution.base.helper.PropertyManager,
                 com.esmart2u.solution.base.helper.StringUtils, 
                 com.esmart2u.solution.base.helper.PropertyConstants,
                 com.esmart2u.solution.base.helper.ConfigurationHelper,
                 com.esmart2u.oeight.member.helper.OEightConstants,
                 com.esmart2u.oeight.data.UserWishers,
                 com.esmart2u.oeight.member.vo.WishVO,
                 com.esmart2u.oeight.member.web.struts.controller.WishForm,
                 com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper"%>   

<% 
    WishForm wishForm = (WishForm)request.getAttribute("WishForm"); 
      
    List friendsList = (List)wishForm.getFriendsList(); 

   
%>
<%-- This page needs total 3 columns instead of 4--%>  
    <table width="100%">
    <tr>  
    
    <td width="30%">&nbsp;</td>
    
    <td width="60%" valign="top" align="center">
   <html:form name="WishForm" type="com.esmart2u.oeight.member.web.struts.controller.WishForm" method="post" action="/wish.do" isRelative="true">
  
   <div id="friendsLayer" class="divBox">   
            <table width="400">
                <COL width="5%"> 
                <COL width="55%">
                <COL width="40%">   
            <tr>
                <td>&nbsp;</td>
                <td colspan="2">&nbsp;<a name="wishers"></a>
                </td>
            </tr> 
             
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>     
            <tr>
                <td>&nbsp;</td>
                <td colspan="2"><h1>My Buddies</h1></td>
            </tr>   
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>      
            <tr>
                <td colspan="3">[C] : Confirm Buddy<br>[X] : Delete Buddy
                </td>
            </tr>  
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>     
               <%
            if (friendsList != null && !friendsList.isEmpty())
            { 
                int count = 0;
                int maxPerRow = friendsList.size() > 30? 8:3;

            %>  
            <tr align="left"> 
                <td colspan="3">  
                    <table align="left"> 
                    <logic:iterate id="friend" name="WishForm" property="friendsList" type="com.esmart2u.oeight.member.vo.WishVO">
                        <%
                            // Every first cell prints open tr
                            if (count%maxPerRow ==0)
                            {
                                out.print("<tr>");
                            }
                            count++;
                        %> 
                        <bean:define id="genderString" name="friend" property="genderString" type="java.lang.String"/>    
                        <bean:define id="userName" name="friend" property="userName" type="java.lang.String"/>    
                        <bean:define id="photoSmallPath" name="friend" property="photoSmallPath" type="java.lang.String"/>    
                       
                        <td>
                        <a href='http://profile.<%=ConfigurationHelper.getDomainName()%>/<bean:write name="userName"/>'><img src="/vthumb/<bean:write name="photoSmallPath" />"  width="38px" height="38px" alt="<bean:write name="userName" filter="true"/>"><br><b><bean:write name="userName" filter="true"/></b></a><br>
                        <%if (OEightConstants.BUDDY_NEW_REQUEST == friend.getStatus()){%><a href="/wish.do?act=confirmFren&userName=<bean:write name="userName"/>" alt="Confirm Add">[C]</a><%}%> <a href="/wish.do?act=deleteFren&userName=<bean:write name="userName"/>" alt="Delete">[X]</a>
                        <br><%if (friend.getDateSent() != null){%>Latest wish: <%=DateObjectHelper.getPrintedTimestamp(friend.getDateSent())%><%}%></div><br><br><br>
                        </td>
                        <%
                            // Every third row prints close tr
                            if (count%maxPerRow ==0)
                            { 
                                out.print("</tr>");
                                count = 0;
                            }
                        %>
                    </logic:iterate> 
                             
                    <%
                        // Now we check if need to do how many colspan for last cell
                        int colLeft = maxPerRow - count;
                        if (colLeft < maxPerRow)
                        {
                           out.print("<td colspan="+colLeft+">&nbsp;</td></tr>"); 
                        }
                    %>
                    </table>
                </td>
            </tr>    
            <%
                        }
            else
            {
            %>   
            
            <tr>
                <td class="hdr_1" colspan="3" align="left">No Buddies Yet
                </td>
            </tr>  
            <%
            } 
            %>       
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>   
            <%--tr>
                <td colspan="3">&nbsp;&nbsp;&nbsp;<a href="#wishers">Back to Top</a>&nbsp;
                </td>
            </tr--%>       
            <tr>
                <td colspan="3">&nbsp;
                </td>
            </tr>  
            </table></div>
        <html:hidden name="WishForm" property="userName" /> 
    </html:form>    
                <td width="10%">&nbsp;
                </td>
        </tr>
    </table> 