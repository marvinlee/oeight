<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<tiles:insert page="/tiles/mosaic_template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 Delete Buddy" />
   <tiles:put name="header" value="/tiles/top.jsp" />
   <tiles:put name="javascript" value="/wish/delete_fren_js.jsp" />  
   <tiles:put name="body" value="/wish/delete_request_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>