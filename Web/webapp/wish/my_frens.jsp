<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<tiles:insert page="/tiles/template.jsp" flush="true">
   <tiles:put name="title" type="string" value="080808 My Buddies" />
   <tiles:put name="header" value="/tiles/top.jsp" />
   <tiles:put name="javascript" value="/wish/main_js.jsp" />
   <tiles:put name="menu" value="/profile/menu.jsp" />
   <tiles:put name="body" value="/wish/my_frens_body.jsp" />
   <tiles:put name="notes" value="/tiles/empty.jsp" />
   <tiles:put name="bottom" value="/tiles/bottom.jsp" /> 
</tiles:insert>