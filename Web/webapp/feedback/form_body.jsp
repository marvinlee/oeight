<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%> 
<%@taglib uri="/WEB-INF/c.tld" prefix="c"%> 
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>  
<%@ page import="java.util.*"%> 
<%@ page import="com.esmart2u.oeight.member.web.struts.controller.FeedbackForm,
                 com.esmart2u.oeight.member.web.struts.helper.FeedbackHelper"%>   

 <% 
    FeedbackForm feedbackForm = (FeedbackForm)request.getAttribute("FeedbackForm"); 
    
    Vector feedbackList = FeedbackHelper.getSubjectList(); 
%>
<%-- This page needs total 3 columns instead of 4--%>
    <td colspan="3">
        <table width="100%">
            <tr>  
    
                <td width="15%">&nbsp;</td>

                <td valign="top">

                <h1>Feedback</h1> 

    <html:form name="FeedbackForm" type="com.esmart2u.oeight.member.web.struts.controller.FeedbackForm" method="post" scope="request"  action="/feedback.do" isRelative="true">
    <div id="feedbacklayer" class="divBox">  
     <table border="0"  bordercolor="#999999" cellpadding="5" cellspacing="0"> 
         
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="inputvalue" >If you have any enquiry or require further information from us, please fill in the form below and we will get back to you as soon as possible.</td>
        </tr>
        <tr>
          <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
        </tr>
          <tr>
                <td class="inputlabel" align="right">Name :&nbsp;
                </td>
                <td align="left">
                    <html:text name="FeedbackForm"  styleClass="inputvalue" property="name" size="30" maxlength="100"/>   
                    <html:errors property="name"/>
                </td>
            </tr> 
            <tr>
                <td class="inputlabel" align="right">Company Name :&nbsp;
                </td>
                <td align="left">
                    <html:text name="FeedbackForm"  styleClass="inputvalue" property="companyName" size="30" maxlength="100"/>   
                    <html:errors property="companyName"/>
                </td>
            </tr>
             <tr>
                <td class="inputlabel" align="right">Contact :&nbsp;
                </td>
                <td align="left">
                    <html:text name="FeedbackForm"  styleClass="inputvalue" property="contactNumber" size="30" maxlength="100"/>   
                    <html:errors property="contactNumber"/>
                </td>
            </tr>
             <tr>
                <td class="inputlabel" align="right">Email :&nbsp;
                </td>
                <td align="left">
                    <html:text name="FeedbackForm"  styleClass="inputvalue" property="email" size="30" maxlength="20"/>   
                    <html:errors property="email"/>
                </td>
            </tr>
             <tr>
                <td class="inputlabel" align="right">Subject :&nbsp;
                </td>
                <td align="left">
                     <html:select name="FeedbackForm"  styleClass="inputvalue" property="subject"> 
                        <%
                        for(int i=0;i<feedbackList.size();i++)
                        {  
                            String[] subject = (String[])feedbackList.get(i);
                            out.print("<option value='" + subject[0] +"'");  
                            out.print(">");
                            out.println(subject[1] + "</option>");
                        }
                        %>
                    </html:select>  
                    <html:errors property="subject"/>
                </td>
            </tr>
             <tr>
                <td class="inputlabel" align="right">Message :&nbsp;
                </td>
                <td align="left">
                    <html:textarea name="FeedbackForm"  styleClass="inputvalue" property="message" cols="30" rows="5" onkeydown="textareaCheck(this, 500)"/>   
                    <html:errors property="message"/>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
            <tr> 
                <td class="inputlabel" align="right">&nbsp;
                </td>
                <td align="left" class="formbuttonsCell" > 
                    <input type="button" name="search" class="formbuttons" value="Submit" onclick="formSubmit();">
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2" class="formbuttonsCell" >&nbsp;</td>
            </tr>
     </table></div>
        <input type="hidden" name="act" value="submitted">
        <%--html:hidden name="FeedbackForm" property="tokenValue"/>
        <input type="hidden" name="token" value="<%=request.getAttribute("token")%>"--%> 
    </html:form> 
                </td>
                <td width="15%">&nbsp;</td>
                </tr>
            </table>
    </td>                 