/*
 * Copyright (c) 2007 TripAdvisor (http://www.tripadvisor.com)
 * @author fitzgerald@tripadvisor.com
 */
package com.tripadvisor.friendster;

public class FriendsterException extends Exception
{
    private int errorCode = -1;
    
    public FriendsterException(String message)
    {
        super(message);
    }
    
    public FriendsterException(int errorCode, String message)
    {
        super(message + " (" + errorCode + ")");
        this.errorCode = errorCode;
    }
    
    public int getErrorCode()
    {
        return errorCode;
    }
}
