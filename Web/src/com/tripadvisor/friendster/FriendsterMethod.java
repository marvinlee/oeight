/*
 * Copyright (c) 2007 TripAdvisor (http://www.tripadvisor.com)
 * @author fitzgerald@tripadvisor.com
 */
package com.tripadvisor.friendster;

public enum FriendsterMethod
{
    USER, FRIENDS, WIDGET, DEPTH, PHOTOS, PHOTO, PRIMARYPHOTO, TOKEN, SESSION, SHOUTOUT, FANS
}
