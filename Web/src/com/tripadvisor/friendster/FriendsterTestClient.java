/*
 * Copyright (c) 2007 TripAdvisor (http://www.tripadvisor.com)
 * @author fitzgerald@tripadvisor.com
 */
package com.tripadvisor.friendster;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FriendsterTestClient
{
    private static final String API_KEY = "d8ff5e3a05f704008f06f7526e8a0887";
    private static final String SECRET_KEY = "9a24b3843e9b2ca47e13a045f59522a8 ";
    private static final Logger log = Logger.getLogger(FriendsterTestClient.class);
    
    public static void main(String argsp[])
    {
        System.out.println("Running FriendsterTestClient");
        BasicConfigurator.configure();
        XPath xpath = XPathFactory.newInstance().newXPath();
        
        String sessionKey = "";
        
        FriendsterClient fc = new FriendsterClient(sessionKey, API_KEY, SECRET_KEY);
        int uid = 9120940;
        int friendUid = 55570807;
        try
        {   /*
                /token       POST    Create a new token that can be exchanged for a session after the user logs in.
                /session     POST    Create a new API Session.
            */
            String sToken = fc.getToken();           
            System.out.println("stoken=" + sToken);
            //String sToken = "047d9ec722ad178.27216786";
            //String sSession = fc.getSession(sToken);
            //System.out.println("session id=" + sSession);
            
            /*
                /user         GET     Get User Information for the logged in user.
                /user/:uids   GET     Get User Information for users in :uids list.
                /friends/:uid GET     Get user's friend list.
            */
            Document doc = fc.getUser(uid);
            log.info("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
            log.info("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
            
            List<Integer> friends = fc.getFriends(uid);
            
            log.info("friends: " + friends); 
            
            doc = fc.getUsers(friends);
            
            /*
                /depth/:uid1,:uid2  GET     Get relation depth (distance) between two users.
            */
            int depth = fc.getDepth(friends.get(0), friends.get(1));
            log.info("depth: " + depth);
            
            /*
                /photos/:uid        GET     Get user's photos.
                /photos/            GET     Get photos for the current logged in user.
                X /photos/:uid        POST    Uploads a photo for the specified user.
                X /photos/            POST    Uploads a photo for the current logged in user.
                /photo/:uid/:pid    GET     Get a user's photo.
                /photo/:pid         GET     Get a photo for the current logged in user.
                X /photo/:pid         PUT     Modify photo attributes.
                /primaryphoto/:uid  GET     Get a user's primary photo.
                /primaryphoto/      GET     Get the primary photo for the current logged in user.
                X /photo/:pid         DELETE  Deletes a user's photos.
            */
            doc = fc.getPhotos();
            doc = fc.getPhotos(uid);
            
            NodeList nl = (NodeList) xpath.evaluate("/photos_response/photo", doc, XPathConstants.NODESET);
            for (int i = 0;i < nl.getLength();i++)
            {
                log.info("url[" + i + "]: " + xpath.evaluate("src", nl.item(i)));
            }
            
            long pid = Long.parseLong(xpath.evaluate("pid", nl.item(0)));
            
            doc = fc.getPhoto(pid);
            log.info("getPhoto(pid): " + xpath.evaluate("/photo_response/photo/src", doc));
            
            doc = fc.getPhoto(uid, pid);
            log.info("getPhoto(uid, pid): " + xpath.evaluate("/photo_response/photo/src", doc));
            
            doc = fc.getPrimaryPhoto();
            log.info("primary photo caption: " + xpath.evaluate("/primaryphoto_response/photo/caption", doc));
            
            doc = fc.getPrimaryPhoto(uid);
            log.info("primary photo caption: " + xpath.evaluate("/primaryphoto_response/photo/caption", doc));
            
            /*
            /shoutout/:uid  GET     Get a user's shoutout
            /shoutout/  GET     Get the shoutout for the current user.
            /shoutout/  POST    Update a user's shoutout.
            */
            
            doc = fc.getShoutout(uid);
            
            doc = fc.getShoutout();
            
            doc = fc.getShoutout(friends);
            
            //fc.setShoutout("Hello World!");
            
            //fc.getShoutout();
            
            /* 
            /widget     POST    Update widget's profile content for the logged in user.
            */
            
            fc.updateProfile("test profile", 1);
        }
        catch (Exception e)
        {
            log.error(e,e);
        }
    }
}
