/*
 * Test.java
 *
 * Created on September 27, 2007, 12:18 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.test;

import com.esmart2u.oeight.admin.helper.mosaic.HibernateUtil;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.SeasonGreetingBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.helper.smtp.EmailAuthentication;
import com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.oeight.member.web.struts.helper.WidgetHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import net.sf.cglib.core.EmitUtils;
import org.apache.tools.ant.taskdefs.email.EmailAddress;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
 

/**
 *
 * @author meauchyuan.lee
 */
public class Test {
    
    public static String DISPLAY_TIMESTAMP_FORMAT = "dd/MMM/yyyy hh:mm a";
    public static SimpleDateFormat timestampFormatter = new SimpleDateFormat(DISPLAY_TIMESTAMP_FORMAT);
    
    private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
    
    public static String getPrintedTimestamp(Date date) {
        return timestampFormatter.format(date);
    }
    
    /** Creates a new instance of Test */
    public Test() { 
    }
    
    private static char nullchar;
    
    public static void main(String[] args) throws Exception{
        Timestamp test = new Timestamp(System.currentTimeMillis());
        System.out.println("Time1:" + test);
        test = null;
        System.out.println("Time2:" + test);
        testSendFromGmail();
        //System.out.println("Time1:" + new Date().getTime());
        //System.out.println("Time2:" + System.currentTimeMillis()/1000);
        /*if (nullchar != PropertyConstants.BOOLEAN_NO)
        {
            nullchar = PropertyConstants.BOOLEAN_YES;
        }
        System.out.println("nullchar=" + nullchar);*/
        //System.out.println("Max " + Long.MAX_VALUE);
        //System.out.println("time:" + getPrintedTimestamp(new Date())); 
        //test16(); //Tricky one, get the first send to next Output : zemails_processed.txt
        //test15(); //Only my friends (yahoo) - filter those already sent in test14() Output : zemails_yahoo_processed.txt
        //test14();//Only my friends - gen insert scripts Output : zemails_frens_processed.txt
        //test13(); // Get all email addresses
        //testMailLaunch();
        //printInsert();
        //getMailingListUsers(); 
        //getFacebookWidgetString();
        //parseMSNContacts(); 
        //System.out.println("Long="+ Long.MAX_VALUE);
        //String icOld = "1223234.0";
        //if (icOld.endsWith(".0")) icOld = icOld.substring(0,icOld.length()-2);
        //System.out.println("icOld:" +icOld);
         
    }
     
     private static void testSendFromGmail( ) throws Exception {
     
        System.out.println( "Getting authentication presetting");
        String userId = "oeight.mailer@gmail.com";
        String password = "8s83mPqr";
        EmailAuthentication auth = new EmailAuthentication(userId,password);
        System.out.println( "Getting mail server config");
        Properties mailServerConfig = new Properties(); 
        String host = "smtp.gmail.com";
        String fromAddress = "oeight.mailer@gmail.com";
        String port = "465";
        mailServerConfig = new Properties();       
        mailServerConfig.put("mail.smtp.auth", "true"); 
        mailServerConfig.put("mail.smtp.port", port); 
        mailServerConfig.put("mail.transport.protocol", "smtp"); 
        mailServerConfig.put("mail.host", host);
        mailServerConfig.put("mail.from", "080808 PostMaster"); 
        mailServerConfig.put("mail.smtp.socketFactory.class", SSL_FACTORY);
        mailServerConfig.put("mail.smtp.socketFactory.fallback", "false");        
        mailServerConfig.setProperty("mail.user", userId);
        mailServerConfig.setProperty("mail.password", password); 
        javax.mail.Session session = Session.getDefaultInstance( mailServerConfig, auth);       
        session.setDebug(true); 
        System.out.println( "Creating Message");
        MimeMessage message = new MimeMessage( session );
     
        try {
            //the "from" address may be set in code, or set in the
            //config file under "mail.from" ; here, the latter style is used
            System.out.println( "Preparing email");
            message.setFrom( new InternetAddress(fromAddress, "080808 PostMaster") );
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("marvinmclee@gmail.com"));
            message.setSubject("Testing from OEight Mailer");
            message.setContent("This is a testing mail body content <b> hahahaha </b>. If you can see this, it's great!", "text/html");
            System.out.println( "Sending email");
            Transport.send( message );
            System.out.println( "Email sent!");
        } catch(AddressException ex) {
            System.err.println("Cannot send email. " + ex);
            System.out.println("Address Exception Cannot send email"+ex);
            throw new MessagingException("Address Exception: "+ex);
        } catch(javax.mail.MessagingException ex) {
            System.err.println("Cannot send email. " + ex);
            System.out.println("Messaging Exception Cannot send email"+ex);
            throw new MessagingException("Messaging Exception: "+ex);
        }
     
    } 
    
    private static void parseMSNContacts()
    {
        try{
        FileReader file_reader = new FileReader("C:/msn2.txt");
        BufferedReader in=new BufferedReader(file_reader);
        String line;
        int i=0;
        while ((line = in.readLine()) != null) {
            //System.out.println("line=" + line);
            if(i>1) {
                String[] values=line.split(",");
                if(values.length<47) continue;
                String email=parseValue(values[46]);
                if(email.length()==0) continue;
                
                String name=parseValue(values[1]);
                if(values[2].length()>0)
                    name+=" "+parseValue(values[2]);
                if(values[3].length()>0)
                    name+=" "+parseValue(values[3]);
                if(name.length()==2) name=email.substring(0, email.indexOf("@"));
                
                email=email.toLowerCase();
                
                System.out.println("Name : " + name + " Email:"+email);
            }
            i++;
        }
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    
    }
    
    private static String parseValue(String value) {
        // chop off quotes
        if(value.length()>0 && value.charAt(0)=='"') {
            value=value.substring(1, value.length()-1);
        }
        return value;
    }
   /* 
    private static void getFacebookWidgetString()
    {
        User user = new UserBO().getUserByUserName("test19");
        String html = WidgetHelper.getBlogWidgetForFacebook(user);
        System.out.println(html); 
        
    }*/
    /*
    private static void getMailingListUsers()
    { 

        org.hibernate.Session session = HibernateUtil.getSessionFactory().openSession(); 

        List mailingList = (List) session.createSQLQuery("SELECT * FROM USER_TEMP where email_address NOT LIKE '%test.com'").addEntity(User.class).list();
        System.out.println("MailingList size:" + mailingList.size());
        
        if (mailingList != null)
        {
            for(int i=0;i<mailingList.size();i++)
            {
                User user = (User)mailingList.get(i);
                String email = user.getEmailAddress();
                //System.out.println("Email is:"+ user.getEmailAddress());
                
                System.out.println("INSERT INTO MAIL1(EMAIL, STATUS) VALUES('"+email+"','N');");
            }
        }
        
        
        session.close();
    } */
    
    private static void printInsert()
    {
        /*long timeTaken = 172800000;
        double timeTakenConverted = (timeTaken*1000)/1000;
        DecimalFormat strFormat = new DecimalFormat("######0.0000"); 
        String timeTakenSeconds = strFormat.format(timeTakenConverted/1000);
        //System.out.println("Time taken =" + timeTakenFloat);
        System.out.println("Time taken =" + timeTakenSeconds);*/
        
        /*for(int i=1;i<101;i++)
        {
            System.out.println("http://080808.com.my/register.do?act=invited&invitedBy=oeight&inviteCode=d"+i);
            //System.out.println("INSERT INTO USER_INVITES(USER_ID, EMAIL, INVITE_CODE, STATUS) VALUES(15, 'd"+i+"@test.com','d"+i+"','S');");
        }*/
    }
    
    
    private static void testMailLaunch()
    {
     StringBuffer old_sb = new StringBuffer();
        StringBuffer sb = new StringBuffer();
        
        try {
            // Create a FileReader and then wrap it with BufferedReader.
            FileReader file_reader = new FileReader("C:\\temp\\mails_launch1.txt");
            BufferedReader buf_reader = new BufferedReader(file_reader);
            
            // Read each line of the file and look for the string of interest. 
            do {
                String line = buf_reader.readLine();
                if (line == null) break;
                sb.append(line);
                sb.append(" ");
                //sb.append("\r\n");
            }while (true);
            buf_reader.close();
            
            // Old File
            HashSet oldEmailSet = new HashSet();
            String oldEmailAddresses = old_sb.toString();
            oldEmailAddresses = oldEmailAddresses.replaceAll(","," ");
            oldEmailAddresses = oldEmailAddresses.replaceAll(";"," ");
            //emailAddresses = emailAddresses.replaceAll("("," ");
            //emailAddresses = emailAddresses.replaceAll(")"," ");
            oldEmailAddresses = oldEmailAddresses.replaceAll("<"," ");
            oldEmailAddresses = oldEmailAddresses.replaceAll(">"," ");
            oldEmailAddresses = oldEmailAddresses.replaceAll("\""," ");
            StringTokenizer oldTokenizer = new StringTokenizer(oldEmailAddresses," ");
            while (oldTokenizer.hasMoreTokens()) {
                String email = oldTokenizer.nextToken().trim();
                if (Validator.validateEmail(email,true)) {
                    if (email.startsWith("("))
                    {
                        email = email.substring(1,email.length());
                    }
                    if (email.endsWith(")"))
                    { 
                        email = email.substring(0,email.length()-1);
                    }
                    oldEmailSet.add(email); //Set ensures no repeat
                }
            }
            
            System.out.println("Size = " + oldEmailSet.size());
            System.out.println("Print to file");
            
            
            // New file
            HashSet emailSet = new HashSet();
            String emailAddresses = sb.toString();
            emailAddresses = emailAddresses.replaceAll("'"," ");
            emailAddresses = emailAddresses.replaceAll(","," ");
            emailAddresses = emailAddresses.replaceAll(";"," ");
            //emailAddresses = emailAddresses.replaceAll("("," ");
            //emailAddresses = emailAddresses.replaceAll(")"," ");
            emailAddresses = emailAddresses.replaceAll("<"," ");
            emailAddresses = emailAddresses.replaceAll(">"," ");
            emailAddresses = emailAddresses.replaceAll("\""," ");
            StringTokenizer tokenizer = new StringTokenizer(emailAddresses," ");
            while (tokenizer.hasMoreTokens()) {
                String email = tokenizer.nextToken().trim();
                if (Validator.validateEmail(email,true)) {
                    if (email.startsWith("("))
                    {
                        email = email.substring(1,email.length());
                    }
                    if (email.endsWith(")"))
                    { 
                        email = email.substring(0,email.length()-1);
                    }
                    if (oldEmailSet.contains(email))
                    {
                        System.out.println("Filter out:" + email);
                    }
                    else{
                        emailSet.add(email); //Set ensures no repeat
                    }
                }
            }
            
            System.out.println("Size = " + emailSet.size());
            System.out.println("Print to file");
            
            //writes to new file
            PrintWriter out = new PrintWriter(new FileWriter("C:\\temp\\mails_launch1_processed.txt"), true);
            
            String str = null;
            Iterator iterator = emailSet.iterator();
            while (iterator.hasNext()) {
                String email = (String)iterator.next();
                String printout = "INSERT INTO MAIL1(EMAIL, STATUS)" +
                 " VALUES('"+email+"','N');";
                out.println(printout);
            }
            
            out.close(); 
        } catch (IOException e) {
            System.out.println("IO exception =" + e );
            //logger.error("IO exception=" + e );
        }
    
    
    }
       
    private static void test16() {
        StringBuffer sb = new StringBuffer();
        
        try {
            // Create a FileReader and then wrap it with BufferedReader.
            //FileReader file_reader = new FileReader("C:\\temp\\zemails.txt");
            //FileReader file_reader = new FileReader("C:\\temp\\zemails_derk.txt");
            FileReader file_reader = new FileReader("C:\\temp\\csFw1.htm");
            BufferedReader buf_reader = new BufferedReader(file_reader);
            
            // Read each line of the file and look for the string of interest.
            do {
                String line = buf_reader.readLine();
                if (line == null) break;
                sb.append(line);
                sb.append(" ");
                //sb.append("\r\n");
            }while (true);
            buf_reader.close();
            
            
            HashSet emailSet = new HashSet();
            HashMap emailMap = new HashMap(); // We need a table to refrain same sending twice
            String emailAddresses = sb.toString();
            emailAddresses = emailAddresses.replaceAll(":"," ");
            emailAddresses = emailAddresses.replaceAll(","," ");
            emailAddresses = emailAddresses.replaceAll(";"," "); 
            emailAddresses = emailAddresses.replaceAll("<"," ");
            emailAddresses = emailAddresses.replaceAll(">"," ");
            emailAddresses = emailAddresses.replaceAll("\""," ");
            StringTokenizer tokenizer = new StringTokenizer(emailAddresses," ");
            String previousEmail = "mclee81@hotmail.com";
            while (tokenizer.hasMoreTokens()) {
                String email = tokenizer.nextToken().trim();
                if (Validator.validateEmail(email,true)) {
                    if (email.startsWith("("))
                    {
                        email = email.substring(1,email.length());
                    }
                    if (email.endsWith(")"))
                    { 
                        email = email.substring(0,email.length()-1);
                    }
                    if (email.endsWith("&gt"))
                    { 
                        email = email.substring(0,email.length()-3);
                    }
                     
                    /*if (!previousEmail.equals(email))
                    {
                        if (emailMap.containsKey(previousEmail))
                        {
                            // Check if got send to same person
                            ArrayList list = (ArrayList)emailMap.get(previousEmail);
                            if (!list.contains(email))
                            {
                                list.add(email);
                            }
                            else
                            {
                                System.out.println("Skip for send from " + previousEmail + " to " + email);
                            }
                        }
                        else
                        {
                            ArrayList list = new ArrayList();
                            list.add(email);
                            emailMap.put(previousEmail, list);
                            System.out.println("Added from " + previousEmail + " to " + email);
                        }

                        previousEmail = email;
                    }*/
                    emailSet.add(email); //Set ensures no repeat
                }
            }
            
            System.out.println("Size = " + emailMap.size());
            System.out.println("Print to file");
            
            //writes to new file
            //PrintWriter out = new PrintWriter(new FileWriter("C:\\temp\\zemails_processed.txt"), true);
            //PrintWriter out = new PrintWriter(new FileWriter("C:\\temp\\zemails_derk_processed.txt"), true);
            PrintWriter out = new PrintWriter(new FileWriter("C:\\temp\\csFw1_processed.txt"), true);
            
            String str = null;
            //Set emailKeySet = (Set)emailMap.keySet();
            Set emailKeySet = emailSet;
            Iterator iterator = emailKeySet.iterator();
            while (iterator.hasNext()) {
                String email = (String)iterator.next();
                System.out.println("EMAIL = " + email);
                out.println(email);
                /*ArrayList recipientList = (ArrayList)emailMap.get(email);
                System.out.println("Size:" + recipientList.size());
                for(int i=0;i<recipientList.size();i++){
                    String recipient = (String)recipientList.get(i);
                    String recipientuser = recipient.substring(0,recipient.indexOf("@"));
                    String senderuser = email.substring(0,email.indexOf("@"));
                    String greetingCode = new SeasonGreetingBO().testGetGreetingCode(email,recipient,new Date());
                    String printout = email;
                    //String printout = "INSERT INTO SEASON_GREETING(SENDER_EMAIL, SENDER_NAME, RECEIVER_EMAIL, RECEIVER_NAME, MESSAGE, GREETING_CODE, SUBSCRIBE, STATUS, TYPE )" +
                    // " VALUES('"+email+"','"+senderuser+"','"+recipient+"','" + recipientuser +"','Wishing you happy holidays and a great year ahead!','"+greetingCode+"',0,'N',2);";
                    out.println(printout);
                }*/
                
            }
            
            out.close(); 
        } catch (IOException e) {
            System.out.println("IO exception =" + e );
            //logger.error("IO exception=" + e );
        }
        
    }
    
    private static void test15() {
        StringBuffer old_sb = new StringBuffer();
        StringBuffer sb = new StringBuffer();
        
        try {
            // Create a FileReader and then wrap it with BufferedReader.
            FileReader old_file_reader = new FileReader("C:\\temp\\zemails_frens.txt");
            FileReader file_reader = new FileReader("C:\\temp\\zemails_yahoo.txt");
            BufferedReader old_buf_reader = new BufferedReader(old_file_reader);
            BufferedReader buf_reader = new BufferedReader(file_reader);
            
            // Read each line of the file and look for the string of interest.
            do {
                String line = old_buf_reader.readLine();
                if (line == null) break;
                old_sb.append(line);
                old_sb.append(" ");
                //sb.append("\r\n");
            }while (true);
            old_buf_reader.close();
            
            do {
                String line = buf_reader.readLine();
                if (line == null) break;
                sb.append(line);
                sb.append(" ");
                //sb.append("\r\n");
            }while (true);
            buf_reader.close();
            
            // Old File
            HashSet oldEmailSet = new HashSet();
            String oldEmailAddresses = old_sb.toString();
            oldEmailAddresses = oldEmailAddresses.replaceAll(","," ");
            oldEmailAddresses = oldEmailAddresses.replaceAll(";"," ");
            //emailAddresses = emailAddresses.replaceAll("("," ");
            //emailAddresses = emailAddresses.replaceAll(")"," ");
            oldEmailAddresses = oldEmailAddresses.replaceAll("<"," ");
            oldEmailAddresses = oldEmailAddresses.replaceAll(">"," ");
            oldEmailAddresses = oldEmailAddresses.replaceAll("\""," ");
            StringTokenizer oldTokenizer = new StringTokenizer(oldEmailAddresses," ");
            while (oldTokenizer.hasMoreTokens()) {
                String email = oldTokenizer.nextToken().trim();
                if (Validator.validateEmail(email,true)) {
                    if (email.startsWith("("))
                    {
                        email = email.substring(1,email.length());
                    }
                    if (email.endsWith(")"))
                    { 
                        email = email.substring(0,email.length()-1);
                    }
                    oldEmailSet.add(email); //Set ensures no repeat
                }
            }
            
            System.out.println("Size = " + oldEmailSet.size());
            System.out.println("Print to file");
            
            
            // New file
            HashSet emailSet = new HashSet();
            String emailAddresses = sb.toString();
            emailAddresses = emailAddresses.replaceAll(","," ");
            emailAddresses = emailAddresses.replaceAll(";"," ");
            //emailAddresses = emailAddresses.replaceAll("("," ");
            //emailAddresses = emailAddresses.replaceAll(")"," ");
            emailAddresses = emailAddresses.replaceAll("<"," ");
            emailAddresses = emailAddresses.replaceAll(">"," ");
            emailAddresses = emailAddresses.replaceAll("\""," ");
            StringTokenizer tokenizer = new StringTokenizer(emailAddresses," ");
            while (tokenizer.hasMoreTokens()) {
                String email = tokenizer.nextToken().trim();
                if (Validator.validateEmail(email,true)) {
                    if (email.startsWith("("))
                    {
                        email = email.substring(1,email.length());
                    }
                    if (email.endsWith(")"))
                    { 
                        email = email.substring(0,email.length()-1);
                    }
                    if (oldEmailSet.contains(email))
                    {
                        System.out.println("Filter out:" + email);
                    }
                    else{
                        emailSet.add(email); //Set ensures no repeat
                    }
                }
            }
            
            System.out.println("Size = " + emailSet.size());
            System.out.println("Print to file");
            
            //writes to new file
            PrintWriter out = new PrintWriter(new FileWriter("C:\\temp\\zemails_yahoo_processed.txt"), true);
            
            String str = null;
            Iterator iterator = emailSet.iterator();
            while (iterator.hasNext()) {
                String email = (String)iterator.next();
                String emailuser = email.substring(0,email.indexOf("@"));
                String greetingCode = new SeasonGreetingBO().testGetGreetingCode("mclee81@hotmail.com",email,new Date());
                String printout = "INSERT INTO SEASON_GREETING(SENDER_EMAIL, SENDER_NAME, RECEIVER_EMAIL, RECEIVER_NAME, MESSAGE, GREETING_CODE, SUBSCRIBE, STATUS, TYPE )" +
                 " VALUES('mclee81@hotmail.com','Marvin Lee','"+email+"','" + emailuser +"','Wishing you happy holidays and a great year ahead! Hope I can share Starbucks coffee with you too if selected by 080808.com.my :p','"+greetingCode+"',0,'N',2);";
                out.println(printout);
            }
            
            out.close(); 
        } catch (IOException e) {
            System.out.println("IO exception =" + e );
            //logger.error("IO exception=" + e );
        }
        
    }
      
      
    private static void test14() {
        StringBuffer sb = new StringBuffer();
        
        try {
            // Create a FileReader and then wrap it with BufferedReader.
            //FileReader file_reader = new FileReader("C:\\temp\\zemails_frens.txt");
            FileReader file_reader = new FileReader("C:\\temp\\zemails_tzuchi2.txt");
            BufferedReader buf_reader = new BufferedReader(file_reader);
            
            // Read each line of the file and look for the string of interest.
            do {
                String line = buf_reader.readLine();
                if (line == null) break;
                sb.append(line);
                sb.append(" ");
                //sb.append("\r\n");
            }while (true);
            buf_reader.close();
            
            
            HashSet emailSet = new HashSet();
            String emailAddresses = sb.toString();
            emailAddresses = emailAddresses.replaceAll(","," ");
            emailAddresses = emailAddresses.replaceAll(";"," ");
            //emailAddresses = emailAddresses.replaceAll("("," ");
            //emailAddresses = emailAddresses.replaceAll(")"," ");
            emailAddresses = emailAddresses.replaceAll("<"," ");
            emailAddresses = emailAddresses.replaceAll(">"," ");
            emailAddresses = emailAddresses.replaceAll("\""," ");
            StringTokenizer tokenizer = new StringTokenizer(emailAddresses," ");
            while (tokenizer.hasMoreTokens()) {
                String email = tokenizer.nextToken().trim();
                if (Validator.validateEmail(email,true)) {
                    if (email.startsWith("("))
                    {
                        email = email.substring(1,email.length());
                    }
                    if (email.endsWith(")"))
                    { 
                        email = email.substring(0,email.length()-1);
                    }
                    emailSet.add(email); //Set ensures no repeat
                }
            }
            
            System.out.println("Size = " + emailSet.size());
            System.out.println("Print to file");
            
            //writes to new file
            //PrintWriter out = new PrintWriter(new FileWriter("C:\\temp\\zemails_frens_processed.txt"), true);
            PrintWriter out = new PrintWriter(new FileWriter("C:\\temp\\zemails_tzuchi2_processed.txt"), true);
            
            String str = null;
            Iterator iterator = emailSet.iterator();
            while (iterator.hasNext()) {
                String email = (String)iterator.next();
                String emailuser = email.substring(0,email.indexOf("@"));
                String greetingCode = new SeasonGreetingBO().testGetGreetingCode("2w1tzuchi_member@gmail.com",email,new Date());
                String printout = "INSERT INTO SEASON_GREETING(SENDER_EMAIL, SENDER_NAME, RECEIVER_EMAIL, RECEIVER_NAME, MESSAGE, GREETING_CODE, SUBSCRIBE, STATUS, TYPE )" +
                 " VALUES('2w1tzuchi_member@gmail.com','West One Group TZ Volunteer','"+email+"','" + emailuser +"','Join our blood donation drive on 1st January 2008, Kelana Jaya Center for a brand new 2008. Free health-screening from 10am to 1pm. Thanks.','"+greetingCode+"',0,'N',0);";
                out.println(printout);
            }
            
            out.close(); 
        } catch (IOException e) {
            System.out.println("IO exception =" + e );
            //logger.error("IO exception=" + e );
        }
        
    }
            //logger.err
            
    private static void test13() {
        StringBuffer sb = new StringBuffer();
        
        try {
            // Create a FileReader and then wrap it with BufferedReader.
            FileReader file_reader = new FileReader("C:\\temp\\zemails.txt");
            BufferedReader buf_reader = new BufferedReader(file_reader);
            
            // Read each line of the file and look for the string of interest.
            do {
                String line = buf_reader.readLine();
                if (line == null) break;
                sb.append(line);
                sb.append(" ");
                //sb.append("\r\n");
            }while (true);
            buf_reader.close();
            
            
            HashSet emailSet = new HashSet();
            String emailAddresses = sb.toString();
            emailAddresses = emailAddresses.replaceAll(","," ");
            emailAddresses = emailAddresses.replaceAll(";"," ");
            //emailAddresses = emailAddresses.replaceAll("("," ");
            //emailAddresses = emailAddresses.replaceAll(")"," ");
            emailAddresses = emailAddresses.replaceAll("<"," ");
            emailAddresses = emailAddresses.replaceAll(">"," ");
            emailAddresses = emailAddresses.replaceAll("\""," ");
            StringTokenizer tokenizer = new StringTokenizer(emailAddresses," ");
            while (tokenizer.hasMoreTokens()) {
                String email = tokenizer.nextToken().trim();
                if (Validator.validateEmail(email,true)) {
                    if (email.startsWith("("))
                    {
                        email = email.substring(1,email.length());
                    }
                    if (email.endsWith(")"))
                    { 
                        email = email.substring(0,email.length()-1);
                    }
                    emailSet.add(email); //Set ensures no repeat
                }
            }
            
            System.out.println("Size = " + emailSet.size());
            System.out.println("Print to file");
            
            //writes to new file
            PrintWriter out = new PrintWriter(new FileWriter("C:\\temp\\zemails_processed.txt"), true);
            
            String str = null;
            Iterator iterator = emailSet.iterator();
            while (iterator.hasNext()) {
                out.println(iterator.next());
            }
            
            out.close();
        } catch (IOException e) {
            System.out.println("IO exception =" + e );
            //logger.error("IO exception=" + e );
        }
        
    }
    /*
     private static void sendIndividualEmail( ) throws Exception {
     
        logger.debug( "Getting authentication presetting");
        EmailAuthentication auth = new EmailAuthentication(userId,password);
        logger.debug( "Getting mail server config");
        Session session = Session.getDefaultInstance( mailServerConfig, auth);
        logger.debug( "Creating Message");
        MimeMessage message = new MimeMessage( session );
     
        try {
            //the "from" address may be set in code, or set in the
            //config file under "mail.from" ; here, the latter style is used
            logger.debug( "Preparing email");
            message.setFrom( new InternetAddress(fromEmailAddr) );
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmailAddr));
            message.setSubject(subject);
            message.setText(mailBody);
            logger.debug( "Sending email");
            Transport.send( message );
            logger.debug( "Email sent!");
        } catch(AddressException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Address Exception Cannot send email"+ex);
            throw new MessagingException("Address Exception: "+ex);
        } catch(javax.mail.MessagingException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Messaging Exception Cannot send email"+ex);
            throw new MessagingException("Messaging Exception: "+ex);
        }
     
    }*/
    
    private static void test12() {
        String emailAddresses = "abc@ac.scom abc@ac.scom a@a.com, b@b.com;c@c.com  d@d.com f@f.com      g@g.com \n h@h.com";
        HashSet emailSet = new HashSet();
        // Forms email address string to a list of valid emails
        emailAddresses = emailAddresses.replaceAll(","," ");
        emailAddresses = emailAddresses.replaceAll(";"," ");
        StringTokenizer tokenizer = new StringTokenizer(emailAddresses," ");
        while (tokenizer.hasMoreTokens()) {
            String email = tokenizer.nextToken().trim();
            if (Validator.validateEmail(email,true)) {
                emailSet.add(email); //Set ensures no repeat
            }
        }
        
        List emailList = null;
        if (!emailSet.isEmpty()) {
            emailList = new ArrayList();
            emailList.addAll(emailSet);
        }
        
        if (emailList == null) {
            System.out.println("Email list is null");
        }else {
            for(int i=0;i<emailList.size();i++) {
                System.out.println("Email added=" + emailList.get(i));
            }
        }
        
    }
    
    private static void test11() throws Exception {
        long currentDate = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fromDate = sdf.parse("16/09/2007");
        long diff = currentDate - fromDate.getTime();
        System.out.println("Diff=" + diff);
    }
    
    
    private static void test10() {
        String CURRENT_MONTH_STRING_FORMAT = "MM/yyyy";
        SimpleDateFormat monthFormatter = new SimpleDateFormat(CURRENT_MONTH_STRING_FORMAT);
        Date date = new Date();
        Date currentMonth = null;
        String currentMonthString = monthFormatter.format(date);
        try{
            currentMonth = monthFormatter.parse(currentMonthString);
        }catch (Exception e) {
            System.out.println("Error parsing date");
        }
        System.out.println("Current Month=" + currentMonth);
    }
    
    private static void test9(){
        String str1 = null;
        String str2 = null;
        
        if (StringUtils.hasValue((str1 + str2).trim())) {
            System.out.println("Has value=" + (str1 + str2).trim());
        }else {
            System.out.println("No value=" + (str1 + str2).trim());
        }
        
        
    }
    private static int test8() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fromDate = sdf.parse("19/03/1980");
        long birthday = fromDate.getTime();
        Calendar age = Calendar.getInstance(Locale.getDefault());
        age.setTimeInMillis(Math.abs(birthday-System.currentTimeMillis()));
        System.out.println("Your are "+(age.get(Calendar.YEAR)-1970)+" years, "+age.get(Calendar.MONTH)+" month, "+age.get(Calendar.DAY_OF_MONTH)+" days "+(System.currentTimeMillis()-birthday<0 ?"Not Born Yet" :"Old")+".");
        System.out.println("Age is " + (age.get(Calendar.YEAR)-1970));
        return (age.get(Calendar.YEAR)-1970);
    }
    
    private static void test7() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date fromDate = null;
        Date toDate = null;
        Date currentDate = null;
        try {
            fromDate = sdf.parse("01/01/1981");
            toDate = sdf.parse("31/12/1981");
            currentDate = sdf.parse("24/08/1981");
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        System.out.println("Current year=" + sdf.format(currentDate).substring(6,10));
        System.out.println("Smaller" + (fromDate.getTime() < currentDate.getTime()));
        System.out.println("Larger" + (toDate.getTime() > currentDate.getTime()));
    }
    /*
    private static void test6() {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        org.hibernate.Session session = sessionFactory.openSession();
        session.beginTransaction();
        
        
        User user = (User)session.createCriteria(User.class)
        //.setFetchMode("permissions", FetchMode.JOIN)
        .add( Restrictions.idEq(new Long("2")))
        //.set("userId", Long.getLong("2"))
        //.setCacheable(true)
        .uniqueResult();
        System.out.println("User="+user);
        System.out.println("User email=" + user.getEmailAddress());
        System.out.println("User state=" + user.getUserCountry().getState());
        session.getTransaction().commit();
    }*/
    
    private static void test5() {
        String inputString = "::'AF'>Afghanistan::'AL'>Albania::'DZ'>Algeria::'AS'>American Samoa::'AD'>Andorra::'AO'>Angola::'AI'>Anguilla::'AQ'>Antarctica::'AG'>Antigua and Barbuda::'AR'>Argentina::'AM'>Armenia::'AW'>Aruba::'AU'>Australia::'AT'>Austria::'AZ'>Azerbaijan::'BS'>Bahamas::'BH'>Bahrain::'BD'>Bangladesh::'BB'>Barbados::'BY'>Belarus::'BE'>Belgium::'BZ'>Belize::'BJ'>Benin::'BM'>Bermuda::'BT'>Bhutan::'BO'>Bolivia::'BA'>Bosnia and Herzegovina::'BW'>Botswana::'BV'>Bouvet Island::'BR'>Brazil::'IO'>British Indian Ocean Territory::'BN'>Brunei Darussalam::'BG'>Bulgaria::'BF'>Burkina Faso::'BI'>Burundi::'KH'>Cambodia::'CM'>Cameroon::'CA'>Canada::'CV'>Cape Verde::'KY'>Cayman Islands::'CF'>Central African Republic::'TD'>Chad::'CL'>Chile::'CN'>China::'CX'>Christmas Island::'CC'>Cocos (Keeling) Islands::'CO'>Colombia::'KM'>Comoros::'CG'>Congo::'CK'>Cook Islands::'CR'>Costa Rica::'CI'>Cote D'Ivoire (Ivory Coast)::'HR'>Croatia (Hrvatska)::'CU'>Cuba::'CY'>Cyprus::'CZ'>Czech Republic::'CS'>Czechoslovakia (former)::'DK'>Denmark::'DJ'>Djibouti::'DM'>Dominica::'DO'>Dominican Republic::'TL'>East Timor::'EC'>Ecuador::'EG'>Egypt::'SV'>El Salvador::'GQ'>Equatorial Guinea::'ER'>Eritrea::'EE'>Estonia::'ET'>Ethiopia::'FK'>Falkland Islands (Malvinas)::'FO'>Faroe Islands::'FJ'>Fiji::'FI'>Finland::'FR'>France::'FX'>France, Metropolitan::'GF'>French Guiana::'PF'>French Polynesia::'TF'>French Southern Territories::'GA'>Gabon::'GM'>Gambia::'GE'>Georgia::'DE'>Germany::'GH'>Ghana::'GI'>Gibraltar::'GB'>Great Britain (UK)::'GR'>Greece::'GL'>Greenland::'GD'>Grenada::'GP'>Guadeloupe::'GU'>Guam::'GT'>Guatemala::'GN'>Guinea::'GW'>Guinea-Bissau::'GY'>Guyana::'HT'>Haiti::'HM'>Heard and McDonald Islands::'HN'>Honduras::'HU'>Hungary::'IS'>Iceland::'IN'>India::'ID'>Indonesia::'IR'>Iran::'IQ'>Iraq::'IE'>Ireland::'IL'>Israel::'IT'>Italy::'JM'>Jamaica::'JP'>Japan::'JO'>Jordan::'KZ'>Kazakhstan::'KE'>Kenya::'KI'>Kiribati::'KP'>Korea (North)::'KR'>Korea (South)::'KW'>Kuwait::'KG'>Kyrgyzstan::'LA'>Laos::'LV'>Latvia::'LB'>Lebanon::'LS'>Lesotho::'LR'>Liberia::'LY'>Libya::'LI'>Liechtenstein::'LT'>Lithuania::'LU'>Luxembourg::'MK'>Macedonia::'MG'>Madagascar::'MW'>Malawi::'MY'>Malaysia::'MV'>Maldives::'ML'>Mali::'MT'>Malta::'MH'>Marshall Islands::'MQ'>Martinique::'MR'>Mauritania::'MU'>Mauritius::'YT'>Mayotte::'MX'>Mexico::'FM'>Micronesia::'MD'>Moldova::'MC'>Monaco::'MN'>Mongolia::'MS'>Montserrat::'MA'>Morocco::'MZ'>Mozambique::'MM'>Myanmar::'NA'>Namibia::'NR'>Nauru::'NP'>Nepal::'NL'>Netherlands::'AN'>Netherlands Antilles::'NT'>Neutral Zone::'NC'>New Caledonia::'NZ'>New Zealand::'NI'>Nicaragua::'NE'>Niger::'NG'>Nigeria::'NU'>Niue::'NF'>Norfolk Island::'MP'>Northern Mariana Islands::'NO'>Norway::'OM'>Oman::'PK'>Pakistan::'PW'>Palau::'PA'>Panama::'PG'>Papua New Guinea::'PY'>Paraguay::'PE'>Peru::'PH'>Philippines::'PN'>Pitcairn::'PL'>Poland::'PT'>Portugal::'PR'>Puerto Rico::'QA'>Qatar::'RE'>Reunion::'RO'>Romania::'RU'>Russian Federation::'RW'>Rwanda::'GS'>S. Georgia and S. Sandwich Isls.::'KN'>Saint Kitts and Nevis::'LC'>Saint Lucia::'VC'>Saint Vincent and the Grenadines::'WS'>Samoa::'SM'>San Marino::'ST'>Sao Tome and Principe::'SA'>Saudi Arabia::'SN'>Senegal::'SC'>Seychelles::'SL'>Sierra Leone::'SG'>Singapore::'SK'>Slovak Republic::'SI'>Slovenia::'Sb'>Solomon Islands::'SO'>Somalia::'ZA'>South Africa::'ES'>Spain::'LK'>Sri Lanka::'SH'>St. Helena::'PM'>St. Pierre and Miquelon::'SD'>Sudan::'SR'>Suriname::'SJ'>Svalbard and Jan Mayen Islands</option>option value='SZ'>Swaziland::'SE'>Sweden::'CH'>Switzerland</option>option value='SY'>Syria::'TJ'>Tajikistan::'TZ'>Tanzania::'TH'>Thailand::'TG'>Togo::'TK'>Tokelau::'TO'>Tonga::'TT'>Trinidad and Tobago::'TN'>Tunisia::'TR'>Turkey::'TM'>Turkmenistan::'TC'>Turks and Caicos Islands::'TV'>Tuvalu::'UG'>Uganda::'UA'>Ukraine::'AE'>United Arab Emirates::'UK'>United Kingdom::'US'>United States::'UY'>Uruguay::'UM'>US Minor Outlying Islands::'SU'>USSR (former)::'UZ'>Uzbekistan::'VU'>Vanuatu::'VA'>Vatican City State (Holy Sea)::'VE'>Venezuela::'VN'>Viet Nam::'VG'>Virgin Islands (British)::'VI'>Virgin Islands (U.S.)::'WF'>Wallis and Futuna Islands::'EH'>Western Sahara::'YE'>Yemen::'YU'>Yugoslavia::'ZR'>Zaire::'ZM'>Zambia::'ZW'>Zimbabwe::'TW'>?? (Taiwan)::'MO'>?? (Macau)::'HK'>?? (Hong Kong)</option>";
        //String inputString = "<option value='AF'>Afghanistan::'AL'>Albania::'DZ'>Algeria::'AS'>American Samoa::'AD'>Andorra::'AO'>Angola::'AI'>Anguilla::'AQ'>Antarctica::'AG'>Antigua and Barbuda::'AR'>Argentina::'AM'>Armenia::'AW'>Aruba::'AU'>Australia::'AT'>Austria::'AZ'>Azerbaijan::'BS'>Bahamas::'BH'>Bahrain::'BD'>Bangladesh::'BB'>Barbados::'BY'>Belarus::'BE'>Belgium::'BZ'>Belize::'BJ'>Benin::'BM'>Bermuda::'BT'>Bhutan::'BO'>Bolivia::'BA'>Bosnia and Herzegovina::'BW'>Botswana::'BV'>Bouvet Island::'BR'>Brazil::'IO'>British Indian Ocean Territory::'BN'>Brunei Darussalam::'BG'>Bulgaria::'BF'>Burkina Faso::'BI'>Burundi::'KH'>Cambodia::'CM'>Cameroon::'CA'>Canada::'CV'>Cape Verde::'KY'>Cayman Islands::'CF'>Central African Republic::'TD'>Chad::'CL'>Chile::'CN'>China::'CX'>Christmas Island::'CC'>Cocos (Keeling) Islands::'CO'>Colombia::'KM'>Comoros::'CG'>Congo::'CK'>Cook Islands::'CR'>Costa Rica::'CI'>Cote D'Ivoire (Ivory Coast)::'HR'>Croatia (Hrvatska)::'CU'>Cuba::'CY'>Cyprus::'CZ'>Czech Republic::'CS'>Czechoslovakia (former)::'DK'>Denmark::'DJ'>Djibouti::'DM'>Dominica::'DO'>Dominican Republic::'TL'>East Timor::'EC'>Ecuador::'EG'>Egypt::'SV'>El Salvador::'GQ'>Equatorial Guinea::'ER'>Eritrea::'EE'>Estonia::'ET'>Ethiopia::'FK'>Falkland Islands (Malvinas)::'FO'>Faroe Islands::'FJ'>Fiji::'FI'>Finland::'FR'>France::'FX'>France, Metropolitan::'GF'>French Guiana::'PF'>French Polynesia::'TF'>French Southern Territories::'GA'>Gabon::'GM'>Gambia::'GE'>Georgia::'DE'>Germany::'GH'>Ghana::'GI'>Gibraltar::'GB'>Great Britain (UK)::'GR'>Greece::'GL'>Greenland::'GD'>Grenada::'GP'>Guadeloupe::'GU'>Guam::'GT'>Guatemala::'GN'>Guinea::'GW'>Guinea-Bissau::'GY'>Guyana::'HT'>Haiti::'HM'>Heard and McDonald Islands::'HN'>Honduras::'HU'>Hungary::'IS'>Iceland::'IN'>India::'ID'>Indonesia::'IR'>Iran::'IQ'>Iraq::'IE'>Ireland::'IL'>Israel::'IT'>Italy::'JM'>Jamaica::'JP'>Japan::'JO'>Jordan::'KZ'>Kazakhstan::'KE'>Kenya::'KI'>Kiribati::'KP'>Korea (North)::'KR'>Korea (South)::'KW'>Kuwait::'KG'>Kyrgyzstan::'LA'>Laos::'LV'>Latvia::'LB'>Lebanon::'LS'>Lesotho::'LR'>Liberia::'LY'>Libya::'LI'>Liechtenstein::'LT'>Lithuania::'LU'>Luxembourg::'MK'>Macedonia::'MG'>Madagascar::'MW'>Malawi::'MY'>Malaysia::'MV'>Maldives::'ML'>Mali::'MT'>Malta::'MH'>Marshall Islands::'MQ'>Martinique::'MR'>Mauritania::'MU'>Mauritius::'YT'>Mayotte::'MX'>Mexico::'FM'>Micronesia::'MD'>Moldova::'MC'>Monaco::'MN'>Mongolia::'MS'>Montserrat::'MA'>Morocco::'MZ'>Mozambique::'MM'>Myanmar::'NA'>Namibia::'NR'>Nauru::'NP'>Nepal::'NL'>Netherlands::'AN'>Netherlands Antilles::'NT'>Neutral Zone::'NC'>New Caledonia::'NZ'>New Zealand::'NI'>Nicaragua::'NE'>Niger::'NG'>Nigeria::'NU'>Niue::'NF'>Norfolk Island::'MP'>Northern Mariana Islands::'NO'>Norway::'OM'>Oman::'PK'>Pakistan::'PW'>Palau::'PA'>Panama::'PG'>Papua New Guinea::'PY'>Paraguay::'PE'>Peru::'PH'>Philippines::'PN'>Pitcairn::'PL'>Poland::'PT'>Portugal::'PR'>Puerto Rico::'QA'>Qatar::'RE'>Reunion::'RO'>Romania::'RU'>Russian Federation::'RW'>Rwanda::'GS'>S. Georgia and S. Sandwich Isls.::'KN'>Saint Kitts and Nevis::'LC'>Saint Lucia::'VC'>Saint Vincent and the Grenadines::'WS'>Samoa::'SM'>San Marino::'ST'>Sao Tome and Principe::'SA'>Saudi Arabia::'SN'>Senegal::'SC'>Seychelles::'SL'>Sierra Leone::'SG'>Singapore::'SK'>Slovak Republic::'SI'>Slovenia::'Sb'>Solomon Islands::'SO'>Somalia::'ZA'>South Africa::'ES'>Spain::'LK'>Sri Lanka::'SH'>St. Helena::'PM'>St. Pierre and Miquelon::'SD'>Sudan::'SR'>Suriname::'SJ'>Svalbard and Jan Mayen Islands</option>option value='SZ'>Swaziland::'SE'>Sweden::'CH'>Switzerland</option>option value='SY'>Syria::'TJ'>Tajikistan::'TZ'>Tanzania::'TH'>Thailand::'TG'>Togo::'TK'>Tokelau::'TO'>Tonga::'TT'>Trinidad and Tobago::'TN'>Tunisia::'TR'>Turkey::'TM'>Turkmenistan::'TC'>Turks and Caicos Islands::'TV'>Tuvalu::'UG'>Uganda::'UA'>Ukraine::'AE'>United Arab Emirates::'UK'>United Kingdom::'US'>United States::'UY'>Uruguay::'UM'>US Minor Outlying Islands::'SU'>USSR (former)::'UZ'>Uzbekistan::'VU'>Vanuatu::'VA'>Vatican City State (Holy Sea)::'VE'>Venezuela::'VN'>Viet Nam::'VG'>Virgin Islands (British)::'VI'>Virgin Islands (U.S.)::'WF'>Wallis and Futuna Islands::'EH'>Western Sahara::'YE'>Yemen::'YU'>Yugoslavia::'ZR'>Zaire::'ZM'>Zambia::'ZW'>Zimbabwe::'TW'>?? (Taiwan)::'MO'>?? (Macau)::'HK'>?? (Hong Kong)</option>";
        StringTokenizer tokenizer = new StringTokenizer(inputString,"::");
        System.out.println("total tokens=" + tokenizer.countTokens());
        while (tokenizer.hasMoreTokens()) {
            String country = tokenizer.nextToken();
            //System.out.println(country+",");
            String code = country.substring(5,country.length());
            System.out.print("\"" + code+"\",");
            
        }
        
        
    }
    
    private static void test4() {
        
        Vector countryList = CountryComboHelper.getCountryList();
        for(int i=0;i<countryList.size();i++) {
            String[] country = (String[])countryList.get(i);
            System.out.print("<option value='" + country[0] +"'");
            if (country[0].equals("MY"))
                System.out.print(" selected ");
            System.out.print(">");
            System.out.println(country[1] + "</option>");
        }
    }
    
    private static void test1() {
        
        String actionPath = "/Web/register.do?jsessionid=blahblahblah";
        String output = actionPath;
        if (actionPath.startsWith("/Web/")) {
            output = actionPath.substring(4,actionPath.length());
        }
        //int index = actionPath.indexOf("/Web/");
        System.out.println("Output=" + output);
    }
    
    private static void test2() {
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date birthDate = new Date(Integer.parseInt("1920"),Integer.parseInt("03"),Integer.parseInt("31"));
        System.out.println("Date=" + birthDate);
        String birthdayString = sdf.format(birthDate);
        System.out.println("String=" + birthdayString);
        try {
            birthDate = sdf.parse("31/03/1920");
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        System.out.println("Date=" + birthDate);
        birthdayString = sdf.format(birthDate);
        System.out.println("String=" + birthdayString);
        
    }
    
    private static void test3() {
        for (Locale locale : Locale.getAvailableLocales()) {
            final String contry = locale.getDisplayCountry();
            if (contry.length() > 0) {
                System.out.println("Country = " + contry + ". ISO code: " + locale.getCountry());
            }
        }
        
    }
    
}
