package com.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.esmart2u.oeight.admin.helper.service.RSSPuller;
import com.esmart2u.oeight.admin.scheduler.jobs.FeedGrabberJob;
import com.esmart2u.oeight.data.GoogleAlertTweet;
import com.esmart2u.oeight.data.UserPost;
import com.rosaloves.net.shorturl.bitly.Bitly;
import com.rosaloves.net.shorturl.bitly.BitlyFactory;
import com.rosaloves.net.shorturl.bitly.url.BitlyUrl;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken; 

public class TweetTest 
{
	private static Logger logger = Logger.getLogger(TweetTest.class);
	public static void main(String[] args) throws Throwable
	{
		// Grab Feed
		//String androidUrl = "http://www.google.com/alerts/feeds/00141091537082778248/4445073440291612029";
		String priusUrl = "http://www.google.com/alerts/feeds/00141091537082778248/4445073440291612029";
		//grabFeed(priusUrl);
		
		//setup();
		
		// Test tweet 

		//String status = "Popular Android Apps Full of Bugs: Researchers Blame Recycling of Code http://bit.ly/UEDwFL";
		//String status = "2014 Ford Fiesta SE Hatch vs 2014 Toyota Prius c Three http://bit.ly/1pv40Cj";
		//testTweet(status);
		
	}
	
	
	private static void testTweet(String status) 
	{

		try {
			Twitter androidTwitter = TwitterFactory.getSingleton();
			androidTwitter.setOAuthConsumer("7wfpZRABMFLFW9OdXdVAtA","vAGIkeU0JgWxFgolGupGV2ORHNMujCCmHnG9QJOHl8");
			//Android MY
		    //AccessToken accessToken = new AccessToken("56295679-r69txFwmGS4N4lTndNHA1cikHDVOU3B7zDIMHMc4E", "1DA2pQSTsfmoPu2mo3AfhohGFeUp2RACLqp1J2go");
	
			//PRIUS MY
			AccessToken accessToken = new AccessToken("56294488-YvnBoR8jVb52bmSujHRpmij0J0J4mwcM8buH878eJ","rJS2lsRstQXhgGXXtXkr0pLnS2Cs9d4cwAmT3slPLr4"); 
			androidTwitter.setOAuthAccessToken(accessToken);
			androidTwitter.updateStatus(status);
			
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private static void setup() throws TwitterException, IOException
	{
	      // The factory instance is re-useable and thread safe.
	      Twitter twitter = new TwitterFactory().getInstance();
	      //insert the appropriate consumer key and consumer secret here      
	      twitter.setOAuthConsumer("7wfpZRABMFLFW9OdXdVAtA","vAGIkeU0JgWxFgolGupGV2ORHNMujCCmHnG9QJOHl8");
	      RequestToken requestToken = twitter.getOAuthRequestToken();
	      AccessToken accessToken = null;
	      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	      while (null == accessToken) {
	        System.out.println("Open the following URL and grant access to your account:");
	        System.out.println(requestToken.getAuthorizationURL());
	        System.out.print("Enter the PIN(if aviailable) or just hit enter.[PIN]:");
	        String pin = br.readLine();
	        try{
	          
	          
	           if(pin.length() > 0){
	             accessToken = twitter.getOAuthAccessToken(requestToken, pin);
	           }else{
	             accessToken = twitter.getOAuthAccessToken();
	           }
	        } catch (TwitterException te) {
	          if(401 == te.getStatusCode()){
	            System.out.println("Unable to get the access token.");
	          }else{
	            te.printStackTrace();
	          }
	        }
	      }
	      //persist to the accessToken for future reference.
	      System.out.println(twitter.verifyCredentials().getId());
	      System.out.println("token : " + accessToken.getToken());
	      System.out.println("tokenSecret : " + accessToken.getTokenSecret());
	      //storeAccessToken(twitter.verifyCredentials().getId() , accessToken);  
	      
	      /*
	       * 56295679
token : 56295679-r69txFwmGS4N4lTndNHA1cikHDVOU3B7zDIMHMc4E
tokenSecret : 1DA2pQSTsfmoPu2mo3AfhohGFeUp2RACLqp1J2go
	       */

	}

	private static void grabFeed(String feedUrl)
	{ 
		ArrayList dataList = getPosts(feedUrl);  
		if (dataList != null && !dataList.isEmpty())
		{
			List alertTweetList = new ArrayList();
			// Saves into db for each alert account
			for(int j=0; j< dataList.size(); j++)
			{ 
                UserPost userPost = (UserPost)dataList.get(j);
                //logger.debug("Got feed: " + userPost.getPostTitle() + ":" + userPost.getPostUrl());
                
                String title = userPost.getPostTitle();
                title = title.replaceAll("<b>", "");
                title = title.replaceAll("</b>", "");
                title = title.replaceAll("&amp;", "&");
                title = title.replaceAll("&#39;", "'");

                System.out.println("Got feed title: " + title);
                 
				try {
					// Get an instance using the API demo credentials.
					Bitly bitly = BitlyFactory.newInstance("marvinlee", "R_6f7cf7d7cfe4421502ac918fd778598a");

					// Shorten the URL to the Hope Transfusion story.
					BitlyUrl bUrl = bitly.shorten(userPost.getPostUrl());
					String shortUrl = bUrl.getShortUrl().toString();
					System.out.println("Short URL:" + shortUrl); 
					// bUrl.getShortUrl() -> http://bit.ly/fB05
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				 
			}// end for loop - every tweet
			 
			
		}
	}

	private static ArrayList getPosts(String feedUrl) {
		ArrayList dataList = null;

		try {
			if (feedUrl != null && !"".equals(feedUrl))
			{
	        	RSSPuller pullerInstance = new RSSPuller();
	        	dataList = pullerInstance.getRssFeedEntries(new URL(feedUrl));
			}
		} catch (MalformedURLException e) {
			logger.error("Unable to retrieve feeds from:" + feedUrl );
			e.printStackTrace();
		}
		
		return dataList;
	}   
}
