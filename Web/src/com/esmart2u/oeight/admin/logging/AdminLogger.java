/*
 * AdminLogger.java
 *
 * Created on April 18, 2008, 11:59 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.logging;

import com.esmart2u.solution.base.logging.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class AdminLogger {
    
    public static final int INFO = 1;
    public static final int DEBUG = 2;
    public static final int WARN = 3;
    public static final int ERROR = 4; 
    private static Logger logger = Logger.getLogger(AdminLogger.class);
    
    /** Creates a new instance of AdminLogger */
    public AdminLogger() {
    } 
    
    
    public static void notifyAdmin(int debugLevel, String message)
    {
        switch (debugLevel)
        {
            case INFO:
                logger.info(message);
                break;
            case DEBUG:
                logger.debug(message);
                break;
            case WARN:
                logger.warn(message);
                break;
            case ERROR:
                logger.error(message);
                break;
        
        }
    }
    
    /**
     *  Defaults to DEBUG level
     */
    public static void notifyAdmin(String message)
    {
        notifyAdmin(DEBUG, message);        
    }
}
