package com.esmart2u.oeight.admin.bo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

import com.esmart2u.oeight.data.GoogleAlert;
import com.esmart2u.oeight.data.GoogleAlertTweet;
import com.esmart2u.oeight.data.OEightPledge;
import com.esmart2u.oeight.data.RankingPodium;
import com.esmart2u.oeight.member.bo.EarthBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;

public class GoogleAlertBO {

    private static Logger logger = Logger.getLogger(GoogleAlertBO.class);
    
	public List getAlerts() {
		 List alertList = null;
		 Session session = HibernateUtil.getSessionFactory().openSession();
         Transaction transaction = null;
         try {
             
             transaction = session.beginTransaction();
            
             Criteria crit = session.createCriteria(GoogleAlert.class);
             //Get only when not too recent
             //crit.add(Expression.eq( "snapshotMonth", lastMonth));
             alertList = crit.list();
             logger.debug("Got alertList");  
             transaction.commit();
             
         } catch (Exception e) {
             if (transaction!=null) transaction.rollback();
             //throw e;
         } finally {
             session.close();
         }
         return alertList;
	}

	public void insertAlert(GoogleAlert alert) {
		 
        Session session = HibernateUtil.getSessionFactory().openSession();
         
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction(); 
            session.save(alert); 

            transaction.commit();
            
        } catch (Exception e) {
        	e.printStackTrace();
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
		
	}

	public boolean foundTweet(GoogleAlertTweet tweet) {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        boolean found = false;
        try {
        
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(GoogleAlertTweet.class);  
            criteria.add(Expression.eq( "title", tweet.getTitle() ));
            criteria.add(Expression.eq( "longUrl", tweet.getLongUrl() ));   
            criteria.setMaxResults(1);
            List resultList = criteria.list();

            if (resultList != null && !resultList.isEmpty())
            {
            	found = true;
            } 
            transaction.commit();
            
        } catch (Exception e) {
        	e.printStackTrace();
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
		return found;
	}

	public void saveTweet(GoogleAlertTweet eachTweet) {
		 
        Session session = HibernateUtil.getSessionFactory().openSession();
         
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction(); 
            session.save(eachTweet); 

            transaction.commit();
            
        } catch (Exception e) {
        	e.printStackTrace();
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
	}

	public List getPendingTweets() {
		List pendingTweetList = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
           
            Criteria crit = session.createCriteria(GoogleAlertTweet.class);
            //Get only when not too recent
            crit.add(Expression.eq("publishedFlag", 'N'));
            crit.add(Expression.le("publishedTime", new Date())); 
            crit.addOrder( Order.asc("publishedTime")) ;
            pendingTweetList = crit.list();
            logger.debug("Got pendingTweetList");  
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return pendingTweetList;
	}

	public void updateTweeted(List doneTweetList) {	 
        Session session = HibernateUtil.getSessionFactory().openSession();
         
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction(); 
            for(int i=0; i<doneTweetList.size(); i++)
            {
            	GoogleAlertTweet tweet = (GoogleAlertTweet)doneTweetList.get(i);
            	tweet.setPublishedFlag('Y');
            	tweet.setTweetedTime(new Date());
            	session.update(tweet);
            	logger.debug("Done update with " + tweet.getAlertTweetId());
            } 

            transaction.commit();
            
        } catch (Exception e) {
        	e.printStackTrace();
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
		
	}

	public void updateCrawled(List alertsList) {
        Session session = HibernateUtil.getSessionFactory().openSession();
         
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction();  
            for(int i=0; i<alertsList.size(); i++)
            {
            	GoogleAlert alert = (GoogleAlert)alertsList.get(i);
            	alert.setDateCrawled(new Date());
            	session.update(alert);
            	logger.debug("Done update with " + alert.getAlertId());
            } 

            transaction.commit();
            
        } catch (Exception e) {
        	e.printStackTrace();
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
		
	}

}
