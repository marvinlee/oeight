/*
 * LoginBO.java
 *
 * Created on September 20, 2007, 4:00 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.bo;

import com.esmart2u.oeight.admin.web.struts.action.UserLogin;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import org.hibernate.*;
import org.hibernate.criterion.Expression;

import java.util.*;
import org.hibernate.criterion.Restrictions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;

/**
 *
 * @author meauchyuan.lee
 */
public class LoginBO {
    
    /** Creates a new instance of LoginBO */
    public LoginBO() {
    }
    
    public List listUsers() {
        
        try  {
            InitialContext initCtx = new InitialContext();
            DataSource ds = (DataSource)initCtx.lookup("java:comp/env/jdbc/firststep");
            Connection conn = ds.getConnection();
            
            //System.out.println("Connection from DataSource successfully opened!<br>");
            
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from user_login");
            
            while (rs.next() )  {
                String onerow = "Email " + rs.getString("email_address");
                //System.out.println(onerow);
            }
            
            rs.close();
            stmt.close();
            conn.close();
            initCtx.close();
            
            System.out.println("Connection from DataSource successfully closed!<br>");
        } catch(Exception e)  {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        
        return new ArrayList();
        
        /*Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
         
        List result = session.createQuery("from UserLogin").list();
         
        session.getTransaction().commit();
         
        return result;*/
    }
    
    public UserLogin getUserDetails(String emailAddress) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserLogin userLogin = null;
        
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction();
            
            //List result = session.createQuery("from UserLogin").list();
            userLogin = (UserLogin)session.createCriteria(UserLogin.class)
            .add( Restrictions.naturalId()
            .set("emailAddress", emailAddress)
            ).setCacheable(true)
            .uniqueResult();
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        /*session.getTransaction().commit();
        if (session.isOpen()){
            session.close();
        }*/
        
        return userLogin;
    }
}
