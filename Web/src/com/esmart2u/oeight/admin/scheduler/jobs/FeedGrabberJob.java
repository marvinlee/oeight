package com.esmart2u.oeight.admin.scheduler.jobs;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.esmart2u.oeight.admin.bo.GoogleAlertBO;
import com.esmart2u.oeight.admin.helper.service.RSSPuller;
import com.esmart2u.oeight.data.GoogleAlert;
import com.esmart2u.oeight.data.GoogleAlertTweet;
import com.esmart2u.oeight.data.UserPost;
import com.rosaloves.net.shorturl.bitly.Bitly;
import com.rosaloves.net.shorturl.bitly.BitlyFactory;
import com.rosaloves.net.shorturl.bitly.url.BitlyUrl;

public class FeedGrabberJob implements Job
{
	private static Logger logger = Logger.getLogger(FeedGrabberJob.class);
	
	public void execute(JobExecutionContext context) throws JobExecutionException
    {		
		logger.debug("FeedGrabberJob started");
		GoogleAlertBO alertBO = new GoogleAlertBO();
		List alertsList = alertBO.getAlerts();
		
		if (alertsList == null || alertsList.isEmpty())
		{ 
			logger.debug("alertList is null, hardcode initialization");
			String feedUrl = "http://www.google.com/alerts/feeds/00141091537082778248/16787770561321566060";
			GoogleAlert gAlert = new GoogleAlert();
			gAlert.setAlertId(1);
			gAlert.setAlertTerm("android");
			gAlert.setDateCrawled(new Date());
			gAlert.setFeedUrl(feedUrl);
			gAlert.setFrequency(1);
			
			alertBO.insertAlert(gAlert);
			alertsList.add(gAlert);
			
			String feedPriusUrl = "http://www.google.com/alerts/feeds/00141091537082778248/4445073440291612029";
			GoogleAlert gPriusAlert = new GoogleAlert();
			gAlert.setAlertId(2);
			gPriusAlert.setAlertTerm("prius");
			gPriusAlert.setDateCrawled(new Date());
			gPriusAlert.setFeedUrl(feedPriusUrl);
			gPriusAlert.setFrequency(1);
			
			alertBO.insertAlert(gPriusAlert); 
			alertsList.add(gPriusAlert);
		} 
		
		for(int i=0;i<alertsList.size();i++)
		{
			GoogleAlert gAlert = (GoogleAlert)alertsList.get(i);
			gAlert.setDateCrawled(new Date());
			ArrayList dataList = getPosts(gAlert.getFeedUrl());  
			if (dataList != null && !dataList.isEmpty())
			{
				List alertTweetList = new ArrayList();
				// Saves into db for each alert account
				for(int j=0; j< dataList.size(); j++)
				{ 
	                UserPost userPost = (UserPost)dataList.get(j);
	                logger.debug("Got feed: " + userPost.getPostTitle() + ":" + userPost.getPostUrl());
	                
	                String title = userPost.getPostTitle();
	                title = title.replaceAll("<b>", "");
	                title = title.replaceAll("</b>", "");
	                title = title.replaceAll("&amp;", "&");
	                title = title.replaceAll("&#39;", "'");
	                GoogleAlertTweet tweet = new GoogleAlertTweet();
	                tweet.setAlertId(gAlert.getAlertId());
	                tweet.setAlertTerm(gAlert.getAlertTerm());
	                tweet.setReceivedTime(gAlert.getDateCrawled());
	                tweet.setPublishedFlag('N');
	                tweet.setTitle(title);
	                tweet.setLongUrl(userPost.getPostUrl());
	                 
					try {
						// Get an instance using the API demo credentials.
						Bitly bitly = BitlyFactory.newInstance("marvinlee", "R_6f7cf7d7cfe4421502ac918fd778598a");

						// Shorten the URL to the Hope Transfusion story.
						BitlyUrl bUrl = bitly.shorten(userPost.getPostUrl());
						String shortUrl = bUrl.getShortUrl().toString();
						logger.debug("Short URL:" + shortUrl);
						tweet.setShortUrl(shortUrl);
						// bUrl.getShortUrl() -> http://bit.ly/fB05
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					// Save the tweet, break loop if found
					if (alertBO.foundTweet(tweet))
					{
						logger.debug("Found tweet, breaking out...");
						break;
					}
					else
					{
						alertTweetList.add(tweet);
					}
				}// end for loop - every tweet
				
				// Shuffle and save with time
				if (alertTweetList != null && !alertTweetList.isEmpty())
				{
					Collections.shuffle(alertTweetList);
					long fiveHourDuration = 1000 * 60 * 60 * 5;
					long diffPerTweet = fiveHourDuration / alertTweetList.size();
					long runningTime = diffPerTweet;
					for(int k=0;k<alertTweetList.size();k++)
					{
						GoogleAlertTweet eachTweet = (GoogleAlertTweet)alertTweetList.get(k);
						eachTweet.setPublishedTime(new Date(eachTweet.getReceivedTime().getTime() + runningTime));
						runningTime+= diffPerTweet;
						alertBO.saveTweet(eachTweet);
					}
				}
				
			}
			
		}// end for loop - every Google Alert 
		
		alertBO.updateCrawled(alertsList);
        logger.info("GoogleAlert job done");

  	}

	private ArrayList getPosts(String feedUrl) {
		ArrayList dataList = null;

		try {
			if (feedUrl != null && !"".equals(feedUrl))
			{
	        	RSSPuller pullerInstance = new RSSPuller();
	        	dataList = pullerInstance.getRssFeedEntries(new URL(feedUrl));
			}
		} catch (MalformedURLException e) {
			logger.error("Unable to retrieve feeds from:" + feedUrl );
			e.printStackTrace();
		}
		
		return dataList;
	}   
        
        public static void main(String[] args)
        {
            FeedGrabberJob fjob = new FeedGrabberJob();
            fjob.getPosts("http://www.google.com/alerts/feeds/00141091537082778248/16787770561321566060");
            /*
        try {
            
            //fjob.execute(null);
        } catch (JobExecutionException ex) {
            ex.printStackTrace();
        }*/
        }
}
