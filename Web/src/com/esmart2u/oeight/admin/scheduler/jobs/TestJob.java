package com.esmart2u.oeight.admin.scheduler.jobs;


import org.apache.log4j.Logger;
import java.io.*;
import java.util.*;
import org.quartz.*;

import javax.naming.Context; 
import java.net.URL;
import java.net.URLConnection;
 

public class TestJob implements Job
{
	private static Logger logger = Logger.getLogger(TestJob.class);
	
	public void execute(JobExecutionContext context) throws JobExecutionException
    {		
		// Every job has its own job detail
          JobDetail jobDetail = context.getJobDetail();

          // The name and group are defined in the job detail
          String jobName = jobDetail.getName();
          logger.info("Name: " + jobDetail.getFullName());

          // The name of this class configured for the job
          logger.info("Job Class : " + jobDetail.getJobClass()); 
         /*
         try 
         { 
			     URL url = new URL("http://localhost:7001/Eligibility/runletters");
			     URLConnection con = url.openConnection();
			    
			     con.setDoOutput(true);
				 InputStreamReader isr = new InputStreamReader(con.getInputStream()); 

			     System.out.println("Servlet called");
			} 
			catch(IOException ioe) 
	    	{
		    	System.out.println("Servlet abaneded");
	      		ioe.printStackTrace();
	    	}
	    	catch(Exception ioe) 
	    	{
		    	System.out.println("Servlet abaneded");
	      		ioe.printStackTrace();
	    	}
			*/
     	
     
          // Log the time the job started
          logger.info(jobName + " fired at " + context.getFireTime());

          logger.info("Next fire time " + context.getNextFireTime());

  	}   
}
