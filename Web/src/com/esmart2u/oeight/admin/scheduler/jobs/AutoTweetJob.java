package com.esmart2u.oeight.admin.scheduler.jobs;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import twitter4j.DirectMessage;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.http.AccessToken;

import com.esmart2u.oeight.admin.bo.GoogleAlertBO;
import com.esmart2u.oeight.data.GoogleAlertTweet;

public class AutoTweetJob implements Job
{
	private static Logger logger = Logger.getLogger(AutoTweetJob.class);
	
	public void execute(JobExecutionContext context) throws JobExecutionException
    {	
		
		logger.debug("AutoTweetJob started");
		GoogleAlertBO alertBO = new GoogleAlertBO();
		List pendingTweetList = alertBO.getPendingTweets();
		List doneTweetList = null;
		if (pendingTweetList != null && !pendingTweetList.isEmpty())
		{ 
			doneTweetList = goTweet(pendingTweetList);
		}
		else
		{
			logger.debug("No pending tweets...");
		}
		
		if ((doneTweetList != null) && (doneTweetList.size()>0))
		{
			alertBO.updateTweeted(doneTweetList);
		}
		
    }

	private List goTweet(List pendingTweetList) {
		
		List tweetedList = new ArrayList();
		
		// Twitter for Android
		//Twitter androidTwitter = new Twitter("test_marvin", "marvintwit");
		//AccessToken accessToken = new AccessToken("112417272-8AK582tE3qbr1nWGUt94AI5ysjFNLZuxUGvFwXeU","nM1Qr4Maurm1VWtzvQluGla5svCmQtxM9LrfgkNKKs");
		Twitter androidTwitter = new Twitter();
		AccessToken accessToken = new AccessToken("56295679-r69txFwmGS4N4lTndNHA1cikHDVOU3B7zDIMHMc4E","1DA2pQSTsfmoPu2mo3AfhohGFeUp2RACLqp1J2go");
		androidTwitter.setOAuthAccessToken(accessToken);
		  
		
		// Twitter for Prius
		Twitter priusTwitter = new Twitter();
		AccessToken priusToken = new AccessToken("56294488-YvnBoR8jVb52bmSujHRpmij0J0J4mwcM8buH878eJ","rJS2lsRstQXhgGXXtXkr0pLnS2Cs9d4cwAmT3slPLr4");
		priusTwitter.setOAuthAccessToken(priusToken);
		

		int countAndroid = 0;
		int countPrius = 0;
		for(int i=0; i<pendingTweetList.size(); i++)
		{
			GoogleAlertTweet tweet = (GoogleAlertTweet)pendingTweetList.get(i);
			if (tweet.getAlertTerm().equals("android"))
			{ 
				try {
					String statusUpdate= tweet.getTitle() + " - " + tweet.getShortUrl();
					//Max at any one time only 3 tweets 
					if (statusUpdate.length() < 140 && countAndroid <1)
					{
						Status status = androidTwitter.updateStatus(statusUpdate);
						logger.debug("Successfully updated the status to [" + status.getText() + "].");
						tweetedList.add(tweet);
						countAndroid +=1;
					} 
	          
		        } catch (TwitterException te) {
		            System.out.println("Failed to send message: " + te.getMessage());
		            
		        }
				
			}
			else if  (tweet.getAlertTerm().equals("prius"))
			{
				try {
					String statusUpdate= tweet.getTitle() + " - " + tweet.getShortUrl();
					//Max at any one time only 3 tweets 
					if (statusUpdate.length() < 140 && countPrius <1)
					{
						Status status = priusTwitter.updateStatus(statusUpdate);
						logger.debug("Successfully updated the status to [" + status.getText() + "].");
						tweetedList.add(tweet);
						countPrius +=1;
					} 
	          
		        } catch (TwitterException te) {
		            System.out.println("Failed to send message: " + te.getMessage());
		            
		        }
			}
			
		}
		return tweetedList;
	}
        
         
        public static void main(String[] args)
        {
            AutoTweetJob tjob = new AutoTweetJob(); 
        try {
            
            tjob.execute(null);
        } catch (JobExecutionException ex) {
            ex.printStackTrace();
        } 
        }
}
