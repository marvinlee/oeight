package com.esmart2u.oeight.admin.scheduler.struts;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.CronTrigger;
import org.quartz.Trigger;
import org.quartz.JobDetail;
import org.quartz.ee.servlet.QuartzInitializerServlet;
import org.quartz.SchedulerException;

import org.apache.log4j.Logger;
import java.util.*;

public final class JobAction extends Action
{
	private static Logger logger = Logger.getLogger(JobAction.class);
	public static final String QUARTZ_FACTORY_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";


    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            								throws Exception
    {
        
        //DocDispatcher docDispatcher = new DocDispatcher();
        //docDispatcher.dispatchDoc(3);
        
        String action = null;
        HttpSession session = request.getSession();
        boolean flag = false;

        JobForm jobForm = (JobForm) form;
        ActionErrors errors = new ActionErrors();
        action = request.getParameter("action");
        logger.debug("action = "+action);
		
        if(action == null)
		   action = "";
		else
		   action = action.trim();

        if (action.equalsIgnoreCase(""))
        {
	    	setJobListinSession(session);	  
    	}
    	else if (action.equalsIgnoreCase("Refresh List"))
        {
	    	setJobListinSession(session);	  
    	}
        else if (action.equalsIgnoreCase("Choose"))
        {
	        int idx = new Integer(request.getParameter("idx")).intValue();
            if ( session.getAttribute("session_jobList") != null )
            {
                ArrayList temp_arVO = (ArrayList) session.getAttribute("session_jobList");
 				JobVO jobVO = (JobVO) temp_arVO.get(idx);
			    jobForm.setJobGroup(jobVO.getJobGroup());
			    jobForm.setJobName(jobVO.getJobName());
			    jobForm.setJobClass(jobVO.getJobClass());
			    jobForm.setHour(jobVO.getHour());
			    jobForm.setMinutes(jobVO.getMinutes());
			    jobForm.setJobFrequency(jobVO.getJobFrequency());
			    jobForm.setTriggerName(jobVO.getTriggerName());
			    jobForm.setMinutesFrequency(jobVO.getMinutesFrequency());
			    
			    //System.out.println("New Trigger Name:" + jobVO.getTriggerName() + "");
            }
            else {
                logger.debug("Unable to find session attribute");
        	}
          
        }
        else if (action.equalsIgnoreCase("Add Job"))
        {
	       //System.out.println("Add Job");
	       flag = false;
	       flag = addJob(session, jobForm); 
	       
	       if(flag)
	       {
		     errors.add( ActionErrors.GLOBAL_ERROR, new ActionError("errors.job.add.success")); 
		     this.saveErrors(request, errors); 
	   	   }
	       else
           {
	         errors.add( ActionErrors.GLOBAL_ERROR, new ActionError("errors.job.add.error"));
         	 this.saveErrors(request, errors);
           }
           jobForm.setJobGroup("");
		   jobForm.setJobName("");
		   jobForm.setJobClass("");
		   jobForm.setHour(null);
		   jobForm.setMinutes(null);
		   jobForm.setJobFrequency(null);
		   jobForm.setTriggerName("");
		   jobForm.setMinutesFrequency("");
	       setJobListinSession(session);
        }
        else if (action.equalsIgnoreCase("Delete Job"))
        {
	        //System.out.println("Delete Job");
	        flag = false;
	        flag = deleteJob(session, jobForm) ;
	        if(flag)
	        {
		     errors.add( ActionErrors.GLOBAL_ERROR, new ActionError("errors.job.delete.success")); 
		     this.saveErrors(request, errors); 
	   	    }
	        else
            {
	         errors.add( ActionErrors.GLOBAL_ERROR, new ActionError("errors.job.delete.error"));
         	 this.saveErrors(request, errors);
            }
            jobForm.setJobGroup("");
		    jobForm.setJobName("");
		    jobForm.setJobClass("");
		    jobForm.setHour(null);
		    jobForm.setMinutes(null);
		    jobForm.setJobFrequency(null);
		    jobForm.setTriggerName("");
			jobForm.setMinutesFrequency("");
	        setJobListinSession(session); 	  
        }
        else if (action.equalsIgnoreCase("Reschedule Job"))
        {
	        //System.out.println("Reschedule Job");
	        flag = false;
	        flag = rescheduleJob(session, jobForm) ;
	        
	        if(flag)
	        {
		     errors.add( ActionErrors.GLOBAL_ERROR, new ActionError("errors.job.reschedule.success")); 
		     this.saveErrors(request, errors); 
	   	    }
	        else
            {
	         errors.add( ActionErrors.GLOBAL_ERROR, new ActionError("errors.job.reschedule.error"));
         	 this.saveErrors(request, errors);
            }
            jobForm.setJobName("");
	        jobForm.setJobGroup("");
	        jobForm.setJobClass("");
		    jobForm.setHour(null);
		    jobForm.setMinutes(null);
		    jobForm.setJobFrequency(null);
		    jobForm.setTriggerName("");
			jobForm.setMinutesFrequency("");
	   		setJobListinSession(session);  	  
        }
      
		return (new ActionForward(mapping.getInput()));
    }   // END - perform()
    
    
    private ArrayList getJobs(HttpSession session) 
    {
      	ServletContext ctx =  session.getServletContext();
     	StdSchedulerFactory factory = (StdSchedulerFactory) ctx.getAttribute(QuartzInitializerServlet.QUARTZ_FACTORY_KEY);
     	ArrayList jobList = new ArrayList();
		JobDetail jobDetail = null;
		String lastRun = "";
		String nextRun = "";
		String cronExpression = "";
		String triggerName = "";
				  
		try
		{
			// Retrieve the scheduler from the factory
	     	Scheduler scheduler = factory.getScheduler();
	     	if (scheduler == null)
	     	{
	      		  System.out.println("Scheduler is NULL");
	              logger.debug("Scheduler is NULL"); 
	     	}
	     	
	     	//get the JobGroup names
			String[] jobGroups = scheduler.getJobGroupNames();
			if( (jobGroups != null))
			{
		  	 	int numOfJobGroups = jobGroups.length;
		
		  		for (int i = 0; i < numOfJobGroups; i++) 
		  		{
		    		//System.out.println("Group: "  + jobGroups[i] + " contains the following jobs");
		
		    		String[] jobsInGroup = scheduler.getJobNames(jobGroups[i]);
		    		int numOfJobsInGroup = jobsInGroup.length;
		
		    		//get the Job names
		    		for (int j = 0; j < numOfJobsInGroup; j++) 
		    		{
		      		  System.out.println(" - "  + jobsInGroup[j]);
		      		  //Get JobDetail to get the run time details
		      		  jobDetail = scheduler.getJobDetail(jobsInGroup[j], jobGroups[i]);
		      		  //System.out.println(" Ajan is here "  + jobDetail.getName() );
		      		  Trigger[] triggers = scheduler.getTriggersOfJob(jobsInGroup[j], jobGroups[i]);
					  
					  
					  try {
						cronExpression = ((CronTrigger) triggers[0]).getCronExpression(); 
						triggerName = ((CronTrigger) triggers[0]).getName(); 
						//System.out.println(" Ajan : " + triggerName );
					  	lastRun = ((CronTrigger) triggers[0]).getPreviousFireTime().toString();
					  	//System.out.println(cronExpression);
				  	  }catch(NullPointerException e) {lastRun = "";}
					  
					  try {
					  	nextRun = ((CronTrigger) triggers[0]).getNextFireTime().toString();
					  }catch(NullPointerException e) {nextRun = "";}
					
					  String[] cronExpressionArr = new String[6];
					  cronExpressionArr = cronExpression.split(" ");
					  String[] hours = cronExpressionArr[2].split(",");
					  String[] dayOfWeeks = cronExpressionArr[5].split(",");
					  
					  //Add to the JobVO now
		      		  JobVO VO = new JobVO();
		      		  
					  VO.setJobGroup(jobGroups[i]); 
					  VO.setJobName(jobsInGroup[j]);   
		    		  VO.setJobClass(jobDetail.getJobClass().getName()); 
					  VO.setHour(hours);
					  String minutesExpr = cronExpressionArr[1];
					  String minutes = "";
					  String minutesFrequency = "";
					  if (minutesExpr.indexOf("/") >0)
					  {
						  minutes = minutesExpr.substring(0, minutesExpr.indexOf("/"));
						  minutesFrequency = minutesExpr.substring(minutesExpr.indexOf("/"), minutesExpr.length());
					  }
					  else
					  {
						  minutes = minutesExpr;
					  }
		    		  VO.setMinutes(minutes);
		    		  VO.setJobFrequency(dayOfWeeks);	    		  
					  VO.setLastRun(lastRun);
					  VO.setNextRun(nextRun);
					  VO.setTriggerName(triggerName);
					  VO.setCronExpression(cronExpression);
					  VO.setMinutesFrequency(minutesFrequency);
					  //System.out.println("New Trigger Name:" + triggerName);
	
					  jobList.add(VO);
		    		}
		  		} 
	  		}   
  		}
  		catch(SchedulerException e)
  		{
	  		e.printStackTrace();
  		}
  		catch(NullPointerException e)
  		{
	  		e.printStackTrace();
  		}
  		
  		return jobList;
 	} //getjobs
 	
 	private boolean deleteJob(HttpSession session, JobForm jobForm) 
    {
	    ServletContext ctx =  session.getServletContext();
     	StdSchedulerFactory factory = (StdSchedulerFactory) ctx.getAttribute(QuartzInitializerServlet.QUARTZ_FACTORY_KEY);
		String jobName = jobForm.getJobName();
		String jobGroup = jobForm.getJobGroup();
		boolean deleted = false;
     	try
		{
			// Retrieve the scheduler from the factory
	     	Scheduler scheduler = factory.getScheduler();
	     	//delete the job
	     	logger.debug("Delete Job");
	     	logger.debug(jobName);
	     	logger.debug(jobGroup);
	     	deleted = scheduler.deleteJob(jobName, jobGroup); 
	    }
  		catch(SchedulerException e)
  		{
	  		e.printStackTrace();
  		} 	
  		catch (Exception e) 
		{
			logger.debug("deleteJob" + e);
			e.printStackTrace();
		}
		return deleted;
 	} //getjobs
 	
 	private boolean addJob(HttpSession session, JobForm jobForm) 
    {
	    ServletContext ctx =  session.getServletContext();
     	StdSchedulerFactory factory = (StdSchedulerFactory) ctx.getAttribute(QuartzInitializerServlet.QUARTZ_FACTORY_KEY);
		String jobName = jobForm.getJobName();
		String jobGroup = jobForm.getJobGroup();
		Class jobClass = null;
		String triggerName = jobForm.getTriggerName().trim();
		String triggerGroup = "triggerGroup";
		String cronExpr = "";
		boolean scheduled = false;
     	try
		{
			jobClass = Class.forName(jobForm.getJobClass().trim());
			// Retrieve the scheduler from the factory
	     	Scheduler scheduler = factory.getScheduler();
	     	//add the job
	     	logger.debug("Add Job");
	     	logger.debug(jobName);
	     	logger.debug(jobGroup);
	     	logger.debug(triggerName);
	     	logger.debug(jobClass);
	     	
	     	cronExpr = retrieveTriggerExpr(jobForm);
	     	logger.debug(cronExpr);
	     	
	     	JobDetail jobDetail = new JobDetail(jobName, jobGroup, jobClass);
			CronTrigger cronTrigger = new CronTrigger(triggerName, triggerGroup);
			cronTrigger.setCronExpression(cronExpr);
			scheduler.scheduleJob(jobDetail, cronTrigger);
			scheduled = true;
	    }
  		catch(SchedulerException e)
  		{
	  		e.printStackTrace();
  		} 	
  		catch (Exception e) 
		{
			logger.debug("addJob" + e);
			e.printStackTrace();
		}
		return scheduled;
 	} //addJob
 	
 	private boolean rescheduleJob(HttpSession session, JobForm jobForm) 
    {
	    ServletContext ctx =  session.getServletContext();
     	StdSchedulerFactory factory = (StdSchedulerFactory) ctx.getAttribute(QuartzInitializerServlet.QUARTZ_FACTORY_KEY);
		String jobName = jobForm.getJobName();
		String jobGroup = jobForm.getJobGroup();
		String triggerName = jobForm.getTriggerName().trim();
		String triggerGroup = "triggerGroup";		
		String cronExpr = "";
		boolean rescheduled = false;
     	try
		{
			cronExpr = retrieveTriggerExpr(jobForm);
			logger.debug("Reschedule Job");
	     	logger.debug(jobName);
	     	logger.debug(jobGroup);
	     	logger.debug(triggerName);
			logger.debug(cronExpr);
			// Retrieve the scheduler from the factory
			Scheduler scheduler = factory.getScheduler();
	     	//reschedule existing job
			CronTrigger cronTrigger = new CronTrigger(triggerName, triggerGroup, jobName, jobGroup); 
			cronTrigger.setCronExpression(cronExpr);
 			scheduler.rescheduleJob(triggerName, triggerGroup, cronTrigger);
 			rescheduled = true;
	    }
  		catch(SchedulerException e)
  		{
	  		e.printStackTrace();
  		} 	
  		catch (Exception e) 
		{
			logger.debug("rescheduleJob" + e);
			e.printStackTrace();
		}
		return rescheduled;
 	} //rescheduleJob
 	
 	private String retrieveTriggerExpr(JobForm jobForm)
 	{
	 	String cronExpr = "";
 		String second = "0 ";
		String minute =  jobForm.getMinutes().trim();
		String minuteFreq =  jobForm.getMinutesFrequency().trim();
		String[] hours =  jobForm.getHour();
		String hour = "";
		for(int i = 0; i < hours.length; i++)
		{
			if(i > 0) 
			{ 
				hour = hour + "," ;
		    }
			hour = hour + hours[i];	
    	}
    	//System.out.println(hour);	    	
		String[] daysOfWeeks = jobForm.getJobFrequency();
		String dayOfWeek = "";

		if(daysOfWeeks.length > 1)
		{
			for(int i = 0; i < daysOfWeeks.length; i++)
			{
				if(daysOfWeeks[i].equals("EVE"))
				{
					dayOfWeek = "*";
					break;
			 	}
				if(i > 0) 
				{ 
					 dayOfWeek = dayOfWeek + "," ;
				}
				dayOfWeek = dayOfWeek + daysOfWeeks[i];					
	    	}
    	}
    	else if(daysOfWeeks.length == 1)
		{
			dayOfWeek = daysOfWeeks[0];
			if(dayOfWeek.equals("EVE"))
			{
				dayOfWeek = "*";
		 	}
		}
	
		String dayOfMonth = "? ";
		String month = "* ";
		
		if (minuteFreq != null && !"".equals(minuteFreq))
		{
			minute = minute + "/" + minuteFreq;
		}
		
		cronExpr = second + minute + " " + hour + " " + dayOfMonth + month + dayOfWeek; 
		
		return cronExpr;
	}
	
	public void setJobListinSession(HttpSession session)
	{
		ArrayList jobs = getJobs(session);
		session.removeAttribute("session_jobList");
    	if(jobs == null || jobs.size() == 0) {
	    	session.setAttribute("session_jobList", jobs);
    	} else {
    		session.setAttribute("session_jobList", jobs);	 
		}   
	}	  
}  
