package com.esmart2u.oeight.admin.scheduler.struts;

import java.io.Serializable;


public class JobVO implements java.io.Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String jobGroup;
    private String jobName;
    private String jobClass;
    private String[] hour;
    private String minutes;
    private String[] jobFrequency;
    private String lastRun;
    private String nextRun;
    private String triggerName;
    private String cronExpression;
	private String minutesFrequency;
	
    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobClass() {
        return jobClass;
    }

    public void setJobClass(String jobClass) {
        this.jobClass = jobClass;
    }

    public String[] getHour() {
        return hour;
    }

    public void setHour(String[] hour) {
        this.hour = hour;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String[] getJobFrequency() {
        return jobFrequency;
    }

    public void setJobFrequency(String[] jobFrequency) {
        this.jobFrequency = jobFrequency;
    }

    public String getLastRun() {
        return lastRun;
    }

    public void setLastRun(String lastRun) {
        this.lastRun = lastRun;
    }

    public String getNextRun() {
        return nextRun;
    }

    public void setNextRun(String nextRun) {
        this.nextRun = nextRun;
    }
    
    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }
       
    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

	public String getMinutesFrequency() {
		return minutesFrequency;
	}

	public void setMinutesFrequency(String minutesFrequency) {
		this.minutesFrequency = minutesFrequency;
	}


}
