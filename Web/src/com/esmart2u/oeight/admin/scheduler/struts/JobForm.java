package com.esmart2u.oeight.admin.scheduler.struts;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
 

public final class JobForm extends ActionForm 
{
    private String jobGroup;
    private String jobName;
    private String jobClass;
    private String[] hour;
    private String minutes;
    private String[] jobFrequency;
    private String triggerName;
	private String action;
	private String cronExpression;
	private String minutesFrequency;
	
	
    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobClass() {
        return jobClass;
    }

    public void setJobClass(String jobClass) {
        this.jobClass = jobClass;
    }

    public String[] getHour() {
        return hour;
    }

    public void setHour(String[] hour) {
        this.hour = hour;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String[] getJobFrequency() {
        return jobFrequency;
    }

    public void setJobFrequency(String[] jobFrequency) {
        this.jobFrequency = jobFrequency;
    }
    
    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public String getAction() {
	    return action;
    }

    public void setAction(String action) {
		this.action = action;
    }

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getMinutesFrequency() {
		return minutesFrequency;
	}

	public void setMinutesFrequency(String minutesFrequency) {
		this.minutesFrequency = minutesFrequency;
	}

}
