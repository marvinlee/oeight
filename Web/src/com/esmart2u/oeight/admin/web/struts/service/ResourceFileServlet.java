/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ResourceFileServlet.java

package com.esmart2u.oeight.admin.web.struts.service;

import java.io.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import com.esmart2u.solution.base.logging.Logger;

/**
 * As a web service to download Application Resource Excel
 *
 * @author  Chris Chern Hak Seng
 * @version $Revision: 1.2 $
 */

public class ResourceFileServlet extends HttpServlet
{
    private Logger logger = Logger.getLogger(ResourceFileServlet.class);

    private String instWeeklyDayFrom = "1";
    private String intWeeklyDayTo = "1";

    protected void service(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse)
        throws ServletException, IOException
    {
        httpServletResponse.setContentType("application/vnd.ms-excel");
//        httpServletResponse.setHeader("Content-Disposition","attachment;filename="+MaintenanceConfigurationManager.getKey("file.excelresource"));
        OutputStream stream = httpServletResponse.getOutputStream();

        StringBuffer uri = new StringBuffer();
//        uri.append(MaintenanceConfigurationManager.getKey("dir.excelresource"));
        uri.append("/");
//        uri.append(MaintenanceConfigurationManager.getKey("file.excelresource"));

        stream.write(getExcelFile(uri.toString()));
    }

    /**
     * Reading excel file from the URI and convert it to bytes for output
     *
     * @param uri File Path + Filename
     * @return
     */
    private byte[] getExcelFile(String uri)
    {
        // get excel file here
        File file = null;
        InputStream is = null;
        try {
            file = new File(uri);
            is = new FileInputStream(file);

            // Get the size of the file
            long length = file.length();

            // You cannot create an array using a long type.
            // It needs to be an int type.
            // Before converting to an int type, check
            // to ensure that file is not larger than Integer.MAX_VALUE.
            if (length > Integer.MAX_VALUE) {
                // File is too large
            }

            // Create the byte array to hold the data
            byte[] bytes = new byte[(int)length];

            // Read in the bytes
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length
                   && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }

            // Ensure all the bytes have been read in
            if (offset < bytes.length) {
                throw new IOException("Could not completely read file "+file.getName());
            }

            //return bytes
            return bytes;
        } catch (FileNotFoundException e) {
            return e.toString().getBytes();
        } catch (IOException e){
            return e.toString().getBytes();
        } finally {
            if (is != null)
            {
                try {
                    // Close the input stream
                    is.close();
                } catch (IOException e) {
                    return e.toString().getBytes();
                }
            }
        }
     }
}

// end of ResourceFileServlet.java