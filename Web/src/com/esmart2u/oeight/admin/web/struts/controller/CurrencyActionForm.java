/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CurrencyActionForm.java

package com.esmart2u.oeight.admin.web.struts.controller;

import java.sql.Date;
import java.util.List;

import com.esmart2u.solution.web.struts.controller.AbstractMaintenanceActionForm;

/**
 * Action Form for Currency Maintenance module.
 *
 * @author  Daniel Ang
 * @version $Revision: 1.10 $
 */

public class CurrencyActionForm extends AbstractMaintenanceActionForm
{
    private String code;
    private String description;
    private String alternateDescription;
    private String buyRate;
    private String sellRate;
    private String otherFlag;
    private String deactivatedFlag;
    private String systemDataFlag;
    private String softDeletedFlag;
    private String defaultFlag;
    private String maintenanceStatusCode;
    private String maintenanceStatusDescription;
    private String createdBy;
    private String updatedBy;
    private Date createdDate;
    private Date updatedDate;
    private int currentRecordVersion;

    private List currencyList;
    private String formattedBuyRate;
    private String formattedSellRate;
    private String formattedCreatedDate;
    private String formattedUpdatedDate;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAlternateDescription()
    {
        return alternateDescription;
    }

    public void setAlternateDescription(String alternateDescription)
    {
        this.alternateDescription = alternateDescription;
    }

    public String getBuyRate()
    {
        return buyRate;
    }

    public void setBuyRate(String buyRate)
    {
        this.buyRate = buyRate;
    }

    public String getSellRate()
    {
        return sellRate;
    }

    public void setSellRate(String sellRate)
    {
        this.sellRate = sellRate;
    }

    public String getOtherFlag()
    {
        return otherFlag;
    }

    public void setOtherFlag(String otherFlag)
    {
        this.otherFlag = otherFlag;
    }

    public String getDeactivatedFlag()
    {
        return deactivatedFlag;
    }

    public void setDeactivatedFlag(String deactivatedFlag)
    {
        this.deactivatedFlag = deactivatedFlag;
    }

    public String getSystemDataFlag()
    {
        return systemDataFlag;
    }

    public void setSystemDataFlag(String systemDataFlag)
    {
        this.systemDataFlag = systemDataFlag;
    }

    public String getSoftDeletedFlag()
    {
        return softDeletedFlag;
    }

    public void setSoftDeletedFlag(String softDeletedFlag)
    {
        this.softDeletedFlag = softDeletedFlag;
    }

    public String getDefaultFlag()
    {
        return defaultFlag;
    }

    public void setDefaultFlag(String defaultFlag)
    {
        this.defaultFlag = defaultFlag;
    }

    public String getMaintenanceStatusCode()
    {
        return maintenanceStatusCode;
    }

    public void setMaintenanceStatusCode(String maintenanceStatusCode)
    {
        this.maintenanceStatusCode = maintenanceStatusCode;
    }

    public String getMaintenanceStatusDescription()
    {
        return maintenanceStatusDescription;
    }

    public void setMaintenanceStatusDescription(String maintenanceStatusDescription)
    {
        this.maintenanceStatusDescription = maintenanceStatusDescription;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public int getCurrentRecordVersion()
    {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion)
    {
        this.currentRecordVersion = currentRecordVersion;
    }

    public List getCurrencyList()
    {
        return currencyList;
    }

    public void setCurrencyList(List currencyList)
    {
        this.currencyList = currencyList;
    }

    public String getFormattedBuyRate()
    {
        return formattedBuyRate;
    }

    public void setFormattedBuyRate(String formattedBuyRate)
    {
        this.formattedBuyRate = formattedBuyRate;
    }

    public String getFormattedSellRate()
    {
        return formattedSellRate;
    }

    public void setFormattedSellRate(String formattedSellRate)
    {
        this.formattedSellRate = formattedSellRate;
    }

    public String getFormattedCreatedDate()
    {
        return formattedCreatedDate;
    }

    public void setFormattedCreatedDate(String formattedCreatedDate)
    {
        this.formattedCreatedDate = formattedCreatedDate;
    }

    public String getFormattedUpdatedDate()
    {
        return formattedUpdatedDate;
    }

    public void setFormattedUpdatedDate(String formattedUpdatedDate)
    {
        this.formattedUpdatedDate = formattedUpdatedDate;
    }
}

// end of CurrencyActionForm.java