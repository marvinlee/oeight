/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// TabProvider.java

package com.esmart2u.oeight.admin.web.struts.helper;

import java.util.List;
import java.util.ArrayList;

import com.esmart2u.solution.web.struts.helper.BaseTabProvider;
import org.apache.struts.action.ActionMapping;

/**
 * Class to handle tab rendering request.
 *
 * @author  Siripong Visasmongkolchai
 * @version $Revision: 1.9.2.1 $
 */

public final class TabProvider extends BaseTabProvider
{
    /**
     * Maintenance Collatearl Category Tabs.
     */
    public static final List COLLATERAL_CATEGORY_LIST = getCollateralCategoryTabList();
    public static final List VALUER_LIST = getValuerTabList();
    public static final List SOLICITOR_LIST = getSolicitorTabList();
    public static final List FACILITY_LIST = getFacilityTabList();
    public static final List REGISTRATION_PLACE_LIST = getRegistrationPlaceList();
    public static final List ATTORNEY_LIST = getAttorneyList();
    public static final List SOLICITOR_TAB_LIST = getSolicitorTabList();
    public static final List FACILITY_PRICING_TAB_LIST = getFacilityPricingTabList();
    public static final List LIMIT_MANAGEMENT = getLimitManagementList();
    public static final List INDUSTRY_LIST = getIndustryTabList();
    public static final List ARRANGEMENT_PURPOSE_LIST = getArrangementPurposeTabList();

    /**
     * Method for collateral category maintenance tab
     * @return List of string array
     */
    private static List getCollateralCategoryTabList()
    {
        List tabList = new ArrayList();

        tabList.add(newTabInformation(ScreenActionConstants.ADD_COLLATERAL_ACTION_CODE, "srn_mt_00007.tab.collateral",
                                                            "maintenance/collateral_category/collateral.view?do=ListRequest"));
//        tabList.add(newTabInformation(ScreenActionConstants.ADD_COLLATERAL_DESCRIPTION_ACTION_CODE, "srn_mt_00007.tab.collateralDescription",
//                                                            "maintenance/collateral_category/description.view?do=ListRequest"));
//        tabList.add(newTabInformation(ScreenActionConstants.ADD_COLLATERAL_INSTRUMENT_ACTION_CODE, "srn_mt_00007.tab.collateralInstrument",
//                                                            "maintenance/collateral_category/instrument.view?do=ListRequest"));
//        tabList.add(newTabInformation(ScreenActionConstants.ADD_COLLATERAL_REPLACEMENT_PLACE_ACTION_CODE, "srn_mt_00007.tab.collateralRegistrationPlace",
//                                                            "maintenance/collateral_category/replacement_place.view?do=ListRequest"));

        return tabList;
    }

    /**
     * Method for valuer maintenance tab
     * @return List of string array
     */
    private static List getValuerTabList()
    {
        List tabList = new ArrayList();

        tabList.add(newTabInformation(ScreenActionConstants.ADD_VALUER_SYSTEM_ACCESS_ACTION_CODE, "srn_mt_00015.tab.systemAccess",
                                                           "maintenance/valuer/security_access.view?do=ListRequest"));

        return tabList;
    }

    /**
     * Method for solicitor maintenance tab
     * @return List of string array
     */
    private static List getSolicitorTabList()
    {
        List tabList = new ArrayList();

        tabList.add(newTabInformation(ScreenActionConstants.ADD_SOLICITOR_SYSTEM_ACCESS_ACTION_CODE, "srn_mt_00064.tab.systemAccess",
                                                           "maintenance/solicitor/security_access.view?do=ListRequest"));
        return tabList;
    }

    /**
     * Method for facility maintenance tab
     * @return List of string array
     */
    private static List getFacilityTabList()
    {
        List tabList = new ArrayList();

        tabList.add(newTabInformation(ScreenActionConstants.ADD_FACILITY_QUESTIONNAIRE_ACTION_CODE, "srn_mt_00011.tab.questionnaire",
                                                           "maintenance/facility/questionnaire.view?do=ListRequest"));
        tabList.add(newTabInformation(ScreenActionConstants.ADD_FACILITY_COLLATERAL_ACTION_CODE, "srn_mt_00011.tab.collateral",
                                                           "maintenance/facility/collateral.view?do=ListRequest"));
        return tabList;
    }

    private static List getRegistrationPlaceList()
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.ADD_REGISTRATION_PLACE_SCHEDULE_CODE, "srn_mt_00115.tab.schedule",
                                                            "maintenance/registration_place/schedule.view?do=ListRequest"));

        return tabList;
    }

    private static List getAttorneyList()
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.ADD_REGISTRATION_PLACE_ATTORNEY_CODE, "srn_mt_00028.tab.registrationPlace",
                                                            "maintenance/attorney/registration_place.view?do=ListRequest"));

        return tabList;
    }

    private static List getFacilityPricingTabList()
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.ADD_FACILITY_PRICING_PACKAGE_CODE, "srn_mt_00028.tab.registrationPlace",
                                                            "maintenance/facility_pricing/facility_pricing_package.view?do=ListRequest"));
        tabList.add(newTabInformation(ScreenActionConstants.ADD_FACILITY_PRICING_RATE_CODE, "srn_mt_00028.tab.registrationPlace",
                                                            "/maintenance/facility_pricing/facility_pricing_rate.view?do=ListRequest"));
        tabList.add(newTabInformation(ScreenActionConstants.ADD_FACILITY_PRICING_TIER_CODE, "srn_mt_00028.tab.registrationPlace",
                                                            "/maintenance/facility_pricing/facility_pricing_tier.view?do=ListRequest"));

        return tabList;
    }

    private static List getLimitManagementList()
    {
        List tabList = new ArrayList();
        //tabList.add(newTabInformation(ScreenActionConstants.ADD_LIMIT_FACILITY_CODE, "srn_mt_00138.tab.limitFacility",
        //                                                    "maintenance/limit_managemen/limit_facility.view?do=SearchRequest"));
        //tabList.add(newTabInformation(ScreenActionConstants.ADD_LIMIT_FACILITY_CODE, "srn_mt_00138.tab.limitFacility",
        //                                                    "maintenance/limit_managemen/limit_facility.view?do=SearchRequest"));
        tabList.add(newTabInformation(ScreenActionConstants.ADD_LIMIT_FACILITY_CODE, "srn_mt_00138.tab.limitFacility",
                                                            "maintenance/limit_management/limit_facility.view?do=SearchRequest"));
        return tabList;
    }

    /**
     * Get Financial Format Version Data Entry tab list.
     * @return List of string array
     */
    public static List getFinancialFormatVersionDataEntryTabList(ActionMapping actionMapping)
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_GENERAL,
                                      "srn_mt_00150.tab.general",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_summary_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_BALANCE_SHEET,
                                      "srn_mt_00150.tab.balanceSheet",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_balance_sheet_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_PROFIT_LOSS,
                                      "srn_mt_00150.tab.profitLoss",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_profit_loss_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_CASHFLOW,
                                      "srn_mt_00150.tab.cashFlow",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_cash_flow_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_RATIO,
                                      "srn_mt_00150.tab.ratio",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_ratio_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_FINANCIAL_DATA,
                                      "srn_mt_00150.tab.financialData",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_financial_data_data_entry")));
        return tabList;
    }

    /**
     * Get Financial Format Version tab list.
     * @return List of string array
     */
    public static List getFinancialFormatVersionTabList(ActionMapping actionMapping)
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_GENERAL,
                                      "srn_mt_00150.tab.general",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_summary")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_BALANCE_SHEET,
                                      "srn_mt_00150.tab.balanceSheet",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_balance_sheet")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_PROFIT_LOSS,
                                      "srn_mt_00150.tab.profitLoss",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_profit_loss")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_CASHFLOW,
                                      "srn_mt_00150.tab.cashFlow",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_cash_flow")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_RATIO,
                                      "srn_mt_00150.tab.ratio",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_ratio")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_FINANCIAL_DATA,
                                      "srn_mt_00150.tab.financialData",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_financial_data")));
        return tabList;
    }

    /**
     * Get Financial Format Version Data Entry tab list.
     * @return List of string array
     */
    public static List getFinancialFormatVersionDataEditTabList(ActionMapping actionMapping)
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_GENERAL,
                                      "srn_mt_00150.tab.general",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_summary_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_BALANCE_SHEET,
                                      "srn_mt_00150.tab.balanceSheet",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_balance_sheet_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_PROFIT_LOSS,
                                      "srn_mt_00150.tab.profitLoss",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_profit_loss_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_CASHFLOW,
                                      "srn_mt_00150.tab.cashFlow",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_cash_flow_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_RATIO,
                                      "srn_mt_00150.tab.ratio",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_ratio_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_SPREADSHEET_FORMAT_FINANCIAL_DATA,
                                      "srn_mt_00150.tab.financialData",
                                      BaseTabProvider.getPath(actionMapping, "financial_format_version_financial_data_edit_data_entry")));
        return tabList;
    }

    /**
     * Get Financial Format Version Data Entry tab list.
     * @return List of string array
     */
    public static List getFinancialIndustryBenchmarkVersionDataEntryTabList(ActionMapping actionMapping)
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_GENERAL,
                                      "srn_mt_00168.tab.general",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_BALANCE_SHEET,
                                      "srn_mt_00168.tab.balanceSheet",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_balance_sheet_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_PROFIT_LOSS,
                                      "srn_mt_00168.tab.profitLoss",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_profit_loss_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_CASHFLOW,
                                      "srn_mt_00168.tab.cashFlow",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_cash_flow_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_RATIO,
                                      "srn_mt_00168.tab.ratio",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_ratio_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_FINANCIAL_DATA,
                                      "srn_mt_00168.tab.financialData",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_financial_data_data_entry")));
        return tabList;
    }

    /**
     * Get Financial Format Version tab list.
     * @return List of string array
     */
    public static List getFinancialIndustryBenchmarkVersionTabList(ActionMapping actionMapping)
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_GENERAL,
                                      "srn_mt_00168.tab.general",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_summary")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_BALANCE_SHEET,
                                      "srn_mt_00168.tab.balanceSheet",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_balance_sheet")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_PROFIT_LOSS,
                                      "srn_mt_00168.tab.profitLoss",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_profit_loss")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_CASHFLOW,
                                      "srn_mt_00168.tab.cashFlow",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_cash_flow")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_RATIO,
                                      "srn_mt_00168.tab.ratio",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_ratio")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_FINANCIAL_DATA,
                                      "srn_mt_00168.tab.financialData",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_financial_data")));
        return tabList;
    }

    /**
     * Get Financial Format Version Data Entry tab list.
     * @return List of string array
     */
    public static List getFinancialIndustryBenchmarkVersionDataEditTabList(ActionMapping actionMapping)
    {
        List tabList = new ArrayList();
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_GENERAL,
                                      "srn_mt_00168.tab.general",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_summary_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_BALANCE_SHEET,
                                      "srn_mt_00168.tab.balanceSheet",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_balance_sheet_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_PROFIT_LOSS,
                                      "srn_mt_00168.tab.profitLoss",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_profit_loss_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_CASHFLOW,
                                      "srn_mt_00168.tab.cashFlow",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_cash_flow_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_RATIO,
                                      "srn_mt_00168.tab.ratio",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_ratio_edit_data_entry")));
        tabList.add(newTabInformation(ScreenActionConstants.FINANCIAL_INDUSTRY_BENCHMARK_FINANCIAL_DATA,
                                      "srn_mt_00168.tab.financialData",
                                      BaseTabProvider.getPath(actionMapping, "financial_industry_benchmark_version_financial_data_edit_data_entry")));
        return tabList;
    }

	private static List getIndustryTabList()
	{
		List tabList = new ArrayList();

		tabList.add(newTabInformation(ScreenActionConstants.ADD_INDUSTRY_QUESTIONNAIRE_ACTION_CODE, "srn_mt_00085.tab.questionnaire",
														   "/maintenance/industry/industry_questionnaire.view?do=ListRequest"));

		return tabList;
    }

    /**
     * Method for collateral maintenance tab
     * @return List of string array
     */
    public static List getCollateralTabList(ActionMapping actionMapping)
    {
        List tabList = new ArrayList();

        tabList.add(newTabInformation(ScreenActionConstants.ADD_COLLATERAL_SAFETY_FACTOR_CLASSIFICATION_ACTION_CODE, "srn_mt_00163.tab.safetyFactorClassification",
                                      BaseTabProvider.getPath(actionMapping, "collateral_safety_factor_classification_listing")));

        return tabList;
    }

      /**
     * Method for facility maintenance tab
     * @return List of string array
     */
    private static List getArrangementPurposeTabList()
    {
        List tabList = new ArrayList();

        tabList.add(newTabInformation(ScreenActionConstants.ADD_ARRANGEMENT_PURPOSE_FACILITY_TYPE_ACTION_CODE, "srn_mt_00196.tab.facility_type",
                                                           "maintenance/arrangement_purpose/facility_type.view?do=ListRequest"));
        tabList.add(newTabInformation(ScreenActionConstants.ADD_ARRANGEMENT_PURPOSE_PERSONAL_CONSUMPTION_ACTION_CODE, "srn_mt_00196.tab.personal_consumption",
                                                           "maintenance/arrangement_purpose/personal_consumption.view?do=ListRequest"));
        return tabList;
    }
}

// end of TabProvider.java