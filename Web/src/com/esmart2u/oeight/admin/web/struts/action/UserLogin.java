/*
 * UserLogin.java
 *
 * Created on September 19, 2007, 11:12 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.web.struts.action;

/**
 *
 * @author meauchyuan.lee
 */
public class UserLogin {
    
    /** Creates a new instance of UserLogin */
    public UserLogin() {
    }
    
    private long id;
    private String emailAddress;
    private String secureCode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getSecureCode() {
        return secureCode;
    }

    public void setSecureCode(String secureCode) {
        this.secureCode = secureCode;
    }

 
    
}
