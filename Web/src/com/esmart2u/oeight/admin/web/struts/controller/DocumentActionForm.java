

package com.esmart2u.oeight.admin.web.struts.controller;

import java.sql.Date;
import java.util.List;

import com.esmart2u.solution.web.struts.controller.AbstractMaintenanceActionForm;
import com.esmart2u.solution.base.helper.SearchBean;
import com.esmart2u.oeight.admin.helper.searchbean.DocumentSearchBean;

/**
 * Action Form for Document Maintenance module.
 *
 * @author  Ko Jun Jie
 * @version $Revision: 1.8 $
 */

public class DocumentActionForm extends AbstractMaintenanceActionForm
{
    private String code;
    private String description;
    private String alternateDescription;
    private String documentCategoryCode;
    private String documentCategoryDescription;
    private String involvedSolicitorFlag;
    private String toSafeCustodyFlag;
    private String securityDocumentFlag;
    private String verificationRequireFlag;
    private String imageRequireFlag;
    private String executionRequireFlag;
    private String presentRequireFlag;
    private String stampingRequireFlag;
    private String otherFlag;
    private String deactivatedFlag;
    private String softDeletedFlag;
    private String systemDataFlag;
    private String maintenanceStatusCode;
    private String maintenanceStatusDescription;
    private String createdBy;
    private String updatedBy;
    private Date createdDate;
    private Date updatedDate;
    private int currentRecordVersion;

    private List documentList;


    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAlternateDescription()
    {
        return alternateDescription;
    }

    public void setAlternateDescription(String alternateDescription)
    {
        this.alternateDescription = alternateDescription;
    }

    public String getDocumentCategoryCode()
    {
        return documentCategoryCode;
    }

    public void setDocumentCategoryCode(String documentCategoryCode)
    {
        this.documentCategoryCode = documentCategoryCode;
    }

    public String getDocumentCategoryDescription()
    {
        return documentCategoryDescription;
    }

    public void setDocumentCategoryDescription(String documentCategoryDescription)
    {
        this.documentCategoryDescription = documentCategoryDescription;
    }

    public String getInvolvedSolicitorFlag()
    {
        return involvedSolicitorFlag;
    }

    public void setInvolvedSolicitorFlag(String involvedSolicitorFlag)
    {
        this.involvedSolicitorFlag = involvedSolicitorFlag;
    }

    public String getToSafeCustodyFlag()
    {
        return toSafeCustodyFlag;
    }

    public void setToSafeCustodyFlag(String toSafeCustodyFlag)
    {
        this.toSafeCustodyFlag = toSafeCustodyFlag;
    }

    public String getSecurityDocumentFlag()
    {
        return securityDocumentFlag;
    }

    public void setSecurityDocumentFlag(String securityDocumentFlag)
    {
        this.securityDocumentFlag = securityDocumentFlag;
    }

    public String getVerificationRequireFlag()
    {
        return verificationRequireFlag;
    }

    public void setVerificationRequireFlag(String verificationRequireFlag)
    {
        this.verificationRequireFlag = verificationRequireFlag;
    }

    public String getImageRequireFlag()
    {
        return imageRequireFlag;
    }

    public void setImageRequireFlag(String imageRequireFlag)
    {
        this.imageRequireFlag = imageRequireFlag;
    }

    public String getExecutionRequireFlag()
    {
        return executionRequireFlag;
    }

    public void setExecutionRequireFlag(String executionRequireFlag)
    {
        this.executionRequireFlag = executionRequireFlag;
    }

    public String getPresentRequireFlag()
    {
        return presentRequireFlag;
    }

    public void setPresentRequireFlag(String presentRequireFlag)
    {
        this.presentRequireFlag = presentRequireFlag;
    }

    public String getStampingRequireFlag()
    {
        return stampingRequireFlag;
    }

    public void setStampingRequireFlag(String stampingRequireFlag)
    {
        this.stampingRequireFlag = stampingRequireFlag;
    }

    public String getOtherFlag()
    {
        return otherFlag;
    }

    public void setOtherFlag(String otherFlag)
    {
        this.otherFlag = otherFlag;
    }

    public String getDeactivatedFlag()
    {
        return deactivatedFlag;
    }

    public void setDeactivatedFlag(String deactivatedFlag)
    {
        this.deactivatedFlag = deactivatedFlag;
    }

    public String getSoftDeletedFlag()
    {
        return softDeletedFlag;
    }

    public void setSoftDeletedFlag(String softDeletedFlag)
    {
        this.softDeletedFlag = softDeletedFlag;
    }

    public String getSystemDataFlag()
    {
        return systemDataFlag;
    }

    public void setSystemDataFlag(String systemDataFlag)
    {
        this.systemDataFlag = systemDataFlag;
    }

    public String getMaintenanceStatusCode()
    {
        return maintenanceStatusCode;
    }

    public void setMaintenanceStatusCode(String maintenanceStatusCode)
    {
        this.maintenanceStatusCode = maintenanceStatusCode;
    }

    public String getMaintenanceStatusDescription()
    {
        return maintenanceStatusDescription;
    }

    public void setMaintenanceStatusDescription(String maintenanceStatusDescription)
    {
        this.maintenanceStatusDescription = maintenanceStatusDescription;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public int getCurrentRecordVersion()
    {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion)
    {
        this.currentRecordVersion = currentRecordVersion;
    }

    public List getDocumentList()
    {
        return documentList;
    }

    public void setDocumentList(List documentList)
    {
        this.documentList = documentList;
    }



     public SearchBean getSearchBean()
    {
        if (this.searchBean == null)
        {
            this.searchBean = new DocumentSearchBean();
        }
        return this.searchBean;
    }

      public String getSearchDocumentCategoryCode()
    {
        DocumentSearchBean documentSearchBean = (DocumentSearchBean)this.searchBean;
        return documentSearchBean.getDocumentCategoryCode();
    }

    public void setSearchDocumentCategoryCode(String documentCategoryCode)
    {
        DocumentSearchBean documentSearchBean = (DocumentSearchBean)this.searchBean;
        documentSearchBean.setDocumentCategoryCode(documentCategoryCode);
    }

    public String getSearchDocumentCategoryDescription()
    {
        DocumentSearchBean documentSearchBean = (DocumentSearchBean)this.searchBean;
        return documentSearchBean.getDocumentCategoryDescription();
    }

    public void setSearchDocumentCategoryDescription(String documentCategoryDescription)
    {
        DocumentSearchBean documentSearchBean = (DocumentSearchBean)this.searchBean;
        documentSearchBean.setDocumentCategoryDescription(documentCategoryDescription);
    }
}

// end of DocumentActionForm.java