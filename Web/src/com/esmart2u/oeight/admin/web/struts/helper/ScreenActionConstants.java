/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ScreenActionConstants.java

package com.esmart2u.oeight.admin.web.struts.helper;

import com.esmart2u.solution.web.struts.helper.BaseScreenActionConstants;

/**
 * Screen action code constants.
 *
 * @author  Siripong Visasmongkolchai
 * @version $Revision: 1.9.2.1 $
 */

public interface ScreenActionConstants extends BaseScreenActionConstants
{
    // general action code
    public static final String ADD_COLLATERAL_ACTION_CODE = "ADD_COLL";
    public static final String ADD_COLLATERAL_DESCRIPTION_ACTION_CODE = "ADD_DSCP";
    public static final String ADD_COLLATERAL_SAFETY_FACTOR_CLASSIFICATION_ACTION_CODE = "ADD_SAFETY_CLS";
//    public static final String ADD_COLLATERAL_INSTRUMENT_ACTION_CODE = "ADD_INSTRU";
//    public static final String ADD_COLLATERAL_REPLACEMENT_PLACE_ACTION_CODE = "ADD_PLACE";

    public static final String ADD_VALUER_CONTACT_CODE = "VAL_CONTACT";
    public static final String ADD_VALUER_RELATED_ACTION_CODE = "VAL_RELATED";
    public static final String ADD_VALUER_SYSTEM_ACCESS_ACTION_CODE = "VAL_SYSTEM_ACCESS";

    public static final String ADD_SOLICITOR_CONTACT_CODE = "SOL_CONTACT";
    public static final String ADD_SOLICITOR_RELATED_ACTION_CODE = "SOL_RELATED";
    public static final String ADD_SOLICITOR_SYSTEM_ACCESS_ACTION_CODE = "SOL_SYSTEM_ACCESS";
    public static final String ADD_SOLICITOR_BRANCH_ACTION_CODE = "SOL_BRANCH";

    public static final String ADD_FACILITY_QUESTIONNAIRE_ACTION_CODE = "FAC_QUESTIONNAIRE";
    public static final String ADD_FACILITY_COLLATERAL_ACTION_CODE = "FAC_COLLATERAL";
//    public static final String ADD_FACILITY_SCHEDULE_TYPE_ACTION_CODE = "FAC_SCHEDULE_TYPE";
//    public static final String ADD_FACILITY_AMOUNT_PURPOSE_ACTION_CODE = "FAC_AMOUNT_PURPOSE";
//    public static final String ADD_FACILITY_BILLING_CYCLE_ACTION_CODE = "FAC_BILLING_CYCLE";

    public static final String ADD_REGISTRATION_PLACE_SCHEDULE_CODE = "REG_PLC_SCHEDULE";
    public static final String ADD_REGISTRATION_PLACE_ATTORNEY_CODE = "ATTOR_REG_PLC";

    public static final String MNU_MT_00000_CONTACT = "CONTACT";
    public static final String MNU_MT_00000_BRANCH ="Branch";

    public static final String ADD_FACILITY_PRICING_PACKAGE_CODE = "FAC_PRC_PAC";
    public static final String ADD_FACILITY_PRICING_RATE_CODE = "FAC_PRC_RATE";
    public static final String ADD_FACILITY_PRICING_TIER_CODE = "FAC_PRC_TIER";

    public static final String ADD_LIMIT_FACILITY_CODE = "LMT_FAC";

    //screen action for Financial Format module
    public static final String FINANCIAL_SPREADSHEET_FORMAT_GENERAL ="FIN_FMT_VS_GENERAL";
    public static final String FINANCIAL_SPREADSHEET_FORMAT_BALANCE_SHEET ="FIN_FMT_VS_BALANCE_SHEET";
    public static final String FINANCIAL_SPREADSHEET_FORMAT_PROFIT_LOSS ="FIN_FMT_VS_PROFIT_LOSS";
    public static final String FINANCIAL_SPREADSHEET_FORMAT_CASHFLOW ="FIN_FMT_VS_CASHFLOW";
    public static final String FINANCIAL_SPREADSHEET_FORMAT_RATIO ="FIN_FMT_VS_RATIO";
    public static final String FINANCIAL_SPREADSHEET_FORMAT_FINANCIAL_DATA ="FIN_FMT_VS_FINANCIAL_DATA";

    public static final String ADD_INDUSTRY_QUESTIONNAIRE_ACTION_CODE = "IND_QUESTIONNAIRE";

    //screen action for Financial Industry Benchmark module
    public static final String FINANCIAL_INDUSTRY_BENCHMARK_GENERAL ="FIN_IND_VS_GENERAL";
    public static final String FINANCIAL_INDUSTRY_BENCHMARK_BALANCE_SHEET ="FIN_IND_VS_BALANCE_SHEET";
    public static final String FINANCIAL_INDUSTRY_BENCHMARK_PROFIT_LOSS ="FIN_IND_VS_PROFIT_LOSS";
    public static final String FINANCIAL_INDUSTRY_BENCHMARK_CASHFLOW ="FIN_IND_VS_CASHFLOW";
    public static final String FINANCIAL_INDUSTRY_BENCHMARK_RATIO ="FIN_IND_VS_RATIO";
    public static final String FINANCIAL_INDUSTRY_BENCHMARK_FINANCIAL_DATA ="FIN_IND_VS_FINANCIAL_DATA";

    public static final String ADD_FINANCIAL_BENCHMARK_VERSION ="ADD_FIN_BENCHMRK_VS";
    public static final String FINANCIAL_BENCHMARK_VERSION_LINK ="FIN_BENCHMRK_VS_LINK";
    public static final String ADD_ARRANGEMENT_PURPOSE_FACILITY_TYPE_ACTION_CODE = "ARGNT_PUR_FAC_TYPE";
    public static final String ADD_ARRANGEMENT_PURPOSE_PERSONAL_CONSUMPTION_ACTION_CODE = "ARGNT_PUR_PERSONAL_CONSUMPTN";
}

// end of ScreenActionConstants.java