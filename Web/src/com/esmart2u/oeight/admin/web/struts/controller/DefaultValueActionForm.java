/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DefaultValueActionForm.java

package com.esmart2u.oeight.admin.web.struts.controller;

import java.util.List;

import com.esmart2u.solution.web.struts.controller.AbstractMaintenanceActionForm;

/**
 * Validation Rule maintenance module action form.
 *
 * @author  Chee Weng Keong
 * @version $Revision: 1.10 $
 */

public class DefaultValueActionForm extends AbstractMaintenanceActionForm
{
    private String tableName;
    private String code;
    private String tableDescription;
    private String tableDescriptionResourceCode;
//    private int codeDataLength;
//    private String codeDataType;
//    private int descriptionDataLength;
//    private String searchCode;
//    private String searchTableName;
//    private String searchLabel;
//    private String searchLabelResourceCode;
    private String defaultRequireFlag;
//    private String defaultSearchRequireFlag;
//    private String paramaterFlag;
//    private String systemDataFlag;
//    private String mappingToHostFlag;
//    private String maintenanceStatusCode;
//    private String maintenanceStatusDescription;
//    private String createdBy;
//    private String updatedBy;
//    private Date createdDate;
//    private Date updatedDate;
//    private int currentRecordVersion;

//    private List validationRuleList;
//    private List dataTypeList;
    private String defaultCode;
    private String defaultDescription;

    private List defaultValueList;
    private boolean canUseCommonSpAliasSelectPickListFlag;
    private boolean tableHasColumnIsDefaultFlag;

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTableDescription()
    {
        return tableDescription;
    }

    public void setTableDescription(String tableDescription)
    {
        this.tableDescription = tableDescription;
    }

    public String getTableDescriptionResourceCode()
    {
        return tableDescriptionResourceCode;
    }

    public void setTableDescriptionResourceCode(String tableDescriptionResourceCode)
    {
        this.tableDescriptionResourceCode = tableDescriptionResourceCode;
    }

    public String getDefaultRequireFlag()
    {
        return defaultRequireFlag;
    }

    public void setDefaultRequireFlag(String defaultRequireFlag)
    {
        this.defaultRequireFlag = defaultRequireFlag;
    }

    public String getDefaultCode()
    {
        return defaultCode;
    }

    public void setDefaultCode(String defaultCode)
    {
        this.defaultCode = defaultCode;
    }

    public String getDefaultDescription()
    {
        return defaultDescription;
    }

    public void setDefaultDescription(String defaultDescription)
    {
        this.defaultDescription = defaultDescription;
    }

    public List getDefaultValueList()
    {
        return defaultValueList;
    }

    public void setDefaultValueList(List defaultValueList)
    {
        this.defaultValueList = defaultValueList;
    }

    public boolean isCanUseCommonSpAliasSelectPickListFlag()
    {
        return canUseCommonSpAliasSelectPickListFlag;
    }

    public void setCanUseCommonSpAliasSelectPickListFlag(boolean canUseCommonSpAliasSelectPickListFlag)
    {
        this.canUseCommonSpAliasSelectPickListFlag = canUseCommonSpAliasSelectPickListFlag;
    }

    public boolean isTableHasColumnIsDefaultFlag()
    {
        return tableHasColumnIsDefaultFlag;
    }

    public void setTableHasColumnIsDefaultFlag(boolean tableHasColumnIsDefaultFlag)
    {
        this.tableHasColumnIsDefaultFlag = tableHasColumnIsDefaultFlag;
    }
}

// end of DefaultValueActionForm.java