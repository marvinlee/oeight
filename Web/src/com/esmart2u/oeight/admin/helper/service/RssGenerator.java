/*
 * RssGenerator.java
 *
 * Created on November 2, 2008, 12:52 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.service;

import com.esmart2u.oeight.data.ForumTopic;
import com.esmart2u.oeight.member.bo.ForumBO;
import com.esmart2u.oeight.member.bo.ForumCacheBO;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.sun.syndication.feed.synd.*;
import com.sun.syndication.io.SyndFeedOutput;

import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class RssGenerator {
    
    /** Creates a new instance of RssGenerator */
    public RssGenerator() {
    }
    
    public static boolean generateRss()
    { 
        // Get list from DB  
        List forumTopicList = ForumCacheBO.getForumsTopicForRss();

        // Write the XML
         try {
                String feedType = PropertyManager.getValue(PropertyConstants.RSS_MAIN_FEED_TYPE); //args[0];
                String fileName = PropertyManager.getValue(PropertyConstants.RSS_MAIN_FEED_FILE);//args[1];

                SyndFeed feed = new SyndFeedImpl();
                feed.setFeedType(feedType);

                feed.setTitle(PropertyManager.getValue(PropertyConstants.RSS_MAIN_FEED_TITLE));
                feed.setLink(PropertyManager.getValue(PropertyConstants.RSS_MAIN_FEED_LINK));
                feed.setDescription(PropertyManager.getValue(PropertyConstants.RSS_MAIN_FEED_DESC));

                List entries = new ArrayList();
                SyndEntry entry;
                SyndContent description;

                if (!forumTopicList.isEmpty())
                {
                    for(int i=0; i<forumTopicList.size();i++)
                    { 
                        ForumTopic topic = (ForumTopic)forumTopicList.get(i); 

                        entry = new SyndEntryImpl();
                        entry.setTitle(topic.getTopicTitle());
                        entry.setLink(topic.getTopicUrl());
                        entry.setPublishedDate(topic.getTopicTime());
                        entry.setAuthor(topic.getTopicPosterName());
                        description = new SyndContentImpl();
                        description.setType("text/html");
                        description.setValue(topic.getTopicPost());
                        entry.setDescription(description);
                        entries.add(entry); 
                    }
 

                    feed.setEntries(entries);

                    Writer writer = new FileWriter(fileName);
                    SyndFeedOutput output = new SyndFeedOutput();
                    output.output(feed,writer);
                    writer.close();

                    System.out.println("The feed has been written to the file ["+fileName+"]");
                } 
            }
            catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("ERROR: "+ex.getMessage());
                return false;
            }
       return true; 
    }
    
}
