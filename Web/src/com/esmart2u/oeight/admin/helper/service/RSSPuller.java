/*
 * RSSPuller.java
 *
 * Created on September 21, 2009, 12:09 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.service;
  
import com.esmart2u.oeight.data.UserPost;
import com.sun.syndication.feed.synd.*;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.net.URL;

import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class RSSPuller {

    private static Logger logger = Logger.getLogger(RSSPuller.class);
    
    /** Creates a new instance of RSSPuller */
    public RSSPuller() {
    }
    

    public ArrayList getRssFeedEntries(URL url)
    {
        int entriesCount = 0;
        SyndFeed feed = null;
        ArrayList feedList = new ArrayList();
        try {
            feed = new SyndFeedInput().build(new XmlReader(url));
                    
            System.out.println("Feed Title" + feed.getTitle());
            System.out.println("Feed Desc" + feed.getDescription()); 
            System.out.println("Feed Link" + feed.getLink()); 
            System.out.println("Feed Size" + feed.getEntries().size()); 
            
            // Iterate through feed items, adding a footer each item 
            Iterator entryIter = feed.getEntries().iterator();
            while (entryIter.hasNext() && entriesCount < 10)
            {
                entriesCount +=1; 
                SyndEntry entry = (SyndEntry) entryIter.next(); 
                String noHTMLString = "";
                if (entry.getDescription() != null)
                {
                    noHTMLString= entry.getDescription().getValue().replaceAll("\\<.*?\\>", ""); 
                    if (noHTMLString.length() > 200)
                    {
                    	noHTMLString = noHTMLString.substring(0,196) + "...";
                    }
                }
                UserPost userPost = new UserPost();
                userPost.setPostTitle(entry.getTitle());
                userPost.setPostUrl(entry.getLink());
                userPost.setPostExcerpt(noHTMLString);
                feedList.add(userPost);
                logger.debug("Got feed: " + userPost.getPostTitle() + ":" + userPost.getPostUrl());
                logger.info("Desc: " + userPost.getPostExcerpt()); 
                System.out.println("Got feed: " + userPost.getPostTitle() + ":" + userPost.getPostUrl());
                System.out.println("Desc: " + userPost.getPostExcerpt()); 
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (FeedException ex) {
            System.out.println("Error getting your feed from " + url);
        }

        return  feedList; 
    
    }
    
    public static int getRssFeed(URL url)
    {
        int entriesCount = 0;
        SyndFeed feed = null;
        try {
            feed = new SyndFeedInput().build(new XmlReader(url));
                    
            //System.out.println("Feed Title" + feed.getTitle());
            //System.out.println("Feed Desc" + feed.getDescription()); 
            //System.out.println("Feed Link" + feed.getLink());
            System.out.println("+++++++++++++++++++++++ " );
            
            // Iterate through feed items, adding a footer each item 
            Iterator entryIter = feed.getEntries().iterator();
            while (entryIter.hasNext() && entriesCount < 10)
            {
                entriesCount +=1;
                System.out.println(entriesCount + ") =======================");
                SyndEntry entry = (SyndEntry) entryIter.next(); 
                //System.out.println("entry Title" + entry.getTitle());
                String noHTMLString = "";
                if (entry.getDescription() != null)
                {
                    noHTMLString= entry.getDescription().getValue().replaceAll("\\<.*?\\>", ""); 
                }
                //System.out.println("entry Desc" + noHTMLString); 
                //System.out.println("entry Link" + entry.getLink());
                //System.out.println("entry Date" + entry.getPublishedDate());
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (FeedException ex) {
            System.out.println("Error getting your feed");
        }

        return  entriesCount; 
    
    }
    
    public static void main(String args[]){
        try {
        	/*
            System.out.println(" >> Marvin lee's blog: " + getRssFeed( new URL("http://marvinlee.net/blog/feed/")));       
            System.out.println(" >> Kenny Sia's blog: " + getRssFeed( new URL("http://www.kennysia.com/atom.xml")));
            System.out.println(" >> Kyspeaks's blog: " + getRssFeed( new URL("http://feeds.feedburner.com/kyspeaks")));
            System.out.println(" >> Archie chi's blog: " + getRssFeed( new URL("http://feeds.feedburner.com/SlylyFacetiousMind")));
            System.out.println(" >> Blogspot: " + getRssFeed(new URL("http://glamoursecret.blogspot.com/feeds/posts/default?alt=rss")));
            */
        	RSSPuller pullerInstance = new RSSPuller();
            System.out.println(" >> GAlert: " + pullerInstance.getRssFeedEntries( new URL("http://www.google.com/alerts/feeds/00141091537082778248/16787770561321566060"))); 
            //System.out.println(" >> Marvin lee's blog: " + pullerInstance.getRssFeedEntries( new URL("http://marvinlee.net/blog/feed/")));       
            //System.out.println(" >> Kenny Sia's blog: " + pullerInstance.getRssFeedEntries( new URL("http://www.kennysia.com/atom.xml")));
            //System.out.println(" >> Kyspeaks's blog: " + pullerInstance.getRssFeedEntries( new URL("http://feeds.feedburner.com/kyspeaks")));
            //System.out.println(" >> Archie chi's blog: " + pullerInstance.getRssFeedEntries( new URL("http://feeds.feedburner.com/SlylyFacetiousMind")));
            //System.out.println(" >> Blogspot: " + pullerInstance.getRssFeedEntries(new URL("http://glamoursecret.blogspot.com/feeds/posts/default?alt=rss")));
             } catch (MalformedURLException ex) {
            ex.printStackTrace();
            System.err.println("Error with URL!!");
        }
    
    }
    
}
