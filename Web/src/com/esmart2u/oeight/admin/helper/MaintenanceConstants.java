/**
 * � 2003 esmart2u Berhad.  All rights reserved.
 * MarvinLee.net
 * 
 * 
 *
 * This software is the intellectual property of esmart2u Malaysia Sdn. Bhd.
 * The program may be used only in accordance with the terms of the license
 * agreement you entered into with esmart2u Malaysia Sdn. Bhd.
 *
 * User: Benjamin
 * Computer: Afinity
 * Date: Dec 26, 2003
 * Time: 4:44:59 PM
 */

package com.esmart2u.oeight.admin.helper;

/**
 * @author Marvin Lee, esmart2u Berhad
 * @version $Revision: 1.2.16.1 $
 */

public interface MaintenanceConstants
{
    // Static values for customer type code.
    // These values must be the same as the values in the tbl_mt_cif_typ.
    public static final String CUSTOMER_TYPE_INDIVIDUAL = "I";
    public static final String CUSTOMER_TYPE_CORPORATE = "C";

    // Static values for default customer category code if no default found in the tbl_mt_cif_cat
    //below code not in use anymore by wwtham 20050316
//    public static final String CUSTOMER_CATEGORY_INDIVIDUAL = "IN";

    // Static values for default country code if no default found in the tbl_mt_ctry.
    public static final String NATIONALITY_COUNTRY_CODE = "TH";
    public static final String NATIONALITY_COUNTRY_NAME = "Thailand";

    // Static values for default country code if no default found in the tbl_mt_cur.
    public static final String NATIONALITY_CURRENCY_CODE = "THB";
    public static final String NATIONALITY_CURRENCY_NAME = "Thai Baht";

    // Static values for relationship category type.
    //These values must be the same as the values in the tbl_mt_cif_rel_cat.
    //below code not in use anymore by wwtham 20050316
//    public static final String OWNER_INDIVIDUAL ="4";
//    public static final String OWNER_CORPORATE ="5";
//    public static final String OWNERSHIP_CORPORATE ="8";
//    public static final String FAMILY ="7";
//    public static final String OWNERSHIP_INDIVIDUAL ="6";

    // Static values for customer type code.
    // These values must be the same as the values in the tbl_mt_coll_typ.
    //below code not in use anymore by wwtham 20050316
//    public static final String COLLATERAL_TYPE_PROPERTY = "PTY";
//    public static final String COLLATERAL_TYPE_DEPOSIT = "DEP";
//    public static final String COLLATERAL_TYPE_SECURITIES = "SEC";
//    public static final String COLLATERAL_TYPE_MACHINERY = "MAC";
//    public static final String COLLATERAL_TYPE_VEHICLE = "VEH";
//    public static final String COLLATERAL_TYPE_VESSEL = "VSL";
//    public static final String COLLATERAL_TYPE_BANK_GUARANTEE = "FIN";
//    public static final String COLLATERAL_TYPE_CONTRACT = "CON";
//    public static final String COLLATERAL_TYPE_SUPPORTIVE_LETTER = "LTR";
//    public static final String COLLATERAL_TYPE_INVENTORY = "INV";
//    public static final String COLLATERAL_TYPE_GUARANTEE = "GUA";
//    public static final String COLLATERAL_TYPE_OTHERS = "OTH";

    // Static values for customer relationship code.
    // These values must be the same as the values in the tbl_mt_cif_rel.

    public static final String CUSTOMER_RELATIONSHIP_WIFE = "09";
    public static final String CUSTOMER_RELATIONSHIP_HUSBAND = "10";
    public static final String CUSTOMER_RELATIONSHIP_PARENT = "11";

    // Static values for gender
    // These values must be the same as the values in the tbl_mt_gender.
    // GENDER_UNSPECIFICED commented by wwtham 20050316
    public static final String GENDER_MALE = "M";
    public static final String GENDER_FEMALE = "F";
//    public static final String GENDER_UNSPECIFICED = "U";

}
