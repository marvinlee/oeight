/*
 * OEightTileFilter.java
 *
 * Created on October 1, 2007, 12:27 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.mosaic;


import com.esmart2u.oeight.data.MosaicCurrent;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.sun.jimi.core.Jimi;
import com.sun.jimi.core.JimiReader;
import com.sun.jimi.core.filters.ImageFilterPlus;
import com.sun.jimi.core.raster.JimiRasterImage;
import java.awt.image.*;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
/**
 *
 * @author meauchyuan.lee
 */


/// An ImageFilter that replicates an image in a tiled pattern.
// <P>
// Tiles the image onto an output image of a specified size.
// The output uses the same color model as the input.
// This filter is very fast.
// <P>
// <A HREF="/resources/classes/com.sun.jimi.core.filters/Tile.java">Fetch the software.</A><BR>
// <A HREF="/resources/classes/Acme.tar.Z">Fetch the entire Acme package.</A>

public class OEightTileFilter extends ImageFilterPlus {
    
    private int width, height;
    private int newWidth, newHeight;
    private int nWide, nHigh;
    
    // Images need userid, thumbnail, large image path and caption
    private Vector images = null;
    private int totalMosaics = 1;
    private MosaicCurrent mosaicCurrent = null;
    
    private int staticCount;
    
    private HashMap jrReaderMap = new HashMap();
    
    /// Constructor.
    public OEightTileFilter( ImageProducer producer, int newWidth, int newHeight ) {
        super( producer, true );
        images = getMosaicImagesFromDB();
        //images = getAvailableImages();
        //System.out.println("Images size=" + images.size());
        this.newWidth = newWidth;
        this.newHeight = newHeight;
        
        mosaicCurrent = new MosaicCurrent();
        mosaicCurrent.setTotalMaps(1); //todo total maps is divided per 28x50
        totalMosaics = mosaicCurrent.getTotalMaps();
        //mosaicCurrent.setTotalMaps(images.size());
        mosaicCurrent.setDateRun(new Date(System.currentTimeMillis()));
    }
    
    private Vector getMosaicImagesFromDB() {
        // Images need userid, thumbnail, large image path and caption
        return MosaicDAO.getPhotosForMosaic();
    }
    
    /**
     * @deprecated Used for testing previously, so links can be left hardcoded
     *
     */
    private Vector getAvailableImages() {
        images = new Vector();
        File imageFolder = new File("C:/Work_home/thumbnail/");
        File[] imageFiles = imageFolder.listFiles();
        System.out.println("Loading Image Files!!");
        for (int i=0;i<imageFiles.length;i++) {
            String fileName = imageFiles[i].getName();
            System.out.println("=" + fileName);
            String thumbnailFile = "C:\\Work_home\\thumbnail\\" + fileName;
            String largeFile = fileName.substring(0,fileName.length()-5) + "l.jpg";
            images.add(new String[]{thumbnailFile, largeFile});
        }
        imageFiles = null;
        return images;
    }
    
    
    public void setDimensions( int width, int height ) {
        this.width = width;
        this.height = height;
        consumer.setDimensions( newWidth, newHeight );
        nWide = ( newWidth + width - 1 ) / width;
        nHigh = ( newHeight + height - 1 ) / height;
    }
    
    public void setPixels( int x, int y, int w, int h, ColorModel model, byte[] pixels, int off, int scansize ) {
        for ( int r = 0; r < nHigh; ++r ) {
            System.out.println("Current byte r="+r);
            int ty = r * height + y;
            int th = h;
            if ( ty + th > newHeight )
                th = newHeight - ty;
            for ( int c = 0; c < nWide; ++c ) {
                int tx = c * width + x;
                int tw = w;
                if ( tx + tw > newWidth )
                    tw = newWidth - tx;
                consumer.setPixels(
                        tx, ty, tw, th, model, pixels, off, scansize );
            }
        }
    }
    
    public void setPixels( int x, int y, int w, int h, ColorModel model, int[] pixels, int off, int scansize ) {
        JimiReader jr = null;
        System.out.println("How many times setPixels is called?" + staticCount++);
        int endx = 0, endy = 0;
        String thumbnailFolderString = PropertyManager.getValue(PropertyConstants.THUMBNAIL_STORE_PATH);

        for (int m=1; m<=totalMosaics;m++) {
            
            StringBuffer mapOutput = new StringBuffer();
            
            mapOutput.append("<map name=\"08map"+m+"\"> ");
            
            
            int count = 0;
            for ( int r = 0; r < nHigh; ++r ) {
                //System.out.println("Current int r="+r);
                
                int ty = r * height + y;
                int th = h;
                
                if ( ty + th > newHeight )
                    th = newHeight - ty;
                
                for ( int c = 0; c < nWide; ++c ) {
                    
                    int tx = c * width + x;
                    int tw = w;
                    
                    if ( tx + tw > newWidth )
                        tw = newWidth - tx;
                    
                    
                    // Start customization
                    int mod = ((count+2) % 10)+1;
                    
                    //HERE TO GET IMAGE FROM VECTOR
                    String[] file = new String[]{"NA","na.jpg","8.gif","Vacant, space available"};
                    String userId="", thumbnailPath = "", largeFile = "", photoDescription = "";
                    String link="", text="Profile";
                    
                    if (count < images.size()) {
                        file = (String[])images.get(count);
                    }
                    userId = file[0];
                    thumbnailPath = file[1];
                    largeFile = file[2];
                    photoDescription = file[3];
                    
                    //System.out.println("Printing image " + count);
                    //System.out.println("pixels length " + pixels.length);
                    endx = tx+38;
                    endy = ty+38;
                    if (endx % 38 ==0 && endy % 38 ==0){
                        //System.out.println("<area id=\""+userId+"\" shape=\"rect\" coords=\""+tx+","+ty+","+endx+","+endy+"\" href=\"/vphotos/"+largeFile+"\" rel=\"lightbox[r]\" title=\""+photoDescription+"<br><center><a href='http://profile.dummy.com/"+userId+"'>More..</a></center>\" >");
                        //mapOutput.append("<area id=\""+userId+"\" shape=\"rect\" coords=\""+tx+","+ty+","+endx+","+endy+"\" href=\"/vphotos/"+largeFile+"\" rel=\"lightbox[r]\" title=\""+photoDescription+"<br><center><a href='http://profile.dummy.com/"+userId+"'>More ..</a></center>\" >");
                        if ("NA".equals(userId))
                        {
                            link = "http://"+ConfigurationHelper.getDomainName();
                            userId = "Vacant";
                            text = "Home";
                        }
                        else
                        {
                            link = "http://profile."+ConfigurationHelper.getDomainName()+"/"+userId;
                        }
                         
                        System.out.println("<area id=\""+userId+"\" shape=\"rect\" coords=\""+tx+","+ty+","+endx+","+endy+"\" href=\"/vphotos/"+largeFile+"\" rel=\"lightbox[r]\" title=\"No "+(count+1)+"  " + userId+ "  <a href='"+link+"'>"+text+"</a>\" >");
                        mapOutput.append("<area id=\""+userId+"\" shape=\"rect\" coords=\""+tx+","+ty+","+endx+","+endy+"\" href=\"/vphotos/"+largeFile+"\" rel=\"lightbox[r]\" title=\"No "+(count+1)+"  " + userId+ "  <a href='"+link+"'>"+text+"</a>\" >");
                    }
                    
                    
                    
                    //   consumer.setPixels(
                    //       tx, ty, tw, th, model, pixels, off, scansize );
                    // the dimensions of the source image, which
                    // are used to create a same-sized target image
                    try{
                        //int mod = 2;
                        //System.out.println("Getting image number " + mod);
                        
                        
                        URL url = new URL("file:///"+thumbnailFolderString + thumbnailPath);
                        //URL url = new URL("file:///C:\\Work_home\\na.jpg");
                        
                        //url = new URL(getDocumentBase(), path);
                        JimiRasterImage source = null;
                        if (jrReaderMap.containsKey(url.getPath()))
                        {
                            source = (JimiRasterImage)jrReaderMap.get(url.getPath());
                        }
                        else
                        {
                            jr = Jimi.createJimiReader(url);
                            source = jr.getRasterImage();
                            jrReaderMap.put(url.getPath(),source);
                        }
                        
                        int width = source.getWidth();
                        int height = source.getHeight();
                        
                        // a small buffer for pixel data to copy-between
                        int[] rowBuffer = new int[width];
                        // loop through, copying each row from source to destination
                        source.getRowRGB(ty%38, rowBuffer, 0);
                        
                        //rowBuffer = pixels;
                        //System.out.println("rowBuffer=" + rowBuffer.length);
                        //System.out.println("tx=["+tx+"]ty=["+ty+"]tw=["+tw+"]th=["+th+"]");
                        
                        consumer.setPixels(
                                tx, ty, tw, th, model, rowBuffer, off, scansize );
                        
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        consumer.setPixels(
                                tx, ty, tw, th, model, pixels, off, scansize );
                    }
                    /*if (jr!=null)
                    {
                        jr.close();
                    }*/
                    count++;
                }
            }
            
            
            
            if (endx % 38 ==0 && endy % 38 ==0){
                mapOutput.append("</map>");
                try {
                    createMapFile(mapOutput, m);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    System.out.println("Error in creating map file, please check log above");
                }
            }
        }
    }
    
    private void createMapFile(StringBuffer stringBuffer, int runningMap) throws Exception{
        System.out.println("Create map file");
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
         
        String dateHourString = dateFormat.format(mosaicCurrent.getDateRun());
        
        // Create folder and store map
        String mapFolderString = PropertyManager.getValue(PropertyConstants.MOSAIC_MAP_PATH);
        String currentFolderString = mapFolderString + dateHourString;
        File currentFolder = new File(currentFolderString);
        if (!currentFolder.exists()) {
            currentFolder.mkdirs();
        }
        currentFolder = null; 
        
        // Writes to map file
        String mapFileName = dateHourString+"_" + runningMap + ".map";
        String mapFullFileName = mapFolderString + dateHourString+"/" + mapFileName;
        PrintWriter out = new PrintWriter(new FileWriter(PropertyManager.getValue(PropertyConstants.WORKING_WEBAPP_HOME) + "map/" + mapFileName), true); 
        out.println(stringBuffer);   
        out.close();
        
        // Archive it
        out = new PrintWriter(new FileWriter(mapFullFileName), true); 
        out.println(stringBuffer);   
        out.close(); 
        
        // Copy to outside
        /*File mapFolder = new File(mapFolderString);
        File[] mapFiles = mapFolder.listFiles();
        for(int i=0;i<mapFiles.length;i++) {
            File singleFile = mapFiles[i];
            if (!singleFile.isDirectory())
                singleFile.delete();
        }*/
        
        
        
        out = null;
    }
    
    public MosaicCurrent getMosaicCurrent() {
        return mosaicCurrent;
    }
    
    public void setMosaicCurrent(MosaicCurrent mosaicCurrent) {
        this.mosaicCurrent = mosaicCurrent;
    }
    
    
    
    
}
