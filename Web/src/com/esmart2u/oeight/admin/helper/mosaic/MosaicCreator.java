/*
 * MosaicCreator.java
 *
 * Created on August 28, 2007, 2:26 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.mosaic;

import com.esmart2u.oeight.data.MosaicCurrent;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.service.HibernateAppListener;
import com.sun.jimi.core.Jimi;
import com.sun.jimi.core.JimiException;
import com.sun.jimi.core.JimiWriter;
import com.sun.jimi.core.filters.EnhancedCropImageFilter;
import com.sun.jimi.core.filters.Shrink;
import com.sun.jimi.core.filters.Tile;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
/**
 *
 * @author meauchyuan.lee
 */
public class MosaicCreator {
    
   
    ImageProducer source;
    ImageProducer filterSource;
    OEightTileFilter filter;
    Image image;
    MosaicCurrent mosaicCurrent = null;
    
    /** Creates a new instance of ShrinkDemo */
    public MosaicCreator() {
        try {   
            // Should get total images need to render, and render one by one
            // Use currentMosaic as number
            int currentMosaicNumber = 1;
            
            String thumbnailFolderString = PropertyManager.getValue(PropertyConstants.THUMBNAIL_STORE_PATH);
            URL url = new URL("file:///"+thumbnailFolderString+"na.jpg");  
            source = Jimi.getImageProducer(url);
            filterSource = source; 

            //filter = new OEightTileFilter(source,1064,1064); // 28 per row
            //filter = new OEightTileFilter(source,494,494); //26 per row, smaller size
            //filter = new OEightTileFilter(source,988,988); //26 per row
            
            //Top
            filter = new OEightTileFilter(source,418,304); //26 per row
            mosaicCurrent = filter.getMosaicCurrent();
            
            System.out.println("Filter initialized");
            ImageProducer product = new FilteredImageSource(source, filter); 
            System.out.println("Image filtered");
            System.out.println("Product=" + product); 
            
            if (mosaicCurrent != null)
            {
                int count = mosaicCurrent.getTotalMaps();
             
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
            
                // Save original size to photos
                String dateHourString = dateFormat.format(mosaicCurrent.getDateRun());
            
                String mosaicFolderString = PropertyManager.getValue(PropertyConstants.MOSAIC_STORE_PATH);
                String currentFolderString = mosaicFolderString + dateHourString;
                File currentFolder = new File(currentFolderString);
                if (!currentFolder.exists())
                {
                    currentFolder.mkdirs();
                }
                currentFolder = null;
                
                String mosaicJPG = dateHourString+"_" + count + ".jpg";
                String mosaicJPGFull = mosaicFolderString + dateHourString+"/" + mosaicJPG;
                String mosaicJPGFullMain = mosaicFolderString + dateHourString+"/" + dateHourString+"_" + count;
                String mosaicPNG = dateHourString+"_" + count + ".png";
                String mosaicPNGFull = mosaicFolderString + dateHourString+"/" + mosaicPNG;

                Jimi.putImage(product, mosaicJPGFull);
                System.out.println("Jimi JPG image created");  

                url = new URL("file:///" + mosaicJPGFull); 
                source = Jimi.getImageProducer(url);
                Jimi.putImage(source, mosaicPNGFull);
                System.out.println("Jimi PNG image created");  
                
                // Copy to outside 
                File mosaicFolder = new File(mosaicFolderString);
                File[] mosaicFiles = mosaicFolder.listFiles();
                for(int i=0;i<mosaicFiles.length;i++)
                {
                    File singleFile = mosaicFiles[i];
                    if (!singleFile.isDirectory())
                        singleFile.delete();
                }
                
                url = new URL("file:///" + mosaicJPGFull); 
                source = Jimi.getImageProducer(url);
                Jimi.putImage(source, mosaicFolderString + mosaicJPG);  
                
                url = new URL("file:///" + mosaicPNGFull); 
                source = Jimi.getImageProducer(url);
                Jimi.putImage(source, mosaicFolderString + mosaicPNG);  
                
                URL mosaicUrl = new URL("file:///" + mosaicJPGFullMain +".jpg"); 
                source = Jimi.getImageProducer(mosaicUrl);
                filterSource = source;
                /*
                EnhancedCropImageFilter cropFilter = new EnhancedCropImageFilter(0,0,988,380); 
                System.out.println("Filter initialized");
                ImageProducer part1 = new FilteredImageSource(source, cropFilter); 
                System.out.println("Image filtered");
                Jimi.putImage(part1, mosaicJPGFullMain +"a.jpg");
                System.out.println("Part 1 created");
                
                cropFilter = new EnhancedCropImageFilter(0,381,988,380);  
                ImageProducer part2 = new FilteredImageSource(source, cropFilter); 
                Jimi.putImage(part2, mosaicJPGFullMain +"b.jpg");
                System.out.println("Part 2 created");
                
                cropFilter = new EnhancedCropImageFilter(0,761,988,228);  
                ImageProducer part3 = new FilteredImageSource(source, cropFilter); 
                Jimi.putImage(part3, mosaicJPGFullMain +"c.jpg");
                System.out.println("Part 3 created");
                */
                
            }
            //Top End
            
            /*//Image shrinking - no difference though
            source = Jimi.getImageProducer("C:\\duke_Test_tiled.jpg");
            filterSource = source;
            
            filter = new Shrink(source, 2);
            FilteredImageSource fis = new FilteredImageSource(filterSource, filter);
            filterSource = fis;
            
            
            Image new_image = Toolkit.getDefaultToolkit().createImage(fis);
            if(new_image != null) {
                //canvas.setImage(new_image);
                Jimi.putImage(new_image, "C:\\duke_Test_tiled_shrinked.jpg");
            }
            */
            
            
  
            //Recent   
            url = new URL("file:///"+thumbnailFolderString+"na.jpg");  
            ImageProducer recentSource = Jimi.getImageProducer(url); 
            OEightRecentFilter recentFilter = new OEightRecentFilter(recentSource,418,304); //26 per row
            mosaicCurrent = recentFilter.getMosaicCurrent();
            
            System.out.println("Filter initialized");
            ImageProducer recentProduct = new FilteredImageSource(recentSource, recentFilter); 
            System.out.println("Image filtered");
            System.out.println("Product=" + recentProduct); 
            
            if (mosaicCurrent != null)
            {
                int count = mosaicCurrent.getTotalMaps();
             
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
            
                // Save original size to photos
                String dateHourString = dateFormat.format(mosaicCurrent.getDateRun());
            
                String mosaicFolderString = PropertyManager.getValue(PropertyConstants.MOSAIC_STORE_PATH);
                String currentFolderString = mosaicFolderString + dateHourString;
                File currentFolder = new File(currentFolderString);
                if (!currentFolder.exists())
                {
                    currentFolder.mkdirs();
                }
                currentFolder = null;
                
                String mosaicJPG = dateHourString+"_" + count + "_recent.jpg";
                String mosaicJPGFull = mosaicFolderString + dateHourString+"/" + mosaicJPG;
                String mosaicJPGFullMain = mosaicFolderString + dateHourString+"/" + dateHourString+"_" + count;
                String mosaicPNG = dateHourString+"_" + count + "_recent.png";
                String mosaicPNGFull = mosaicFolderString + dateHourString+"/" + mosaicPNG;

                Jimi.putImage(recentProduct, mosaicJPGFull);
                System.out.println("Jimi JPG image created");  

                url = new URL("file:///" + mosaicJPGFull); 
                recentSource = Jimi.getImageProducer(url);
                Jimi.putImage(recentSource, mosaicPNGFull);
                System.out.println("Jimi PNG image created");  
                
                // Copy to outside  
                url = new URL("file:///" + mosaicJPGFull); 
                recentSource = Jimi.getImageProducer(url);
                Jimi.putImage(recentSource, mosaicFolderString + mosaicJPG);  
                
                url = new URL("file:///" + mosaicPNGFull); 
                recentSource = Jimi.getImageProducer(url);
                Jimi.putImage(recentSource, mosaicFolderString + mosaicPNG);  
                
                URL mosaicUrl = new URL("file:///" + mosaicJPGFullMain +"_recent.jpg"); 
                recentSource = Jimi.getImageProducer(mosaicUrl);
                filterSource = recentSource; 
                
            } 
            //Recent End
           
            
            //Profile 
            url = new URL("file:///"+thumbnailFolderString+"na.jpg");  
            ImageProducer profileSource = Jimi.getImageProducer(url);  
            OEightProfileFilter profileFilter = new OEightProfileFilter(profileSource,418,304); //26 per row
            mosaicCurrent = profileFilter.getMosaicCurrent();
            
            System.out.println("Filter initialized");
            ImageProducer profileProduct = new FilteredImageSource(profileSource, profileFilter); 
            System.out.println("Image filtered");
            System.out.println("Product=" + profileProduct); 
            
            if (mosaicCurrent != null)
            {
                int count = mosaicCurrent.getTotalMaps();
             
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
            
                // Save original size to photos
                String dateHourString = dateFormat.format(mosaicCurrent.getDateRun());
            
                String mosaicFolderString = PropertyManager.getValue(PropertyConstants.MOSAIC_STORE_PATH);
                String currentFolderString = mosaicFolderString + dateHourString;
                File currentFolder = new File(currentFolderString);
                if (!currentFolder.exists())
                {
                    currentFolder.mkdirs();
                }
                currentFolder = null;
                
                String mosaicJPG = dateHourString+"_" + count + "_profile.jpg";
                String mosaicJPGFull = mosaicFolderString + dateHourString+"/" + mosaicJPG;
                String mosaicJPGFullMain = mosaicFolderString + dateHourString+"/" + dateHourString+"_" + count;
                String mosaicPNG = dateHourString+"_" + count + "_profile.png";
                String mosaicPNGFull = mosaicFolderString + dateHourString+"/" + mosaicPNG;

                Jimi.putImage(profileProduct, mosaicJPGFull);
                System.out.println("Jimi JPG image created");  

                url = new URL("file:///" + mosaicJPGFull); 
                profileSource = Jimi.getImageProducer(url);
                Jimi.putImage(profileSource, mosaicPNGFull);
                System.out.println("Jimi PNG image created");  
                
                // Copy to outside   
                url = new URL("file:///" + mosaicJPGFull); 
                profileSource = Jimi.getImageProducer(url);
                Jimi.putImage(profileSource, mosaicFolderString + mosaicJPG);  
                
                url = new URL("file:///" + mosaicPNGFull); 
                profileSource = Jimi.getImageProducer(url);
                Jimi.putImage(profileSource, mosaicFolderString + mosaicPNG);  
                
                URL mosaicUrl = new URL("file:///" + mosaicJPGFullMain +"_profile.jpg"); 
                profileSource = Jimi.getImageProducer(mosaicUrl);
                filterSource = profileSource; 
                
            } 
            //Profile End
            
            
            //Green 
            url = new URL("file:///"+thumbnailFolderString+"na.jpg");  
            ImageProducer greenSource = Jimi.getImageProducer(url); 
            OEightGreenFilter greenFilter = new OEightGreenFilter(greenSource,418,304); //26 per row
            mosaicCurrent = greenFilter.getMosaicCurrent();
            
            System.out.println("Filter initialized");
            ImageProducer greenProduct = new FilteredImageSource(greenSource, greenFilter); 
            System.out.println("Image filtered");
            System.out.println("Product=" + greenProduct); 
            
            if (mosaicCurrent != null)
            {
                int count = mosaicCurrent.getTotalMaps();
             
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
            
                // Save original size to photos
                String dateHourString = dateFormat.format(mosaicCurrent.getDateRun());
            
                String mosaicFolderString = PropertyManager.getValue(PropertyConstants.MOSAIC_STORE_PATH);
                String currentFolderString = mosaicFolderString + dateHourString;
                File currentFolder = new File(currentFolderString);
                if (!currentFolder.exists())
                {
                    currentFolder.mkdirs();
                }
                currentFolder = null;
                
                String mosaicJPG = dateHourString+"_" + count + "_green.jpg";
                String mosaicJPGFull = mosaicFolderString + dateHourString+"/" + mosaicJPG;
                String mosaicJPGFullMain = mosaicFolderString + dateHourString+"/" + dateHourString+"_" + count;
                String mosaicPNG = dateHourString+"_" + count + "_green.png";
                String mosaicPNGFull = mosaicFolderString + dateHourString+"/" + mosaicPNG;

                Jimi.putImage(greenProduct, mosaicJPGFull);
                System.out.println("Jimi JPG image created");  

                url = new URL("file:///" + mosaicJPGFull); 
                greenSource = Jimi.getImageProducer(url);
                Jimi.putImage(greenSource, mosaicPNGFull);
                System.out.println("Jimi PNG image created");  
                
                // Copy to outside  
                url = new URL("file:///" + mosaicJPGFull); 
                greenSource = Jimi.getImageProducer(url);
                Jimi.putImage(greenSource, mosaicFolderString + mosaicJPG);  
                
                url = new URL("file:///" + mosaicPNGFull); 
                greenSource = Jimi.getImageProducer(url);
                Jimi.putImage(greenSource, mosaicFolderString + mosaicPNG);  
                
                URL mosaicUrl = new URL("file:///" + mosaicJPGFullMain +"_green.jpg"); 
                greenSource = Jimi.getImageProducer(mosaicUrl);
                filterSource = greenSource; 
                
            } 
            //Green End
            
            // Update database after image is generated (Once only at the end for all mosaics)
            updateDatabase(mosaicCurrent);
         
        } catch(JimiException jimiex)
        { 
            System.err.println(jimiex); 
            jimiex.printStackTrace();
        } catch(MalformedURLException bug) 
        {
            System.err.println(bug);
            bug.printStackTrace();
        }
    }
    
    private void updateDatabase(MosaicCurrent mosaicCurrent)
    {
        MosaicDAO.updateLatestMosaic(mosaicCurrent); 
    }
    
    public static void main(String[] args) {  
        // Setting property Manager 
        
        String propertyFile = ConfigurationHelper.getInstance().getPropertyValue("config.startup.path");
        System.setProperty(PropertyConstants.MAIN_CONFIG_FILE_KEY,propertyFile);
                           //PropertyConstants.MAIN_CONFIG_FILE_VALUE);
            
            
        MosaicCreator demo = new MosaicCreator();
    }

}
