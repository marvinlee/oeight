/*
 * WeddingMosaicCreator.java
 *
 * Created on June 29, 2008, 9:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.mosaic;

import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.service.HibernateAppListener;
import com.sun.jimi.core.Jimi;
import com.sun.jimi.core.JimiException;
import com.sun.jimi.core.JimiWriter;
import com.sun.jimi.core.filters.EnhancedCropImageFilter;
import com.sun.jimi.core.filters.Shrink;
import com.sun.jimi.core.filters.Tile;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
/**
 *
 * @author meauchyuan.lee
 */
public class WeddingMosaicCreator {
    
    ImageProducer source;
    ImageProducer filterSource;
    WeddingMosaicTileFilter filter;
    Image image;
    
    /** Creates a new instance of WeddingMosaicCreator */
    public WeddingMosaicCreator() {
         try {   
            // Should get total images need to render, and render one by one
            // Use currentMosaic as number
            int currentMosaicNumber = 1;
            
            String thumbnailFolderString = PropertyManager.getValue(PropertyConstants.WEDDING_THUMBNAIL_STORE_PATH);
            URL url = new URL("file:///"+thumbnailFolderString+"na.jpg");  
            source = Jimi.getImageProducer(url);
            filterSource = source; 
 
            filter = new WeddingMosaicTileFilter(source,800,800); //10 per row at 80px each 
            
            System.out.println("Filter initialized");
            ImageProducer product = new FilteredImageSource(source, filter); 
            System.out.println("Image filtered");
            System.out.println("Product=" + product); 
            
            //if (mosaicCurrent != null)
            //{ 

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");


            String mosaicFolderString = PropertyManager.getValue(PropertyConstants.WEDDING_MOSAIC_STORE_PATH);


            String mosaicJPG = "080808_wedding.jpg";
            String mosaicJPGFull = mosaicFolderString + mosaicJPG;
            //String mosaicJPGFullMain = mosaicFolderString + dateHourString+"/" + dateHourString+"_" + count;
            String mosaicPNG = "080808_wedding.png";
            String mosaicPNGFull = mosaicFolderString + mosaicPNG;

            Jimi.putImage(product, mosaicJPGFull);
            System.out.println("Jimi JPG image created");  

            url = new URL("file:///" + mosaicJPGFull); 
            source = Jimi.getImageProducer(url);
            Jimi.putImage(source, mosaicPNGFull);
            System.out.println("Jimi PNG image created");  

            // Copy to outside 
            /*
            File mosaicFolder = new File(mosaicFolderString);
            File[] mosaicFiles = mosaicFolder.listFiles();
            for(int i=0;i<mosaicFiles.length;i++)
            {
                File singleFile = mosaicFiles[i];
                if (!singleFile.isDirectory())
                    singleFile.delete();
            }*/

            url = new URL("file:///" + mosaicJPGFull); 
            source = Jimi.getImageProducer(url);
            Jimi.putImage(source, mosaicFolderString + mosaicJPG);  

            /*url = new URL("file:///" + mosaicPNGFull); 
            source = Jimi.getImageProducer(url);
            Jimi.putImage(source, mosaicFolderString + mosaicPNG);  
*/
                /*
                URL mosaicUrl = new URL("file:///" + mosaicJPGFullMain +".jpg"); 
                source = Jimi.getImageProducer(mosaicUrl);
                filterSource = source;  
                EnhancedCropImageFilter cropFilter = new EnhancedCropImageFilter(0,0,988,380); 
                System.out.println("Filter initialized");
                ImageProducer part1 = new FilteredImageSource(source, cropFilter); 
                System.out.println("Image filtered");
                Jimi.putImage(part1, mosaicJPGFullMain +"a.jpg");
                System.out.println("Part 1 created");
                
                cropFilter = new EnhancedCropImageFilter(0,381,988,380);  
                ImageProducer part2 = new FilteredImageSource(source, cropFilter); 
                Jimi.putImage(part2, mosaicJPGFullMain +"b.jpg");
                System.out.println("Part 2 created");
                
                cropFilter = new EnhancedCropImageFilter(0,761,988,228);  
                ImageProducer part3 = new FilteredImageSource(source, cropFilter); 
                Jimi.putImage(part3, mosaicJPGFullMain +"c.jpg");
                System.out.println("Part 3 created");
                */
                
            //}
             
         
        } catch(JimiException jimiex)
        { 
            System.err.println(jimiex); 
            jimiex.printStackTrace();
        } catch(MalformedURLException bug) 
        {
            System.err.println(bug);
            bug.printStackTrace();
        }
    }
     
    public static void main(String[] args) {  
        // Setting property Manager 
        
        String propertyFile = ConfigurationHelper.getInstance().getPropertyValue("config.startup.path");
        System.setProperty(PropertyConstants.MAIN_CONFIG_FILE_KEY,propertyFile);
                           //PropertyConstants.MAIN_CONFIG_FILE_VALUE);
            
            
        WeddingMosaicCreator demo = new WeddingMosaicCreator();
        
        System.exit(0);
    }  
}
