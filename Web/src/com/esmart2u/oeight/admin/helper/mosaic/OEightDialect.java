/*
 * OEightDialect.java
 *
 * Created on December 27, 2007, 2:11 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.mosaic;

import java.sql.Types;
import org.hibernate.Hibernate;
import org.hibernate.dialect.MySQL5InnoDBDialect;

/**
 *
 * @author meauchyuan.lee
 */
public class OEightDialect extends MySQL5InnoDBDialect {
    
    /** Creates a new instance of OEightDialect */
    public OEightDialect() {
        registerHibernateType( Types.LONGVARCHAR, Hibernate.STRING.getName() ); // TODO: Check make sure the 2nd arg is right 

    }
    
}
