/*
 * HibernateUtil.java
 *
 * Created on October 3, 2007, 10:33 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.mosaic;
 
import com.esmart2u.oeight.data.*;
import com.opensymphony.oscache.base.Config;
import java.sql.Types;

import org.hibernate.*;
import org.hibernate.cfg.*;

public class HibernateUtil {

    private static final SessionFactory sessionFactory;

    static {
        try {
            
            System.out.println("Build sessionFactory");  
            Configuration config = new Configuration();

            // Use the dialect below to solve a text field problem in mysql
            config.setProperty("hibernate.dialect", "com.esmart2u.oeight.admin.helper.mosaic.OEightDialect");
            //config.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
            config.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
            config.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3306/secondstep");
            //config.setProperty("hibernate.connection.username", "marvin");
            //config.setProperty("hibernate.connection.password", "z1x2c3v4"); 
            config.setProperty("hibernate.connection.username", "user");
            config.setProperty("hibernate.connection.password", "user123"); 
            
            config.addClass(User.class);
            config.addClass(UserCountry.class);
            config.addClass(UserDetails.class);
            config.addClass(Mosaic.class);
            config.addClass(MosaicCurrent.class);
            // check below and remove where not needed
            /*config.addClass(Advertisement.class);
            config.addClass(FeaturedBlog.class);
            config.addClass(Feedback.class);
            config.addClass(Invitation.class);
            config.addClass(Login.class);
            config.addClass(LoginReset.class);
            //config.addClass(Mosaic.class);
            //config.addClass(MosaicCurrent.class);
            config.addClass(Ranking.class);
            config.addClass(RankingPodium.class);
            config.addClass(RankingSnapshot.class);
            config.addClass(ReservedNames.class);
            config.addClass(SeasonGreeting.class);
            config.addClass(Sponsor.class);
            config.addClass(Store.class);
            //config.addClass(User.class);
            //config.addClass(UserCountry.class);
            //config.addClass(UserDetails.class);
            config.addClass(UserFriends.class);
            config.addClass(UserInvites.class);
            config.addClass(UserPayment.class);
            config.addClass(UserPaymentAddress.class);
            config.addClass(UserPictures.class);
            config.addClass(UserSponsor.class);
            config.addClass(UserVotes.class);
            config.addClass(UserWishers.class);*/
             
            
            
            //config.addClass(UserWishers.class);
            
            
            //System.out.println("org.hibernate.dialect.TypeNames.get -16" + new org.hibernate.dialect.TypeNames().get(-16));
            //System.out.println("org.hibernate.dialect.TypeNames.get -4" + new org.hibernate.dialect.TypeNames().get(-4));
            //System.out.println("org.hibernate.dialect.TypeNames.get -1" + new org.hibernate.dialect.TypeNames().get(-1));
            /*System.out.println(" type " + Types.ARRAY);
            System.out.println(" type " + Types.BIGINT);
            System.out.println(" type " + Types.BINARY);
            System.out.println(" type " + Types.BIT);
            System.out.println(" type " + Types.BLOB);
            System.out.println(" type " + Types.BOOLEAN);
            System.out.println(" type " + Types.CHAR);
            System.out.println(" type " + Types.CLOB);
            System.out.println(" type " + Types.DATALINK);
            System.out.println(" type " + Types.DATE);
            System.out.println(" type " + Types.DECIMAL);
            System.out.println(" type " + Types.DISTINCT);
            System.out.println(" type " + Types.DOUBLE);
            System.out.println(" type " + Types.FLOAT);
            System.out.println(" type " + Types.INTEGER);
            System.out.println(" type " + Types.JAVA_OBJECT);
            System.out.println(" type " + Types.LONGNVARCHAR);
            System.out.println(" type " + Types.LONGVARBINARY);
            System.out.println(" type Types.LONGVARCHAR" + Types.LONGVARCHAR);
            System.out.println(" type " + Types.NCHAR);
            System.out.println(" type " + Types.NCLOB);
            System.out.println(" type " + Types.NULL);
            System.out.println(" type " + Types.NUMERIC);
            System.out.println(" type " + Types.NVARCHAR);
            System.out.println(" type " + Types.OTHER);
            System.out.println(" type " + Types.REAL);
            System.out.println(" type " + Types.REF);
            System.out.println(" type " + Types.ROWID);
            System.out.println(" type " + Types.SMALLINT);
            System.out.println(" type " + Types.SQLXML);
            System.out.println(" type " + Types.STRUCT);
            System.out.println(" type " + Types.TIME);
            System.out.println(" type " + Types.TIMESTAMP);
            System.out.println(" type " + Types.TINYINT);
            System.out.println(" type " + Types.VARBINARY);
            System.out.println(" type " + Types.VARCHAR);*/
            // Create the SessionFactory from properties above
            sessionFactory = config.buildSessionFactory(); 
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            ex.printStackTrace();
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}