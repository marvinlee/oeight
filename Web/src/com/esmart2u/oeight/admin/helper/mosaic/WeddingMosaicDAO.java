/*
 * WeddingMosaicDAO.java
 *
 * Created on June 29, 2008, 9:51 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.mosaic;

import com.esmart2u.solution.web.struts.service.HibernateUtil;
import com.esmart2u.oeight.data.*;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import java.text.SimpleDateFormat;
import java.util.*;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Expression;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
/**
 *
 * @author meauchyuan.lee
 */
public class WeddingMosaicDAO {
    
    /** Creates a new instance of WeddingMosaicDAO */
    public WeddingMosaicDAO() {
    }
    
    public static Vector getPhotosForMosaic() {
        Vector photos = new Vector();
        Session session = null;
        Transaction transaction = null;
        try  {
            
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession(); 
            transaction = session.beginTransaction();
              
            Iterator results = session.createSQLQuery("SELECT USER.USER_NAME, OEIGHT_WEDDING.PHOTO_S_PATH, OEIGHT_WEDDING.PHOTO_L_PATH, OEIGHT_WEDDING.COUPLE_NAME FROM OEIGHT_WEDDING INNER JOIN USER" +
                                                      " ON OEIGHT_WEDDING.USER_ID = USER.USER_ID" +
                                                      " INNER JOIN USER_COUNTRY" +
                                                      " ON OEIGHT_WEDDING.USER_ID = USER_COUNTRY.USER_ID" +
                                                      //" WHERE SNAPSHOT_MONTH = DATE('"+dateFormatter.format(currentMonth)+" 00:00:00')" +
                                                      " WHERE OEIGHT_WEDDING.STATUS != 'C'" +
                                                      " AND USER_COUNTRY.VALIDATED = 'Y'" +
                                                      //" AND USER_COUNTRY.PHOTO_S_PATH != '" + PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL) + "'" + 
                                                      //" AND USER.USER_ID != " + PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY) +
                                                      " ORDER BY OEIGHT_WEDDING.STATUS ASC, OEIGHT_WEDDING.DATE_REGISTERED ASC").list().iterator();
            
            while (results.hasNext() )  { 
                Object[] row = (Object[]) results.next(); 
                String userId = (String)row[0];
                String photoSmall = (String)row[1];
                String photoLarge = (String)row[2];
                String coupleName = (String)row[3];
                //String photoDescription = (String)row[3];
                System.out.println("User=" + userId + ", Small=" + photoSmall + ", Large=" + photoLarge + "Couple=" + coupleName);
                String[] values = new String[4];
                values[0] = userId;
                values[1] = photoSmall;
                values[2] = photoLarge;
                values[3] = coupleName;
                photos.add(values);
            }
              
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
                
        
        return photos;
    }
    
}
