/*
 * MosaicDAO.java
 *
 * Created on October 2, 2007, 11:55 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.admin.helper.mosaic;

import com.esmart2u.solution.web.struts.service.HibernateUtil;
import com.esmart2u.oeight.data.*;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import java.text.SimpleDateFormat;
import java.util.*;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Expression;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
/**
 *
 * @author meauchyuan.lee
 */
public class MosaicDAO {
    
    /** Creates a new instance of MosaicDAO */
    public MosaicDAO() {
    }
    
    public static Vector getPhotosForMosaic() {
        Vector photos = new Vector();
        Session session = null;
        try  {
            
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();
            
            //Iterator results = session.createQuery("select user.userName, user.userCountry.photoSmallPath, user.userCountry.photoLargePath, user.userCountry.photoDescription  from User user").list().iterator();
            java.util.Date currentMonth = DateObjectHelper.getCurrentMonthDateObject(); 
            String DISPLAY_STRING_FORMAT = "yyyy-MM-dd";
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DISPLAY_STRING_FORMAT);
            Iterator results = session.createSQLQuery("SELECT USER.USER_NAME, USER_COUNTRY.PHOTO_S_PATH, USER_COUNTRY.PHOTO_L_PATH, USER_COUNTRY.PHOTO_DESC FROM USER INNER JOIN USER_COUNTRY" +
                                                      " ON USER.USER_ID = USER_COUNTRY.USER_ID" +
                                                      " LEFT JOIN RANKING_SNAPSHOT ON USER.USER_ID = RANKING_SNAPSHOT.USER_ID" + 
                                                      " WHERE SNAPSHOT_MONTH = DATE('"+dateFormatter.format(currentMonth)+" 00:00:00')" +
                                                      " AND USER_COUNTRY.REPORT_ABUSE != 'C'" +
                                                      " AND USER_COUNTRY.VALIDATED = 'Y'" +
                                                      " AND USER_COUNTRY.PHOTO_S_PATH != '" + PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL) + "'" + 
                                                      " AND USER.USER_ID != " + PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY) +
                                                      " ORDER BY VOTES DESC, USER_COUNTRY.SYS_REGISTERED DESC" +
                                                      " LIMIT 88").list().iterator();
            
            while (results.hasNext() )  { 
                Object[] row = (Object[]) results.next();
                /*String userId = rs.getString("user_id");
                String photoSmall = rs.getString("photo_s_path");
                String photoLarge = rs.getString("photo_l_path");
                String photoDescription = rs.getString("photo_desc");*/
                String userId = (String)row[0];
                String photoSmall = (String)row[1];
                String photoLarge = (String)row[2];
                String photoDescription = (String)row[3];
                System.out.println("User=" + userId + ", Small=" + photoSmall + ", Large=" + photoLarge + ", Desc=" + photoDescription);
                String[] values = new String[4];
                values[0] = userId;
                values[1] = photoSmall;
                values[2] = photoLarge;
                values[3] = photoDescription;
                photos.add(values);
            }
             
            
            session.getTransaction().commit();
            
            System.out.println("Connection from DataSource successfully closed!<br>");
        } catch(Exception e)  {
            session.getTransaction().rollback();
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        finally{
            //if (session.isOpen()){
                session.close();
            //}
        }
                
        
        return photos;
    }
    
    public static void updateLatestMosaic(MosaicCurrent mosaicCurrent) {
        Session session = null;
        Transaction transaction = null;
        try  {
            
            System.out.println("Going to update MosaicCurrent table.");
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
            String dateHourString = dateFormat.format(mosaicCurrent.getDateRun());
            
            // Get latest mosaic check if matches current
            List result = session.createQuery("from MosaicCurrent").list();
            Iterator iterator = result.iterator();
            String dbValue = "";
            MosaicCurrent dbCurrent = null;
            if (result != null && !result.isEmpty()){
                System.out.println("Size=" + result.size()); 
                while (iterator.hasNext()) {
                    Object obj = (Object)iterator.next(); 
                    System.out.println("obj=" + obj + " class=" + obj.getClass());
                    dbCurrent = (MosaicCurrent)obj;
                    if (dbCurrent == null)
                    {
                        System.out.println("DBCurrent is null");
                    }
                    else
                    {
                        System.out.println("DBCurrent not null");
                        System.out.println("DB Current date=" + dbCurrent.getDateRun() + " id=" + dbCurrent.getMosaicForeignId());
                    }
                    System.out.println("Remove old record");
                    performMosaicPersistence(true, dateHourString, mosaicCurrent, dbCurrent, session);
                }
            }else {
                System.out.println("Generate new record");
                performMosaicPersistence(false, dateHourString, mosaicCurrent, null, session);
            }
            
            transaction.commit();
            
            System.out.println("Connection from DataSource successfully closed!<br>");
        } catch(Exception e)  {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
            System.out.println(e.getMessage());
        }finally{ 
            //if (session.isOpen()){
                session.close();
            //}
        }
        
    }
    
    private static void performMosaicPersistence(boolean deleteCurrent, String dateHourString, MosaicCurrent mosaicCurrent, MosaicCurrent dbCurrent, Session session) {
        
        if (deleteCurrent){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHH");
            String  dbValue = dateFormat.format(dbCurrent.getDateRun());
            System.out.println("DB dateRun=" + dbValue);
            // If matches, do nothing, else update
            //if (!dateHourString.equals(dbValue)){
                // Delete the old current version
                if (dbCurrent != null) {
                    session.delete(dbCurrent);
                }
            //}
        }
        
        // Save new current mosaic
        mosaicCurrent.setUploaded(PropertyConstants.BOOLEAN_NO);
        mosaicCurrent.setMosaicForeignId(Long.parseLong(dateHourString));
        Long generatedId = (Long)session.save(mosaicCurrent);
        System.out.println("MosaicId generated=" + generatedId);
        
        int mosaicSequence = mosaicCurrent.getTotalMaps();
        for (int i=1; i<=mosaicSequence; i++) {
            // Save all other mosaics
            Mosaic mosaic = new Mosaic();
            mosaic.setDateGenerated(mosaicCurrent.getDateRun());
            mosaic.setSequenceNumber(i);
            mosaic.setMapFilePath(dateHourString + "_" + i + ".map"); // Single file
            //mosaic.setPictureLocalPath(dateHourString + "_" + i + ".png");// Seperate files
            //mosaic.setPictureLocalPath(dateHourString + "_" + i + ".jpg");// Seperate files
            mosaic.setPictureLocalPath(dateHourString +"/" + dateHourString + "_" + i);// Seperate files
            mosaic.setMosaicCurrentId(mosaicCurrent.getMosaicId());
            //mosaic.setPictureRemotePath(); -- Not set here 
            generatedId = (Long)session.save(mosaic);
            System.out.println("MosaicId generated=" + generatedId);
        }
        
        
    }
    
    
    
    
        public static MosaicCurrent getMosaicCurrent()
        { 
            MosaicCurrent current = null;
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            Session session = sessionFactory.openSession();
            Transaction transaction = null;
            
            try {
                
                transaction = session.beginTransaction();
                List result = session.createCriteria(MosaicCurrent.class)
                .addOrder(Order.desc("mosaicId"))
                .list();
                Iterator iterator = result.iterator(); 
                if (iterator.hasNext())
                {
                    current = (MosaicCurrent)iterator.next();
                    //System.out.println("Current Id is " + current.getMosaicId());
                } 

                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
            
            return current;
        }
        
        public static List getCurrentMosaicsList(long mosaicId)
        { 
            List mosaicList = null;
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            Session session = sessionFactory.openSession();
            Transaction transaction = null;
            
            try {
                
                transaction = session.beginTransaction();
                mosaicList = session.createCriteria(Mosaic.class)
                .add( Restrictions.eq("mosaicCurrentId", mosaicId))
                .addOrder(Order.asc("sequenceNumber"))
                .list(); 

                transaction.commit();
                
             } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
            return mosaicList;
        }
    
        /*public static void main(String[] args) {
            MosaicCurrent current = getMosaicCurrent();
            List list = getCurrentMosaicsList(current.getMosaicId());
            for(int i=0;i<list.size();i++)
            {
                Mosaic mosaic = (Mosaic)list.get(i);
                System.out.println("Sequence=" + mosaic.getSequenceNumber());
                System.out.println("Map file=" + mosaic.getMapFilePath());
                System.out.println("Pic file=" + mosaic.getPictureLocalPath());
            }
        }*/
        
    // Recent mosaic - todo, get from recent links, now recent users
    public static Vector getPhotosForMosaicRecent() {
        Vector photos = new Vector();
        Session session = null;
        try  {
            
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();
            
            //Iterator results = session.createQuery("select user.userName, user.userCountry.photoSmallPath, user.userCountry.photoLargePath, user.userCountry.photoDescription  from User user").list().iterator();
            java.util.Date currentMonth = DateObjectHelper.getCurrentMonthDateObject(); 
            String DISPLAY_STRING_FORMAT = "yyyy-MM-dd";
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DISPLAY_STRING_FORMAT);
            Iterator results = session.createSQLQuery("SELECT USER.USER_NAME, USER_COUNTRY.PHOTO_S_PATH, USER_COUNTRY.PHOTO_L_PATH, USER_COUNTRY.PHOTO_DESC FROM USER INNER JOIN USER_COUNTRY" +
                                                      " ON USER.USER_ID = USER_COUNTRY.USER_ID" + 
                                                      " AND USER_COUNTRY.REPORT_ABUSE != 'C'" +
                                                      " AND USER_COUNTRY.VALIDATED = 'Y'" +
                                                      " AND USER_COUNTRY.PHOTO_S_PATH != '" + PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL) + "'" + 
                                                      " AND USER.USER_ID != " + PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY) +
                                                      " ORDER BY USER_COUNTRY.SYS_REGISTERED DESC"+ 
                                                      " LIMIT 88").list().iterator();
            
            while (results.hasNext() )  { 
                Object[] row = (Object[]) results.next();
                /*String userId = rs.getString("user_id");
                String photoSmall = rs.getString("photo_s_path");
                String photoLarge = rs.getString("photo_l_path");
                String photoDescription = rs.getString("photo_desc");*/
                String userId = (String)row[0];
                String photoSmall = (String)row[1];
                String photoLarge = (String)row[2];
                String photoDescription = (String)row[3];
                System.out.println("User=" + userId + ", Small=" + photoSmall + ", Large=" + photoLarge + ", Desc=" + photoDescription);
                String[] values = new String[4];
                values[0] = userId;
                values[1] = photoSmall;
                values[2] = photoLarge;
                values[3] = photoDescription;
                photos.add(values);
            }
             
            
            session.getTransaction().commit();
            
            System.out.println("Connection from DataSource successfully closed!<br>");
        } catch(Exception e)  {
            session.getTransaction().rollback();
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        finally{
            //if (session.isOpen()){
                session.close();
            //}
        }
                
        
        return photos;
    }
            
    // Profile mosaic - todo, get from profile with most friends, now user order
    public static Vector getPhotosForMosaicProfile() {
        Vector photos = new Vector();
        Session session = null;
        try  {
            
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();
            
            //Iterator results = session.createQuery("select user.userName, user.userCountry.photoSmallPath, user.userCountry.photoLargePath, user.userCountry.photoDescription  from User user").list().iterator();
            java.util.Date currentMonth = DateObjectHelper.getCurrentMonthDateObject(); 
            String DISPLAY_STRING_FORMAT = "yyyy-MM-dd";
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DISPLAY_STRING_FORMAT);
            Iterator results = session.createSQLQuery("SELECT USER.USER_NAME, USER_COUNTRY.PHOTO_S_PATH, USER_COUNTRY.PHOTO_L_PATH, USER_COUNTRY.PHOTO_DESC FROM USER INNER JOIN USER_COUNTRY" +
                                                      " ON USER.USER_ID = USER_COUNTRY.USER_ID" + 
                                                      " AND USER_COUNTRY.REPORT_ABUSE != 'C'" +
                                                      " AND USER_COUNTRY.VALIDATED = 'Y'" +
                                                      " AND USER_COUNTRY.PHOTO_S_PATH != '" + PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL) + "'" + 
                                                      " AND USER.USER_ID != " + PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY) +
                                                      " ORDER BY USER_COUNTRY.SYS_REGISTERED"+ 
                                                      " LIMIT 88").list().iterator();
            
            while (results.hasNext() )  { 
                Object[] row = (Object[]) results.next();
                /*String userId = rs.getString("user_id");
                String photoSmall = rs.getString("photo_s_path");
                String photoLarge = rs.getString("photo_l_path");
                String photoDescription = rs.getString("photo_desc");*/
                String userId = (String)row[0];
                String photoSmall = (String)row[1];
                String photoLarge = (String)row[2];
                String photoDescription = (String)row[3];
                System.out.println("User=" + userId + ", Small=" + photoSmall + ", Large=" + photoLarge + ", Desc=" + photoDescription);
                String[] values = new String[4];
                values[0] = userId;
                values[1] = photoSmall;
                values[2] = photoLarge;
                values[3] = photoDescription;
                photos.add(values);
            }
             
            
            session.getTransaction().commit();
            
            System.out.println("Connection from DataSource successfully closed!<br>");
        } catch(Exception e)  {
            session.getTransaction().rollback();
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        finally{
            //if (session.isOpen()){
                session.close();
            //}
        }
                
        
        return photos;
    }
    
          
    // Profile mosaic - todo, get from profile with most friends, now only female users
    public static Vector getPhotosForMosaicGreen() {
        Vector photos = new Vector();
        Session session = null;
        try  {
            
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();
            
            //Iterator results = session.createQuery("select user.userName, user.userCountry.photoSmallPath, user.userCountry.photoLargePath, user.userCountry.photoDescription  from User user").list().iterator();
            java.util.Date currentMonth = DateObjectHelper.getCurrentMonthDateObject(); 
            String DISPLAY_STRING_FORMAT = "yyyy-MM-dd";
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DISPLAY_STRING_FORMAT);
            Iterator results = session.createSQLQuery("SELECT USER.USER_NAME, USER_COUNTRY.PHOTO_S_PATH, USER_COUNTRY.PHOTO_L_PATH, USER_COUNTRY.PHOTO_DESC FROM USER INNER JOIN USER_COUNTRY" +
                                                      " ON USER.USER_ID = USER_COUNTRY.USER_ID" + 
                                                      " AND USER_COUNTRY.REPORT_ABUSE != 'C'" +
                                                      " AND USER_COUNTRY.VALIDATED = 'Y'" +
                                                      " AND USER_COUNTRY.GENDER = 'F'" + //Female only
                                                      " AND USER_COUNTRY.PHOTO_S_PATH != '" + PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL) + "'" + 
                                                      " AND USER.USER_ID != " + PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY) +
                                                      " ORDER BY USER_COUNTRY.SYS_REGISTERED DESC"+ 
                                                      " LIMIT 88").list().iterator();
            
            while (results.hasNext() )  { 
                Object[] row = (Object[]) results.next();
                /*String userId = rs.getString("user_id");
                String photoSmall = rs.getString("photo_s_path");
                String photoLarge = rs.getString("photo_l_path");
                String photoDescription = rs.getString("photo_desc");*/
                String userId = (String)row[0];
                String photoSmall = (String)row[1];
                String photoLarge = (String)row[2];
                String photoDescription = (String)row[3];
                System.out.println("User=" + userId + ", Small=" + photoSmall + ", Large=" + photoLarge + ", Desc=" + photoDescription);
                String[] values = new String[4];
                values[0] = userId;
                values[1] = photoSmall;
                values[2] = photoLarge;
                values[3] = photoDescription;
                photos.add(values);
            }
             
            
            session.getTransaction().commit();
            
            System.out.println("Connection from DataSource successfully closed!<br>");
        } catch(Exception e)  {
            session.getTransaction().rollback();
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        finally{
            //if (session.isOpen()){
                session.close();
            //}
        }
                
        
        return photos;
    }
}
