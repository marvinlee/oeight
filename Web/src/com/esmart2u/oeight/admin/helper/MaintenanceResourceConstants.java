/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// MaintenanceResourceConstants.java

package com.esmart2u.oeight.admin.helper;

/**
 * Resource constants such as keys.
 *
 * @author  Chee Weng Keong
 * @version $Revision: 1.10.2.1 $
 */

public interface MaintenanceResourceConstants extends com.esmart2u.solution.base.helper.MaintenanceResourceConstants
{
    public static final String PARAMETER_MISSING_CODE = "common.message.mt_parameter_000001000";

    public static final String BRANCH_MISSING_CODE = "common.message.mt_branch_000001100";

    public static final String CURRENCY_MISSING_CODE = "common.message.mt_currency_000001200";

    public static final String VALUER_MISSING_CODE = "common.message.mt_valuer_000001301";
    public static final String VALUER_CONTACT_PERSON_MISSING_CODE = "common.message.mt_valuer_000001302";
    public static final String VALUER_RELATED_MISSING_CODE = "common.message.mt_valuer_000001303";

    public static final String FACILITY_MISSING_CODE = "common.message.mt_facility_000001400";
    public static final String FACILITY_QUESTIONNAIRE_MISSING_CODE = "common.message.mt_facility_000001401";
    public static final String FACILITY_COLLATERAL_MISSING_CODE = "common.message.mt_facility_000001402";
    public static final String FACILITY_SCHEDULE_TYPE_MISSING_CODE = "common.message.mt_facility_000001403";
    public static final String FACILITY_AMOUNT_PURPOSE_MISSING_CODE = "common.message.mt_facility_000001404";
    public static final String FACILITY_BILLING_CYCLE_MISSING_CODE = "common.message.mt_facility_000001405";

    public static final String COLLATERAL_CATEGORY_MISSING_CODE = "common.message.mt_collateral_000001500";
    public static final String COLLATERAL_MISSING_CODE = "common.message.mt_collateral_000001501";
    public static final String COLLATERAL_DESCRIPTION_MISSING_CODE = "common.message.mt_collateral_000001502";
    public static final String COLLATERAL_INSTRUMENT_MISSING_CODE = "common.message.mt_collateral_000001503";
    public static final String COLLATERAL_REGISTRATION_PLACE_MISSING_CODE = "common.message.mt_collateral_000001504";
    public static final String COLLATERAL_SAFETY_FACTOR_CLASSIFICATION_MISSING_CODE = "common.message.mt_collateral_000001505";

    public static final String FACILITY_PRICING_MISSING_CODE = "common.message.mt_facility_pricing_000001700";
    public static final String FACILITY_PRICING_PACKAGE_MISSING_CODE = "common.message.mt_facility_pricing_000001701";

    public static final String DOCUMENT_MISSING_CODE = "common.message.mt_document_000001900";

    public static final String OPTION_MISSING_CODE = "common.message.mt_option_000002000";

    public static final String RATE_TYPE_MISSING_CODE = "common.message.mt_rate_type_000002100";

    public static final String CREDIT_BUREAU_TYPE_MISSING_CODE = "common.message.mt_credit_bureau_type_000002600";
    public static final String OLD_PASSWORD_NOT_SPECIFIED = "common.message.mt_credit_bureau_type_000002601";
    public static final String OLD_PASSWORD_INCORRECT = "common.message.mt_credit_bureau_type_000002602";
    public static final String NEW_PASSWORD_NOT_CONFIRM = "common.message.mt_credit_bureau_type_000002603";
    public static final String RECONFIRM_PASSWORD_INCORRECT = "common.message.mt_credit_bureau_type_000002604";

    public static final String ATTORNEY_MISSING_CODE = "common.message.mt_attorney_000002700";

    public static final String SAFE_CUSTODY_MISSING_CODE = "common.message.mt_safe_custody_000002800";
    public static final String SAFE_CUSTODY_BRANCH_LINK_MISSING_CODE = "common.message.mt_safe_custody_000002801";

    public static final String NOTE_MISSING_CODE = "common.message.mt_note_000002900";

    public static final String SALESMAN_MISSING_CODE = "common.message.mt_salesman_000003700";

    public static final String PERIOD_MISSING_CODE = "common.message.mt_period_000003800";

    public static final String SOLICITOR_MISSING_CODE = "common.message.mt_solicitor_000004100";
    public static final String SOLICITOR_BRANCH_LINK_MISSING_CODE = "common.message.mt_solicitor_000004101";
    public static final String SOLICITOR_CONTACT_PERSON_LINK_MISSING_CODE = "common.message.mt_solicitor_000004102";

    public static final String BRANCH_STRUCTURE_MISSING_CODE = "common.message.mt_branch_structure_000004500";
    public static final String BRANCH_STRUCTURE_VERSION_MISSING_ID = "common.message.mt_branch_structure_000004501";
    public static final String BRANCH_STRUCTURE_VERSION_MISSING_VERSION_NUMBER = "common.message.mt_branch_structure_000004502";
    public static final String BRANCH_STRUCTURE_VERSION_DUPLICATE_VERSION_NUMBER = "common.message.mt_branch_structure_000004503";

    public static final String ISIC_MISSING_CODE = "common.message.mt_isic_000004900";
    public static final String TOWN_MISSING_CODE = "common.message.mt_town_000004800";

    public static final String PRODUCT_POLICY_MISSING_CODE = "common.message.mt_prod_policy_000005000";
    public static final String PRODUCT_POLICY_FEE_MISSING_CODE = "common.message.mt_prod_policy_000005001";
    public static final String STATE_MISSING_CODE = "common.message.mt_state_000005100";
    public static final String REGION_MISSING_CODE = "common.message.mt_region_000005200";
    public static final String BUSINESS_TYPE_MISSING_CODE = "common.message.mt_business_type_000005300";

    public static final String INDUSTRY_MISSING_CODE = "common.message.mt_industry_000005900";
    public static final String FINANCIAL_HEADER_MISSING_CODE = "common.message.mt_fin_hdr_000005100";
    public static final String FINANCIAL_ITEM_MISSING_CODE = "common.message.mt_fin_item_000005101";

    public static final String ARRANGEMENT_PURPOSE_MISSING_CODE = "common.message.mt_argnt_pur_000009000";

}
// end of MaintenanceResourceConstants.java