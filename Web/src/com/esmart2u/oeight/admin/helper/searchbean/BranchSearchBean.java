/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BranchSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Extended Search Bean for Validation use.
 *
 * @author  Siripong Visasmongkolchai
 * @version $Revision: 1.8 $
 */

public class BranchSearchBean extends SearchBean
{
    public static final String SEARCH_BY_ABBREVIATION = "abbreviation";

    private String stateCode;
    private String stateDescription;
    private String countryCode;
    private String countryDescription;
    private String townCode;
    private String townDescription;

    public String getStateCode()
    {
        return stateCode;
    }

    public void setStateCode(String stateCode)
    {
        this.stateCode = stateCode;
    }

    public String getStateDescription()
    {
        return stateDescription;
    }

    public void setStateDescription(String stateDescription)
    {
        this.stateDescription = stateDescription;
    }

    public String getCountryCode()
    {
        return countryCode;
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getCountryDescription()
    {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription)
    {
        this.countryDescription = countryDescription;
    }

    public String getTownCode() {
        return townCode;
    }

    public void setTownCode(String townCode) {
        this.townCode = townCode;
    }

    public String getTownDescription() {
        return townDescription;
    }

    public void setTownDescription(String townDescription) {
        this.townDescription = townDescription;
    }
}

// end of BranchSearchBean.java