package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Extended Search Bean for Validation use.
 *
 * @author Terence Koh
 * @version $Revision: 1.10 $
 */

public class ParameterSearchBean extends SearchBean
{

    private String tableName;
    private String tableBusinessName;
    private String referenceCode;
    private String referenceDescription;
    private String refTo;

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getTableBusinessName()
    {
        return tableBusinessName;
    }

    public void setTableBusinessName(String tableBusinessName)
    {
        this.tableBusinessName = tableBusinessName;
    }

    public String getReferenceCode()
    {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode)
    {
        this.referenceCode = referenceCode;
    }

    public String getReferenceDescription()
    {
        return referenceDescription;
    }

    public void setReferenceDescription(String referenceDescription)
    {
        this.referenceDescription = referenceDescription;
    }

    public String getRefTo()
    {
        return refTo;
    }

    public void setRefTo(String refTo)
    {
        this.refTo = refTo;
    }
}

// end of ParameterSearchBean.java