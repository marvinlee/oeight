/**
 * ? 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BranchStructureVersionSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Branch Structure Version Search Bean
 *
 * @author  Peerapong Charoenpong
 * @version $Revision: 1.10 $
 */

public class BranchStructureVersionSearchBean extends SearchBean
{
    public static final String SEARCH_BY_BRANCH_STRUCTURE_CODE = "branchStructureCode";

}

// end of BranchStructureVersionSearchBean.java