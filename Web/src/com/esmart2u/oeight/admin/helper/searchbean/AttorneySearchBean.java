package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Created by IntelliJ IDEA.
 * User: ctquah
 * Date: Jan 6, 2004
 * Time: 7:48:46 PM
 * To change this template use Options | File Templates.
 * @version $Revision: 1.2 $
 */

public class AttorneySearchBean extends SearchBean
{
    private String registrationPlaceCode;
    private String registrationPlaceDescription;

    public String getRegistrationPlaceCode() {
        return registrationPlaceCode;
    }

    public void setRegistrationPlaceCode(String registrationPlaceCode) {
        this.registrationPlaceCode = registrationPlaceCode;
    }

    public String getRegistrationPlaceDescription() {
        return registrationPlaceDescription;
    }

    public void setRegistrationPlaceDescription(String registrationPlaceDescription) {
        this.registrationPlaceDescription = registrationPlaceDescription;
    }

}
