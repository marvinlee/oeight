package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Created by IntelliJ IDEA.
 * User: scchiang
 * Date: Jun 18, 2004
 * Time: 6:30:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class CentralBankCollateralSearchBean extends SearchBean
{
    private String collateralClassificationTypeCode;
    private int safetyFactorClassificationCurrentPage;
    private int safetyFactorClassificationTotalPage;
    private int centralBankCollateralCurrentPage;
    private int centralBankCollateralTotalPage;
    private boolean searchExecuted;

    public int getCentralBankCollateralCurrentPage()
    {
        return centralBankCollateralCurrentPage;
    }

    public void setCentralBankCollateralCurrentPage(int centralBankCollateralCurrentPage)
    {
        this.centralBankCollateralCurrentPage = centralBankCollateralCurrentPage;
    }

    public int getCentralBankCollateralTotalPage()
    {
        return centralBankCollateralTotalPage;
    }

    public void setCentralBankCollateralTotalPage(int centralBankCollateralTotalPage)
    {
        this.centralBankCollateralTotalPage = centralBankCollateralTotalPage;
    }

    public int getSafetyFactorClassificationTotalPage()
    {
        return safetyFactorClassificationTotalPage;
    }

    public void setSafetyFactorClassificationTotalPage(int safetyFactorClassificationTotalPage)
    {
        this.safetyFactorClassificationTotalPage = safetyFactorClassificationTotalPage;
    }

    public int getSafetyFactorClassificationCurrentPage()
    {
        return safetyFactorClassificationCurrentPage;
    }

    public void setSafetyFactorClassificationCurrentPage(int safetyFactorClassificationCurrentPage)
    {
        this.safetyFactorClassificationCurrentPage = safetyFactorClassificationCurrentPage;
    }

    public String getCollateralClassificationTypeCode()
    {
        return collateralClassificationTypeCode;
    }

    public void setCollateralClassificationTypeCode(String collateralClassificationTypeCode)
    {
        this.collateralClassificationTypeCode = collateralClassificationTypeCode;
    }

    /**
       * Pre-validate paging parameter before searching.
       */
      public void beforeSearch()
      {
          if (safetyFactorClassificationCurrentPage < 1)
          {
              safetyFactorClassificationCurrentPage = 1;
          }
      }

      /**
       * Post-validate paging parameter after searching.
       */
      public void afterSearch()
      {
          // checking to avoid invalid page number shown on screen; if current page requested is
          // greater than total pages available, set current page count to total page available
          if (safetyFactorClassificationCurrentPage > safetyFactorClassificationTotalPage)
          {
              safetyFactorClassificationCurrentPage = safetyFactorClassificationTotalPage;
          }
          searchExecuted = true;
      }
}
