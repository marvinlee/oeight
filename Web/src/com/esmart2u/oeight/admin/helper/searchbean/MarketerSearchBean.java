/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

/**
 *
 * User: Lisia
 * Date: Dec 6, 2003
 * Time: 4:51:02 PM
 */
package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Marketer Search Bean
 *
 * @author
 * @version $Revision: 1.2 $
 */

public class MarketerSearchBean extends SearchBean{

    private String salesTeamCode;
    private String salesTeamDescription;

    public String getSalesTeamCode()
    {
        return salesTeamCode;
    }

    public void setSalesTeamCode(String salesTeamCode)
    {
        this.salesTeamCode = salesTeamCode;
    }

    public String getSalesTeamDescription()
    {
        return salesTeamDescription;
    }

    public void setSalesTeamDescription(String salesTeamDescription)
    {
        this.salesTeamDescription = salesTeamDescription;
    }
}
