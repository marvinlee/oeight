/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// PromotionSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import java.sql.Date;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Search bean for Promotion module.
 *
 * @author  Lee Meau Chyuan
 * @version $Revision: 1.8 $
 */

public class PromotionSearchBean extends SearchBean
{
    private Date startApprovedDate;
    private String facilityCode;
    private String facilityDescription;

    public Date getStartApprovedDate()
    {
        return startApprovedDate;
    }

    public void setStartApprovedDate(Date startApprovedDate)
    {
        this.startApprovedDate = startApprovedDate;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getFacilityDescription() {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription) {
        this.facilityDescription = facilityDescription;
    }

}

// end of PromotionSearchBean.java