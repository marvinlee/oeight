/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// TownSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * @author todo: Please fill in your name
 * @version $Id: CollateralSubDistrictSearchBean.java,v 1.2 2004/08/11 08:17:17 hschern Exp $
 * @version $Revision: 1.2 $
 */

public class CollateralSubDistrictSearchBean extends SearchBean
{
    private String collateralProvinceCode;
    private String collateralDistrictCode;
    private String countryCode;
    private String collateralProvinceDescription;
    private String collateralDistrictDescription;
    private String countryDescription;


    public String getCountryCode()
    {
        return countryCode;
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getCountryDescription()
    {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription)
    {
        this.countryDescription = countryDescription;
    }

    public String getCollateralProvinceCode() {
        return collateralProvinceCode;
    }

    public void setCollateralProvinceCode(String collateralProvinceCode) {
        this.collateralProvinceCode = collateralProvinceCode;
    }

    public String getCollateralDistrictCode() {
        return collateralDistrictCode;
    }

    public void setCollateralDistrictCode(String collateralDistrictCode) {
        this.collateralDistrictCode = collateralDistrictCode;
    }

    public String getCollateralProvinceDescription() {
        return collateralProvinceDescription;
    }

    public void setCollateralProvinceDescription(String collateralProvinceDescription) {
        this.collateralProvinceDescription = collateralProvinceDescription;
    }

    public String getCollateralDistrictDescription() {
        return collateralDistrictDescription;
    }

    public void setCollateralDistrictDescription(String collateralDistrictDescription) {
        this.collateralDistrictDescription = collateralDistrictDescription;
    }
}

// end of TownSearchBean.java