/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ValuerSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Created by IntelliJ IDEA.
 * User: ctquah
 * Date: Dec 4, 2003
 * Time: 9:59:54 AM
 * To change this template use Options | File Templates.
 * @version $Revision: 1.2 $
 */

public class ValuerSearchBean extends SearchBean
{
    private String townCode;
    private String townDescription;
    private String stateCode;
    private String stateDescription;
    private String countryCode;
    private String countryDescription;
    private String code;
    private String description;
    private String businessCode;
    private String businessDescription;
    private String customerCounterpartyCode;
    private String customerCounterpartyDescription;
    private String securityUserId;
    private String securityUserDescription;
    private String valuerTypeCode;
    private String valuerTypeDescription;

    public String getTownCode() {
        return townCode;
    }

    public void setTownCode(String townCode) {
        this.townCode = townCode;
    }

    public String getTownDescription() {
        return townDescription;
    }

    public void setTownDescription(String townDescription) {
        this.townDescription = townDescription;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateDescription() {
        return stateDescription;
    }

    public void setStateDescription(String stateDescription) {
        this.stateDescription = stateDescription;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryDescription() {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }

    public String getCustomerCounterpartyCode() {
        return customerCounterpartyCode;
    }

    public void setCustomerCounterpartyCode(String customerCounterpartyCode) {
        this.customerCounterpartyCode = customerCounterpartyCode;
    }

    public String getCustomerCounterpartyDescription() {
        return customerCounterpartyDescription;
    }

    public void setCustomerCounterpartyDescription(String customerCounterpartyDescription) {
        this.customerCounterpartyDescription = customerCounterpartyDescription;
    }

    public String getSecurityUserId() {
        return securityUserId;
    }

    public void setSecurityUserId(String securityUserId) {
        this.securityUserId = securityUserId;
    }

    public String getSecurityUserDescription() {
        return securityUserDescription;
    }

    public void setSecurityUserDescription(String securityUserDescription) {
        this.securityUserDescription = securityUserDescription;
    }

    public String getValuerTypeCode() {
        return valuerTypeCode;
    }

    public void setValuerTypeCode(String valuerTypeCode) {
        this.valuerTypeCode = valuerTypeCode;
    }

    public String getValuerTypeDescription() {
        return valuerTypeDescription;
    }

    public void setValuerTypeDescription(String valuerTypeDescription) {
        this.valuerTypeDescription = valuerTypeDescription;
    }

}

//end of ValuerSearchBean.java
