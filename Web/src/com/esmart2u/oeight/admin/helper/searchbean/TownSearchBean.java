/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// TownSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * @author todo: Please fill in your name
 * @version $Id: TownSearchBean.java,v 1.8 2004/05/28 07:24:48 wkchee Exp $
 * @version $Revision: 1.8 $
 */

public class TownSearchBean extends SearchBean
{
    private String stateCode;
    private String countryCode;
    private String stateDescription;
    private String countryDescription;

    public String getStateCode()
    {
        return stateCode;
    }

    public void setStateCode(String stateCode)
    {
        this.stateCode = stateCode;
    }

    public String getStateDescription()
    {
        return stateDescription;
    }

    public void setStateDescription(String stateDescription)
    {
        this.stateDescription = stateDescription;
    }

    public String getCountryCode()
    {
        return countryCode;
    }

    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getCountryDescription()
    {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription)
    {
        this.countryDescription = countryDescription;
    }

}

// end of TownSearchBean.java