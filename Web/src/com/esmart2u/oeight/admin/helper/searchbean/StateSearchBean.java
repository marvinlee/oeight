/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ISICSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * ISIC Version Search Bean
 *
 * @author Goh Siew Chyn
 * @version $Id: StateSearchBean.java,v 1.2 2004/06/09 07:12:41 hschern Exp $
 */

public class StateSearchBean extends SearchBean
{
    private String countryCode;
    private String countryDescription;
    private String regionCode;
    private String regionDescription;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryDescription() {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionDescription() {
        return regionDescription;
    }

    public void setRegionDescription(String regionDescription) {
        this.regionDescription = regionDescription;
    }
}

// end of ISICSearchBean.java