/**
 * � 2001 - 2004 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// FinancialIndustryBenchmarkSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Extended Search Bean for Validation use.
 *
 * @author  skliew
 * @version $Id:
 */

public class FinancialIndustryBenchmarkSearchBean extends SearchBean
{
    private String searchFinancialIndustryBenchmarkVersionCode;

    private int financialIndustryBenchmarkVersionCurrentPage;
    private int financialIndustryBenchmarkVersionTotalPage;
    private String financialIndustryBenchmarkVersionResultLinkKey;

    public String getSearchFinancialIndustryBenchmarkVersionCode() {
        return searchFinancialIndustryBenchmarkVersionCode;
    }

    public void setSearchFinancialIndustryBenchmarkVersionCode(String searchFinancialIndustryBenchmarkVersionCode) {
        this.searchFinancialIndustryBenchmarkVersionCode = searchFinancialIndustryBenchmarkVersionCode;
    }

    public int getFinancialIndustryBenchmarkVersionCurrentPage() {
        return financialIndustryBenchmarkVersionCurrentPage;
    }

    public void setFinancialIndustryBenchmarkVersionCurrentPage(int financialIndustryBenchmarkVersionCurrentPage) {
        this.financialIndustryBenchmarkVersionCurrentPage = financialIndustryBenchmarkVersionCurrentPage;
    }

    public int getFinancialIndustryBenchmarkVersionTotalPage() {
        return financialIndustryBenchmarkVersionTotalPage;
    }

    public void setFinancialIndustryBenchmarkVersionTotalPage(int financialIndustryBenchmarkVersionTotalPage) {
        this.financialIndustryBenchmarkVersionTotalPage = financialIndustryBenchmarkVersionTotalPage;
    }

    public String getFinancialIndustryBenchmarkVersionResultLinkKey() {
        return financialIndustryBenchmarkVersionResultLinkKey;
    }

    public void setFinancialIndustryBenchmarkVersionResultLinkKey(String financialIndustryBenchmarkVersionResultLinkKey) {
        this.financialIndustryBenchmarkVersionResultLinkKey = financialIndustryBenchmarkVersionResultLinkKey;
    }


    /**
     * Pre-validate paging parameter before searching.
     */
    public void beforeFinancialIndustryBenchmarkVersionSearch()
    {
        if (financialIndustryBenchmarkVersionCurrentPage < 1)
        {
            financialIndustryBenchmarkVersionCurrentPage = 1;
        }
    }

    /**
     * Post-validate paging parameter after searching.
     */
    public void afterFinancialIndustryBenchmarkVersionSearch()
    {
        // checking to avoid invalid page number shown on screen; if current page requested is
        // greater than total pages available, set current page count to total page available
        if (financialIndustryBenchmarkVersionCurrentPage > financialIndustryBenchmarkVersionTotalPage)
        {
            financialIndustryBenchmarkVersionCurrentPage = financialIndustryBenchmarkVersionTotalPage;
        }
        //searchExecuted = true;
    }
}

// end of FinancialIndustryBenchmarkVersionSearchBean.java