/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// FacilityPricingSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import java.sql.Date;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Extended Search Bean
 *
 * @author  Ku Kian Kiong
 * @version $Revision: 1.2 $
 */

public class FacilityPricingSearchBean extends SearchBean
{
    private String searchStartDateDay;
    private String searchStartDateMonth;
    private String searchStartDateYear;
    private String id;
    private String facilityPricingPackageId;
    private int tier;
    private Date searchStartDate;

    public String getSearchStartDateDay()
    {
        return searchStartDateDay;
    }

    public void setSearchStartDateDay(String searchStartDateDay)
    {
        this.searchStartDateDay = searchStartDateDay;
    }

    public String getSearchStartDateMonth()
    {
        return searchStartDateMonth;
    }

    public void setSearchStartDateMonth(String searchStartDateMonth)
    {
        this.searchStartDateMonth = searchStartDateMonth;
    }

    public String getSearchStartDateYear()
    {
        return searchStartDateYear;
    }

    public void setSearchStartDateYear(String searchStartDateYear)
    {
        this.searchStartDateYear = searchStartDateYear;
    }

    public Date getSearchStartDate()
    {
        return searchStartDate;
    }

    public void setSearchStartDate(Date searchStartDate)
    {
        this.searchStartDate = searchStartDate;
    }
        public int getTier()
    {
        return tier;
    }

    public void setTier(int tier)
    {
        this.tier = tier;
    }
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
        public String getFacilityPricingPackageId()
    {
        return facilityPricingPackageId;
    }

    public void setFacilityPricingPackageId(String facilityPricingPackageId)
    {
        this.facilityPricingPackageId = facilityPricingPackageId;
    }
}

// end of FacilityPricingSearchBean.java