/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ValidationRuleSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Extended Search Bean for Validation Rule use.
 *
 * @author  Chee Weng Keong
 * @version $Revision: 1.8 $
 */

public class ValidationRuleSearchBean extends SearchBean
{
    private boolean searchParameter;
    private boolean searchSystemTable;

    public boolean isSearchParameter()
    {
        return searchParameter;
    }

    public void setSearchParameter(boolean searchParameter)
    {
        this.searchParameter = searchParameter;
    }

    public boolean isSearchSystemTable()
    {
        return searchSystemTable;
    }

    public void setSearchSystemTable(boolean searchSystemTable)
    {
        this.searchSystemTable = searchSystemTable;
    }
}

// end of ValidationRuleSearchBean.java