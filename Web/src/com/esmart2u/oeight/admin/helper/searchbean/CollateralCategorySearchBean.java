/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CollateralCategorySearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Extended Search Bean for Validation use.
 *
 * @author  Siripong Visasmongkolchai
 * @version $Revision: 1.9 $
 */

public class CollateralCategorySearchBean extends SearchBean
{
    private String collateralTypeCode;
    private String collateralTypeDescription;

    private int childLevelOneCurrentPage;
    private int childLevelOneTotalPage;
    private String childLevelOneResultLinkKey;

    public String getCollateralTypeCode()
    {
        return collateralTypeCode;
    }

    public void setCollateralTypeCode(String collateralTypeCode)
    {
        this.collateralTypeCode = collateralTypeCode;
    }

    public String getCollateralTypeDescription()
    {
        return collateralTypeDescription;
    }

    public void setCollateralTypeDescription(String collateralTypeDescription)
    {
        this.collateralTypeDescription = collateralTypeDescription;
    }

    public int getChildLevelOneCurrentPage()
    {
        return childLevelOneCurrentPage;
    }

    public void setChildLevelOneCurrentPage(int childLevelOneCurrentPage)
    {
        this.childLevelOneCurrentPage = childLevelOneCurrentPage;
    }

    public int getChildLevelOneTotalPage()
    {
        return childLevelOneTotalPage;
    }

    public void setChildLevelOneTotalPage(int childLevelOneTotalPage)
    {
        this.childLevelOneTotalPage = childLevelOneTotalPage;
    }

    public String getChildLevelOneResultLinkKey()
    {
        return childLevelOneResultLinkKey;
    }

    public void setChildLevelOneResultLinkKey(String childLevelOneResultLinkKey)
    {
        this.childLevelOneResultLinkKey = childLevelOneResultLinkKey;
    }

    /**
     * Pre-validate paging parameter before searching.
     */
    public void beforeChildLevelOneSearch()
    {
        if (childLevelOneCurrentPage < 1)
        {
            childLevelOneCurrentPage = 1;
        }
    }

    /**
     * Post-validate paging parameter after searching.
     */
    public void afterChildLevelOneSearch()
    {
        // checking to avoid invalid page number shown on screen; if current page requested is
        // greater than total pages available, set current page count to total page available
        if (childLevelOneCurrentPage > childLevelOneTotalPage)
        {
            childLevelOneCurrentPage = childLevelOneTotalPage;
        }
        //searchExecuted = true;
    }
}

// end of CollateralCategorySearchBean.java