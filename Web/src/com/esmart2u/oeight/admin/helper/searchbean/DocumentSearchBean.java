/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DocumentSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * {@TODO: Fill in the class description}
 *
 * @author Ko Jun Jie
 * @version $Revision: 1.2 $
 */

public class DocumentSearchBean extends SearchBean
{
    private String documentCategoryCode;
    private String documentCategoryDescription;

    public String getDocumentCategoryCode()
    {
        return documentCategoryCode;
    }

    public void setDocumentCategoryCode(String documentCategoryCode)
    {
        this.documentCategoryCode = documentCategoryCode;
    }

    public String getDocumentCategoryDescription()
    {
        return documentCategoryDescription;
    }

    public void setDocumentCategoryDescription(String documentCategoryDescription)
    {
        this.documentCategoryDescription = documentCategoryDescription;
    }

}

// end of DocumentSearchBean.java