/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CollateralCategorySearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Extended Search Bean for Validation use.
 *
 * @author  skliew
 * @version $Id:
 */

public class FinancialFormatSearchBean extends SearchBean
{
    private String searchFinancialFormatVersionCode;

    private int financialFormatVersionCurrentPage;
    private int financialFormatVersionTotalPage;
    private String financialFormatVersionResultLinkKey;

    public String getSearchFinancialFormatVersionCode() {
        return searchFinancialFormatVersionCode;
    }

    public void setSearchFinancialFormatVersionCode(String searchFinancialFormatVersionCode) {
        this.searchFinancialFormatVersionCode = searchFinancialFormatVersionCode;
    }

    public int getFinancialFormatVersionCurrentPage() {
        return financialFormatVersionCurrentPage;
    }

    public void setFinancialFormatVersionCurrentPage(int financialFormatVersionCurrentPage) {
        this.financialFormatVersionCurrentPage = financialFormatVersionCurrentPage;
    }

    public int getFinancialFormatVersionTotalPage() {
        return financialFormatVersionTotalPage;
    }

    public void setFinancialFormatVersionTotalPage(int financialFormatVersionTotalPage) {
        this.financialFormatVersionTotalPage = financialFormatVersionTotalPage;
    }

    public String getFinancialFormatVersionResultLinkKey() {
        return financialFormatVersionResultLinkKey;
    }

    public void setFinancialFormatVersionResultLinkKey(String financialFormatVersionResultLinkKey) {
        this.financialFormatVersionResultLinkKey = financialFormatVersionResultLinkKey;
    }


    /**
     * Pre-validate paging parameter before searching.
     */
    public void beforeFinancialFormatVersionSearch()
    {
        if (financialFormatVersionCurrentPage < 1)
        {
            financialFormatVersionCurrentPage = 1;
        }
    }

    /**
     * Post-validate paging parameter after searching.
     */
    public void afterFinancialFormatVersionSearch()
    {
        // checking to avoid invalid page number shown on screen; if current page requested is
        // greater than total pages available, set current page count to total page available
        if (financialFormatVersionCurrentPage > financialFormatVersionTotalPage)
        {
            financialFormatVersionCurrentPage = financialFormatVersionTotalPage;
        }
        //searchExecuted = true;
    }
}

// end of FinancialFormatVersionSearchBean.java