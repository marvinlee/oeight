/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CollateralCategorySearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Extended Search Bean for Validation use.
 *
 * @author  skliew
 * @version $Id:
 */

public class FinancialStructureSearchBean extends SearchBean
{
    private String searchFinancialItemCode;

    private int financialItemCurrentPage;
    private int financialItemTotalPage;
    private String financialItemResultLinkKey;

    public String getSearchFinancialItemCode() {
        return searchFinancialItemCode;
    }

    public void setSearchFinancialItemCode(String searchFinancialItemCode) {
        this.searchFinancialItemCode = searchFinancialItemCode;
    }

    public int getFinancialItemCurrentPage() {
        return financialItemCurrentPage;
    }

    public void setFinancialItemCurrentPage(int financialItemCurrentPage) {
        this.financialItemCurrentPage = financialItemCurrentPage;
    }

    public int getFinancialItemTotalPage() {
        return financialItemTotalPage;
    }

    public void setFinancialItemTotalPage(int financialItemTotalPage) {
        this.financialItemTotalPage = financialItemTotalPage;
    }

    public String getFinancialItemResultLinkKey() {
        return financialItemResultLinkKey;
    }

    public void setFinancialItemResultLinkKey(String financialItemResultLinkKey) {
        this.financialItemResultLinkKey = financialItemResultLinkKey;
    }

//    public int getFinancialHeaderCurren

    /**
     * Pre-validate paging parameter before searching.
     */
    public void beforeFinancialItemSearch()
    {
        if (financialItemCurrentPage < 1)
        {
            financialItemCurrentPage = 1;
        }
    }

    /**
     * Post-validate paging parameter after searching.
     */
    public void afterFinancialItemSearch()
    {
        // checking to avoid invalid page number shown on screen; if current page requested is
        // greater than total pages available, set current page count to total page available
        if (financialItemCurrentPage > financialItemTotalPage)
        {
            financialItemCurrentPage = financialItemTotalPage;
        }
        //searchExecuted = true;
    }
}

// end of CollateralCategorySearchBean.java