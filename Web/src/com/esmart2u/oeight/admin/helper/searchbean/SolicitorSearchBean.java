package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Created by IntelliJ IDEA.
 * User: ctquah
 * Date: Dec 22, 2003
 * Time: 3:10:48 PM
 * To change this template use Options | File Templates.
 * @version $Revision: 1.2 $
 */

public class SolicitorSearchBean extends SearchBean
{
    private String stateCode;
    private String stateDescription;
    private String countryCode;
    private String countryDescription;
    private String code;
    private String description;
    private String businessCode;
    private String businessDescription;
    private String customerCounterpartyCode;
    private String customerCounterpartyDescription;
    private String securityUserId;
    private String securityUserName;
    private String solicitorTypeCode;
    private String solicitorTypeDescription;
    private String townCode;
    private String townDescription;

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateDescription() {
        return stateDescription;
    }

    public void setStateDescription(String stateDescription) {
        this.stateDescription = stateDescription;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryDescription() {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }

    public String getCustomerCounterpartyCode() {
        return customerCounterpartyCode;
    }

    public void setCustomerCounterpartyCode(String customerCounterpartyCode) {
        this.customerCounterpartyCode = customerCounterpartyCode;
    }

    public String getCustomerCounterpartyDescription() {
        return customerCounterpartyDescription;
    }

    public void setCustomerCounterpartyDescription(String customerCounterpartyDescription) {
        this.customerCounterpartyDescription = customerCounterpartyDescription;
    }

    public String getSecurityUserId() {
        return securityUserId;
    }

    public void setSecurityUserId(String securityUserId) {
        this.securityUserId = securityUserId;
    }

    public String getSecurityUserName() {
        return securityUserName;
    }

    public void setSecurityUserName(String securityUserName) {
        this.securityUserName = securityUserName;
    }

    public String getSolicitorTypeCode() {
        return solicitorTypeCode;
    }

    public void setSolicitorTypeCode(String solicitorTypeCode) {
        this.solicitorTypeCode = solicitorTypeCode;
    }

    public String getSolicitorTypeDescription() {
        return solicitorTypeDescription;
    }

    public void setSolicitorTypeDescription(String solicitorTypeDescription) {
        this.solicitorTypeDescription = solicitorTypeDescription;
    }

    public String getTownCode() {
        return townCode;
    }

    public void setTownCode(String townCode) {
        this.townCode = townCode;
    }

    public String getTownDescription() {
        return townDescription;
    }

    public void setTownDescription(String townDescription) {
        this.townDescription = townDescription;
    }

}
