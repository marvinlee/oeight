/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// InsuranceCompanySearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Created by IntelliJ IDEA.
 * User: skliew
 * Date: Dec 4, 2003
 * Time: 9:59:54 AM
 * To change this template use Options | File Templates.
 * @version $Revision: 1.2 $
 */

public class InsuranceCompanySearchBean extends SearchBean
{
    private String townCode;
    private String townDescription;
    private String stateCode;
    private String stateDescription;
    private String countryCode;
    private String countryDescription;
    private String code;
    private String description;


    public String getTownCode() {
        return townCode;
    }

    public void setTownCode(String townCode) {
        this.townCode = townCode;
    }

    public String getTownDescription() {
        return townDescription;
    }

    public void setTownDescription(String townDescription) {
        this.townDescription = townDescription;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateDescription() {
        return stateDescription;
    }

    public void setStateDescription(String stateDescription) {
        this.stateDescription = stateDescription;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryDescription() {
        return countryDescription;
    }

    public void setCountryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}

//end of InsuranceCompanySearchBean.java
