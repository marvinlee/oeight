/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ISICSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * ISIC Version Search Bean
 *
 * @author Chern Hak Seng, Chris
 * @version 1.0
 */

public class IndustrySearchBean extends SearchBean
{
    private String industryTypeCode;
    private String industryTypeDescription;
    private String industryCategoryCode;
    private String industryCategoryDescription;

    public String getIndustryTypeCode()
    {
        return industryTypeCode;
    }

    public void setIndustryTypeCode(String IndustryTypeCode)
    {
        this.industryTypeCode = IndustryTypeCode;
    }

    public String getIndustryTypeDescription()
    {
        return industryTypeDescription;
    }

    public void setIndustryTypeDescription(String IndustryTypeDescription)
    {
        this.industryTypeDescription = IndustryTypeDescription;
    }

    public String getIndustryCategoryCode()
    {
        return industryCategoryCode;
    }

    public void setIndustryCategoryCode(String IndustryCategoryCode)
    {
        this.industryCategoryCode = IndustryCategoryCode;
    }

    public String getIndustryCategoryDescription()
    {
        return industryCategoryDescription;
    }

    public void setIndustryCategoryDescription(String IndustryCategoryDescription)
    {
        this.industryCategoryDescription = IndustryCategoryDescription;
    }
}

// end of IndustrySearchBean.java