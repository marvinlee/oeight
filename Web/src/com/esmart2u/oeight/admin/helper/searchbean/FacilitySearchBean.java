package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Created by IntelliJ IDEA.
 * User: ctquah
 * Date: Dec 23, 2003
 * Time: 3:58:35 PM
 * To change this template use Options | File Templates.
 * @version $Revision: 1.2 $
 */

public class FacilitySearchBean extends SearchBean
{
    private String facilityTypeCode;
    private String facilityCategoryCode;
    private String backendHostSystemCode;
    private String standardFacilityPricingCode;
    private String standardRepaymentCalculationMethodCode;
    private String productPolicyCode;
    private String hostSystemCode;
    private String facilityTypeDescription;
    private String facilityCategoryDescription;
    private String backendHostSystemDesciption;
    private String standardFacilityPricingDescription;
    private String standardRepaymentCalculationMethodDescription;
    private String productPolicyDescription;
    private String hostSystemDescription;
    private String preScreeningQuestionnaireCode;
    private String preScreeningQuestionnaireDescription;
    private String collateralCode;
    private String collateralDescription;

    public String getFacilityTypeCode() {
        return facilityTypeCode;
    }

    public void setFacilityTypeCode(String facilityTypeCode) {
        this.facilityTypeCode = facilityTypeCode;
    }

    public String getFacilityCategoryCode() {
        return facilityCategoryCode;
    }

    public void setFacilityCategoryCode(String facilityCategoryCode) {
        this.facilityCategoryCode = facilityCategoryCode;
    }

    public String getBackendHostSystemCode() {
        return backendHostSystemCode;
    }

    public void setBackendHostSystemCode(String backendHostSystemCode) {
        this.backendHostSystemCode = backendHostSystemCode;
    }

    public String getStandardFacilityPricingCode() {
        return standardFacilityPricingCode;
    }

    public void setStandardFacilityPricingCode(String standardFacilityPricingCode) {
        this.standardFacilityPricingCode = standardFacilityPricingCode;
    }

    public String getStandardRepaymentCalculationMethodCode() {
        return standardRepaymentCalculationMethodCode;
    }

    public void setStandardRepaymentCalculationMethodCode(String standardRepaymentCalculationMethodCode) {
        this.standardRepaymentCalculationMethodCode = standardRepaymentCalculationMethodCode;
    }

    public String getProductPolicyCode() {
        return productPolicyCode;
    }

    public void setProductPolicyCode(String productPolicyCode) {
        this.productPolicyCode = productPolicyCode;
    }

    public String getFacilityTypeDescription() {
        return facilityTypeDescription;
    }

    public void setFacilityTypeDescription(String facilityTypeDescription) {
        this.facilityTypeDescription = facilityTypeDescription;
    }

    public String getFacilityCategoryDescription() {
        return facilityCategoryDescription;
    }

    public void setFacilityCategoryDescription(String facilityCategoryDescription) {
        this.facilityCategoryDescription = facilityCategoryDescription;
    }

    public String getBackendHostSystemDesciption() {
        return backendHostSystemDesciption;
    }

    public void setBackendHostSystemDesciption(String backendHostSystemDesciption) {
        this.backendHostSystemDesciption = backendHostSystemDesciption;
    }

    public String getStandardFacilityPricingDescription() {
        return standardFacilityPricingDescription;
    }

    public void setStandardFacilityPricingDescription(String standardFacilityPricingDescription) {
        this.standardFacilityPricingDescription = standardFacilityPricingDescription;
    }

    public String getStandardRepaymentCalculationMethodDescription() {
        return standardRepaymentCalculationMethodDescription;
    }

    public void setStandardRepaymentCalculationMethodDescription(String standardRepaymentCalculationMethodDescription) {
        this.standardRepaymentCalculationMethodDescription = standardRepaymentCalculationMethodDescription;
    }

    public String getProductPolicyDescription() {
        return productPolicyDescription;
    }

    public void setProductPolicyDescription(String productPolicyDescription) {
        this.productPolicyDescription = productPolicyDescription;
    }

    public String getHostSystemCode() {
        return hostSystemCode;
    }

    public void setHostSystemCode(String hostSystemCode) {
        this.hostSystemCode = hostSystemCode;
    }

    public String getHostSystemDescription() {
        return hostSystemDescription;
    }

    public void setHostSystemDescription(String hostSystemDescription) {
        this.hostSystemDescription = hostSystemDescription;
    }

    public String getPreScreeningQuestionnaireCode() {
        return preScreeningQuestionnaireCode;
    }

    public void setPreScreeningQuestionnaireCode(String preScreeningQuestionnaireCode) {
        this.preScreeningQuestionnaireCode = preScreeningQuestionnaireCode;
    }

    public String getPreScreeningQuestionnaireDescription() {
        return preScreeningQuestionnaireDescription;
    }

    public void setPreScreeningQuestionnaireDescription(String preScreeningQuestionnaireDescription) {
        this.preScreeningQuestionnaireDescription = preScreeningQuestionnaireDescription;
    }

    public String getCollateralCode() {
        return collateralCode;
    }

    public void setCollateralCode(String collateralCode) {
        this.collateralCode = collateralCode;
    }

    public String getCollateralDescription() {
        return collateralDescription;
    }

    public void setCollateralDescription(String collateralDescription) {
        this.collateralDescription = collateralDescription;
    }

}
