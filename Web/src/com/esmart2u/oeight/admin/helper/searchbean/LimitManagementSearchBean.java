package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Created by IntelliJ IDEA.
 * User: ctquah
 * Date: Feb 24, 2004
 * Time: 5:50:45 PM
 * To change this template use Options | File Templates.
 * @version $Revision: 1.3 $
 */

public class LimitManagementSearchBean  extends SearchBean
{
    private String mainFacilityCode;
    private String mainFacilityDescription;
    private String toValue;
    private String fromValue;

    public String getMainFacilityCode() {
        return mainFacilityCode;
    }

    public void setMainFacilityCode(String mainFacilityCode) {
        this.mainFacilityCode = mainFacilityCode;
    }

    public String getMainFacilityDescription() {
        return mainFacilityDescription;
    }

    public void setMainFacilityDescription(String mainFacilityDescription) {
        this.mainFacilityDescription = mainFacilityDescription;
    }

    public String getToValue() {
        return toValue;
    }

    public void setToValue(String toValue) {
        this.toValue = toValue;
    }

    public String getFromValue() {
        return fromValue;
    }

    public void setFromValue(String fromValue) {
        this.fromValue = fromValue;
    }

}
