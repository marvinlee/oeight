/**
 * ? 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BranchStructureCloneSearchBean.java

package com.esmart2u.oeight.admin.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Branch Structure Clone Search Bean
 *
 * @author  Peerapong Charoenpong
 * @version $Revision: 1.8 $
 */

public class BranchStructureCloneSearchBean extends SearchBean
{
    private String branchStructureCode;
    private String branchStructureDescription;
    private String branchStructureVersionId;
    private String branchStructureVersionNumber;

    public String getBranchStructureCode()
    {
        return branchStructureCode;
    }

    public void setBranchStructureCode(String branchStructureCode)
    {
        this.branchStructureCode = branchStructureCode;
    }

    public String getBranchStructureDescription()
    {
        return branchStructureDescription;
    }

    public void setBranchStructureDescription(String branchStructureDescription)
    {
        this.branchStructureDescription = branchStructureDescription;
    }

    public String getBranchStructureVersionId()
    {
        return branchStructureVersionId;
    }

    public void setBranchStructureVersionId(String branchStructureVersionId)
    {
        this.branchStructureVersionId = branchStructureVersionId;
    }

    public String getBranchStructureVersionNumber()
    {
        return branchStructureVersionNumber;
    }

    public void setBranchStructureVersionNumber(String branchStructureVersionNumber)
    {
        this.branchStructureVersionNumber = branchStructureVersionNumber;
    }
}

// end of BranchStructureCloneSearchBean.java