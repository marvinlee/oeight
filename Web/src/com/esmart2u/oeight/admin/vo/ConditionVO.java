/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ConditionVO.java

package com.esmart2u.oeight.admin.vo;

import java.lang.String;
import java.sql.Date;
import java.util.Map;
import java.io.Serializable;
import java.lang.Cloneable;

import com.esmart2u.solution.base.helper.ValueObject;

/**
 * Value object to represent table tbl_mt_cond.
 *
 * @author  Chee Weng Keong
 * @version $Revision: 1.8 $
 */

public class ConditionVO extends ValueObject
    implements Serializable, Cloneable
{
    private static final long serialVersionUID = -8811881188118811881L;

    public static final String SP_SELECT = "MtConditionSelect";
    public static final String SP_INSERT = "MtConditionInsert";
    public static final String SP_UPDATE = "MtConditionUpdate";
    public static final String SP_DELETE = "MtConditionSoftDelete";

    public static final Map COLUMN_MAP = toColumnMap(new String[][]
    {
        // properties direct mapping with value object table
        {"cd", "code"},
        {"mt_cond_cat_cd", "conditionCategoryCode"},
        {"dscp", "description"},
        {"alt_dscp", "alternateDescription"},
        {"det", "detail"},
        {"freq_mt_prd_cd", "frequencyCheckingPeriodCode"},
        {"is_adhoc", "adhocCheckedFlag"},
        {"is_waive_allow", "allowWaiveFlag"},
        {"is_deactivated", "deactivatedFlag"},
        {"is_del", "softDeletedFlag"},
        {"is_sys", "systemDataFlag"},
        {"mt_maint_sts_cd", "maintenanceStatusCode"},
        {"created_by", "createdBy"},
        {"updated_by", "updatedBy"},
        {"dt_created", "createdDate"},
        {"dt_updated", "updatedDate"},
        {"version", "currentRecordVersion"}

        // extra properties
    });


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS PROPERTIES DIRECT MAPPING WITH VALUE OBJECT TABLE.
    //******************************************************************************************************************
    private String code;
    private String conditionCategoryCode;
    private String description;
    private String alternateDescription;
    private String detail;
    private String frequencyCheckingPeriodCode;
    private String adhocCheckedFlag;
    private String allowWaiveFlag;
    private String deactivatedFlag;
    private String softDeletedFlag;
    private String systemDataFlag;
    private String maintenanceStatusCode;
    private String createdBy;
    private String updatedBy;
    private Date createdDate;
    private Date updatedDate;
    private int currentRecordVersion;


    //******************************************************************************************************************
    // EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS METHODS TO ACCESS THE VALUE OBJECT TABLE.
    //******************************************************************************************************************
    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getConditionCategoryCode()
    {
        return conditionCategoryCode;
    }

    public void setConditionCategoryCode(String conditionCategoryCode)
    {
        this.conditionCategoryCode = conditionCategoryCode;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAlternateDescription()
    {
        return alternateDescription;
    }

    public void setAlternateDescription(String alternateDescription)
    {
        this.alternateDescription = alternateDescription;
    }

    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
    }

    public String getFrequencyCheckingPeriodCode()
    {
        return frequencyCheckingPeriodCode;
    }

    public void setFrequencyCheckingPeriodCode(String frequencyCheckingPeriodCode)
    {
        this.frequencyCheckingPeriodCode = frequencyCheckingPeriodCode;
    }

    public String getAdhocCheckedFlag()
    {
        return adhocCheckedFlag;
    }

    public void setAdhocCheckedFlag(String adhocCheckedFlag)
    {
        this.adhocCheckedFlag = adhocCheckedFlag;
    }

    public String getAllowWaiveFlag()
    {
        return allowWaiveFlag;
    }

    public void setAllowWaiveFlag(String allowWaiveFlag)
    {
        this.allowWaiveFlag = allowWaiveFlag;
    }

    public String getDeactivatedFlag()
    {
        return deactivatedFlag;
    }

    public void setDeactivatedFlag(String deactivatedFlag)
    {
        this.deactivatedFlag = deactivatedFlag;
    }

    public String getSoftDeletedFlag()
    {
        return softDeletedFlag;
    }

    public void setSoftDeletedFlag(String softDeletedFlag)
    {
        this.softDeletedFlag = softDeletedFlag;
    }

    public String getSystemDataFlag()
    {
        return systemDataFlag;
    }

    public void setSystemDataFlag(String systemDataFlag)
    {
        this.systemDataFlag = systemDataFlag;
    }

    public String getMaintenanceStatusCode()
    {
        return maintenanceStatusCode;
    }

    public void setMaintenanceStatusCode(String maintenanceStatusCode)
    {
        this.maintenanceStatusCode = maintenanceStatusCode;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public int getCurrentRecordVersion()
    {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion)
    {
        this.currentRecordVersion = currentRecordVersion;
    }


    //******************************************************************************************************************
    // METHODS FOR EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************


    //******************************************************************************************************************
    // MISCELLANEOUS.
    //******************************************************************************************************************
    public Object clone() throws CloneNotSupportedException
    {
        ConditionVO conditionVO = (ConditionVO)super.clone();
        if (getCreatedDate() != null) conditionVO.setCreatedDate((Date)getCreatedDate().clone());
        if (getUpdatedDate() != null) conditionVO.setUpdatedDate((Date)getUpdatedDate().clone());
        return conditionVO;
    }
}

// end of ConditionVO.java