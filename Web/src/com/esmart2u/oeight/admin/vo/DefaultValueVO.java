/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DefaultValueVO.java

package com.esmart2u.oeight.admin.vo;

import com.esmart2u.solution.base.helper.ValueObject;
import com.esmart2u.solution.base.vo.HostSourceMappingVO;

import java.io.Serializable;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.sql.Date;

/**
 * Value object to represent table tbl_mt_valid.
 *
 * @author  Siripong Visasmongkolchai
 * @version $Revision: 1.9 $
 */

public class DefaultValueVO extends ValueObject
    implements Serializable, Cloneable
{
    private static final long serialVersionUID = -8811881188118811881L;

    public static final String SP_SELECT = "MtDefaultValueSelect";
    public static final String SP_INSERT = "MtDefaultValueInsert";
    public static final String SP_UPDATE = "MtDefaultValueUpdate";
    public static final String SP_DELETE = "MtDefaultValueDelete";

    public static final Map COLUMN_MAP = toColumnMap(new String[][]
    {
        // properties direct mapping with value object table
        {"tbl_nm", "tableName"},
        {"cd", "code"},
        {"dscp", "tableDescription"},
        {"dscp_mt_resrc_cd", "tableDescriptionResourceCode"},
        {"cd_length", "codeDataLength"},
        {"cd_mt_data_typ_cd", "codeDataType"},
        {"dscp_length", "descriptionDataLength"},
        {"search_by", "searchCode"},
        {"search_by_tbl_nm", "searchTableName"},
        {"search_by_dscp", "searchLabel"},
        {"search_by_dscp_mt_resrc_cd", "searchLabelResourceCode"},
        {"is_default_req", "defaultRequireFlag"},
        {"is_default_search_by_req", "defaultSearchRequireFlag"},
        {"is_parameter", "paramaterFlag"},
        {"is_sys", "systemDataFlag"},
        {"is_src_map", "mappingToHostFlag"},
        {"mt_maint_sts_cd", "maintenanceStatusCode"},
        {"created_by", "createdBy"},
        {"updated_by", "updatedBy"},
        {"dt_created", "createdDate"},
        {"dt_updated", "updatedDate"},
        {"version", "currentRecordVersion"},

        // extra properties
        {"mt_maint_sts_dscp", "maintenanceStatusDescription"},
    });

    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS PROPERTIES DIRECT MAPPING WITH VALUE OBJECT TABLE.
    //******************************************************************************************************************
    private String tableName;
    private String code;
    private String tableDescription;
    private String tableDescriptionResourceCode;
    private int codeDataLength;
    private String codeDataType;
    private int descriptionDataLength;
    private String searchCode;
    private String searchTableName;
    private String searchLabel;
    private String searchLabelResourceCode;
    private String defaultRequireFlag;
    private String defaultSearchRequireFlag;
    private String paramaterFlag;
    private String systemDataFlag;
    private String mappingToHostFlag;
    private String maintenanceStatusCode;
    private String createdBy;
    private String updatedBy;
    private Date createdDate;
    private Date updatedDate;
    private int currentRecordVersion;


    //******************************************************************************************************************
    // EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    private String maintenanceStatusDescription;
    private List hostSourceMappingList;
    private String defaultCode;
    private String defaultDescription;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTableDescription() {
        return tableDescription;
    }

    public void setTableDescription(String tableDescription) {
        this.tableDescription = tableDescription;
    }

    public String getTableDescriptionResourceCode() {
        return tableDescriptionResourceCode;
    }

    public void setTableDescriptionResourceCode(String tableDescriptionResourceCode) {
        this.tableDescriptionResourceCode = tableDescriptionResourceCode;
    }

    public int getCodeDataLength() {
        return codeDataLength;
    }

    public void setCodeDataLength(int codeDataLength) {
        this.codeDataLength = codeDataLength;
    }

    public String getCodeDataType() {
        return codeDataType;
    }

    public void setCodeDataType(String codeDataType) {
        this.codeDataType = codeDataType;
    }

    public int getDescriptionDataLength() {
        return descriptionDataLength;
    }

    public void setDescriptionDataLength(int descriptionDataLength) {
        this.descriptionDataLength = descriptionDataLength;
    }

    public String getSearchCode() {
        return searchCode;
    }

    public void setSearchCode(String searchCode) {
        this.searchCode = searchCode;
    }

    public String getSearchTableName() {
        return searchTableName;
    }

    public void setSearchTableName(String searchTableName) {
        this.searchTableName = searchTableName;
    }

    public String getSearchLabel() {
        return searchLabel;
    }

    public void setSearchLabel(String searchLabel) {
        this.searchLabel = searchLabel;
    }

    public String getSearchLabelResourceCode() {
        return searchLabelResourceCode;
    }

    public void setSearchLabelResourceCode(String searchLabelResourceCode) {
        this.searchLabelResourceCode = searchLabelResourceCode;
    }

    public String getDefaultRequireFlag() {
        return defaultRequireFlag;
    }

    public void setDefaultRequireFlag(String defaultRequireFlag) {
        this.defaultRequireFlag = defaultRequireFlag;
    }

    public String getDefaultSearchRequireFlag() {
        return defaultSearchRequireFlag;
    }

    public void setDefaultSearchRequireFlag(String defaultSearchRequireFlag) {
        this.defaultSearchRequireFlag = defaultSearchRequireFlag;
    }

    public String getParamaterFlag() {
        return paramaterFlag;
    }

    public void setParamaterFlag(String paramaterFlag) {
        this.paramaterFlag = paramaterFlag;
    }

    public String getSystemDataFlag() {
        return systemDataFlag;
    }

    public void setSystemDataFlag(String systemDataFlag) {
        this.systemDataFlag = systemDataFlag;
    }

    public String getMappingToHostFlag() {
        return mappingToHostFlag;
    }

    public void setMappingToHostFlag(String mappingToHostFlag) {
        this.mappingToHostFlag = mappingToHostFlag;
    }

    public String getMaintenanceStatusCode() {
        return maintenanceStatusCode;
    }

    public void setMaintenanceStatusCode(String maintenanceStatusCode) {
        this.maintenanceStatusCode = maintenanceStatusCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getCurrentRecordVersion() {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion) {
        this.currentRecordVersion = currentRecordVersion;
    }

    public String getMaintenanceStatusDescription() {
        return maintenanceStatusDescription;
    }

    public void setMaintenanceStatusDescription(String maintenanceStatusDescription) {
        this.maintenanceStatusDescription = maintenanceStatusDescription;
    }

    public List getHostSourceMappingList() {
        return hostSourceMappingList;
    }

    public void setHostSourceMappingList(List hostSourceMappingList) {
        this.hostSourceMappingList = hostSourceMappingList;
    }

    public String getDefaultCode() {
        return defaultCode;
    }

    public void setDefaultCode(String defaultCode) {
        this.defaultCode = defaultCode;
    }

    public String getDefaultDescription() {
        return defaultDescription;
    }

    public void setDefaultDescription(String defaultDescription) {
        this.defaultDescription = defaultDescription;
    }

    //******************************************************************************************************************
    // MISCELLANEOUS.
    //******************************************************************************************************************
    public Object clone() throws CloneNotSupportedException
    {
        DefaultValueVO defaultValueVO = (DefaultValueVO)super.clone();
        if (getCreatedDate() != null) defaultValueVO.setCreatedDate((Date)getCreatedDate().clone());
        if (getUpdatedDate() != null) defaultValueVO.setUpdatedDate((Date)getUpdatedDate().clone());
        if (getHostSourceMappingList() != null && getHostSourceMappingList() instanceof ArrayList)
        {
            defaultValueVO.setHostSourceMappingList((List)((ArrayList)getHostSourceMappingList()).clone());
            for (int i = 0; i < defaultValueVO.getHostSourceMappingList().size(); i++)
            {
                Object object = defaultValueVO.getHostSourceMappingList().get(i);
                if (object instanceof HostSourceMappingVO)
                {
                    defaultValueVO.getHostSourceMappingList().set(i, ((HostSourceMappingVO)object).clone());
                }
            }
        }
        return defaultValueVO;
    }
}

// end of DefaultValueVO.java