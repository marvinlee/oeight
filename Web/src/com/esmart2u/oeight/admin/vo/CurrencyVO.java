/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CurrencyVO.java

package com.esmart2u.oeight.admin.vo;

import java.lang.String;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.io.Serializable;
import java.lang.Cloneable;
import java.math.BigDecimal;

import com.esmart2u.solution.base.helper.ValueObject;
import com.esmart2u.solution.base.helper.Converter;
import com.esmart2u.solution.base.vo.HostSourceMappingVO;

/**
 * Value object to represent table tbl_mt_cur .
 *
 * @author  Chee Weng Keong
 * @version $Revision: 1.9 $
 */

public class CurrencyVO extends ValueObject
    implements Serializable, Cloneable
{
    private static final long serialVersionUID = -8811881188118811881L;

    public static final String SP_SELECT = "MtCurrencySelect";
    public static final String SP_INSERT = "MtCurrencyInsert";
    public static final String SP_UPDATE = "MtCurrencyUpdate";
    public static final String SP_DELETE = "MtCurrencySoftDelete";

    public static final Map COLUMN_MAP = toColumnMap(new String[][]
    {
        // properties direct mapping with value object table
        {"cd", "code"},
        {"dscp", "description"},
        {"alt_dscp", "alternateDescription"},
        {"buy_rate", "buyRate"},
        {"sell_rate", "sellRate"},
        {"is_default", "defaultFlag"},
        {"is_other", "otherFlag"},
        {"is_deactivated", "deactivatedFlag"},
        {"is_del", "softDeletedFlag"},
        {"is_sys", "systemDataFlag"},
        {"mt_maint_sts_cd", "maintenanceStatusCode"},
        {"created_by", "createdBy"},
        {"updated_by", "updatedBy"},
        {"dt_created", "createdDate"},
        {"dt_updated", "updatedDate"},
        {"version", "currentRecordVersion"},

        // extra properties
        {"mt_maint_sts_dscp", "maintenanceStatusDescription"},
    });


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS PROPERTIES DIRECT MAPPING WITH VALUE OBJECT TABLE.
    //******************************************************************************************************************
    private String code;
    private String description;
    private String alternateDescription;
    private BigDecimal buyRate;
    private BigDecimal sellRate;
    private String defaultFlag;
    private String otherFlag;
    private String deactivatedFlag;
    private String softDeletedFlag;
    private String systemDataFlag;
    private String maintenanceStatusCode;
    private String createdBy;
    private String updatedBy;
    private Date createdDate;
    private Date updatedDate;
    private int currentRecordVersion;


    //******************************************************************************************************************
    // EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    private String maintenanceStatusDescription;
    private List hostSourceMappingList;
    private ValidationRuleVO validationRuleVO;


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS METHODS TO ACCESS THE VALUE OBJECT TABLE.
    //******************************************************************************************************************
    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAlternateDescription()
    {
        return alternateDescription;
    }

    public void setAlternateDescription(String alternateDescription)
    {
        this.alternateDescription = alternateDescription;
    }

    public BigDecimal getBuyRate()
    {
        return buyRate;
    }

    public void setBuyRate(BigDecimal buyRate)
    {
        this.buyRate = buyRate;
    }

    public BigDecimal getSellRate()
    {
        return sellRate;
    }

    public void setSellRate(BigDecimal sellRate)
    {
        this.sellRate = sellRate;
    }

    public String getDefaultFlag()
    {
        return Converter.toBooleanString(defaultFlag);
    }

    public void setDefaultFlag(String defaultFlag)
    {
        this.defaultFlag = Converter.toBooleanString(defaultFlag);
    }

    public String getOtherFlag()
    {
        return Converter.toBooleanString(otherFlag);
    }

    public void setOtherFlag(String otherFlag)
    {
        this.otherFlag = Converter.toBooleanString(otherFlag);
    }

    public String getDeactivatedFlag()
    {
        return Converter.toBooleanString(deactivatedFlag);
    }

    public void setDeactivatedFlag(String deactivatedFlag)
    {
        this.deactivatedFlag = Converter.toBooleanString(deactivatedFlag);
    }

    public String getSoftDeletedFlag()
    {
        return Converter.toBooleanString(softDeletedFlag);
    }

    public void setSoftDeletedFlag(String softDeletedFlag)
    {
        this.softDeletedFlag = Converter.toBooleanString(softDeletedFlag);
    }

    public String getSystemDataFlag()
    {
        return Converter.toBooleanString(systemDataFlag);
    }

    public void setSystemDataFlag(String systemDataFlag)
    {
        this.systemDataFlag = Converter.toBooleanString(systemDataFlag);
    }

    public String getMaintenanceStatusCode()
    {
        return maintenanceStatusCode;
    }

    public void setMaintenanceStatusCode(String maintenanceStatusCode)
    {
        this.maintenanceStatusCode = maintenanceStatusCode;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public int getCurrentRecordVersion()
    {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion)
    {
        this.currentRecordVersion = currentRecordVersion;
    }


    //******************************************************************************************************************
    // METHODS FOR EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    public String getMaintenanceStatusDescription()
    {
        return maintenanceStatusDescription;
    }

    public void setMaintenanceStatusDescription(String maintenanceStatusDescription)
    {
        this.maintenanceStatusDescription = maintenanceStatusDescription;
    }

    public List getHostSourceMappingList()
    {
        if (hostSourceMappingList == null) hostSourceMappingList = new ArrayList(0);
        return hostSourceMappingList;
    }

    public void setHostSourceMappingList(List hostSourceMappingList)
    {
        this.hostSourceMappingList = hostSourceMappingList;
    }

    public ValidationRuleVO getValidationRuleVO()
    {
        return validationRuleVO;
    }

    public void setValidationRuleVO(ValidationRuleVO validationRuleVO)
    {
        this.validationRuleVO = validationRuleVO;
    }


    //******************************************************************************************************************
    // MISCELLANEOUS.
    //******************************************************************************************************************
    public Object clone() throws CloneNotSupportedException
    {
        CurrencyVO currencyVO = (CurrencyVO)super.clone();
        if (getCreatedDate() != null) currencyVO.setCreatedDate((Date)getCreatedDate().clone());
        if (getUpdatedDate() != null) currencyVO.setUpdatedDate((Date)getUpdatedDate().clone());
        if (getHostSourceMappingList() != null && getHostSourceMappingList() instanceof ArrayList)
        {
            currencyVO.setHostSourceMappingList((List)((ArrayList)getHostSourceMappingList()).clone());
            for (int i = 0, size_i = currencyVO.getHostSourceMappingList().size(); i < size_i; i++)
            {
                Object object = currencyVO.getHostSourceMappingList().get(i);
                if (object instanceof HostSourceMappingVO)
                {
                    currencyVO.getHostSourceMappingList().set(i, ((HostSourceMappingVO)object).clone());
                }
            }
        }
        if (getValidationRuleVO() != null) currencyVO.setValidationRuleVO((ValidationRuleVO)getValidationRuleVO().clone());
        return currencyVO;
    }
}

// end of CurrencyVO.java