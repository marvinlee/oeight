 

// ValidationRuleVO.java

package com.esmart2u.oeight.admin.vo;

import java.lang.String;
import java.sql.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.io.Serializable;
import java.lang.Cloneable;

import com.esmart2u.solution.base.helper.Converter;
import com.esmart2u.solution.base.helper.ValueObject;
import com.esmart2u.solution.base.vo.HostSourceMappingVO;

/**
 * Value object to represent table tbl_mt_valid.
 *
 * @author  Chee Weng Keong
 * @version $Revision: 1.9 $
 */

public class ValidationRuleVO extends ValueObject
    implements Serializable, Cloneable
{
    private static final long serialVersionUID = -8811881188118811881L;

    public static final String SP_SELECT = "MtValidationRuleSelect";
    public static final String SP_INSERT = "MtValidationRuleInsert";
    public static final String SP_UPDATE = "MtValidationRuleUpdate";
    public static final String SP_DELETE = "MtValidationRuleDelete";

    public static final Map COLUMN_MAP = toColumnMap(new String[][]
    {
        // properties direct mapping with value object table
        {"tbl_nm", "tableName"},
        {"cd", "code"},
        {"dscp", "tableDescription"},
        {"dscp_mt_resrc_cd", "tableDescriptionResourceCode"},
        {"cd_length", "codeDataLength"},
        {"cd_mt_data_typ_cd", "codeDataType"},
        {"dscp_length", "descriptionDataLength"},
        {"search_by", "searchCode"},
        {"search_by_tbl_nm", "searchTableName"},
        {"search_by_dscp", "searchLabel"},
        {"search_by_dscp_mt_resrc_cd", "searchLabelResourceCode"},
        {"is_default_req", "defaultRequireFlag"},
        {"is_default_search_by_req", "defaultSearchRequireFlag"},
        {"is_parameter", "paramaterFlag"},
        {"is_sys", "systemDataFlag"},
        {"is_src_map", "mappingToHostFlag"},
        {"mt_maint_sts_cd", "maintenanceStatusCode"},
        {"created_by", "createdBy"},
        {"updated_by", "updatedBy"},
        {"dt_created", "createdDate"},
        {"dt_updated", "updatedDate"},
        {"version", "currentRecordVersion"},

        // extra properties
        {"mt_maint_sts_dscp", "maintenanceStatusDescription"},
    });


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS PROPERTIES DIRECT MAPPING WITH VALUE OBJECT TABLE.
    //******************************************************************************************************************
    private String tableName;
    private String code;
    private String tableDescription;
    private String tableDescriptionResourceCode;
    private int codeDataLength;
    private String codeDataType;
    private int descriptionDataLength;
    private String searchCode;
    private String searchTableName;
    private String searchLabel;
    private String searchLabelResourceCode;
    private String defaultRequireFlag;
    private String defaultSearchRequireFlag;
    private String paramaterFlag;
    private String systemDataFlag;
    private String mappingToHostFlag;
    private String maintenanceStatusCode;
    private String createdBy;
    private String updatedBy;
    private Date createdDate;
    private Date updatedDate;
    private int currentRecordVersion;


    //******************************************************************************************************************
    // EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    private String maintenanceStatusDescription;
    private List hostSourceMappingList;


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS METHODS TO ACCESS THE VALUE OBJECT TABLE.
    //******************************************************************************************************************
    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getTableDescription()
    {
        return tableDescription;
    }

    public void setTableDescription(String tableDescription)
    {
        this.tableDescription = tableDescription;
    }

    public String getTableDescriptionResourceCode()
    {
        return tableDescriptionResourceCode;
    }

    public void setTableDescriptionResourceCode(String tableDescriptionResourceCode)
    {
        this.tableDescriptionResourceCode = tableDescriptionResourceCode;
    }

    public int getCodeDataLength()
    {
        return codeDataLength;
    }

    public void setCodeDataLength(int codeDataLength)
    {
        this.codeDataLength = codeDataLength;
    }

    public String getCodeDataType()
    {
        return codeDataType;
    }

    public void setCodeDataType(String codeDataType)
    {
        this.codeDataType = codeDataType;
    }

    public int getDescriptionDataLength()
    {
        return descriptionDataLength;
    }

    public void setDescriptionDataLength(int descriptionDataLength)
    {
        this.descriptionDataLength = descriptionDataLength;
    }

    public String getSearchCode()
    {
        return searchCode;
    }

    public void setSearchCode(String searchCode)
    {
        this.searchCode = searchCode;
    }

    public String getSearchTableName()
    {
        return searchTableName;
    }

    public void setSearchTableName(String searchTableName)
    {
        this.searchTableName = searchTableName;
    }

    public String getSearchLabel()
    {
        return searchLabel;
    }

    public void setSearchLabel(String searchLabel)
    {
        this.searchLabel = searchLabel;
    }

    public String getSearchLabelResourceCode()
    {
        return searchLabelResourceCode;
    }

    public void setSearchLabelResourceCode(String searchLabelResourceCode)
    {
        this.searchLabelResourceCode = searchLabelResourceCode;
    }

    public String getDefaultRequireFlag()
    {
        return Converter.toBooleanString(defaultRequireFlag);
    }

    public void setDefaultRequireFlag(String defaultRequireFlag)
    {
        this.defaultRequireFlag = Converter.toBooleanString(defaultRequireFlag);
    }

    public String getDefaultSearchRequireFlag()
    {
        return Converter.toBooleanString(defaultSearchRequireFlag);
    }

    public void setDefaultSearchRequireFlag(String defaultSearchRequireFlag)
    {
        this.defaultSearchRequireFlag = Converter.toBooleanString(defaultSearchRequireFlag);
    }

    public String getParamaterFlag()
    {
        return Converter.toBooleanString(paramaterFlag);
    }

    public void setParamaterFlag(String paramaterFlag)
    {
        this.paramaterFlag = Converter.toBooleanString(paramaterFlag);
    }

    public String getSystemDataFlag()
    {
        return Converter.toBooleanString(systemDataFlag);
    }

    public void setSystemDataFlag(String systemDataFlag)
    {
        this.systemDataFlag = Converter.toBooleanString(systemDataFlag);
    }

    public String getMappingToHostFlag()
    {
        return Converter.toBooleanString(mappingToHostFlag);
    }

    public void setMappingToHostFlag(String mappingToHostFlag)
    {
        this.mappingToHostFlag = Converter.toBooleanString(mappingToHostFlag);
    }

    public String getMaintenanceStatusCode()
    {
        return maintenanceStatusCode;
    }

    public void setMaintenanceStatusCode(String maintenanceStatusCode)
    {
        this.maintenanceStatusCode = maintenanceStatusCode;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public int getCurrentRecordVersion()
    {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion)
    {
        this.currentRecordVersion = currentRecordVersion;
    }


    //******************************************************************************************************************
    // METHODS FOR EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    public String getMaintenanceStatusDescription()
    {
        return maintenanceStatusDescription;
    }

    public void setMaintenanceStatusDescription(String maintenanceStatusDescription)
    {
        this.maintenanceStatusDescription = maintenanceStatusDescription;
    }

    public List getHostSourceMappingList()
    {
        if (hostSourceMappingList == null) hostSourceMappingList = new ArrayList(0);
        return hostSourceMappingList;
    }

    public void setHostSourceMappingList(List hostSourceMappingList)
    {
        this.hostSourceMappingList = hostSourceMappingList;
    }


    //******************************************************************************************************************
    // MISCELLANEOUS.
    //******************************************************************************************************************
    public Object clone() throws CloneNotSupportedException
    {
        ValidationRuleVO validationRuleVO = (ValidationRuleVO)super.clone();
        if (getCreatedDate() != null) validationRuleVO.setCreatedDate((Date)getCreatedDate().clone());
        if (getUpdatedDate() != null) validationRuleVO.setUpdatedDate((Date)getUpdatedDate().clone());
        if (getHostSourceMappingList() != null && getHostSourceMappingList() instanceof ArrayList)
        {
            validationRuleVO.setHostSourceMappingList((List)((ArrayList)getHostSourceMappingList()).clone());
            for (int i = 0; i < validationRuleVO.getHostSourceMappingList().size(); i++)
            {
                Object object = validationRuleVO.getHostSourceMappingList().get(i);
                if (object instanceof HostSourceMappingVO)
                {
                    validationRuleVO.getHostSourceMappingList().set(i, ((HostSourceMappingVO)object).clone());
                }
            }
        }
        return validationRuleVO;
    }
}

// end of ValidationRuleVO.java