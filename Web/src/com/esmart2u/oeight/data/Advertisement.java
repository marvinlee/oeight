/*
 * Advertisement.java
 *
 * Created on 22 September 2007, 12:08
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class Advertisement {
    
    /** Creates a new instance of Advertisement */
    public Advertisement() {
    }
    
    private long advertId;
    private char advertCode;
    private Date advertDate;
    private String advertFilepath;
    private String advertLink;
    private char status;
    private Date dateUpdated;
    private int version;
    
    public long getAdvertId() {
        return advertId;
    }
    
    public void setAdvertId(long advertId) {
        this.advertId = advertId;
    }
    
    public char getAdvertCode() {
        return advertCode;
    }
    
    public void setAdvertCode(char advertCode) {
        this.advertCode = advertCode;
    }
    
    public Date getAdvertDate() {
        return advertDate;
    }
    
    public void setAdvertDate(Date advertDate) {
        this.advertDate = advertDate;
    }
    
    public String getAdvertFilepath() {
        return advertFilepath;
    }
    
    public void setAdvertFilepath(String advertFilepath) {
        this.advertFilepath = advertFilepath;
    }
    
    public String getAdvertLink() {
        return advertLink;
    }
    
    public void setAdvertLink(String advertLink) {
        this.advertLink = advertLink;
    }
    
    public char getStatus() {
        return status;
    }
    
    public void setStatus(char status) {
        this.status = status;
    }
    
    public Date getDateUpdated() {
        return dateUpdated;
    }
    
    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
    
    public int getVersion() {
        return version;
    }
    
    public void setVersion(int version) {
        this.version = version;
    }
    
    
    
}
