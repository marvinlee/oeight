/*
 * UserInvites.java
 *
 * Created on October 21, 2007, 1:33 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserInvites implements Serializable{
    
    private long userId;
    private String email;
    private String inviteCode;
    private char status;
    private Date dateSent;
    private Date dateMailed;
    private Date dateAccepted;
    
    // Join Table
    private User user;
    
    /** Creates a new instance of UserInvites */
    public UserInvites() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    } 

    public Date getDateMailed() {
        return dateMailed;
    }

    public void setDateMailed(Date dateMailed) {
        this.dateMailed = dateMailed;
    }
    public Date getDateAccepted() {
        return dateAccepted;
    }

    public void setDateAccepted(Date dateAccepted) {
        this.dateAccepted = dateAccepted;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
}
