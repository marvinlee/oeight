/*
 * FriendsterAPITest.java
 *
 * Created on April 17, 2008, 3:07 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterAPITest {
      
    private long friendsterApiTestId;
    private long userId;
    private String name;
    private Date birthday;
    private String userType;
    private String email;
    private long referralUserId;
    private String referralName; 

    public long getFriendsterApiTestId() {
        return friendsterApiTestId;
    }

    public void setFriendsterApiTestId(long friendsterApiTestId) {
        this.friendsterApiTestId = friendsterApiTestId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getReferralUserId() {
        return referralUserId;
    }

    public void setReferralUserId(long referralUserId) {
        this.referralUserId = referralUserId;
    }

    public String getReferralName() {
        return referralName;
    }

    public void setReferralName(String referralName) {
        this.referralName = referralName;
    }
    
    

}
