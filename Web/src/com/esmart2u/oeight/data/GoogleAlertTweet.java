package com.esmart2u.oeight.data;

import java.util.Date;

public class GoogleAlertTweet {

    private long alertTweetId; 
    private long alertId; 
    private String alertTerm;
    private Date receivedTime;
    private Date publishedTime;
    private char publishedFlag;
    private Date tweetedTime;
    private String title;
    private String longUrl;
    private String shortUrl;
    
	public long getAlertTweetId() {
		return alertTweetId;
	}
	public void setAlertTweetId(long alertTweetId) {
		this.alertTweetId = alertTweetId;
	}
	public long getAlertId() {
		return alertId;
	}
	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}
	public String getAlertTerm() {
		return alertTerm;
	}
	public void setAlertTerm(String alertTerm) {
		this.alertTerm = alertTerm;
	}
	public Date getReceivedTime() {
		return receivedTime;
	}
	public void setReceivedTime(Date receivedTime) {
		this.receivedTime = receivedTime;
	}
	public Date getPublishedTime() {
		return publishedTime;
	}
	public void setPublishedTime(Date publishedTime) {
		this.publishedTime = publishedTime;
	}
	public char getPublishedFlag() {
		return publishedFlag;
	}
	public void setPublishedFlag(char publishedFlag) {
		this.publishedFlag = publishedFlag;
	}
	public Date getTweetedTime() {
		return tweetedTime;
	}
	public void setTweetedTime(Date tweetedTime) {
		this.tweetedTime = tweetedTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLongUrl() {
		return longUrl;
	}
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
    
    
    
    
}
