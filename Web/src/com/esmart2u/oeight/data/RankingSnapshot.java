/*
 * RankingSnapshot.java
 *
 * Created on 22 September 2007, 11:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author marvin
 */
public class RankingSnapshot implements Serializable{
    
    /** Creates a new instance of RankingSnapshot */
    public RankingSnapshot() {
    }
  
    private long userId;
    private Date snapshotMonth;
    private long pageView;
    private long invitesSent;
    private long invitesAccepted;
    private long votes;
    private long gameTopPoints;
    private Date lastUpdate;

    // foreign table
    private User user;
    
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getPageView() {
        return pageView;
    }

    public void setPageView(long pageView) {
        this.pageView = pageView;
    }

    public long getInvitesSent() {
        return invitesSent;
    }

    public void setInvitesSent(long invitesSent) {
        this.invitesSent = invitesSent;
    }

    public long getInvitesAccepted() {
        return invitesAccepted;
    }

    public void setInvitesAccepted(long invitesAccepted) {
        this.invitesAccepted = invitesAccepted;
    }

    public long getVotes() {
        return votes;
    }

    public void setVotes(long votes) {
        this.votes = votes;
    }

    public long getGameTopPoints() {
        return gameTopPoints;
    }

    public void setGameTopPoints(long gameTopPoints) {
        this.gameTopPoints = gameTopPoints;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    } 

    public Date getSnapshotMonth() {
        return snapshotMonth;
    }

    public void setSnapshotMonth(Date snapshotMonth) {
        this.snapshotMonth = snapshotMonth;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
     

    

}
