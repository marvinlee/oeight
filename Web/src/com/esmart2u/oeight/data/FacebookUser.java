/*
 * FacebookUser.java
 *
 * Created on April 3, 2008, 11:38 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class FacebookUser {
      
    private long facebookUserPk;
    private long facebookUserId;
    private String facebookUserName;
    private String profileUrl;
    private String primaryPhotoUrl;
    private Date dateUpdated;
    
    /** Creates a new instance of FacebookUser */
    public FacebookUser() {
    }

    public long getFacebookUserPk() {
        return facebookUserPk;
    }

    public void setFacebookUserPk(long facebookUserPk) {
        this.facebookUserPk = facebookUserPk;
    }

    public long getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(long facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public String getFacebookUserName() {
        return facebookUserName;
    }

    public void setFacebookUserName(String facebookUserName) {
        this.facebookUserName = facebookUserName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getPrimaryPhotoUrl() {
        return primaryPhotoUrl;
    }

    public void setPrimaryPhotoUrl(String primaryPhotoUrl) {
        this.primaryPhotoUrl = primaryPhotoUrl;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
    
}
