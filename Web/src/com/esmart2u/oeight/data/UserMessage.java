/*
 * UserMessage.java
 *
 * Created on March 18, 2008, 4:10 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserMessage implements Serializable{
    
    /** Creates a new instance of UserMessage */
    public UserMessage() {
    }
    
    private long userMessageId;
    private long senderUserId;
    private long recipientUserId;
    private String subject;
    private String message;
    private Date dateSent;
    private Date dateRead;
    private boolean isRead;
    private boolean isReplied;
    private boolean isSaved;
    private boolean isDeleted;
    private boolean isSavedDeleted;
    private long referredSourceMessageId;
     
    private User sender;
    private User recipient;

    public long getUserMessageId() {
        return userMessageId;
    }

    public void setUserMessageId(long userMessageId) {
        this.userMessageId = userMessageId;
    }

    public long getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(long senderUserId) {
        this.senderUserId = senderUserId;
    }

    public long getRecipientUserId() {
        return recipientUserId;
    }

    public void setRecipientUserId(long recipientUserId) {
        this.recipientUserId = recipientUserId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public Date getDateRead() {
        return dateRead;
    }

    public void setDateRead(Date dateRead) {
        this.dateRead = dateRead;
    }

    public boolean getIsRead() {
        return isRead;
    }
    
    public boolean isRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public boolean getIsReplied() {
        return isReplied;
    }
    
    public boolean isReplied() {
        return isReplied;
    }

    public void setIsReplied(boolean isReply) {
        this.isReplied = isReply;
    }

    public long getReferredSourceMessageId() {
        return referredSourceMessageId;
    }

    public void setReferredSourceMessageId(long referredSourceMessageId) {
        this.referredSourceMessageId = referredSourceMessageId;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public boolean getIsSaved() {
        return isSaved;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setIsSaved(boolean isSaved) {
        this.isSaved = isSaved;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }
    
    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public boolean getIsSavedDeleted() {
        return isSavedDeleted;
    }
    public boolean isSavedDeleted() {
        return isSavedDeleted;
    }

    public void setIsSavedDeleted(boolean isSavedDeleted) {
        this.isSavedDeleted = isSavedDeleted;
    }
    
    
            
}
