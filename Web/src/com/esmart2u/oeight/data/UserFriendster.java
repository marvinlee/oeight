/*
 * UserFriendster.java
 *
 * Created on March 22, 2008, 8:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserFriendster {
    
    /** Creates a new instance of UserFriendster */
    public UserFriendster() {
    }
    
    private long userFriendsterId;
    private long userId;
    private long friendsterUserId;
    private String primaryPhoto;
    private String photosString;
    private String friendsString;
    private Date dateUpdated;

    public long getUserFriendsterId() {
        return userFriendsterId;
    }

    public void setUserFriendsterId(long userFriendsterId) {
        this.userFriendsterId = userFriendsterId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getFriendsterUserId() {
        return friendsterUserId;
    }

    public void setFriendsterUserId(long friendsterUserId) {
        this.friendsterUserId = friendsterUserId;
    }

    public String getPrimaryPhoto() {
        return primaryPhoto;
    }

    public void setPrimaryPhoto(String primaryPhoto) {
        this.primaryPhoto = primaryPhoto;
    }

    public String getPhotosString() {
        return photosString;
    }

    public void setPhotosString(String photosString) {
        this.photosString = photosString;
    }

    public String getFriendsString() {
        return friendsString;
    }

    public void setFriendsString(String friendsString) {
        this.friendsString = friendsString;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
    
    
    
            
}
