/*
 * UserSponsor.java
 *
 * Created on 22 September 2007, 11:46
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class UserSponsor {
    
    /** Creates a new instance of UserSponsor */
    public UserSponsor() {
    }
 
    private long userId;
    private long sponsorId;
    private Date assignTimestamp;
    private int version;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(long sponsorId) {
        this.sponsorId = sponsorId;
    }

    public Date getAssignTimestamp() {
        return assignTimestamp;
    }

    public void setAssignTimestamp(Date assignTimestamp) {
        this.assignTimestamp = assignTimestamp;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    
}
