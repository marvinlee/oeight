/*
 * UserFacebook.java
 *
 * Created on April 2, 2008, 5:24 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserFacebook {
    
    /** Creates a new instance of UserFacebook */
    public UserFacebook() {
    }
    
    private long userFacebookId;
    private long userId;
    private long facebookUserId;
    private String primaryPhoto;
    private String photosString;
    private String friendsString;
    private Date dateUpdated;

    public long getUserFacebookId() {
        return userFacebookId;
    }

    public void setUserFacebookId(long userFacebookId) {
        this.userFacebookId = userFacebookId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(long facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public String getPrimaryPhoto() {
        return primaryPhoto;
    }

    public void setPrimaryPhoto(String primaryPhoto) {
        this.primaryPhoto = primaryPhoto;
    }

    public String getPhotosString() {
        return photosString;
    }

    public void setPhotosString(String photosString) {
        this.photosString = photosString;
    }

    public String getFriendsString() {
        return friendsString;
    }

    public void setFriendsString(String friendsString) {
        this.friendsString = friendsString;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
 
    
}
