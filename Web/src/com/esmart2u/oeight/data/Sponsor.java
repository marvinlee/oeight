/*
 * Sponsor.java
 *
 * Created on 22 September 2007, 11:53
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class Sponsor {
    
    /** Creates a new instance of Sponsor */
    public Sponsor() {
    }
    
    private long sponsorId;
    private String sponsorName;
    private String sponsorEmail;
    private String sponsorPhone;
    private Date sponsorTimestamp;
    private String backgroundFile;
    private String loadingFile;
    private String bannerFile;
    private int version;

    public long getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(long sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    public String getSponsorEmail() {
        return sponsorEmail;
    }

    public void setSponsorEmail(String sponsorEmail) {
        this.sponsorEmail = sponsorEmail;
    }

    public String getSponsorPhone() {
        return sponsorPhone;
    }

    public void setSponsorPhone(String sponsorPhone) {
        this.sponsorPhone = sponsorPhone;
    }

    public Date getSponsorTimestamp() {
        return sponsorTimestamp;
    }

    public void setSponsorTimestamp(Date sponsorTimestamp) {
        this.sponsorTimestamp = sponsorTimestamp;
    }

    public String getBackgroundFile() {
        return backgroundFile;
    }

    public void setBackgroundFile(String backgroundFile) {
        this.backgroundFile = backgroundFile;
    }

    public String getLoadingFile() {
        return loadingFile;
    }

    public void setLoadingFile(String loadingFile) {
        this.loadingFile = loadingFile;
    }

    public String getBannerFile() {
        return bannerFile;
    }

    public void setBannerFile(String bannerFile) {
        this.bannerFile = bannerFile;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    
    
}
