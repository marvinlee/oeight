/*
 * UserDetails.java
 *
 * Created on 22 September 2007, 11:28
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

/**
 *
 * @author marvin
 */
public class UserDetails {
    
    /** Creates a new instance of UserDetails */
    public UserDetails() {
    }
    
    private long userDetailsId;
    private long userId;
    private char lifeSmoker;
    private char lifeDrinker;
    private String lifeAboutMe;
    private String lifeToKnow;
    private String lifeKnowMe;
    private String life1;
    private String life2;
    private String life3;
    private String life4;
    private String life5;
    private String lifeValue1;
    private String lifeValue2;
    private String lifeValue3;
    private String lifeValue4;
    private String lifeValue5;
    private String interestActor;
    private String interestActress;
    private String interestSinger;
    private String interestMusic;
    private String interestBand;
    private String interestMovies;
    private String interestBooks;
    private String interestSports;
    private String interestWebsites;
    private String interest1;
    private String interest2;
    private String interest3;
    private String interest4;
    private String interest5;
    private String interestValue1;
    private String interestValue2;
    private String interestValue3;
    private String interestValue4;
    private String interestValue5;
    private String skillsExpert;
    private String skillsGood;
    private String skillsLearn;
    private String contactPhone;
    private String contactGoogle;
    private String contactYahoo;
    private String contactMsn;
    private String webFriendster;
    private String webHi5;
    private String webWayn;
    private String webOrkut;
    private String webFacebook;
    private String webMyspace;
    private String webReserved1;
    private String webReserved2;
    private String webReserved3;
    private String webBlog;
    private boolean webBlogFeature;
    private String web1;
    private String web2;
    private String web3;
    private String web4;
    private String web5;
    private String webValue1;
    private String webValue2;
    private String webValue3;
    private String webValue4;
    private String webValue5;
    private int version;
    
    // Join table
    private UserCountry userCountry;
    
    public long getUserDetailsId() {
        return userDetailsId;
    }

    public void setUserDetailsId(long userDetailsId) {
        this.userDetailsId = userDetailsId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public char getLifeSmoker() {
        return lifeSmoker;
    }

    public void setLifeSmoker(char lifeSmoker) {
        this.lifeSmoker = lifeSmoker;
    }

    public char getLifeDrinker() {
        return lifeDrinker;
    }

    public void setLifeDrinker(char lifeDrinker) {
        this.lifeDrinker = lifeDrinker;
    }

    public String getLifeAboutMe() {
        return lifeAboutMe;
    }

    public void setLifeAboutMe(String lifeAboutMe) {
        this.lifeAboutMe = lifeAboutMe;
    }

    public String getLifeToKnow() {
        return lifeToKnow;
    }

    public void setLifeToKnow(String lifeToKnow) {
        this.lifeToKnow = lifeToKnow;
    }

    public String getLifeKnowMe() {
        return lifeKnowMe;
    }

    public void setLifeKnowMe(String lifeKnowMe) {
        this.lifeKnowMe = lifeKnowMe;
    }

    public String getLife1() {
        return life1;
    }

    public void setLife1(String life1) {
        this.life1 = life1;
    }

    public String getLife2() {
        return life2;
    }

    public void setLife2(String life2) {
        this.life2 = life2;
    }

    public String getLife3() {
        return life3;
    }

    public void setLife3(String life3) {
        this.life3 = life3;
    }

    public String getLife4() {
        return life4;
    }

    public void setLife4(String life4) {
        this.life4 = life4;
    }

    public String getLife5() {
        return life5;
    }

    public void setLife5(String life5) {
        this.life5 = life5;
    }

    public String getLifeValue1() {
        return lifeValue1;
    }

    public void setLifeValue1(String lifeValue1) {
        this.lifeValue1 = lifeValue1;
    }

    public String getLifeValue2() {
        return lifeValue2;
    }

    public void setLifeValue2(String lifeValue2) {
        this.lifeValue2 = lifeValue2;
    }

    public String getLifeValue3() {
        return lifeValue3;
    }

    public void setLifeValue3(String lifeValue3) {
        this.lifeValue3 = lifeValue3;
    }

    public String getLifeValue4() {
        return lifeValue4;
    }

    public void setLifeValue4(String lifeValue4) {
        this.lifeValue4 = lifeValue4;
    }

    public String getLifeValue5() {
        return lifeValue5;
    }

    public void setLifeValue5(String lifeValue5) {
        this.lifeValue5 = lifeValue5;
    }

    public String getInterestActor() {
        return interestActor;
    }

    public void setInterestActor(String interestActor) {
        this.interestActor = interestActor;
    }

    public String getInterestActress() {
        return interestActress;
    }

    public void setInterestActress(String interestActress) {
        this.interestActress = interestActress;
    }

    public String getInterestSinger() {
        return interestSinger;
    }

    public void setInterestSinger(String interestSinger) {
        this.interestSinger = interestSinger;
    }

    public String getInterestMusic() {
        return interestMusic;
    }

    public void setInterestMusic(String interestMusic) {
        this.interestMusic = interestMusic;
    }

    public String getInterestBand() {
        return interestBand;
    }

    public void setInterestBand(String interestBand) {
        this.interestBand = interestBand;
    }

    public String getInterestMovies() {
        return interestMovies;
    }

    public void setInterestMovies(String interestMovies) {
        this.interestMovies = interestMovies;
    }

    public String getInterestBooks() {
        return interestBooks;
    }

    public void setInterestBooks(String interestBooks) {
        this.interestBooks = interestBooks;
    }

    public String getInterestSports() {
        return interestSports;
    }

    public void setInterestSports(String interestSports) {
        this.interestSports = interestSports;
    }

    public String getInterestWebsites() {
        return interestWebsites;
    }

    public void setInterestWebsites(String interestWebsites) {
        this.interestWebsites = interestWebsites;
    }

    public String getInterest1() {
        return interest1;
    }

    public void setInterest1(String interest1) {
        this.interest1 = interest1;
    }

    public String getInterest2() {
        return interest2;
    }

    public void setInterest2(String interest2) {
        this.interest2 = interest2;
    }

    public String getInterest3() {
        return interest3;
    }

    public void setInterest3(String interest3) {
        this.interest3 = interest3;
    }

    public String getInterest4() {
        return interest4;
    }

    public void setInterest4(String interest4) {
        this.interest4 = interest4;
    }

    public String getInterest5() {
        return interest5;
    }

    public void setInterest5(String interest5) {
        this.interest5 = interest5;
    }

    public String getInterestValue1() {
        return interestValue1;
    }

    public void setInterestValue1(String interestValue1) {
        this.interestValue1 = interestValue1;
    }

    public String getInterestValue2() {
        return interestValue2;
    }

    public void setInterestValue2(String interestValue2) {
        this.interestValue2 = interestValue2;
    }

    public String getInterestValue3() {
        return interestValue3;
    }

    public void setInterestValue3(String interestValue3) {
        this.interestValue3 = interestValue3;
    }

    public String getInterestValue4() {
        return interestValue4;
    }

    public void setInterestValue4(String interestValue4) {
        this.interestValue4 = interestValue4;
    }

    public String getInterestValue5() {
        return interestValue5;
    }

    public void setInterestValue5(String interestValue5) {
        this.interestValue5 = interestValue5;
    }

    public String getSkillsExpert() {
        return skillsExpert;
    }

    public void setSkillsExpert(String skillsExpert) {
        this.skillsExpert = skillsExpert;
    }

    public String getSkillsGood() {
        return skillsGood;
    }

    public void setSkillsGood(String skillsGood) {
        this.skillsGood = skillsGood;
    }

    public String getSkillsLearn() {
        return skillsLearn;
    }

    public void setSkillsLearn(String skillsLearn) {
        this.skillsLearn = skillsLearn;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactGoogle() {
        return contactGoogle;
    }

    public void setContactGoogle(String contactGoogle) {
        this.contactGoogle = contactGoogle;
    }

    public String getContactYahoo() {
        return contactYahoo;
    }

    public void setContactYahoo(String contactYahoo) {
        this.contactYahoo = contactYahoo;
    }

    public String getContactMsn() {
        return contactMsn;
    }

    public void setContactMsn(String contactMsn) {
        this.contactMsn = contactMsn;
    }

    public String getWebFriendster() {
        return webFriendster;
    }

    public void setWebFriendster(String webFriendster) {
        this.webFriendster = webFriendster;
    }

    public String getWebHi5() {
        return webHi5;
    }

    public void setWebHi5(String webHi5) {
        this.webHi5 = webHi5;
    }

    public String getWebWayn() {
        return webWayn;
    }

    public void setWebWayn(String webWayn) {
        this.webWayn = webWayn;
    }

    public String getWebOrkut() {
        return webOrkut;
    }

    public void setWebOrkut(String webOrkut) {
        this.webOrkut = webOrkut;
    }

    public String getWebFacebook() {
        return webFacebook;
    }

    public void setWebFacebook(String webFacebook) {
        this.webFacebook = webFacebook;
    }

    public String getWebMyspace() {
        return webMyspace;
    }

    public void setWebMyspace(String webMyspace) {
        this.webMyspace = webMyspace;
    }

    public String getWebReserved1() {
        return webReserved1;
    }

    public void setWebReserved1(String webReserved1) {
        this.webReserved1 = webReserved1;
    }

    public String getWebReserved2() {
        return webReserved2;
    }

    public void setWebReserved2(String webReserved2) {
        this.webReserved2 = webReserved2;
    }

    public String getWebReserved3() {
        return webReserved3;
    }

    public void setWebReserved3(String webReserved3) {
        this.webReserved3 = webReserved3;
    }
    
    public String getWebBlog() {
        return webBlog;
    }

    public void setWebBlog(String webBlog) {
        this.webBlog = webBlog;
    }

    public boolean isWebBlogFeature() {
        return webBlogFeature;
    }
    
    public boolean getWebBlogFeature() {
        return webBlogFeature;
    }

    public void setWebBlogFeature(boolean webBlogFeature) {
        this.webBlogFeature = webBlogFeature;
    }
    
    public String getWeb1() {
        return web1;
    }

    public void setWeb1(String web1) {
        this.web1 = web1;
    }

    public String getWeb2() {
        return web2;
    }

    public void setWeb2(String web2) {
        this.web2 = web2;
    }

    public String getWeb3() {
        return web3;
    }

    public void setWeb3(String web3) {
        this.web3 = web3;
    }

    public String getWeb4() {
        return web4;
    }

    public void setWeb4(String web4) {
        this.web4 = web4;
    }

    public String getWeb5() {
        return web5;
    }

    public void setWeb5(String web5) {
        this.web5 = web5;
    }

    public String getWebValue1() {
        return webValue1;
    }

    public void setWebValue1(String webValue1) {
        this.webValue1 = webValue1;
    }

    public String getWebValue2() {
        return webValue2;
    }

    public void setWebValue2(String webValue2) {
        this.webValue2 = webValue2;
    }

    public String getWebValue3() {
        return webValue3;
    }

    public void setWebValue3(String webValue3) {
        this.webValue3 = webValue3;
    }

    public String getWebValue4() {
        return webValue4;
    }

    public void setWebValue4(String webValue4) {
        this.webValue4 = webValue4;
    }

    public String getWebValue5() {
        return webValue5;
    }

    public void setWebValue5(String webValue5) {
        this.webValue5 = webValue5;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    } 

    public UserCountry getUserCountry() {
        if (userCountry == null)
        {
            userCountry = new UserCountry();
        }
        return userCountry;
    }

    public void setUserCountry(UserCountry userCountry) {
        this.userCountry = userCountry;
    }


    
    
    
}
