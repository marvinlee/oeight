/*
 * EmailUpdate.java
 *
 * Created on April 11, 2008, 5:07 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import com.esmart2u.oeight.member.helper.OEightConstants;
import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class EmailUpdate {
    
    private long emailUpdateId;
    private long userId;
    private String emailAddress;
    private int emailUpdateCode;
    private Date dateEffective;
    private char status = OEightConstants.MAIL_STATUS_NEW;
    private String customField1;
    private String customField2;
    private String customField3;
    private String customField4;
    private String customField5;
    private Date dateEmailed;
    private int totalSent;
    
    /** Creates a new instance of EmailUpdate */
    public EmailUpdate() {
    }

    public long getEmailUpdateId() {
        return emailUpdateId;
    }

    public void setEmailUpdateId(long emailUpdateId) {
        this.emailUpdateId = emailUpdateId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getEmailUpdateCode() {
        return emailUpdateCode;
    }

    public void setEmailUpdateCode(int emailUpdateCode) {
        this.emailUpdateCode = emailUpdateCode;
    }

    public Date getDateEffective() {
        return dateEffective;
    }

    public void setDateEffective(Date dateEffective) {
        this.dateEffective = dateEffective;
    }

    /**
     *  Only available status are SENT, SEND_FAIL, NEW
     */
    public char getStatus() { 
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public String getCustomField1() {
        return customField1;
    }

    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }

    public String getCustomField2() {
        return customField2;
    }

    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }

    public String getCustomField3() {
        return customField3;
    }

    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }

    public String getCustomField4() {
        return customField4;
    }

    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }

    public String getCustomField5() {
        return customField5;
    }

    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }

    public Date getDateEmailed() {
        return dateEmailed;
    }

    public void setDateEmailed(Date dateEmailed) {
        this.dateEmailed = dateEmailed;
    }

    public int getTotalSent() {
        return totalSent;
    }

    public void setTotalSent(int totalSent) {
        this.totalSent = totalSent;
    }
    
}
