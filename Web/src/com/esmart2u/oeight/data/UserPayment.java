/*
 * UserPayment.java
 *
 * Created on 22 September 2007, 11:41
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class UserPayment {
    
    /** Creates a new instance of UserPayment */
    public UserPayment() {
    }
    
    private long userId;
    private long userPaymentId;
    private long productId;
    private int quantity; // Cannot more than 100
    private double orderPrice;
    private char ordered;
    private Date orderTimestamp;
    private char paid;
    private Date paidTimestamp;
    private String paidRemarks;
    private char delivered;
    private Date deliveredTimestamp;
    private String deliveredId;
    private String deliveredTracking;
    private char cancelled;
    private Date cancelledTimestamp;
    private char isContest;
    private int version;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUserPaymentId() {
        return userPaymentId;
    }

    public void setUserPaymentId(long userPaymentId) {
        this.userPaymentId = userPaymentId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(double orderPrice) {
        this.orderPrice = orderPrice;
    }

    public char getOrdered() {
        return ordered;
    }

    public void setOrdered(char ordered) {
        this.ordered = ordered;
    }

    public Date getOrderTimestamp() {
        return orderTimestamp;
    }

    public void setOrderTimestamp(Date orderTimestamp) {
        this.orderTimestamp = orderTimestamp;
    }

    public char getPaid() {
        return paid;
    }

    public void setPaid(char paid) {
        this.paid = paid;
    }

    public Date getPaidTimestamp() {
        return paidTimestamp;
    }

    public void setPaidTimestamp(Date paidTimestamp) {
        this.paidTimestamp = paidTimestamp;
    }

    public String getPaidRemarks() {
        return paidRemarks;
    }

    public void setPaidRemarks(String paidRemarks) {
        this.paidRemarks = paidRemarks;
    }

    public char getDelivered() {
        return delivered;
    }

    public void setDelivered(char delivered) {
        this.delivered = delivered;
    }

    public Date getDeliveredTimestamp() {
        return deliveredTimestamp;
    }

    public void setDeliveredTimestamp(Date deliveredTimestamp) {
        this.deliveredTimestamp = deliveredTimestamp;
    }

    public String getDeliveredId() {
        return deliveredId;
    }

    public void setDeliveredId(String deliveredId) {
        this.deliveredId = deliveredId;
    }

    public String getDeliveredTracking() {
        return deliveredTracking;
    }

    public void setDeliveredTracking(String deliveredTracking) {
        this.deliveredTracking = deliveredTracking;
    }

    public char getCancelled() {
        return cancelled;
    }

    public void setCancelled(char cancelled) {
        this.cancelled = cancelled;
    }

    public Date getCancelledTimestamp() {
        return cancelledTimestamp;
    }

    public void setCancelledTimestamp(Date cancelledTimestamp) {
        this.cancelledTimestamp = cancelledTimestamp;
    }

    public char getIsContest() {
        return isContest;
    }

    public void setIsContest(char isContest) {
        this.isContest = isContest;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    

    
    
    
}
