/*
 * OEightWedding.java
 *
 * Created on June 22, 2008, 4:20 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class OEightWedding {
    
    /** Creates a new instance of OEightWedding */
    public OEightWedding() {
    }
    private long weddingId;
    private long userId;
    private String coupleName;
    private String weddingDescription;
    private String photoSmallPath;
    private String photoLargePath;
    private String video1;
    private String video2;
    private String video3;
    private String video4;
    private String video5;
    private String video6;
    private String video7;
    private String video8; 
    private char status;
    private Date dateRegistered;
    private Date dateUpdated;
    private char allowComments; // Allow, Moderated, No
    private String weddingUrl; 

    public long getWeddingId() {
        return weddingId;
    }

    public void setWeddingId(long weddingId) {
        this.weddingId = weddingId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getCoupleName() {
        return coupleName;
    }

    public void setCoupleName(String coupleName) {
        this.coupleName = coupleName;
    }

    public String getWeddingDescription() {
        return weddingDescription;
    }

    public void setWeddingDescription(String weddingDescription) {
        this.weddingDescription = weddingDescription;
    }

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }

    public String getPhotoLargePath() {
        return photoLargePath;
    }

    public void setPhotoLargePath(String photoLargePath) {
        this.photoLargePath = photoLargePath;
    }

    public String getVideo1() {
        return video1;
    }

    public void setVideo1(String video1) {
        this.video1 = video1;
    }

    public String getVideo2() {
        return video2;
    }

    public void setVideo2(String video2) {
        this.video2 = video2;
    }

    public String getVideo3() {
        return video3;
    }

    public void setVideo3(String video3) {
        this.video3 = video3;
    }

    public String getVideo4() {
        return video4;
    }

    public void setVideo4(String video4) {
        this.video4 = video4;
    }

    public String getVideo5() {
        return video5;
    }

    public void setVideo5(String video5) {
        this.video5 = video5;
    }

    public String getVideo6() {
        return video6;
    }

    public void setVideo6(String video6) {
        this.video6 = video6;
    }

    public String getVideo7() {
        return video7;
    }

    public void setVideo7(String video7) {
        this.video7 = video7;
    }

    public String getVideo8() {
        return video8;
    }

    public void setVideo8(String video8) {
        this.video8 = video8;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(Date dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    public char getAllowComments() {
        return allowComments;
    }

    public void setAllowComments(char allowComments) {
        this.allowComments = allowComments;
    }

    public String getWeddingUrl() {
        return weddingUrl;
    }

    public void setWeddingUrl(String weddingUrl) {
        this.weddingUrl = weddingUrl;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

}
