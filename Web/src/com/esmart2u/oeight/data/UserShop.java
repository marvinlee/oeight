/*
 * UserShop.java
 *
 * Created on February 13, 2008, 2:07 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserShop {
    
    private long userShopId;
    private long userId;
    private String shopCode;
    private String shopDescription;
    private long shopReferenceNo;
    private String name;
    private String contactNo;
    private String email;
    private String address;
    private String shopStatus;
    private String trackingNo;
    private Date dateUpdated;
    private long version;
    
    
    
    
    /** Creates a new instance of UserShop */
    public UserShop() {
    }

    public long getUserShopId() {
        return userShopId;
    }

    public void setUserShopId(long userShopId) {
        this.userShopId = userShopId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopDescription() {
        return shopDescription;
    }

    public void setShopDescription(String shopDescription) {
        this.shopDescription = shopDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(String shopStatus) {
        this.shopStatus = shopStatus;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public long getShopReferenceNo() {
        return shopReferenceNo;
    }

    public void setShopReferenceNo(long shopReferenceNo) {
        this.shopReferenceNo = shopReferenceNo;
    }

    public String getTrackingNo() {
        return trackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        this.trackingNo = trackingNo;
    }

    
}
