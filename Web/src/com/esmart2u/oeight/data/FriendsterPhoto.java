/*
 * FriendsterPhoto.java
 *
 * Created on March 31, 2008, 4:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterPhoto {
    
    private long friendsterPhotoId;
    private long friendsterUserId;
    private long photoId;
    private String photoUrl;
    private String photoLargeUrl;
    private String photoCaption;
    private Date dateUpdated;
    
    /** Creates a new instance of FriendsterPhoto */
    public FriendsterPhoto() {
    }

    public long getFriendsterPhotoId() {
        return friendsterPhotoId;
    }

    public void setFriendsterPhotoId(long friendsterPhotoId) {
        this.friendsterPhotoId = friendsterPhotoId;
    }

    public long getFriendsterUserId() {
        return friendsterUserId;
    }

    public void setFriendsterUserId(long friendsterUserId) {
        this.friendsterUserId = friendsterUserId;
    }

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
    
    public String getPhotoLargeUrl() {
        return photoLargeUrl;
    }

    public void setPhotoLargeUrl(String photoLargeUrl) {
        this.photoLargeUrl = photoLargeUrl;
    }

    public String getPhotoCaption() {
        return photoCaption;
    }

    public void setPhotoCaption(String photoCaption) {
        this.photoCaption = photoCaption;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
    
}
