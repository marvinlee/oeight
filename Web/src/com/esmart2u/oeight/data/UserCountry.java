/*
 * UserCountry.java
 *
 * Created on 22 September 2007, 11:23
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import com.esmart2u.solution.base.helper.PropertyConstants;
import java.util.Date;

/**
 *
 * @author marvin
 */
public class UserCountry {
    
    /** Creates a new instance of UserCountry */
    public UserCountry() {
    }
    
    private long userId;
    private String name;
    private String nric;
    private char gender;
    private String maritalStatus;
    private Date birthDate;
    private String city;
    private String state;
    private String nationality;
    private String currentLocation;
    private String schools;
    private String occupation;
    private String company;
    private String publicEmail;
    private String photoSmallPath;
    private String photoLargePath;
    private String photoDescription;
    private String flickrId;
    private String yahooId;
    private String myWish;
    private String userType;
    private char systemStatus;
    private Date systemRegistered;
    private char reportAbuse;
    private int profilePreference;
    private int version;
    private boolean hasMessage;
    private char subscribe;
    private char validated;
    private boolean hasBuddyRequest;

    // For join
    private User user;
    private UserDetails userDetails;
    
    // For join select
    //private String userName;
    
    // For search 
    private int fromAge;
    private int toAge; 
    private String searchLifestyle;
    private String searchInterest;
    private String searchSkills;
    
    private int currentPage;
    private int totalPage;
    
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNric() {
        return nric;
    }

    public void setNric(String nric) {
        this.nric = nric;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getSchools() {
        return schools;
    }

    public void setSchools(String schools) {
        this.schools = schools;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPublicEmail() {
        return publicEmail;
    }

    public void setPublicEmail(String publicEmail) {
        this.publicEmail = publicEmail;
    }

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }

    public String getPhotoLargePath() {
        return photoLargePath;
    }

    public void setPhotoLargePath(String photoLargePath) {
        this.photoLargePath = photoLargePath;
    }
    
    public String getPhotoDescription() {
        return photoDescription;
    }

    public void setPhotoDescription(String photoDescription) {
        this.photoDescription = photoDescription;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public char getSystemStatus() {
        return systemStatus;
    }

    public void setSystemStatus(char systemStatus) {
        this.systemStatus = systemStatus;
    }

    public Date getSystemRegistered() {
        return systemRegistered;
    }

    public void setSystemRegistered(Date systemRegistered) {
        this.systemRegistered = systemRegistered;
    }

    public char getReportAbuse() {
        return reportAbuse;
    }

    public void setReportAbuse(char reportAbuse) {
        this.reportAbuse = reportAbuse;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    } 

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }
    
    /*
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }*/

    public int getFromAge() {
        return fromAge;
    }

    public void setFromAge(int fromAge) {
        this.fromAge = fromAge;
    }

    public int getToAge() {
        return toAge;
    }

    public void setToAge(int toAge) {
        this.toAge = toAge;
    }

    public String getSearchLifestyle() {
        return searchLifestyle;
    }

    public void setSearchLifestyle(String searchLifestyle) {
        this.searchLifestyle = searchLifestyle;
    }

    public String getSearchInterest() {
        return searchInterest;
    }

    public void setSearchInterest(String searchInterest) {
        this.searchInterest = searchInterest;
    }

    public String getSearchSkills() {
        return searchSkills;
    }

    public void setSearchSkills(String searchSkills) {
        this.searchSkills = searchSkills;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public String getFlickrId() {
        return flickrId;
    }

    public void setFlickrId(String flickrId) {
        this.flickrId = flickrId;
    }

    public String getYahooId() {
        return yahooId;
    }

    public void setYahooId(String yahooId) {
        this.yahooId = yahooId;
    }

    public int getProfilePreference() {
        return profilePreference;
    }

    public void setProfilePreference(int profilePreference) {
        this.profilePreference = profilePreference;
    } 

    public String getMyWish() {
        return myWish;
    }

    public void setMyWish(String myWish) {
        this.myWish = myWish;
    }

    public boolean getHasMessage() {
        return isHasMessage();
    }

    public boolean isHasMessage() {
        return hasMessage;
    }

    public void setHasMessage(boolean hasMessage) {
        this.hasMessage = hasMessage;
    }

    public char getSubscribe() {
        if (subscribe != PropertyConstants.BOOLEAN_NO)
        {
            subscribe = PropertyConstants.BOOLEAN_YES;
        }
        return subscribe;
    }

    public void setSubscribe(char subscribe) {
        if (subscribe != PropertyConstants.BOOLEAN_NO)
        {
            subscribe = PropertyConstants.BOOLEAN_YES;
        }
        this.subscribe = subscribe;
    }

    public char getValidated() {
        if (validated != PropertyConstants.BOOLEAN_YES)
        {
            validated = PropertyConstants.BOOLEAN_NO;
        }
        return validated;
    }

    public void setValidated(char validated) {
        if (validated != PropertyConstants.BOOLEAN_YES)
        {
            validated = PropertyConstants.BOOLEAN_NO;
        }
        this.validated = validated;
    }

    public boolean getHasBuddyRequest() {
        return isHasBuddyRequest();
    }
    public boolean isHasBuddyRequest() {
        return hasBuddyRequest;
    }

    public void setHasBuddyRequest(boolean hasBuddyRequest) {
        this.hasBuddyRequest = hasBuddyRequest;
    }
 

}
