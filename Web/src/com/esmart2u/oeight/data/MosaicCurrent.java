/*
 * MosaicCurrent.java
 *
 * Created on 22 September 2007, 12:07
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List; 

/**
 *
 * @author marvin
 */
public class MosaicCurrent {
    
    /** Creates a new instance of MosaicCurrent */
    public MosaicCurrent() {
    }
  
    private long mosaicId;
    private Date dateRun;
    private char uploaded;
    private long mosaicForeignId;
    
    private int totalMaps;

    private List mosaics;
    
    public long getMosaicId() {
        return mosaicId;
    }

    public void setMosaicId(long mosaicId) {
        this.mosaicId = mosaicId;
    }

    public long getMosaicForeignId() {
        return mosaicForeignId;
    }

    public void setMosaicForeignId(long mosaicForeignId) {
        this.mosaicForeignId = mosaicForeignId;
    }

    public Date getDateRun() {
        return dateRun;
    }

    public void setDateRun(Date dateRun) {
        this.dateRun = dateRun;
    }

    public char getUploaded() {
        return uploaded;
    }

    public void setUploaded(char uploaded) {
        this.uploaded = uploaded;
    }

    public int getTotalMaps() {
        return totalMaps;
    }

    public void setTotalMaps(int totalMaps) {
        this.totalMaps = totalMaps;
    }
    
    public List getMosaics() {
        return mosaics;
    }

    public void setMosaics(List mosaics) {
        this.mosaics = mosaics;
    }
  
    
}
