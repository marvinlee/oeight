/*
 * OEightPledge.java
 *
 * Created on May 6, 2008, 5:18 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class OEightPledge { 

    private long pledgeId;
    private long userId;
    private String userName;
    private String pledgerName;
    private Date pledgeDate;
    private boolean memberFlag;
    private int pledgeCode;
    private int pledgeFrom;
    private boolean showPledgeFrom;
    private long pledgeFromUserId;
    
    /** Creates a new instance of OEightPledge */
    public OEightPledge() {
    }

    public long getPledgeId() {
        return pledgeId;
    }

    public void setPledgeId(long pledgeId) {
        this.pledgeId = pledgeId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPledgerName() {
        return pledgerName;
    }

    public void setPledgerName(String pledgerName) {
        this.pledgerName = pledgerName;
    }

    public Date getPledgeDate() {
        return pledgeDate;
    }

    public void setPledgeDate(Date pledgeDate) {
        this.pledgeDate = pledgeDate;
    }

    public boolean isMemberFlag() {
        return memberFlag;
    }

    public void setMemberFlag(boolean memberFlag) {
        this.memberFlag = memberFlag;
    }

    public int getPledgeCode() {
        return pledgeCode;
    }

    public void setPledgeCode(int pledgeCode) {
        this.pledgeCode = pledgeCode;
    }

    public int getPledgeFrom() {
        return pledgeFrom;
    }

    public void setPledgeFrom(int pledgeFrom) {
        this.pledgeFrom = pledgeFrom;
    }

    public boolean getShowPledgeFrom() {
        return isShowPledgeFrom();
    }
    
    public boolean isShowPledgeFrom() {
        return showPledgeFrom;
    }

    public void setShowPledgeFrom(boolean showPledgeFrom) {
        this.showPledgeFrom = showPledgeFrom;
    }

    public long getPledgeFromUserId() {
        return pledgeFromUserId;
    }

    public void setPledgeFromUserId(long pledgeFromUserId) {
        this.pledgeFromUserId = pledgeFromUserId;
    } 

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    
}
