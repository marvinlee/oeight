/*
 * Login.java
 *
 * Created on 22 September 2007, 11:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class Login {
    
    private long loginSessionId;
    private String loginEmail;
    private char loginSuccessFlag;
    private long userId;
    private Date loginTime;
    private Date logoutTime;
    private String ipAddress;
    private String referrer;
    
    /** Creates a new instance of Login */
    public Login() {
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }

    public char getLoginSuccessFlag() {
        return loginSuccessFlag;
    }

    public void setLoginSuccessFlag(char loginSuccessFlag) {
        this.loginSuccessFlag = loginSuccessFlag;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public long getLoginSessionId() {
        return loginSessionId;
    }

    public void setLoginSessionId(long loginSessionId) {
        this.loginSessionId = loginSessionId;
    }

    public Date getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    
    
}
