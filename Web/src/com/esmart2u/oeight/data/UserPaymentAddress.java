/*
 * UserPaymentAddress.java
 *
 * Created on 22 September 2007, 11:45
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class UserPaymentAddress {
    
    /** Creates a new instance of UserPaymentAddress */
    public UserPaymentAddress() {
    }
    
    private long userPaymentId;
    private String address;
    private String city;
    private String state;
    private String postcode;
    private Date dateUpdated;
    private int version;

    public long getUserPaymentId() {
        return userPaymentId;
    }

    public void setUserPaymentId(long userPaymentId) {
        this.userPaymentId = userPaymentId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    

}
