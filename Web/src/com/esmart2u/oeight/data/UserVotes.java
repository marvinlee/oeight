/*
 * UserVotes.java
 *
 * Created on October 30, 2007, 12:50 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserVotes implements Serializable{
    
    private long userId;
    private boolean votedMyself;
    private int votedToday;
    private long totalVotes;
    private Date voteDate;
           
            
    /** Creates a new instance of UserVotes */
    public UserVotes() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public boolean isVotedMyself() {
        return votedMyself;
    }
    
    public boolean getVotedMyself() {
        return votedMyself;
    }

    public void setVotedMyself(boolean votedMyself) {
        this.votedMyself = votedMyself;
    }

    public int getVotedToday() {
        return votedToday;
    }

    public void setVotedToday(int votedToday) {
        this.votedToday = votedToday;
    }

    public long getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(long totalVotes) {
        this.totalVotes = totalVotes;
    }

    public Date getVoteDate() {
        return voteDate;
    }

    public void setVoteDate(Date voteDate) {
        this.voteDate = voteDate;
    }
    
}
