/*
 * EmailsArchive.java
 *
 * Created on May 30, 2008, 11:39 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

/**
 *
 * @author meauchyuan.lee
 */
public class EmailsArchive {
    
    private long emailsArchiveId;
    private String emailAddress;
    private String contactList;
    /** Creates a new instance of EmailsArchive */
    public EmailsArchive() {
    }

    public long getEmailsArchiveId() {
        return emailsArchiveId;
    }

    public void setEmailsArchiveId(long emailsArchiveId) {
        this.emailsArchiveId = emailsArchiveId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getContactList() {
        return contactList;
    }

    public void setContactList(String contactList) {
        this.contactList = contactList;
    }
    
    
}
