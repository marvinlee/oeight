/*
 * PhpBbUserGroup.java
 *
 * Created on August 14, 2008, 3:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data.phpbb;

import java.io.Serializable;

/**
 *
 * @author meauchyuan.lee
 */
public class PhpBbUserGroup implements Serializable{
    
    private long groupId;
    private long userId;
    private int groupLeader;
    private int userPending;
    
    /** Creates a new instance of PhpBbUserGroup */
    public PhpBbUserGroup() {
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getGroupLeader() {
        return groupLeader;
    }

    public void setGroupLeader(int groupLeader) {
        this.groupLeader = groupLeader;
    }

    public int getUserPending() {
        return userPending;
    }

    public void setUserPending(int userPending) {
        this.userPending = userPending;
    }
    
}
