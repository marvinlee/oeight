/*
 * PhpBbTopics.java
 *
 * Created on October 18, 2008, 1:10 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data.phpbb;

/**
 *
 * @author meauchyuan.lee
 */
public class PhpBbTopics {
    
    private long topicId;
    private long forumId;
    private String topicTitle;
    private long topicTime;
    private String topicFirstPosterName;
    
    /**
     * Creates a new instance of PhpBbTopics
     */
    public PhpBbTopics() {
    }

    public long getTopicId() {
        return topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public long getForumId() {
        return forumId;
    }

    public void setForumId(long forumId) {
        this.forumId = forumId;
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    public long getTopicTime() {
        return topicTime;
    }

    public void setTopicTime(long topicTime) {
        this.topicTime = topicTime;
    }

    public String getTopicFirstPosterName() {
        return topicFirstPosterName;
    }

    public void setTopicFirstPosterName(String topicFirstPosterName) {
        this.topicFirstPosterName = topicFirstPosterName;
    }
    
    
    
}
