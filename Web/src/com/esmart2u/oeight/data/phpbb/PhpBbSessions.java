/*
 * PhpBbSessions.java
 *
 * Created on August 14, 2008, 3:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data.phpbb;

/**
 *
 * @author meauchyuan.lee
 */
public class PhpBbSessions {
    
    private String sessionId;
    private long sessionUserId;
    private long sessionForumId;
    private long sessionLastVisit;
    private long sessionStart;
    private long sessionTime;
    private String sessionIp;
    private String sessionBrowser;
    private String sessionForwardedFor;
    private String sessionPage;
    private int sessionViewOnline;
    private int sessionAutoLogin;
    private int sessionAdmin; 
   
    
    /** Creates a new instance of PhpBbSessions */
    public PhpBbSessions() {
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getSessionUserId() {
        return sessionUserId;
    }

    public void setSessionUserId(long sessionUserId) {
        this.sessionUserId = sessionUserId;
    }

    public long getSessionForumId() {
        return sessionForumId;
    }

    public void setSessionForumId(long sessionForumId) {
        this.sessionForumId = sessionForumId;
    }

    public long getSessionLastVisit() {
        return sessionLastVisit;
    }

    public void setSessionLastVisit(long sessionLastVisit) {
        this.sessionLastVisit = sessionLastVisit;
    }

    public long getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(long sessionStart) {
        this.sessionStart = sessionStart;
    }

    public long getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(long sessionTime) {
        this.sessionTime = sessionTime;
    }

    public String getSessionIp() {
        return sessionIp;
    }

    public void setSessionIp(String sessionIp) {
        this.sessionIp = sessionIp;
    }

    public String getSessionForwardedFor() {
        return sessionForwardedFor;
    }

    public void setSessionForwardedFor(String sessionForwardedFor) {
        this.sessionForwardedFor = sessionForwardedFor;
    }

    public String getSessionPage() {
        return sessionPage;
    }

    public void setSessionPage(String sessionPage) {
        this.sessionPage = sessionPage;
    }

    public int getSessionViewOnline() {
        return sessionViewOnline;
    }

    public void setSessionViewOnline(int sessionViewOnline) {
        this.sessionViewOnline = sessionViewOnline;
    }

    public int getSessionAutoLogin() {
        return sessionAutoLogin;
    }

    public void setSessionAutoLogin(int sessionAutoLogin) {
        this.sessionAutoLogin = sessionAutoLogin;
    }

    public int getSessionAdmin() {
        return sessionAdmin;
    }

    public void setSessionAdmin(int sessionAdmin) {
        this.sessionAdmin = sessionAdmin;
    }

    public String getSessionBrowser() {
        return sessionBrowser;
    }

    public void setSessionBrowser(String sessionBrowser) {
        this.sessionBrowser = sessionBrowser;
    }
    
}
