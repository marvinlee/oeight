/*
 * PhpBbUsers.java
 *
 * Created on August 14, 2008, 3:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data.phpbb;

import com.esmart2u.solution.base.helper.BeanUtils;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author meauchyuan.lee
 */
public class PhpBbUsers {
    
    private long userId;
    private int userType;
    private int groupId;
    private String userPermissions;
    private int userPermissionsFrom;
    private String userIp;
    private long userRegisteredDate;
    private String userName;
    private String userNameClean;
    private String userPassword;
    private long userPasswordChange; 
    private int userPasswordConvert;
    private String userEmail;
    private BigInteger userEmailHash;
    private String userBirthday;
    private long userLastVisit;
    private long userLastMark;
    private long userLastPostTime;
    private String userLastPage;
    private String userLastConfirmKey;
    private long userLastSearch;
    private int userWarnings;
    private long userLastWarning;
    private int userLoginAttempts;
    private long userInactiveReason;
    private long userInactiveTime;
    private long userPosts;
    private String userLanguage;
    private BigDecimal userTimezone;
    private int userDst;
    private String userDateFormat;
    private int userStyle;
    private long userRank;
    private String userColour;
    private int userNewPrivateMessage;
    private int userUnreadPrivateMessage;
    private long userLastPrivateMessage;
    private int userMessageRules;
    private long userFullFolder;
    private long userEmailTime;
    private int userTopicShowDays;
    private String userTopicSortbyType;
    private String userTopicSortbyDir;
    private int userPostShowDays;
    private String userPostSortbyType;
    private String userPostSortbyDir;
    private int userNotify;
    private int userNotifyPm;
    private int userNotifyType;
    private int userAllowPm;
    private int userAllowViewOnline;
    private int userAllowViewEmail;
    private int userAllowMassEmail;
    private long userOptions;
    private String userAvatar;
    private int userAvatarType;
    private int userAvatarWidth;
    private int userAvatarHeight;
    private String userSignature;
    private String userSignatureBBCodeUid;
    private String userSignatureBBCodeBitField;
    private String userFrom;
    private String userIcq;
    private String userAim;
    private String userYim;
    private String userMsnm;
    private String userJabber;
    private String userWebsite;
    private String userOcc;
    private String userInterests;
    private String userActKey;
    private String userNewPassword;
    private String userFormSalt;
            
     
    
    /** Creates a new instance of PhpBbUsers */
    public PhpBbUsers() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getUserPermissions() {
        return userPermissions;
    }

    public void setUserPermissions(String userPermissions) {
        this.userPermissions = userPermissions;
    }

    public int getUserPermissionsFrom() {
        return userPermissionsFrom;
    }

    public void setUserPermissionsFrom(int userPermissionsFrom) {
        this.userPermissionsFrom = userPermissionsFrom;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public long getUserRegisteredDate() {
        return userRegisteredDate;
    }

    public void setUserRegisteredDate(long userRegisteredDate) {
        this.userRegisteredDate = userRegisteredDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNameClean() {
        return userNameClean;
    }

    public void setUserNameClean(String userNameClean) {
        this.userNameClean = userNameClean;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public long getUserPasswordChange() {
        return userPasswordChange;
    }

    public void setUserPasswordChange(long userPasswordChange) {
        this.userPasswordChange = userPasswordChange;
    }

    public int getUserPasswordConvert() {
        return userPasswordConvert;
    }

    public void setUserPasswordConvert(int userPasswordConvert) {
        this.userPasswordConvert = userPasswordConvert;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public BigInteger getUserEmailHash() {
        return userEmailHash;
    }

    public void setUserEmailHash(BigInteger userEmailHash) {
        this.userEmailHash = userEmailHash;
    }

    public String getUserBirthday() {
        return userBirthday;
    }

    public void setUserBirthday(String userBirthday) {
        this.userBirthday = userBirthday;
    }

    public long getUserLastVisit() {
        return userLastVisit;
    }

    public void setUserLastVisit(long userLastVisit) {
        this.userLastVisit = userLastVisit;
    }

    public long getUserLastMark() {
        return userLastMark;
    }

    public void setUserLastMark(long userLastMark) {
        this.userLastMark = userLastMark;
    }

    public long getUserLastPostTime() {
        return userLastPostTime;
    }

    public void setUserLastPostTime(long userLastPostTime) {
        this.userLastPostTime = userLastPostTime;
    }

    public String getUserLastPage() {
        return userLastPage;
    }

    public void setUserLastPage(String userLastPage) {
        this.userLastPage = userLastPage;
    }

    public String getUserLastConfirmKey() {
        return userLastConfirmKey;
    }

    public void setUserLastConfirmKey(String userLastConfirmKey) {
        this.userLastConfirmKey = userLastConfirmKey;
    }

    public long getUserLastSearch() {
        return userLastSearch;
    }

    public void setUserLastSearch(long userLastSearch) {
        this.userLastSearch = userLastSearch;
    }

    public int getUserWarnings() {
        return userWarnings;
    }

    public void setUserWarnings(int userWarnings) {
        this.userWarnings = userWarnings;
    }

    public long getUserLastWarning() {
        return userLastWarning;
    }

    public void setUserLastWarning(long userLastWarning) {
        this.userLastWarning = userLastWarning;
    }

    public int getUserLoginAttempts() {
        return userLoginAttempts;
    }

    public void setUserLoginAttempts(int userLoginAttempts) {
        this.userLoginAttempts = userLoginAttempts;
    }

    public long getUserInactiveReason() {
        return userInactiveReason;
    }

    public void setUserInactiveReason(long userInactiveReason) {
        this.userInactiveReason = userInactiveReason;
    }

    public long getUserInactiveTime() {
        return userInactiveTime;
    }

    public void setUserInactiveTime(long userInactiveTime) {
        this.userInactiveTime = userInactiveTime;
    }

    public long getUserPosts() {
        return userPosts;
    }

    public void setUserPosts(long userPosts) {
        this.userPosts = userPosts;
    }

    public String getUserLanguage() {
        return userLanguage;
    }

    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }

    public BigDecimal getUserTimezone() {
        return userTimezone;
    }

    public void setUserTimezone(BigDecimal userTimezone) {
        this.userTimezone = userTimezone;
    }

    public int getUserDst() {
        return userDst;
    }

    public void setUserDst(int userDst) {
        this.userDst = userDst;
    }

    public String getUserDateFormat() {
        return userDateFormat;
    }

    public void setUserDateFormat(String userDateFormat) {
        this.userDateFormat = userDateFormat;
    }

    public int getUserStyle() {
        return userStyle;
    }

    public void setUserStyle(int userStyle) {
        this.userStyle = userStyle;
    }

    public long getUserRank() {
        return userRank;
    }

    public void setUserRank(long userRank) {
        this.userRank = userRank;
    }

    public String getUserColour() {
        return userColour;
    }

    public void setUserColour(String userColour) {
        this.userColour = userColour;
    }

    public int getUserNewPrivateMessage() {
        return userNewPrivateMessage;
    }

    public void setUserNewPrivateMessage(int userNewPrivateMessage) {
        this.userNewPrivateMessage = userNewPrivateMessage;
    }

    public int getUserUnreadPrivateMessage() {
        return userUnreadPrivateMessage;
    }

    public void setUserUnreadPrivateMessage(int userUnreadPrivateMessage) {
        this.userUnreadPrivateMessage = userUnreadPrivateMessage;
    }

    public long getUserLastPrivateMessage() {
        return userLastPrivateMessage;
    }

    public void setUserLastPrivateMessage(long userLastPrivateMessage) {
        this.userLastPrivateMessage = userLastPrivateMessage;
    }

    public int getUserMessageRules() {
        return userMessageRules;
    }

    public void setUserMessageRules(int userMessageRules) {
        this.userMessageRules = userMessageRules;
    }

    public long getUserFullFolder() {
        return userFullFolder;
    }

    public void setUserFullFolder(long userFullFolder) {
        this.userFullFolder = userFullFolder;
    }

    public long getUserEmailTime() {
        return userEmailTime;
    }

    public void setUserEmailTime(long userEmailTime) {
        this.userEmailTime = userEmailTime;
    }

    public int getUserTopicShowDays() {
        return userTopicShowDays;
    }

    public void setUserTopicShowDays(int userTopicShowDays) {
        this.userTopicShowDays = userTopicShowDays;
    }

    public String getUserTopicSortbyType() {
        return userTopicSortbyType;
    }

    public void setUserTopicSortbyType(String userTopicSortbyType) {
        this.userTopicSortbyType = userTopicSortbyType;
    }

    public String getUserTopicSortbyDir() {
        return userTopicSortbyDir;
    }

    public void setUserTopicSortbyDir(String userTopicSortbyDir) {
        this.userTopicSortbyDir = userTopicSortbyDir;
    }

    public int getUserPostShowDays() {
        return userPostShowDays;
    }

    public void setUserPostShowDays(int userPostShowDays) {
        this.userPostShowDays = userPostShowDays;
    }

    public String getUserPostSortbyType() {
        return userPostSortbyType;
    }

    public void setUserPostSortbyType(String userPostSortbyType) {
        this.userPostSortbyType = userPostSortbyType;
    }

    public String getUserPostSortbyDir() {
        return userPostSortbyDir;
    }

    public void setUserPostSortbyDir(String userPostSortbyDir) {
        this.userPostSortbyDir = userPostSortbyDir;
    }

    public int getUserNotify() {
        return userNotify;
    }

    public void setUserNotify(int userNotify) {
        this.userNotify = userNotify;
    }

    public int getUserNotifyPm() {
        return userNotifyPm;
    }

    public void setUserNotifyPm(int userNotifyPm) {
        this.userNotifyPm = userNotifyPm;
    }

    public int getUserNotifyType() {
        return userNotifyType;
    }

    public void setUserNotifyType(int userNotifyType) {
        this.userNotifyType = userNotifyType;
    }

    public int getUserAllowPm() {
        return userAllowPm;
    }

    public void setUserAllowPm(int userAllowPm) {
        this.userAllowPm = userAllowPm;
    }

    public int getUserAllowViewOnline() {
        return userAllowViewOnline;
    }

    public void setUserAllowViewOnline(int userAllowViewOnline) {
        this.userAllowViewOnline = userAllowViewOnline;
    }

    public int getUserAllowViewEmail() {
        return userAllowViewEmail;
    }

    public void setUserAllowViewEmail(int userAllowViewEmail) {
        this.userAllowViewEmail = userAllowViewEmail;
    }

    public int getUserAllowMassEmail() {
        return userAllowMassEmail;
    }

    public void setUserAllowMassEmail(int userAllowMassEmail) {
        this.userAllowMassEmail = userAllowMassEmail;
    }

    public long getUserOptions() {
        return userOptions;
    }

    public void setUserOptions(long userOptions) {
        this.userOptions = userOptions;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public int getUserAvatarType() {
        return userAvatarType;
    }

    public void setUserAvatarType(int userAvatarType) {
        this.userAvatarType = userAvatarType;
    }

    public int getUserAvatarWidth() {
        return userAvatarWidth;
    }

    public void setUserAvatarWidth(int userAvatarWidth) {
        this.userAvatarWidth = userAvatarWidth;
    }

    public int getUserAvatarHeight() {
        return userAvatarHeight;
    }

    public void setUserAvatarHeight(int userAvatarHeight) {
        this.userAvatarHeight = userAvatarHeight;
    }

    public String getUserSignature() {
        return userSignature;
    }

    public void setUserSignature(String userSignature) {
        this.userSignature = userSignature;
    }

    public String getUserSignatureBBCodeUid() {
        return userSignatureBBCodeUid;
    }

    public void setUserSignatureBBCodeUid(String userSignatureBBCodeUid) {
        this.userSignatureBBCodeUid = userSignatureBBCodeUid;
    }

    public String getUserSignatureBBCodeBitField() {
        return userSignatureBBCodeBitField;
    }

    public void setUserSignatureBBCodeBitField(String userSignatureBBCodeBitField) {
        this.userSignatureBBCodeBitField = userSignatureBBCodeBitField;
    }

    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(String userFrom) {
        this.userFrom = userFrom;
    }

    public String getUserIcq() {
        return userIcq;
    }

    public void setUserIcq(String userIcq) {
        this.userIcq = userIcq;
    }

    public String getUserAim() {
        return userAim;
    }

    public void setUserAim(String userAim) {
        this.userAim = userAim;
    }

    public String getUserYim() {
        return userYim;
    }

    public void setUserYim(String userYim) {
        this.userYim = userYim;
    }

    public String getUserMsnm() {
        return userMsnm;
    }

    public void setUserMsnm(String userMsnm) {
        this.userMsnm = userMsnm;
    }

    public String getUserJabber() {
        return userJabber;
    }

    public void setUserJabber(String userJabber) {
        this.userJabber = userJabber;
    }

    public String getUserWebsite() {
        return userWebsite;
    }

    public void setUserWebsite(String userWebsite) {
        this.userWebsite = userWebsite;
    }

    public String getUserOcc() {
        return userOcc;
    }

    public void setUserOcc(String userOcc) {
        this.userOcc = userOcc;
    }

    public String getUserInterests() {
        return userInterests;
    }

    public void setUserInterests(String userInterests) {
        this.userInterests = userInterests;
    }

    public String getUserActKey() {
        return userActKey;
    }

    public void setUserActKey(String userActKey) {
        this.userActKey = userActKey;
    }

    public String getUserNewPassword() {
        return userNewPassword;
    }

    public void setUserNewPassword(String userNewPassword) {
        this.userNewPassword = userNewPassword;
    }

    public String getUserFormSalt() {
        return userFormSalt;
    }

    public void setUserFormSalt(String userFormSalt) {
        this.userFormSalt = userFormSalt;
    }

    public PhpBbUsers cloneNewUser(PhpBbUsers newPhpBbUser) throws Exception {  
        BeanUtils.copyBean(this, newPhpBbUser);
        return newPhpBbUser; 
    }
    
}
