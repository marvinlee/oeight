/*
 * PhpBbForums.java
 *
 * Created on October 18, 2008, 1:09 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data.phpbb;

/**
 *
 * @author meauchyuan.lee
 */
public class PhpBbForums {
    
    /** Creates a new instance of PhpBbForums */
    public PhpBbForums() {
    }
    private long forumId;
    private String forumName;

    public long getForumId() {
        return forumId;
    }

    public void setForumId(long forumId) {
        this.forumId = forumId;
    }

    public String getForumName() {
        return forumName;
    }

    public void setForumName(String forumName) {
        this.forumName = forumName;
    }
    
    
}
