package com.esmart2u.oeight.data;

import java.util.Date;

public class GoogleAlert {
	  
    private long alertId; 
    private String alertTerm;
    private String feedUrl;
    private Date dateCrawled;
    private int frequency;
    
	public long getAlertId() {
		return alertId;
	}
	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}
	public String getAlertTerm() {
		return alertTerm;
	}
	public void setAlertTerm(String alertTerm) {
		this.alertTerm = alertTerm;
	}
	public String getFeedUrl() {
		return feedUrl;
	}
	public void setFeedUrl(String feedUrl) {
		this.feedUrl = feedUrl;
	}
	public Date getDateCrawled() {
		return dateCrawled;
	}
	public void setDateCrawled(Date dateCrawled) {
		this.dateCrawled = dateCrawled;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
    
    
}
