/*
 * FacebookPhoto.java
 *
 * Created on April 3, 2008, 11:38 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class FacebookPhoto {
    
    private long facebookPhotoId;
    private long facebookUserId;
    private long photoId;
    private String photoUrl;
    private String photoLargeUrl;
    private String photoCaption;
    private Date dateUpdated;
    
    /** Creates a new instance of FacebookPhoto */
    public FacebookPhoto() {
    }

    public long getFacebookPhotoId() {
        return facebookPhotoId;
    }

    public void setFacebookPhotoId(long facebookPhotoId) {
        this.facebookPhotoId = facebookPhotoId;
    }

    public long getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(long facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoCaption() {
        return photoCaption;
    }

    public void setPhotoCaption(String photoCaption) {
        this.photoCaption = photoCaption;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getPhotoLargeUrl() {
        return photoLargeUrl;
    }

    public void setPhotoLargeUrl(String photoLargeUrl) {
        this.photoLargeUrl = photoLargeUrl;
    }
    
}
