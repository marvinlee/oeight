/*
 * UserDetails.java
 *
 * Created on 22 September 2007, 11:28
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author marvin
 */
public class UserPostVotes  implements Serializable{
    
    /** Creates a new instance of UserDetails */
    public UserPostVotes() {
    }
    
    private long userBlogId;
    private long userId;   
    private Date dateVoted;
    private int votedUp;
    private int votedDown; 

    public long getUserBlogId() {
        return userBlogId;
    }

    public void setUserBlogId(long userBlogId) {
        this.userBlogId = userBlogId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getVotedUp() {
        return votedUp;
    }

    public void setVotedUp(int votedUp) {
        this.votedUp = votedUp;
    }

    public int getVotedDown() {
        return votedDown;
    }

    public void setVotedDown(int votedDown) {
        this.votedDown = votedDown;
    }

    public Date getDateVoted() {
        return dateVoted;
    }

    public void setDateVoted(Date dateVoted) {
        this.dateVoted = dateVoted;
    }
     
     
    
}
