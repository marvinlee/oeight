/*
 * FriendsterUser.java
 *
 * Created on March 31, 2008, 4:54 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterUser {
    
    private long friendsterUserPk;
    private long friendsterUserId;
    private String friendsterUserName;
    private String profileUrl;
    private String primaryPhotoUrl;
    private Date dateUpdated;
    
    /** Creates a new instance of FriendsterUser */
    public FriendsterUser() {
    }

    public long getFriendsterUserId() {
        return friendsterUserId;
    }

    public void setFriendsterUserId(long friendsterUserId) {
        this.friendsterUserId = friendsterUserId;
    }

    public String getFriendsterUserName() {
        return friendsterUserName;
    }

    public void setFriendsterUserName(String friendsterUserName) {
        this.friendsterUserName = friendsterUserName;
    }

    public String getPrimaryPhotoUrl() {
        return primaryPhotoUrl;
    }

    public void setPrimaryPhotoUrl(String primaryPhotoUrl) {
        this.primaryPhotoUrl = primaryPhotoUrl;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }
    
}
