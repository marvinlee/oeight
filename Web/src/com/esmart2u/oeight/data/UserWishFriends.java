/*
 * UserWishFriends.java
 *
 * Created on January 15, 2008, 11:03 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserWishFriends {
    
    private long wishFriendId;
    private long userId;
    private long friendUserId;
    private char status;
    // Order desc
    // Flow - New Request Z -> Confirmed - N (insert another to the other side), D- Deleted 
    // Z - New Request
    // N - New/Currently friend
    // D - Deleted
    // B - Blocked forever
    // A - Not friend yet
    private Date latestSentDate;
    
    private User user;
    
    /** Creates a new instance of UserWishFriends */
    public UserWishFriends() {
    }

    public long getWishFriendId() {
        return wishFriendId;
    }

    public void setWishFriendId(long wishFriendId) {
        this.wishFriendId = wishFriendId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getFriendUserId() {
        return friendUserId;
    }

    public void setFriendUserId(long friendUserId) {
        this.friendUserId = friendUserId;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getLatestSentDate() {
        return latestSentDate;
    }

    public void setLatestSentDate(Date latestSentDate) {
        this.latestSentDate = latestSentDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
    
}
