/*
 * ReservedNames.java
 *
 * Created on 22 September 2007, 12:10
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

/**
 *
 * @author marvin
 */
public class ReservedNames {
    
    /** Creates a new instance of ReservedNames */
    public ReservedNames() {
    }
   
    private long namesId;
    private String names;

    public long getNamesId() {
        return namesId;
    }

    public void setNamesId(long namesId) {
        this.namesId = namesId;
    }
    
    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    
}
