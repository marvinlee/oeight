/*
 * OEightWeddingComment.java
 *
 * Created on June 22, 2008, 4:20 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class OEightWeddingComment {
    
    /** Creates a new instance of OEightWeddingComment */
    public OEightWeddingComment() {
    }
    
    private long weddingCommentId;
    private long userId;
    private long commentorId;
    private String comments;
    private Date dateSubmitted;
    private char moderated;
    private char systemStatus;

    public long getWeddingCommentId() {
        return weddingCommentId;
    }

    public void setWeddingCommentId(long weddingCommentId) {
        this.weddingCommentId = weddingCommentId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCommentorId() {
        return commentorId;
    }

    public void setCommentorId(long commentorId) {
        this.commentorId = commentorId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(Date dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public char getModerated() {
        return moderated;
    }

    public void setModerated(char moderated) {
        this.moderated = moderated;
    }

    public char getSystemStatus() {
        return systemStatus;
    }

    public void setSystemStatus(char systemStatus) {
        this.systemStatus = systemStatus;
    }
    
    

}
