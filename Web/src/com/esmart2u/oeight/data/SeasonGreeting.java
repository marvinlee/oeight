/*
 * SeasonGreeting.java
 *
 * Created on November 14, 2007, 11:32 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class SeasonGreeting {
    
    /** Creates a new instance of SeasonGreeting */
    public SeasonGreeting() {
    }
    
    private long greetingId;
    private String senderEmail;
    private String senderName;
    private String receiverEmail;
    private String receiverName;
    private String message;
    private String greetingCode;
    private int type;
    private boolean subscribe;
    private char status;
    private Date dateSent;
    private Date dateMailed;
    private Date dateAccepted;

    public long getGreetingId() {
        return greetingId;
    }

    public void setGreetingId(long greetingId) {
        this.greetingId = greetingId;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
 

    public String getGreetingCode() {
        return greetingCode;
    }

    public void setGreetingCode(String greetingCode) {
        this.greetingCode = greetingCode;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public Date getDateMailed() {
        return dateMailed;
    }

    public void setDateMailed(Date dateMailed) {
        this.dateMailed = dateMailed;
    }

    public Date getDateAccepted() {
        return dateAccepted;
    }

    public void setDateAccepted(Date dateAccepted) {
        this.dateAccepted = dateAccepted;
    }

    public boolean getSubscribe() {
        return subscribe;
    }

    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    
    
    
    
}
