/*
 * Mail1.java
 *
 * Created on 26 January 2008, 01:13
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class Mail1 {
    private long mailId;
    private String email;
    private char status;
    private Date dateMailed;
    /** Creates a new instance of Mail1 */
    public Mail1() {
    }

    public long getMailId() {
        return mailId;
    }

    public void setMailId(long mailId) {
        this.mailId = mailId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getDateMailed() {
        return dateMailed;
    }

    public void setDateMailed(Date dateMailed) {
        this.dateMailed = dateMailed;
    }
    
}
