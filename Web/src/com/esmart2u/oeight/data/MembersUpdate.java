/*
 * MembersUpdate.java
 *
 * Created on January 19, 2008, 12:03 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class MembersUpdate {
    
    private long membersUpdateId;
    private long userId;
    private String userName;
    private String photoSmallPath;
    private String photoLargePath;
    private char updateType;
    private Date dateUpdate;
    
    /** Creates a new instance of MembersUpdate */
    public MembersUpdate() {
    }

    public long getMembersUpdateId() {
        return membersUpdateId;
    }

    public void setMembersUpdateId(long membersUpdateId) {
        this.membersUpdateId = membersUpdateId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }

    public String getPhotoLargePath() {
        return photoLargePath;
    }

    public void setPhotoLargePath(String photoLargePath) {
        this.photoLargePath = photoLargePath;
    }

    public char getUpdateType() {
        return updateType;
    }

    public void setUpdateType(char updateType) {
        this.updateType = updateType;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }
    
}
