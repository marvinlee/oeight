/*
 * Login.java
 *
 * Created on 22 September 2007, 11:19
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class LoginFailure {
    
    private long loginSessionId;
    private String loginEmail;
    private char loginFailureFlag;
    private long userId;
    private String loginCode;
    private Date lastLoginTime; 
    private String ipAddress;
    private String referrer;

    public long getLoginSessionId() {
        return loginSessionId;
    }

    public void setLoginSessionId(long loginSessionId) {
        this.loginSessionId = loginSessionId;
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(String loginEmail) {
        this.loginEmail = loginEmail;
    }

    public char getLoginFailureFlag() {
        return loginFailureFlag;
    }

    public void setLoginFailureFlag(char loginFailureFlag) {
        this.loginFailureFlag = loginFailureFlag;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(String loginCode) {
        this.loginCode = loginCode;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }
     

    
    
}
