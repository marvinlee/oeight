/*
 * UserLogin.java
 *
 * Created on September 19, 2007, 11:12 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import com.esmart2u.solution.base.helper.StringUtils;
import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class User {
    
    /** Creates a new instance of UserLogin */
    public User() {
    }
    
    private long userId;
    private String userName;
    private String emailAddress;
    private String secureCode;
    private int version;
    private UserCountry userCountry;
    
    private List photoList;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        
        if(StringUtils.hasValue(emailAddress))
        {
            emailAddress = emailAddress.toLowerCase().trim();
        }
        this.emailAddress = emailAddress;
    }

    public String getSecureCode() {
        return secureCode;
    }

    public void setSecureCode(String secureCode) {
        this.secureCode = secureCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        
        if(StringUtils.hasValue(userName))
        {
            userName = userName.toLowerCase().trim();
        }
        this.userName = userName;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public UserCountry getUserCountry() {
        return userCountry;
    }

    public void setUserCountry(UserCountry userCountry) {
        this.userCountry = userCountry;
    }

    public List getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List photoList) {
        this.photoList = photoList;
    }

 
    
}
