/*
 * FeaturedBlog.java
 *
 * Created on October 23, 2007, 11:35 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class FeaturedBlog implements Serializable{
    
    /** Creates a new instance of FeaturedBlog */
    private long blogId; 
    private long userId;
    private String blogName;
    private String blogDescription;
    private String blogPhotoPath;
    private String blogLink;
    private char status; 
    
    
    private User user;
    /*private String webBlog;
    private Date dateToList;
    
    
    public FeaturedBlog() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getWebBlog() {
        return webBlog;
    }

    public void setWebBlog(String webBlog) {
        this.webBlog = webBlog;
    }

    public Date getDateToList() {
        return dateToList;
    }

    public void setDateToList(Date dateToList) {
        this.dateToList = dateToList;
    }

    */
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public long getBlogId() {
        return blogId;
    }

    public void setBlogId(long blogId) {
        this.blogId = blogId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getBlogDescription() {
        return blogDescription;
    }

    public void setBlogDescription(String blogDescription) {
        this.blogDescription = blogDescription;
    }

    public String getBlogPhotoPath() {
        return blogPhotoPath;
    }

    public void setBlogPhotoPath(String blogPhotoPath) {
        this.blogPhotoPath = blogPhotoPath;
    }

    public String getBlogLink() {
        return blogLink;
    }

    public void setBlogLink(String blogLink) {
        this.blogLink = blogLink;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }
    
}
