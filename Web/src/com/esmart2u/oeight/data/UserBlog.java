/*
 * UserDetails.java
 *
 * Created on 22 September 2007, 11:28
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author marvin
 */
public class UserBlog {
    
    /** Creates a new instance of UserDetails */
    public UserBlog() {
    }
    
    private long userBlogId;
    private long userId;
    private String blogName;
    private String blogUrl;
    private String blogFeed;
    private String blogThumbnail;
    private Date lastFetched;
    private int fetchFrequency;
    private char greenFlag;
    private char validated;
    private char spamFlag; 
    private int version;
     
    private ArrayList postsList;
    
    public long getUserBlogId() {
        return userBlogId;
    }

    public void setUserBlogId(long userBlogId) {
        this.userBlogId = userBlogId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getBlogUrl() {
        return blogUrl;
    }

    public void setBlogUrl(String blogUrl) {
        this.blogUrl = blogUrl;
    }

    public String getBlogFeed() {
        return blogFeed;
    }

    public void setBlogFeed(String blogFeed) {
        this.blogFeed = blogFeed;
    }

    public String getBlogThumbnail() {
        return blogThumbnail;
    }

    public void setBlogThumbnail(String blogThumbnail) {
        this.blogThumbnail = blogThumbnail;
    }

    public Date getLastFetched() {
        return lastFetched;
    }

    public void setLastFetched(Date lastFetched) {
        this.lastFetched = lastFetched;
    }

    public int getFetchFrequency() {
        return fetchFrequency;
    }

    public void setFetchFrequency(int fetchFrequency) {
        this.fetchFrequency = fetchFrequency;
    }
    
    public char getGreenFlag() {
        return greenFlag;
    }

    public void setGreenFlag(char greenFlag) {
        this.greenFlag = greenFlag;
    }

    public char getValidated() {
        return validated;
    }

    public void setValidated(char validated) {
        this.validated = validated;
    }

    public char getSpamFlag() {
        return spamFlag;
    }

    public void setSpamFlag(char spamFlag) {
        this.spamFlag = spamFlag;
    } 

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

	public ArrayList getPostsList() {
		return postsList;
	}

	public void setPostsList(ArrayList postsList) {
		this.postsList = postsList;
	}  

    
}
