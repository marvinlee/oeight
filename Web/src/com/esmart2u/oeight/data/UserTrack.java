/*
 * UserTrack.java
 *
 * Created on May 5, 2008, 11:47 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserTrack implements Serializable{
    
    
    private long userId;
    private int trackCode;
    private Date trackDate;
    private int trackValue;
    
    /** Creates a new instance of UserTrack */
    public UserTrack() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getTrackCode() {
        return trackCode;
    }

    public void setTrackCode(int trackCode) {
        this.trackCode = trackCode;
    }

    public Date getTrackDate() {
        return trackDate;
    }

    public void setTrackDate(Date trackDate) {
        this.trackDate = trackDate;
    }

    public int getTrackValue() {
        return trackValue;
    }

    public void setTrackValue(int trackValue) {
        this.trackValue = trackValue;
    }
    
}
