/*
 * RankingSnapshot.java
 *
 * Created on 22 September 2007, 11:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author marvin
 */
public class RankingPodium implements Serializable{

    /** Creates a new instance of RankingSnapshot */
    public RankingPodium() {
    }

    private long rankingPodiumId;
    private long userId;
    private Date snapshotMonth;
    private char snapshotCode;
    private int rank;
    private long value;

    // foreign table
    private User user;
    
    public long getRankingPodiumId() {
        return rankingPodiumId;
    }

    public void setRankingPodiumId(long rankingPodiumId) {
        this.rankingPodiumId = rankingPodiumId;
    }
    
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
    
    public Date getSnapshotMonth() {
        return snapshotMonth;
    }

    public void setSnapshotMonth(Date snapshotMonth) {
        this.snapshotMonth = snapshotMonth;
    }

    public char getSnapshotCode() {
        return snapshotCode;
    }

    public void setSnapshotCode(char snapshotCode) {
        this.snapshotCode = snapshotCode;
    } 

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    } 

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    } 

}
