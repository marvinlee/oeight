/*
 * Mosaic.java
 *
 * Created on 22 September 2007, 12:04
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class Mosaic {
    
    /** Creates a new instance of Mosaic */
    public Mosaic() {
    }
    
    private long mosaicId;
    private int sequenceNumber;
    private long mosaicCurrentId;
    private Date dateGenerated;
    private String mapFilePath;
    private String pictureLocalPath;
    private String pictureRemotePath; 
    
    private MosaicCurrent mosaicCurrent;
    
    public long getMosaicId() {
        return mosaicId;
    }

    public void setMosaicId(long mosaicId) {
        System.out.println("Method----- setMosaicId is called=" + mosaicId);
        this.mosaicId = mosaicId;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }
    
    public long getMosaicCurrentId() {
        return mosaicCurrentId;
    }

    public void setMosaicCurrentId(long mosaicCurrentId) {
        System.out.println("Method----- setMosaicCurrentId is called=" + mosaicCurrentId);
        this.mosaicCurrentId = mosaicCurrentId;
    }

    public Date getDateGenerated() {
        return dateGenerated;
    }

    public void setDateGenerated(Date dateGenerated) {
        this.dateGenerated = dateGenerated;
    }

    public String getMapFilePath() {
        return mapFilePath;
    }

    public void setMapFilePath(String mapFilePath) {
        this.mapFilePath = mapFilePath;
    }
    
    public String getPictureLocalPath() {
        return pictureLocalPath;
    }

    public void setPictureLocalPath(String pictureLocalPath) {
        this.pictureLocalPath = pictureLocalPath;
    }

    public String getPictureRemotePath() {
        return pictureRemotePath;
    }

    public void setPictureRemotePath(String pictureRemotePath) {
        this.pictureRemotePath = pictureRemotePath;
    } 

     
    public MosaicCurrent getMosaicCurrent() {
        return mosaicCurrent;
    }

    public void setMosaicCurrent(MosaicCurrent mosaicCurrent) {
        System.out.println("Method----- setMosaicCurrent is called=" + mosaicCurrent);
        this.mosaicCurrent = mosaicCurrent;
    }
}
