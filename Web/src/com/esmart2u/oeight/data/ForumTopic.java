/*
 * ForumTopic.java
 *
 * Created on October 18, 2008, 1:46 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class ForumTopic { 
         
    private long forumTopicId;
    private long forumId;
    private String forumName;
    private long topicId;
    private String topicTitle;
    private String topicUrl;
    private Date topicTime;
    private String topicPosterName;
    private String topicPost;
    
    /** Creates a new instance of ForumTopic */
    public ForumTopic() {
    }

    public long getForumTopicId() {
        return forumTopicId;
    }

    public void setForumTopicId(long forumTopicId) {
        this.forumTopicId = forumTopicId;
    }

    public long getForumId() {
        return forumId;
    }

    public void setForumId(long forumId) {
        this.forumId = forumId;
    }

    public String getForumName() {
        return forumName;
    }

    public void setForumName(String forumName) {
        this.forumName = forumName;
    }

    public long getTopicId() {
        return topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    public String getTopicUrl() {
        return topicUrl;
    }

    public void setTopicUrl(String topicUrl) {
        this.topicUrl = topicUrl;
    }

    public Date getTopicTime() {
        return topicTime;
    }

    public void setTopicTime(Date topicTime) {
        this.topicTime = topicTime;
    }

    public String getTopicPosterName() {
        return topicPosterName;
    }

    public void setTopicPosterName(String topicPosterName) {
        this.topicPosterName = topicPosterName;
    }

    public String getTopicPost() {
        return topicPost;
    }

    public void setTopicPost(String topicPost) {
        this.topicPost = topicPost;
    }

}
