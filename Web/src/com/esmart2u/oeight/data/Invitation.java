/*
 * Invitation.java
 *
 * Created on 22 September 2007, 12:01
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class Invitation {
    
    /** Creates a new instance of Invitation */
    public Invitation() {
    }
    
    private long userId;
    private String emailAddress;
    private Date dateInvited;
    private Date dateJoined;
    private int timeSent;
    private int version;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getDateInvited() {
        return dateInvited;
    }

    public void setDateInvited(Date dateInvited) {
        this.dateInvited = dateInvited;
    }

    public Date getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Date dateJoined) {
        this.dateJoined = dateJoined;
    }

    public int getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(int timeSent) {
        this.timeSent = timeSent;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    

}
