/*
 * OEightWeddingPhoto.java
 *
 * Created on June 22, 2008, 4:20 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class OEightWeddingPhoto {
    
    /** Creates a new instance of OEightWeddingPhoto */
    public OEightWeddingPhoto() {
    }
       
    private long weddingPhotoId;
    private long userId;
    private String photoSmallPath;
    private String photoLargePath;
    private Date dateSubmitted;
    private boolean moderated; 

    public long getWeddingPhotoId() {
        return weddingPhotoId;
    }

    public void setWeddingPhotoId(long weddingPhotoId) {
        this.weddingPhotoId = weddingPhotoId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }

    public String getPhotoLargePath() {
        return photoLargePath;
    }

    public void setPhotoLargePath(String photoLargePath) {
        this.photoLargePath = photoLargePath;
    }

    public Date getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(Date dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public boolean isModerated() {
        return moderated;
    }

    public void setModerated(boolean moderated) {
        this.moderated = moderated;
    }
    
}
