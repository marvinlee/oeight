/*
 * UserPictures.java
 *
 * Created on 22 September 2007, 11:38
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class UserPictures {
    
    /** Creates a new instance of UserPictures */
    public UserPictures() {
    }
    
    private long photoId;
    private long userId;
    private char photoType;
    private String photoSmall;
    private String photoLarge;
    private Date dateSubmitted;
    private String caption;
    private long views;
    private char moderated;
    private int version;

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }
    
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public char getPhotoType() {
        return photoType;
    }

    public void setPhotoType(char photoType) {
        this.photoType = photoType;
    }

    public String getPhotoSmall() {
        return photoSmall;
    }

    public void setPhotoSmall(String photoSmall) {
        this.photoSmall = photoSmall;
    }

    public String getPhotoLarge() {
        return photoLarge;
    }

    public void setPhotoLarge(String photoLarge) {
        this.photoLarge = photoLarge;
    }

    public Date getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(Date dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public char getModerated() {
        return moderated;
    }

    public void setModerated(char moderated) {
        this.moderated = moderated;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    
    
}
