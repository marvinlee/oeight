/*
 * UserDetails.java
 *
 * Created on 22 September 2007, 11:28
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author marvin
 */
public class UserPost {
    
    /** Creates a new instance of UserDetails */
    public UserPost() {
    }
    
    private long userPostId;
    private long userBlogId; //optional
    private long postedByUserId;
    private String postUrl;
    private String postTitle;
    private String postExcerpt;
    private String postThumbnail;
    private Date postedDate;
    private long votedUp;
    private long votedDown;
    private long viewedCount;
    private long clickedCount; 
    private char greenFlag;
    private char validated;
    private char spamFlag; 
    private int version;
     
    
    public long getUserPostId() {
        return userPostId;
    }

    public void setUserPostId(long userPostId) {
        this.userPostId = userPostId;
    }

    public long getUserBlogId() {
        return userBlogId;
    }

    public void setUserBlogId(long userBlogId) {
        this.userBlogId = userBlogId;
    }

    public long getPostedByUserId() {
        return postedByUserId;
    }

    public void setPostedByUserId(long postedByUserId) {
        this.postedByUserId = postedByUserId;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }


    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostExcerpt() {
        return postExcerpt;
    }

    public void setPostExcerpt(String postExcerpt) {
        this.postExcerpt = postExcerpt;
    }

    public String getPostThumbnail() {
        return postThumbnail;
    }

    public void setPostThumbnail(String postThumbnail) {
        this.postThumbnail = postThumbnail;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public long getVotedUp() {
        return votedUp;
    }

    public void setVotedUp(long votedUp) {
        this.votedUp = votedUp;
    }

    public long getVotedDown() {
        return votedDown;
    }

    public void setVotedDown(long votedDown) {
        this.votedDown = votedDown;
    }

    public long getViewedCount() {
        return viewedCount;
    }

    public void setViewedCount(long viewedCount) {
        this.viewedCount = viewedCount;
    }

    public long getClickedCount() {
        return clickedCount;
    }

    public void setClickedCount(long clickedCount) {
        this.clickedCount = clickedCount;
    }

    public char getGreenFlag() {
        return greenFlag;
    }

    public void setGreenFlag(char greenFlag) {
        this.greenFlag = greenFlag;
    }

    public char getValidated() {
        return validated;
    }

    public void setValidated(char validated) {
        this.validated = validated;
    }

    public char getSpamFlag() {
        return spamFlag;
    }

    public void setSpamFlag(char spamFlag) {
        this.spamFlag = spamFlag;
    } 
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }  

    
}
