/*
 * UserWidgetFacebook.java
 *
 * Created on April 2, 2008, 4:34 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import com.esmart2u.oeight.member.helper.OEightConstants;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserWidgetFacebook  implements Serializable{
    
    /** Creates a new instance of UserWidgetFacebook */
    public UserWidgetFacebook() {
    }
    
    private long userWidgetFacebookId;
    private long userId;
    private String userName;
    private long facebookUserId;
    private int widgetType;
    private Date dateInstalled;
    private long views;
    private char status;
    
    private User user;

    public long getUserWidgetFacebookId() {
        return userWidgetFacebookId;
    }

    public void setUserWidgetFacebookId(long userWidgetFacebookId) {
        this.userWidgetFacebookId = userWidgetFacebookId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getFacebookUserId() {
        return facebookUserId;
    }

    public void setFacebookUserId(long facebookUserId) {
        this.facebookUserId = facebookUserId;
    }

    public int getWidgetType() {
        return widgetType;
    }

    public void setWidgetType(int widgetType) {
        this.widgetType = widgetType;
    }

    public Date getDateInstalled() {
        return dateInstalled;
    }

    public void setDateInstalled(Date dateInstalled) {
        this.dateInstalled = dateInstalled;
    }

    public long getViews() {
        return views;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    // Other than D (Removed) we set is as N for active status.
    public char getStatus() {
        if (status != OEightConstants.SYS_STATUS_REMOVED)
        {
            status = OEightConstants.SYS_STATUS_NEW;
        }
        return status;
    }

    public void setStatus(char status) {
        if (status != OEightConstants.SYS_STATUS_REMOVED)
        {
            status = OEightConstants.SYS_STATUS_NEW;
        }
        this.status = status;
    }
    
    
}
