/*
 * UserWishers.java
 *
 * Created on November 29, 2007, 5:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.data;

import java.util.Date;

/**
 *
 * @author meauchyuan.lee
 */
public class UserWishers {
    
    /** Creates a new instance of UserWishers */
    public UserWishers() {
    }
    
    private long wishId;
    private long userId;
    private long wisherId;
    private String wishContent; 
    private Date dateSent;
    private char status;
    
    // Foreign key to wisher info
    private User user;

    public long getWishId() {
        return wishId;
    }

    public void setWishId(long wishId) {
        this.wishId = wishId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getWisherId() {
        return wisherId;
    }

    public void setWisherId(long wisherId) {
        this.wisherId = wisherId;
    }

    public String getWishContent() {
        return wishContent;
    }

    public void setWishContent(String wishContent) {
        this.wishContent = wishContent;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
}
