/*
 * FriendsterException.java
 *
 * Created on April 3, 2008, 3:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.exception;

import com.esmart2u.solution.base.logging.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterException extends Exception{
      
    private static Logger logger = Logger.getLogger(FriendsterException.class);
   
    public FriendsterException(String message)
    {
        super(message);
    }

    public FriendsterException(Throwable throwable)
    { 
            logger.error(getMessage(), throwable); 
    }
}
