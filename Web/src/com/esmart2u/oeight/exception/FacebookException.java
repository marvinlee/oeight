/*
 * FacebookException.java
 *
 * Created on April 3, 2008, 4:00 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.exception;

import com.esmart2u.solution.base.logging.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class FacebookException extends Exception {
      
    private static Logger logger = Logger.getLogger(FacebookException.class);
 
    public FacebookException(String message)
    {
        super(message);
    }
    
    public FacebookException(Throwable throwable)
    { 
            logger.error(getMessage(), throwable); 
    }
}
