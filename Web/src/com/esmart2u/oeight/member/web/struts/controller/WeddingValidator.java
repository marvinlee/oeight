/*
 * WeddingValidator.java
 *
 * Created on June 25, 2008, 10:02 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.solution.base.helper.PhotoUploadHelper;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.logging.Logger;
import java.io.FileNotFoundException;
import java.io.IOException;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author meauchyuan.lee
 */
public class WeddingValidator { 
    
    private static Logger logger = Logger.getLogger(WeddingValidator.class);
    
    /** Creates a new instance of WeddingValidator */
    public WeddingValidator() {
    }

    static ActionErrors validateRegisterUser(ActionErrors errors, WeddingForm weddingForm) { 
        
        //todo, do not allow if already registered
        return errors; 
    }

    static ActionErrors validateRegistrationSubmission(ActionErrors errors, WeddingForm weddingForm) {
        
        //todo, do not allow if already registered
        
        if (!Validator.validateString(weddingForm.getCoupleName(),true,1,100,false)) {
            errors.add("coupleName",new ActionError("common.invalid.weddingcampaign.coupleName"));
        } 
        if (!Validator.validateString(weddingForm.getWeddingDescription(),true,1,500,false)) {
            errors.add("weddingDescription",new ActionError("common.invalid.weddingcampaign.weddingDescription"));
        } 
        
        // Validate photo type, photo size
        FormFile photoFile = weddingForm.getPhotoFile();
        String mimeType = photoFile.getContentType();
        logger.debug("Validating pageThree mimeType=" + mimeType);
        //System.out.println("Uploaded filesize=" + photoFile.getFileSize());
        logger.debug("Uploaded filesize=" + photoFile.getFileSize());
        
        if (photoFile == null || photoFile.getFileSize()<1) {
            //System.out.println("Checking zero filezie");
            logger.debug("Checking zero filezie");
            return errors; // Just return if user does not upload photo
            //errors.add("photoPath",new ActionError("common.invalid.photoPath"));
        } else{
            checkUploadedFile(errors, photoFile, "photoPath");
        } 
        return errors;
    }
    
    static ActionErrors validatePageSave(ActionErrors errors, WeddingForm weddingForm) {
        
        //todo, do not allow if already registered
        
        if (!Validator.validateString(weddingForm.getCoupleName(),true,1,100,false)) {
            errors.add("coupleName",new ActionError("common.invalid.weddingcampaign.coupleName"));
        } 
        if (!Validator.validateString(weddingForm.getWeddingDescription(),true,1,500,false)) {
            errors.add("weddingDescription",new ActionError("common.invalid.weddingcampaign.weddingDescription"));
        } 
        
        // Validate photo type, photo size
        // !!! If photo is null, just do not update it, as could be available without update
        FormFile photoFile = weddingForm.getPhotoFile(); 
        if (photoFile != null){
            checkUploadedFile(errors, photoFile, "photoPath");
        } 
        return errors;
    }
    
    private static ActionErrors checkUploadedFile(ActionErrors errors, FormFile photoFile, String errorKey)
    {
            String mimeType = photoFile.getContentType();  
            logger.debug("Uploaded filesize=" + photoFile.getFileSize());

            /*if (photoFile == null || photoFile.getFileSize()<1) { 
                logger.debug("Checking zero filezie");
                errors.add(errorKey,new ActionError("common.invalid.photoPath"));
            } else */
            if (photoFile.getFileSize() > 0){
                if (photoFile.getFileSize() > 250000) { 
                    logger.debug("Check not over filesize");
                    errors.add(errorKey,new ActionError("common.invalid.photoPathSize"));
                } else if (!PhotoUploadHelper.isAcceptedMimeType(mimeType)) { 
                    logger.debug("mimetype not accepted");
                    errors.add(errorKey,new ActionError("common.invalid.photoPathContentType"));
                } else {
                    try {
                        logger.debug("Checking magic content");
                        Magic parser = new Magic() ;
                        MagicMatch match;
                        match = parser.getMagicMatch(photoFile.getFileData());
                        logger.debug("Actual file mimetype=" + match.getMimeType()) ;

                        //if (!match.mimeTypeMatches(mimeType)) { 
                        // the actual mimetype string might not match
                        // so we take if it is of accepted mimetype
                        if (!PhotoUploadHelper.isAcceptedMimeType(match.getMimeType())) {
                            logger.debug("Mimetype of actual file not match"); 
                            errors.add(errorKey,new ActionError("common.mismatch.photoPathMime"));
                        }

                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.invalid.photoPath"));
                    } catch (MagicMatchNotFoundException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.mismatch.photoPathMime"));
                    } catch (MagicParseException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.mismatch.photoPathMime"));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.invalid.photoPath"));
                    } catch (MagicException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.mismatch.photoPathMime"));
                    }
               }
            }
        return errors;
    }
}
