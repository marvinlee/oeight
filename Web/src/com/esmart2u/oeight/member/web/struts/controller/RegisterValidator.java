/*
 * RegisterValidator.java
 *
 * Created on September 28, 2007, 12:58 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.member.bo.InviteBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.oeight.member.web.struts.helper.ReservedNamesHelper;
import com.esmart2u.oeight.member.web.struts.helper.StateComboHelper;
import com.esmart2u.solution.base.helper.PhotoUploadHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.web.struts.service.Recaptcha4JService;
import java.io.FileNotFoundException;
import java.io.IOException;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author meauchyuan.lee
 */
public class RegisterValidator {
    
    private static Logger logger = Logger.getLogger(RegisterValidator.class);
    
    /** Creates a new instance of RegisterValidator */
    public RegisterValidator() {
    }
    
    public static ActionErrors validateInvitation(ActionErrors errors, RegisterForm registerForm) {
         
        if (!Validator.validateString(registerForm.getInvitedBy(),true,1,50)) {
            errors.add("referral",new ActionError("common.invalid.referral"));
        } 
        else if (!Validator.validateString(registerForm.getInviteCode(),true,1,500)) {
            errors.add("referral",new ActionError("common.invalid.referral"));
        }
        
        // Now that both fields are valid, we check against USER_INVITES table 
        if (errors.size() < 1) {
            InviteBO inviteBO = new InviteBO();
            if (!inviteBO.validateInvite(registerForm))
            {
                errors.add("referral",new ActionError("common.invalid.referral")); 
                registerForm.setInviteError(PropertyConstants.BOOLEAN_YES);
            }
            inviteBO = null;
        }
        else
        {
            // Has no referral or invite code, so it's a self requested    
            // Remove the previous error
            errors.clear();
            registerForm.setInviteError(PropertyConstants.BOOLEAN_YES);
            registerForm.setFromRequest(PropertyConstants.BOOLEAN_YES);
        }
        return errors;
    }
    
    public static ActionErrors validatePageOne(ActionErrors errors, RegisterForm registerForm) {
         
        if (!Validator.validateString(registerForm.getUserName(),true,1,50)) {
            errors.add("userName",new ActionError("common.invalid.userName"));
        }
        else
        {   String userName = registerForm.getUserName();
            // now checks format
            if (userName.length() < 4 || userName.length() > 12) {
                errors.add("userName", new ActionError("common.invalid.userName"));
            }
            else
            {
            
                // alpha numerics
                boolean foundInvalidChars = false;
                for(int i=0;i<userName.length();i++)
                {
                    int testStrIndex=userName.charAt(i);
                    if(testStrIndex<48 || (testStrIndex>57 && testStrIndex<65) || (testStrIndex>90 && testStrIndex<97)
                            || testStrIndex>122)
                    {
                         errors.add("userName", new ActionError("common.invalid.userName"));
                         i=userName.length();
                         foundInvalidChars = true;
                    }
                }
                if (!foundInvalidChars){
                    // if whole string numbers only
                    int foundNumbers = 0;
                    int userNameLength = userName.length();
                     for(int i=0;i<userNameLength;i++)
                    {
                        int testStrIndex=userName.charAt(i);
                        if(testStrIndex>47 && testStrIndex<58)
                        {
                            foundNumbers += 1;
                        }
                    }
                    if (foundNumbers == userNameLength){
                        errors.add("userName", new ActionError("common.invalid.userName"));
                    }
                }
            }
        }
        if (!Validator.validateEmail(registerForm.getEmailAddress(),true)) {
            errors.add("emailAddress",new ActionError("common.invalid.emailAddress"));
        }
        if (!Validator.validateString(registerForm.getPassword(),true,4,12)) {
            errors.add("password",new ActionError("common.invalid.password"));
        }
        if (!Validator.validateString(registerForm.getConfirmPassword(),true,4,12)) {
            errors.add("confirmPassword",new ActionError("common.invalid.confirmPassword"));
        }
        
        if (errors.size() < 1) {
            UserBO userBO = new UserBO();
            // Check if user name already exists
            if (userBO.usernameExists(registerForm.getUserName())) {
                errors.add("userName",new ActionError("common.exists.userName"));
            }
            // Check if not of reserved names list
            else if (ReservedNamesHelper.isReservedName(registerForm.getUserName())) {
                errors.add("userName",new ActionError("common.exists.userName"));
            } 
            
            
            // Check if email has been used
            if (userBO.emailExists(registerForm.getEmailAddress())) {
                errors.add("emailAddress",new ActionError("common.exists.emailAddress"));
            }
            
            userBO = null;
            
            // Checks password
            if (!registerForm.getPassword().equals(registerForm.getConfirmPassword())) {
                errors.add("confirmPasswordMismatch",new ActionError("common.mismatch.confirmPassword"));
            }
        }
        
        // Checks Recaptcha
        if (Recaptcha4JService.isEnabled())
        {
            try{
                ReCaptcha captcha = Recaptcha4JService.getReCaptchaInstance();
                String challengeField = registerForm.getRecaptcha_challenge_field();
                String responseField = registerForm.getRecaptcha_response_field();
                String ipAddress = registerForm.getActionContext().getRequest().getRemoteAddr();
                ReCaptchaResponse captchaResponse = captcha.checkAnswer(ipAddress, challengeField, responseField);
                logger.debug("Recaptcha - Challenge field:" + challengeField);
                logger.debug("Recaptcha - Response  field:" + responseField );  
                logger.debug("ReCaptcha response valid:" + captchaResponse.isValid()); 
                logger.debug("ReCaptcha response errorMessage:" + captchaResponse.getErrorMessage());
                
                if (!captchaResponse.isValid())
                {
                    errors.add("recaptchaResponse",new ActionError("common.recaptcha.invalidResponse")); 
                }
                
            }catch (Throwable t){
                logger.error("Error in resolving recaptcha, possible error in connection to server");
                errors.add("recaptchaResponse",new ActionError("common.recaptcha.error"));
            }
        }
        
        return errors;
        
    }
    
    
    
    public static ActionErrors validatePageTwo(ActionErrors errors, RegisterForm registerForm) {
        
        if (!Validator.validateString(registerForm.getName(),true,1,100)) {
            errors.add("name",new ActionError("common.invalid.name"));
        }
        if (!Validator.validateMandatoryField(Character.toString(registerForm.getGender()))) {
            errors.add("gender",new ActionError("common.invalid.gender"));
        }
        if (!Validator.validateString(registerForm.getBirthdayString(),true,10,10)) {
            errors.add("birthdayString",new ActionError("common.invalid.birthdayString"));
        }
        else
        {
            // Check is it between 14 and more
            if (DateObjectHelper.getAge(registerForm.getBirthDate().getTime()) < 14)
            { 
                errors.add("birthdayString",new ActionError("common.invalid.birthdayAgeTooYoung")); 
            } 
        }
        
        
        
        if (!Validator.validateString(registerForm.getCity(),false,0,100)) {
            errors.add("city",new ActionError("common.invalid.city"));
        }
       // if (!Validator.validateString(registerForm.getNationality(),true,4,12)) {
        if (!CountryComboHelper.isAcceptedCountryCode(registerForm.getNationality())){
            errors.add("nationality",new ActionError("common.invalid.nationality"));
        }
        else
        {
            if (registerForm.getNationality().equals(
                    PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY)))
            {
                // Country is accepted, check state 
                if (!StateComboHelper.isAcceptedStateCode(registerForm.getState())) {
                    errors.add("state",new ActionError("common.invalid.state"));
                }
            }
        }
        
        
        return errors;
        
    }
    
    
    
    public static ActionErrors validatePageThree(ActionErrors errors, RegisterForm registerForm) { 
        // if skip, just return
        if (PropertyConstants.BOOLEAN_YES == registerForm.getSkip3()) {
            return errors;
        }
        
        // Validate photo type, photo size
        FormFile photoFile = registerForm.getPhotoFile();
        String mimeType = photoFile.getContentType();
        logger.debug("Validating pageThree mimeType=" + mimeType);
        //System.out.println("Uploaded filesize=" + photoFile.getFileSize());
        logger.debug("Uploaded filesize=" + photoFile.getFileSize());
        
        if (photoFile == null || photoFile.getFileSize()<1) {
            //System.out.println("Checking zero filezie");
            logger.debug("Checking zero filezie");
            errors.add("photoPath",new ActionError("common.invalid.photoPath"));
        } else if (photoFile.getFileSize() > 250000) { //todo 1MB (1000000), change smaller and provide notes
            //System.out.println("Check not over filesize");
            logger.debug("Check not over filesize");
            errors.add("photoPath",new ActionError("common.invalid.photoPathSize"));
        } else if (!PhotoUploadHelper.isAcceptedMimeType(mimeType)) {
            //System.out.println("mimetype not accepted");
            logger.debug("mimetype not accepted");
            errors.add("photoPath",new ActionError("common.invalid.photoPathContentType"));
        } else {
            try {
                logger.debug("Checking magic content");
                Magic parser = new Magic() ;
                MagicMatch match;
                match = parser.getMagicMatch(photoFile.getFileData());
                logger.debug("Actual file mimetype=" + match.getMimeType()) ;
                
                //if (!match.mimeTypeMatches(mimeType)) { 
                // the actual mimetype string might not match
                // so we take if it is of accepted mimetype
                if (!PhotoUploadHelper.isAcceptedMimeType(match.getMimeType())) {
                    logger.debug("Mimetype of actual file not match"); 
                    errors.add("photoPath",new ActionError("common.mismatch.photoPathMime"));
                }
                
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
                errors.add("photoPath",new ActionError("common.invalid.photoPath"));
            } catch (MagicMatchNotFoundException ex) {
                ex.printStackTrace();
                errors.add("photoPath",new ActionError("common.mismatch.photoPathMime"));
            } catch (MagicParseException ex) {
                ex.printStackTrace();
                errors.add("photoPath",new ActionError("common.mismatch.photoPathMime"));
            } catch (IOException ex) {
                ex.printStackTrace();
                errors.add("photoPath",new ActionError("common.invalid.photoPath"));
            } catch (MagicException ex) {
                ex.printStackTrace();
                errors.add("photoPath",new ActionError("common.mismatch.photoPathMime"));
            }
        } 
        
            /*if (!Validator.validateMandatoryField(registerForm.getPhotoPath()))
            {
                errors.add("photoPath",new ActionError("common.invalid.photoPath"));
            }*/
        if (!Validator.validateString(registerForm.getPhotoDescription(),false,0,500,false)) {
            errors.add("photoDescription",new ActionError("common.invalid.photoDescription"));
        }
        
        return errors;
        
    }
    
    
    
    public static ActionErrors validatePageFour(ActionErrors errors, RegisterForm registerForm) {
         
        // if skip, just return
        if (PropertyConstants.BOOLEAN_YES == registerForm.getSkip4()) {
            return errors;
        }
        
        if (!Validator.validateEmail(registerForm.getEmailFwd1(),false)) {
            errors.add("emailFwd1",new ActionError("common.invalid.emailFwd1"));
        }
        if (!Validator.validateEmail(registerForm.getEmailFwd2(),false)) {
            errors.add("emailFwd2",new ActionError("common.invalid.emailFwd2"));
        }
        if (!Validator.validateEmail(registerForm.getEmailFwd3(),false)) {
            errors.add("emailFwd3",new ActionError("common.invalid.emailFwd3"));
        }
        
        return errors;
        
    }
    
    public static ActionErrors validateEmailLink(ActionErrors errors, RegisterForm registerForm) {
         
        if (!Validator.validateString(registerForm.getInvitedBy(),true,1,50)) {
            errors.add("validateError",new ActionError("common.invalid.emailLinkError"));
        } 
        else if (!Validator.validateString(registerForm.getInviteCode(),true,1,500)) {
            errors.add("validateError",new ActionError("common.invalid.emailLinkError"));
        }
        
        // Now that both fields are valid, we check against USER_INVITES table 
        if (errors.size() < 1) {
            InviteBO inviteBO = new InviteBO();
            boolean validInvite = inviteBO.validateInvite(registerForm);
            if (!validInvite)
            {
                errors.add("validateError",new ActionError("common.invalid.emailLinkError")); 
                registerForm.setInviteError(PropertyConstants.BOOLEAN_YES);
            }
            else
            {
                
                boolean validated = InviteBO.updateAcceptedInvite
                        (registerForm.getReferralId(),registerForm.getInviteCode(),registerForm.getEmailAddress());
            
                logger.debug("Set Email validated for userId" + registerForm.getUserId());
                UserBO.setEmailValidated(new Long(registerForm.getUserId()));
                errors.add("validateSuccess",new ActionError("common.invalid.emailLinkSuccess"));  
            }
            inviteBO = null;
        } 
        return errors;
    }

    public static ActionErrors validateNewRegistration(ActionErrors errors, RegisterForm registerForm) {
          
        if (!Validator.validateString(registerForm.getUserName(),true,1,50)) {
            errors.add("userName",new ActionError("common.invalid.userName"));
        }
        else
        {   String userName = registerForm.getUserName();
            // now checks format
            if (userName.length() < 4 || userName.length() > 12) {
                errors.add("userName", new ActionError("common.invalid.userName"));
            }
            else
            {
            
                // alpha numerics
                boolean foundInvalidChars = false;
                for(int i=0;i<userName.length();i++)
                {
                    int testStrIndex=userName.charAt(i);
                    if(testStrIndex<48 || (testStrIndex>57 && testStrIndex<65) || (testStrIndex>90 && testStrIndex<97)
                            || testStrIndex>122)
                    {
                         errors.add("userName", new ActionError("common.invalid.userName"));
                         i=userName.length();
                         foundInvalidChars = true;
                    }
                }
                if (!foundInvalidChars){
                    // if whole string numbers only
                    int foundNumbers = 0;
                    int userNameLength = userName.length();
                     for(int i=0;i<userNameLength;i++)
                    {
                        int testStrIndex=userName.charAt(i);
                        if(testStrIndex>47 && testStrIndex<58)
                        {
                            foundNumbers += 1;
                        }
                    }
                    if (foundNumbers == userNameLength){
                        errors.add("userName", new ActionError("common.invalid.userName"));
                    }
                }
            }
        }
        if (!Validator.validateEmail(registerForm.getEmailAddress(),true)) {
            errors.add("emailAddress",new ActionError("common.invalid.emailAddress"));
        }
        if (!Validator.validateString(registerForm.getPassword(),true,4,12)) {
            errors.add("password",new ActionError("common.invalid.password"));
        }
        if (!Validator.validateString(registerForm.getConfirmPassword(),true,4,12)) {
            errors.add("confirmPassword",new ActionError("common.invalid.confirmPassword"));
        }
        
        if (errors.size() < 1) {
            UserBO userBO = new UserBO();
            // Check if user name already exists
            if (userBO.usernameExists(registerForm.getUserName())) {
                errors.add("userName",new ActionError("common.exists.userName"));
            }
            // Check if not of reserved names list
            else if (ReservedNamesHelper.isReservedName(registerForm.getUserName())) {
                errors.add("userName",new ActionError("common.exists.userName"));
            } 
            
            
            // Check if email has been used
            if (userBO.emailExists(registerForm.getEmailAddress())) {
                errors.add("emailAddress",new ActionError("common.exists.emailAddress"));
            }
            
            userBO = null;
            
            // Checks password
            if (!registerForm.getPassword().equals(registerForm.getConfirmPassword())) {
                errors.add("confirmPasswordMismatch",new ActionError("common.mismatch.confirmPassword"));
            }
        }
        
        // Checks Recaptcha
        if (Recaptcha4JService.isEnabled())
        {
            try{
                ReCaptcha captcha = Recaptcha4JService.getReCaptchaInstance();
                String challengeField = registerForm.getRecaptcha_challenge_field();
                String responseField = registerForm.getRecaptcha_response_field();
                String ipAddress = registerForm.getActionContext().getRequest().getRemoteAddr();
                ReCaptchaResponse captchaResponse = captcha.checkAnswer(ipAddress, challengeField, responseField);
                logger.debug("Recaptcha - Challenge field:" + challengeField);
                logger.debug("Recaptcha - Response  field:" + responseField );  
                logger.debug("ReCaptcha response valid:" + captchaResponse.isValid()); 
                logger.debug("ReCaptcha response errorMessage:" + captchaResponse.getErrorMessage());
                
                if (!captchaResponse.isValid())
                {
                    errors.add("recaptchaResponse",new ActionError("common.recaptcha.invalidResponse")); 
                }
                
            }catch (Throwable t){
                logger.error("Error in resolving recaptcha, possible error in connection to server");
                errors.add("recaptchaResponse",new ActionError("common.recaptcha.error"));
            }
        }
        
        if (!Validator.validateString(registerForm.getName(),true,1,100)) {
            errors.add("name",new ActionError("common.invalid.name"));
        }
        if (!Validator.validateMandatoryField(Character.toString(registerForm.getGender()))) {
            errors.add("gender",new ActionError("common.invalid.gender"));
        }
        if (!Validator.validateString(registerForm.getBirthdayString(),true,10,10)) {
            errors.add("birthdayString",new ActionError("common.invalid.birthdayString"));
        }
        else
        {
            // Check is it between 14 and more
            if (DateObjectHelper.getAge(registerForm.getBirthDate().getTime()) < 14)
            { 
                errors.add("birthdayString",new ActionError("common.invalid.birthdayAgeTooYoung")); 
            } 
        }
        
        
        
        if (!Validator.validateString(registerForm.getCity(),false,0,100)) {
            errors.add("city",new ActionError("common.invalid.city"));
        }
       // if (!Validator.validateString(registerForm.getNationality(),true,4,12)) {
        if (!CountryComboHelper.isAcceptedCountryCode(registerForm.getNationality())){
            errors.add("nationality",new ActionError("common.invalid.nationality"));
        }
        /*else
        {
            if (registerForm.getNationality().equals(
                    PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY)))
            {
                // Country is accepted, check state 
                if (!StateComboHelper.isAcceptedStateCode(registerForm.getState())) {
                    errors.add("state",new ActionError("common.invalid.state"));
                }
            }
        }*/
        
        // if skip, just return
        if (PropertyConstants.BOOLEAN_YES != registerForm.getSkip3()) {
        
            // Validate photo type, photo size
            FormFile photoFile = registerForm.getPhotoFile();
            String mimeType = photoFile.getContentType();
            logger.debug("Validating pageThree mimeType=" + mimeType);
            //System.out.println("Uploaded filesize=" + photoFile.getFileSize());
            logger.debug("Uploaded filesize=" + photoFile.getFileSize());

            if (photoFile == null || photoFile.getFileSize()<1) {
                //System.out.println("Checking zero filezie");
                logger.debug("Checking zero filezie");
                errors.add("photoPath",new ActionError("common.invalid.photoPath"));
            } else if (photoFile.getFileSize() > 250000) { //todo 1MB (1000000), change smaller and provide notes
                //System.out.println("Check not over filesize");
                logger.debug("Check not over filesize");
                errors.add("photoPath",new ActionError("common.invalid.photoPathSize"));
            } else if (!PhotoUploadHelper.isAcceptedMimeType(mimeType)) {
                //System.out.println("mimetype not accepted");
                logger.debug("mimetype not accepted");
                errors.add("photoPath",new ActionError("common.invalid.photoPathContentType"));
            } else {
                try {
                    logger.debug("Checking magic content");
                    Magic parser = new Magic() ;
                    MagicMatch match;
                    match = parser.getMagicMatch(photoFile.getFileData());
                    logger.debug("Actual file mimetype=" + match.getMimeType()) ;

                    //if (!match.mimeTypeMatches(mimeType)) { 
                    // the actual mimetype string might not match
                    // so we take if it is of accepted mimetype
                    if (!PhotoUploadHelper.isAcceptedMimeType(match.getMimeType())) {
                        logger.debug("Mimetype of actual file not match"); 
                        errors.add("photoPath",new ActionError("common.mismatch.photoPathMime"));
                    }

                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                    errors.add("photoPath",new ActionError("common.invalid.photoPath"));
                } catch (MagicMatchNotFoundException ex) {
                    ex.printStackTrace();
                    errors.add("photoPath",new ActionError("common.mismatch.photoPathMime"));
                } catch (MagicParseException ex) {
                    ex.printStackTrace();
                    errors.add("photoPath",new ActionError("common.mismatch.photoPathMime"));
                } catch (IOException ex) {
                    ex.printStackTrace();
                    errors.add("photoPath",new ActionError("common.invalid.photoPath"));
                } catch (MagicException ex) {
                    ex.printStackTrace();
                    errors.add("photoPath",new ActionError("common.mismatch.photoPathMime"));
                }
            } 
 
            if (!Validator.validateString(registerForm.getPhotoDescription(),false,0,500,false)) {
                errors.add("photoDescription",new ActionError("common.invalid.photoDescription"));
            }
            
        }// Skip3 Ends
        
        
        
        return errors;
    }
    
}