/*
 * ForumAction.java
 *
 * Created on October 18, 2008, 2:14 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.ForumTopic;
import com.esmart2u.oeight.member.bo.ForumBO;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.logging.Logger;
import java.util.HashMap;
import java.util.List;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
 
/**
 *
 * @author meauchyuan.lee
 */
public class ForumAction extends AbstractApplicationAction {
     
    private static Logger logger = Logger.getLogger(ForumAction.class);
    
    // Common page
    private static String NOT_FOUND_PAGE = "notFound";
     
    private static String MARVIN_FORUM_INPUT = "marvinForumAdd";
    private static String MARVIN_FORUM_SUBMITTED = "marvinForumAdded";
    private static String MARVIN_FORUM_REFRESH = "marvinForumRefresh";
    
    /** Creates a new instance of ForumAction */
    public ForumAction() {
    }
          
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (MARVIN_FORUM_SUBMITTED.equals(event))
            return true;
        
        
        return result;
    }
       
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        ForumForm forumForm = (ForumForm)actionForm; 
        if (MARVIN_FORUM_SUBMITTED.equals(action)) {
            //errors = FeedbackValidator.validateFeedback(errors, feedbackForm); 
        } else {
            logger.debug("Temporary skip");
        }
        
        forumForm.getActionContext().setActionErrors(errors);
        return forumForm;
    }
    
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (MARVIN_FORUM_SUBMITTED.equals(action)) {
            action = MARVIN_FORUM_INPUT; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    protected String getDefaultActionName() {
                  
        return MARVIN_FORUM_INPUT;
    }
    
    public String doMarvinForumAdd(ForumForm actionForm)
    throws ApplicationException {
    try {  
            // if user not marvin, throw
            String userId = checkUserAccess(actionForm);
            if (Integer.parseInt(userId) != PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY))
            {
                return NOT_FOUND_PAGE;
            } 
            
            ForumBO forumBO = new ForumBO(); 
            List forumDropDown = forumBO.getForumDropDown();
            actionForm.setForumDropDown(forumDropDown);

            if (actionForm.getForumId()>0)
            {
                List topicDropDown = forumBO.getTopicDropDown(actionForm.getForumId());
                actionForm.setTopicDropDown(topicDropDown);
            }  
            
            forumBO = null;
            
            return MARVIN_FORUM_INPUT;
        } catch (Exception e) {
            e.printStackTrace();
            return NOT_FOUND_PAGE;
        }
    }
    
    public String doMarvinForumAdded(ForumForm actionForm)
    throws ApplicationException {
    try {  
            // if user not marvin, throw
            String userId = checkUserAccess(actionForm);
            if (Integer.parseInt(userId) != PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY))
            {
                return NOT_FOUND_PAGE;
            }
            
            logger.debug("Forum add topic : Forum:" + actionForm.getForumId());
            logger.debug("Forum add topic : Topic:" + actionForm.getTopicId());
            logger.debug("Forum add topic : Url:" + actionForm.getTopicUrl());
            logger.debug("Forum add topic : Content:" + actionForm.getTopicPost());
            
            ForumBO forumBO = new ForumBO(); 
            ForumTopic forumTopic = new ForumTopic();
            forumTopic.setForumId(actionForm.getForumId());
            forumTopic.setTopicId(actionForm.getTopicId());
            forumTopic.setTopicUrl(actionForm.getTopicUrl());
            forumTopic.setTopicPost(actionForm.getTopicPost());
            HashMap forumMap = forumBO.addNewForumTopic(forumTopic);
            actionForm.setForumMap(forumMap);
            
            forumBO = null;
            
            return MARVIN_FORUM_SUBMITTED;
        } catch (Exception e) {
            return NOT_FOUND_PAGE;
        }
    }
    
     
    public String doMarvinForumRefresh(ForumForm actionForm)
    throws ApplicationException {
    try {  
            // if user not marvin, throw
            String userId = checkUserAccess(actionForm);
            if (Integer.parseInt(userId) != PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY))
            {
                return NOT_FOUND_PAGE;
            }
         
            logger.debug("Forum refresh Content");
             
            ForumBO forumBO = new ForumBO(); 
            ForumTopic forumTopic = new ForumTopic(); 
            HashMap forumMap = forumBO.refreshForumTopic();
            actionForm.setForumMap(forumMap);
            
            forumBO = null;
            
            return MARVIN_FORUM_REFRESH;
        } catch (Exception e) {
            return NOT_FOUND_PAGE;
        }
    }
}
