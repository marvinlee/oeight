/*
 * GuideAction.java
 *
 * Created on January 8, 2008, 9:35 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class GuideAction  extends AbstractApplicationAction {
      
    private static String MAIN = "main";
    
    /** Creates a new instance of GuideAction */
    public GuideAction() {
    }
       /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(GuideAction.class);
    
    
    
    protected boolean isValidationRequired(String event) { 
        
        return false;
    } 
    
     
    
    
    protected String getDefaultActionName() {
                  
        return MAIN;
    }
     
    public String doMain(CommonForm actionForm){ 
        return MAIN;  
    } 
}
