/*
 * EarthForm.java
 *
 * Created on May 12, 2008, 3:42 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.OEightPledge;
import com.esmart2u.oeight.member.web.struts.helper.Page;
import com.esmart2u.solution.base.helper.BeanUtils;
import com.esmart2u.solution.base.helper.PropertyConstants;
import java.util.Date;
import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class EarthForm  extends AbstractApplicationActionForm {
    
    private long pledgeId;
    private long userId;
    private String userName; // oeightId
    private String pledgerName;
    private Date pledgeDate;
    private char memberFlag = PropertyConstants.BOOLEAN_NO;
    private int pledgeCode;
    private int pledgeFrom; // 0 from oEight, 1 from Friendster, 2 from Facebook
    private boolean showPledgeFrom = true;
    private long pledgeFromUserId; 
    
    private String user_id; // Friendster Id
    private String fb_user; // Facebook Id
     
    public static final int PLEDGE_FROM_OEIGHT = 0;
    public static final int PLEDGE_FROM_FRIENDSTER = 1;
    public static final int PLEDGE_FROM_FACEBOOK = 2;
    
    private boolean pledgeSaved = false;
    
    private long totalAccepted;
    
    // Listing
    private int currentPage;
    private int totalPage;
    private List resultList;
     
    private Page pledgeListPage;
    
    // For contact list retriever
    private String emailUser;
    private String provider;
    private String password;
    
    // For list 
    private List contactEmailList;
    private String[] contactEmailString;  
    
    // For invite
    private String from;
    private String to;
    
    // Manual invites
    private String emails;
    private List addressList; 
    
    // Blog contest 
    private String blogUrl;
    private String name;
    private String contactNo;
    private String email; 
    
    /** Creates a new instance of EarthForm */
    public EarthForm() {
    }

    public long getPledgeId() {
        return pledgeId;
    }

    public void setPledgeId(long pledgeId) {
        this.pledgeId = pledgeId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getPledgerName() {
        return pledgerName;
    }

    public void setPledgerName(String pledgerName) {
        this.pledgerName = pledgerName;
    }

    public Date getPledgeDate() {
        return pledgeDate;
    }

    public void setPledgeDate(Date pledgeDate) {
        this.pledgeDate = pledgeDate;
    }

    public char getMemberFlag() {
        return memberFlag;
    }

    public void setMemberFlag(char memberFlag) {
        this.memberFlag = memberFlag;
    }

    public int getPledgeCode() {
        return pledgeCode;
    }

    public void setPledgeCode(int pledgeCode) {
        this.pledgeCode = pledgeCode;
    }

    public int getPledgeFrom() {
        return pledgeFrom;
    }

    public void setPledgeFrom(int pledgeFrom) {
        this.pledgeFrom = pledgeFrom;
    }

    public boolean isShowPledgeFrom() {
        return showPledgeFrom;
    }

    public void setShowPledgeFrom(boolean showPledgeFrom) {
        this.showPledgeFrom = showPledgeFrom;
    }

    public long getPledgeFromUserId() {
        switch(pledgeFrom)
        {
            case PLEDGE_FROM_FRIENDSTER: setPledgeFromUserId(Long.parseLong(getUser_id()));
                                        break;
            case PLEDGE_FROM_FACEBOOK: setPledgeFromUserId(Long.parseLong(getFb_user()));
                                        break;
        }
        return pledgeFromUserId;
    }

    public void setPledgeFromUserId(long pledgeFromUserId) {
        this.pledgeFromUserId = pledgeFromUserId;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFb_user() {
        return fb_user;
    }

    public void setFb_user(String fb_user) {
        this.fb_user = fb_user;
    }
      
    public OEightPledge toOEightPledgeObject() throws Exception {
        OEightPledge oeightPledge = new OEightPledge();
        BeanUtils.copyBean(this, oeightPledge);
        return oeightPledge;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List getResultList() {
        return resultList;
    }

    public void setResultList(List resultList) {
        this.resultList = resultList;
    }

    public Page getPledgeListPage() {
        return pledgeListPage;
    }

    public void setPledgeListPage(Page pledgeListPage) {
        this.pledgeListPage = pledgeListPage;
    }

    public boolean getPledgeSaved() {
        return isPledgeSaved();
    }
    
    public boolean isPledgeSaved() {
        return pledgeSaved;
    }

    public void setPledgeSaved(boolean pledgeSaved) {
        this.pledgeSaved = pledgeSaved;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List getContactEmailList() {
        return contactEmailList;
    }

    public void setContactEmailList(List contactEmailList) {
        this.contactEmailList = contactEmailList;
    }

    public String[] getContactEmailString() {
        return contactEmailString;
    }

    public void setContactEmailString(String[] contactEmailString) {
        this.contactEmailString = contactEmailString;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public List getAddressList() {
        return addressList;
    }

    public void setAddressList(List addressList) {
        this.addressList = addressList;
    }

    public long getTotalAccepted() {
        return totalAccepted;
    }

    public void setTotalAccepted(long totalAccepted) {
        this.totalAccepted = totalAccepted;
    }

    public String getBlogUrl() {
        return blogUrl;
    }

    public void setBlogUrl(String blogUrl) {
        this.blogUrl = blogUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
}
