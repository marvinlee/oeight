/*
 * MessageForm.java
 *
 * Created on March 18, 2008, 4:46 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.member.vo.MessageVO;
import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class MessageForm  extends AbstractApplicationActionForm {
    
    
    /** Creates a new instance of MessageForm */
    public MessageForm() {
    }
    
    private long userId;
    private String userName;
    private String recipientUserName;
    private String subject;
    private String message;
    private boolean isSaved;
    private String nonceToken; // md5 of userName + timeStamp, session key: "msg+userName"
    
    // Message List, Delete, View
    private List messageList;
    private String[] messageIdString; // for delete
    private String msgId; // for single view
    private MessageVO messageVO;
    
    // New Message
    private String to; //userName
    private String photoSmallPath;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRecipientUserName() {
        return recipientUserName;
    }

    public void setRecipientUserName(String recipientUserName) {
        this.recipientUserName = recipientUserName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getIsSaved() {
        return isSaved;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setIsSaved(boolean isSaved) {
        this.isSaved = isSaved;
    }

    public String getNonceToken() {
        return nonceToken;
    }

    public void setNonceToken(String nonceToken) {
        this.nonceToken = nonceToken;
    }

    public List getMessageList() {
        return messageList;
    }

    public void setMessageList(List messageList) {
        this.messageList = messageList;
    }

    public String[] getMessageIdString() {
        return messageIdString;
    }

    public void setMessageIdString(String[] messageIdString) {
        this.messageIdString = messageIdString;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public MessageVO getMessageVO() {
        return messageVO;
    }

    public void setMessageVO(MessageVO messageVO) {
        this.messageVO = messageVO;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }

    
    
    
}
