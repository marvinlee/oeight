/*
 * MosaicHelper.java
 *
 * Created on October 3, 2007, 3:03 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.oeight.admin.helper.mosaic.MosaicCreator;
import com.esmart2u.oeight.admin.helper.mosaic.MosaicDAO;
import com.esmart2u.oeight.data.MosaicCurrent;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author meauchyuan.lee
 */
public class MosaicHelper {
    
    private static MosaicHelper instance = null;
    
    private String currentMosaicId = "";
    private int currentTotalMosaics = 1;
    private List currentMosaics = null;
    
    private final Timer timer = new Timer();
    
    // Generate every 15 mins
    private boolean firstTime = true;
    private static boolean enabled = PropertyManager.getBoolean(PropertyConstants.MOSAIC_GENERATE_ENABLE,false); 
    private final int minutes = PropertyManager.getInt(PropertyConstants.MOSAIC_GENERATE_FREQUENCY,30); 

    /*
     * The file name is in format : yyyyMMddHH_i.png or yyyyMMddHH_i.map
     * Where i is index of the mosaic and the suffix depends on the file extension or file type
     */
    private String currentMosaicFileNamePrefix = "";
    
    /** Creates a new instance of MosaicHelper */
    protected MosaicHelper() {
        //initializeMosaics();
        
    }
    
    private void initializeMosaics(){
        // Get MosaicCurrent
        MosaicCurrent mosaicCurrent = MosaicDAO.getMosaicCurrent();
        currentMosaicId = Long.toString(mosaicCurrent.getMosaicId());
        
        // Get Mosaic
        currentMosaics = MosaicDAO.getCurrentMosaicsList(mosaicCurrent.getMosaicId());
        currentTotalMosaics = currentMosaics.size();
    }
    
    public synchronized static MosaicHelper getInstance() {
        if(instance == null) {
            instance = new MosaicHelper();
            if (enabled){
                instance.start();
            }
        }
        return instance;
    }
     
    
    public static void reload()
    {
        getInstance().initializeMosaics();
    } 
    
    
    public static String getCurrentMosaicId() {
        return getInstance().currentMosaicId;
    }

    private void setCurrentMosaicId(String currentMosaicId) {
        this.currentMosaicId = currentMosaicId;
    }

    public static int getCurrentTotalMosaics() {
        return getInstance().currentTotalMosaics;
    }

    private void setCurrentTotalMosaics(int currentTotalMosaics) {
        this.currentTotalMosaics = currentTotalMosaics;
    }

    public static List getCurrentMosaics() {
        return getInstance().currentMosaics;
    }

    private void setCurrentMosaics(List currentMosaics) {
        this.currentMosaics = currentMosaics;
    }

    public static String getCurrentMosaicFileNamePrefix() {
        return getInstance().currentMosaicFileNamePrefix;
    }

    private void setCurrentMosaicFileNamePrefix(String currentMosaicFileNamePrefix) {
        this.currentMosaicFileNamePrefix = currentMosaicFileNamePrefix;
    }
    
    public static void main(String[] args) {
        getInstance();
    }
    
    private void start() {
        if (enabled){
            long runningTime = minutes * 60 * 1000;
            if (firstTime){
                runningTime = 100;
                firstTime = false; 
            }

            timer.schedule(new TimerTask() {
                public void run() {
                    System.out.println("Mosaic Timer task starting at " + new Date());
                    regenerateMosaic();
                    System.out.println("Done regenerating Mosaic at " + new Date());
                    reload();
                    System.out.println("MosaicHelper reloaded! Ready to serve.");
                    getInstance().start();
                    // Do what you need to do
                }
                private void regenerateMosaic() {  
                    MosaicCreator demo = new MosaicCreator();
                }
            }, runningTime);
        }
    }

}
