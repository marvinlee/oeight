/*
 * CountryComboHelper.java
 *
 * Created on September 30, 2007, 6:49 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.solution.base.helper.StringUtils;
import java.util.*;

/**
 *
 * @author meauchyuan.lee
 */
public class CountryComboHelper {
    
    private static String[] countryCodes = new String[]{"AF","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AT","AZ","BS","BH","BD","BB","BY","BE","BZ","BJ","BM","BT","BO","BA","BW","BV","BR","IO","BN","BG","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CG","CK","CR","CI","HR","CU","CY","CZ","CS","DK","DJ","DM","DO","TL","EC","EG","SV","GQ","ER","EE","ET","FK","FO","FJ","FI","FR","FX","GF","PF","TF","GA","GM","GE","DE","GH","GI","GB","GR","GL","GD","GP","GU","GT","GN","GW","GY","HT","HM","HN","HU","IS","IN","ID","IR","IQ","IE","IL","IT","JM","JP","JO","KZ","KE","KI","KP","KR","KW","KG","LA","LV","LB","LS","LR","LY","LI","LT","LU","MK","MG","MW","MY","MV","ML","MT","MH","MQ","MR","MU","YT","MX","FM","MD","MC","MN","MS","MA","MZ","MM","NA","NR","NP","NL","AN","NT","NC","NZ","NI","NE","NG","NU","NF","MP","NO","OM","PK","PW","PA","PG","PY","PE","PH","PN","PL","PT","PR","QA","RE","RO","RU","RW","GS","KN","LC","VC","WS","SM","ST","SA","SN","SC","SL","SG","SK","SI","Sb","SO","ZA","ES","LK","SH","PM","SD","SR","SJ","SE","CH","TJ","TZ","TH","TG","TK","TO","TT","TN","TR","TM","TC","TV","UG","UA","AE","UK","US","UY","UM","SU","UZ","VU","VA","VE","VN","VG","VI","WF","EH","YE","YU","ZR","ZM","ZW","TW","MO","HK"};
    private static String[] countryLabels = new String[]{"Afghanistan","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Congo","Cook Islands","Costa Rica","Cote D'Ivoire (Ivory Coast)","Croatia (Hrvatska)","Cuba","Cyprus","Czech Republic","Czechoslovakia (former)","Denmark","Djibouti","Dominica","Dominican Republic","East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands (Malvinas)","Faroe Islands","Fiji","Finland","France","France, Metropolitan","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Great Britain (UK)","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guinea","Guinea-Bissau","Guyana","Haiti","Heard and McDonald Islands","Honduras","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy","Jamaica","Japan","Jordan","Kazakhstan","Kenya","Kiribati","Korea (North)","Korea (South)","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","Neutral Zone","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russian Federation","Rwanda","S. Georgia and S. Sandwich Isls.","Saint Kitts and Nevis","Saint Lucia","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Seychelles","Sierra Leone","Singapore","Slovak Republic","Slovenia","Solomon Islands","Somalia","South Africa","Spain","Sri Lanka","St. Helena","St. Pierre and Miquelon","Sudan","Suriname","Svalbard and Jan Mayen Islands</option>option value='SZ'>Swaziland","Sweden","Switzerland</option>option value='SY'>Syria","Tajikistan","Tanzania","Thailand","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","Uruguay","US Minor Outlying Islands","USSR (former)","Uzbekistan","Vanuatu","Vatican City State (Holy Sea)","Venezuela","Viet Nam","Virgin Islands (British)","Virgin Islands (U.S.)","Wallis and Futuna Islands","Western Sahara","Yemen","Yugoslavia","Zaire","Zambia","Zimbabwe","Taiwan","Macau","Hong Kong"};
    private static Vector countryList;// = getCountryCombo();
    private static Vector countryCodesVector;
    
    /** Creates a new instance of CountryComboHelper */
    public CountryComboHelper() {
    }
    
    /*private static Vector getCountryCombo() {
        TreeMap countryMap = new TreeMap();
        System.out.println("Getting locales");
        Locale[] locale = Locale.getAvailableLocales();
        
        for (int i=0;i<locale.length;i++) {
            String country = locale[i].getDisplayCountry();
            System.out.println("Country=" + country);
            if (country.length() > 0) {
                countryMap.put(locale[i].getCountry(),country);
            }
        }
        System.out.println("returnMap to set");
        Vector returnMap = new Vector();
        Set set = countryMap.keySet();
        Iterator keyIterator = set.iterator();
        System.out.println("going to iterate");
        while (keyIterator.hasNext()) {
            System.out.println("putting country");
            String[] countries = new String[2];
            countries[0] = (String)keyIterator.next();
            countries[1] = (String)countryMap.get(countries[0]);
            System.out.println("Added " + countries[0] + countries[1]);
            returnMap.add(countries);
            System.out.println("countries added");
            //returnMap.put(key, countryMap.get(key));
        }
        return returnMap;
    }*/
    
    public static Vector getCountryList(){
        if (countryList == null || countryList.size() <1)
        {
            countryList = getCountryCombo(); 
        } 
        return countryList;
    }
    
    private static Vector getCountryCombo()
    {
        Vector countries = new Vector();
        countryCodesVector = new Vector();
        for(int i=0;i<countryCodes.length;i++){
            String[] value = new String[2];
            value[0] = countryCodes[i];
            value[1] = countryLabels[i];
            countries.add(value);
            countryCodesVector.add(countryCodes[i]);
        }
        return countries;
    }
    
    public static boolean isAcceptedCountryCode(String countryCode)
    {
        if (StringUtils.hasValue(countryCode) && countryCodesVector.contains(countryCode))
            return true;
        else
            return false;
    }
    
    public static String getCountryLabelByCode(String countryCode)
    {
        for(int i=0;i<countryCodes.length;i++){ 
            if (countryCode.equals(countryCodes[i]))
            {
                return countryLabels[i];
            }
        }
        return null;
    }
}
