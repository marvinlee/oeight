/*
 * EmailProviderComboHelper.java
 *
 * Created on May 23, 2008, 4:09 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.solution.base.helper.StringUtils;
import java.util.Vector;

/**
 *
 * @author meauchyuan.lee
 */
public class EmailProviderComboHelper {
      
    private static String[] providerCodes = new String[]{"","gmail.com","googlemail.com","hotmail.com","msn.com","live.com","live.com.my","yahoo.com","yahoo.com.my"};
    private static String[] providerLabels = new String[]{"","@gmail.com","@googlemail.com","@hotmail.com","@msn.com","@live.com","@live.com.my","@yahoo.com","@yahoo.com.my"};
    private static Vector providerList;
    private static Vector providerCodesVector;
    
    /** Creates a new instance of EmailProviderComboHelper */
    public EmailProviderComboHelper() {
    }
     
    public static Vector getProviderList(){
        if (providerList == null || providerList.size() <1)
        {
            providerList = getProviderCombo(); 
        } 
        return providerList;
    }
    
    private static Vector getProviderCombo()
    {
        Vector provider = new Vector();
        providerCodesVector = new Vector();
        for(int i=0;i<providerCodes.length;i++){
            String[] value = new String[2];
            value[0] = providerCodes[i];
            value[1] = providerLabels[i];
            provider.add(value);
            providerCodesVector.add(providerCodes[i]);
        }
        return provider;
    }
    
    public static boolean isAcceptedProviderCode(String providerCode)
    {
        if (StringUtils.hasValue(providerCode) && providerCodesVector.contains(providerCode))
            return true;
        else
            return false;
    }
    
    public static String getStatusLabelByCode(String providerCode)
    {
        for(int i=0;i<providerCodes.length;i++){ 
            if (providerCode.equals(providerCodes[i]))
            {
                return providerLabels[i];
            }
        }
        return null;
    }
    
    public static void main(String[] args) {
        Vector list = EmailProviderComboHelper.getProviderList();
        System.out.println("list size;" + list.size());
    }
}
