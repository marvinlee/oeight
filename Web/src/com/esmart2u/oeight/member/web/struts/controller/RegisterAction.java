/*
 * RegisterAction.java
 *
 * Created on September 20, 2007, 11:55 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.phpbb.PhpBbUsers;
import com.esmart2u.oeight.member.bo.ForumBO;
import com.esmart2u.oeight.member.bo.InviteBO;
import com.esmart2u.oeight.member.bo.RankingBO;
import com.esmart2u.oeight.member.bo.RegisterBO;
import com.esmart2u.oeight.member.bo.RegisterBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.AppConstants;
import com.esmart2u.solution.base.helper.AppContext;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PhotoFile;
import com.esmart2u.solution.base.helper.PhotoUploadHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import javax.mail.Session;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.esmart2u.solution.base.helper.Validator;
/**
 *
 * @author meauchyuan.lee
 */
public class RegisterAction extends AbstractApplicationAction {
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(RegisterAction.class);
    
    private static String FIRST_PAGE = "one";
    private static String SECOND_PAGE = "two";
    private static String THIRD_PAGE = "three";
    private static String FOURTH_PAGE = "four";
    
    private static String VALIDATED_PAGE = "validated";
    
    private static String REGISTER_FORM_PAGE = "new";
    private static String REGISTER_SUBMITTED_PAGE = "submit";  
    
    public String doMain(RegisterForm actionForm)
    throws ApplicationException {
        logger.debug("Going back to first page");
        logger.debug("Errors=" + actionForm.getActionContext().getActionErrors().size());
        //actionForm.clear(); 
        //return FIRST_PAGE;
        return doNew(actionForm);
    }
    
    
    public String doNew(RegisterForm actionForm)
    throws ApplicationException {
        logger.debug("New user page");
        //logger.debug("Errors=" + actionForm.getActionContext().getActionErrors().size());
        
        // If error occurs, clear form?
        //  - Clear if is invitation error
        //  - Do not clear if is other errors
        if (PropertyConstants.BOOLEAN_YES == actionForm.getInviteError()) {
            //System.out.println("Clear action Form, invitation invalid");
            //actionForm.clear();
            
            //if (PropertyConstants.BOOLEAN_YES == actionForm.getFromRequest())
            //{
                actionForm.setReferralId(""+PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY));
                actionForm.setReferralName(ConfigurationHelper.getDomainName()); 
            //}
            //else
            //{
            //    actionForm.setReferralId(null);
            //    actionForm.setReferralName(null); 
            //}
            actionForm.setUserId(0); 
            actionForm.setInvitedBy(null);
            actionForm.setInviteCode(null);
            actionForm.setEmailAddress(null);
            actionForm.setUserName(null);
            actionForm.setPassword(null);
            actionForm.setConfirmPassword(null);
            actionForm.setSecureCode(null);
            actionForm.setInviteError(PropertyConstants.BOOLEAN_NO);
            
        } else {
            //System.out.println("Referral error is null");
            // Get invitedBy and find validity with userId and inviteCode
            String invitedBy = actionForm.getInvitedBy();
            String inviteCode = actionForm.getInviteCode();
            //logger.debug("Invited by:" + invitedBy);
            //logger.debug("Invited cd:" + inviteCode);
            //logger.debug("RegisterAction doInvited email=" + actionForm.getEmailAddress());
            UserBO userBO = new UserBO();
            User user = userBO.getUserByUserName(invitedBy);
            
            // Should've been validated to be exists
            String referralId = Long.toString(user.getUserId());
            actionForm.setReferralId(referralId);
            String referralName = user.getUserCountry().getName();
            actionForm.setReferralName(referralName);
            actionForm.setInviteError(PropertyConstants.BOOLEAN_NO);
        }
        
        return REGISTER_FORM_PAGE;
    }
    
    
    public String doSubmit(RegisterForm actionForm)
    throws ApplicationException {
        logger.debug("User form submission");
        try {
            logger.debug("Set password");
            String applicationId = (String)((AppContext)actionForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            //logger.debug("Session Id=" + applicationId);
            // generate a new token and persist it for multiple submission checking
            generateAndPersistToken(actionForm);

            // Set Password to MD5
            if (!StringUtils.hasValue(actionForm.getSecureCode())){
                actionForm.setSecureCodeFromPassword();
                actionForm.setPassword(null);
                actionForm.setConfirmPassword(null);
            } 
            
            
             // Photo is validated, upload and save photo to server, link id to the user   
            if (PropertyConstants.BOOLEAN_YES != actionForm.getSkip3()) {
        
                try{
                    PhotoFile photoFile = PhotoUploadHelper.
                            uploadMosaicPhoto(actionForm.getPhotoFile(), actionForm.getUserName());

                    actionForm.setPhotoPath(photoFile.getFilePath());
                    actionForm.setPhotoLargePath(photoFile.getFileName());
                    actionForm.setPhotoSmallPath(photoFile.getThumbnailFileName());

                }catch (Exception e) {
                    // Add error message
                }
            
            }else
            { 
                // Sets default photo values 
                actionForm.setPhotoLargePath(PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_LARGE));
                actionForm.setPhotoSmallPath(PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL));
            } 
            
            
            // Do persistence
            //logger.debug("If reach here then proceed to persist into DB");
            User user = actionForm.toUserObject();
            String userName = user.getUserName();
            UserCountry userCountry = actionForm.toUserCountryObject();
            RegisterBO registerBO = new RegisterBO();
            Long userId = registerBO.register(user, userCountry);
            //logger.debug(" ### User id to set into session=" + userId);
            registerBO = null;
            
            logger.debug("Debug \n" + actionForm.printValues());
            
            // Update the referral, set invite been accepted and add referrer counter  
            boolean validated = InviteBO.updateAcceptedInvite(actionForm.getReferralId(),actionForm.getInviteCode(),actionForm.getEmailAddress());
            if (validated)
            {
                UserBO.setEmailValidated(userId);
                actionForm.setEmailValidated(PropertyConstants.BOOLEAN_YES);
            }
            RankingBO.increaseInvitesAccepted(actionForm.getReferralId());
            // Auto increase vote
            RankingBO.increaseVotes(userId.toString()); 
            
            // Clear session form after done
            actionForm.clear();
            
            // Keep the username to proceed to profile page
            actionForm.setUserName(userName);
            
            // Set User Id to session
            String sessionId = actionForm.getActionContext().getRequest().getSession().getId(); 
            actionForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_USER_ID, userId.toString()); 
            actionForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_USER_NAME, userName);
            actionForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_APPLICATION_ID_KEY, sessionId.toUpperCase());
                            
            // Sets Forum integration and cookie
            ForumBO forumBO = new ForumBO();
            forumBO.registerForumUser(user, userCountry);
            
            // Set forum login
            setForumLoginCookies(actionForm, sessionId);
            
            return REGISTER_SUBMITTED_PAGE;
        
        } catch (Exception e) {
            return rethrow(e, REGISTER_FORM_PAGE);
        }
        
    }
    
    private void setForumLoginCookies(RegisterForm registerForm, String sessionId) throws Exception
    { 
            Cookie[] cookies = registerForm.getActionContext().getRequest().getCookies();
            //System.out.println("Try to print cookies");
            if (cookies != null)
            {
               for(int i=0;i<cookies.length; i++)
               {
                    Cookie cookie = (Cookie)cookies[i];
                    //System.out.println("Cookie : " + cookie.getDomain() + ":"+ cookie.getName() + ":" + cookie.getValue());
                    cookie.setDomain("." + ConfigurationHelper.getDomainName());
                    cookie.setPath("/");
                    cookie.setMaxAge(-1);//Not stored persistently, deletes when exited 
                    // Remove phpbb2mysql_sid if found
                    if (cookie.getName().equals(OEightConstants.PHPBB_COOKIE_SESSION_ID_KEY))
                    {
                        //System.out.println("Found cookie to delete!!");
                        cookie.setValue(sessionId.toLowerCase());
                        //cookie.setMaxAge(0);// Delete cookie
                    }
                    
                    registerForm.getActionContext().getResponse().addCookie(cookie);
               }
            
            }
            // Sets session id to cookie and save to phpbb
            Cookie phpbbCookie = new Cookie(OEightConstants.PHPBB_COOKIE_SESSION_ID_KEY, sessionId.toLowerCase());
            phpbbCookie.setDomain("." + ConfigurationHelper.getDomainName());
            phpbbCookie.setPath("/");
            phpbbCookie.setMaxAge(-1);//Not stored persistently, deletes when exited
            //System.out.println("New Cookie1 Set " + phpbbCookie.getDomain() + ":" + phpbbCookie.getName() + ":" + phpbbCookie.getValue() );
            ForumBO forumBO = new ForumBO();
            PhpBbUsers phpBbUser = forumBO.performForumLogin(registerForm.getEmailAddress(), sessionId.toLowerCase());
            forumBO = null;
            registerForm.getActionContext().getResponse().addCookie(phpbbCookie);
            //System.out.println("Cookie1 set!!");
            
            if (phpBbUser != null){
                phpbbCookie = new Cookie(OEightConstants.PHPBB_COOKIE_USER_ID_KEY, "" + phpBbUser.getUserId());
                phpbbCookie.setDomain("." + ConfigurationHelper.getDomainName());
                phpbbCookie.setPath("/");
                phpbbCookie.setMaxAge(-1);//Not stored persistently, deletes when exited
                //System.out.println("New Cookie1 Set " + phpbbCookie.getDomain() + ":" + phpbbCookie.getName() + ":" + phpbbCookie.getValue() );

                registerForm.getActionContext().getResponse().addCookie(phpbbCookie);
            }
            
    }
    
    public String doValidated(RegisterForm actionForm)
    throws ApplicationException {
        return VALIDATED_PAGE;
    }
     
    public String doInvited(RegisterForm actionForm)
    throws ApplicationException {
        logger.debug("Requested from invites");
        //logger.debug("Errors=" + actionForm.getActionContext().getActionErrors().size());
        
        // If error occurs, clear form?
        //  - Clear if is invitation error
        //  - Do not clear if is other errors
        if (PropertyConstants.BOOLEAN_YES == actionForm.getInviteError()) {
            //System.out.println("Clear action Form, invitation invalid");
            //actionForm.clear();
            
            if (PropertyConstants.BOOLEAN_YES == actionForm.getFromRequest())
            {
                actionForm.setReferralId(""+PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY));
                actionForm.setReferralName(ConfigurationHelper.getDomainName()); 
            }
            else
            {
                actionForm.setReferralId(null);
                actionForm.setReferralName(null); 
            }
            actionForm.setUserId(0); 
            actionForm.setInvitedBy(null);
            actionForm.setInviteCode(null);
            actionForm.setEmailAddress(null);
            actionForm.setUserName(null);
            actionForm.setPassword(null);
            actionForm.setConfirmPassword(null);
            actionForm.setSecureCode(null);
            actionForm.setInviteError(PropertyConstants.BOOLEAN_NO);
            
        } else {
            //System.out.println("Referral error is null");
            // Get invitedBy and find validity with userId and inviteCode
            String invitedBy = actionForm.getInvitedBy();
            String inviteCode = actionForm.getInviteCode();
            //logger.debug("Invited by:" + invitedBy);
            //logger.debug("Invited cd:" + inviteCode);
            //logger.debug("RegisterAction doInvited email=" + actionForm.getEmailAddress());
            UserBO userBO = new UserBO();
            User user = userBO.getUserByUserName(invitedBy);
            
            // Should've been validated to be exists
            String referralId = Long.toString(user.getUserId());
            actionForm.setReferralId(referralId);
            String referralName = user.getUserCountry().getName();
            actionForm.setReferralName(referralName);
            actionForm.setInviteError(PropertyConstants.BOOLEAN_NO);
        }
        
        //return FIRST_PAGE;
        return REGISTER_FORM_PAGE;
    }  
    
    public String doDone(RegisterForm actionForm)
    throws ApplicationException {
        try { 
            
            String applicationId = (String)((AppContext)actionForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY); 
            
            
            return MAIN_PAGE;
        } catch (Exception e) {
            return rethrow(e, REGISTER_FORM_PAGE);
        }
    }
    
    /*
    public String doOne(RegisterForm actionForm)
    throws ApplicationException {
        try {
            logger.debug("Running doOne");
            String applicationId = (String)((AppContext)actionForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            //logger.debug("Session Id=" + applicationId);
            // generate a new token and persist it for multiple submission checking
            generateAndPersistToken(actionForm);
            
            // Set Password to MD5
            if (!StringUtils.hasValue(actionForm.getSecureCode())){
                actionForm.setSecureCodeFromPassword();
                actionForm.setPassword(null);
                actionForm.setConfirmPassword(null);
            }
            
            //logger.debug("RegisterAction doOne email=" + actionForm.getEmailAddress());
            
            //logger.debug("Debug \n" + actionForm.printValues());
            //logger.debug("Id=" + actionForm.getUserName());
            //logger.debug("Email=" + actionForm.getEmailAddress());
            //logger.debug("Secure code=" + actionForm.getSecureCode());
            //logger.debug("Password=" + actionForm.getPassword());
            //logger.debug("Confirm Password=" + actionForm.getConfirmPassword());
             
            return SECOND_PAGE;
        } catch (Exception e) {
            return rethrow(e, FIRST_PAGE);
        }
    }
    
    
    public String doTwo(RegisterForm actionForm)
    throws ApplicationException {
        try {
            // token verification to avoid multiple submission 
            String applicationId = (String)((AppContext)actionForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            
            // generate a new token and persist it for multiple submission checking
            generateAndPersistToken(actionForm);
            
            
            //logger.debug("RegisterAction doTwo email=" + actionForm.getEmailAddress());
            //logger.debug("Name=" + actionForm.getName());
            //logger.debug("Gender=" + actionForm.getGender());
            //logger.debug("Birthday=" + actionForm.getBirthDate());
            //logger.debug("Birthday Str=" + actionForm.getBirthdayString());
            //logger.debug("City" + actionForm.getCity());
            //logger.debug("State" + actionForm.getState());
            //logger.debug("Country" + actionForm.getNationality());
            
            logger.debug("Debug \n" + actionForm.printValues());
            
            return THIRD_PAGE;
        } catch (Exception e) {
            return rethrow(e, SECOND_PAGE);
        }
    }
    
    
    public String doThree(RegisterForm actionForm)
    throws ApplicationException {
        try {
            // token verification to avoid multiple submission
            
           
            String applicationId = (String)((AppContext)actionForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            
            // generate a new token and persist it for multiple submission checking
            generateAndPersistToken(actionForm);
             
            // Photo is validated, upload and save photo to server, link id to the user   
            if (PropertyConstants.BOOLEAN_YES != actionForm.getSkip3()) {
        
                try{
                    PhotoFile photoFile = PhotoUploadHelper.
                            uploadMosaicPhoto(actionForm.getPhotoFile(), actionForm.getUserName());

                    actionForm.setPhotoPath(photoFile.getFilePath());
                    actionForm.setPhotoLargePath(photoFile.getFileName());
                    actionForm.setPhotoSmallPath(photoFile.getThumbnailFileName());

                }catch (Exception e) {
                    // Add error message
                }
            
            }else
            { 
                // Sets default photo values 
                actionForm.setPhotoLargePath(PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_LARGE));
                actionForm.setPhotoSmallPath(PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL));
            }
            //logger.debug("Description=" + actionForm.getPhotoDescription());
            
            
            // Do persistence
            //logger.debug("If reach here then proceed to persist into DB");
            User user = actionForm.toUserObject();
            String userName = user.getUserName();
            UserCountry userCountry = actionForm.toUserCountryObject();
            RegisterBO registerBO = new RegisterBO();
            Long userId = registerBO.register(user, userCountry);
            //logger.debug(" ### User id to set into session=" + userId);
            registerBO = null;
            
            logger.debug("Debug \n" + actionForm.printValues());
            
            // Update the referral, set invite been accepted and add referrer counter 
            //System.out.println("Going to update Counters for invitation referral");
            //System.out.println("Registered Referral Id=" + actionForm.getReferralId());
            //System.out.println("Registered Invited By =" + actionForm.getInvitedBy());
            //System.out.println("Registered Invite Code=" + actionForm.getInviteCode());
            boolean validated = InviteBO.updateAcceptedInvite(actionForm.getReferralId(),actionForm.getInviteCode(),actionForm.getEmailAddress());
            if (validated)
            {
                UserBO.setEmailValidated(userId);
                actionForm.setEmailValidated(PropertyConstants.BOOLEAN_YES);
            }
            RankingBO.increaseInvitesAccepted(actionForm.getReferralId());
            // Auto increase vote
            RankingBO.increaseVotes(userId.toString());
            
            // show registered info and request update referral (if not input)
            
            
            // Clear session form after done
            actionForm.clear();
            
            // Keep the username to proceed to profile page
            actionForm.setUserName(userName);
            
            // Set User Id to session
            actionForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_USER_ID, userId.toString()); 
            actionForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_USER_NAME, userName);
            return FOURTH_PAGE;
        } catch (Exception e) {
            return rethrow(e, THIRD_PAGE);
        }
    }
    
    
    public String doFourForm(RegisterForm actionForm)
    throws ApplicationException {
        try {
            
            // token verification to avoid multiple submission
            
            
            String applicationId = (String)((AppContext)actionForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            
            // generate a new token and persist it for multiple submission checking
            generateAndPersistToken(actionForm);
            
            //logger.debug("Debug \n" + actionForm.printValues());
            //logger.debug("Email 1 =" + actionForm.getEmailFwd1());
            //logger.debug("Email 2 =" + actionForm.getEmailFwd2());
            //logger.debug("Email 3 =" + actionForm.getEmailFwd3());
            
            return FOURTH_PAGE;
        } catch (Exception e) {
            return rethrow(e, FOURTH_PAGE);
        }
    }
    
    public String doFour(RegisterForm actionForm)
    throws ApplicationException {
        try {
             
            
            // token verification to avoid multiple submission
          
            
            String applicationId = (String)((AppContext)actionForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            
            // generate a new token and persist it for multiple submission checking
            generateAndPersistToken(actionForm);
            
            //logger.debug("Debug \n" + actionForm.printValues());
            //logger.debug("Email 1 =" + actionForm.getEmailFwd1());
            //logger.debug("Email 2 =" + actionForm.getEmailFwd2());
            //logger.debug("Email 3 =" + actionForm.getEmailFwd3());
            
            
            // Check email invites and send
            logger.debug("If reach here then proceed to invites");
            
            
            return MAIN_PAGE;
        } catch (Exception e) {
            return rethrow(e, FOURTH_PAGE);
        }
    }
    */
    
    
    protected boolean isValidationRequired(String event) {
        //boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        /*if ("one".equals(event)||
            "two".equals(event)||
            "three".equals(event)||
            "four".equals(event))
            return true;
         */
        
        return true;
    }
    
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        RegisterForm registerForm = (RegisterForm)actionForm;
        
        logger.debug("ValidateParameter=" + action);
        
        if ("invited".equals(action)) {
            errors = RegisterValidator.validateInvitation(errors, registerForm);
        } else if ("new".equals(action)) {
            errors = RegisterValidator.validateInvitation(errors, registerForm);
        } else if ("submit".equals(action)) {
            errors = RegisterValidator.validateInvitation(errors, registerForm);
            errors = RegisterValidator.validateNewRegistration(errors, registerForm);
        /*
        } else if ("one".equals(action)) {
            errors = RegisterValidator.validateInvitation(errors, registerForm);
            errors = RegisterValidator.validatePageOne(errors, registerForm);
        } else if ("two".equals(action)) {
            errors = RegisterValidator.validatePageTwo(errors, registerForm);
        } else if ("three".equals(action)) {
            errors = RegisterValidator.validatePageThree(errors, registerForm);
        } else if ("four".equals(action)) {
            errors = RegisterValidator.validatePageFour(errors, registerForm);*/
        } else if ("validated".equals(action)) {
            errors = RegisterValidator.validateEmailLink(errors, registerForm);
        } else {
            logger.debug("Temporary skip");
        } 
        
        registerForm.getActionContext().setActionErrors(errors);
        return registerForm;
    }
    
    protected String getDefaultEvent(String action) {
        String previousAction = action;

        
        if ("submit".equals(action))
            action = "new";
        /*else if ("one".equals(action))
            action = "invited";
        else if ("two".equals(action))
            action = "one";
        else if ("three".equals(action))
            action = "two";
        else if ("four".equals(action))
            action = "fourForm";*/
        else if ("validated".equals(action))
            action = "validated";
        else {
            logger.debug("Temporary skip");
        }
        logger.debug("Going back to previous action from [" + previousAction + "] to [" + action + "] ");
        return action;
    }
    
    protected String getDefaultActionName() {
                  
        return REGISTER_FORM_PAGE;
    }
}
