 

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.solution.web.struts.controller.BaseActionForm;
 

public class AbstractApplicationActionForm
    extends BaseActionForm
{
    private boolean holderFlag;
    private String applicationStatusCode;
    private String applicationStatusDescription;
    
    //For validation
    private String viewerUserEmail; 

    public boolean isHolderFlag()
    {
        return holderFlag;
    }

    public void setHolderFlag(boolean holderFlag)
    {
        this.holderFlag = holderFlag;
    }

    public String getApplicationStatusCode()
    {
        return applicationStatusCode;
    }

    public void setApplicationStatusCode(String applicationStatusCode)
    {
        this.applicationStatusCode = applicationStatusCode;
    }

    public String getApplicationStatusDescription()
    {
        return applicationStatusDescription;
    }

    public void setApplicationStatusDescription(String applicationStatusDescription)
    {
        this.applicationStatusDescription = applicationStatusDescription;
    }

    public String getViewerUserEmail() {
        return viewerUserEmail;
    }

    public void setViewerUserEmail(String viewerUserEmail) {
        this.viewerUserEmail = viewerUserEmail;
    }
 
}

// end of AbstractApplicationActionForm.java