/*
 * TourAction.java
 *
 * Created on January 5, 2008, 7:15 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.solution.base.helper.ApplicationException;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class TourAction extends AbstractApplicationAction {
      
    private static String WHAT = "what";
    private static String WHY = "why";
    private static String HOW = "how";    
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(TourAction.class);
    
    
    
    protected boolean isValidationRequired(String event) { 
        
        return false;
    } 
    
     
    
    
    protected String getDefaultActionName() {
                  
        return WHAT;
    }
     
    public String doWhat(TourForm actionForm){ 
        return WHAT;  
    } 
    public String doWhy(TourForm actionForm) {
        return WHY; 
    } 
    public String doHow(TourForm actionForm) {
        return HOW; 
    } 
    
    /** Creates a new instance of TourAction */
    public TourAction() {
    }
    
}
