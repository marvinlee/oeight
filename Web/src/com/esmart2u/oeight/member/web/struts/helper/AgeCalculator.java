/*
 * AgeCalculator.java
 *
 * Created on November 12, 2007, 1:18 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

/**
 *
 * @author meauchyuan.lee
 */
public class AgeCalculator {
    
    /** Creates a new instance of AgeCalculator */
    public AgeCalculator() {
    }
     
    public static String getYear(int yr)throws Exception {
        int outYear=0;
        int year;
        year = yr;
        String usrYear = new String();
        String output = new String();
        usrYear = getString();
        
        try {
            outYear = Integer.parseInt(usrYear);
            if(outYear < 1800 || outYear > year)
                throw(new NumberFormatException());
        }
        
        catch(NumberFormatException error)
        
        
        {
            outYear = check(("Input must be between 1800-" + year + ": "),1);
        }
        
        output = Integer.toString(outYear);
        return output;
    }
    
    public static String getMonth()throws Exception {
        int outMonth=0;
        String usrMonth = new String();
        String output = new String();
        usrMonth = getString();
        try {
            outMonth = Integer.parseInt(usrMonth);
            if(outMonth < 1 || outMonth > 12)
                throw(new NumberFormatException());
        }
        
        catch(NumberFormatException error)
        
        
        {
            outMonth = check(("Input must be between 1-12: "),2);
        }
        output = Integer.toString(outMonth);
        return output;
    }
    
    public static String getDay(int mth)throws Exception {
        int outDay=0;
        int [] dayInMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
        int usrMth, dayInMth;
        usrMth=mth;
        dayInMth = dayInMonth[usrMth - 1];
        String usrDay = new String();
        String output = new String();
        usrDay = getString();
        try {
            outDay = Integer.parseInt(usrDay);
            if(outDay < 1 || outDay > dayInMth)
                throw(new NumberFormatException());
        }
        
        catch(NumberFormatException error)
        
        
        {
            outDay = check(("Input must be between 1-" + dayInMth + ": "),3);
        }
        output = Integer.toString(outDay);
        return output;
    }
    
    public static void displayAge(int yr, int tdYr, int mth, int tdMth, int usday, int tdDay)
    
    
    {
        int usrYear, year, usrMonth, month, usrDay, day, outDay=0, outMonth, outYear, tempMth;
        String pyr = new String();
        String pmth = new String();
        String pdy = new String();
        int [] dayInMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
        int tempDyMth;
        pyr = "year";
        pmth = "month";
        pdy = "day";
        usrYear = yr;
        year = tdYr;
        usrMonth = mth;
        month = tdMth;
        usrDay = usday;
        day = tdDay;
        outDay = day - usrDay;
        tempMth = usrMonth;
        outMonth = (month+1) - usrMonth;
        outYear = year - usrYear;
        
        if (usrMonth-1<month&&usrDay>day)
            
            
        {
            outMonth = outMonth - 1;
            tempDyMth = dayInMonth[usrMonth-1];
            outDay = tempDyMth+outDay;
            
        }
        if ( outDay<0 )
            
            
        {
            outDay = getDaysInMonth(tempMth,usrDay,day);
            outMonth = Math.abs(outMonth);
            outMonth=(12-outMonth)-1;
            outYear=outYear-1;
        }
        
        if (outMonth<0) {
            outMonth=Math.abs(outMonth);
            outMonth=12-outMonth;
            outYear=outYear-1;
        }
        if( outYear > 1 )
            pyr = "years";
        if( outMonth > 1 )
            pmth = "months";
        if( outDay > 1 )
            pdy = "days";
        
        System.out.println("As of today (" + (month+1) +"/"+day+"/"+year+") you are "
                + outYear + " " + pyr + " "
                + outMonth + " " + pmth + " "
                + outDay + " " + pdy + " old.");
        
    }
    
    public static int getDaysInMonth(int usrMth, int usrDy, int tdDay)
    
    
    {
        int usrMonth, usrDay, daysInMth,day, outDay;
        int [] dayInMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
        usrMonth = usrMth-1;
        usrDay = usrDy;
        day = tdDay;
        
        daysInMth = dayInMonth[usrMonth];
        outDay = daysInMth - usrDay;
        outDay = outDay + day;
        
        
        return outDay;
    }
    
    
    public static String getString() throws Exception {
        char newchar;
        
        String newstring = new String();
        newchar = (char)System.in.read();
        while (newchar != '\r')
            
            
        {
            newstring = newstring + newchar;
            newchar = (char)System.in.read();
        }
        System.in.read();
        return newstring;
    }
    
    public static int check(String err,int n)throws Exception
            
            
    {
        boolean test = false;
        String testStr = new String();
        int output = 0;
        while (test == false)
            
            
        {
            System.out.print(err);
            testStr = getString();
            test = testIn(testStr,n);
        }
        output = Integer.parseInt(testStr);
        return output;
    }
    public static boolean testIn(String str,int x)
    
    
    {
        int input = 0;
        boolean test = true;
        try
                
                
        {
            input = Integer.parseInt(str);
        } catch (NumberFormatException one)
        
        
        {
            test = false;
        }
        switch (x)
        
        
        {
            case 1:
                if (test == true && input > 2002)
                    test = false;
                if (test == true && input < 1800)
                    test = false;
                break;
            case 2:
                if (test == true && input < 1)
                    test = false;
                if (test == true && input > 12)
                    test= false;
                break;
            case 3:
                if (test == true && input < 1)
                    test = false;
                if (test == true && input >= 31)
                    test = false;
        }
        return test;
    }
}
