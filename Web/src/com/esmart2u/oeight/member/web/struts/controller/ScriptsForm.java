/*
 * ScriptsForm.java
 *
 * Created on January 14, 2008, 1:48 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

/**
 *
 * @author meauchyuan.lee
 */
public class ScriptsForm  extends AbstractApplicationActionForm {
    
    
    private String id;  
    private String userName; 
    private String widgetMainHTML;
    private String type;
    
    private String widgetBlogHTML;
    
    private boolean showMyself = true;
    
    private String sessionValue;
    
    /** Creates a new instance of ScriptsForm */
    public ScriptsForm() {
    }
     
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
        this.userName = id;
    }
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWidgetMainHTML() {
        return widgetMainHTML;
    }

    public void setWidgetMainHTML(String widgetMainHTML) {
        this.widgetMainHTML = widgetMainHTML;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWidgetBlogHTML() {
        return widgetBlogHTML;
    }

    public void setWidgetBlogHTML(String widgetBlogHTML) {
        this.widgetBlogHTML = widgetBlogHTML;
    }

    public boolean isShowMyself() {
        return showMyself;
    }

    public void setShowMyself(boolean showMyself) {
        this.showMyself = showMyself;
    }

    public String getSessionValue() {
        return sessionValue;
    }

    public void setSessionValue(String sessionValue) {
        this.sessionValue = sessionValue;
    }
    
    
}
