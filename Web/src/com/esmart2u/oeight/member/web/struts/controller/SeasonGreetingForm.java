/*
 * SeasonGreetingForm.java
 *
 * Created on November 14, 2007, 12:00 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

/**
 *
 * @author meauchyuan.lee
 */
public class SeasonGreetingForm  extends AbstractApplicationActionForm {
    
    private long greetingId;
    private String senderEmail;
    private String senderName;
    private String receiverEmail;
    private String receiverName;
    private String message;
    private boolean subscribe;
    private String greetingCode;
    private int t; // card type
    
    /** Creates a new instance of SeasonGreetingForm */
    public SeasonGreetingForm() {
    }
    
    public void clear()
    {
        this.greetingId = 0;
        this.senderEmail = null;
        this.senderName = null;
        this.receiverEmail = null;
        this.receiverName = null;
        this.message = null;
        this.subscribe = false; 
        this.greetingCode = null;
        this.t =0;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSubscribe() {
        return subscribe;
    }
    
    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }

    public String getGreetingCode() {
        return greetingCode;
    }

    public void setGreetingCode(String greetingCode) {
        this.greetingCode = greetingCode;
    }

    public long getGreetingId() {
        return greetingId;
    }

    public void setGreetingId(long greetingId) {
        this.greetingId = greetingId;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }
    
}
