/*
 * WeddingForm.java
 *
 * Created on June 25, 2008, 10:02 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.OEightWedding;
import com.esmart2u.oeight.member.web.struts.helper.Page;
import com.esmart2u.solution.base.helper.BeanUtils;
import java.util.List;

import org.apache.struts.upload.FormFile;
/**
 *
 * @author meauchyuan.lee
 */
public class WeddingForm   extends AbstractApplicationActionForm {
    
    /** Creates a new instance of WeddingForm */
    public WeddingForm() {
    }
    
    private long weddingId;
    private long userId;
    private String userName; // oeightId
    private String coupleName;
    private String weddingDescription;
    private String photoSmallPath;
    private String photoLargePath;
    private String video1;
    private String video2;
    private String video3;
    private String video4;
    private String video5;
    private String video6;
    private String video7;
    private String video8; 
    private char status; 
    private char allowComments; // Allow, Moderated, No
    private String weddingUrl; 
    
    private FormFile photoFile;
    private String photoPath;
    
    // Listing
    private int currentPage;
    private int totalPage;
    private List resultList;
     
    private Page weddingListPage;

    private boolean pageSavedSuccess;
    
    // for viewing, used to store the oeight id
    private String wedding;
    private String[] wedStrId;
    private String[] wedStatus;
    private List adminWeddingList;
    
    public long getWeddingId() {
        return weddingId;
    }

    public void setWeddingId(long weddingId) {
        this.weddingId = weddingId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCoupleName() {
        return coupleName;
    }

    public void setCoupleName(String coupleName) {
        this.coupleName = coupleName;
    }

    public String getWeddingDescription() {
        return weddingDescription;
    }

    public void setWeddingDescription(String weddingDescription) {
        this.weddingDescription = weddingDescription;
    }

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }

    public String getPhotoLargePath() {
        return photoLargePath;
    }

    public void setPhotoLargePath(String photoLargePath) {
        this.photoLargePath = photoLargePath;
    }

    public String getVideo1() {
        return video1;
    }

    public void setVideo1(String video1) {
        this.video1 = video1;
    }

    public String getVideo2() {
        return video2;
    }

    public void setVideo2(String video2) {
        this.video2 = video2;
    }

    public String getVideo3() {
        return video3;
    }

    public void setVideo3(String video3) {
        this.video3 = video3;
    }

    public String getVideo4() {
        return video4;
    }

    public void setVideo4(String video4) {
        this.video4 = video4;
    }

    public String getVideo5() {
        return video5;
    }

    public void setVideo5(String video5) {
        this.video5 = video5;
    }

    public String getVideo6() {
        return video6;
    }

    public void setVideo6(String video6) {
        this.video6 = video6;
    }

    public String getVideo7() {
        return video7;
    }

    public void setVideo7(String video7) {
        this.video7 = video7;
    }

    public String getVideo8() {
        return video8;
    }

    public void setVideo8(String video8) {
        this.video8 = video8;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public char getAllowComments() {
        return allowComments;
    }

    public void setAllowComments(char allowComments) {
        this.allowComments = allowComments;
    }

    public String getWeddingUrl() {
        return weddingUrl;
    }

    public void setWeddingUrl(String weddingUrl) {
        this.weddingUrl = weddingUrl;
    }

    public FormFile getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(FormFile photoFile) {
        this.photoFile = photoFile;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List getResultList() {
        return resultList;
    }

    public void setResultList(List resultList) {
        this.resultList = resultList;
    }

    public Page getWeddingListPage() {
        return weddingListPage;
    }

    public void setWeddingListPage(Page weddingListPage) {
        this.weddingListPage = weddingListPage;
    }
       
    public OEightWedding toOEightWeddingObject() throws Exception {
        OEightWedding eightWedding = new OEightWedding();
        BeanUtils.copyBean(this, eightWedding);
        return eightWedding;
    }

    public boolean isPageSavedSuccess() {
        return pageSavedSuccess;
    }
    
    public boolean getPageSavedSuccess() {
        return isPageSavedSuccess();
    }

    public void setPageSavedSuccess(boolean pageSavedSuccess) {
        this.pageSavedSuccess = pageSavedSuccess;
    }

    public String getWedding() {
        return wedding;
    }

    public void setWedding(String wedding) {
        this.wedding = wedding;
    }

    public String[] getWedStrId() {
        return wedStrId;
    }

    public void setWedStrId(String[] wedStrId) {
        this.wedStrId = wedStrId;
    }

    public String[] getWedStatus() {
        return wedStatus;
    }

    public void setWedStatus(String[] wedStatus) {
        this.wedStatus = wedStatus;
    }

    public List getAdminWeddingList() {
        return adminWeddingList;
    }

    public void setAdminWeddingList(List adminWeddingList) {
        this.adminWeddingList = adminWeddingList;
    }
    
}
