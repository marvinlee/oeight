/*
 * MessageAction.java
 *
 * Created on March 18, 2008, 4:43 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.RegisterBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserMessageBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.MessageVO;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.logging.Logger;
import java.util.Date;
import java.util.List;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class MessageAction extends AbstractApplicationAction {
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(MessageAction.class);
    
    private static String LIST = "list";
    private static String SAVED = "saved";
    private static String DELETE = "delete";
    private static String DELETE_SAVED = "deleteSaved";
    private static String VIEW = "view";
    private static String VIEW_SAVED = "viewSaved";
    private static String NEW = "new";
    private static String NEW_SUBMITTED = "newSubmitted"; 
    private static String REPLY = "reply";
    private static String REPLY_SUBMITTED = "replySubmitted"; 
    private static String RELOGIN = "relogin";
    private static String NOT_VALIDATED = "notValidated";
    
    /** Creates a new instance of MessageAction */
    public MessageAction() {
    }
      
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (NEW_SUBMITTED.equals(event) || REPLY_SUBMITTED.equals(event))
            return true;
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        MessageForm messageForm = (MessageForm)actionForm; 
        if (NEW_SUBMITTED.equals(action)) {
            errors = MessageValidator.validateNewMessage(errors, messageForm); 
        } else if (REPLY_SUBMITTED.equals(action)) {
            errors = MessageValidator.validateReplyMessage(errors, messageForm); 
        } else {
            logger.debug("Temporary skip");
        }
        
        messageForm.getActionContext().setActionErrors(errors);
        return messageForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (NEW_SUBMITTED.equals(action)) {
            action = NEW; 
        } else if (REPLY_SUBMITTED.equals(action)) {
            action = REPLY; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doList(MessageForm actionForm)
    throws ApplicationException {
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
            
            UserBO userBO = new UserBO();
            User user = userBO.getUserById(userId); 
           
            boolean hasNew = user.getUserCountry().getHasMessage();
            
            // Get user messages
            UserMessageBO userMessageBO = new UserMessageBO();
            List messageList = userMessageBO.getUserMessageList(userId, hasNew);
            //actionForm.setUserName(user.getUserName());
            actionForm.setMessageList(messageList);
            
            userMessageBO = null; 
            //userBO = null;
            
            return LIST;
        } catch (Exception e) { 
            return RELOGIN;
        } 
    }
    
    public String doDelete(MessageForm actionForm)
    throws ApplicationException {
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
            
            UserBO userBO = new UserBO();
            User user = userBO.getUserById(userId); 
           
            // Delete messages 
            String[] messageIds = actionForm.getMessageIdString(); 
            
            // Get user messages
            UserMessageBO userMessageBO = new UserMessageBO();
            List messageList = userMessageBO.getUserMessageDelete(userId,messageIds);
            actionForm.setUserName(user.getUserName());
            actionForm.setMessageList(messageList);
            
            userMessageBO = null; 
            userBO = null;
            
            return LIST;
        } catch (Exception e) { 
            return RELOGIN;
        } 
    }
    
    public String doSaved(MessageForm actionForm)
    throws ApplicationException {
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
            
            UserBO userBO = new UserBO();
            User user = userBO.getUserById(userId);  
            
            // Get user messages
            UserMessageBO userMessageBO = new UserMessageBO();
            List messageList = userMessageBO.getUserSavedMessageList(userId);
            //actionForm.setUserName(user.getUserName());
            actionForm.setMessageList(messageList);
            
            userMessageBO = null; 
            //userBO = null;
            
            return SAVED;
        } catch (Exception e) { 
            return RELOGIN;
        } 
    }
    
    
    public String doDeleteSaved(MessageForm actionForm)
    throws ApplicationException {
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
            
            UserBO userBO = new UserBO();
            User user = userBO.getUserById(userId); 
           
            // Delete messages 
            String[] messageIds = actionForm.getMessageIdString(); 
            
            // Get user messages
            UserMessageBO userMessageBO = new UserMessageBO();
            List messageList = userMessageBO.getUserSavedMessageDelete(userId,messageIds);
            actionForm.setUserName(user.getUserName());
            actionForm.setMessageList(messageList);
            
            userMessageBO = null; 
            userBO = null;
            
            return SAVED;
        } catch (Exception e) { 
            return RELOGIN;
        } 
    }
       
    public String doViewSaved(MessageForm actionForm)
    throws ApplicationException {
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
               
            // Get user messages
            UserMessageBO userMessageBO = new UserMessageBO(); 
            MessageVO messageVO = userMessageBO.getUserSavedMessage(actionForm.getMsgId(), userId);
            if (messageVO != null)
            { 
                //actionForm.setUserName(messageVO.getRecipient().getUserName());
                actionForm.setMessageVO(messageVO);
            }
            else
            {
                System.out.println("MessageVO is null");
            }
            
            userMessageBO = null;  
            
            return VIEW_SAVED;
        } catch (Exception e) { 
            return RELOGIN;
        } 
    }
    
    public String doView(MessageForm actionForm)
    throws ApplicationException {
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
               
            // Get user messages
            UserMessageBO userMessageBO = new UserMessageBO(); 
            MessageVO messageVO = userMessageBO.getUserMessage(actionForm.getMsgId(), userId, true);
            if (messageVO != null)
            { 
                //actionForm.setUserName(messageVO.getRecipient().getUserName());
                actionForm.setMessageVO(messageVO);
            }
            else
            {
                System.out.println("MessageVO is null");
            }
            
            userMessageBO = null;  
            
            return VIEW;
        } catch (Exception e) { 
            return RELOGIN;
        } 
    }
    
    public String doNew(MessageForm actionForm)
    throws ApplicationException {
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
             
            UserBO userBO = new UserBO();
            User user = userBO.getUserByUserName(actionForm.getTo());  
               

            // Sending wish/message needs validation
            User sender = userBO.getUserById(userId);
            if (PropertyConstants.BOOLEAN_YES != sender.getUserCountry().getValidated())
            {
                actionForm.setViewerUserEmail(sender.getEmailAddress()); 
                actionForm.getActionContext().getRequest().setAttribute("viewerUserEmail", sender.getEmailAddress());
                return NOT_VALIDATED;
            } 
            
            
            userBO = null;
            
            if (user != null){
                actionForm.setRecipientUserName(user.getUserName());
                actionForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            }
            
            RegisterBO registerBO = new RegisterBO();
            String nonceToken = registerBO.getSecureCode(actionForm.getTo(),""+System.currentTimeMillis());
            registerBO = null;
            
            actionForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_NONCE_TOKEN,nonceToken);
            actionForm.setNonceToken(nonceToken);
            
            return NEW;
        } catch (Exception e) { 
            return RELOGIN;
        } 
    }
    
    
    public String doNewSubmitted(MessageForm actionForm)
    throws ApplicationException {
        try { 
                
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
                
            // Save into DB 
            MessageVO messageVO = new MessageVO();
            messageVO.setSenderUserId(Long.parseLong(userId));
            messageVO.setRecipientUserName(actionForm.getTo());
            messageVO.setSubject(actionForm.getSubject());
            messageVO.setMessage(actionForm.getMessage()); 
            messageVO.setIsSaved(actionForm.getIsSaved());
            messageVO.setDateSent(new Date()); 
    
            UserMessageBO userMessageBO = new UserMessageBO();
            userMessageBO.saveMessage(messageVO);
            userMessageBO = null;  
          
            actionForm.setMessageVO(messageVO);
            
            return NEW_SUBMITTED;
        } catch (Exception e) { 
            RegisterBO registerBO = new RegisterBO();
            String nonceToken = "";
            try {
                nonceToken = registerBO.getSecureCode(actionForm.getTo(), "" + System.currentTimeMillis());
            } catch (Exception ex) {
                logger.error("Error in generating nonce");
            }
            registerBO = null;
            
            actionForm.getActionContext().getRequest().getSession().setAttribute("nonceToken",nonceToken);
            actionForm.setNonceToken(nonceToken);
            return NEW;
        }
    }
    
    public String doReply(MessageForm actionForm)
    throws ApplicationException {
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
             
            UserMessageBO userMessageBO = new UserMessageBO();
            MessageVO messageVO = userMessageBO.getUserMessage(actionForm.getMsgId(), userId);
            actionForm.setMessageVO(messageVO);
            
            if (messageVO != null){
                actionForm.setTo(messageVO.getSenderUserName());
                actionForm.setRecipientUserName(messageVO.getSenderUserName());
                actionForm.setPhotoSmallPath(messageVO.getPhotoSmallPath());
                actionForm.setSubject("Re: " + messageVO.getSubject());
            }
            
            RegisterBO registerBO = new RegisterBO();
            String nonceToken = registerBO.getSecureCode(actionForm.getTo(),""+System.currentTimeMillis());
            registerBO = null;
            
            actionForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_NONCE_TOKEN,nonceToken);
            actionForm.setNonceToken(nonceToken);
            
            return REPLY;
        } catch (Exception e) { 
            return RELOGIN;
        } 
    }
    
    
    public String doReplySubmitted(MessageForm actionForm)
    throws ApplicationException {
        try { 
                
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
                
            // Save into DB 
            MessageVO messageVO = new MessageVO();
            messageVO.setSenderUserId(Long.parseLong(userId));
            messageVO.setRecipientUserName(actionForm.getTo());
            messageVO.setSubject(actionForm.getSubject());
            messageVO.setMessage(actionForm.getMessage()); 
            messageVO.setIsSaved(actionForm.getIsSaved());
            messageVO.setDateSent(new Date()); 
            messageVO.setReferredSourceMessageId(Long.parseLong(actionForm.getMsgId()));
    
            UserMessageBO userMessageBO = new UserMessageBO();
            System.out.println("referred msgId="+actionForm.getMsgId());
            userMessageBO.saveMessage(messageVO,Long.parseLong(actionForm.getMsgId()));
            userMessageBO = null;  
          
            actionForm.setMessageVO(messageVO);
            
            return REPLY_SUBMITTED;
        } catch (Exception e) { 
            
            return doReply(actionForm);
        }
    }
    protected String getDefaultActionName() {
                  
        return LIST;
    }
}
