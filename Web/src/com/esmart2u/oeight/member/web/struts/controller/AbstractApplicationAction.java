

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.solution.web.struts.controller.BaseAction;
import java.util.List;
import java.util.HashMap;
import java.lang.reflect.Method;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.*;

import com.esmart2u.solution.base.helper.*;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.controller.BaseActionException;
import com.esmart2u.solution.web.struts.controller.BaseActionContext;
import com.esmart2u.solution.web.struts.controller.BaseActionForm;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.ActionConstants;


public abstract class AbstractApplicationAction
        extends BaseAction implements ActionConstants{
    private static Logger logger = Logger.getLogger(AbstractApplicationAction.class); 
    /**
     * Process the action request.
     *
     * @param actionMapping  Action mapping.
     * @param baseActionForm Base action form.
     * @param request        HTTP Servlet request.
     * @param response       HTTP Servlet response.
     * @return Action forward.
     * @throws Exception If exception is caught.
     */
    protected ActionForward processRequest(ActionMapping actionMapping, BaseActionForm baseActionForm,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        //logger.debug("Running in AbstractApplicationAction");
        //System.out.println("Running in AbstractApplicationAction");
        String doAction = parseActionName(request.getParameter("act"));
        HttpSession session = request.getSession();
        if (StringUtils.hasValue(doAction)) { 
            
            //logger.info("Processing action [" + doAction + "]...");
            //logger.debug("Processing action [" + doAction + "]...");
            
            // check action form type
            if (!AbstractApplicationActionForm.class.isInstance(baseActionForm)) {
                throw new ClassCastException("ActionForm is not derived from AbstractApplicationActionForm!");
            }
            
            // prepare application action form
            AbstractApplicationActionForm abstractApplicationActionForm = (AbstractApplicationActionForm)baseActionForm;
            prepareActionForm(abstractApplicationActionForm);
            
            BaseActionContext baseActionContext = baseActionForm.getActionContext(); 
            
            //logger.debug("Act Context " + baseActionContext);
            //logger.debug("App Context " + baseActionContext.getAppContext());
            
            String sessionId = (String)baseActionContext.getAppContext().get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            String userId = (String)baseActionContext.getAppContext().get(AppConstants.USER_ID);
            //logger.debug("Session id = " + sessionId);
            //logger.debug("Session app context User id = " + userId);
            
            Object returnMapping = null;
            //if (StringUtils.hasValue(sessionId)) {
            // Generate new session id if new session
            if (sessionId == null){
                sessionId = request.getSession(true).getId();
                //logger.debug("AbstractApplicationAction Session Id" + sessionId);
                baseActionContext.getAppContext().put(OEightConstants.SESSION_APPLICATION_ID_KEY, sessionId.toUpperCase());
                
                        // set appContext into session for login
                        //AppContext appContext = new AppContext();
                        //appContext.put(AppConstants.USER_ID, userId);
                        //appContext.put(AppConstants.USER_NAME, StringUtils.convertNullString(loginBean.getUserName()));
                        //appContext.put(AppConstants.APP_CODE, applicationCode);
                        //appContext.put(AppConstants.WORKSTATION_IP, workStationIp);
                        //appContext.put(AppConstants.SESSION_ID, loginBean.getSessionId());
                        //appContext.put(AppConstants.BRANCH_CODE, StringUtils.convertNullString(loginBean.getBranchCode()));
                        //appContext.put(AppConstants.BRANCH_NAME, StringUtils.convertNullString(loginBean.getBranchName()));
                        //appContext.put(AppConstants.APPLICATION_ROLE_LIST, loginBean.getApplicationRoleList());
                        session.setAttribute(AppConstants.APP_CONTEXT, baseActionContext.getAppContext());
                        baseActionContext.setCacheObject(AppConstants.APP_CONTEXT, baseActionContext.getAppContext());
                        baseActionForm.setActionContext(baseActionContext);
            }
            
                try {
                    
                    long timeStart = System.currentTimeMillis();
                    //logger.info(" Time start = " + timeStart);
                    
                    // Validate first
                    
                    
                    // get the 'do' method
                    Method doMethod = null;
                    doMethod = getClass().getMethod(doAction, new Class[]{abstractApplicationActionForm.getClass()});
                    
                    
                    
                    // calling the 'do' method
                    logger.debug(getClass().getName() + " accessed, calling action [" + doAction + "] now...");
                    returnMapping = doMethod.invoke(this, new Object[]{abstractApplicationActionForm});
                    
                    
                    long timeEnd = System.currentTimeMillis();
                    if (logger.isInfoEnabled()) {
                        logger.info(" Time end  = " + timeEnd);
                        logger.info(" Time used = " + (timeEnd - timeStart) + " ms");
                    }
                } catch (NoSuchMethodException e) {
                    throw new BaseActionException("Unrecognized action [" + doAction + "]");
                } catch (SecurityException e) {
                    throw new BaseActionException("Security error in accessing action [" + doAction + "]");
                //} catch (ApplicationException e) {
                //    abstractApplicationActionForm.setActionList(null);
                //    logger.error(e.getMessage(), e);
                    
                }
            //}
            
            // set system date time
            abstractApplicationActionForm.setSystemDateTime(DateUtils.getCurrentDateTime());
            
            // save messages
            logger.debug("Number of messages after action invocation = " + baseActionContext.getActionMessages().size());
            saveMessages(request, baseActionContext.getActionMessages());
            
            // return to page
            if (returnMapping instanceof String) {
                return (actionMapping.findForward((String)returnMapping));
            } else {
                throw new BaseActionException("Unrecognized return-mapping of [" + returnMapping + "]");
            }
        } else {
            logger.error("Action is not defined");
            throw new BaseActionException("Action not defined");
        }
    }
    
    protected boolean hasErrors(BaseActionForm actionForm)
    {
        return actionForm.getActionContext().getActionErrors().size() > 0;
    }  
    
    protected String checkUserAccess(AbstractApplicationActionForm actionForm) throws Exception {
        if (actionForm.getActionContext().getRequest().getSession() == null)
            throw new ApplicationException(new Exception("Invalid Session or User Not Logged On"));
        
        String userId = (String)actionForm.getActionContext().getRequest().getSession().getAttribute(OEightConstants.SESSION_USER_ID);
        //logger.debug("User Id from ctx  =" + actionForm.getActionContext().getRequest().getSession().getAttribute(OEightConstants.SESSION_USER_ID));
        
        if (!StringUtils.hasValue(userId)) {
            throw new ApplicationException(new Exception("User Not Logged On"));
        }
        return userId;
    }
    
}

// end of AbstractApplicationAction.java