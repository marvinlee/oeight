/*
 * EarthAction.java
 *
 * Created on May 12, 2008, 3:42 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.OEightBlog;
import com.esmart2u.oeight.data.OEightPledge;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.CampaignInviteBO;
import com.esmart2u.oeight.member.bo.EarthBO;
import com.esmart2u.oeight.member.bo.InviteBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.controller.EarthValidator;
import com.esmart2u.oeight.member.web.struts.helper.OEightPledgeHelper;
import com.esmart2u.oeight.member.web.struts.helper.Page;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.contactlist.ContactListHelper;
import com.esmart2u.solution.base.logging.Logger;
import java.util.Date;
import java.util.List;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class EarthAction extends AbstractApplicationAction {
     
    private static String MAIN = "main";
    private static String PLEDGE_ABOUT = "pledgeAbout";
    private static String PLEDGE_MAKE = "pledgeMake";
    private static String PLEDGE_MAKE_SUBMITTED = "pledgeMakeSubmitted";
    private static String PLEDGE_ROOM = "pledgeRoom";
    private static String CONTEST = "contest";
    private static String CONTEST_BLOG = "contestBlog";
    private static String CONTEST_BLOG_FORM = "contestBlogForm";
    private static String CONTEST_BLOG_SUBMIT = "contestBlogSubmit"; 
    private static String CONTEST_EARTH = "contestEarth";
    private static String CONTEST_EARTH_LOGIN = "contestEarthLogin";
    private static String SHARE_PLEDGE = "sharePledge";
    private static String SHARE_SELECT = "shareSelect";
    private static String SHARE_RESULT = "shareResult"; 
    private static String SHARE_MANUAL = "shareManual";
    private static String SHARE_MANUAL_SUBMITTED = "shareManualSubmitted";
    private static String FRIENDSTER = "friendster";
    private static String FACEBOOK = "facebook";
    private static String RESOURCES = "resources";
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(EarthAction.class);
    
    
    /** Creates a new instance of EarthAction */
    public EarthAction() {
    }
       
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (PLEDGE_MAKE_SUBMITTED.equals(event) || CONTEST_BLOG_SUBMIT.equals(event) )
            return true;
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        EarthForm earthForm = (EarthForm)actionForm; 
        if (PLEDGE_MAKE.equals(action)) {
            errors = EarthValidator.validatePledge(errors, earthForm); 
        } else if (PLEDGE_MAKE_SUBMITTED.equals(action)) {
            errors = EarthValidator.validatePledgeSubmitted(errors, earthForm); 
        } else if (CONTEST_BLOG_SUBMIT.equals(action)) {
            errors = EarthValidator.validateBlogSubmitted(errors, earthForm); 
        } else {
            logger.debug("Temporary skip");
        }
        
        earthForm.getActionContext().setActionErrors(errors);
        return earthForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (PLEDGE_MAKE_SUBMITTED.equals(action)) {
            action = PLEDGE_MAKE; 
        } else if (CONTEST_BLOG_SUBMIT.equals(action)) {
            action = CONTEST_BLOG_FORM; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doMain(EarthForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return PLEDGE_ABOUT;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    }
     
    public String doPledgeAbout(EarthForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return PLEDGE_ABOUT;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    }
    
    public String doPledgeMake(EarthForm actionForm)
    throws ApplicationException {
        try { 
            try {
                
                // Get User Id from session and get details
                String userId = checkUserAccess(actionForm);
                actionForm.setUserId(Long.parseLong(userId));
                UserBO userBO = new UserBO();
                User user = userBO.getUserById(userId);
                actionForm.setUserName(user.getUserName());
                actionForm.setPledgerName(user.getUserCountry().getName());
                actionForm.setMemberFlag(PropertyConstants.BOOLEAN_YES); 
                
            } catch (Exception ex) {
                logger.debug("Pledge not from login user");
                actionForm.setUserId(0);
            }
            
               
            // Set session values
            String from = actionForm.getFrom();
            String to = actionForm.getTo();

            if (StringUtils.hasValue(from)){
                String fromKey = OEightConstants.SESSION_CAMPAIGN_INVITE_FROM + OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE;
                actionForm.getActionContext().getRequest().getSession().setAttribute(fromKey, from);
            }
            if (StringUtils.hasValue(to))
            {
                String toKey = OEightConstants.SESSION_CAMPAIGN_INVITE_TO + OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE;
                actionForm.getActionContext().getRequest().getSession().setAttribute(toKey, to);
            }
            
            // Auto populate if already login
            
            // 
            
            //actionForm.clear();  
            return PLEDGE_MAKE;
        } catch (Exception e) {
            return PLEDGE_MAKE;
        }
    }
    
    
    public String doPledgeMakeSubmitted(EarthForm actionForm)
    throws ApplicationException {
        try {
            try {
                
                // Get User Id from session and get details
                String userId = checkUserAccess(actionForm);
                actionForm.setUserId(Long.parseLong(userId));
                UserBO userBO = new UserBO();
                User user = userBO.getUserById(userId);
                actionForm.setUserName(user.getUserName());
                actionForm.setPledgerName(user.getUserCountry().getName());
                actionForm.setMemberFlag(PropertyConstants.BOOLEAN_YES);
                
            } catch (Exception ex) {
                logger.debug("Feedback not from login user");
                actionForm.setUserId(0);
            }
            
            // Save into DB 
            OEightPledge oeightPledge = actionForm.toOEightPledgeObject(); 
            EarthBO earthBO = new EarthBO();
            earthBO.savePledge(oeightPledge);
            actionForm.setPledgeSaved(true);
            
            // Set invite accepted and add referral value  
            String fromKey = OEightConstants.SESSION_CAMPAIGN_INVITE_FROM + OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE;
            String fromValue = (String)actionForm.getActionContext().getRequest().getSession().getAttribute(fromKey); 
             
            String toKey = OEightConstants.SESSION_CAMPAIGN_INVITE_TO + OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE;
            String toValue = (String)actionForm.getActionContext().getRequest().getSession().getAttribute(toKey); 
            
            if (StringUtils.hasValue(fromValue) && StringUtils.hasValue(toValue))
            {
                CampaignInviteBO campaignBO = new CampaignInviteBO();
                campaignBO.updateAccepted(fromValue, toValue, OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE, 0);
                campaignBO = null; 
                
                // Clear session values
                actionForm.getActionContext().getRequest().getSession().removeAttribute(fromKey);
                actionForm.getActionContext().getRequest().getSession().removeAttribute(toKey);
                
                // Reinit top number
                OEightPledgeHelper.reInitMaxPledgeFriends();
            }
            
            
            // Set search first page list for display
            Page page = earthBO.getPledgeList(actionForm.getCurrentPage());
            actionForm.setPledgeListPage(page);
            
            earthBO = null; 
            
            //return PLEDGE_MAKE_SUBMITTED;
            return PLEDGE_ROOM;
        } catch (Exception e) {
            e.printStackTrace();
            return PLEDGE_ABOUT;
        }
    }
     
    public String doPledgeRoom(EarthForm actionForm)
    throws ApplicationException {
        try {  
            EarthBO earthBO = new EarthBO(); 
            Page page = earthBO.getPledgeList(actionForm.getCurrentPage());
            actionForm.setPledgeListPage(page);
            
            earthBO = null; 
            return PLEDGE_ROOM;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    }  
   
    public String doContest(EarthForm actionForm)
    throws ApplicationException {
        try {  
            // Show Number of Friends Who Pledged here (if already login)
            try{
                String userId = checkUserAccess(actionForm);
                if (StringUtils.hasValue(userId))
                { 
                    long accepted = CampaignInviteBO.getTotalAccepted(userId, OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE);
                    actionForm.setTotalAccepted(accepted);
                }
            }catch (Exception ue)
            {
                //doNothing, just do not show
            } 
            
            return CONTEST;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    }
       
    public String doContestBlog(EarthForm actionForm)
    throws ApplicationException {
        try {  
            return CONTEST_BLOG;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    }
       
    public String doContestEarth(EarthForm actionForm)
    throws ApplicationException {
        try {  
            return CONTEST_EARTH;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    }
       
    public String doSharePledge(EarthForm actionForm)
    throws ApplicationException {
        try {  
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            return SHARE_PLEDGE;
        } catch (Exception e) {
            return CONTEST_EARTH_LOGIN;
        }
    }
          
    public String doShareSelect(EarthForm actionForm)
    throws ApplicationException {
        try {  
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            //
            String emailName = actionForm.getEmailUser();
            String provider = actionForm.getProvider();
            String password = actionForm.getPassword();
            
            // Get contacts, need instance as this process requires time
            ContactListHelper contactListHelper = new ContactListHelper();
            // contactList is list of Contact objects
            List contactList = contactListHelper.getContacts(emailName, provider, password);
             
            actionForm.setContactEmailList(contactList); 
            
            return SHARE_SELECT;
        } catch (Exception e) {
            return CONTEST_EARTH_LOGIN;
        }
    }
          
    public String doShareResult(EarthForm actionForm)
    throws ApplicationException {
        try {  
              // !!! IMPORTANT STEP
            // Get User Id from session and get details
            String loginUserId = checkUserAccess(actionForm);
            List addedMembersList = null;
            
            UserBO userBO = new UserBO(); 
            
            // Get non members list and send request as invites
            String[] contactEmails = actionForm.getContactEmailString();
            if (contactEmails != null && contactEmails.length>0)
            {
                String fullLengthEmail = "";
                for(int i=0; i<contactEmails.length;i++)
                {
                    String contactEmail = contactEmails[i]; 
                    if (StringUtils.hasValue(contactEmail)) {
                        fullLengthEmail += contactEmail + ",";
                    }
                }  
                
                // Save addresses
                CampaignInviteBO campaignInviteBO = new CampaignInviteBO(); 
                actionForm.setContactEmailList(campaignInviteBO.saveCampaignInviteAddresses(loginUserId, fullLengthEmail, OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE)); 
                campaignInviteBO = null;
            }
            
            userBO = null;
            
            return SHARE_RESULT;
        } catch (Exception e) {
            return CONTEST_EARTH_LOGIN;
        }
    }       
     
    public String doShareManual(EarthForm actionForm)
    throws ApplicationException {  
        try {  
             
            actionForm.setEmails(null);
            actionForm.setAddressList(null);
            return SHARE_MANUAL;
        } catch (Exception e) { 
            //return rethrow(e, RELOGIN);
            return CONTEST_EARTH_LOGIN;
        }
    }
     
     
    public String doShareManualSubmitted(EarthForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
             
            // Save addresses
            CampaignInviteBO campaignInviteBO = new CampaignInviteBO(); 
            actionForm.setContactEmailList(campaignInviteBO.saveCampaignInviteAddresses(userId, actionForm.getEmails(), OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE)); 
            campaignInviteBO = null; 
            
            return SHARE_MANUAL_SUBMITTED;
        } catch (Exception e) {  
            //return rethrow(e, RELOGIN);
            return CONTEST_EARTH_LOGIN;
        }
    }
    
    public String doFriendster(EarthForm actionForm)
    throws ApplicationException {
        try {  
            return FRIENDSTER;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    }
    
    public String doFacebook(EarthForm actionForm)
    throws ApplicationException {
        try {  
            return FACEBOOK;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    } 
    
    public String doResources(EarthForm actionForm)
    throws ApplicationException {
        try {  
            return RESOURCES;
        } catch (Exception e) {
            return PLEDGE_ABOUT;
        }
    } 
    
    protected String getDefaultActionName() {
                  
        return PLEDGE_ABOUT;
    }
    
    // Blog submission, pingback not successful 
    public String doContestBlogForm(EarthForm actionForm)
    throws ApplicationException {
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            return CONTEST_BLOG_FORM;
        } catch (Exception e) {
            return CONTEST_EARTH_LOGIN;
        }
    }
    
    
    public String doContestBlogSubmit(EarthForm actionForm)
    throws ApplicationException {
        try {
           
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Save into DB 
            OEightBlog oeightBlog = new OEightBlog();
            oeightBlog.setUserId(Long.parseLong(userId));
            oeightBlog.setBlogUrl(actionForm.getBlogUrl());
            oeightBlog.setName(actionForm.getName());
            oeightBlog.setContactNo(actionForm.getContactNo());
            oeightBlog.setEmail(actionForm.getEmail());
            oeightBlog.setStatus(OEightConstants.SYS_STATUS_PENDING_MODERATION);
            oeightBlog.setDateSubmitted(new Date()); 
    
            EarthBO earthBO = new EarthBO();
            earthBO.saveOEightPledge(oeightBlog);
            earthBO = null;
          
            return CONTEST_BLOG_SUBMIT;
        } catch (Exception e) {
            e.printStackTrace();
            return CONTEST_EARTH_LOGIN;
        }
    }
    
    
}
