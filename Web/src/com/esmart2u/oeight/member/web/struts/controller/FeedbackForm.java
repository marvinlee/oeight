/*
 * FeedbackForm.java
 *
 * Created on January 14, 2008, 8:37 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

/**
 *
 * @author meauchyuan.lee
 */
public class FeedbackForm  extends AbstractApplicationActionForm {
    
    private long feedbackId;
    private long userId;
    private String name;
    private String companyName;
    private String contactNumber;
    private String email;
    private String subject; // set as combo box for selection Site Technical Error, Suggestion, Report Abuse, Others
    private String message; 
    
    /** Creates a new instance of FeedbackForm */
    public FeedbackForm() {
    }
    
    public void clear()
    {
        this.feedbackId = 0;
        this.userId = 0;
        this.name = null;
        this.companyName = null;
        this.contactNumber = null;
        this.email = null;
        this.subject = null; 
        this.message = null; 
    }

    public long getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(long feedbackId) {
        this.feedbackId = feedbackId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
