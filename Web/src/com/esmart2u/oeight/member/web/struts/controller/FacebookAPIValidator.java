/*
 * FacebookAPIValidator.java
 *
 * Created on April 2, 2008, 4:16 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserDetails;
import com.esmart2u.oeight.data.UserWidgetFacebook;
import com.esmart2u.oeight.member.bo.LoginBO;
import com.esmart2u.oeight.member.bo.RegisterBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserFacebookBO;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 *
 * @author meauchyuan.lee
 */
public class FacebookAPIValidator {
       
    private static Logger logger = Logger.getLogger(FacebookAPIValidator.class);
    
    /** Creates a new instance of FacebookAPIValidator */
    public FacebookAPIValidator() {
    }   
        
    public static ActionErrors validateCallback1(ActionErrors errors, FacebookForm facebookForm) {
        System.out.println("Validating facebook callback1");
        //if (StringUtils.hasValue(facebookForm.getFb_user()))
        //{
            // If FacebookUserId does not exists in db, is first installation
            //if (UserFacebookBO.getByFacebookId(facebookForm.getUser_id(),FacebookForm.MAIN_TYPE) == null 
            //        && "y".equals(facebookForm.getFromInstallPage()))
            if ("y".equals(facebookForm.getFromInstallPage()))
            { 
                System.out.println("Facebook first install");
                facebookForm.setFirstInstall("y"); 
 
                String apiKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK1_API_KEY);
                
                // Else do the following as this will be the request to install 
                // Ensure facebook values are available
                if (!StringUtils.hasValue(facebookForm.getFb_session_key()) &&
                    !StringUtils.hasValue(facebookForm.getAuth_token())) 
                {
                    errors.add("facebookInput",new ActionError("common.invalid.facebookAPI.invalidparams"));
                    facebookForm.setReloginApiKey(apiKey);
                } 
                else{
                    // Check login credentials  
                     LoginBO loginBO = null;

                        if (!Validator.validateEmail(facebookForm.getLogin(),true)) {
                            errors.add("login",new ActionError("common.mandatory.emailAddress"));
                        }
                        if (!Validator.validateString(facebookForm.getPassword(),true,4,12)) {
                            errors.add("password",new ActionError("common.mandatory.password"));
                        }
                        System.out.println("Checking password");
                        if (errors.size() < 1) {
                            // Checks password
                            loginBO = new LoginBO();


                            User userLogin= new User();
                            logger.debug("Login="+facebookForm.getLogin());
                            userLogin.setEmailAddress(facebookForm.getLogin()); 
                            userLogin.setSecureCode(facebookForm.getPassword());

                            // Checks password
                            User dbUserLogin = loginBO.getUserDetails(facebookForm.getLogin());
                            if (dbUserLogin == null) { 
                                errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                System.out.println("Password mismatch");
                            } else { 

                                // Try use singleton for performance
                                RegisterBO registerBO = new RegisterBO();
                                boolean success = false;
                                try{
                                    success = registerBO.matchSecureCode(userLogin.getEmailAddress(), userLogin.getSecureCode(), dbUserLogin.getSecureCode());
                                } catch(Exception e) {
                                    logger.error("Exception in matching secure code");
                                }
                                registerBO = null;

                                 if (success) { 
                                    System.out.println("Password matched!!");
                                    // Put user id into session
                                    if (dbUserLogin.getUserId() > 0) {
                                        facebookForm.setUserId(""+dbUserLogin.getUserId());
                                        facebookForm.setUserName(dbUserLogin.getUserName()); 
                                        System.out.println("oEight UserId=" + facebookForm.getUserId());
                                    } else {
                                        // Should not happen
                                        errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                    }


                                } else {
                                    errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                }
                            }
                            loginBO = null;

                        }  
                    
                }
                
            }else
            {
                System.out.println("Facebook app already installed");
                // Already installed
                facebookForm.setFirstInstall("n");
                // Gets the userId and userName
                UserWidgetFacebook userWidgetFacebook = UserFacebookBO.getByFacebookId(facebookForm.getFb_user(),FacebookForm.MAIN_TYPE);
                if (userWidgetFacebook != null)
                {
                    facebookForm.setUserId(""+userWidgetFacebook.getUserId());
                    facebookForm.setUserName(userWidgetFacebook.getUserName());
                }
                
            }
        /*}
        else
        { 
            errors.add("facebookInput",new ActionError("common.invalid.facebookAPI.invalidparams"));
        } */
        
        return errors;
    }
    
     
    public static ActionErrors validateCallback3(ActionErrors errors, FacebookForm facebookForm) {
        System.out.println("Validating facebook callback3");
        //if (StringUtils.hasValue(facebookForm.getFb_user()))
        //{
            // If FacebookUserId does not exists in db, is first installation
            //if (UserFacebookBO.getByFacebookId(facebookForm.getUser_id(),FacebookForm.MAIN_TYPE) == null 
            //        && "y".equals(facebookForm.getFromInstallPage()))
            if ("y".equals(facebookForm.getFromInstallPage()))
            { 
                System.out.println("Facebook first install");
                facebookForm.setFirstInstall("y"); 
 
                String apiKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK3_API_KEY);
                
                // Else do the following as this will be the request to install 
                // Ensure facebook values are available
                if (!StringUtils.hasValue(facebookForm.getFb_session_key()) &&
                    !StringUtils.hasValue(facebookForm.getAuth_token())) 
                {
                    errors.add("facebookInput",new ActionError("common.invalid.facebookAPI.invalidparams"));
                    facebookForm.setReloginApiKey(apiKey);
                } 
                else{
                    // Check login credentials  
                     LoginBO loginBO = null;

                        if (!Validator.validateEmail(facebookForm.getLogin(),true)) {
                            errors.add("login",new ActionError("common.mandatory.emailAddress"));
                        }
                        if (!Validator.validateString(facebookForm.getPassword(),true,4,12)) {
                            errors.add("password",new ActionError("common.mandatory.password"));
                        }
                        System.out.println("Checking password");
                        if (errors.size() < 1) {
                            // Checks password
                            loginBO = new LoginBO();


                            User userLogin= new User();
                            logger.debug("Login="+facebookForm.getLogin());
                            userLogin.setEmailAddress(facebookForm.getLogin()); 
                            userLogin.setSecureCode(facebookForm.getPassword());

                            // Checks password
                            User dbUserLogin = loginBO.getUserDetails(facebookForm.getLogin());
                            if (dbUserLogin == null) { 
                                errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                System.out.println("Password mismatch");
                            } else { 

                                // Try use singleton for performance
                                RegisterBO registerBO = new RegisterBO();
                                boolean success = false;
                                try{
                                    success = registerBO.matchSecureCode(userLogin.getEmailAddress(), userLogin.getSecureCode(), dbUserLogin.getSecureCode());
                                } catch(Exception e) {
                                    logger.error("Exception in matching secure code");
                                }
                                registerBO = null;

                                 if (success) { 
                                    System.out.println("Password matched!!");
                                    // Put user id into session
                                    if (dbUserLogin.getUserId() > 0) {
                                        facebookForm.setUserId(""+dbUserLogin.getUserId());
                                        facebookForm.setUserName(dbUserLogin.getUserName()); 
                                        System.out.println("oEight UserId=" + facebookForm.getUserId());
                                          
                                        // Successful login, checks blog information here (if not moderated, it will show in ScriptsAction )
                                        UserBO userBO = new UserBO();
                                        UserDetails userDetails = userBO.getUserContactById(""+dbUserLogin.getUserId());
                                        if (userDetails == null)
                                        {
                                            errors.add("blog",new ActionError("common.invalid.facebookAPI.blogFeatureNotFound"));
                                        }
                                        else if (userDetails != null && 
                                             (!StringUtils.hasValue(userDetails.getWebBlog()) || !userDetails.getWebBlogFeature()))
                                        {
                                            errors.add("blog",new ActionError("common.invalid.facebookAPI.blogFeatureNotFound")); 
                                        }
                                          
                                    } else {
                                        // Should not happen
                                        errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                    }


                                } else {
                                    errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                }
                            }
                            loginBO = null;

                        }  
                    
                }
                
            }else
            {
                System.out.println("Facebook app already installed");
                // Already installed
                facebookForm.setFirstInstall("n");
                // Gets the userId and userName
                UserWidgetFacebook userWidgetFacebook = UserFacebookBO.getByFacebookId(facebookForm.getFb_user(),FacebookForm.FEATURED_BLOG_TYPE);
                if (userWidgetFacebook != null)
                {
                    facebookForm.setUserId(""+userWidgetFacebook.getUserId());
                    facebookForm.setUserName(userWidgetFacebook.getUserName());
                }
                
            }
        /*}
        else
        { 
            errors.add("facebookInput",new ActionError("common.invalid.facebookAPI.invalidparams"));
        } */
        
        return errors;
    }
        
    public static ActionErrors validateCallback6(ActionErrors errors, FacebookForm facebookForm) {
        System.out.println("Validating facebook callback6"); 
            if ("y".equals(facebookForm.getFromInstallPage()))
            { 
                System.out.println("Facebook first install");
                facebookForm.setFirstInstall("y"); 
 
                String apiKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK6_API_KEY);
                
                // Else do the following as this will be the request to install 
                // Ensure facebook values are available
                if (!StringUtils.hasValue(facebookForm.getFb_session_key()) &&
                    !StringUtils.hasValue(facebookForm.getAuth_token())) 
                {
                    errors.add("facebookInput",new ActionError("common.invalid.facebookAPI.invalidparams"));
                    facebookForm.setReloginApiKey(apiKey);
                }   
                // Check fields 
                if (!Validator.validateInteger(Integer.toString(facebookForm.getPledgeCode()),true,1,999)) {
                    errors.add("pledgeCode",new ActionError("common.invalid.earthcampaign.pledgeCode"));
                } 
                
            }else
            {
                System.out.println("Facebook app already installed");
                // Already installed
                facebookForm.setFirstInstall("n");
                // Gets the userId and userName
                // Not used!
                /*UserWidgetFacebook userWidgetFacebook = UserFacebookBO.getByFacebookId(facebookForm.getFb_user(),FacebookForm.CLIMATE_CHANGE_TYPE);
                if (userWidgetFacebook != null)
                {
                    facebookForm.setUserId(""+userWidgetFacebook.getUserId());
                    facebookForm.setUserName(userWidgetFacebook.getUserName());
                }*/
                
            }
        /*}
        else
        { 
            errors.add("facebookInput",new ActionError("common.invalid.facebookAPI.invalidparams"));
        } */
        
        return errors;
    }
    
}
