/*
 * WishValidator.java
 *
 * Created on December 3, 2007, 1:56 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserTrackBO;
import com.esmart2u.oeight.member.bo.UserWishesBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.Validator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 *
 * @author meauchyuan.lee
 */
public class WishValidator {
    
    /** Creates a new instance of WishValidator */
    public WishValidator() {
    }
     
    private static Logger logger = Logger.getLogger(WishValidator.class); 
    
    public static ActionErrors validateListSubmit(ActionErrors errors, WishForm wishForm) {
        
        if (!Validator.validateString(wishForm.getWishContent(),false,0,500,false)) {
            errors.add("wishContent",new ActionError("common.invalid.wishContent"));
        }
        
        return errors;
    }
    
    public static ActionErrors validateMainSubmit(ActionErrors errors, WishForm wishForm) {   
        if (!Validator.validateNumber(""+wishForm.getWishId(),true,1,500)) {
            errors.add("wishId",new ActionError("common.invalid.wishId"));
        }  
        
        return errors;
    }

    static ActionErrors validateRequestFriend(ActionErrors errors, WishForm wishForm) {
        wishForm.setHasError(false);
        String userId = (String)wishForm.getActionContext().getRequest().getSession().getAttribute(OEightConstants.SESSION_USER_ID);
        long userIdLong = Long.parseLong(userId);
        char buddyStatus = UserWishesBO.getRequestBuddyStatus(userIdLong, wishForm.getUserName());
        System.out.println("BuddyStatus:" + buddyStatus + " from:" + userIdLong +":" + wishForm.getUserName());
        if (OEightConstants.BUDDY_IS_FREN == buddyStatus)
        {
            errors.add("requestFren", new ActionError("common.invalid.requestFrenAlreadyFren"));
        }
        else if (OEightConstants.BUDDY_NEW_REQUEST == buddyStatus)
        {
            errors.add("requestFren", new ActionError("common.invalid.requestFrenAlreadyRequested"));
        }
        else //if (OEightConstants.BUDDY_BLOCKED == buddyStatus
             //   || OEightConstants.BUDDY_DELETED == buddyStatus)
        {
            // do nothing, allow adding again
            // but before that check if already exceeded limit
            int currentAdded = UserTrackBO.getTrackValue(userId, OEightConstants.TRACK_ADD_BUDDY);
            String key = PropertyConstants.TRACKER_MAX_PREFIX + OEightConstants.TRACK_ADD_BUDDY + PropertyConstants.TRACKER_MAX_POSTFIX;
            if (PropertyManager.getInt(key)<currentAdded)
            {
                errors.add("requestFren", new ActionError("common.invalid.requestFrenDailyLimitExceeded")); 
            }
        }
        
        if (errors.size() > 0)
        {
            // Sets the photo
            UserBO userBO = new UserBO();
            User user = userBO.getUserByUserName(wishForm.getUserName());
            wishForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            wishForm.setHasError(true);
        }
        return errors;
    }
    
    static ActionErrors validateConfirmFriend(ActionErrors errors, WishForm wishForm) {
        wishForm.setHasError(false);
        String userId = (String)wishForm.getActionContext().getRequest().getSession().getAttribute(OEightConstants.SESSION_USER_ID);
        long userIdLong = Long.parseLong(userId);
        char buddyStatus = UserWishesBO.getRequestBuddyStatus(userIdLong, wishForm.getUserName());
        System.out.println("BuddyStatus:" + buddyStatus + " from:" + userIdLong +":" + wishForm.getUserName());
        if (OEightConstants.BUDDY_IS_FREN == buddyStatus)
        {
            errors.add("requestFren", new ActionError("common.invalid.requestFrenAlreadyFren"));
        } 
        else if (OEightConstants.BUDDY_BLOCKED == buddyStatus
                || OEightConstants.BUDDY_DELETED == buddyStatus)
        {
            // do nothing, allow adding again
        }
        
        if (errors.size() > 0)
        {
            // Sets the photo
            UserBO userBO = new UserBO();
            User user = userBO.getUserByUserName(wishForm.getUserName());
            wishForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            wishForm.setHasError(true);
        }
        return errors;
    }

    static ActionErrors validateDeleteFriend(ActionErrors errors, WishForm wishForm) {
        wishForm.setHasError(false);
        String userId = (String)wishForm.getActionContext().getRequest().getSession().getAttribute(OEightConstants.SESSION_USER_ID);
        long userIdLong = Long.parseLong(userId);
        char buddyStatus = UserWishesBO.getRequestBuddyStatus(userIdLong, wishForm.getUserName());
        System.out.println("BuddyStatus:" + buddyStatus + " from:" + userIdLong +":" + wishForm.getUserName());
        if (OEightConstants.BUDDY_NOT_FREN == buddyStatus)
        {
            errors.add("deleteFren", new ActionError("common.invalid.deleteFrenNotFren"));
        }
        /*else if (OEightConstants.BUDDY_BLOCKED == buddyStatus
                || OEightConstants.BUDDY_DELETED == buddyStatus)
        {
            // do nothing, allow delete again
        }*/
        
        if (errors.size() > 0)
        {
            // Sets the photo
            UserBO userBO = new UserBO();
            User user = userBO.getUserByUserName(wishForm.getUserName());
            wishForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            wishForm.setHasError(true);
        }
        return errors;
    }
}
