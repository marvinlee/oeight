/*
 * ProfileValidator.java
 *
 * Created on October 4, 2007, 11:01 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.aetrion.flickr.Flickr;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserVotesBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.oeight.member.web.struts.helper.MaritalStatusComboHelper;
import com.esmart2u.oeight.member.web.struts.helper.StateComboHelper;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.PhotoUploadHelper;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.web.struts.service.FlickrService;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicException;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import java.io.FileNotFoundException;
import java.io.IOException; 
import org.apache.struts.upload.FormFile;

/**
 *
 * @author meauchyuan.lee
 */
public class ProfileValidator {
    
    private static Logger logger = Logger.getLogger(ProfileValidator.class);
    
    
    /** Creates a new instance of ProfileValidator */
    public ProfileValidator() {
    }
     
    
    public static ActionErrors validateProfileMain(ActionErrors errors, ProfileForm profileForm) {
      
        if (!Validator.validateString(profileForm.getMyWish(),false,0,500,false)) {
            errors.add("myWish",new ActionError("common.invalid.myWish"));
        }
        if (!Validator.validateString(profileForm.getName(),true,1,100)) {
            errors.add("name",new ActionError("common.invalid.name"));
        } 
        //if (!Validator.validateString(profileForm.getNric(),false,0,12)) {
        //    errors.add("nric",new ActionError("common.invalid.nric"));
        //} 
        if (!Validator.validateMandatoryField(Character.toString(profileForm.getGender()))) {
            errors.add("gender",new ActionError("common.invalid.gender"));
        }
        if (!MaritalStatusComboHelper.isAcceptedStatusCode(profileForm.getMaritalStatus())) {
            errors.add("maritalStatus",new ActionError("common.invalid.maritalStatus"));
        }
        if (!Validator.validateString(profileForm.getBirthdayString(),true,10,10)) {
            errors.add("birthdayString",new ActionError("common.invalid.birthdayString"));
        }
        else
        {
            // Check is it between 14 and more
            if (DateObjectHelper.getAge(profileForm.getBirthDate().getTime()) < 14)
            { 
                errors.add("birthdayString",new ActionError("common.invalid.birthdayAgeTooYoung")); 
            } 
        }
        
        if (!Validator.validateString(profileForm.getCity(),false,0,100)) {
            errors.add("city",new ActionError("common.invalid.city"));
        } 
        if (!CountryComboHelper.isAcceptedCountryCode(profileForm.getNationality())){
            errors.add("nationality",new ActionError("common.invalid.nationality"));
        }
        else
        {
            if (profileForm.getNationality().equals(
                    PropertyManager.getValue(OEightConstants.COUNTRY_DEFAULT_KEY)))
            {
                // Country is accepted, check state 
                if (!StateComboHelper.isAcceptedStateCode(profileForm.getState())) {
                    errors.add("state",new ActionError("common.invalid.state"));
                }
            }
        }
        if (!Validator.validateString(profileForm.getCurrentLocation(),false,0,100,false)) {
            errors.add("currentLocation",new ActionError("common.invalid.currentLocation"));
        } 
        if (!Validator.validateString(profileForm.getSchools(),false,0,100,false)) {
            errors.add("schools",new ActionError("common.invalid.schools"));
        } 
        if (!Validator.validateString(profileForm.getOccupation(),false,0,100,false)) {
            errors.add("occupation",new ActionError("common.invalid.occupation"));
        } 
        if (!Validator.validateString(profileForm.getCompany(),false,0,100,false)) {
            errors.add("company",new ActionError("common.invalid.company"));
        } 
        if (!Validator.validateEmail(profileForm.getPublicEmail(),false)) {
            errors.add("publicEmail",new ActionError("common.invalid.publicEmail"));
        } 
        
        return errors;
        
    }
    
    /**
     *  Photo Validation
     */
    public static ActionErrors validateProfilePhoto(ActionErrors errors, ProfileForm profileForm) {
        
        // Mosaic Photo check 
        FormFile photoFile = profileForm.getPhotoFile();
        if (photoFile != null){
            checkUploadedFile(errors, photoFile, "photoPath");
        }
        if (!Validator.validateString(profileForm.getPhotoDescription(),false,0,500,false)) {
            errors.add("photoDescription",new ActionError("common.invalid.photoDescription"));
        }
        
        // Profile Photo check 
        FormFile profilePhotoFile = profileForm.getProfilePhotoFile();
        if (profilePhotoFile != null){
            checkUploadedFile(errors, profilePhotoFile, "profilePhotoPath");
        }
        if (!Validator.validateString(profileForm.getProfilePhotoDescription(),false,0,500,false)) {
            errors.add("profilePhotoDescription",new ActionError("common.invalid.photoDescription"));
        }
        
        // Shirt Photo check 
        /*FormFile shirtPhotoFile = profileForm.getShirtPhotoFile();
        if (shirtPhotoFile != null){
            checkUploadedFile(errors, shirtPhotoFile, "shirtPhotoPath");
        }
        if (!Validator.validateString(profileForm.getShirtPhotoDescription(),false,0,500)) {
            errors.add("shirtPhotoDescription",new ActionError("common.invalid.photoDescription"));
        }*/
        
        
        return errors;
        
    }   
    
    private static ActionErrors checkUploadedFile(ActionErrors errors, FormFile photoFile, String errorKey)
    {
            String mimeType = photoFile.getContentType();  
            logger.debug("Uploaded filesize=" + photoFile.getFileSize());

            /*if (photoFile == null || photoFile.getFileSize()<1) { 
                logger.debug("Checking zero filezie");
                errors.add(errorKey,new ActionError("common.invalid.photoPath"));
            } else */
            if (photoFile.getFileSize() > 0){
                if (photoFile.getFileSize() > 250000) { 
                    logger.debug("Check not over filesize");
                    errors.add(errorKey,new ActionError("common.invalid.photoPathSize"));
                } else if (!PhotoUploadHelper.isAcceptedMimeType(mimeType)) { 
                    logger.debug("mimetype not accepted");
                    errors.add(errorKey,new ActionError("common.invalid.photoPathContentType"));
                } else {
                    try {
                        logger.debug("Checking magic content");
                        Magic parser = new Magic() ;
                        MagicMatch match;
                        match = parser.getMagicMatch(photoFile.getFileData());
                        logger.debug("Actual file mimetype=" + match.getMimeType()) ;

                        //if (!match.mimeTypeMatches(mimeType)) { 
                        // the actual mimetype string might not match
                        // so we take if it is of accepted mimetype
                        if (!PhotoUploadHelper.isAcceptedMimeType(match.getMimeType())) {
                            logger.debug("Mimetype of actual file not match"); 
                            errors.add(errorKey,new ActionError("common.mismatch.photoPathMime"));
                        }

                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.invalid.photoPath"));
                    } catch (MagicMatchNotFoundException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.mismatch.photoPathMime"));
                    } catch (MagicParseException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.mismatch.photoPathMime"));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.invalid.photoPath"));
                    } catch (MagicException ex) {
                        ex.printStackTrace();
                        errors.add(errorKey,new ActionError("common.mismatch.photoPathMime"));
                    }
               }
            }
        return errors;
    }
    
    public static ActionErrors validateProfileLifestyle(ActionErrors errors, ProfileForm profileForm) {
   
        /*if (!Validator.validateMandatoryField(Character.toString(profileForm.getLifeSmoker()))) {
            errors.add("lifeSmoker",new ActionError("common.invalid.lifeSmoker"));
        }
        if (!Validator.validateMandatoryField(Character.toString(profileForm.getLifeDrinker()))) {
            errors.add("lifeDrinker",new ActionError("common.invalid.lifeDrinker"));
        }*/
        if (!Validator.validateString(profileForm.getLifeAboutMe(),false,0,500,false)) {
            errors.add("lifeAboutMe",new ActionError("common.invalid.lifeAboutMe"));
        } 
        if (!Validator.validateString(profileForm.getLifeToKnow(),false,0,500,false)) {
            errors.add("lifeToKnow",new ActionError("common.invalid.lifeToKnow"));
        } 
        if (!Validator.validateString(profileForm.getLifeKnowMe(),false,0,500,false)) {
            errors.add("lifeKnowMe",new ActionError("common.invalid.lifeKnowMe"));
        } 
        if (!Validator.validateString(profileForm.getLife1(),false,0,100,false)) {
            errors.add("life1",new ActionError("common.invalid.life1"));
        } 
        if (!Validator.validateString(profileForm.getLife2(),false,0,100,false)) {
            errors.add("life2",new ActionError("common.invalid.life2"));
        } 
        if (!Validator.validateString(profileForm.getLife3(),false,0,100,false)) {
            errors.add("life3",new ActionError("common.invalid.life3"));
        } 
        if (!Validator.validateString(profileForm.getLife4(),false,0,100,false)) {
            errors.add("life4",new ActionError("common.invalid.life4"));
        } 
        if (!Validator.validateString(profileForm.getLife5(),false,0,100,false)) {
            errors.add("life5",new ActionError("common.invalid.life5"));
        } 
        if (!Validator.validateString(profileForm.getLifeValue1(),false,0,500,false)) {
            errors.add("lifeValue1",new ActionError("common.invalid.lifeValue1"));
        } 
        if (!Validator.validateString(profileForm.getLifeValue2(),false,0,500,false)) {
            errors.add("lifeValue2",new ActionError("common.invalid.lifeValue2"));
        } 
        if (!Validator.validateString(profileForm.getLifeValue3(),false,0,500,false)) {
            errors.add("lifeValue3",new ActionError("common.invalid.lifeValue3"));
        } 
        if (!Validator.validateString(profileForm.getLifeValue4(),false,0,500,false)) {
            errors.add("lifeValue4",new ActionError("common.invalid.lifeValue4"));
        } 
        if (!Validator.validateString(profileForm.getLifeValue5(),false,0,500,false)) {
            errors.add("lifeValue5",new ActionError("common.invalid.lifeValue5"));
        } 
        return errors;
    } 
    
    public static ActionErrors validateProfileInterest(ActionErrors errors, ProfileForm profileForm) {
   
        if (!Validator.validateString(profileForm.getInterestActor(),false,0,200,false)) {
            errors.add("interestActor",new ActionError("common.invalid.interestActor"));
        } 
        if (!Validator.validateString(profileForm.getInterestActress(),false,0,200,false)) {
            errors.add("interestActress",new ActionError("common.invalid.interestActress"));
        } 
        if (!Validator.validateString(profileForm.getInterestSinger(),false,0,200,false)) {
            errors.add("interestSinger",new ActionError("common.invalid.interestSinger"));
        } 
        if (!Validator.validateString(profileForm.getInterestMusic(),false,0,200,false)) {
            errors.add("interestMusic",new ActionError("common.invalid.interestMusic"));
        } 
        if (!Validator.validateString(profileForm.getInterestBand(),false,0,200,false)) {
            errors.add("interestBand",new ActionError("common.invalid.interestBand"));
        } 
        if (!Validator.validateString(profileForm.getInterestMovies(),false,0,200,false)) {
            errors.add("interestMovies",new ActionError("common.invalid.interestMovies"));
        } 
        if (!Validator.validateString(profileForm.getInterestBooks(),false,0,200,false)) {
            errors.add("interestBooks",new ActionError("common.invalid.interestBooks"));
        } 
        if (!Validator.validateString(profileForm.getInterestSports(),false,0,200,false)) {
            errors.add("interestSports",new ActionError("common.invalid.interestSports"));
        } 
        if (!Validator.validateString(profileForm.getInterestWebsites(),false,0,200,false)) {
            errors.add("interestWebsites",new ActionError("common.invalid.interestWebsites"));
        }  
        if (!Validator.validateString(profileForm.getInterest1(),false,0,100,false)) {
            errors.add("interest1",new ActionError("common.invalid.interest1"));
        } 
        if (!Validator.validateString(profileForm.getInterest2(),false,0,100,false)) {
            errors.add("interest2",new ActionError("common.invalid.interest2"));
        } 
        if (!Validator.validateString(profileForm.getInterest3(),false,0,100,false)) {
            errors.add("interest3",new ActionError("common.invalid.interest3"));
        } 
        if (!Validator.validateString(profileForm.getInterest4(),false,0,100,false)) {
            errors.add("interest4",new ActionError("common.invalid.interest4"));
        } 
        if (!Validator.validateString(profileForm.getInterest5(),false,0,100,false)) {
            errors.add("interest5",new ActionError("common.invalid.interest5"));
        } 
        if (!Validator.validateString(profileForm.getInterestValue1(),false,0,500,false)) {
            errors.add("interestValue1",new ActionError("common.invalid.interestValue1"));
        } 
        if (!Validator.validateString(profileForm.getInterestValue2(),false,0,500,false)) {
            errors.add("interestValue2",new ActionError("common.invalid.interestValue2"));
        } 
        if (!Validator.validateString(profileForm.getInterestValue3(),false,0,500,false)) {
            errors.add("interestValue3",new ActionError("common.invalid.interestValue3"));
        } 
        if (!Validator.validateString(profileForm.getInterestValue4(),false,0,500,false)) {
            errors.add("interestValue4",new ActionError("common.invalid.interestValue4"));
        } 
        if (!Validator.validateString(profileForm.getInterestValue5(),false,0,500,false)) {
            errors.add("interestValue5",new ActionError("common.invalid.interestValue5"));
        } 
        return errors;
    }
    
    public static ActionErrors validateProfileSkills(ActionErrors errors, ProfileForm profileForm) {
   
        if (!Validator.validateString(profileForm.getSkillsExpert(),false,0,500,false)) {
            errors.add("skillsExpert",new ActionError("common.invalid.skillsExpert"));
        } 
        if (!Validator.validateString(profileForm.getSkillsGood(),false,0,500,false)) {
            errors.add("skillsGood",new ActionError("common.invalid.skillsGood"));
        } 
        if (!Validator.validateString(profileForm.getSkillsLearn(),false,0,500,false)) {
            errors.add("skillsLearn",new ActionError("common.invalid.skillsLearn"));
        } 
        return errors;
    }
    
    public static ActionErrors validateProfileContact(ActionErrors errors, ProfileForm profileForm) {
   
        if (!Validator.validatePhoneNumber(profileForm.getContactPhone(),false,null)) {
            errors.add("contactPhone",new ActionError("common.invalid.contactPhone"));
        } 
        if (!Validator.validateEmail(profileForm.getContactGoogle(),false)){//,0,200)) {
            errors.add("contactGoogle",new ActionError("common.invalid.contactGoogle"));
        } 
        if (!Validator.validateEmail(profileForm.getContactYahoo(),false)){//,0,200)) {
            errors.add("contactYahoo",new ActionError("common.invalid.contactYahoo"));
        } 
        if (!Validator.validateEmail(profileForm.getContactMsn(),false)){//,0,200)) {
            errors.add("contactMsn",new ActionError("common.invalid.contactMsn"));
        } 
        if (!Validator.validateString(profileForm.getWebFriendster(),false,0,200,false)) {
            errors.add("webFriendster",new ActionError("common.invalid.webFriendster"));
        } 
        if (!Validator.validateString(profileForm.getWebHi5(),false,0,200,false)) {
            errors.add("webHi5",new ActionError("common.invalid.webHi5"));
        } 
        if (!Validator.validateString(profileForm.getWebWayn(),false,0,200,false)) {
            errors.add("webWayn",new ActionError("common.invalid.webWayn"));
        } 
        if (!Validator.validateString(profileForm.getWebOrkut(),false,0,200,false)) {
            errors.add("webOrkut",new ActionError("common.invalid.webOrkut"));
        } 
        if (!Validator.validateString(profileForm.getWebFacebook(),false,0,200,false)) {
            errors.add("webFacebook",new ActionError("common.invalid.webFacebook"));
        } 
        if (!Validator.validateString(profileForm.getWebMyspace(),false,0,200,false)) {
            errors.add("webMyspace",new ActionError("common.invalid.webMyspace"));
        } 
        if (!Validator.validateString(profileForm.getWebBlog(),false,0,200,false)) {
            errors.add("webBlog",new ActionError("common.invalid.webBlog"));
        }
        if (!Validator.validateString(profileForm.getWeb1(),false,0,100,false)) {
            errors.add("web1",new ActionError("common.invalid.web1"));
        } 
        if (!Validator.validateString(profileForm.getWeb2(),false,0,100,false)) {
            errors.add("web2",new ActionError("common.invalid.web2"));
        } 
        if (!Validator.validateString(profileForm.getWeb3(),false,0,100,false)) {
            errors.add("web3",new ActionError("common.invalid.web3"));
        } 
        if (!Validator.validateString(profileForm.getWeb4(),false,0,100,false)) {
            errors.add("web4",new ActionError("common.invalid.web4"));
        } 
        if (!Validator.validateString(profileForm.getWeb5(),false,0,100,false)) {
            errors.add("web5",new ActionError("common.invalid.web5"));
        } 
        if (!Validator.validateString(profileForm.getWebValue1(),false,0,500,false)) {
            errors.add("webValue1",new ActionError("common.invalid.webValue1"));
        } 
        if (!Validator.validateString(profileForm.getWebValue2(),false,0,500,false)) {
            errors.add("webValue2",new ActionError("common.invalid.webValue2"));
        } 
        if (!Validator.validateString(profileForm.getWebValue3(),false,0,500,false)) {
            errors.add("webValue3",new ActionError("common.invalid.webValue3"));
        } 
        if (!Validator.validateString(profileForm.getWebValue4(),false,0,500,false)) {
            errors.add("webValue4",new ActionError("common.invalid.webValue4"));
        } 
        if (!Validator.validateString(profileForm.getWebValue5(),false,0,500,false)) {
            errors.add("webValue5",new ActionError("common.invalid.webValue5"));
        }   
        return errors;
    }
    
    public static ActionErrors validateVote(ActionErrors errors, ProfileForm profileForm) {
        try { 
            String userId = (String)profileForm.getActionContext().getRequest().getSession()
                            .getAttribute(OEightConstants.SESSION_USER_ID);
            UserBO userBO = new UserBO();
            User user = userBO.getUserByUserName(profileForm.getUserName());
            if  (!Validator.validateString(profileForm.getId(),true,1,50)) {
                errors.add("vote",new ActionError("common.invalid.votedUserId"));
            } 
            else if (user == null || user.getUserId()<1)
            { 
                errors.add("vote",new ActionError("common.invalid.votedUserId"));  
            }
            else if (UserVotesBO.getVotesLeft(userId) < 1) {
                errors.add("vote",new ActionError("common.deny.voting"));
            }   
            else
            {
                // DO NOT ALLOW SELF VOTE TWICE
                if (!UserVotesBO.validVoting(userId, Long.toString(user.getUserId())))
                { 
                    errors.add("vote",new ActionError("common.invalid.selfVoting"));   
                }
            
            }
        } catch (Exception e ) {
            errors.add("vote",new ActionError("common.invalid.vote"));
        }
        
        
    
        return errors;
    }  
    
    
    public static ActionErrors validateFlickr(ActionErrors errors, ProfileForm profileForm) { 
        String userId = (String)profileForm.getActionContext().getRequest().getSession()
                        .getAttribute(OEightConstants.SESSION_USER_ID);
        UserBO userBO = null; 

        // No FlickrId, then expect YahooId
        if  (!StringUtils.hasValue(profileForm.getFlickrId())) {
            System.out.println("No Flickr ID Found!");
            // Check YahooId
            if (!StringUtils.hasValue(profileForm.getYahooId()))
            {
                System.out.println("No Yahoo ID Found!");
                errors.add("flickrId",new ActionError("common.invalid.flickrIdOrYahooId"));
            }
            else
            {
               System.out.println("Has Yahoo try to get FlickrId with it");
               // Has got Yahoo Id, get FlickrId with it 
               FlickrService service = new FlickrService();
               String flickrId = service.getUserIdByUsername(profileForm.getYahooId());
               service = null;
               userBO = new UserBO();
               System.out.println("Flickr ID returned = " + flickrId);
               if (!StringUtils.hasValue(flickrId))
               {  
                    // Save yahoo id
                    userBO.saveFlickerInfo(userId,profileForm.getYahooId(),flickrId);
                    errors.add("flickrId",new ActionError("common.notfound.flickrId"));
               }
               else
               {
                   // This is actually where both yahooid and flickrid is found
                   profileForm.setFlickrId(flickrId);
                    // Save yahoo id
                    userBO.saveFlickerInfo(userId,profileForm.getYahooId(),flickrId);
                    errors.add("flickrId",new ActionError("common.found.flickrId"));
               }
                
            } 
        }  
        else
        {
           System.out.println("HAS Flickr id");
            // Has FlickrId, try to validate it 
           FlickrService service = new FlickrService();
           boolean validFlickr = service.validFlickrId(profileForm.getFlickrId());
           service = null;
           if (!validFlickr)
           {  
                System.out.println("Invalid Flickr Id?");
                errors.add("flickrId",new ActionError("common.notfound.flickrId"));
           }
        } 
        userBO = null;
    
        return errors;
    }






}


