/*
 * ShopForm.java
 *
 * Created on February 13, 2008, 2:36 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.UserShop;
import com.esmart2u.solution.base.helper.BeanUtils;
import org.apache.log4j.Logger;
import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class ShopForm  extends AbstractApplicationActionForm {
    
    public static final String NEW = "N";
    public static final String NEW_NOTIFIED = "S";
    public static final String NEW_FAIL_NOTIFY = "F";
    public static final String PAYMENT_CONFIRMED = "C";
    public static final String IN_TRANSIT = "T";
    public static final String DELIVERED = "D";
    
    private static Logger logger = Logger.getLogger(ShopForm.class);
    
    private long userId;
    private String shopCode;
    private String shopDescription;
    private long shopReferenceNo;
    private String name;
    private String contactNo;
    private String email;
    private String address;
    private String shopStatus;
    private String trackingNo;
    
    // For couple tee
    private String f; // girl size
    private String m; // guy size
    
    private List orderList;
    
    /** Creates a new instance of ShopForm */
    public ShopForm() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopDescription() {
        return shopDescription;
    }

    public void setShopDescription(String shopDescription) {
        this.shopDescription = shopDescription;
    }

    public long getShopReferenceNo() {
        return shopReferenceNo;
    }

    public void setShopReferenceNo(long shopReferenceNo) {
        this.shopReferenceNo = shopReferenceNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getShopStatus() {
        return shopStatus;
    }

    public void setShopStatus(String shopStatus) {
        this.shopStatus = shopStatus;
    }

    public String getTrackingNo() {
        return trackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        this.trackingNo = trackingNo;
    }
     
    public List getOrderList() {
        return orderList;
    }

    public void setOrderList(List orderList) {
        this.orderList = orderList;
    }
    
    UserShop getUserShop() {
        UserShop userShop = new UserShop();
        userShop.setUserId(this.userId);
        userShop.setShopCode(this.shopCode);
        userShop.setShopDescription(this.shopDescription);
        userShop.setShopReferenceNo(this.shopReferenceNo);
        userShop.setName(this.name);
        userShop.setContactNo(this.contactNo);
        userShop.setEmail(this.email);
        userShop.setAddress(this.address);
        userShop.setShopStatus(this.shopStatus);
        userShop.setTrackingNo(this.trackingNo); 
        return userShop;
    }
    
    void setUserShop(UserShop userShop) { 
        try {
            
            BeanUtils.copyBean(userShop, this);  
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public String getCoupleTeeDescription()
    {
        return "f="+f+",m="+m;
    }
   
    
    
}
