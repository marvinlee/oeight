/*
 * SeasonGreetingAction.java
 *
 * Created on November 14, 2007, 11:59 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.SeasonGreeting;
import com.esmart2u.oeight.member.bo.SeasonGreetingBO;
import com.esmart2u.oeight.member.helper.smtp.SeasonsGreetingsEmailSender;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class SeasonGreetingAction  extends AbstractApplicationAction {
     
    private static String SEND = "send";
    private static String SEND_SUBMITTED = "sendSubmitted";
    private static String RETRIEVE = "retrieve";
    private static String STATIC1 = "static1";
    private static String STATIC2 = "static2";
    private static String CONTEST = "contest";    
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(SeasonGreetingAction.class);
    
    
    
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (SEND_SUBMITTED.equals(event)|| RETRIEVE.equals(event))
            return true;
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        SeasonGreetingForm greetingForm = (SeasonGreetingForm)actionForm; 
        if (SEND_SUBMITTED.equals(action)) {
            errors = SeasonGreetingValidator.validateSend(errors, greetingForm); 
        } else if (RETRIEVE.equals(action)) {
            errors = SeasonGreetingValidator.validateRetrieval(errors, greetingForm);
        } else {
            logger.debug("Temporary skip");
        }
        
        greetingForm.getActionContext().setActionErrors(errors);
        return greetingForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (SEND_SUBMITTED.equals(action)) {
            action = SEND;
        } else if (RETRIEVE.equals(action)) {
            action = RETRIEVE;
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doSend(SeasonGreetingForm actionForm)
    throws ApplicationException {
        try {
            int type = actionForm.getT();
            actionForm.clear(); 
            actionForm.setT(type);
            return SEND;
        } catch (Exception e) {
            return SEND;
        }
    }
    
    
    public String doSendSubmitted(SeasonGreetingForm actionForm)
    throws ApplicationException {
        try {
            
            // Save into DB and send email
            SeasonGreeting seasonGreeting = new SeasonGreeting();
            seasonGreeting.setSenderEmail(actionForm.getSenderEmail());
            seasonGreeting.setSenderName(actionForm.getSenderName());
            seasonGreeting.setReceiverEmail(actionForm.getReceiverEmail());
            seasonGreeting.setReceiverName(actionForm.getReceiverName());
            seasonGreeting.setSubscribe(actionForm.getSubscribe());
            seasonGreeting.setMessage(actionForm.getMessage());
            seasonGreeting.setType(actionForm.getT());
            
            SeasonGreetingBO greetingBO = new SeasonGreetingBO();
            greetingBO.sendGreetings(seasonGreeting);
            greetingBO = null;
              
            // No need to explicitly invoke, email daemon runs independantly
            //SeasonsGreetingsEmailSender.getInstance().invoke();
            
            return SEND_SUBMITTED;
        } catch (Exception e) {
            e.printStackTrace();
            return SEND;
        }
    }
     
    
    public String doRetrieve(SeasonGreetingForm actionForm)
    throws ApplicationException {
        try {
            
            // ActionForm should have been populated by validator
            // Get greeting card with sender's email and greetingCode 
            logger.debug("Sender Email:" + actionForm.getSenderEmail());
            logger.debug("Sender Name:" + actionForm.getSenderName());
            logger.debug("Receiver Email:" + actionForm.getReceiverEmail());
            logger.debug("Receiver Name:" + actionForm.getReceiverName());
            logger.debug("Greeting Message:" + actionForm.getMessage());
            logger.debug("Card Type:" + actionForm.getT());
            
            // Update as retrieved 
            SeasonGreetingBO greetingBO = new SeasonGreetingBO();
            SeasonGreeting seasonGreeting = new SeasonGreeting();
            seasonGreeting.setGreetingId(actionForm.getGreetingId());
            greetingBO.setGreetingRetrieved(seasonGreeting);
            greetingBO = null;
            
            return RETRIEVE;
        } catch (Exception e) {
            return RETRIEVE;
        }
    }
    
    
    protected String getDefaultActionName() {
                  
        return SEND;
    }
     
    public String doStatic1(SeasonGreetingForm actionForm)
    throws ApplicationException {
        try { 
            return STATIC1;
        } catch (Exception e) {
            return SEND;
        }
    } 
    public String doStatic2(SeasonGreetingForm actionForm)
    throws ApplicationException {
        try { 
            return STATIC2;
        } catch (Exception e) {
            return SEND;
        }
    } 
    public String doContest(SeasonGreetingForm actionForm)
    throws ApplicationException {
        try { 
            return CONTEST;
        } catch (Exception e) {
            return SEND;
        }
    }
    
}
