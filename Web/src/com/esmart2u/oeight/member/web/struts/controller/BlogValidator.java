package com.esmart2u.oeight.member.web.struts.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import com.esmart2u.oeight.admin.helper.service.RSSPuller;
import com.esmart2u.oeight.data.UserBlog;
import com.esmart2u.oeight.data.UserPost;
import com.esmart2u.oeight.member.bo.BlogBO;
import com.esmart2u.oeight.member.bo.PostBO;
import com.esmart2u.oeight.member.helper.HTMLPageRequestor;
import com.esmart2u.solution.base.helper.Validator;

public class BlogValidator { 
	      
		    private static Logger logger = Logger.getLogger(BlogValidator.class);
		    
		    /** Creates a new instance of BlogValidator */
		    public BlogValidator() {
		    }
		      
		    public static ActionErrors checkBlog(ActionErrors errors, BlogForm blogForm) {

		        BlogBO blogBO = new BlogBO();
		        if (!Validator.validateString(blogForm.getBlogUrl(),true,1,100,false)) {
		            errors.add("url",new ActionError("common.invalid.bloglink.url"));
		        }
		        else
		        {
			        try { 
			            //Retrieve URL
			            ArrayList pageTitleList = HTMLPageRequestor.fetchPage(new URL(blogForm.getBlogUrl()));
			            if (pageTitleList == null || pageTitleList.isEmpty() 
			                || pageTitleList.get(0) == null || "".equals(pageTitleList.get(0).toString().trim()))
			            {
			                errors.add("url",new ActionError("common.invalid.bloglink.urlNotOK")); 
			            }
			            else
			            {
			            	//Got the title
			            	blogForm.setBlogName(pageTitleList.get(0).toString().trim());
			            }
			            
			        } catch (MalformedURLException ex) {
			                errors.add("url",new ActionError("common.invalid.bloglink.urlNotOK")); 
			        }
		        
			        //Check already exist in db 
			        UserBlog userBlog = blogBO.getBlogWithUrl(blogForm.getBlogUrl());
			        if (userBlog != null && userBlog.getUserId()!= blogForm.getUserId())
			        {
			            errors.add("url",new ActionError("common.exists.bloglink.url"));  
			            blogForm.setBlogName(userBlog.getBlogName());  
			            blogForm.setUrlDuplicate(true);
			        } 
		        }
		        
		        // Feed is optional 
		        if (!Validator.validateString(blogForm.getBlogFeed(),false,1,100,false)) {
		            errors.add("feed",new ActionError("common.invalid.bloglink.feed"));
		        }
		        else
		        {
			        //Check already exist in db
			        UserBlog userBlog = blogBO.getFeedWithUrl(blogForm.getBlogFeed());
			        if (userBlog != null && userBlog.getUserId()!= blogForm.getUserId())
			        {
			            errors.add("feed",new ActionError("common.exists.bloglink.feed"));   
			        }
			        
		        	// Feed is ok, so we pull them
			        RSSPuller rssPuller = new RSSPuller();
			        try {
						ArrayList postList = rssPuller.getRssFeedEntries(new URL(blogForm.getBlogFeed()));
						if (postList == null || postList.isEmpty())
						{
							errors.add("feed",new ActionError("common.invalid.bloglink.feedNoPost"));
						}
						else
						{
							blogForm.setPostList(postList);
						}
					} catch (MalformedURLException e) {  
			            errors.add("feed",new ActionError("common.invalid.bloglink.feedFail"));
					} 
			        
		        }  

		        blogBO = null;
		        return errors;
		    }
		    
		  
}
