/*
 * SeasonGreetingValidator.java
 *
 * Created on November 14, 2007, 3:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.SeasonGreeting;
import com.esmart2u.oeight.member.bo.SeasonGreetingBO;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 *
 * @author meauchyuan.lee
 */
public class SeasonGreetingValidator {
    
    private static Logger logger = Logger.getLogger(SeasonGreetingValidator.class);
    
    /** Creates a new instance of SeasonGreetingValidator */
    public SeasonGreetingValidator() {
    }
    
    public static ActionErrors validateSend(ActionErrors errors, SeasonGreetingForm greetingForm) {
        
        if (!Validator.validateString(greetingForm.getSenderName(),true,1,50,false)) {
            errors.add("senderName",new ActionError("common.invalid.senderName"));
        }
        if (!Validator.validateEmail(greetingForm.getSenderEmail(),true)) {
            errors.add("senderEmail",new ActionError("common.invalid.senderEmail"));
        }
        if (!Validator.validateString(greetingForm.getReceiverName(),true,1,50,false)) {
            errors.add("receiverName",new ActionError("common.invalid.receiverName"));
        }
        if (!Validator.validateEmail(greetingForm.getReceiverEmail(),true)) {
            errors.add("receiverEmail",new ActionError("common.invalid.receiverEmail"));
        }
        if (!Validator.validateString(greetingForm.getMessage(),false,0,200,false)) {
            errors.add("greetingMessage",new ActionError("common.invalid.greetingMessage"));
        }
        
        return errors;
    }
    
    public static ActionErrors validateRetrieval(ActionErrors errors, SeasonGreetingForm greetingForm) {
        if (!Validator.validateEmail(greetingForm.getSenderEmail(),true)) {
            errors.add("greetingLink",new ActionError("common.invalid.greetingLink"));
        } else if (!Validator.validateString(greetingForm.getGreetingCode(),true,1,50)) {
            errors.add("greetingLink",new ActionError("common.invalid.greetingLink"));
        }
        
        if (errors.size() < 1) {
            SeasonGreeting seasonGreeting = new SeasonGreeting();
            seasonGreeting.setSenderEmail(greetingForm.getSenderEmail());
            seasonGreeting.setGreetingCode(greetingForm.getGreetingCode());
            SeasonGreetingBO seasonGreetingBO = new SeasonGreetingBO();
            
            SeasonGreeting dbGreeting = seasonGreetingBO.getGreeting(seasonGreeting);
            if (dbGreeting == null || dbGreeting.getGreetingId() <1) {
                errors.add("greetingLink",new ActionError("common.invalid.greetingLink"));
            }
            else
            {
                greetingForm.setGreetingId(dbGreeting.getGreetingId());
                greetingForm.setSenderEmail(dbGreeting.getSenderEmail());
                greetingForm.setSenderName(dbGreeting.getSenderName());
                greetingForm.setReceiverEmail(dbGreeting.getReceiverEmail());
                greetingForm.setReceiverName(dbGreeting.getReceiverName());
                greetingForm.setMessage(dbGreeting.getMessage());
                greetingForm.setT(dbGreeting.getType());
            }
            seasonGreetingBO = null;
        }
        
        return errors;
    }
    
    
}
