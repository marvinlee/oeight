/*
 * FeedbackHelper.java
 *
 * Created on January 14, 2008, 11:34 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.solution.base.helper.StringUtils;
import java.util.Vector;

/**
 *
 * @author meauchyuan.lee
 */
public class FeedbackHelper {
        
    private static String[] subjectCodes = new String[]{"Tech","Suggest","Abuse","Bug","Others"};
    private static String[] subjectLabels = new String[]{"Technical Support","Suggestion","Report Abuse","Application Bug","Others"};
    private static Vector subjectList;
    private static Vector subjectCodesVector;
    
    /** Creates a new instance of FeedbackHelper */
    public FeedbackHelper() {
    }
    
    public static Vector getSubjectList(){
        if (subjectList == null || subjectList.size() <1)
        {
            subjectList = getSubjectCombo(); 
        } 
        return subjectList;
    }
    
    private static Vector getSubjectCombo()
    {
        Vector subject = new Vector();
        subjectCodesVector = new Vector();
        for(int i=0;i<subjectCodes.length;i++){
            String[] value = new String[2];
            value[0] = subjectCodes[i];
            value[1] = subjectLabels[i];
            subject.add(value);
            subjectCodesVector.add(subjectCodes[i]);
        }
        return subject;
    }
    
    public static boolean isAcceptedSubjectCode(String subjectCode)
    {
        if (StringUtils.hasValue(subjectCode) && subjectCodesVector.contains(subjectCode))
            return true;
        else
            return false;
    }
    
    public static String getSubjectLabelByCode(String subjectCode)
    {
        for(int i=0;i<subjectCodes.length;i++){ 
            if (subjectCode.equals(subjectCodes[i]))
            {
                return subjectLabels[i];
            }
        }
        return null;
    }
}