/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ScreenActionConstants.java

package com.esmart2u.oeight.member.web.struts.helper;

/**
 * Screen Actions for DCMS Application.
 *
 * @author  Lee Meau Chyuan
 * @version $Revision: 1.10.2.9 $
 */

public interface ScreenActionConstants extends com.esmart2u.solution.web.struts.helper.BaseScreenActionConstants
{
    public final static String DCMS_CONSUMER_TAB = "DCMS_CONSUMER";
    public final static String DCMS_CONSUMER_TNC_TAB = "DCMS_CONSUMER_TCA";
    public final static String DCMS_CONSUMER_COT_TAB = "DCMS_CONSUMER_COT";
    public final static String DCMS_CONSUMER_RELATIONSHIP_RELATIONSHIP_TAB = "DCMS_CONSUMER_REL";
    public final static String DCMS_CONSUMER_RELATIONSHIP_RELATED_TAB = "CUSTOMER_RELATED";
    // Application Tabs
    public final static String DCMS_CONSUMER_APP_INFO   = "tab_dcms_0001";
    public final static String DCMS_CONSUMER_RELATION   = "tab_dcms_0002";
    public final static String DCMS_CONSUMER_FACILITY   = "tab_dcms_0003";
    public final static String DCMS_CONSUMER_COLLATERAL = "tab_dcms_0004";
    public final static String DCMS_CONSUMER_CONDITION  = "tab_dcms_0005";
    public final static String DCMS_CONSUMER_SUP_DOC    = "tab_dcms_0006";
    public final static String DCMS_CONSUMER_CHECKLIST  = "tab_dcms_0007";
    public final static String DCMS_CONSUMER_ACTIVITY   = "tab_dcms_0008";
    public final static String DCMS_CONSUMER_COMMENT    = "tab_dcms_0009";
    public final static String DCMS_CONSUMER_SOL_INFO   = "tab_dcms_0010";
    public final static String DCMS_CONSUMER_DOC_GEN    = "tab_dcms_0011";
    public final static String DCMS_CONSUMER_SEC_DOC    = "tab_dcms_0012";
    public final static String DCMS_CONSUMER_WORKFLOW   = "tab_dcms_0013";

    // Relationship Tabs
    public final static String DCMS_CONSUMER_RELATIONSHIP_SUMMARY   = "tab_dcms_0014";
    public final static String DCMS_CONSUMER_CUSTOMER_RELATED   = "tab_dcms_0015";
    public final static String DCMS_CONSUMER_RELATED   = "tab_dcms_0016";
    public final static String DCMS_CONSUMER_SHAREHOLDERS   = "tab_dcms_0017";
    public final static String DCMS_CONSUMER_RELATIONSHIP_INTERNAL_EXPLOSURE   = "tab_dcms_0018";
    public final static String DCMS_CONSUMER_RELATIONSHIP_EXTERNAL_EXPLOSURE   = "tab_dcms_0019";
    public final static String DCMS_CONSUMER_RELATIONSHIP_CREDIT_CHECK   = "tab_dcms_0024";

    // T&C Tabs
    public final static String DCMS_TNC_CONSUMER_FACILITY   = "tab_dcms_0020";
    public final static String DCMS_TNC_CONSUMER_COLLATERAL   = "tab_dcms_0021";
    public final static String DCMS_TNC_CONSUMER_CONDITION   = "tab_dcms_0022";

    // Application Worksheet Tab
    public final static String DCMS_CONSUMER_WORKSHEET   = "tab_dcms_0023";

    public static final String PREVIOUS_ACTION_CODE = "PREVIOUS";
    public static final String NEXT_ACTION_CODE = "NEXT";
    public static final String SCANNING_REQ_ACTION_CODE = "SCANNING_REQ";
    public static final String WAIVE_ACTION_CODE = "WAIVE";
    public static final String VIEW_ACTION_CODE = "VIEW";
    public static final String RECEIVE_ACTION_CODE = "RECEIVE";
    public static final String VERIFY_ACTION_CODE = "VERIFY";
    public static final String EXECUTED_ACTION_CODE = "EXECUTED";
    public static final String PRESENTED_ACTION_CODE = "PRESENTED";
    public static final String STAMPED_ACTION_CODE = "STAMPED";
    public static final String SAFECUSTODY_ACTION_CODE = "SAFECUSTODY";
    public static final String VIEW_SUMMARY_ACTION_CODE = "VIEW_APP_SUMMARY";
    public static final String VIEW_REMARK_ACTION_CODE = "VIEW_REMARK";
    public static final String VIEW_SUMMARY_COT_ACTION_CODE = "VIEW_APP_SUMMARY_COT";
    public static final String VIEW_SUMMARY_TNC_ACTION_CODE = "VIEW_APP_SUMMARY_TNC";
    public static final String VIEW_COT_SUMMARY_ACTION_CODE = "VIEW_COT_APP_SUMMARY";


    public static final String PICK_LIST = "PICK_LIST";

    //screen action for credit scoring
    public static final String VIEW = "VIEW";
    public static final String CALCULATE = "CALCULATE";

    //screen action for solicitor
    public static final String ASSIGN = "ASSIGN";

    //screen action for collateral property
    public static final String DUPLICATE_ACTION_CODE = "DUPLICATE";
    public static final String MORE_DETAILS_ACTION_CODE = "MOREDETAIL";

    // screen action for facility
    public static final String CALCULATE_ACTION_CODE = "CALCULATE";

    // Application Tabs
    public final static String MNU_DCMS_00003_APP_INFO = "APP_INFO";
    public final static String MNU_DCMS_00003_RELATION = "RELATION";
    public final static String MNU_DCMS_00003_FACILITY = "FACILITY";
    public final static String MNU_DCMS_00003_COLLATERAL = "COLLATERAL";
    public final static String MNU_DCMS_00003_CONDITION = "CONDITION";
    public final static String MNU_DCMS_00003_SUP_DOC = "SUP_DOC";
    public final static String MNU_DCMS_00003_CHECKLIST = "CHECKLIST";
    public final static String MNU_DCMS_00003_ACTIVITY = "ACTIVITY";
    public final static String MNU_DCMS_00003_COMMENT = "COMMENT";
    public final static String MNU_DCMS_00003_SOL_INFO = "SOL_INFO";
    public final static String MNU_DCMS_00003_DOC_GEN = "DOC_GEN";
    public final static String MNU_DCMS_00003_SEC_DOC = "SEC_DOC";
    public final static String MNU_DCMS_00003_WORKFLOW = "WORKFLOW";

    public static final String MNU_DCMS_00001_SIMPLIFIED_DATA_ENTRY = "simplified";
    public static final String MNU_DCMS_00001_GENERAL = "general";
    public static final String MNU_DCMS_00001_RESIDENCE = "residence";
    public static final String MNU_DCMS_00001_EMPLOYEMENT = "employment";
    public static final String MNU_DCMS_00001_FINANCIAL = "financial";

    //Tabs for consumer loan application collateral which is type of property
    public static final String MNU_DCMS_00029_PR_SIMPLIFIED_DATA_ENTRY = "SIMPLIFIED_DATA_ENTRY";
    public static final String MNU_DCMS_00029_PR_GENERAL = "GENERAL";
    public static final String MNU_DCMS_00029_PR_BUILDING = "BUILDING";
    public static final String MNU_DCMS_00029_PR_LAND = "LAND";

    public static final String DCMS_CONSUMER_COLLATERAL_SUMMARY = "SUMMARY";
    public static final String DCMS_CONSUMER_COLLATERAL_APPRAISAL = "APPRAISAL";
    public static final String DCMS_CONSUMER_COLLATERAL_INSURANCE = "INSURANCE";
    public static final String DCMS_CONSUMER_COLLATERAL_OWNER = "OWNER";

    public static final String DOCUMENT_RECEIVING = "DOCUMENT RECEIVING";
    public static final String DOCUMENT_VERIFICATION = "DOCUMENT VERIFICATION";

    //Tabs for Safe Custody
    public static final String SAFE_CUSTODY_SUMMARY = "safe_custody_summary";
    public static final String SAFE_CUSTODY_INCOMING = "safe_custody_incoming";
    public static final String SAFE_CUSTODY_AVAILABLE = "safe_custody_available";
    public static final String SAFE_CUSTODY_CHECKED_OUT = "safe_custody_checked_out";
    public static final String SAFE_CUSTODY_REDEEMED = "safe_custody_redeemed";

    public static final String DOWNLOAD_ACTION_CODE = "DOWNLOAD";
    public static final String CREATE_CHANGE_REQUEST = "CREATE_CHANGE_REQ";

    // Service Request log - Save button
    public static final String SAVE_ACTION_CODE = "SAVE";

    public static final String DCMS_CS_WORKSHEET_SERVICES_REQUEST_RECOMMENDATION_BRANCH = "04";
    public static final String DCMS_CS_WORKSHEET_SERVICES_REQUEST_APPROVAL_COMMITTEE = "05";
    public static final String DCMS_CS_WORKSHEET_SERVICES_REQUEST_APPROVAL_CONCLUSION = "06";


}

// end of ScreenActionConstants.java