/*
 * EarthValidator.java
 *
 * Created on May 12, 2008, 4:00 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 *
 * @author meauchyuan.lee
 */
public class EarthValidator {
    
    private static Logger logger = Logger.getLogger(EarthValidator.class);
    
    /** Creates a new instance of EarthValidator */
    public EarthValidator() {
    }

    static ActionErrors validatePledge(ActionErrors errors, EarthForm earthForm) { 
        return errors; 
    }

    static ActionErrors validatePledgeSubmitted(ActionErrors errors, EarthForm earthForm) {
        if (!Validator.validateString(earthForm.getPledgerName(),true,1,100,false)) {
            errors.add("name",new ActionError("common.invalid.earthcampaign.name"));
        }
        if (!Validator.validateInteger(Integer.toString(earthForm.getPledgeCode()),true,1,999)) {
            errors.add("pledgeCode",new ActionError("common.invalid.earthcampaign.pledgeCode"));
        }
        if (!Validator.validateInteger(Integer.toString(earthForm.getPledgeFrom()),false,0,2)) {
            errors.add("pledgeFrom",new ActionError("common.invalid.earthcampaign.pledgeFrom"));
        }
        if (earthForm.getPledgeFrom() > 0 && earthForm.getPledgeFrom() < 3)
        {
             if (!Validator.validateInteger(Long.toString(earthForm.getPledgeFromUserId()),true,1,Integer.MAX_VALUE)) {
                errors.add("pledgeFromUserId",new ActionError("common.invalid.earthcampaign.pledgeFromUserId"));
            }
        
        }
        
        
        return errors; 
    }

    static ActionErrors validateBlogSubmitted(ActionErrors errors, EarthForm earthForm) {
        if (!Validator.validateString(earthForm.getBlogUrl(),true,1,500,false)) {
            errors.add("blogUrl",new ActionError("common.invalid.earthcampaign.blogUrl"));
        } 
        if (!Validator.validateString(earthForm.getName(),true,1,100,false)) {
            errors.add("name",new ActionError("common.invalid.earthcampaign.name"));
        } 
        if (!Validator.validateString(earthForm.getContactNo(),true,1,20,true)) {
            errors.add("contactNo",new ActionError("common.invalid.earthcampaign.contactNo"));
        }
        if (!Validator.validateEmail(earthForm.getEmail(),true)) {
            errors.add("email",new ActionError("common.invalid.earthcampaign.email"));
        } 
        return errors;
    }
    
}
