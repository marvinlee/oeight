/*
 * HibernatePage.java
 *
 * Created on October 16, 2007, 2:06 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;


import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Criteria;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
 
public class HibernatePage implements Page {

  protected Criteria criteria;

  protected List elements;
  protected int pageSize;
  protected int pageNumber;
  protected int totalElements = -1;

  public static Set jdbcClassesSupportingScrollCursors = new HashSet();
  private ScrollableResults scrollableResults;
  public static final int DEFAULT_PAGE_SIZE = 10 ;

  private HibernatePage(int pageNumber, int pageSize) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
  }


  public boolean isFirstPage() {
    return getPageNumber() == 0;
  }

  public boolean isLastPage() {
    return getPageNumber() >= getLastPageNumber();
  }

  public boolean hasNextPage() {
    return !isLastPage();
  }

  public boolean hasPreviousPage() {
    return getPageNumber() > 0;
  }

  public int getLastPageNumber() {

    double totalResults = new Integer(getTotalNumberOfElements()).doubleValue();
    return new Double(Math.floor(totalResults / getPageSize())).intValue();
  }

  public List getThisPageElements() {
    return elements;
  }

  public Logger getLogger() {
     //WARN: THIS CODE MUST BE REPLACED
    return Logger.getLogger("platformLogger");
  }

  public int getTotalNumberOfElements() {
    return totalElements;
  }

  public int getThisPageFirstElementNumber() {
    return getPageNumber() * getPageSize() + 1;
  }

  public int getThisPageLastElementNumber() {
    int fullPage = getThisPageFirstElementNumber() + getPageSize() - 1;
    return getTotalNumberOfElements() < fullPage ?
            getTotalNumberOfElements() :
            fullPage;
  }

  public int getNextPageNumber() {
    return getPageNumber() + 1;
  }

  public int getPreviousPageNumber() {
    return getPageNumber() - 1;
  }

  public int getPageSize() {
    return pageSize;
  }

  public int getPageNumber() {
    return pageNumber;
  }

  public List getAllElements() {
    HibernatePage pageTmp = getHibernatePageInstance(criteria,1,getTotalNumberOfElements());
    return pageTmp.getThisPageElements();
  }


  /*public static HibernatePage getHibernatePageInstance(Criteria criteria, int pageNumber, int pageSize) {

    return getHibernatePageInstance(criteria, pageNumber, pageSize);
  }*/

  public static HibernatePage getHibernatePageInstance(Criteria criteria,
                                                       int pageNumber,
                                                       int pageSize) {

    //if (criteria.getCriteriaString().toLowerCase().indexOf("order by")==-1) {
     // WARN IN SOME WAY: warn("Using pagination without order by can lead to inconsistent results, for example on certain Oracle instances: "+criteria.getCriteriaString());
    //}

    //if (jdbcClassesSupportingScrollCursors.contains(driverClass))
      return HibernatePage.getScrollPageInstanceWithTotalByScroll(criteria, pageNumber, pageSize);
    //else
    //  return HibernatePage.getScrollPageInstanceWithTotalByList(criteria, pageNumber, pageSize);
  }

  /**
   * Construct a new HibernatePage. HibernatePage numbers are zero-based so the
   * first page is page 0.
   *
   * @param criteria      the Hibernate Criteria
   * @param pageNumber the page number (zero-based);
   *                   if Integer.MAX_VALUE will return the last page for the criteria
   * @param pageSize   the number of results to display on the page
   */
  protected static HibernatePage getScrollPageInstanceWithTotalByScroll(Criteria criteria, int pageNumber, int pageSize) {

    HibernatePage sp = new HibernatePage(pageNumber, pageSize);
    sp.criteria = criteria;
    try {
      sp.scrollableResults = criteria.scroll(ScrollMode.SCROLL_SENSITIVE);
      sp.scrollableResults.last();
      sp.totalElements = sp.scrollableResults.getRowNumber();

      sp.fixThisPageElements();
      sp.scrollableResults.close();
    } catch (HibernateException e) {
      sp.getLogger().error("Failed to create ScrollPage by getScrollPageInstanceWithTotalByScroll: " + e.getMessage());
      throw new RuntimeException(e);
    }

    return sp;
  }


  /**
   * Construct a new HibernatePage. HibernatePage numbers are zero-based so the
   * first page is page 0.
   *
   * @param criteria      the Hibernate Criteria
   * @param pageNumber the page number (zero-based);
   *                   if Integer.MAX_VALUE will return the last page for the criteria
   * @param pageSize   the number of results to display on the page
   */
  protected static HibernatePage getScrollPageInstanceWithTotalByList(Criteria criteria, int pageNumber, int pageSize) {

    HibernatePage sp = new HibernatePage(pageNumber, pageSize);
    sp.criteria = criteria;
    try {
      //bacame useless
      //sp.scrollableResults = criteria.scroll(ScrollMode.FORWARD_ONLY);
      sp.totalElements = sp.calculateTotalElementsByList();
      sp.fixThisPageElements();

    } catch (HibernateException e) {
      sp.getLogger().error("Failed to create ScrollPage by getScrollPageInstanceWithTotalByCriteria: " + e.getMessage());
      throw new RuntimeException(e);
    }

    return sp;
  }

  private void fixThisPageElements() throws HibernateException {

    if (this.pageSize<=0)
      this.pageSize=HibernatePage.DEFAULT_PAGE_SIZE;

    if (Integer.MAX_VALUE == this.pageNumber)
      this.pageNumber = (getTotalNumberOfElements() / this.pageSize);
    else if (pageNumber>(totalElements/pageSize))
      pageNumber = totalElements/pageSize;
    criteria = criteria.setFirstResult(this.pageNumber * this.pageSize);
    criteria = criteria.setMaxResults(this.pageSize);
    elements = criteria.list();
  }

  private int calculateTotalElementsByList() throws HibernateException {

    return criteria.list().size()-1;

  }
}
