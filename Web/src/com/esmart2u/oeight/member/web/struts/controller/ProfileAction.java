/*
 * ProfileAction.java
 *
 * Created on October 4, 2007, 11:01 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.RankingSnapshot;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.UserDetails;
import com.esmart2u.oeight.data.UserPictures;
import com.esmart2u.oeight.member.bo.RankingBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserFacebookBO;
import com.esmart2u.oeight.member.bo.UserFriendsterBO;
import com.esmart2u.oeight.member.bo.UserVotesBO;
import com.esmart2u.oeight.member.bo.UserWishesBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.WidgetHelper;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.PhotoFile;
import com.esmart2u.solution.base.helper.PhotoUploadHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class ProfileAction extends AbstractApplicationAction {
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(ProfileAction.class);
    
    private static String VIEW_PROFILE = "view";
    private static String MAIN = "mainEdit";
    private static String MAIN_DISPLAY = "mainSubmitted";
    private static String PHOTO = "photoEdit";
    private static String PHOTO_DISPLAY = "photoSubmitted";
    private static String LIFESTYLE = "lifestyleEdit";
    private static String LIFESTYLE_DISPLAY = "lifestyleSubmitted";
    private static String INTEREST = "interestEdit";
    private static String INTEREST_DISPLAY = "interestSubmitted";
    private static String SKILLS = "skillsEdit";
    private static String SKILLS_DISPLAY = "skillsSubmitted";
    private static String CONTACT = "contactEdit";
    private static String CONTACT_DISPLAY = "contactSubmitted";
    private static String FLICKR = "flickrEdit";
    private static String FLICKR_DISPLAY = "flickrSubmitted";
    private static String LOGIN = "login";
    private static String RELOGIN = "relogin";
    private static String NOT_FOUND = "notFound";
    private static String PROFILE_REMOVED = "removed";
    private static String VOTE = "vote";
    private static String REPORT = "report";
    private static String WIDGET = "widget";
    private static String NOT_VALIDATED = "notValidated";
    
    
    public String doView(ProfileForm actionForm)
    throws ApplicationException {
        try {
            logger.debug("View Profile page");
            
            // Get User Id from session and get details
            // Probably allow user to view one or two profiles first
            String userId = checkUserAccess(actionForm); 
            String loginUserId = userId;
                    
            UserBO userBO = new UserBO();
            User user = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
                
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }
                else
                {
                    UserCountry userCountry = user.getUserCountry();
                    if (OEightConstants.REPORT_ABUSE_CONFIRMED == userCountry.getReportAbuse())
                        return PROFILE_REMOVED;
                }
                    
                 
                // increase page view counter only if not own profile
                if (!userId.equals(Long.toString(user.getUserId())) && !actionForm.isFromReportAbuse()){ 
                    long pageView = RankingBO.increasePageView(Long.toString(user.getUserId()));
                    //actionForm.setPageViewCount(pageView);
                }    
                
                userId = Long.toString(user.getUserId());
                        
            } else {
                // Get user main details
                user = userBO.getUserById(userId);
                
            }
            
            // A key to identify is viewing own profile page
            if (loginUserId.equals(userId))
            {
                actionForm.setViewingOwnProfile(true);
            }
            else
            { 
                User loginUser = userBO.getUserById(loginUserId);
                
                // Latest UPDATE : allow viewing even if not validated, sending wish/message needs validation
                // If email not validated, return not validated screen
                /*if (PropertyConstants.BOOLEAN_YES != loginUser.getUserCountry().getValidated())
                {
                    actionForm.setViewerUserEmail(loginUser.getEmailAddress());
                    return NOT_VALIDATED;
                } */
                
                
                boolean isFriend = UserWishesBO.isFriend(loginUserId,userId);
                actionForm.setInBuddyList(isFriend);
            }
            
            UserDetails userDetail = userBO.getUserDetailsById(userId);
            
            RankingSnapshot rankingSnapshot = RankingBO.getUserDetails(userId);
            actionForm.setPageViewCount(rankingSnapshot.getPageView());
            actionForm.setVotesCount(rankingSnapshot.getVotes());  
            
            //todo Consider joining all the queries together, probably details and photos
            actionForm.setUserProfileAll(userDetail);
            actionForm.setUserProfileMain(user);
            User photoUser = userBO.getUserPhotosById(user);
            actionForm.setUserProfilePhotos(photoUser);
            if (!actionForm.isFromVoting())
            {
                actionForm.setVotesLeft(UserVotesBO.getVotesLeft(loginUserId));
            }
            
            boolean hasFriendster = UserFriendsterBO.hasFriendsterXChange(userId);
            actionForm.setHasFriendsterFriends(hasFriendster);
            
            boolean hasFacebook = UserFacebookBO.hasFacebookXChange(userId);
            actionForm.setHasFacebookFriends(hasFacebook);
            
            userBO = null;
            
            return VIEW_PROFILE;
        } catch (Exception e) {
            //return rethrow(e, LOGIN);
            return RELOGIN;
        }
    }
    
    public String doMainEdit(ProfileForm actionForm)
    throws ApplicationException {
        try {
            logger.debug("Main Profile page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_YES));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            if (!hasErrors(actionForm)){
                // Get user mail details
                UserBO userBO = new UserBO();
                User user = userBO.getUserById(userId);
                actionForm.setUserProfileMain(user);
                userBO = null;
            }
            
            return MAIN;
        } catch (Exception e) { 
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    public String doMainSubmitted(ProfileForm actionForm)
    throws ApplicationException {
        
        try {
            logger.debug("Main Profile page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_NO));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Get user main details
            User editedUser = actionForm.getUserProfileMain();
            logger.debug("User DOB =" + editedUser.getUserCountry().getBirthDate());
            UserBO userBO = new UserBO();
            User user = userBO.saveUserProfileMain(userId, editedUser);
            actionForm.setUserProfileMain(user);
            userBO = null;
            
            return MAIN_DISPLAY;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    public String doPhotoEdit(ProfileForm actionForm)
    throws ApplicationException {
        try {
            logger.debug("Profile Photo page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_YES));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            if (!hasErrors(actionForm)){
                // Get user photo details
                UserBO userBO = new UserBO();
                logger.debug("get photos");
                User user = userBO.getUserPhotosById(userId);
                logger.debug("set Photos");
                actionForm.setUserName(user.getUserName());
                actionForm.setUserProfilePhotos(user);
                userBO = null;
            }
            
            return PHOTO;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    public String doPhotoSubmitted(ProfileForm actionForm)
    throws ApplicationException {
        
        try {
            logger.debug("Profile Photo page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_NO));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Photo is validated, upload and save photo to server, link id to the user
            try{
                
                UserBO userBO = new UserBO(); 
                User user = userBO.getUserById(userId); 
                String userName = user.getUserName();
                actionForm.setUserName(userName);
                
                
                // Update Mosaic file if updated, set moderated=N
                if (actionForm.getPhotoFile().getFileSize() > 0 ) {
                    PhotoFile photoFile = PhotoUploadHelper.
                            uploadMosaicPhoto(actionForm.getPhotoFile(), userName);
                    //actionForm.setPhotoPath(photoFile.getFilePath());
                    actionForm.setPhotoLargePath(photoFile.getFileName());
                    actionForm.setPhotoSmallPath(photoFile.getThumbnailFileName());
                }
                
                // Do persistence
                // Get user main details
                User editedUser = actionForm.getUserProfileMain(); 
                // Only save the user photo description
                user = userBO.saveUserProfileMainPhoto(userId, editedUser);
                actionForm.setUserProfileMain(user);
                
                
                List photoList = new ArrayList();
                // Update other photos if updated, set moderated=N
                UserPictures profilePhoto = new UserPictures();
                profilePhoto.setUserId(Long.parseLong(userId));  
                profilePhoto.setPhotoType(OEightConstants.PHOTO_TYPE_PROFILE);
                profilePhoto.setCaption(actionForm.getProfilePhotoDescription());
                if (actionForm.getProfilePhotoFile().getFileSize() > 0 ) {
                    PhotoFile profilePhotoFile = PhotoUploadHelper.
                            uploadProfilePhoto(actionForm.getProfilePhotoFile(), userName, OEightConstants.PHOTO_TYPE_PROFILE);
                    profilePhoto.setPhotoLarge(profilePhotoFile.getFileName());
                    profilePhoto.setDateSubmitted(new Date(System.currentTimeMillis()));
                    profilePhoto.setModerated(PropertyConstants.BOOLEAN_NO);
                    //logger.debug("Done upload profile photo");
                }
                //logger.debug("Adding profile photo to list");
                photoList.add(profilePhoto);
                
                /*UserPictures shirtPhoto = new UserPictures();
                shirtPhoto.setUserId(Long.parseLong(userId));
                shirtPhoto.setPhotoType(OEightConstants.PHOTO_TYPE_SHIRT);
                shirtPhoto.setCaption(actionForm.getShirtPhotoDescription());
                if (actionForm.getShirtPhotoFile().getFileSize() > 0 ) {
                    PhotoFile shirtPhotoFile = PhotoUploadHelper.
                            uploadProfilePhoto(actionForm.getShirtPhotoFile(), actionForm.getUserName(), OEightConstants.PHOTO_TYPE_SHIRT);
                    shirtPhoto.setPhotoLarge(shirtPhotoFile.getFileName());
                    shirtPhoto.setDateSubmitted(new Date(System.currentTimeMillis()));
                    shirtPhoto.setModerated(PropertyConstants.BOOLEAN_NO); 
                    logger.debug("Done upload tshirt photo");
                }
                photoList.add(shirtPhoto);*/
                
                // Persists photoList
                photoList = userBO.saveUserProfilePhotosList(userId, photoList);
                //logger.debug("set Photos");
                user.setPhotoList(photoList);
                actionForm.setUserProfilePhotos(user); 
                
                userBO = null;
                
            }catch (Exception e) {
                // Add error message
            }
            
            return PHOTO_DISPLAY;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    
    public String doLifestyleEdit(ProfileForm actionForm)
    throws ApplicationException {
        try {
            logger.debug("Lifestyle edit page");
            actionForm.setUserName("test only");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_YES));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            if (!hasErrors(actionForm)){
                // Get user main details
                UserBO userBO = new UserBO();
                UserDetails userDetail = userBO.getUserLifestyleById(userId);
                actionForm.setUserProfileLifestyle(userDetail);
                userBO = null;
            }
            
            return LIFESTYLE;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    public String doLifestyleSubmitted(ProfileForm actionForm)
    throws ApplicationException {
        try {
            logger.debug("Lifestyle edit submitted page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_NO));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Get user mail details
            UserDetails editedUserDetails = actionForm.getUserProfileLifestyle();
            
            UserBO userBO = new UserBO();
            UserDetails userDetails = userBO.saveUserProfileLifestyle(userId, editedUserDetails);
            actionForm.setUserProfileLifestyle(userDetails);
            userBO = null;
            
            return LIFESTYLE_DISPLAY;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
        
    }
    
    
    public String doInterestEdit(ProfileForm actionForm)
    throws ApplicationException {
        
        try {
            logger.debug("Interests edit page");
            actionForm.setUserName("test only");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_YES));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            if (!hasErrors(actionForm)){
                // Get user mail details
                UserBO userBO = new UserBO();
                UserDetails userDetail = userBO.getUserInterestById(userId);
                actionForm.setUserProfileInterest(userDetail);
                userBO = null;
            }
            
            return INTEREST;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
        
    }
    
    public String doInterestSubmitted(ProfileForm actionForm)
    throws ApplicationException {
        
        try {
            logger.debug("Interests edit submitted page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_NO));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Get user mail details
            UserDetails editedUserDetails = actionForm.getUserProfileInterest();
            
            UserBO userBO = new UserBO();
            UserDetails userDetails = userBO.saveUserProfileInterest(userId, editedUserDetails);
            actionForm.setUserProfileInterest(userDetails);
            userBO = null;
            
            return INTEREST_DISPLAY;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
        
    }
    
    public String doSkillsEdit(ProfileForm actionForm)
    throws ApplicationException {
        
        
        try {
            logger.debug("Skills edit page");
            actionForm.setUserName("test only");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_YES));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            if (!hasErrors(actionForm)){
                // Get user mail details
                UserBO userBO = new UserBO();
                UserDetails userDetail = userBO.getUserSkillsById(userId);
                actionForm.setUserProfileSkills(userDetail);
                userBO = null;
            }
            
            return SKILLS;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
        
    }
    
    public String doSkillsSubmitted(ProfileForm actionForm)
    throws ApplicationException {
        
        try {
            logger.debug("Skills edit submitted page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_NO));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Get user mail details
            UserDetails editedUserDetails = actionForm.getUserProfileSkills();
            
            UserBO userBO = new UserBO();
            UserDetails userDetails = userBO.saveUserProfileSkills(userId, editedUserDetails);
            actionForm.setUserProfileSkills(userDetails);
            userBO = null;
            
            return SKILLS_DISPLAY;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
        
    }
    
    public String doContactEdit(ProfileForm actionForm)
    throws ApplicationException {
        try {
            logger.debug("Contact edit page"); 
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_YES));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            
            if (!hasErrors(actionForm)){
                // Get user mail details
                UserBO userBO = new UserBO();
                UserDetails userDetail = userBO.getUserContactById(userId);
                actionForm.setUserProfileContact(userDetail);
                userBO = null;
            }
            
            return CONTACT;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    public String doContactSubmitted(ProfileForm actionForm)
    throws ApplicationException {
        
        
        try {
            logger.debug("Contact edit submitted page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_NO));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Get user mail details
            UserDetails editedUserDetails = actionForm.getUserProfileContact();
            
            UserBO userBO = new UserBO();
            UserDetails userDetails = userBO.saveUserProfileContact(userId, editedUserDetails);
            actionForm.setUserProfileContact(userDetails);
            
            return CONTACT_DISPLAY;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    public String doVote(ProfileForm actionForm)
    throws ApplicationException { 
       
        try {
            logger.debug("Voting page"); 
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            UserBO userBO = new UserBO();
            User user = null;
            
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null)
                    return NOT_FOUND;
                 
                
                if (!userId.equals(Long.toString(user.getUserId()))){// increase votes for others
                    int votesLeft = UserVotesBO.increaseVotes(userId, Long.toString(user.getUserId()));
                    actionForm.setVotesLeft(votesLeft);
                } 
                else
                { 
                    int votesLeft = UserVotesBO.increaseOwnVotes(userId);
                    actionForm.setVotesLeft(votesLeft);
                }
                
            } 
            userBO = null; 
            
            actionForm.setFromVoting(true);
            return doView(actionForm);
        } catch (Exception e) { 
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        } 
    }
    
    public String doReport(ProfileForm actionForm)
    throws ApplicationException { 
       
        try {
            logger.debug("Report abuse page"); 
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            UserBO userBO = new UserBO();
            User user = null;
            
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null)
                    return NOT_FOUND;
                 
                userBO.logReportedAbuseUser(user);
                
            } 
            userBO = null; 
            
            actionForm.setFromReportAbuse(true);
            return REPORT;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        } 
    }
    
    public String doFlickrEdit(ProfileForm actionForm)
    throws ApplicationException {
        try {
            logger.debug("Contact flickr page"); 
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_YES));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            
            if (!hasErrors(actionForm)){
                // Get user mail details
                UserBO userBO = new UserBO();
                User user = userBO.getUserById(userId);
                actionForm.setUserProfileMain(user);
                userBO = null;
            }
            // Do not display yahooId
            actionForm.setYahooId(null);
            
            return FLICKR;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    public String doFlickrSubmitted(ProfileForm actionForm)
    throws ApplicationException {
        
        
        try {
            logger.debug("Contact flickr submitted page");
            actionForm.setRequestEdit(Character.toString(PropertyConstants.BOOLEAN_NO));
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
             
             // FlickrId available 
            UserBO userBO = new UserBO();
            userBO.saveFlickerInfo(userId,actionForm.getYahooId(),actionForm.getFlickrId());
            userBO = null;
            
            return FLICKR_DISPLAY;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    
    public String doWidget(ProfileForm actionForm)
    throws ApplicationException { 
       
        try {
            logger.debug("Generate widget page"); 
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            UserBO userBO = new UserBO();
            User user = null;
            
            String widgetMainHTML = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                } 
                  
                
            }else {
                // Get user main details
                user = userBO.getUserById(userId);
                
            }
            
            widgetMainHTML = WidgetHelper.getMainWidget(user); 
            userBO = null; 
            
            actionForm.setId(user.getUserName());
            actionForm.setWidgetMainHTML(widgetMainHTML);
            return WIDGET;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        } 
    }
    
    protected boolean isValidationRequired(String event) {
        //boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (MAIN_DISPLAY.equals(event)||
                PHOTO_DISPLAY.equals(event)||
                LIFESTYLE_DISPLAY.equals(event)||
                INTEREST_DISPLAY.equals(event)||
                SKILLS_DISPLAY.equals(event)||
                CONTACT_DISPLAY.equals(event)||
                VOTE.equals(event)||
                FLICKR_DISPLAY.equals(event))
            return true;
        
        
        return true;
    }
    
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        ProfileForm profileForm = (ProfileForm)actionForm;
        
        logger.debug("Validate one=" + action);
        
        if (MAIN_DISPLAY.equals(action)) {
            errors = ProfileValidator.validateProfileMain(errors, profileForm);
        } else if (PHOTO_DISPLAY.equals(action)) {
            errors = ProfileValidator.validateProfilePhoto(errors, profileForm);
        } else if (LIFESTYLE_DISPLAY.equals(action)) {
            errors = ProfileValidator.validateProfileLifestyle(errors, profileForm);
        } else if (INTEREST_DISPLAY.equals(action)) {
            errors = ProfileValidator.validateProfileInterest(errors, profileForm);
        } else if (SKILLS_DISPLAY.equals(action)) {
            errors = ProfileValidator.validateProfileSkills(errors, profileForm);
        } else if (CONTACT_DISPLAY.equals(action)) {
            errors = ProfileValidator.validateProfileContact(errors, profileForm);
        } else if (VOTE.equals(action)) {
            errors = ProfileValidator.validateVote(errors, profileForm);
        } else if (FLICKR_DISPLAY.equals(action)) {
            errors = ProfileValidator.validateFlickr(errors, profileForm);
        } else {
            logger.debug("Temporary skip");
        }
        
        profileForm.getActionContext().setActionErrors(errors);
        return profileForm;
    }
    
    protected String getDefaultEvent(String action) {
        String previousAction = action;
        if (MAIN_DISPLAY.equals(action))
            action = MAIN;
        else if (PHOTO_DISPLAY.equals(action))
            action = PHOTO;
        else if (LIFESTYLE_DISPLAY.equals(action))
            action = LIFESTYLE;
        else if (INTEREST_DISPLAY.equals(action))
            action = INTEREST;
        else if (SKILLS_DISPLAY.equals(action))
            action = SKILLS;
        else if (CONTACT_DISPLAY.equals(action))
            action = CONTACT;
        else if (VOTE.equals(action))
            action = VIEW_PROFILE;
        else if (FLICKR_DISPLAY.equals(action))
            action = FLICKR;
        else {
            logger.debug("Temporary skip");
        }
        logger.debug("Going back to previous action from [" + previousAction + "] to [" + action + "] ");
        return action;
    }
  
}
