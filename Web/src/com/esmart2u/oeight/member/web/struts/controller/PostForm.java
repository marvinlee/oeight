/*
 * FeedbackForm.java
 *
 * Created on January 14, 2008, 8:37 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.esmart2u.oeight.member.helper.OEightConstants;

/**
 *
 * @author meauchyuan.lee
 */
public class PostForm  extends AbstractApplicationActionForm {
    
    private long userPostId;
    private long userId;
    private String url;
    private String title;
    private String description; 

	private String urlOk;
    private boolean urlDuplicate;
    private Date datePosted;
    private String datePostedString;
    private String postUserName;
    SimpleDateFormat sdf = new SimpleDateFormat(OEightConstants.DATE_FORMAT);
    
    
    /** Creates a new instance of FeedbackForm */
    public PostForm() {
    }
    
    public void clear()
    {
        this.setUserPostId(0);
        this.setUserId(0);
        this.setUrl(null);
        this.setTitle(null);
        this.setDescription(null); 
    }

    public long getUserPostId() {
        return userPostId;
    }

    public void setUserPostId(long userPostId) {
        this.userPostId = userPostId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlOk() {
        return urlOk;
    }

    public void setUrlOk(String urlOk) {
        this.urlOk = urlOk;
    }

    public boolean isUrlDuplicate() {
        return urlDuplicate;
    }

    public void setUrlDuplicate(boolean urlDuplicate) {
        this.urlDuplicate = urlDuplicate;
    }

	public Date getDatePosted() {
		return datePosted;
	}

	public void setDatePosted(Date datePosted) {
		datePostedString = sdf.format(datePosted);
		this.datePosted = datePosted;
	}

    public String getDatePostedString() {
		return datePostedString;
	}

	public void setDatePostedString(String datePostedString) {
		this.datePostedString = datePostedString;
	}

	public String getPostUserName() {
		return postUserName;
	}

	public void setPostUserName(String postUserName) {
		this.postUserName = postUserName;
	}
	
}
