/*
 * InviteForm.java
 *
 * Created on October 21, 2007, 11:25 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class InviteForm extends AbstractApplicationActionForm { 
    
    private long userId;
    private String emails;
    private List addressList; 
    private List historyList;
    private long totalInvitesSent;
    private long totalInvitesAccepted;
            
    // For contact list retriever
    private String emailUser;
    private String provider;
    private String password;
    
    // For list
    private List membersEmailList;
    private String[] membersUserNameString;
    private List nonMembersEmailList;
    private String[] nonMembersEmailString;    
    
    /** Creates a new instance of InviteForm */
    public InviteForm() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public List getAddressList() {
        return addressList;
    }

    public void setAddressList(List addressList) {
        this.addressList = addressList;
    }

    public List getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List historyList) {
        this.historyList = historyList;
    }

    public long getTotalInvitesSent() {
        return totalInvitesSent;
    }

    public void setTotalInvitesSent(long totalInvitesSent) {
        this.totalInvitesSent = totalInvitesSent;
    }

    public long getTotalInvitesAccepted() {
        return totalInvitesAccepted;
    }

    public void setTotalInvitesAccepted(long totalInvitesAccepted) {
        this.totalInvitesAccepted = totalInvitesAccepted;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String[] getMembersUserNameString() {
        return membersUserNameString;
    }

    public void setMembersUserNameString(String[] membersUserNameString) {
        this.membersUserNameString = membersUserNameString;
    }

    public String[] getNonMembersEmailString() {
        return nonMembersEmailString;
    }

    public void setNonMembersEmailString(String[] nonMembersEmailString) {
        this.nonMembersEmailString = nonMembersEmailString;
    }

    public List getMembersEmailList() {
        return membersEmailList;
    }

    public void setMembersEmailList(List membersEmailList) {
        this.membersEmailList = membersEmailList;
    }

    public List getNonMembersEmailList() {
        return nonMembersEmailList;
    }

    public void setNonMembersEmailList(List nonMembersEmailList) {
        this.nonMembersEmailList = nonMembersEmailList;
    }
    
}
