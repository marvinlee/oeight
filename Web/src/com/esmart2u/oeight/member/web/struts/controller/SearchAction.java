/*
 * SearchAction.java
 *
 * Created on October 14, 2007, 11:21 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.member.bo.SearchBO; 
import com.esmart2u.oeight.member.web.struts.helper.ListPage;
import com.esmart2u.oeight.member.web.struts.helper.Page;
import com.esmart2u.oeight.member.web.struts.helper.ProfileMapper;
import com.esmart2u.solution.base.helper.ApplicationException;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class SearchAction extends AbstractApplicationAction {
    
    private static String SEARCH_PAGE = "search";
    private static String RESULTS_PAGE = "searchResult"; 
    
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(SearchAction.class);
    
    public String doSearch(SearchForm actionForm)
    throws ApplicationException { 
        return SEARCH_PAGE;
    }
    
    public String doSearchResult(SearchForm searchForm)
    throws ApplicationException {
        try {
            logger.debug("Running doSearchResult");
            
            //todo Check session id, allow 3 searches only
            SearchBO searchBO = new SearchBO();
            UserCountry userCountry = ProfileMapper.mapUserSearchObject(searchForm);
            Page page = searchBO.getSearchUserResult(userCountry, searchForm.getCurrentPage());
            searchForm.setListPage(page);
            //List resultList = searchBO.getSearchUserResult(userCountry);
            //searchForm.setResultList(resultList);
            searchBO = null;
            
            return RESULTS_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, SEARCH_PAGE);
        }
    } 
    
    
}
