/*
 * FacebookAPIAction.java
 *
 * Created on April 2, 2008, 4:16 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.exception.FacebookException;
import com.esmart2u.oeight.member.bo.UserFacebookBO;
import com.esmart2u.oeight.member.vo.UserFacebookVO;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
/**
 *
 * @author meauchyuan.lee
 */
public class FacebookAPIAction extends AbstractApplicationAction {
      
      
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(FacebookAPIAction.class); 
    
    // Main - compact wish widget
    private static String INSTALL_1 = "install1";
    private static String CALLBACK_1 = "callback1"; 
    
    // Wish + Buddies widget
    private static String INSTALL_2 = "install2";
    private static String CALLBACK_2 = "callback2"; 
    
    // Featured blog
    private static String INSTALL_3 = "install3";
    private static String CALLBACK_3 = "callback3"; 
    
    // What say u?
    private static String INSTALL_4 = "install4";
    private static String CALLBACK_4 = "callback4"; 
    
    // oEight + Facebook Friends xChange
    private static String INSTALL_5 = "install5";
    private static String CALLBACK_5 = "callback5"; 
    
    // for Climate Change
    private static String INSTALL_6 = "install6";
    private static String CALLBACK_6 = "callback6"; 
    
      
    /** Creates a new instance of FacebookAPIAction */
    public FacebookAPIAction() {
    }
    
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (CALLBACK_1.equals(event) || CALLBACK_2.equals(event) || 
                CALLBACK_3.equals(event) || CALLBACK_4.equals(event) || 
                CALLBACK_5.equals(event) || CALLBACK_6.equals(event))
            return true; 
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        FacebookForm facebookForm = (FacebookForm)actionForm; 
        if (CALLBACK_1.equals(action)) {
            errors = FacebookAPIValidator.validateCallback1(errors, facebookForm); 
        } else if (CALLBACK_3.equals(action)) {
            errors = FacebookAPIValidator.validateCallback3(errors, facebookForm); 
        } else if (CALLBACK_5.equals(action)) {
            //errors = FacebookAPIValidator.validateCallback5(errors, facebookForm); 
        } else if (CALLBACK_6.equals(action)) {
            errors = FacebookAPIValidator.validateCallback6(errors, facebookForm); 
        } else {
            logger.debug("Temporary skip");
        }
        
        facebookForm.getActionContext().setActionErrors(errors);
        return facebookForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (CALLBACK_1.equals(action)) {
            action = INSTALL_1; 
        } 
        else if (CALLBACK_2.equals(action)) {
            action = INSTALL_2; 
        } 
        else if (CALLBACK_3.equals(action)) {
            action = INSTALL_3; 
        } 
        else if (CALLBACK_4.equals(action)) {
            action = INSTALL_4; 
        } 
        else if (CALLBACK_5.equals(action)) {
            action = INSTALL_5; 
        }
        else if (CALLBACK_6.equals(action)) {
            action = INSTALL_6; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doInstall1(FacebookForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return INSTALL_1;
        } catch (Exception e) {
            return INSTALL_1;
        }
    }
    
    
    public String doCallback1(FacebookForm actionForm)
    throws ApplicationException {
        try {
            
            UserFacebookBO userFacebookBO = new UserFacebookBO();
            UserFacebookVO userFacebookVO = actionForm.getUserFacebookVO();
            // Check userName availability, if exists, means validated and already installed
            if (StringUtils.hasValue(actionForm.getUserName()) 
                    && "n".equals(actionForm.getFirstInstall())
                    && !"y".equals(actionForm.getFromInstallPage())){
                System.out.println("Callback userName found for=" + actionForm.getUserName());
                logger.debug("Callback userName found for=" + actionForm.getUserName());
                userFacebookVO = userFacebookBO.updateCallbackViewsCounter(userFacebookVO, FacebookForm.MAIN_TYPE);
            }
            else{
                System.out.println("Username not found, is install time");
            // Else userName not found means now is the install time
            // Call facebook API and link with the login user id 
                userFacebookVO = userFacebookBO.newCallback1Install(userFacebookVO);
                if (StringUtils.hasValue(userFacebookVO.getUserName())){
                    actionForm.setUserName(userFacebookVO.getUserName());
                }
                else
                {
                    throw new Exception("Unable to get oEight User Name for facebook api Callback1 install");
                }
            }
            userFacebookBO = null;
            logger.debug("Callback ok for " + actionForm.getUserName());
            return CALLBACK_1;
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof FacebookException)
            {
                actionForm.getActionContext().getActionErrors().add("facebookInput",new ActionError("common.invalid.facebookAPI.connectionError"));
            }
            return INSTALL_1; 
        }
    }
     
    /**
     *  Featured Blog
     */
    public String doInstall3(FacebookForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return INSTALL_3;
        } catch (Exception e) {
            return INSTALL_3;
        }
    }
    
    
    public String doCallback3(FacebookForm actionForm)
    throws ApplicationException {
        try {
            
            UserFacebookBO userFacebookBO = new UserFacebookBO();
            UserFacebookVO userFacebookVO = actionForm.getUserFacebookVO();
            // Check userName availability, if exists, means validated and already installed
            if (StringUtils.hasValue(actionForm.getUserName()) 
                    && "n".equals(actionForm.getFirstInstall())
                    && !"y".equals(actionForm.getFromInstallPage())){
                System.out.println("Callback userName found for=" + actionForm.getUserName());
                logger.debug("Callback userName found for=" + actionForm.getUserName());
                userFacebookVO = userFacebookBO.updateCallbackViewsCounter(userFacebookVO, FacebookForm.FEATURED_BLOG_TYPE);
            }
            else{
                System.out.println("Username not found, is install time");
                // Else userName not found means now is the install time
                // Call facebook API and link with the login user id 
                userFacebookVO = userFacebookBO.newCallback3Install(userFacebookVO);
                if (StringUtils.hasValue(userFacebookVO.getUserName())){
                    actionForm.setUserName(userFacebookVO.getUserName());
                }
                else
                {
                    throw new Exception("Unable to get oEight User Name for facebook api Callback1 install");
                }
            }
            userFacebookBO = null;
            logger.debug("Callback ok for " + actionForm.getUserName());
            return CALLBACK_3;
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof FacebookException)
            {
                actionForm.getActionContext().getActionErrors().add("facebookInput",new ActionError("common.invalid.facebookAPI.connectionError"));
            }
            return INSTALL_3; 
        }
    }
    
    
    public String doInstall6(FacebookForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return INSTALL_6;
        } catch (Exception e) {
            return INSTALL_6;
        }
    }
    
    
    public String doCallback6(FacebookForm actionForm)
    throws ApplicationException {
        try {
            
            UserFacebookBO userFacebookBO = new UserFacebookBO();
            UserFacebookVO userFacebookVO = actionForm.getUserFacebookVO();
            // Check userName availability, if exists, means validated and already installed
            if (StringUtils.hasValue(actionForm.getFb_user()) 
                    && userFacebookBO.hasPledge(actionForm.getFb_user())
                    && "n".equals(actionForm.getFirstInstall())
                    && !"y".equals(actionForm.getFromInstallPage())){ 
                logger.debug("Callback id found for=" + actionForm.getFb_user());
                userFacebookVO = userFacebookBO.updateCallbackViewsCounter(userFacebookVO, FacebookForm.CLIMATE_CHANGE_TYPE);
            }
            else{ 
                // Else userName not found means now is the install time
                // Call facebook API and link with the login user id 
                userFacebookVO = userFacebookBO.newCallback6Install(userFacebookVO, actionForm.getPledgeCode());
            }
            userFacebookBO = null;
            logger.debug("Callback ok for " + actionForm.getFb_user());
            return CALLBACK_6;
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof FacebookException)
            {
                actionForm.getActionContext().getActionErrors().add("facebookInput",new ActionError("common.invalid.facebookAPI.connectionError"));
            }
            return INSTALL_6; 
        }
    }
    
    protected String getDefaultActionName() {
                  
        return INSTALL_1;
    }
}
