/*
 * OEightPledgeHelper.java
 *
 * Created on May 12, 2008, 4:01 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.oeight.member.bo.CampaignInviteBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author meauchyuan.lee
 */
public class OEightPledgeHelper {
        
    private static String[] pledgeCodes = initPledgeCodes();
    private static String[] pledgeCats = initPledgeCategory();
    private static String[] pledgeCodesNumber = initPledgeCodesNumber();
    private static String[] pledgeLabels = initPledgeLabels();
    private static Vector pledgeList;    
    private static Vector pledgeCodesVector;
    private static long maxPledgeFriends = reInitMaxPledgeFriends();
    
    /** Creates a new instance of OEightPledgeHelper */
    public OEightPledgeHelper() {
    }
    
    public static Vector getPledgeList(){
        if (pledgeList == null || pledgeList.size() <1)
        {
            pledgeList = getPledgeListForSelection(); 
        } 
        return pledgeList;
    }
    
    private static String[] initPledgeCodes()
    {
        if (pledgeCodes == null)
        {    
            List pledgeCodeList = new ArrayList();
            boolean done = false;
            int count = 1;
            while (!done)
            {
                String code = PropertyManager.getValue("pledge.code." + count);
                if (!StringUtils.hasValue(code))
                {
                    done = true;
                }
                else{
                    pledgeCodeList.add(code);
                    count++;
                }
            } 
            
            pledgeCodes = (String[])pledgeCodeList.toArray(new String[count]);  
        
        }
        
        return pledgeCodes;
    }   
    
    private static String[] initPledgeCategory()
    {     
        List pledgeCodeCategory = new ArrayList();
        boolean done = false;
        int count = 1;
        while (!done)
        {
            String pledgeCode = PropertyManager.getValue("pledge.code." + count);
            String pledgeCategory = "";
            if (!StringUtils.hasValue(pledgeCode))
            {
                done = true;
            }
            else{
                pledgeCategory = pledgeCode.substring(0,pledgeCode.indexOf(".")); 
                pledgeCodeCategory.add(pledgeCategory);
                count++;
            }
        } 

        pledgeCats = (String[])pledgeCodeCategory.toArray(new String[count]);  
        
        return pledgeCats;
    }   
     
    private static String[] initPledgeCodesNumber()
    {     
        List pledgeCodeNumberList = new ArrayList();
        boolean done = false;
        int count = 1;
        while (!done)
        {
            String pledgeCode = PropertyManager.getValue("pledge.code." + count);
            String pledgeNumber = "";
            if (!StringUtils.hasValue(pledgeCode))
            {
                done = true;
            }
            else{
                
                pledgeNumber = pledgeCode.substring(pledgeCode.indexOf(".")+1,pledgeCode.length());
                pledgeCodeNumberList.add(pledgeNumber);
                count++;
            }
        } 

        pledgeCodesNumber = (String[])pledgeCodeNumberList.toArray(new String[count]);  
        
        return pledgeCodesNumber;
    }   
    
    private static String[] initPledgeLabels()
    {
        if (pledgeLabels == null)
        {
          
            List pledgeLabelsList = new ArrayList();
            boolean done = false;
            int count = 1;
            while (!done)
            {
                String label = PropertyManager.getValue("pledge.label." + count);
                if (!StringUtils.hasValue(label))
                {
                    done = true;
                }
                else{
                    pledgeLabelsList.add(label);
                    count++;
                }
            } 
            
            pledgeLabels = (String[])pledgeLabelsList.toArray(new String[count]);  
        
        }
        
        return pledgeLabels;
    }
    
    private static Vector getPledgeListForSelection()
    {
        Vector pledge = new Vector();
        pledgeCodesVector = new Vector();
        for(int i=0;i<pledgeCodes.length;i++){
            String[] value = new String[2];
            value[0] = pledgeCodes[i];
            value[1] = pledgeLabels[i];
            pledge.add(value);
            pledgeCodesVector.add(value[0]); 
        }
        return pledge;
    }
    
    public static boolean isAcceptedPledgeCode(String pledgeCode)
    {
        if (StringUtils.hasValue(pledgeCode) && pledgeCodesVector.contains(pledgeCode))
            return true;
        else
            return false;
    }
      
    public static String getPledgeCategoryByCode(String pledgeCode)
    {
        for(int i=0;i<pledgeCats.length;i++){ 
            if (pledgeCode.equals(pledgeCodesNumber[i]))
            {
                return pledgeCats[i];
            }
        }
        return null;
    }
    
    public static String getPledgeLabelByCode(String pledgeCode)
    {
        for(int i=0;i<pledgeCodesNumber.length;i++){ 
            if (pledgeCode.equals(pledgeCodesNumber[i]))
            {
                return pledgeLabels[i];
            }
        }
        return null;
    }

    public static long reInitMaxPledgeFriends() {
        long maxAccepted = CampaignInviteBO.getMaxTotalAccepted(OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE); 
        maxPledgeFriends = maxAccepted;
        return maxAccepted;
    }
    
    public static long getMaxPledgeFriends()
    {
        return maxPledgeFriends;
    }
}
