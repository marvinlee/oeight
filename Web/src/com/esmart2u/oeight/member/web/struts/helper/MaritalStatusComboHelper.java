/*
 * MaritalStatusComboHelper.java
 *
 * Created on October 9, 2007, 12:11 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.solution.base.helper.StringUtils;
import java.util.*;

/**
 *
 * @author meauchyuan.lee
 */
public class MaritalStatusComboHelper {
    
      
    private static String[] statusCodes = new String[]{"UN","AV","AT","MR","CM"};
    private static String[] statusLabels = new String[]{"Unknown","Available","Attached","Married","Contact Me"};
    private static Vector statusList;
    private static Vector statusCodesVector;
    
    /** Creates a new instance of StatusComboHelper */
    public MaritalStatusComboHelper() {
    }
    
    public static Vector getStatusList(){
        if (statusList == null || statusList.size() <1)
        {
            statusList = getStatusCombo(); 
        } 
        return statusList;
    }
    
    private static Vector getStatusCombo()
    {
        Vector status = new Vector();
        statusCodesVector = new Vector();
        for(int i=0;i<statusCodes.length;i++){
            String[] value = new String[2];
            value[0] = statusCodes[i];
            value[1] = statusLabels[i];
            status.add(value);
            statusCodesVector.add(statusCodes[i]);
        }
        return status;
    }
    
    public static boolean isAcceptedStatusCode(String statusCode)
    {
        if (StringUtils.hasValue(statusCode) && statusCodesVector.contains(statusCode))
            return true;
        else
            return false;
    }
    
    public static String getStatusLabelByCode(String statusCode)
    {
        for(int i=0;i<statusCodes.length;i++){ 
            if (statusCode.equals(statusCodes[i]))
            {
                return statusLabels[i];
            }
        }
        return null;
    }
}
