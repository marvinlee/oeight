/*
 * StateComboHelper.java
 *
 * Created on September 30, 2007, 6:49 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;


import com.esmart2u.solution.base.helper.StringUtils;
import java.util.*;

/**
 *
 * @author meauchyuan.lee
 */
public class StateComboHelper {
    
      
    private static String[] stateCodes = new String[]{"JH","KH","KL","KN","LA","ME","NS","PG","PH","PJ","PK","PL","SA","SK","SL","TE"};
    private static String[] stateLabels = new String[]{"Johor","Kedah","Wilayah Persekutuan","Kelantan","Labuan","Melaka","Negeri Sembilan","Pulau Pinang","Pahang","Putrajaya","Perak","Perlis","Sabah","Sarawak","Selangor","Terengganu"};    
    private static Vector stateList;
    private static Vector stateCodesVector;
    
    /** Creates a new instance of StateComboHelper */
    public StateComboHelper() {
    }
    
    public static Vector getStateList(){
        if (stateList == null || stateList.size() <1)
        {
            stateList = getStateCombo(); 
        } 
        return stateList;
    }
    
    private static Vector getStateCombo()
    {
        Vector states = new Vector();
        stateCodesVector = new Vector();
        for(int i=0;i<stateCodes.length;i++){
            String[] value = new String[2];
            value[0] = stateCodes[i];
            value[1] = stateLabels[i];
            states.add(value);
            stateCodesVector.add(stateCodes[i]);
        }
        return states;
    }
    
    public static boolean isAcceptedStateCode(String stateCode)
    {
        if (StringUtils.hasValue(stateCode) && stateCodesVector.contains(stateCode))
            return true;
        else
            return false;
    }
    
    public static String getStateLabelByCode(String stateCode)
    {
        for(int i=0;i<stateCodes.length;i++){ 
            if (stateCode.equals(stateCodes[i]))
            {
                return stateLabels[i];
            }
        }
        return null;
    }
    
}
