/*
 * ProfileForm.java
 *
 * Created on October 4, 2007, 11:01 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.UserDetails;
import com.esmart2u.oeight.data.UserPictures;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.CountryComboHelper;
import com.esmart2u.oeight.member.web.struts.helper.MaritalStatusComboHelper;
import com.esmart2u.oeight.member.web.struts.helper.ProfileMapper;
import com.esmart2u.oeight.member.web.struts.helper.ProfilePreferenceComboHelper;
import com.esmart2u.oeight.member.web.struts.helper.StateComboHelper;
import com.esmart2u.solution.base.helper.BeanUtils;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author meauchyuan.lee
 */
public class ProfileForm  extends AbstractApplicationActionForm {
    
    
    private static Logger logger = Logger.getLogger(ProfileForm.class);
    
    SimpleDateFormat sdf = new SimpleDateFormat(OEightConstants.DATE_FORMAT);
    
    private String id;
    private int votesLeft;
    private boolean fromVoting = false;
    private boolean fromReportAbuse = false;
    private boolean viewingOwnProfile = false;
    private boolean inBuddyList = false;
    
    // For apps
    private boolean hasFriendsterFriends = false;
    private boolean hasFriendsterPhotos = false;
    private boolean hasFacebookFriends = false;
    private boolean hasFacebookPhotos = false;
    
    private String userName;
    private String emailAddress;
    private long pageViewCount;
    private long votesCount;
    
    //Default is for mosaic photo
    private FormFile photoFile;
    private String photoSmallPath;
    private String photoLargePath;
    private String photoDescription;
    
    // Profile photo
    private FormFile profilePhotoFile;
    private String profilePhotoLargePath;
    private String profilePhotoDescription;
    
    // T-shirt photo
    private FormFile shirtPhotoFile;
    private String shirtPhotoLargePath;
    private String shirtPhotoDescription;
    
    // Flickr Integration
    private String flickrId;
    private String yahooId;
    
    private String requestEdit;
    
    // From UserCountry
    private long userId;
    private String name;
    private String nric;
    private char gender;
    private String maritalStatus;
    private Date birthDate;
     
    private String city;
    private String state;
    private String nationality;
    private String currentLocation;
    private String schools;
    private String occupation;
    private String company;
    private String publicEmail;
    private Date systemRegistered;
    private String myWish;
    private int profilePreference;
    private String profilePreferenceString;
    private boolean hasMessage;
    private char subscribe;
    private char validated;
    private boolean hasBuddyRequest;
    
        // For display in UI
        private String birthDay;
        private String birthMonth;
        private String birthYear;
        private String birthdayString;
        private String genderString;
        private String maritalStatusString;
        private String stateString;
        private String nationalityString;
    
    // From UserDetails
    private char lifeSmoker;
    private char lifeDrinker;
    private String lifeAboutMe;
    private String lifeToKnow;
    private String lifeKnowMe;
    private String life1;
    private String life2;
    private String life3;
    private String life4;
    private String life5;
    private String lifeValue1;
    private String lifeValue2;
    private String lifeValue3;
    private String lifeValue4;
    private String lifeValue5;
    private String interestActor;
    private String interestActress;
    private String interestSinger;
    private String interestMusic;
    private String interestBand;
    private String interestMovies;
    private String interestBooks;
    private String interestSports;
    private String interestWebsites;
    private String interest1;
    private String interest2;
    private String interest3;
    private String interest4;
    private String interest5;
    private String interestValue1;
    private String interestValue2;
    private String interestValue3;
    private String interestValue4;
    private String interestValue5;
    private String skillsExpert;
    private String skillsGood;
    private String skillsLearn;
    private String contactPhone;
    private String contactGoogle;
    private String contactYahoo;
    private String contactMsn;
    private String webFriendster;
    private String webHi5;
    private String webWayn;
    private String webOrkut;
    private String webFacebook;
    private String webMyspace;
    private String webReserved1;
    private String webReserved2;
    private String webReserved3;
    private String webBlog;
    private boolean webBlogFeature;
    private String web1;
    private String web2;
    private String web3;
    private String web4;
    private String web5;
    private String webValue1;
    private String webValue2;
    private String webValue3;
    private String webValue4;
    private String webValue5;
     
    private String widgetMainHTML; 
    
    /** Creates a new instance of ProfileForm */
    public ProfileForm() {
    }
    
    /**
     * Clears the object prior to each request.
     *
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        this.clear();
    }
    
    
    /**
     *
     * clear() sets all the object's fields to <code>null</code>.
     *
     */
    public void clear() {
    }
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
        this.userName = id;
    }
    
    public long getPageViewCount() {
        return pageViewCount;
    }

    public void setPageViewCount(long pageViewCount) {
        this.pageViewCount = pageViewCount;
    } 

    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }
    
    public String getPhotoLargePath() {
        return photoLargePath;
    }

    public void setPhotoLargePath(String photoLargePath) {
        this.photoLargePath = photoLargePath;
    }
    
    public String getPhotoDescription() {
        return photoDescription;
    }

    public void setPhotoDescription(String photoDescription) {
        this.photoDescription = photoDescription;
    } 
    
    public FormFile getPhotoFile() {
        return photoFile;
    }

    public void setPhotoFile(FormFile photoFile) {
        this.photoFile = photoFile;
    }

    public FormFile getProfilePhotoFile() {
        return profilePhotoFile;
    }

    public void setProfilePhotoFile(FormFile profilePhotoFile) {
        this.profilePhotoFile = profilePhotoFile;
    }

    public String getProfilePhotoLargePath() {
        return profilePhotoLargePath;
    }

    public void setProfilePhotoLargePath(String profilePhotoLargePath) {
        this.profilePhotoLargePath = profilePhotoLargePath;
    }

    public String getProfilePhotoDescription() {
        return profilePhotoDescription;
    }

    public void setProfilePhotoDescription(String profilePhotoDescription) {
        this.profilePhotoDescription = profilePhotoDescription;
    }

    public FormFile getShirtPhotoFile() {
        return shirtPhotoFile;
    }

    public void setShirtPhotoFile(FormFile shirtPhotoFile) {
        this.shirtPhotoFile = shirtPhotoFile;
    }

    public String getShirtPhotoLargePath() {
        return shirtPhotoLargePath;
    }

    public void setShirtPhotoLargePath(String shirtPhotoLargePath) {
        this.shirtPhotoLargePath = shirtPhotoLargePath;
    }

    public String getShirtPhotoDescription() {
        return shirtPhotoDescription;
    }

    public void setShirtPhotoDescription(String shirtPhotoDescription) {
        this.shirtPhotoDescription = shirtPhotoDescription;
    }
    
    public String getRequestEdit() {
        return requestEdit;
    }
    
    public void setRequestEdit(String requestEdit) {
        this.requestEdit = requestEdit;
    }
    
    public long getUserId() {
        return userId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getNric() {
        return nric;
    }
    
    public void setNric(String nric) {
        this.nric = nric;
    }
    
    public char getGender() {
        return gender;
    }
    
    public void setGender(char gender) {
        this.gender = gender;
    }
    
    public Date getBirthDate() {
        if (birthDate == null)
        {
            getBirthdayString();
        }
        return birthDate;
    }
    
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        if (birthDate == null)
        {
            getBirthdayString();
        }
        else if (!(StringUtils.hasValue(birthDay) && StringUtils.hasValue(birthMonth) && StringUtils.hasValue(birthYear)))
        { 
            birthdayString = sdf.format(birthDate);
            StringTokenizer tokenizer = new StringTokenizer(birthdayString, "/"); 
            birthDay = tokenizer.nextToken("/");
            birthMonth = tokenizer.nextToken("/");
            birthYear = tokenizer.nextToken("/");
        }
    }
    
    
    public String getBirthDay() {
        return birthDay;
    }
    
    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }
    
    public String getBirthMonth() {
        return birthMonth;
    }
    
    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }
    
    public String getBirthYear() {
        return birthYear;
    }
    
    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }
    
    
    public String getBirthdayString() {
        if (StringUtils.hasValue(birthDay) && StringUtils.hasValue(birthMonth) && StringUtils.hasValue(birthYear)) {
            try {
                birthDate = sdf.parse(birthDay + "/" + birthMonth + "/" + birthYear);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            birthdayString = sdf.format(birthDate);
        }
        
        return birthdayString;
    }
    
    public void setBirthdayString(String birthdayString) {
        this.birthdayString = birthdayString;
    }
    
    
    
    public String getCity() {
        return city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getState() {
        return state;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    public String getNationality() {
        return nationality;
    }
    
    public void setNationality(String nationality) {
        this.nationality = nationality;
    } 
    
    public String getGenderString() {
        if (gender == PropertyConstants.GENDER_MALE)
        {
            genderString = OEightConstants.GENDER_MALE_DISPLAY;
        }
        else if (gender == PropertyConstants.GENDER_FEMALE)
        {
            genderString = OEightConstants.GENDER_FEMALE_DISPLAY; 
        }
        return genderString;
    }

    private void setGenderString(String genderString) {
        this.genderString = genderString;
    }

    public String getStateString() {
        if (StringUtils.hasValue(state))
        {
            stateString = StateComboHelper.getStateLabelByCode(state);
        }
        return stateString;
    }

    private void setStateString(String stateString) {
        
        this.stateString = stateString;
    }

    public String getNationalityString() {
        if (StringUtils.hasValue(nationality))
        {
            nationalityString = CountryComboHelper.getCountryLabelByCode(nationality);
        }
        return nationalityString;
    }

    private void setNationalityString(String nationalityString) {
        this.nationalityString = nationalityString;
    } 
    
    
    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) { 
        if (StringUtils.hasValue(maritalStatus))
        {
            maritalStatusString = MaritalStatusComboHelper.getStatusLabelByCode(maritalStatus);
        }
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatusString() {
        if (StringUtils.hasValue(maritalStatus))
        {
            maritalStatusString = MaritalStatusComboHelper.getStatusLabelByCode(maritalStatus);
        }
        return maritalStatusString;
    }

    public void setMaritalStatusString(String maritalStatusString) {
        this.maritalStatusString = maritalStatusString;
    }
    
    
    public String getCurrentLocation() {
        return currentLocation;
    }
    
    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }
    
    public String getSchools() {
        return schools;
    }
    
    public void setSchools(String schools) {
        this.schools = schools;
    }
    
    public String getOccupation() {
        return occupation;
    }
    
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
    
    public String getCompany() {
        return company;
    }
    
    public void setCompany(String company) {
        this.company = company;
    }
    
    public String getPublicEmail() {
        return publicEmail;
    }
    
    public void setPublicEmail(String publicEmail) {
        this.publicEmail = publicEmail;
    } 

    public Date getSystemRegistered() {
        return systemRegistered;
    }

    public void setSystemRegistered(Date systemRegistered) {
        this.systemRegistered = systemRegistered;
    }   
     
    public String getMyWish() {
        return myWish;
    }

    public void setMyWish(String myWish) {
        this.myWish = myWish;
    }
    
    public int getProfilePreference() {
        return profilePreference;
    }

    public void setProfilePreference(int profilePreference) {
        if (profilePreference > 0)
        {
            profilePreferenceString = ProfilePreferenceComboHelper.getPreferenceLabelByCode(profilePreference);
        } 
        this.profilePreference = profilePreference;
    }
   
    public String getProfilePreferenceString() {
        if (profilePreference > 0)
        {
            profilePreferenceString = ProfilePreferenceComboHelper.getPreferenceLabelByCode(profilePreference);
        }
        return profilePreferenceString;
    }

    public void setProfilePreferenceString(String profilePreferenceString) {
        this.profilePreferenceString = profilePreferenceString;
    } 

    public boolean getHasMessage() {
        return hasMessage;
    }
    
    public boolean isHasMessage() {
        return hasMessage;
    }

    public void setHasMessage(boolean hasMessage) {
        this.hasMessage = hasMessage;
    }
     
    public char getSubscribe() {
        if (subscribe != PropertyConstants.BOOLEAN_NO)
        {
            subscribe = PropertyConstants.BOOLEAN_YES;
        }
        return subscribe;
    }

    public void setSubscribe(char subscribe) {
        if (subscribe != PropertyConstants.BOOLEAN_NO)
        {
            subscribe = PropertyConstants.BOOLEAN_YES;
        }
        this.subscribe = subscribe;
    }
    
    public char getLifeSmoker() {
        return lifeSmoker;
    }
    
    public void setLifeSmoker(char lifeSmoker) {
        this.lifeSmoker = lifeSmoker;
    }
    
    public char getLifeDrinker() {
        return lifeDrinker;
    }
    
    public void setLifeDrinker(char lifeDrinker) {
        this.lifeDrinker = lifeDrinker;
    }
    
    public String getLifeAboutMe() {
        return lifeAboutMe;
    }
    
    public void setLifeAboutMe(String lifeAboutMe) {
        this.lifeAboutMe = lifeAboutMe;
    }
    
    public String getLifeToKnow() {
        return lifeToKnow;
    }
    
    public void setLifeToKnow(String lifeToKnow) {
        this.lifeToKnow = lifeToKnow;
    }
    
    public String getLifeKnowMe() {
        return lifeKnowMe;
    }
    
    public void setLifeKnowMe(String lifeKnowMe) {
        this.lifeKnowMe = lifeKnowMe;
    }
    
    public String getLife1() {
        return life1;
    }
    
    public void setLife1(String life1) {
        this.life1 = life1;
    }
    
    public String getLife2() {
        return life2;
    }
    
    public void setLife2(String life2) {
        this.life2 = life2;
    }
    
    public String getLife3() {
        return life3;
    }
    
    public void setLife3(String life3) {
        this.life3 = life3;
    }
    
    public String getLife4() {
        return life4;
    }
    
    public void setLife4(String life4) {
        this.life4 = life4;
    }
    
    public String getLife5() {
        return life5;
    }
    
    public void setLife5(String life5) {
        this.life5 = life5;
    }
    
    public String getLifeValue1() {
        return lifeValue1;
    }
    
    public void setLifeValue1(String lifeValue1) {
        this.lifeValue1 = lifeValue1;
    }
    
    public String getLifeValue2() {
        return lifeValue2;
    }
    
    public void setLifeValue2(String lifeValue2) {
        this.lifeValue2 = lifeValue2;
    }
    
    public String getLifeValue3() {
        return lifeValue3;
    }
    
    public void setLifeValue3(String lifeValue3) {
        this.lifeValue3 = lifeValue3;
    }
    
    public String getLifeValue4() {
        return lifeValue4;
    }
    
    public void setLifeValue4(String lifeValue4) {
        this.lifeValue4 = lifeValue4;
    }
    
    public String getLifeValue5() {
        return lifeValue5;
    }
    
    public void setLifeValue5(String lifeValue5) {
        this.lifeValue5 = lifeValue5;
    }
    
    public String getInterestActor() {
        return interestActor;
    }
    
    public void setInterestActor(String interestActor) {
        this.interestActor = interestActor;
    }
    
    public String getInterestActress() {
        return interestActress;
    }
    
    public void setInterestActress(String interestActress) {
        this.interestActress = interestActress;
    }
    
    public String getInterestSinger() {
        return interestSinger;
    }
    
    public void setInterestSinger(String interestSinger) {
        this.interestSinger = interestSinger;
    }
    
    public String getInterestMusic() {
        return interestMusic;
    }
    
    public void setInterestMusic(String interestMusic) {
        this.interestMusic = interestMusic;
    }
    
    public String getInterestBand() {
        return interestBand;
    }
    
    public void setInterestBand(String interestBand) {
        this.interestBand = interestBand;
    }
    
    public String getInterestMovies() {
        return interestMovies;
    }
    
    public void setInterestMovies(String interestMovies) {
        this.interestMovies = interestMovies;
    }
    
    public String getInterestBooks() {
        return interestBooks;
    }
    
    public void setInterestBooks(String interestBooks) {
        this.interestBooks = interestBooks;
    }
    
    public String getInterestSports() {
        return interestSports;
    }
    
    public void setInterestSports(String interestSports) {
        this.interestSports = interestSports;
    }
    
    public String getInterestWebsites() {
        return interestWebsites;
    }
    
    public void setInterestWebsites(String interestWebsites) {
        this.interestWebsites = interestWebsites;
    }
    
    public String getInterest1() {
        return interest1;
    }
    
    public void setInterest1(String interest1) {
        this.interest1 = interest1;
    }
    
    public String getInterest2() {
        return interest2;
    }
    
    public void setInterest2(String interest2) {
        this.interest2 = interest2;
    }
    
    public String getInterest3() {
        return interest3;
    }
    
    public void setInterest3(String interest3) {
        this.interest3 = interest3;
    }
    
    public String getInterest4() {
        return interest4;
    }
    
    public void setInterest4(String interest4) {
        this.interest4 = interest4;
    }
    
    public String getInterest5() {
        return interest5;
    }
    
    public void setInterest5(String interest5) {
        this.interest5 = interest5;
    }
    
    public String getInterestValue1() {
        return interestValue1;
    }
    
    public void setInterestValue1(String interestValue1) {
        this.interestValue1 = interestValue1;
    }
    
    public String getInterestValue2() {
        return interestValue2;
    }
    
    public void setInterestValue2(String interestValue2) {
        this.interestValue2 = interestValue2;
    }
    
    public String getInterestValue3() {
        return interestValue3;
    }
    
    public void setInterestValue3(String interestValue3) {
        this.interestValue3 = interestValue3;
    }
    
    public String getInterestValue4() {
        return interestValue4;
    }
    
    public void setInterestValue4(String interestValue4) {
        this.interestValue4 = interestValue4;
    }
    
    public String getInterestValue5() {
        return interestValue5;
    }
    
    public void setInterestValue5(String interestValue5) {
        this.interestValue5 = interestValue5;
    }
    
    public String getSkillsExpert() {
        return skillsExpert;
    }
    
    public void setSkillsExpert(String skillsExpert) {
        this.skillsExpert = skillsExpert;
    }
    
    public String getSkillsGood() {
        return skillsGood;
    }
    
    public void setSkillsGood(String skillsGood) {
        this.skillsGood = skillsGood;
    }
    
    public String getSkillsLearn() {
        return skillsLearn;
    }
    
    public void setSkillsLearn(String skillsLearn) {
        this.skillsLearn = skillsLearn;
    }
    
    public String getContactPhone() {
        return contactPhone;
    }
    
    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }
    
    public String getContactGoogle() {
        return contactGoogle;
    }
    
    public void setContactGoogle(String contactGoogle) {
        this.contactGoogle = contactGoogle;
    }
    
    public String getContactYahoo() {
        return contactYahoo;
    }
    
    public void setContactYahoo(String contactYahoo) {
        this.contactYahoo = contactYahoo;
    }
    
    public String getContactMsn() {
        return contactMsn;
    }
    
    public void setContactMsn(String contactMsn) {
        this.contactMsn = contactMsn;
    }
    
    public String getWebFriendster() {
        return webFriendster;
    }
    
    public void setWebFriendster(String webFriendster) {
        this.webFriendster = webFriendster;
    }
    
    public String getWebHi5() {
        return webHi5;
    }
    
    public void setWebHi5(String webHi5) {
        this.webHi5 = webHi5;
    }
    
    public String getWebWayn() {
        return webWayn;
    }
    
    public void setWebWayn(String webWayn) {
        this.webWayn = webWayn;
    }
    
    public String getWebOrkut() {
        return webOrkut;
    }
    
    public void setWebOrkut(String webOrkut) {
        this.webOrkut = webOrkut;
    }
    
    public String getWebFacebook() {
        return webFacebook;
    }

    public void setWebFacebook(String webFacebook) {
        this.webFacebook = webFacebook;
    }

    public String getWebMyspace() {
        return webMyspace;
    }

    public void setWebMyspace(String webMyspace) {
        this.webMyspace = webMyspace;
    }

    public String getWebReserved1() {
        return webReserved1;
    }

    public void setWebReserved1(String webReserved1) {
        this.webReserved1 = webReserved1;
    }

    public String getWebReserved2() {
        return webReserved2;
    }

    public void setWebReserved2(String webReserved2) {
        this.webReserved2 = webReserved2;
    }

    public String getWebReserved3() {
        return webReserved3;
    }

    public void setWebReserved3(String webReserved3) {
        this.webReserved3 = webReserved3;
    }
    
    public String getWebBlog() {
        return webBlog;
    }
    
    public void setWebBlog(String webBlog) {
        this.webBlog = webBlog;
    }
    
    public boolean isWebBlogFeature() {
        return webBlogFeature;
    }
    
    public boolean getWebBlogFeature() {
        return webBlogFeature;
    }

    public void setWebBlogFeature(boolean webBlogFeature) {
        this.webBlogFeature = webBlogFeature;
    }
    
    public String getWeb1() {
        return web1;
    }
    
    public void setWeb1(String web1) {
        this.web1 = web1;
    }
    
    public String getWeb2() {
        return web2;
    }
    
    public void setWeb2(String web2) {
        this.web2 = web2;
    }
    
    public String getWeb3() {
        return web3;
    }
    
    public void setWeb3(String web3) {
        this.web3 = web3;
    }
    
    public String getWeb4() {
        return web4;
    }
    
    public void setWeb4(String web4) {
        this.web4 = web4;
    }
    
    public String getWeb5() {
        return web5;
    }
    
    public void setWeb5(String web5) {
        this.web5 = web5;
    }
    
    public String getWebValue1() {
        return webValue1;
    }
    
    public void setWebValue1(String webValue1) {
        this.webValue1 = webValue1;
    }
    
    public String getWebValue2() {
        return webValue2;
    }
    
    public void setWebValue2(String webValue2) {
        this.webValue2 = webValue2;
    }
    
    public String getWebValue3() {
        return webValue3;
    }
    
    public void setWebValue3(String webValue3) {
        this.webValue3 = webValue3;
    }
    
    public String getWebValue4() {
        return webValue4;
    }
    
    public void setWebValue4(String webValue4) {
        this.webValue4 = webValue4;
    }
    
    public String getWebValue5() {
        return webValue5;
    }
    
    public void setWebValue5(String webValue5) {
        this.webValue5 = webValue5;
    }
    
    void setUserProfileMain(User user) {
        UserCountry userCountry = user.getUserCountry();
        try {
            
            BeanUtils.copyBean(user, this);
            BeanUtils.copyBean(userCountry, this);
            logger.debug("ProfileForm state=" + this.state);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }
    
    void setUserProfilePhotos(User user)
    {

        logger.debug("actionForm setting userprofile photos1");
        setUserProfileMain(user);
        
        // Now set photos

        logger.debug("actionForm setting userprofile photos2");
        List photoList = user.getPhotoList();
        if (photoList != null)
        {
            for (int i=0; i<photoList.size();i++)
            {
                UserPictures photo = (UserPictures)photoList.get(i);
                char photoType = photo.getPhotoType();
                logger.debug("Photo type=" + photo.getPhotoType());
                switch (photoType)
                {
                    case OEightConstants.PHOTO_TYPE_MOSAIC:
                        this.photoLargePath = photo.getPhotoLarge();
                        this.photoDescription= photo.getCaption();
                        break;
                    case OEightConstants.PHOTO_TYPE_PROFILE:
                        this.profilePhotoLargePath = photo.getPhotoLarge();
                        this.profilePhotoDescription= photo.getCaption(); 
                        break;
                    case OEightConstants.PHOTO_TYPE_SHIRT:
                        this.shirtPhotoLargePath = photo.getPhotoLarge();
                        this.shirtPhotoDescription= photo.getCaption(); 
                        break; 
                }
                
            } 
        
        } 
    }
    
    User getUserProfileMain() {
        User user = ProfileMapper.mapUserProfileMain(this);
        return user;
    }
    
    void setUserProfileLifestyle(UserDetails userDetails) { 
        try {
            
            BeanUtils.copyBean(userDetails, this);  
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }

    UserDetails getUserProfileLifestyle() {
        UserDetails userDetails = ProfileMapper.mapUserProfileLifestyle(this);
        return userDetails;
    }
    
    
    void setUserProfileInterest(UserDetails userDetails) { 
        try {
            
            BeanUtils.copyBean(userDetails, this);  
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }

    UserDetails getUserProfileInterest() {
        UserDetails userDetails = ProfileMapper.mapUserProfileInterest(this);
        return userDetails;
    }
    
    void setUserProfileSkills(UserDetails userDetails) { 
        try {
            
            BeanUtils.copyBean(userDetails, this);  
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }

    UserDetails getUserProfileSkills() {
        UserDetails userDetails = ProfileMapper.mapUserProfileSkills(this);
        return userDetails;
    }
    
    
    void setUserProfileContact(UserDetails userDetails) { 
        try {
            
            BeanUtils.copyBean(userDetails, this);  
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }

    UserDetails getUserProfileContact() {
        UserDetails userDetails = ProfileMapper.mapUserProfileContact(this);
        return userDetails;
    }

    void setUserProfileAll(UserDetails userDetails){ 
        try {
            
            BeanUtils.copyBean(userDetails, this);  
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }

    public int getVotesLeft() {
        return votesLeft;
    }

    public void setVotesLeft(int votesLeft) {
        this.votesLeft = votesLeft;
    }

    public boolean isFromVoting() {
        return fromVoting;
    }

    public void setFromVoting(boolean fromVoting) {
        this.fromVoting = fromVoting;
    }

    public long getVotesCount() {
        return votesCount;
    }

    public void setVotesCount(long votesCount) {
        this.votesCount = votesCount;
    }

    public boolean isFromReportAbuse() {
        return fromReportAbuse;
    }

    public void setFromReportAbuse(boolean fromReportAbuse) {
        this.fromReportAbuse = fromReportAbuse;
    }

    public boolean isViewingOwnProfile() {
        return viewingOwnProfile;
    }

    public void setViewingOwnProfile(boolean viewingOwnProfile) {
        this.viewingOwnProfile = viewingOwnProfile;
    }

    public String getFlickrId() {
        return flickrId;
    }

    public void setFlickrId(String flickrId) {
        this.flickrId = flickrId;
    }

    public String getYahooId() {
        return yahooId;
    }

    public void setYahooId(String yahooId) {
        this.yahooId = yahooId;
    }

    public String getWidgetMainHTML() {
        return widgetMainHTML;
    }

    public void setWidgetMainHTML(String widgetMainHTML) {
        this.widgetMainHTML = widgetMainHTML;
    }

    public boolean isHasFriendsterFriends() {
        return hasFriendsterFriends;
    }

    public void setHasFriendsterFriends(boolean hasFriendsterFriends) {
        this.hasFriendsterFriends = hasFriendsterFriends;
    }

    public boolean isHasFriendsterPhotos() {
        return hasFriendsterPhotos;
    }

    public void setHasFriendsterPhotos(boolean hasFriendsterPhotos) {
        this.hasFriendsterPhotos = hasFriendsterPhotos;
    }

    public boolean isHasFacebookFriends() {
        return hasFacebookFriends;
    }

    public void setHasFacebookFriends(boolean hasFacebookFriends) {
        this.hasFacebookFriends = hasFacebookFriends;
    }

    public boolean isHasFacebookPhotos() {
        return hasFacebookPhotos;
    }

    public void setHasFacebookPhotos(boolean hasFacebookPhotos) {
        this.hasFacebookPhotos = hasFacebookPhotos;
    } 

    public boolean isInBuddyList() {
        return inBuddyList;
    }

    public void setInBuddyList(boolean inBuddyList) {
        this.inBuddyList = inBuddyList;
    }

    public char getValidated() {
        return validated;
    }

    public void setValidated(char validated) {
        this.validated = validated;
    }

    public boolean isHasBuddyRequest() {
        return hasBuddyRequest;
    }

    public void setHasBuddyRequest(boolean hasBuddyRequest) {
        this.hasBuddyRequest = hasBuddyRequest;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    
}
