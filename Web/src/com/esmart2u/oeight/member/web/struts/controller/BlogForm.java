package com.esmart2u.oeight.member.web.struts.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.esmart2u.oeight.member.helper.OEightConstants;

public class BlogForm extends AbstractApplicationActionForm {
    

    private long userBlogId;
    private long userId;
    private String blogName;
    private String blogUrl;
    private String blogFeed;
    private String blogThumbnail;
    private Date lastFetched; 
    private String lastFetchedString;
    private char greenFlag;
    private char validated;
    private char spamFlag; 
    
	private String urlOk;
    private boolean urlDuplicate; 
    SimpleDateFormat sdf = new SimpleDateFormat(OEightConstants.DATE_FORMAT);
    

    private ArrayList postList;
    
    
	public long getUserBlogId() {
		return userBlogId;
	}
	public void setUserBlogId(long userBlogId) {
		this.userBlogId = userBlogId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getBlogName() {
		return blogName;
	}
	public void setBlogName(String blogName) {
		this.blogName = blogName;
	}
	public String getBlogUrl() {
		return blogUrl;
	}
	public void setBlogUrl(String blogUrl) {
		this.blogUrl = blogUrl;
	}
	public String getBlogFeed() {
		return blogFeed;
	}
	public void setBlogFeed(String blogFeed) {
		this.blogFeed = blogFeed;
	}
	public String getBlogThumbnail() {
		return blogThumbnail;
	}
	public void setBlogThumbnail(String blogThumbnail) {
		this.blogThumbnail = blogThumbnail;
	}
	public Date getLastFetched() {
		return lastFetched;
	}
	public void setLastFetched(Date lastFetched) {
		this.lastFetched = lastFetched;
	}
	public String getLastFetchedString() {
		return lastFetchedString;
	}
	public void setLastFetchedString(String lastFetchedString) {
		this.lastFetchedString = lastFetchedString;
	}
	public char getGreenFlag() {
		return greenFlag;
	}
	public void setGreenFlag(char greenFlag) {
		this.greenFlag = greenFlag;
	}
	public char getValidated() {
		return validated;
	}
	public void setValidated(char validated) {
		this.validated = validated;
	}
	public char getSpamFlag() {
		return spamFlag;
	}
	public void setSpamFlag(char spamFlag) {
		this.spamFlag = spamFlag;
	}
	public String getUrlOk() {
		return urlOk;
	}
	public void setUrlOk(String urlOk) {
		this.urlOk = urlOk;
	}
	public boolean isUrlDuplicate() {
		return urlDuplicate;
	}
	public void setUrlDuplicate(boolean urlDuplicate) {
		this.urlDuplicate = urlDuplicate;
	}
	public ArrayList getPostList() {
		return postList;
	}
	public void setPostList(ArrayList postList) {
		this.postList = postList;
	}
    

    

}
