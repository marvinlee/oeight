/*
 * ProfilePreferenceComboHelper.java
 *
 * Created on November 3, 2007, 4:56 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import java.util.*;
/**
 *
 * @author meauchyuan.lee
 */
public class ProfilePreferenceComboHelper {  
    private static String[] preferenceCodes = new String[]{Integer.toString(OEightConstants.PROFILE_PREFERENCE_SHOW),
                                                           Integer.toString(OEightConstants.PROFILE_PREFERENCE_HIDE),
                                                           Integer.toString(OEightConstants.PROFILE_PREFERENCE_CONTACT)};
    private static String[] preferenceLabels = new String[]{"Show Profile","Hide Profile","Hide Contact Only"};
    private static Vector preferenceList;
    private static Vector preferenceCodesVector;
     
    /** Creates a new instance of ProfilePreferenceComboHelper */
    public ProfilePreferenceComboHelper() {
    }
    
    public static Vector getPreferenceList(){
        if (preferenceList == null || preferenceList.size() <1)
        {
            preferenceList = getPreferenceCombo(); 
        } 
        return preferenceList;
    }
    
    private static Vector getPreferenceCombo()
    {
        Vector status = new Vector();
        preferenceCodesVector = new Vector();
        for(int i=0;i<preferenceCodes.length;i++){
            String[] value = new String[2];
            value[0] = preferenceCodes[i];
            value[1] = preferenceLabels[i];
            status.add(value);
            preferenceCodesVector.add(preferenceCodes[i]);
        }
        return status;
    }
    
    public static boolean isAcceptedPreferenceCode(int preferenceCode)
    {
        if (preferenceCode> 0 && preferenceCodesVector.contains(Integer.toString(preferenceCode)))
            return true;
        else
            return false;
    }
    
    public static String getPreferenceLabelByCode(int preferenceCode)
    {
        if (preferenceCode > 0)
        {
            for(int i=0;i<preferenceCodes.length;i++){ 
                if (Integer.toString(preferenceCode).equals(preferenceCodes[i]))
                {
                    return preferenceLabels[i];
                }
            }
        }
        return null;
    }
    
}
