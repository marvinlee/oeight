/*
 * MessageValidator.java
 *
 * Created on March 18, 2008, 4:46 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserMessageBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.MessageVO;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class MessageValidator {
    
    private static Logger logger = Logger.getLogger(MessageValidator.class);
    
    /** Creates a new instance of MessageValidator */
    public MessageValidator() {
    }
        
    public static ActionErrors validateNewMessage(ActionErrors errors, MessageForm messageForm) {
          
        //todo, check how many messages sent today, max 200
        
        UserBO userBO = new UserBO();
        User user = userBO.getUserByUserName(messageForm.getTo());
        if (user == null)
        { 
            errors.add("recipient",new ActionError("common.invalid.message.recipient"));
        }
        userBO = null;
        
        String sessionToken = (String) messageForm.getActionContext().getRequest().getSession().getAttribute(OEightConstants.SESSION_NONCE_TOKEN);
        String formToken = messageForm.getNonceToken();
        System.out.println("sessionToken=" + sessionToken + "formToken=" + formToken);
        if (!StringUtils.hasValue(formToken) || !formToken.equals(sessionToken))
        { 
            errors.add("nonceToken",new ActionError("common.invalid.message.nonceToken"));
        } 
        if (!Validator.validateString(messageForm.getSubject(),true,1,100,false)) {
            errors.add("subject",new ActionError("common.invalid.message.subject"));
        }
        if (!Validator.validateString(messageForm.getMessage(),true,0,500,false)) {
            errors.add("message",new ActionError("common.invalid.message.message"));
        }
        
        return errors;
    }    
    public static ActionErrors validateReplyMessage(ActionErrors errors, MessageForm messageForm) {
        
        // Validates if message belongs
        UserMessageBO userMessageBO = new UserMessageBO();
        String userId = (String)messageForm.getActionContext().getRequest().getSession().getAttribute(OEightConstants.SESSION_USER_ID);
        
        MessageVO messageVO = userMessageBO.getUserMessage(""+messageForm.getMsgId(),userId);
        if (messageVO == null)
        {
            // msgId been tampered with, show invalid request
            errors.add("nonceToken",new ActionError("common.invalid.message.nonceToken")); 
        }
        
        
        UserBO userBO = new UserBO();
        User user = userBO.getUserByUserName(messageForm.getTo());
        if (user == null)
        { 
            errors.add("recipient",new ActionError("common.invalid.message.recipient"));
        }
        userBO = null;
        
        String sessionToken = (String) messageForm.getActionContext().getRequest().getSession().getAttribute(OEightConstants.SESSION_NONCE_TOKEN);
        String formToken = messageForm.getNonceToken();
        System.out.println("sessionToken=" + sessionToken + "formToken=" + formToken);
        if (!StringUtils.hasValue(formToken) || !formToken.equals(sessionToken))
        { 
            errors.add("nonceToken",new ActionError("common.invalid.message.nonceToken"));
        } 
        if (!Validator.validateString(messageForm.getSubject(),true,1,100,false)) {
            errors.add("subject",new ActionError("common.invalid.message.subject"));
        }
        if (!Validator.validateString(messageForm.getMessage(),true,0,500,false)) {
            errors.add("message",new ActionError("common.invalid.message.message"));
        }
        
        return errors;
    }
}
