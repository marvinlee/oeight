/*
 * ShopAction.java
 *
 * Created on January 12, 2008, 2:30 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserShop;
import com.esmart2u.oeight.member.bo.ShopBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.helper.smtp.ShopEmailer;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.base.helper.ApplicationException;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class ShopAction   extends AbstractApplicationAction {
      
    private static String MAIN = "main";
    private static String LOVERS = "lovers";
    private static String GUIDE_PUBLIC = "guide_public";
    private static String GUIDE_PRIVATE = "guide_private";
    private static String COUPLE08 = "couple08";
    private static String COUPLE08CONFIRM = "couple08Confirm";
    private static String ORDERRECEIVED = "orderReceived";
    private static String ORDERLIST = "orderList";
    private static String RELOGIN = "relogin";
   
    /** Creates a new instance of ShopAction */
    public ShopAction() {
    }
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(ShopAction.class);
    
    
    
    protected boolean isValidationRequired(String event) { 
        
        if (COUPLE08CONFIRM.equals(event))
        {
            return true;
        }
        
        return false;
    } 
    
       
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        ShopForm shopForm = (ShopForm)actionForm;
        
        logger.debug("Validate one=" + action);
        
        if (COUPLE08CONFIRM.equals(action)) {
            errors = ShopValidator.validateCoupleTee(errors, shopForm);
        } else {
            logger.debug("Temporary skip");
        }
        
        shopForm.getActionContext().setActionErrors(errors);
        return shopForm;
    }
    
      protected String getDefaultEvent(String action) {
        String previousAction = action;
        if (COUPLE08CONFIRM.equals(action))
            action = COUPLE08; 
        else {
            logger.debug("Temporary skip");
        }
        logger.debug("Going back to previous action from [" + previousAction + "] to [" + action + "] ");
        return action;
    }
      
    protected String getDefaultActionName() {
                  
        return MAIN;
    }
     
    public String doMain(ShopForm actionForm){ 
        return MAIN;  
    } 
       
    public String doLovers(ShopForm actionForm){ 
        return LOVERS;  
    } 
        
    public String doGuide(ShopForm actionForm)
    throws ApplicationException {
        
        //todo we segregate the guide to public and private in the future
        //try {
            
            //
            // Get User Id from session and get details
            //String userId = checkUserAccess(actionForm);
        
            return GUIDE_PUBLIC;
        //} catch (Exception e) { 
        //    return GUIDE_PUBLIC;
        //}
    } 
    
    
    public String doCouple08(ShopForm shopForm)
    throws ApplicationException { 
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(shopForm); 
            
            // Set userId and email to form
            UserBO userBO = new UserBO();
            User user = userBO.getUserById(userId);
            
            shopForm.setUserId(Long.parseLong(userId));
            shopForm.setName(user.getUserCountry().getName()); // We use this to validate payment
            shopForm.setEmail(user.getEmailAddress());  
            shopForm.setShopCode(COUPLE08);
            
            userBO = null; 
             
            return COUPLE08;  
        } catch (Exception e) { 
            return RELOGIN;
        }
        
    } 
    
    public String doCouple08Confirm(ShopForm shopForm)
    throws ApplicationException { 
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(shopForm); 
              
            // Set userId and email to form
            UserBO userBO = new UserBO();
            User user = userBO.getUserById(userId);
            
            shopForm.setUserId(Long.parseLong(userId)); 
            shopForm.setEmail(user.getEmailAddress());   
            shopForm.setShopCode(COUPLE08);
            shopForm.setShopStatus(ShopForm.NEW);
            
            // Gets values and insert into DB
            UserShop userShop = shopForm.getUserShop();
            userShop.setShopDescription(shopForm.getCoupleTeeDescription());
            String referenceNumber = DateObjectHelper.getReferenceNumberTimestamp(new Date());
            long shopReferenceNumber = Long.parseLong(referenceNumber) + Long.parseLong(userId);
            userShop.setShopReferenceNo(shopReferenceNumber);
            
            ShopBO shopBO = new ShopBO();
            shopBO.takeNewOrder(userShop); 
           
            shopBO = null;
            shopForm.setShopReferenceNo(shopReferenceNumber);
                    
            return COUPLE08CONFIRM;  
        } catch (Exception e) { 
            return RELOGIN;
        }
        
    } 
    
    public String doOrderReceived(ShopForm shopForm)
    throws ApplicationException { 
        
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(shopForm); 
            
            long referenceNumber = shopForm.getShopReferenceNo(); 
            UserShop userShop = new UserShop();
            userShop.setUserId(Long.parseLong(userId));
            userShop.setShopReferenceNo(referenceNumber);
            
            ShopBO shopBO = new ShopBO();         
            userShop = shopBO.getOrderByReferenceNo(userShop);
            shopForm.setEmail(userShop.getEmail());
                    
            // Sends email
            boolean mailSent = ShopEmailer.sendOrderNotification(userShop);
            
            if (mailSent)
            {
                userShop.setShopStatus(ShopForm.NEW_NOTIFIED);
            }else
            {
                userShop.setShopStatus(ShopForm.NEW_FAIL_NOTIFY); 
            }
            shopBO.updateOrder(userShop); 
           
            shopBO = null;
            
              
            return ORDERRECEIVED; 
        } catch (Exception e) { 
            return RELOGIN;
        }
        
    } 
    
    public String doOrderList(ShopForm shopForm)
    throws ApplicationException { 
         try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(shopForm); 
             
            // Get shopping list
            ShopBO shopBO = new ShopBO();
            List orderList = shopBO.getOrderList(userId);
             
            shopForm.setOrderList(orderList);
              
            shopBO = null;
            
            return ORDERLIST;  
        } catch (Exception e) { 
            return RELOGIN;
        }
        
    } 
    
}
