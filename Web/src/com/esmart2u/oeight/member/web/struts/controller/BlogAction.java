package com.esmart2u.oeight.member.web.struts.controller;

import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
  
import com.esmart2u.oeight.data.UserBlog; 
import com.esmart2u.oeight.member.bo.BlogBO; 
import com.esmart2u.solution.base.helper.ApplicationException; 
import com.esmart2u.solution.base.helper.PropertyConstants;

public class BlogAction  extends AbstractApplicationAction {
     
    private static String MAIN = "main";
    private static String UPDATE = "update";
    private static String SUBMITTED = "submitted";
    
    private static String RELOGIN = "relogin";
     
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(BlogAction.class);
    
    
    /** Creates a new instance of BlogAction */
    public BlogAction() {
    }
     
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (SUBMITTED.equals(event))
            return true;
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        BlogForm blogForm = (BlogForm)actionForm; 
        if (SUBMITTED.equals(action)) {
            try {
    			
            	String loginUserId = checkUserAccess(blogForm);
    			blogForm.setUserId(Long.parseLong(loginUserId));
    			
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
            errors = BlogValidator.checkBlog(errors, blogForm); 
        } else
        {
            logger.debug("Temporary skip");
        }
        
        blogForm.getActionContext().setActionErrors(errors);
        return blogForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (UPDATE.equals(action)) {
            action = MAIN; 
        } else if (SUBMITTED.equals(action)) {
            action = UPDATE; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doMain(BlogForm actionForm)
    throws ApplicationException {
        try { 
            String loginUserId = checkUserAccess(actionForm);
            // Get existing user blog info for display

            BlogBO blogBO = new BlogBO();
            UserBlog userBlog = (UserBlog)blogBO.getBlogDetailsByUserId(loginUserId);
            
            actionForm.setBlogUrl(userBlog.getBlogUrl());
            actionForm.setBlogFeed(userBlog.getBlogFeed());
            
            return MAIN;
        } catch (Exception e) {
            return MAIN;
        }
    }
  
    public String doUpdate(BlogForm actionForm)
    throws ApplicationException {
        try { 
            String loginUserId = checkUserAccess(actionForm);
            // Get existing user blog info for update
            BlogBO blogBO = new BlogBO();
            UserBlog userBlog = (UserBlog)blogBO.getBlogDetailsByUserId(loginUserId);
             
            actionForm.setBlogUrl(userBlog.getBlogUrl());
            actionForm.setBlogFeed(userBlog.getBlogFeed());
            
            return UPDATE;
        } catch (Exception e) {
            e.printStackTrace();
            return RELOGIN;
        }
    }
       
    
    public String doSubmitted(BlogForm actionForm)
    throws ApplicationException {
        try {
            String loginUserId = checkUserAccess(actionForm);  
            // Get User Id from session and get details 
            actionForm.setUserId(Long.parseLong(loginUserId));
            actionForm.setLastFetched(new Date()); 
            
            // Save blog + posts into DB     
            UserBlog userBlog = new UserBlog();
            userBlog.setUserId(actionForm.getUserId());
            userBlog.setBlogName(actionForm.getBlogName());
            userBlog.setBlogUrl(actionForm.getBlogUrl());
            userBlog.setBlogFeed(actionForm.getBlogFeed());
            userBlog.setLastFetched(new Date());
            userBlog.setGreenFlag(actionForm.getGreenFlag());  
            userBlog.setValidated(PropertyConstants.BOOLEAN_NO); 
    
            BlogBO blogBO = new BlogBO();
            blogBO.saveBlog(userBlog); 
            blogBO = null;
          
            return SUBMITTED;
        } catch (Exception e) {
            e.printStackTrace();
            return RELOGIN;
        }
    }
    
    protected String getDefaultActionName() {
                  
        return MAIN;
    }

}
