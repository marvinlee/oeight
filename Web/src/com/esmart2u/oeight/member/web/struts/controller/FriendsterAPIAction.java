/*
 * FriendsterAPIAction.java
 *
 * Created on March 22, 2008, 9:11 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.exception.FriendsterException;
import com.esmart2u.oeight.member.bo.UserFriendsterBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.UserFriendsterVO;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterAPIAction extends AbstractApplicationAction {
     
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(FriendsterAPIAction.class); 
    
    // Main - compact wish widget
    private static String INSTALL_1 = "install1";
    private static String CALLBACK_1 = "callback1"; 
    
    // Wish + Buddies widget
    private static String INSTALL_2 = "install2";
    private static String CALLBACK_2 = "callback2"; 
    
    // Featured blog
    private static String INSTALL_3 = "install3";
    private static String CALLBACK_3 = "callback3"; 
    
    // What say u?
    private static String INSTALL_4 = "install4";
    private static String CALLBACK_4 = "callback4"; 
    
    // oEight + Friendster Friends xChange
    private static String INSTALL_5 = "install5";
    private static String CALLBACK_5 = "callback5"; 
    
    // for Climate Change
    private static String INSTALL_6 = "install6";
    private static String CALLBACK_6 = "callback6"; 
    
    // API testing
    private static String API_TEST = "apiTest";
    
    /** Creates a new instance of FriendsterAPIAction */
    public FriendsterAPIAction() {
    }
    
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (CALLBACK_1.equals(event) || CALLBACK_2.equals(event) || 
                CALLBACK_3.equals(event) || CALLBACK_4.equals(event) || 
                CALLBACK_5.equals(event) || CALLBACK_6.equals(event))
            return true; 
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        FriendsterForm friendsterForm = (FriendsterForm)actionForm; 
        if (CALLBACK_1.equals(action)) {
            errors = FriendsterAPIValidator.validateCallback1(errors, friendsterForm); 
        } else if (CALLBACK_3.equals(action)) {
            errors = FriendsterAPIValidator.validateCallback3(errors, friendsterForm); 
        } else if (CALLBACK_5.equals(action)) {
            errors = FriendsterAPIValidator.validateCallback5(errors, friendsterForm); 
        } else if (CALLBACK_6.equals(action)) {
            errors = FriendsterAPIValidator.validateCallback6(errors, friendsterForm); 
        } else {
            logger.debug("Temporary skip");
        }
        
        friendsterForm.getActionContext().setActionErrors(errors);
        return friendsterForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (CALLBACK_1.equals(action)) {
            action = INSTALL_1; 
        } 
        else if (CALLBACK_2.equals(action)) {
            action = INSTALL_2; 
        } 
        else if (CALLBACK_3.equals(action)) {
            action = INSTALL_3; 
        } 
        else if (CALLBACK_4.equals(action)) {
            action = INSTALL_4; 
        } 
        else if (CALLBACK_5.equals(action)) {
            action = INSTALL_5; 
        }
        else if (CALLBACK_6.equals(action)) {
            action = INSTALL_6; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doInstall1(FriendsterForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return INSTALL_1;
        } catch (Exception e) {
            return INSTALL_1;
        }
    }
    
    
    public String doCallback1(FriendsterForm actionForm)
    throws ApplicationException {
        try {
            
            UserFriendsterBO userFriendsterBO = new UserFriendsterBO();
            UserFriendsterVO userFriendsterVO = actionForm.getUserFriendsterVO();
            // Check userName availability, if exists, means validated and already installed
            if (StringUtils.hasValue(actionForm.getUserName()) 
                    && "n".equals(actionForm.getFirstInstall())
                    && !"y".equals(actionForm.getFromInstallPage())){
                logger.debug("Callback userName found for=" + actionForm.getUserName());
                userFriendsterVO = userFriendsterBO.updateCallbackViewsCounter(userFriendsterVO, FriendsterForm.MAIN_TYPE);
            }
            else{
            // Else userName not found means now is the install time
            // Call friendster API and link with the login user id 
                userFriendsterVO = userFriendsterBO.newCallback1Install(userFriendsterVO);
                if (StringUtils.hasValue(userFriendsterVO.getUserName())){
                    actionForm.setUserName(userFriendsterVO.getUserName());
                }
                else
                {
                    throw new Exception("Unable to get oEight User Name for friendster api Callback1 install");
                }
            }
            userFriendsterBO = null;
            logger.debug("Callback ok for " + actionForm.getUserName());
            return CALLBACK_1;
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof FriendsterException)
            {
                actionForm.getActionContext().getActionErrors().add("friendsterInput",new ActionError("common.invalid.friendsterAPI.connectionError"));
            }
            return INSTALL_1;
        }
    }
     
    public String doInstall3(FriendsterForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return INSTALL_3;
        } catch (Exception e) {
            return INSTALL_3;
        }
    }
    
    
    public String doCallback3(FriendsterForm actionForm)
    throws ApplicationException {
        try {
            
            UserFriendsterBO userFriendsterBO = new UserFriendsterBO();
            UserFriendsterVO userFriendsterVO = actionForm.getUserFriendsterVO();
            // Check userName availability, if exists, means validated and already installed
            if (StringUtils.hasValue(actionForm.getUserName()) 
                    && "n".equals(actionForm.getFirstInstall())
                    && !"y".equals(actionForm.getFromInstallPage())){
                logger.debug("Callback userName found for=" + actionForm.getUserName());
                userFriendsterVO = userFriendsterBO.updateCallbackViewsCounter(userFriendsterVO, FriendsterForm.FEATURED_BLOG_TYPE);
            }
            else{
                // Else userName not found means now is the install time
                // Call friendster API and link with the login user id 
                userFriendsterVO = userFriendsterBO.newCallback3Install(userFriendsterVO);
                if (StringUtils.hasValue(userFriendsterVO.getUserName())){
                    actionForm.setUserName(userFriendsterVO.getUserName());
                }
                else
                {
                    throw new Exception("Unable to get oEight User Name for friendster api Callback3 install");
                }
            }
            userFriendsterBO = null;
            logger.debug("Callback ok for " + actionForm.getUserName());
            return CALLBACK_3;
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof FriendsterException)
            {
                actionForm.getActionContext().getActionErrors().add("friendsterInput",new ActionError("common.invalid.friendsterAPI.connectionError"));
            }
            return INSTALL_3;
        }
    }
     
     
    public String doInstall5(FriendsterForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return INSTALL_5;
        } catch (Exception e) {
            return INSTALL_5;
        }
    }    
    
    public String doCallback5(FriendsterForm actionForm)
    throws ApplicationException {
        try {
            
            UserFriendsterBO userFriendsterBO = new UserFriendsterBO();
            UserFriendsterVO userFriendsterVO = actionForm.getUserFriendsterVO();
            // Check userName availability, if exists, means validated and already installed
            if (StringUtils.hasValue(actionForm.getUserName()) 
                    && "n".equals(actionForm.getFirstInstall())
                    && !"y".equals(actionForm.getFromInstallPage())){
                logger.debug("Callback userName found for=" + actionForm.getUserName());
                userFriendsterVO = userFriendsterBO.updateCallbackViewsCounter(userFriendsterVO, FriendsterForm.FRIENDS_XCHANGE_TYPE);
            }
            else{
            // Else userName not found means now is the install time
            // Call friendster API and link with the login user id 
                userFriendsterVO = userFriendsterBO.newCallback5Install(userFriendsterVO);
                if (StringUtils.hasValue(userFriendsterVO.getUserName())){
                    actionForm.setUserName(userFriendsterVO.getUserName());
                }
                else
                {
                    throw new Exception("Unable to get oEight User Name for friendster api Callback1 install");
                }
            }
            userFriendsterBO = null;
            logger.debug("Callback ok for " + actionForm.getUserName());
            return CALLBACK_5;
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof FriendsterException)
            {
                actionForm.getActionContext().getActionErrors().add("friendsterInput",new ActionError("common.invalid.friendsterAPI.connectionError"));
            }
            return INSTALL_5;
        }
    }
    
    protected String getDefaultActionName() {
                  
        return INSTALL_1;
    }
    
    public String doApiTest(FriendsterForm actionForm)
    throws ApplicationException { 
        try {
        
            UserFriendsterVO userFriendsterVO = actionForm.getUserFriendsterVO();
            UserFriendsterBO userFriendsterBO = new UserFriendsterBO();
            userFriendsterVO = userFriendsterBO.testFriendsterAPI(userFriendsterVO);
            
            // Print birthday
            userFriendsterBO.printBirthdays();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return API_TEST;
    }
    
    
       
    public String doInstall6(FriendsterForm actionForm)
    throws ApplicationException {
        try {  
            return INSTALL_6;
        } catch (Exception e) {
            return INSTALL_6;
        }
    }
    
    
    public String doCallback6(FriendsterForm actionForm)
    throws ApplicationException {
        try {

            UserFriendsterBO userFriendsterBO = new UserFriendsterBO();
            UserFriendsterVO userFriendsterVO = actionForm.getUserFriendsterVO();
            // Check from pledge table, does friendster id exists
            if (StringUtils.hasValue(actionForm.getUser_id()) 
                    && userFriendsterBO.hasPledge(actionForm.getUser_id())
                    && "n".equals(actionForm.getFirstInstall())
                    && !"y".equals(actionForm.getFromInstallPage())){
                logger.debug("Callback userName found for=" + actionForm.getUserName());
                userFriendsterVO = userFriendsterBO.updateCallbackViewsCounter(userFriendsterVO, FriendsterForm.CLIMATE_CHANGE_TYPE);
            }
            else{
                // Else user_id not found means now is the install time
                // Call friendster API and link with the login user id 
                userFriendsterVO = userFriendsterBO.newCallback6Install(userFriendsterVO, actionForm.getPledgeCode());
                
                // After that we remove the session value? - Temporary NO, check back later
            }
            userFriendsterBO = null;
            logger.debug("Callback ok for " + actionForm.getUser_id());
            return CALLBACK_6;
        } catch (Exception e) {
            e.printStackTrace();
            if (e instanceof FriendsterException)
            {
                actionForm.getActionContext().getActionErrors().add("friendsterInput",new ActionError("common.invalid.friendsterAPI.connectionError"));
            }
            return INSTALL_6;
        }
    }
     
    
}
