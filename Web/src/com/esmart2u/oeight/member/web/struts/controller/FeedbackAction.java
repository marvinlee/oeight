/*
 * FeedbackAction.java
 *
 * Created on January 14, 2008, 8:37 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.Feedback;
import com.esmart2u.oeight.member.bo.FeedbackBO;
import com.esmart2u.solution.base.helper.ApplicationException;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class FeedbackAction  extends AbstractApplicationAction {
     
    private static String MAIN = "main";
    private static String SUBMITTED = "submitted";
     
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(FeedbackAction.class);
    
    
    /** Creates a new instance of FeedbackAction */
    public FeedbackAction() {
    }
     
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (SUBMITTED.equals(event))
            return true;
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        FeedbackForm feedbackForm = (FeedbackForm)actionForm; 
        if (SUBMITTED.equals(action)) {
            errors = FeedbackValidator.validateFeedback(errors, feedbackForm); 
        } else {
            logger.debug("Temporary skip");
        }
        
        feedbackForm.getActionContext().setActionErrors(errors);
        return feedbackForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (SUBMITTED.equals(action)) {
            action = MAIN; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doMain(FeedbackForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return MAIN;
        } catch (Exception e) {
            return MAIN;
        }
    }
    
    
    public String doSubmitted(FeedbackForm actionForm)
    throws ApplicationException {
        try {
            try {
                
                // Get User Id from session and get details
                String userId = checkUserAccess(actionForm);
                actionForm.setUserId(Long.parseLong(userId));
            } catch (Exception ex) {
                logger.debug("Feedback not from login user");
                actionForm.setUserId(0);
            }
            
            // Save into DB 
            Feedback feedback = new Feedback();
            feedback.setUserId(actionForm.getUserId());
            feedback.setName(actionForm.getName());
            feedback.setCompanyName(actionForm.getCompanyName());
            feedback.setEmail(actionForm.getEmail());
            feedback.setSubject(actionForm.getSubject());
            feedback.setMessage(actionForm.getMessage()); 
            feedback.setDateSent(new Date()); 
    
            FeedbackBO feedbackBO = new FeedbackBO();
            feedbackBO.saveFeedback(feedback);
            feedbackBO = null;
          
            return SUBMITTED;
        } catch (Exception e) {
            e.printStackTrace();
            return MAIN;
        }
    }
    
    protected String getDefaultActionName() {
                  
        return MAIN;
    }
}
