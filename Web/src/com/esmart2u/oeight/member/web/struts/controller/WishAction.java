/*
 * WishAction.java
 *
 * Created on November 30, 2007, 2:31 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserWishers;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserTrackBO;
import com.esmart2u.oeight.member.bo.UserWishesBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.WishVO;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class WishAction extends AbstractApplicationAction {
    
    private static String MAIN_PAGE = "main";
    private static String MAIN_SUBMITTED_PAGE = "mainSubmitted";
    private static String LIST_PAGE = "list";
    private static String LIST_SUBMITTED_PAGE = "listSubmitted";
    private static String LOGIN = "login"; 
    private static String RELOGIN = "relogin";
    private static String NOT_FOUND = "not_found";
    private static String NOT_VALIDATED = "notValidated";
    
    // Friend list
    private static String MY_FRENS_LIST_PAGE = "myFrens";
    private static String FRENS_LIST_PAGE = "frens";
    
    // Add friend
    private static String REQUEST_FREN = "requestFren";
    private static String ADD_FREN = "addFren";
    private static String CONFIRM_FREN = "confirmFren";
    private static String DELETE_FREN = "deleteFren";
    private static String DELETE_FREN_SUBMITTED = "deleteFrenSubmitted";
    private static String FREN_MESSAGE_PAGE = "message";
    
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(WishAction.class);
    
    public String doMain(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm); 
              
            // Get user wishes
            UserWishesBO userWishesBO = new UserWishesBO();
            List wishesList = userWishesBO.getUserWishersList(userId); 
            actionForm.setWishersList(wishesList);
            
            userWishesBO = null;  
            
            return MAIN_PAGE;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    } 
      
    public String doMainSubmitted(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
         
            // Error if person trying to delete is not owner
            UserWishesBO userWishesBO = new UserWishesBO();
            UserWishers userWishers = (UserWishers)userWishesBO.getUserWishersById(actionForm.getWishId());
            //long wishId = userWishers.getWishId(); 
            if (!userId.equals(Long.toString(userWishers.getUserId())))
            {
                return MAIN_PAGE;
            }
            
            // Proceed for deletion
            userWishesBO.deleteWish(userWishers);
            
            return doMain(actionForm);
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    } 
    
    public String doList(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            String loginUserId = userId;
            
            UserBO userBO = new UserBO();
            User user = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
                
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }   
                
                userId = Long.toString(user.getUserId());
                        
            } else {
                // Get user main details
                user = userBO.getUserById(userId);
                
            }
            
         
            
            // Get user wishes
            UserWishesBO userWishesBO = new UserWishesBO();
            List wishesList = userWishesBO.getUserWishersList(userId);   

            // A key to identify is viewing own profile page
            if (loginUserId.equals(userId))
            {
                actionForm.setViewingOwnProfile(true);
            }
           
            actionForm.setMyWish(user.getUserCountry().getMyWish());
            actionForm.setWishersList(wishesList);
            
            userWishesBO = null; 
            userBO = null;
            
            return LIST_PAGE;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    }  
    
      
    public String doListSubmitted(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String wisherId = checkUserAccess(actionForm);
            String userName = actionForm.getUserName();
            
            UserBO userBO = new UserBO();
            User wisher = userBO.getUserById(wisherId);

            // Sending wish/message needs validation
            if (PropertyConstants.BOOLEAN_YES != wisher.getUserCountry().getValidated())
            {
                actionForm.setViewerUserEmail(wisher.getEmailAddress());
                actionForm.getActionContext().getRequest().setAttribute("viewerUserEmail", wisher.getEmailAddress());
                return NOT_VALIDATED;
            } 
            userBO = null;
            
            // Insert into DB
            UserWishesBO userWishesBO = new UserWishesBO();
            userWishesBO.insertUserWishes(userName, wisherId, actionForm.getWishContent());
            userWishesBO = null;
            actionForm.setWishContent("");
            
            return doList(actionForm);
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    } 
    
      
    public String doFrens(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            String loginUserId = userId;
            
            UserBO userBO = new UserBO();
            User user = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
                
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }   
                
                userId = Long.toString(user.getUserId());
                        
            } else {
                // Get user main details
                user = userBO.getUserById(userId);
                
            }
            
         
            actionForm.setUserName(user.getUserName());
                    
            // Get user wishes
            UserWishesBO userWishesBO = new UserWishesBO();
            List friendsList = null; 
            
            // A key to identify is viewing own profile page
            if (loginUserId.equals(userId))
            {
                actionForm.setViewingOwnProfile(true);
                friendsList = userWishesBO.getUserFriendsList(userId);
            } 
            else
            { 
                friendsList = userWishesBO.getUserConfirmedFriendsList(userId);
                
                // UPDATE!! method created above is used now
                // If user is not using own profile, then we need to filter out pending confirmation
                /*List filteredList = new ArrayList();
                if (friendsList != null)
                {
                    for(int i=0; i<friendsList.size();i++)
                    { 
                        WishVO wishVO = (WishVO)friendsList.get(i);
                        if (OEightConstants.BUDDY_IS_FREN == wishVO.getStatus())
                        {
                            filteredList.add(wishVO);
                        }
                    }
                }
                friendsList = filteredList;*/
                
            }
            
            
            actionForm.setFriendsList(friendsList);
            
            userWishesBO = null; 
            userBO = null;
            
            return FRENS_LIST_PAGE;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    }  
    
  
      
    public String doMyFrens(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
             
            UserBO userBO = new UserBO(); 
            // Get user main details
            User user = userBO.getUserById(userId);
            actionForm.setUserName(user.getUserName());
            
            // Get user wishes
            UserWishesBO userWishesBO = new UserWishesBO();
            List friendsList = userWishesBO.getUserFriendsList(userId); 
            actionForm.setFriendsList(friendsList);
            
            userWishesBO = null; 
            userBO = null;
            
            return MY_FRENS_LIST_PAGE;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    }      
      
    public String doRequestFren(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            String loginUserId = userId; 
            
            UserBO userBO = new UserBO(); 
            User user = null;
             // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
                
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }    
                userId = Long.toString(user.getUserId());
                        
            } 
            
             
            if (loginUserId.equals(userId))
            {
                // Cannot add yourself
                actionForm.setViewingOwnProfile(true);
            }  
            
            // Get user main details 
            actionForm.setUserName(user.getUserName()); 
            actionForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            
            userBO = null;
            
            return REQUEST_FREN;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    }      
    
      
    public String doAddFren(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            String loginUserId = userId; 
            
            UserBO userBO = new UserBO(); 
            User user = null;
             // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
                
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }    
                userId = Long.toString(user.getUserId());
                        
            } 
            
             
            if (loginUserId.equals(userId))
            {
                // Cannot add yourself
                actionForm.setViewingOwnProfile(true);
            } 
            else
            { 
                // Add friend
                UserWishesBO.addRequestFriend(loginUserId, userId);
                UserTrackBO.increaseTrackValue(loginUserId, OEightConstants.TRACK_ADD_BUDDY);
                // Insert for notification
            }
            
            // Get user main details 
            actionForm.setUserName(user.getUserName()); 
            actionForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            
            userBO = null;
            
            return ADD_FREN;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    }      
    
    
    public String doConfirmFren(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            String loginUserId = userId; 
            
            UserBO userBO = new UserBO(); 
            User user = null;
             // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
                
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }    
                userId = Long.toString(user.getUserId());
                        
            } 
            
             
            if (loginUserId.equals(userId))
            {
                // Cannot add yourself
                actionForm.setViewingOwnProfile(true);
            } 
            else
            { 
                // Add friend
                UserWishesBO.confirmRequestFriend(loginUserId, userId);
                // Insert for notification
            }
            
            // Get user main details 
            actionForm.setUserName(user.getUserName()); 
            actionForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            
            userBO = null;
            
            return CONFIRM_FREN;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    }      
     
    public String doDeleteFren(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            String loginUserId = userId; 
            
            UserBO userBO = new UserBO(); 
            User user = null;
             // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
                
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }    
                userId = Long.toString(user.getUserId());
                        
            } 
            
             
            if (loginUserId.equals(userId))
            {
                // Cannot add yourself
                actionForm.setViewingOwnProfile(true);
            }  
            
            // Get user main details 
            actionForm.setUserName(user.getUserName()); 
            actionForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            
            userBO = null;
            
            return DELETE_FREN;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    }      
       
    public String doDeleteFrenSubmitted(WishForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            String loginUserId = userId; 
            
            UserBO userBO = new UserBO(); 
            User user = null;
             // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(actionForm.getUserName())) {
                
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }    
                userId = Long.toString(user.getUserId());
                        
            }  
            
             
            if (loginUserId.equals(userId))
            {
                // Cannot add yourself
                actionForm.setViewingOwnProfile(true);
            } 
            else
            { 
                // Delete friend
                UserWishesBO.deleteFriend(loginUserId, userId);
            }
            
            // Get user main details 
            actionForm.setUserName(user.getUserName()); 
            actionForm.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            
            userBO = null;
            
            return DELETE_FREN_SUBMITTED;
        } catch (Exception e) { 
            //return LOGIN;
            return RELOGIN;
        }
    }      
    
    public String doMessage(WishForm actionForm)
    throws ApplicationException {  
        return MESSAGE_PAGE;
    }
    
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (LIST_SUBMITTED_PAGE.equals(event)|| MAIN_SUBMITTED_PAGE.equals(event)
            ||REQUEST_FREN.equals(event)|| ADD_FREN.equals(event) || CONFIRM_FREN.equals(event)
            || DELETE_FREN.equals(event) || DELETE_FREN_SUBMITTED.equals(event))
            return true;
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        WishForm wishForm = (WishForm)actionForm; 
        if (LIST_SUBMITTED_PAGE.equals(action)) {
            errors = WishValidator.validateListSubmit(errors, wishForm); 
        } else if (MAIN_SUBMITTED_PAGE.equals(action)) {
            errors = WishValidator.validateMainSubmit(errors, wishForm);
        } else if (REQUEST_FREN.equals(action)) {
            errors = WishValidator.validateRequestFriend(errors, wishForm);
        } else if (ADD_FREN.equals(action)) {
            errors = WishValidator.validateRequestFriend(errors, wishForm);
        } else if (CONFIRM_FREN.equals(action)) {
            errors = WishValidator.validateConfirmFriend(errors, wishForm);
        } else if (DELETE_FREN.equals(action)) {
            errors = WishValidator.validateDeleteFriend(errors, wishForm);
        } else if (DELETE_FREN_SUBMITTED.equals(action)) {
            errors = WishValidator.validateDeleteFriend(errors, wishForm);
        } else {
            logger.debug("Temporary skip");
        }
        
        wishForm.getActionContext().setActionErrors(errors);
        return wishForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (LIST_SUBMITTED_PAGE.equals(action)) {
            action = LIST_PAGE;
        } else if (MAIN_SUBMITTED_PAGE.equals(action)) {
            action = MAIN_PAGE;
        } else if (REQUEST_FREN.equals(action)) {
            action = FREN_MESSAGE_PAGE;
        } else if (ADD_FREN.equals(action)) {
            action = FREN_MESSAGE_PAGE;
        } else if (CONFIRM_FREN.equals(action)) {
            action = FREN_MESSAGE_PAGE;
        } else if (DELETE_FREN.equals(action)) {
            action = DELETE_FREN;
        } else if (DELETE_FREN_SUBMITTED.equals(action)) {
            action = DELETE_FREN;
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
     
}
