/*
 * ProfileMapper.java
 *
 * Created on October 7, 2007, 1:19 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.oeight.admin.logging.AdminLogger;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.UserDetails;
import com.esmart2u.oeight.data.UserPictures;
import com.esmart2u.oeight.data.phpbb.PhpBbUsers;
import com.esmart2u.oeight.member.bo.ForumBO;
import com.esmart2u.oeight.member.web.struts.controller.ProfileForm;
import com.esmart2u.oeight.member.web.struts.controller.SearchForm;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import com.ibm.icu.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class ProfileMapper { 
 
    private static Logger logger = Logger.getLogger(ProfileMapper.class);
    
    /** Creates a new instance of ProfileMapper */
    public ProfileMapper() {
    }
    
    public static User mapUserProfileMain(ProfileForm actionForm){
        User user = new User();
        user.setUserId(actionForm.getUserId());
        user.setUserName(actionForm.getUserName());
        
        UserCountry userCountry = new UserCountry();
        userCountry.setUserId(actionForm.getUserId());
        userCountry.setName(actionForm.getName());
        userCountry.setNric(actionForm.getNric());
        userCountry.setGender(actionForm.getGender());
        userCountry.setMaritalStatus(actionForm.getMaritalStatus());
        userCountry.setBirthDate(actionForm.getBirthDate());
        userCountry.setCity(actionForm.getCity());
        userCountry.setState(actionForm.getState());
        userCountry.setNationality(actionForm.getNationality());
        userCountry.setCurrentLocation(actionForm.getCurrentLocation());
        userCountry.setSchools(actionForm.getSchools());
        userCountry.setOccupation(actionForm.getOccupation());
        userCountry.setCompany(actionForm.getCompany());
        userCountry.setPublicEmail(actionForm.getPublicEmail());
        
        userCountry.setPhotoSmallPath(actionForm.getPhotoSmallPath());
        userCountry.setPhotoLargePath(actionForm.getPhotoLargePath());
        userCountry.setPhotoDescription(actionForm.getPhotoDescription());
        
        userCountry.setMyWish(actionForm.getMyWish());
        userCountry.setProfilePreference(actionForm.getProfilePreference());
        userCountry.setSubscribe(actionForm.getSubscribe());
        
        user.setUserCountry(userCountry);
        return user;
    }

    public static User updateUserProfileMain(User dbUser, User editedUser) {
        UserCountry editedUserCountry = editedUser.getUserCountry(); 
        
        UserCountry userCountry = dbUser.getUserCountry();
        
        // For forum updates
        boolean editedBirthDate = userCountry.getBirthDate().getTime() != editedUserCountry.getBirthDate().getTime();
        boolean editedCurrentLocation = (editedUserCountry.getCurrentLocation() != null && !editedUserCountry.getCurrentLocation().equals(userCountry.getCurrentLocation()));
        boolean editedOccupation = (editedUserCountry.getOccupation() != null && !editedUserCountry.getOccupation().equals(userCountry.getOccupation()));
        
        userCountry.setName(editedUserCountry.getName());
        userCountry.setNric(editedUserCountry.getNric());
        userCountry.setGender(editedUserCountry.getGender());
        userCountry.setMaritalStatus(editedUserCountry.getMaritalStatus());
        userCountry.setBirthDate(editedUserCountry.getBirthDate());
        userCountry.setCity(editedUserCountry.getCity());
        userCountry.setState(editedUserCountry.getState());
        userCountry.setNationality(editedUserCountry.getNationality());
        userCountry.setCurrentLocation(editedUserCountry.getCurrentLocation());
        userCountry.setSchools(editedUserCountry.getSchools());
        userCountry.setOccupation(editedUserCountry.getOccupation());
        userCountry.setCompany(editedUserCountry.getCompany());
        userCountry.setPublicEmail(editedUserCountry.getPublicEmail());
        
        if (StringUtils.hasValue(editedUserCountry.getPhotoLargePath())){
            userCountry.setPhotoSmallPath(editedUserCountry.getPhotoSmallPath());
            userCountry.setPhotoLargePath(editedUserCountry.getPhotoLargePath());
            userCountry.setPhotoDescription(editedUserCountry.getPhotoDescription());
        }
        userCountry.setMyWish(editedUserCountry.getMyWish());
        userCountry.setProfilePreference(editedUserCountry.getProfilePreference()); 
        if ( userCountry.getSubscribe() == PropertyConstants.BOOLEAN_YES &&
                editedUserCountry.getSubscribe() == PropertyConstants.BOOLEAN_NO)
        {
            AdminLogger.notifyAdmin(userCountry.getName() + " unsubscribed newsletter.");
        }
        userCountry.setSubscribe(editedUserCountry.getSubscribe());
        
        dbUser.setUserCountry(userCountry);
        
        // If dob and current location has changed, update Forum values
        if ((editedBirthDate)//DOB is mandatory field
        || (editedCurrentLocation)
        || (editedOccupation))
        {
            PhpBbUsers forumUser = new PhpBbUsers();
            forumUser.setUserName(userCountry.getUser().getUserName());
            logger.debug("Going to update forum user details for:" + forumUser.getUserName());
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String birthdayString = sdf.format(editedUserCountry.getBirthDate());
            forumUser.setUserBirthday(birthdayString);
            String editedUserLocation = StringUtils.hasValue(editedUserCountry.getCurrentLocation())? editedUserCountry.getCurrentLocation() : "";
            forumUser.setUserFrom(editedUserLocation);
            String editedUserOccupation = StringUtils.hasValue(editedUserCountry.getOccupation())? editedUserCountry.getOccupation() : "";
            forumUser.setUserOcc(editedUserOccupation);
            ForumBO forumBO = new ForumBO();
            forumBO.updateForumUserPersonalDetails(forumUser);
        }
        
        return dbUser;
    }
    
    public static List updateUserProfilePhotos(List dbPhotoList, List updatedPhotoList)
    {
        // Check updatePhotoList and make changes to dbPhotoList
        //logger.debug("updateUserProfilePhotos");
        for(int i=0;i<updatedPhotoList.size();i++)
        {
            //logger.debug("updatedPhotoList["+i+"]");
            UserPictures updatedPhoto = (UserPictures)updatedPhotoList.get(i);
            boolean found = false;
            for(int j=0;j<dbPhotoList.size();j++)
            { 
                //logger.debug("dbPhotoList["+j+"]");
                UserPictures dbPhoto = (UserPictures)dbPhotoList.get(j);
                if (dbPhoto.getPhotoType() == updatedPhoto.getPhotoType())
                {
                    found = true;
                    dbPhoto.setCaption(updatedPhoto.getCaption());
                    
                    // If photo is changed/ uploaded
                    if (StringUtils.hasValue(updatedPhoto.getPhotoLarge())){
                        dbPhoto.setPhotoLarge(updatedPhoto.getPhotoLarge());
                        dbPhoto.setDateSubmitted(updatedPhoto.getDateSubmitted());
                        dbPhoto.setModerated(updatedPhoto.getModerated()); 
                        dbPhoto.setVersion(dbPhoto.getVersion()+1);
                    }
                    
                    //todo housekeep photos removed
                
                }
            }
            if (!found)
            {
                //logger.debug("New photo is added " + updatedPhoto.getPhotoType());
                // This is a new photo
                dbPhotoList.add(updatedPhoto);
            }
        }
    
        return dbPhotoList;
    }
    
    /*  Lifestyle
     *
     */
    public static UserDetails mapUserProfileLifestyle(ProfileForm actionForm){ 
        
        UserDetails userDetails = new UserDetails(); 
        userDetails.setLifeSmoker(actionForm.getLifeSmoker());
        userDetails.setLifeDrinker(actionForm.getLifeDrinker());
        userDetails.setLifeAboutMe(actionForm.getLifeAboutMe());
        userDetails.setLifeToKnow(actionForm.getLifeToKnow());
        userDetails.setLifeKnowMe(actionForm.getLifeKnowMe());
        userDetails.setLife1(actionForm.getLife1());
        userDetails.setLife2(actionForm.getLife2());
        userDetails.setLife3(actionForm.getLife3());
        userDetails.setLife4(actionForm.getLife4());
        userDetails.setLife5(actionForm.getLife5()); 
        userDetails.setLifeValue1(actionForm.getLifeValue1());
        userDetails.setLifeValue2(actionForm.getLifeValue2());
        userDetails.setLifeValue3(actionForm.getLifeValue3());
        userDetails.setLifeValue4(actionForm.getLifeValue4());
        userDetails.setLifeValue5(actionForm.getLifeValue5());  
         
        return userDetails;
    }

    /*  Lifestyle
     *
     */
    public static UserDetails updateUserProfileLifestyle(UserDetails dbUserDetails, UserDetails editedUserDetails) {
         
        dbUserDetails.setLifeSmoker(editedUserDetails.getLifeSmoker());
        dbUserDetails.setLifeDrinker(editedUserDetails.getLifeDrinker());
        dbUserDetails.setLifeAboutMe(editedUserDetails.getLifeAboutMe());
        dbUserDetails.setLifeToKnow(editedUserDetails.getLifeToKnow());
        dbUserDetails.setLifeKnowMe(editedUserDetails.getLifeKnowMe());
        dbUserDetails.setLife1(editedUserDetails.getLife1());
        dbUserDetails.setLife2(editedUserDetails.getLife2());
        dbUserDetails.setLife3(editedUserDetails.getLife3());
        dbUserDetails.setLife4(editedUserDetails.getLife4());
        dbUserDetails.setLife5(editedUserDetails.getLife5()); 
        dbUserDetails.setLifeValue1(editedUserDetails.getLifeValue1());
        dbUserDetails.setLifeValue2(editedUserDetails.getLifeValue2());
        dbUserDetails.setLifeValue3(editedUserDetails.getLifeValue3());
        dbUserDetails.setLifeValue4(editedUserDetails.getLifeValue4());
        dbUserDetails.setLifeValue5(editedUserDetails.getLifeValue5());  
          
        
        return dbUserDetails;
    }
    
    
    /*  Interest
     *
     */
    public static UserDetails mapUserProfileInterest(ProfileForm actionForm){  
        
        UserDetails userDetails = new UserDetails(); 
        userDetails.setInterestActor(actionForm.getInterestActor());
        userDetails.setInterestActress(actionForm.getInterestActress());
        userDetails.setInterestSinger(actionForm.getInterestSinger());
        userDetails.setInterestMusic(actionForm.getInterestMusic());
        userDetails.setInterestBand(actionForm.getInterestBand());
        userDetails.setInterestMovies(actionForm.getInterestMovies());
        userDetails.setInterestBooks(actionForm.getInterestBooks());
        userDetails.setInterestSports(actionForm.getInterestSports());
        userDetails.setInterestWebsites(actionForm.getInterestWebsites());
        userDetails.setInterest1(actionForm.getInterest1());
        userDetails.setInterest2(actionForm.getInterest2());
        userDetails.setInterest3(actionForm.getInterest3());
        userDetails.setInterest4(actionForm.getInterest4());
        userDetails.setInterest5(actionForm.getInterest5()); 
        userDetails.setInterestValue1(actionForm.getInterestValue1());
        userDetails.setInterestValue2(actionForm.getInterestValue2());
        userDetails.setInterestValue3(actionForm.getInterestValue3());
        userDetails.setInterestValue4(actionForm.getInterestValue4());
        userDetails.setInterestValue5(actionForm.getInterestValue5());  
         
        return userDetails;
    }

    /*  Interest
     *
     */
    public static UserDetails updateUserProfileInterest(UserDetails dbUserDetails, UserDetails editedUserDetails) {
         
        dbUserDetails.setInterestActor(editedUserDetails.getInterestActor());
        dbUserDetails.setInterestActress(editedUserDetails.getInterestActress());
        dbUserDetails.setInterestSinger(editedUserDetails.getInterestSinger());
        dbUserDetails.setInterestMusic(editedUserDetails.getInterestMusic());
        dbUserDetails.setInterestBand(editedUserDetails.getInterestBand());
        dbUserDetails.setInterestMovies(editedUserDetails.getInterestMovies());
        dbUserDetails.setInterestBooks(editedUserDetails.getInterestBooks());
        dbUserDetails.setInterestSports(editedUserDetails.getInterestSports());
        dbUserDetails.setInterestWebsites(editedUserDetails.getInterestWebsites());
        dbUserDetails.setInterest1(editedUserDetails.getInterest1());
        dbUserDetails.setInterest2(editedUserDetails.getInterest2());
        dbUserDetails.setInterest3(editedUserDetails.getInterest3());
        dbUserDetails.setInterest4(editedUserDetails.getInterest4());
        dbUserDetails.setInterest5(editedUserDetails.getInterest5()); 
        dbUserDetails.setInterestValue1(editedUserDetails.getInterestValue1());
        dbUserDetails.setInterestValue2(editedUserDetails.getInterestValue2());
        dbUserDetails.setInterestValue3(editedUserDetails.getInterestValue3());
        dbUserDetails.setInterestValue4(editedUserDetails.getInterestValue4());
        dbUserDetails.setInterestValue5(editedUserDetails.getInterestValue5());   
        
        return dbUserDetails;
    }
    
      
    
    /*  Skills
     *
     */
    public static UserDetails mapUserProfileSkills(ProfileForm actionForm){  
        
        UserDetails userDetails = new UserDetails(); 
        userDetails.setSkillsExpert(actionForm.getSkillsExpert());
        userDetails.setSkillsGood(actionForm.getSkillsGood());
        userDetails.setSkillsLearn(actionForm.getSkillsLearn()); 
         
        return userDetails;
    }

    /*  Skills
     *
     */
    public static UserDetails updateUserProfileSkills(UserDetails dbUserDetails, UserDetails editedUserDetails) {
         
        dbUserDetails.setSkillsExpert(editedUserDetails.getSkillsExpert());
        dbUserDetails.setSkillsGood(editedUserDetails.getSkillsGood());
        dbUserDetails.setSkillsLearn(editedUserDetails.getSkillsLearn()); 
         
        return dbUserDetails;
    }
    
    
    /*  Contact
     *
     */
    public static UserDetails mapUserProfileContact(ProfileForm actionForm){  
          
        UserDetails userDetails = new UserDetails(); 
        userDetails.setContactPhone(actionForm.getContactPhone());
        userDetails.setContactGoogle(actionForm.getContactGoogle());
        userDetails.setContactYahoo(actionForm.getContactYahoo()); 
        userDetails.setContactMsn(actionForm.getContactMsn()); 
        userDetails.setWebFriendster(actionForm.getWebFriendster()); 
        userDetails.setWebHi5(actionForm.getWebHi5()); 
        userDetails.setWebWayn(actionForm.getWebWayn()); 
        userDetails.setWebOrkut(actionForm.getWebOrkut()); 
        userDetails.setWebFacebook(actionForm.getWebFacebook()); 
        userDetails.setWebMyspace(actionForm.getWebMyspace()); 
        userDetails.setWebReserved1(actionForm.getWebReserved1()); 
        userDetails.setWebReserved2(actionForm.getWebReserved2()); 
        userDetails.setWebReserved3(actionForm.getWebReserved3()); 
        userDetails.setWebBlog(actionForm.getWebBlog()); 
        userDetails.setWebBlogFeature(actionForm.getWebBlogFeature());
        userDetails.setWeb1(actionForm.getWeb1()); 
        userDetails.setWeb2(actionForm.getWeb2()); 
        userDetails.setWeb3(actionForm.getWeb3()); 
        userDetails.setWeb4(actionForm.getWeb4()); 
        userDetails.setWeb5(actionForm.getWeb5()); 
        userDetails.setWebValue1(actionForm.getWebValue1()); 
        userDetails.setWebValue2(actionForm.getWebValue2()); 
        userDetails.setWebValue3(actionForm.getWebValue3()); 
        userDetails.setWebValue4(actionForm.getWebValue4()); 
        userDetails.setWebValue5(actionForm.getWebValue5());  
      
        
        return userDetails;
    }

    /*  Contact
     *
     */
    public static UserDetails updateUserProfileContact(UserDetails dbUserDetails, UserDetails editedUserDetails) {
         
        //System.out.println("Running updateUserProfileContact");
        boolean editedContactYahoo = (editedUserDetails.getContactYahoo() != null && !editedUserDetails.getContactYahoo().equals(dbUserDetails.getContactYahoo()));
        //System.out.println("editedContactYahoo " + editedContactYahoo);
        boolean editedContactMsn = (editedUserDetails.getContactMsn() != null && !editedUserDetails.getContactMsn().equals(dbUserDetails.getContactMsn()));
        //System.out.println("editedContactMsn " + editedContactMsn);
        boolean editedWebBlog = (editedUserDetails.getWebBlog() != null && !editedUserDetails.getWebBlog().equals(dbUserDetails.getWebBlog())); 
        //System.out.println("editedWebBlog " + editedWebBlog);
                
        dbUserDetails.setContactPhone(editedUserDetails.getContactPhone());
        dbUserDetails.setContactGoogle(editedUserDetails.getContactGoogle());
        dbUserDetails.setContactYahoo(editedUserDetails.getContactYahoo()); 
        dbUserDetails.setContactMsn(editedUserDetails.getContactMsn()); 
        dbUserDetails.setWebFriendster(editedUserDetails.getWebFriendster()); 
        dbUserDetails.setWebHi5(editedUserDetails.getWebHi5()); 
        dbUserDetails.setWebWayn(editedUserDetails.getWebWayn()); 
        dbUserDetails.setWebOrkut(editedUserDetails.getWebOrkut()); 
        dbUserDetails.setWebFacebook(editedUserDetails.getWebFacebook()); 
        dbUserDetails.setWebMyspace(editedUserDetails.getWebMyspace()); 
        dbUserDetails.setWebReserved1(editedUserDetails.getWebReserved1()); 
        dbUserDetails.setWebReserved2(editedUserDetails.getWebReserved2()); 
        dbUserDetails.setWebReserved3(editedUserDetails.getWebReserved3()); 
        /* Not used, but blog is managed seperately
        dbUserDetails.setWebBlog(editedUserDetails.getWebBlog()); 
        // If user turn on Feature blog from off, notify admin
        if (!dbUserDetails.getWebBlogFeature() && editedUserDetails.getWebBlogFeature() )
        {
            AdminLogger.notifyAdmin("Feature Blog Updated " + dbUserDetails.getWebBlog());
        }
        dbUserDetails.setWebBlogFeature(editedUserDetails.getWebBlogFeature());
        */
        dbUserDetails.setWeb1(editedUserDetails.getWeb1()); 
        dbUserDetails.setWeb2(editedUserDetails.getWeb2()); 
        dbUserDetails.setWeb3(editedUserDetails.getWeb3()); 
        dbUserDetails.setWeb4(editedUserDetails.getWeb4()); 
        dbUserDetails.setWeb5(editedUserDetails.getWeb5()); 
        dbUserDetails.setWebValue1(editedUserDetails.getWebValue1()); 
        dbUserDetails.setWebValue2(editedUserDetails.getWebValue2()); 
        dbUserDetails.setWebValue3(editedUserDetails.getWebValue3()); 
        dbUserDetails.setWebValue4(editedUserDetails.getWebValue4()); 
        dbUserDetails.setWebValue5(editedUserDetails.getWebValue5());  
        
        //System.out.println("checking edited for forum");
        try{
            // If msn, yahoo, blog has changed, update Forum values
            if ((editedContactYahoo)
            || (editedContactMsn))
            //|| (editedWebBlog))
            {
                PhpBbUsers forumUser = new PhpBbUsers();
                forumUser.setUserName(dbUserDetails.getUserCountry().getUser().getUserName()); 
                logger.debug("Going to update forum user contact for:" + forumUser.getUserName());
                String editedYahoo = StringUtils.hasValue(editedUserDetails.getContactYahoo())? editedUserDetails.getContactYahoo() : "";
                forumUser.setUserYim(editedYahoo);
                String editedMsn = StringUtils.hasValue(editedUserDetails.getContactMsn())? editedUserDetails.getContactMsn() : "";
                forumUser.setUserMsnm(editedMsn);
                //String editedWebBlogUrl = StringUtils.hasValue(editedUserDetails.getWebBlog())? editedUserDetails.getWebBlog() : "";
                //forumUser.setUserWebsite(editedWebBlogUrl);
                ForumBO forumBO = new ForumBO();
                forumBO.updateForumUserPersonalContacts(forumUser);
            }
        }catch (Throwable t)
        {
            t.printStackTrace();
        }
        
        return dbUserDetails;
    }
    
    /**
     *  Maps SearchForm to UserCountry to search.
     */
    public static UserCountry mapUserSearchObject(SearchForm searchForm)
    { 
        UserCountry userCountry = new UserCountry(); 
        userCountry.setName(searchForm.getName()); 
        userCountry.setGender(searchForm.getGender()); 
        userCountry.setFromAge(searchForm.getFromAge());
        userCountry.setToAge(searchForm.getToAge());
        userCountry.setCity(searchForm.getCity());
        userCountry.setState(searchForm.getState());
        userCountry.setNationality(searchForm.getNationality());
        userCountry.setCurrentLocation(searchForm.getCurrentLocation()); 
        userCountry.setPublicEmail(searchForm.getPublicEmail());
        userCountry.setSearchLifestyle(searchForm.getLifestyle());
        userCountry.setSearchInterest(searchForm.getInterest());
        userCountry.setSearchSkills(searchForm.getSkills());
        
        return userCountry;
    }
    
}
