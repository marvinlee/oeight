/*
 * WeddingAction.java
 *
 * Created on June 25, 2008, 10:01 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.OEightWedding;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.WeddingBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.Page;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PhotoFile;
import com.esmart2u.solution.base.helper.PhotoUploadHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.logging.Logger;
import java.util.List;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
 

/**
 *
 * @author meauchyuan.lee
 */
public class WeddingAction  extends AbstractApplicationAction {
     
    private static String MOSAIC = "mosaic";
    private static String ABOUT = "about";
    private static String REGISTER = "register";
    private static String REGISTER_SUBMITTED = "registerSubmitted"; 
    private static String RESOURCES = "resources"; 
    private static String WEDDING_LOGIN = "weddingLogin";
    private static String LIST = "list";
    private static String VIEW = "view";
    private static String PAGE = "page";
    private static String PAGE_SAVE = "pageSave"; //Action from user clicking Save in PAGE
    private static String PAGE_SPECIAL = "pageSpecial";
    private static String PAGE_EXCLUSIVE = "pageExclusive";
    
    // Common page
    private static String NOT_FOUND_PAGE = "notFound";
    
    // Admin
    private static String MARVIN_LIST = "marvinList";
    private static String MARVIN_LIST_UPDATE = "marvinListUpdate";
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(WeddingAction.class);
    
    /** Creates a new instance of WeddingAction */
    public WeddingAction() {
    }   
    
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (REGISTER_SUBMITTED.equals(event)|| PAGE_SAVE.equals(event))
            return true; 
        
        return result;
    }
       
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        WeddingForm weddingForm = (WeddingForm)actionForm; 
        if (REGISTER.equals(action)) {
            errors = WeddingValidator.validateRegisterUser(errors, weddingForm); 
        } else if (REGISTER_SUBMITTED.equals(action)) {
            errors = WeddingValidator.validateRegistrationSubmission(errors, weddingForm);  
        } else if (PAGE_SAVE.equals(action)) {
            errors = WeddingValidator.validatePageSave(errors, weddingForm);  
        } else {
            logger.debug("Temporary skip");
        }
        
        weddingForm.getActionContext().setActionErrors(errors);
        return weddingForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (REGISTER_SUBMITTED.equals(action)) {
            action = REGISTER; 
        } else if (PAGE_SAVE.equals(action)) {
            action = PAGE; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doMosaic(WeddingForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return MOSAIC;
        } catch (Exception e) {
            return MOSAIC;
        }
    }
     
    public String doAbout(WeddingForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return ABOUT;
        } catch (Exception e) {
            return ABOUT;
        }
    }
    
    public String doRegister(WeddingForm actionForm)
    throws ApplicationException {
        try {  
                
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            actionForm.setUserId(Long.parseLong(userId));
            UserBO userBO = new UserBO();
            User user = userBO.getUserById(userId);
            actionForm.setUserName(user.getUserName()); 
            
            // Get registration, if found, display wedding page directly
            WeddingBO weddingBO = new WeddingBO();
            OEightWedding weddingObject = weddingBO.getWeddingRegistration(userId);
            if (weddingObject != null)
            { 
                // Set object to form here
                actionForm.setCoupleName(weddingObject.getCoupleName());
                actionForm.setWeddingDescription(weddingObject.getWeddingDescription());
                actionForm.setPhotoSmallPath(weddingObject.getPhotoSmallPath());
                actionForm.setPhotoLargePath(weddingObject.getPhotoLargePath());
                
                switch (weddingObject.getStatus())
                {
                    case OEightConstants.WEDDING_TYPE_CANCEL:
                    case OEightConstants.WEDDING_TYPE_NORMAL:
                        return PAGE; 
                        
                    case OEightConstants.WEDDING_TYPE_SPECIAL:
                        return PAGE_SPECIAL; 
                        
                    case OEightConstants.WEDDING_TYPE_XCLUSIVE:
                        return PAGE_EXCLUSIVE; 
                        
                    default:
                        return WEDDING_LOGIN;
                }
               
            }
            
            //actionForm.clear();  
            return REGISTER;
        } catch (Exception ex) {
            logger.debug("Registration not from login user");
            return WEDDING_LOGIN;
        }
    }
    
    
    public String doRegisterSubmitted(WeddingForm actionForm)
    throws ApplicationException {
        try { 
                
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            actionForm.setUserId(Long.parseLong(userId));
            UserBO userBO = new UserBO();
            User user = userBO.getUserById(userId); 
            actionForm.setUserName(user.getUserName()); 
           
            System.out.println("Debugging");
            System.out.println("Couple Name: " + actionForm.getCoupleName());
            System.out.println("Wedding Dsc: " + actionForm.getWeddingDescription());
                 
            // Photo is validated, upload and save photo to server, link id to the user   
            if (actionForm.getPhotoFile() != null && actionForm.getPhotoFile().getFileSize() > 0) {
                System.out.println("Photo=" + actionForm.getPhotoFile().getFileName());
                try{
                    PhotoFile photoFile = PhotoUploadHelper.
                            uploadWeddingMosaicPhoto(actionForm.getPhotoFile(), actionForm.getUserName());

                    actionForm.setPhotoPath(photoFile.getFilePath());
                    actionForm.setPhotoLargePath(photoFile.getFileName());
                    actionForm.setPhotoSmallPath(photoFile.getThumbnailFileName());

                }catch (Exception e) {
                    // Add error message
                }
            
            }else
            { 
                // Sets default photo values 
                actionForm.setPhotoLargePath(PropertyManager.getValue(PropertyConstants.WEDDING_PHOTO_DEFAULT_LARGE));
                actionForm.setPhotoSmallPath(PropertyManager.getValue(PropertyConstants.WEDDING_PHOTO_DEFAULT_SMALL));
            }
            
            // Save into DB  
            OEightWedding oeightWedding = actionForm.toOEightWeddingObject();
            WeddingBO weddingBO = new WeddingBO();
            weddingBO.saveRegistration(oeightWedding);
             
            weddingBO = null;
            
            // Update Mosaic
            
            
            return REGISTER_SUBMITTED;
       
        } catch (Exception ex) {
            logger.debug("Registration not from login user");
            return WEDDING_LOGIN;
        }
    }
                
    public String doList(WeddingForm actionForm)
    throws ApplicationException {
    try {  
            
            WeddingBO weddingBO = new WeddingBO(); 
            Page page = weddingBO.getWeddingList(actionForm.getCurrentPage());
            actionForm.setWeddingListPage(page);

            weddingBO = null;   

            return LIST;
        } catch (Exception e) {
            return ABOUT;
        }
    }
    
    public String doView(WeddingForm actionForm)
    throws ApplicationException {
        try {   
            
            String userName = actionForm.getWedding();
            UserBO userBO = new UserBO();
            User weddingUser = userBO.getUserByUserName(userName);
            
            if (weddingUser != null){
                // Get values for editing
                WeddingBO weddingBO = new WeddingBO();
                OEightWedding weddingObject = weddingBO.getWeddingRegistration("" + weddingUser.getUserId());
                if (weddingObject != null)
                { 
                    // Set object to form here
                    actionForm.setCoupleName(weddingObject.getCoupleName());
                    actionForm.setWeddingDescription(weddingObject.getWeddingDescription());
                    actionForm.setPhotoSmallPath(weddingObject.getPhotoSmallPath());
                    actionForm.setPhotoLargePath(weddingObject.getPhotoLargePath());

                    switch (weddingObject.getStatus())
                    {
                        case OEightConstants.WEDDING_TYPE_CANCEL:
                        case OEightConstants.WEDDING_TYPE_NORMAL:
                            return VIEW; 
                        /*        
                        case OEightConstants.WEDDING_TYPE_SPECIAL:
                            return PAGE_SPECIAL; 

                        case OEightConstants.WEDDING_TYPE_XCLUSIVE:
                            return PAGE_EXCLUSIVE; 
                        */    
                        default:
                            return NOT_FOUND_PAGE;
                    }

                }
                else
                {
                    return NOT_FOUND_PAGE;
                }
            }
            else
            {
                return NOT_FOUND_PAGE;
            }
        } catch (Exception e) {
            return WEDDING_LOGIN;
        }
    } 
    
    public String doPage(WeddingForm actionForm)
    throws ApplicationException {
        try {   
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            actionForm.setUserId(Long.parseLong(userId));
            
            // Get values for editing
            WeddingBO weddingBO = new WeddingBO();
            OEightWedding weddingObject = weddingBO.getWeddingRegistration(userId);
            if (weddingObject != null)
            { 
                // Set object to form here
                actionForm.setCoupleName(weddingObject.getCoupleName());
                actionForm.setWeddingDescription(weddingObject.getWeddingDescription());
                actionForm.setPhotoSmallPath(weddingObject.getPhotoSmallPath());
                actionForm.setPhotoLargePath(weddingObject.getPhotoLargePath());
                
                switch (weddingObject.getStatus())
                {
                    case OEightConstants.WEDDING_TYPE_CANCEL:
                    case OEightConstants.WEDDING_TYPE_NORMAL:
                        return PAGE; 
                        
                    case OEightConstants.WEDDING_TYPE_SPECIAL:
                        return PAGE_SPECIAL; 
                        
                    case OEightConstants.WEDDING_TYPE_XCLUSIVE:
                        return PAGE_EXCLUSIVE; 
                        
                    default:
                        return WEDDING_LOGIN;
                }
               
            }
            else
            {
                return doRegister(actionForm);
            }
            //return NOT_FOUND_PAGE;
        } catch (Exception e) {
            return WEDDING_LOGIN;
        }
    } 
    
    public String doPageSave(WeddingForm actionForm)
    throws ApplicationException {
        try {   
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            actionForm.setUserId(Long.parseLong(userId));

            // Get username
            UserBO userBO = new UserBO(); 
            User user = userBO.getUserById(userId); 
            String userName = user.getUserName();
            actionForm.setUserName(userName);
            userBO = null;
            
            WeddingBO weddingBO = new WeddingBO();
            OEightWedding oeightWeddingDB = weddingBO.getWeddingRegistration(userId);
            long weddingId = oeightWeddingDB.getWeddingId();
             // Photo is validated, upload and save photo to server, link id to the user   
            if (actionForm.getPhotoFile() != null && actionForm.getPhotoFile().getFileSize() > 0) {
                System.out.println("Photo=" + actionForm.getPhotoFile().getFileName());
                try{
                    PhotoFile photoFile = PhotoUploadHelper.
                            uploadWeddingMosaicPhoto(actionForm.getPhotoFile(), actionForm.getUserName());

                    actionForm.setPhotoPath(photoFile.getFilePath());
                    actionForm.setPhotoLargePath(photoFile.getFileName());
                    actionForm.setPhotoSmallPath(photoFile.getThumbnailFileName());

                }catch (Exception e) {
                    // Add error message
                }
            
            }else
            { 
                // No photo upload means no change, we do not update the file path and make sure is not set to null
                actionForm.setPhotoLargePath(oeightWeddingDB.getPhotoLargePath());
                actionForm.setPhotoSmallPath(oeightWeddingDB.getPhotoSmallPath());
            }
            OEightWedding oeightWedding = actionForm.toOEightWeddingObject();
            
            // Set updated values to db values
            oeightWeddingDB.setCoupleName(oeightWedding.getCoupleName());
            oeightWeddingDB.setWeddingDescription(oeightWedding.getWeddingDescription());
            oeightWeddingDB.setPhotoLargePath(oeightWedding.getPhotoLargePath());
            oeightWeddingDB.setPhotoSmallPath(oeightWedding.getPhotoSmallPath());
            
            // Save  updated values
            weddingBO.updateRegistration(oeightWeddingDB); 
            actionForm.setPageSavedSuccess(true);
            
            // Get values for editing 
            OEightWedding weddingObject = oeightWeddingDB;
            if (weddingObject != null)
            { 
                // Set object to form here
                actionForm.setCoupleName(weddingObject.getCoupleName());
                actionForm.setWeddingDescription(weddingObject.getWeddingDescription());
                actionForm.setPhotoSmallPath(weddingObject.getPhotoSmallPath());
                actionForm.setPhotoLargePath(weddingObject.getPhotoLargePath());
                
                switch (weddingObject.getStatus())
                {
                    case OEightConstants.WEDDING_TYPE_NORMAL:
                        return PAGE; 
                        
                    case OEightConstants.WEDDING_TYPE_SPECIAL:
                        return PAGE_SPECIAL; 
                        
                    case OEightConstants.WEDDING_TYPE_XCLUSIVE:
                        return PAGE_EXCLUSIVE; 
                        
                    default:
                        return WEDDING_LOGIN;
                }
               
            }
            return NOT_FOUND_PAGE;
        } catch (Exception e) {
            return WEDDING_LOGIN;
        }
    } 
    
    
    public String doResources(WeddingForm actionForm)
    throws ApplicationException {
        try {  
            return RESOURCES;
        } catch (Exception e) {
            return ABOUT;
        }
    } 
    
    
    protected String getDefaultActionName() {
                  
        return MOSAIC;
    }
    
    
    // Wedding admin function 
    public String doMarvinList(WeddingForm actionForm)
    throws ApplicationException {
    try {  
            // if user not marvin, throw
            String userId = checkUserAccess(actionForm);
            if (Integer.parseInt(userId) != PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY))
            {
                return NOT_FOUND_PAGE;
            } 
            
            WeddingBO weddingBO = new WeddingBO(); 
            List list = weddingBO.getWeddingListAll();
            actionForm.setAdminWeddingList(list);

            weddingBO = null;   

            return MARVIN_LIST;
        } catch (Exception e) {
            return ABOUT;
        }
    }
    
    public String doMarvinListUpdate(WeddingForm actionForm)
    throws ApplicationException {
    try {  
            // if user not marvin, throw
            String userId = checkUserAccess(actionForm);
            if (Integer.parseInt(userId) != PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY))
            {
                return NOT_FOUND_PAGE;
            }
            
            // The idea is just to set C to those not valid users
            String[] weddingIds = actionForm.getWedStrId(); 
            String[] weddingStatus = actionForm.getWedStatus(); 
            
            WeddingBO weddingBO = new WeddingBO(); 
            List list = weddingBO.updateAdminWeddingList(weddingIds, weddingStatus);
            actionForm.setAdminWeddingList(list);

            weddingBO = null;   

            return MARVIN_LIST;
        } catch (Exception e) {
            return ABOUT;
        }
    }
}
