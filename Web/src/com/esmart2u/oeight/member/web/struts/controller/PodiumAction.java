/*
 * PodiumAction.java
 *
 * Created on October 18, 2007, 4:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.member.bo.FeaturedBO;
import com.esmart2u.oeight.member.bo.PodiumBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.ApplicationException;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class PodiumAction extends AbstractApplicationAction {
    
    private static String MAIN_PAGE = "main";
    private static String VOTES_PAGE = "votes";
    private static String INVITES_PAGE = "invites";
    private static String VIEWS_PAGE = "views"; 
    private static String GAMERS_PAGE = "gamer";
    private static String BLOGS_PAGE = "blog";
    
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(PodiumAction.class);
    
    public String doMain(PodiumForm actionForm)
    throws ApplicationException { 
        
        //todo Check session id, allow 3 searches only
        HashMap podiumHashMap = PodiumBO.getInstance().getLatestPodiumStand();
        actionForm.setPodiumHashMap(podiumHashMap); 
        return MAIN_PAGE;
    }
    
    public String doVotes(PodiumForm podiumForm)
    throws ApplicationException {
        try {
            logger.debug("Running doVotes");
             
            // Get previous monthly top voted
            List votedHistoryList = PodiumBO.getInstance().getMostVotedHistory();
            podiumForm.setHistoryList(votedHistoryList);
            podiumForm.setSnapshotCode(OEightConstants.PODIUM_TYPE_VOTES);
            
            return VOTES_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, MAIN_PAGE);
        }
    }  
    
    public String doViews(PodiumForm podiumForm)
    throws ApplicationException {
        try {
            logger.debug("Running doViews");
             
            // Get previous monthly top voted
            List viewedHistoryList = PodiumBO.getInstance().getMostViewedHistory();
            podiumForm.setHistoryList(viewedHistoryList);
            podiumForm.setSnapshotCode(OEightConstants.PODIUM_TYPE_VIEWS);
            
            return VIEWS_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, MAIN_PAGE);
        }
    }  
    
    
    public String doInvites(PodiumForm podiumForm)
    throws ApplicationException {
        try {
            logger.debug("Running doInvites");
             
            // Get previous monthly top voted
            List invitesHistoryList = PodiumBO.getInstance().getMostInvitesHistory();
            podiumForm.setHistoryList(invitesHistoryList);
            podiumForm.setSnapshotCode(OEightConstants.PODIUM_TYPE_INVITES);
            
            return INVITES_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, MAIN_PAGE);
        }
    }  
    
    
    public String doGamer(PodiumForm podiumForm)
    throws ApplicationException {
        try {
            logger.debug("Running doGamer");
             
            // Get previous monthly top voted
            List gamerHistoryList = PodiumBO.getInstance().getMostGamePointsHistory();
            podiumForm.setHistoryList(gamerHistoryList);
            podiumForm.setSnapshotCode(OEightConstants.PODIUM_TYPE_GAMER);
            
            return GAMERS_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, MAIN_PAGE);
        }
    }  
    
    public String doBlog(PodiumForm podiumForm)
    throws ApplicationException {
        try {
            logger.debug("Running doBlog");
             
            // Get today's blog listing
            List blogList = FeaturedBO.getInstance().getFeaturedBlogs();
            podiumForm.setBlogList(blogList);
            
            return BLOGS_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, MAIN_PAGE);
        }
    }  
}
