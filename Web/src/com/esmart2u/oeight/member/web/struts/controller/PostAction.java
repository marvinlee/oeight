/*
 * PostAction.java
 *
 * Created on January 14, 2008, 8:37 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;
 
import com.esmart2u.oeight.member.bo.PostBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserPost;
import com.esmart2u.solution.base.helper.ApplicationException;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class PostAction  extends AbstractApplicationAction {
     
    private static String MAIN = "main";
    private static String CHECKED = "checked";
    private static String SUBMITTED = "submitted";
    
    private static String RELOGIN = "relogin";
     
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(PostAction.class);
    
    
    /** Creates a new instance of PostAction */
    public PostAction() {
    }
     
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (SUBMITTED.equals(event) || CHECKED.equals(event))
            return true;
        
        
        return result;
    }
    
      
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        PostForm postForm = (PostForm)actionForm; 
        if (CHECKED.equals(action)) {
            errors = PostValidator.checkPost(errors, postForm); 
        } else if (SUBMITTED.equals(action)) {
            errors = PostValidator.validatePost(errors, postForm); 
        } else
        {
            logger.debug("Temporary skip");
        }
        
        postForm.getActionContext().setActionErrors(errors);
        return postForm;
    }
    
      
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (CHECKED.equals(action)) {
            action = MAIN; 
        } else if (SUBMITTED.equals(action)) {
            action = CHECKED; 
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doMain(PostForm actionForm)
    throws ApplicationException {
        try { 
            //actionForm.clear();  
            return MAIN;
        } catch (Exception e) {
            return MAIN;
        }
    }
  
    public String doChecked(PostForm actionForm)
    throws ApplicationException {
        try { 
            actionForm.getActionContext().getRequest().getSession().setAttribute("verifiedURL", actionForm.getUrl());
            //actionForm.clear();  
            actionForm.setUrlOk("Y");
            return CHECKED;
        } catch (Exception e) {
            return MAIN;
        }
    }
       
    
    public String doSubmitted(PostForm actionForm)
    throws ApplicationException {
        try {
            String loginUserId = checkUserAccess(actionForm);
            try {
                String url = (String)actionForm.getActionContext().getRequest().getSession().getAttribute("verifiedURL");
                actionForm.setUrl(url);
                actionForm.getActionContext().getRequest().getSession().removeAttribute("verifiedURL");
                // Get User Id from session and get details 
                actionForm.setUserId(Long.parseLong(loginUserId));
                actionForm.setDatePosted(new Date());
                UserBO userBO = new UserBO(); 
                User user = userBO.getUserById(loginUserId); 
                String userName = user.getUserName();
                actionForm.setPostUserName(userName);
            } catch (Exception ex) {
                logger.debug("Post not from system");
                return MAIN;
            }
            
            // Save into DB     
            UserPost userPost = new UserPost();
            userPost.setPostedByUserId(actionForm.getUserId());
            userPost.setPostUrl(actionForm.getUrl());
            userPost.setPostTitle(actionForm.getTitle());
            userPost.setPostExcerpt(actionForm.getDescription());
            userPost.setPostedDate(new Date());
            userPost.setVotedUp(1);  
            userPost.setViewedCount(1); 
    
            PostBO postBO = new PostBO();
            postBO.savePost(userPost); 
            postBO = null;
          
            return SUBMITTED;
        } catch (Exception e) {
            e.printStackTrace();
            return RELOGIN;
        }
    }
    
    protected String getDefaultActionName() {
                  
        return MAIN;
    }
}
