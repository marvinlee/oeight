/*
 * FeedbackValidator.java
 *
 * Created on January 14, 2008, 10:40 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.solution.base.helper.Validator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 *
 * @author meauchyuan.lee
 */
public class FeedbackValidator {
      
    private static Logger logger = Logger.getLogger(FeedbackValidator.class);
    
    /** Creates a new instance of FeedbackValidator */
    public FeedbackValidator() {
    }
      
    public static ActionErrors validateFeedback(ActionErrors errors, FeedbackForm feedbackForm) {
         
        if (!Validator.validateString(feedbackForm.getName(),true,1,100,true)) {
            errors.add("name",new ActionError("common.invalid.feedback.name"));
        }
        if (!Validator.validateString(feedbackForm.getCompanyName(),false,0,100,true)) {
            errors.add("companyName",new ActionError("common.invalid.feedback.companyName"));
        }
        if (!Validator.validateString(feedbackForm.getContactNumber(),false,0,20,true)) {
            errors.add("contactNumber",new ActionError("common.invalid.feedback.contactNumber"));
        }
        if (!Validator.validateEmail(feedbackForm.getEmail(),true)) {
            errors.add("email",new ActionError("common.invalid.feedback.email"));
        }
        if (!Validator.validateString(feedbackForm.getSubject(),true,1,100,true)) {
            errors.add("subject",new ActionError("common.invalid.feedback.subject"));
        }
        if (!Validator.validateString(feedbackForm.getMessage(),true,0,500,false)) {
            errors.add("message",new ActionError("common.invalid.feedback.message"));
        }
        
        return errors;
    }
}
