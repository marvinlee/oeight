/*
 * WishForm.java
 *
 * Created on November 30, 2007, 2:31 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class WishForm  extends AbstractApplicationActionForm { 
    
    private long wishId; 
    private long userId;
    private String userName;
    private long wisherId;
    private String wishContent;
    private List wishersList; 
    private boolean viewingOwnProfile = false;
    private boolean hasError = false;
    private String myWish;
    
    private List friendsList;
    
    private String photoSmallPath;
    
    /** Creates a new instance of WishForm */
    public WishForm() {
    }
 
    public long getWishId() {
        return wishId;
    }

    public void setWishId(long wishId) {
        this.wishId = wishId;
    }
    
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public long getWisherId() {
        return wisherId;
    }

    public void setWisherId(long wisherId) {
        this.wisherId = wisherId;
    }

    public String getWishContent() {
        return wishContent;
    }

    public void setWishContent(String wishContent) {
        this.wishContent = wishContent;
    }

    public List getWishersList() {
        return wishersList;
    }

    public void setWishersList(List wishersList) {
        this.wishersList = wishersList;
    }

    public boolean isViewingOwnProfile() {
        return viewingOwnProfile;
    }

    public void setViewingOwnProfile(boolean viewingOwnProfile) {
        this.viewingOwnProfile = viewingOwnProfile;
    }

    public String getMyWish() {
        return myWish;
    }

    public void setMyWish(String myWish) {
        this.myWish = myWish;
    }

    public List getFriendsList() {
        return friendsList;
    }

    public void setFriendsList(List friendsList) {
        this.friendsList = friendsList;
    } 

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }
    
}
