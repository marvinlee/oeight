/*
 * FeedbackValidator.java
 *
 * Created on January 14, 2008, 10:40 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.UserPost;
import com.esmart2u.oeight.member.bo.PostBO; 
import com.esmart2u.oeight.member.helper.HTMLPageRequestor;
import com.esmart2u.solution.base.helper.Validator;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 *
 * @author meauchyuan.lee
 */
public class PostValidator {
      
    private static Logger logger = Logger.getLogger(PostValidator.class);
    
    /** Creates a new instance of PostValidator */
    public PostValidator() {
    }
      
    public static ActionErrors checkPost(ActionErrors errors, PostForm postForm) {
         
        if (!Validator.validateString(postForm.getUrl(),true,1,100,false)) {
            errors.add("url",new ActionError("common.invalid.postlink.url"));
        }
        
        try { 
            //Retrieve URL
            ArrayList pageTitleList = HTMLPageRequestor.fetchPage(new URL(postForm.getUrl()));
            if (pageTitleList == null || pageTitleList.isEmpty() 
                || pageTitleList.get(0) == null || "".equals(pageTitleList.get(0).toString().trim()))
            {
                errors.add("url",new ActionError("common.invalid.postlink.urlNotOK")); 
            }
            else
            {
            	//Got the title
            	postForm.setTitle(pageTitleList.get(0).toString().trim());
            }
            
        } catch (MalformedURLException ex) {
                errors.add("url",new ActionError("common.invalid.postlink.urlNotOK")); 
        }
        
        //Check already exist in db
        PostBO postBO = new PostBO();
        UserPost userPost = postBO.getPostWithUrl(postForm.getUrl());
        if (userPost != null)
        {
            errors.add("url",new ActionError("common.exists.postlink.url"));  
            postForm.setTitle(userPost.getPostTitle());
            postForm.setDescription(userPost.getPostExcerpt());  
            postForm.setUrlDuplicate(true);
        }
        postBO = null;
        
        return errors;
    }
    
       
    public static ActionErrors validatePost(ActionErrors errors, PostForm postForm) {
        
        if ("".equals(postForm.getUrlOk()) || !"Y".equals(postForm.getUrlOk()))
        {
            errors.add("url",new ActionError("common.invalid.postlink.urlNotOK"));
        }
        postForm.setUrl((String)postForm.getActionContext().getRequest().getSession().getAttribute("verifiedURL"));
        if (!Validator.validateString(postForm.getUrl(),true,1,200,true)) {
            errors.add("url",new ActionError("common.invalid.postlink.url"));
        }
        if (!Validator.validateString(postForm.getTitle(),false,1,100,false)) {
            errors.add("title",new ActionError("common.invalid.postlink.title"));
        }
        if (!Validator.validateString(postForm.getDescription(),true,1,200,false)) {
            errors.add("description",new ActionError("common.invalid.postlink.description"));
        } 
        
        return errors;
    }
}
