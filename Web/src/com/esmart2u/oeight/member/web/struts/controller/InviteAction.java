/*
 * InviteAction.java
 *
 * Created on October 21, 2007, 11:25 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserInvites;
import com.esmart2u.oeight.member.bo.InviteBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserWishesBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.helper.smtp.InvitesEmailSender;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.contactlist.ContactImpl;
import com.esmart2u.solution.base.helper.contactlist.ContactListHelper;
import java.util.ArrayList;
import java.util.List; 
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class InviteAction  extends AbstractApplicationAction {
    
    private static String MAIN_PAGE = "main";
    private static String INVITED_PAGE = "invited";
    private static String HISTORY_PAGE = "history";
    private static String LOGIN = "login"; 
    private static String RELOGIN = "relogin";
    private static String REQUEST = "request"; 
    private static String REQUEST_INVITED = "requestSubmitted";
    
    // Contact list retriever - for Profile invite, replacing old one
    private static String AUTO_PAGE = "auto";
    private static String AUTO_LIST_SELECT = "autoSelect";
    private static String AUTO_LIST_RESULT = "autoResult";
    
    // Contact list retriever - for Registration
    private static String CONTACT_LIST_REQUEST = "contactRequest";
    private static String CONTACT_LIST_SELECT = "contactSelect";
    private static String CONTACT_LIST_RESULT = "contactResult";
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(InviteAction.class);
    
    public String doMain(InviteForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
        
            return MAIN_PAGE;
        } catch (Exception e) { 
            return LOGIN;
        }
    }  
    
    public String doInvited(InviteForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Save email list to DB for scheduler to send emails
            InviteBO inviteBO = new InviteBO();
            actionForm.setAddressList(inviteBO.saveAddresses(userId, actionForm.getEmails()));
            if (actionForm.getAddressList() != null && !actionForm.getAddressList().isEmpty())
            {
                // Uncomment for actual sending
                //todo InvitesEmailSender.getInstance().invoke();
            }
            inviteBO = null;
            
            return INVITED_PAGE;
        } catch (Exception e) { 
            return RELOGIN;
        }
    }
     
    
    public String doHistory(InviteForm actionForm)
    throws ApplicationException {  
        try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            // Save email list to DB for scheduler to send emails
            InviteBO inviteBO = new InviteBO();
            List userInvitesList = (List)inviteBO.getUserInvitesList(userId);
            actionForm.setHistoryList(userInvitesList);
            if (userInvitesList != null && !userInvitesList.isEmpty())
            {
                actionForm.setTotalInvitesSent(userInvitesList.size());
                actionForm.setTotalInvitesAccepted(getNoOfAccepted(userInvitesList));
            }
            inviteBO = null;
            
            return HISTORY_PAGE;
        } catch (Exception e) { 
            return RELOGIN;
        }
    }
     
    private long getNoOfAccepted(List inviteList)
    {
        long count = 0;
        for(int i=0;i<inviteList.size();i++)
        {
            UserInvites invite = (UserInvites)inviteList.get(i);
            if (OEightConstants.MAIL_STATUS_ACCEPTED == invite.getStatus())
            {
                count++;
            }
        }
        return count;
    }
     
    public String doRequest(InviteForm actionForm)
    throws ApplicationException {  
        try {  
             
            actionForm.setEmails(null);
            actionForm.setAddressList(null);
            return REQUEST;
        } catch (Exception e) { 
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
     
     
    public String doRequestSubmitted(InviteForm actionForm)
    throws ApplicationException {  
        try { 
            // No User Id from session, set systemId
            String userId = PropertyManager.getValue(PropertyConstants.SYSTEM_USERID_KEY);
            
            // Save email list to DB for scheduler to send emails
            InviteBO inviteBO = new InviteBO();
            actionForm.setAddressList(inviteBO.saveAddresses(userId, actionForm.getEmails()));
            if (actionForm.getAddressList() != null && !actionForm.getAddressList().isEmpty())
            {
                // Uncomment for actual sending
                // No need to explicitly invoke, email daemon runs independantly
                //InvitesEmailSender.getInstance().invoke();
            }
            inviteBO = null;
            
            return REQUEST_INVITED;
        } catch (Exception e) {  
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
   
    /**
     *  Input page only
     */
    public String doAuto(InviteForm actionForm)
    throws ApplicationException {  
          try { 
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
        
            return AUTO_PAGE;
        } catch (Exception e) { 
            return RELOGIN;
        }
    }
    
    /**
     *  This is the processing and retrieving contact list with email and password,
     *  requires validation
     *  This page only shows non-members and previously uninvited people to send invites.
     */
    public String doAutoSelect(InviteForm actionForm)
    throws ApplicationException {  
        try {  
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            //
            String emailName = actionForm.getEmailUser();
            String provider = actionForm.getProvider();
            String password = actionForm.getPassword();
            
            // Get contacts, need instance as this process requires time
            ContactListHelper contactListHelper = new ContactListHelper();
            // contactList is list of Contact objects
            List contactList = contactListHelper.getContacts(emailName, provider, password);
            
            // Filter, search for existing members to filter/ignore
            // At the meantime, search for friends who already joined but not buddy yet
            InviteBO inviteBO = new InviteBO();
            List membersList = inviteBO.getMembersNotBuddyListFromContacts(userId, contactList);
                    
            // Keep all others as invitation
            List nonMembersList = inviteBO.getNonMembersListFromContacts(contactList, membersList); 
            
            actionForm.setMembersEmailList(membersList);
            actionForm.setNonMembersEmailList(nonMembersList);
            inviteBO = null;
            
            return AUTO_LIST_SELECT;
        } catch (Exception e) {  
            e.printStackTrace();
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    /**
     *  This is the result page of user selection and invites.
     *
     */
    public String doAutoResult(InviteForm actionForm)
    throws ApplicationException {  
        try {  
            // !!! IMPORTANT STEP
            // Get User Id from session and get details
            String loginUserId = checkUserAccess(actionForm);
            List addedMembersList = null;
            
            UserBO userBO = new UserBO();
            // Get members list and send request to add as buddy
            String[] membersUserName = actionForm.getMembersUserNameString();
            if (membersUserName != null && membersUserName.length>0)
            {
                addedMembersList = new ArrayList();
                for(int i=0; i<membersUserName.length;i++)
                {
                    String memberUserName = membersUserName[i];
                    User user = null; 
                    if (StringUtils.hasValue(memberUserName)) {
                
                        user = userBO.getUserByUserName(memberUserName);
                        String userId = Long.toString(user.getUserId());
                        UserWishesBO.addRequestFriend(loginUserId, userId); 
                        ContactImpl contact = new ContactImpl("","");
                        contact.setUserId(Long.parseLong(userId));
                        contact.setUserName(user.getUserName());
                        contact.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
                        addedMembersList.add(contact);
                        //Track counter is not required here as is from email address
                    }
                }
                // MembersEmailList are list of ContactImpl objects
                actionForm.setMembersEmailList(addedMembersList);
            }
            
            // Get non members list and send request as invites
            String[] nonMembersEmail = actionForm.getNonMembersEmailString();
            if (nonMembersEmail != null && nonMembersEmail.length>0)
            {
                String fullLengthEmail = "";
                for(int i=0; i<nonMembersEmail.length;i++)
                {
                    String nonMemberEmail = nonMembersEmail[i]; 
                    if (StringUtils.hasValue(nonMemberEmail)) {
                        fullLengthEmail += nonMemberEmail + ",";
                    }
                }  
                
                // Save addresses
                InviteBO inviteBO = new InviteBO();
                // NonMembersEmailList are list of UserInvites objects
                actionForm.setNonMembersEmailList(inviteBO.saveAddresses(loginUserId, fullLengthEmail)); 
                inviteBO = null;
            }
            
            userBO = null;
            
            return AUTO_LIST_RESULT;
        } catch (Exception e) {  
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }   
    
    /**
     *  Input page only
     */
    public String doContactRequest(InviteForm actionForm)
    throws ApplicationException {  
        try { 
           
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            
            
            return CONTACT_LIST_REQUEST;
        } catch (Exception e) {  
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    /**
     *  This is the processing and retrieving contact list with email and password,
     *  requires validation
     *  This page also allows to select add buddy and invite users.
     */
    public String doContactSelect(InviteForm actionForm)
    throws ApplicationException {  
        try {  
            
            // Get User Id from session and get details
            String userId = checkUserAccess(actionForm);
            //
            String emailName = actionForm.getEmailUser();
            String provider = actionForm.getProvider();
            String password = actionForm.getPassword();
            
            // Get contacts, need instance as this process requires time
            ContactListHelper contactListHelper = new ContactListHelper();
            // contactList is list of Contact objects
            List contactList = contactListHelper.getContacts(emailName, provider, password);
            
            // Filter, search for existing members to add as buddy
            InviteBO inviteBO = new InviteBO();
            List membersList = inviteBO.getMembersListFromContacts(contactList);
                    
            // Keep all others as invitation
            List nonMembersList = inviteBO.getNonMembersListFromContacts(contactList, membersList); 
            
            actionForm.setMembersEmailList(membersList);
            actionForm.setNonMembersEmailList(nonMembersList);
            inviteBO = null;
            
            return CONTACT_LIST_SELECT;
        } catch (Exception e) {  
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    }
    
    /**
     *  This is the result page of user selection and invites.
     *
     */
    public String doContactResult(InviteForm actionForm)
    throws ApplicationException {  
        try {  
            // !!! IMPORTANT STEP
            // Get User Id from session and get details
            String loginUserId = checkUserAccess(actionForm);
            List addedMembersList = null;
            
            UserBO userBO = new UserBO();
            // Get members list and send request to add as buddy
            String[] membersUserName = actionForm.getMembersUserNameString();
            if (membersUserName != null && membersUserName.length>0)
            {
                addedMembersList = new ArrayList();
                for(int i=0; i<membersUserName.length;i++)
                {
                    String memberUserName = membersUserName[i];
                    User user = null; 
                    if (StringUtils.hasValue(memberUserName)) {
                
                        user = userBO.getUserByUserName(memberUserName);
                        String userId = Long.toString(user.getUserId());
                        UserWishesBO.addRequestFriend(loginUserId, userId); 
                        ContactImpl contact = new ContactImpl("","");
                        contact.setUserId(Long.parseLong(userId));
                        contact.setUserName(user.getUserName());
                        contact.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
                        addedMembersList.add(contact);
                        //Track counter is not required here as is from email address
                    }
                }
                // MembersEmailList are list of ContactImpl objects
                actionForm.setMembersEmailList(addedMembersList);
            }
            
            // Get non members list and send request as invites
            String[] nonMembersEmail = actionForm.getNonMembersEmailString();
            if (nonMembersEmail != null && nonMembersEmail.length>0)
            {
                String fullLengthEmail = "";
                for(int i=0; i<nonMembersEmail.length;i++)
                {
                    String nonMemberEmail = nonMembersEmail[i]; 
                    if (StringUtils.hasValue(nonMemberEmail)) {
                        fullLengthEmail += nonMemberEmail + ",";
                    }
                }  
                
                // Save addresses
                InviteBO inviteBO = new InviteBO();
                // NonMembersEmailList are list of UserInvites objects
                actionForm.setNonMembersEmailList(inviteBO.saveAddresses(loginUserId, fullLengthEmail)); 
                inviteBO = null;
            }
            
            userBO = null;
            
            return CONTACT_LIST_RESULT;
        } catch (Exception e) {  
            //return rethrow(e, RELOGIN);
            return RELOGIN;
        }
    } 
    
    protected boolean isValidationRequired(String event) { 
        
        logger.debug("In isValidationRequired for " + event);
        if (CONTACT_LIST_SELECT.equals(event)||
            CONTACT_LIST_RESULT.equals(event)||
            AUTO_LIST_SELECT.equals(event)||
            AUTO_LIST_RESULT.equals(event))
            return true;
         
        
        return false;
    }
    
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        InviteForm inviteForm = (InviteForm)actionForm;
        
        logger.debug("ValidateParameter=" + action);
        
        if (CONTACT_LIST_SELECT.equals(action)) {
            errors = InviteValidator.validateInviteRequest(errors, inviteForm);
        } else if (CONTACT_LIST_RESULT.equals(action)) {
            errors = InviteValidator.validateInvitesSelected(errors, inviteForm);  
        } else if (AUTO_LIST_SELECT.equals(action)) {
            errors = InviteValidator.validateInviteRequest(errors, inviteForm);
        } else if (AUTO_LIST_RESULT.equals(action)) {
            errors = InviteValidator.validateInvitesSelected(errors, inviteForm);  
        } else {
            logger.debug("Temporary skip");
        }
        
        inviteForm.getActionContext().setActionErrors(errors);
        return inviteForm;
    }
    
    
    protected String getDefaultEvent(String action) {
        String previousAction = action;
        if (CONTACT_LIST_REQUEST.equals(action))
            action = CONTACT_LIST_REQUEST;
        else if (CONTACT_LIST_SELECT.equals(action))
            action = CONTACT_LIST_REQUEST;
        else if (CONTACT_LIST_RESULT.equals(action))
            action = CONTACT_LIST_SELECT; 
        else if (AUTO_PAGE.equals(action))
            action = AUTO_PAGE;
        else if (AUTO_LIST_SELECT.equals(action))
            action = AUTO_PAGE;
        else if (AUTO_LIST_RESULT.equals(action))
            action = AUTO_LIST_SELECT; 
        else {
            logger.debug("Temporary skip");
        }
        logger.debug("Going back to previous action from [" + previousAction + "] to [" + action + "] ");
        return action;
    }
  
    
}
