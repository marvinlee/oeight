/*
 * WidgetHelper.java
 *
 * Created on January 13, 2008, 12:22 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.oeight.data.FeaturedBlog;
import com.esmart2u.oeight.data.FriendsterPhoto;
import com.esmart2u.oeight.data.FriendsterUser;
import com.esmart2u.oeight.data.OEightPledge;
import com.esmart2u.oeight.data.RankingSnapshot;
import com.esmart2u.oeight.data.User;  
import com.esmart2u.oeight.member.bo.CampaignInviteBO;
import com.esmart2u.oeight.member.bo.EarthBO;
import com.esmart2u.oeight.member.bo.FeaturedBO;
import com.esmart2u.oeight.member.bo.RankingBO;
import com.esmart2u.oeight.member.bo.UserFriendsterBO;
import com.esmart2u.oeight.member.bo.UserWishesBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.WishVO;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.web.struts.taglib.TagUtils;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer; 
import org.apache.struts.util.ResponseUtils;

/**
 *
 * @author meauchyuan.lee
 */
public class WidgetHelper {
    
    /** Creates a new instance of WidgetHelper */
    public WidgetHelper() {
    }
    
    public static String getMainWidget(User user)
    {
        StringBuffer output = new StringBuffer();
        String wish = user.getUserCountry().getMyWish();
        wish = formatForTable(wish);
        String photoPath = "/vthumb/" + user.getUserCountry().getPhotoSmallPath();
        String userName = user.getUserName();
        String profilePath = "http://profile."+ ConfigurationHelper.getDomainName() + "/" + userName;
        
        RankingSnapshot rankingSnapshot = RankingBO.getUserDetails("" + user.getUserId());
        long views = rankingSnapshot.getPageView();
        long votes = rankingSnapshot.getVotes();   
        
        output.append("<style type=\"text/css\"><!--");
        output.append(".widgetTxt {font-family: tahoma, helvetica, arial, sans-serif;font-size: 11px; color: #6f6f6f;}");
        output.append(".widgetTxt a {color: #ce7210;}");
        output.append(".widgetTxt a:hover {color: #6f3f0b;}");
        output.append("--></style>");
        output.append("<center><table class=\"widgetTxt\" width=\"180\" height=\"220\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"http://"+ConfigurationHelper.getDomainName()+"/images/gadget/gadget1_bg.gif\">");
        output.append("<tr>");
        output.append("<td height=\"5\" width=\"15\">&nbsp;&nbsp;&nbsp;</td>");
        output.append("<td height=\"5\" width=\"38\">&nbsp;</td>");
        output.append("<td height=\"5\" width=\"112\">&nbsp;</td>");
        output.append("<td height=\"5\" width=\"15\">&nbsp;&nbsp;&nbsp;</td>");
        output.append("</tr>");
        output.append("<tr>");
        output.append("<td height=\"100\">&nbsp;</td>");
        output.append("<td height=\"100\" colspan=\"2\" align=\"left\" valign=\"top\" class=\"widgetTxt\"><a href=\""+profilePath+"\" target=\"_blank\"><script>document.write(unescape(decodeURIComponent(\""+wish+"\")));</script> ...</a> </td>");
        output.append("<td height=\"100\">&nbsp;</td>");
        output.append("</tr>");
        output.append("<tr>");
        output.append("<td height=\"10\">&nbsp;</td>");
        output.append("<td height=\"10\">&nbsp;</td>");
        output.append("<td height=\"10\">&nbsp;</td>");
        output.append("<td height=\"10\">&nbsp;</td>");
        output.append("</tr>");
        output.append("<tr>");
        output.append("<td height=\"38\">&nbsp;</td>");
        output.append("<td height=\"38\" width=\"38\" align=\"center\" valign=\"top\"><a href=\""+profilePath+"\" target=\"_blank\"><img src=\"http://"+ConfigurationHelper.getDomainName()+photoPath+"\" width=\"38\" height=\"38\"></a></td>");
        output.append("<td height=\"38\" valign=\"top\" align=\"left\"><a href=\""+profilePath+"\" target=\"_blank\">"+userName+"</a><br>Votes: "+votes+"<br>Views: "+views+"</td>");
        output.append("<td height=\"38\">&nbsp;</td>");
        output.append("</tr>");
        output.append("<tr>");
        output.append("<td height=\"37\" colspan=\"4\">&nbsp;</td>");
        output.append("</tr>");
        output.append("</table>");
        output.append("</center>");

    
        //todo the need to HTML encode the wish content and check line max length to add spaces
        return output.toString();
    }
    
    /**
     *For Facebook
     */ 
    public static String getMainWidgetForFacebook(User user)
    {
        StringBuffer output = new StringBuffer();
        String wish = user.getUserCountry().getMyWish();
        wish = formatForTable(wish,false);
        String photoPath = "/vthumb/" + user.getUserCountry().getPhotoSmallPath();
        String userName = user.getUserName();
        String profilePath = "http://profile."+ ConfigurationHelper.getDomainName() + "/" + userName;
        
        RankingSnapshot rankingSnapshot = RankingBO.getUserDetails("" + user.getUserId());
        long views = rankingSnapshot.getPageView();
        long votes = rankingSnapshot.getVotes();   
        
        output.append("<style type=\"text/css\"><!--");
        output.append(".widgetTxt {font-family: tahoma, helvetica, arial, sans-serif;font-size: 11px; color: #6f6f6f;}");
        output.append(".widgetTxt a {color: #ce7210;}");
        output.append(".widgetTxt a:hover {color: #6f3f0b;}");
        output.append("--></style>");
        output.append("<center><table class=\"widgetTxt\" width=\"180\" height=\"220\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"http://"+ConfigurationHelper.getDomainName()+"/images/gadget/gadget1_bg.gif\">");
        output.append("<tr>");
        output.append("<td height=\"5\" width=\"15\">&nbsp;&nbsp;&nbsp;</td>");
        output.append("<td height=\"5\" width=\"38\">&nbsp;</td>");
        output.append("<td height=\"5\" width=\"112\">&nbsp;</td>");
        output.append("<td height=\"5\" width=\"15\">&nbsp;&nbsp;&nbsp;</td>");
        output.append("</tr>");
        output.append("<tr>");
        output.append("<td height=\"100\">&nbsp;</td>");
        output.append("<td height=\"100\" colspan=\"2\" align=\"left\" valign=\"top\" class=\"widgetTxt\"><a href=\""+profilePath+"\" target=\"_blank\">"+ ResponseUtils.filter(wish) + " ...</a> </td>");
        output.append("<td height=\"100\">&nbsp;</td>");
        output.append("</tr>");
        output.append("<tr>");
        output.append("<td height=\"10\">&nbsp;</td>");
        output.append("<td height=\"10\">&nbsp;</td>");
        output.append("<td height=\"10\">&nbsp;</td>");
        output.append("<td height=\"10\">&nbsp;</td>");
        output.append("</tr>");
        output.append("<tr>");
        output.append("<td height=\"38\">&nbsp;</td>");
        output.append("<td height=\"38\" width=\"38\" align=\"center\" valign=\"top\"><a href=\""+profilePath+"\" target=\"_blank\"><img src=\"http://"+ConfigurationHelper.getDomainName()+photoPath+"\" width=\"38\" height=\"38\"></a></td>");
        output.append("<td height=\"38\" valign=\"top\" align=\"left\"><a href=\""+profilePath+"\" target=\"_blank\">"+userName+"</a><br>Votes: "+votes+"<br>Views: "+views+"</td>");
        output.append("<td height=\"38\">&nbsp;</td>");
        output.append("</tr>");
        output.append("<tr>");
        output.append("<td height=\"37\" colspan=\"4\">&nbsp;</td>");
        output.append("</tr>");
        output.append("</table>");
        output.append("<br><a href=\""+PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK1_INSTALL_URL)+"\">Grab This App</a>");
        output.append("</center>");

    
        //todo the need to HTML encode the wish content and check line max length to add spaces
        return output.toString();
    }
    
    private static String formatForTable(String input)
    {
        return formatForTable(input, true);
    }
    
    private static String formatForTable(String input, boolean encode)
    {
        String output = null;
        
        // Take first 130 chars, check and do space every 20chars
        if (input!=null && !"".equals(input))
        {
            int endIndex = input.length() > 130? 130:input.length();
            input = input.substring(0,endIndex);
            output = spaceToWords(input);
            //System.out.println("Output=" + output);
        }
        
        //URL encode it
        if (encode){
            try{
                output = URLEncoder.encode(output.trim(),"UTF-8");
            }catch (Exception e)
            {
                System.out.println("Error encoding");
            }
            //System.out.println("FInal output="+ output);
            output = replaceSpaces(output);
            //System.out.println("Spaces replaced output="+ output);
            //output.replaceAll("%2B","%20");
        }
        return output;
    }
    
    private static String spaceToWords(String input)
    { 
        String sentence = "";
        StringTokenizer tokenizer = new StringTokenizer(input," ");
        while (tokenizer.hasMoreTokens())
        {
            String word = tokenizer.nextToken();
            if (word.length() > 20)
            { 
                word = word.substring(0,20) + " " + spaceToWords(word.substring(20, word.length())); 
                //System.out.println("After space=" + word);
            }
            sentence+= word + " ";
        }
        //System.out.println("Sentence now is="+sentence);
        return sentence;
    }
    
    /**
     *  This replaces '+' sign to '%20'.
     *
     */
    private static String replaceSpaces(String input)
    {
        StringBuffer appended = new StringBuffer();
        char doublequotes = 34;
        char space = 43;
        if (input!=null && !"".equals(input)){
            for (int i=0;i<input.length();i++)
            {
                char letter = input.charAt(i);
                if (space == letter)
                {
                    appended.append("%20");
                }
                else if (doublequotes == letter)
                { 
                    appended.append("%22");
                }
                else
                {
                    appended.append(letter);
                }
            }
        }
        return appended.toString();
    }
    
    public static void main(String[] args) {
        System.out.println("formatted=" + formatForTable("To be leng chai r+1@!#@%$^&%&^*^&(&*)&^&''''${:\"<:>?<>123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"));
        //System.out.println("--" + formatForTable("\"1<script language=\"javascript\">alert(\"Alert!\")</script>2sfksj sfjslj skfjs lfjslkf ksjf s jfslkjf klsjf klsjfksjf lksj fklsjf lksjfklsjflksjf ksjf s jflks jflksjfs kljf s fklsjf lksjf lksjf lksjf<script language=\"javascript\">alert(\"Alert!\")</script>sfksj sfjslj skfjs lfjslkf ksjf s jfslkjf klsjf klsjfksjf lksj fklsjf lksjfklsjflksjf ksjf s jflks j"));
        //System.out.println("--" + formatForTable(""));
    }

    public static String getBlogWidget(User user) {
        return getBlogWidget(user, null);
    }
    public static String getBlogWidget(User user, String postAppend) {
        
        StringBuffer output = new StringBuffer();
        
        String blogLinkText = "http://" + ConfigurationHelper.getDomainName();
        String blogLink = "http://" + ConfigurationHelper.getDomainName() + "/podium.do?act=blog";
        String blogName = "Pending Verification";
        String blogDescription = "User has submitted blog to be featured.";
        String blogPhotoPath = PropertyManager.getValue(PropertyConstants.SYSTEM_DEFAULT_BLOG_PHOTO);
        boolean moderatedBlog = false;
        
        // Blog info
        FeaturedBlog featuredBlog = FeaturedBO.getFeaturedBlog(user.getUserId());
        if (featuredBlog != null && featuredBlog.getStatus() == OEightConstants.SYS_STATUS_MODERATED){
            moderatedBlog = true;
            blogLink = featuredBlog.getBlogLink();
            blogLinkText = blogLink;
            blogName = featuredBlog.getBlogName();
            blogDescription = featuredBlog.getBlogDescription();
            blogPhotoPath = featuredBlog.getBlogPhotoPath();  
        }
          
        String photoPath = "/vthumb/" + user.getUserCountry().getPhotoSmallPath();
        String userName = user.getUserName();
        String profilePath = "http://profile."+ ConfigurationHelper.getDomainName() + "/" + userName;
         
output.append("<style type=\"text/css\">");
output.append("<!--");
output.append(".widgetTxt {	font-family: tahoma, helvetica, arial, sans-serif;");
output.append("font-size: 11px;");
output.append("color: #6f6f6f;}");
output.append(".widgetTxt a {");
output.append("color: #ce7210;  ");
output.append(".widgetTxt a:hover {");
output.append("color: #6f3f0b; ");
output.append("-->");
output.append("</style>");
output.append("<center>");
output.append("<table width=\"400\" height=\"300\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\"http://"+ConfigurationHelper.getDomainName()+"/images/gadget/feat_blog_bg.gif\" class=\"widgetTxt\">");
output.append("<tr>");
output.append("<td height=\"70\" width=\"60\">&nbsp;&nbsp;&nbsp;</td>");
output.append("<td width=\"50\" height=\"70\">&nbsp;</td>");
output.append("<td width=\"60\" height=\"70\">&nbsp;</td>");
output.append("<td height=\"70\" width=\"40\">&nbsp;</td>");
output.append("<td width=\"40\" height=\"70\">&nbsp;</td>");
output.append("<td height=\"70\" width=\"120\">&nbsp;</td>");
output.append("<td height=\"70\" width=\"60\">&nbsp;&nbsp;&nbsp;</td>");
output.append("</tr>");
output.append("<tr>");
output.append("<td width=\"60\" height=\"20\">&nbsp;</td>");
output.append("<td height=\"20\" colspan=\"5\" align=\"center\" valign=\"top\" class=\"widgetTxt\"><a href=\""+ blogLink +"\" target=\"_blank\"><strong>"+ blogLinkText +"</strong></a></td>");
output.append("<td height=\"20\">&nbsp;</td>");
output.append("</tr>");
output.append("<tr>");
output.append("<td width=\"60\" height=\"120\">&nbsp;</td>");
output.append("<td height=\"120\">&nbsp;</td>");
output.append("<td height=\"120\" align=\"right\" valign=\"top\"></td>");
output.append("<td height=\"120\" colspan=\"3\" align=\"right\" valign=\"top\"><a href=\""+ blogLink +"\" target=\"_blank\"><img style=\"border-color:#333333 \" src=\""+blogPhotoPath+"\" width=\"200\" height=\"112\" border=\"1\"></a></td>");
output.append("<td height=\"120\">&nbsp;</td>");
output.append("</tr>");
output.append("<tr>");
output.append("<td width=\"60\" height=\"30\">&nbsp;</td>");
output.append("<td width=\"50\" height=\"30\" align=\"center\" valign=\"top\">&nbsp;</td>");
output.append("<td width=\"60\" height=\"30\" align=\"center\" valign=\"top\">&nbsp;</td>");
output.append("<td height=\"30\" colspan=\"3\" align=\"center\" valign=\"top\"><a href=\""+ blogLink +"\" target=\"_blank\">"+blogName+"</a></td>");
output.append("<td width=\"60\" height=\"30\">&nbsp;</td>");
output.append("</tr>");
output.append("<tr>");
output.append("<td width=\"60\" height=\"38\">&nbsp;</td>");
output.append("<td width=\"50\" height=\"38\" align=\"center\" valign=\"top\">&nbsp;</td>");
output.append("<td width=\"60\" height=\"38\" align=\"center\" valign=\"top\">&nbsp;</td>");
output.append("<td height=\"38\" colspan=\"3\" align=\"center\" valign=\"middle\"><a href=\""+profilePath+"\" target=\"_blank\"><img src=\"http://"+ConfigurationHelper.getDomainName()+photoPath+"\" width=\"38\" height=\"38\" border=\"0\" align=\"absmiddle\"></a> <a href=\""+profilePath+"\" target=\"_blank\">"+userName+"</a></td>");
output.append("<td width=\"60\" height=\"38\">&nbsp;</td>");
output.append("</tr>");
output.append("<tr>");
output.append("<td height=\"22\">&nbsp;</td>");
output.append("<td width=\"50\" height=\"22\" align=\"center\" valign=\"top\">&nbsp;</td>");
output.append("<td width=\"60\" height=\"22\" align=\"center\" valign=\"top\">&nbsp;</td>");
output.append("<td width=\"40\" height=\"22\" align=\"center\" valign=\"top\">&nbsp;</td>");
output.append("<td height=\"22\" colspan=\"2\" align=\"center\" valign=\"top\">&nbsp;</td>");
output.append("<td width=\"60\" height=\"22\">&nbsp;</td>");
output.append("</tr>");
output.append("</table>");
        if (StringUtils.hasValue(postAppend))
        {
            output.append(postAppend);
        }
output.append("</center>");  
         
    
        //todo the need to HTML encode the wish content and check line max length to add spaces
        return output.toString();
    }
    
    
    public static String getBlogWidgetForFacebook(User user) {
        StringBuffer postAppend = new StringBuffer(); 
        postAppend.append("<br><a href=\""+PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK3_INSTALL_URL)+"\">Grab This App</a>");
        return getBlogWidget(user, postAppend.toString());
    }

    
            
    public static String getBuddiesWidget(User user) {
        
        StringBuffer output = new StringBuffer(); 
        
        // Buddy list
        List buddyList = UserWishesBO.getUserFriendsListForWidget(user.getUserId());
        
          
        String photoPath = "/vthumb/" + user.getUserCountry().getPhotoSmallPath();
        String userName = user.getUserName();
        String profilePath = "http://profile."+ ConfigurationHelper.getDomainName() + "/" + userName;
         
        //output.append("<a href=\""+ profilePath + "\" target=\"_blank\"><img src=\"http://"+ ConfigurationHelper.getDomainName() + photoPath + "\"></a><br>");
        //output.append(userName + "&#39;s Buddies<br>");
        String buddyPhotoPaths[] = new String[8];
        String buddyNames[] = new String[8];
        String buddyProfilePaths[] = new String[8];
        String emptyBuddy = "&nbsp;";
        String transparentGif = "http://" + ConfigurationHelper.getDomainName() + "/images/common/tp.gif";
        if (buddyList != null && !buddyList.isEmpty())
        {
            for(int i=0;i<buddyList.size();i++)
            {
                WishVO wishVO = (WishVO)buddyList.get(i);
                buddyPhotoPaths[i] = "/vthumb/" + wishVO.getPhotoSmallPath();
                buddyNames[i] = wishVO.getUserName();
                buddyProfilePaths[i] = "http://profile."+ ConfigurationHelper.getDomainName() + "/" + buddyNames[i];
                //output.append("<a href=\""+ buddyProfilePath + "\" target=\"_blank\"><img src=\"http://"+ ConfigurationHelper.getDomainName() + "/vthumb/" + buddyPhotoPath + "\"></a><br>");
                //output.append(buddyName + "<br>");
            }
        }
        String imageString[] = new String[8];
        for(int i=0;i<8;i++)
        {
            if (!StringUtils.hasValue(buddyPhotoPaths[i]))
            {
                imageString[i] = emptyBuddy;
            }
            else
            {
                imageString[i] = "<img src=\"http://" + ConfigurationHelper.getDomainName() +buddyPhotoPaths[i]+"\"><br><a href=\""+buddyProfilePaths[i]+"\" target=\"_blank\">"+buddyNames[i]+"</a>";
            }
        }
        
        //output.append("<a href=\"http://" + ConfigurationHelper.getDomainName() + "/wish.do?act=frens&userName="+userName+"\" target=\"_blank\">More friends</a>");
        
        
output.append("<style type=\"text/css\">");
output.append("<!--");
output.append("img { border: 0; }");
output.append(".widgetLblS {	font-family: tahoma, helvetica, arial, sans-serif;");
output.append("font-size: 12px;");
output.append("font-weight: 700;");
output.append("color: #6f6f6f;}");
output.append(".widgetTxt {	font-family: tahoma, helvetica, arial, sans-serif;");
output.append("font-size: 11px;");
output.append("color: #6f6f6f;}");
output.append(".widgetTxt a {");
output.append("color: #ce7210;");
output.append(".widgetTxt a:hover {");
output.append("color: #6f3f0b;");
output.append("-->");
output.append("</style>");
output.append("<center><table background=\"http://080808.org.my/img/widget/buddies_l.gif\" width=\"440px\" height=\"250px\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">");
output.append("<tr height=\"80px\">");
output.append("<td width=\"120px\" colspan=\"2\">&nbsp;");
output.append("</td>");
output.append("<td width=\"300px\" colspan=\"3\" class=\"widgetLblS\" align=\"left\"><a href=\""+ profilePath + "\" target=\"_blank\" ><img src=\"http://"+ ConfigurationHelper.getDomainName() + photoPath + "\" align=\"absmiddle\" ></a>&nbsp;&nbsp;&nbsp;"+userName+"&#39;s Buddies");
output.append("</td>");
output.append("<td width=\"20px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"70px\">");
output.append("<td width=\"20px\">&nbsp;");
output.append("</td>");
output.append("<td width=\"100px\" valign=\"top\" align=\"center\" class=\"widgetTxt\">"+imageString[0]);
output.append("</td>");
output.append("<td width=\"100px\" valign=\"top\" align=\"center\" class=\"widgetTxt\">"+imageString[1]);
output.append("</td>");
output.append("<td width=\"100px\" valign=\"top\" align=\"center\" class=\"widgetTxt\">"+imageString[2]);
output.append("</td>");
output.append("<td width=\"100px\" valign=\"top\" align=\"center\" class=\"widgetTxt\">"+imageString[3]);
output.append("</td>");
output.append("<td width=\"20px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"70px\">");
output.append("<td width=\"20px\">&nbsp;");
output.append("</td>");
output.append("<td width=\"100px\" valign=\"top\" align=\"center\" class=\"widgetTxt\">"+imageString[4]);
output.append("</td>");
output.append("<td width=\"100px\" valign=\"top\" align=\"center\" class=\"widgetTxt\">"+imageString[5]);
output.append("</td>");
output.append("<td width=\"100px\" valign=\"top\" align=\"center\" class=\"widgetTxt\">"+imageString[6]);
output.append("</td>");
output.append("<td width=\"100px\" valign=\"top\" align=\"center\" class=\"widgetTxt\">"+imageString[7]);
output.append("</td>");
output.append("<td width=\"20px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"30px\">");
output.append("<td>&nbsp;");
output.append("</td>");
output.append("<td>&nbsp;");
output.append("</td>");
output.append("<td valign=\"top\" class=\"widgetTxt\"><a href=\"http://" + ConfigurationHelper.getDomainName() + "/wish.do?act=frens&userName="+userName+"\" target=\"_blank\">All buddies</a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"100px\" height=\"30\"></a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"100px\" height=\"30\"></a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"20px\" height=\"30\"></a>");
output.append("</td>");
output.append("</tr>");
output.append("</table></center>");
        
        
        
        return output.toString();
    } 
 
    public static String getWishesWidget(User user) {

        StringBuffer output = new StringBuffer(); 
        
        // Wishes list
        List wishesList = UserWishesBO.getUserWishesListForWidget(user.getUserId());
        
          
        String photoPath = "/vthumb/" + user.getUserCountry().getPhotoSmallPath();
        String userName = user.getUserName();
        String profilePath = "http://profile."+ ConfigurationHelper.getDomainName() + "/" + userName;
          
        String buddyPhotoPaths[] = new String[2];
        String buddyNames[] = new String[2];
        String buddyProfilePaths[] = new String[2];
        String wishContent[] = new String[2];
        String emptyBuddy = "&nbsp;";
        String transparentGif = "http://" + ConfigurationHelper.getDomainName() + "/images/common/tp.gif";
        if (wishesList != null && !wishesList.isEmpty())
        {
            for(int i=0;i<wishesList.size();i++)
            {
                WishVO wishVO = (WishVO)wishesList.get(i);
                buddyPhotoPaths[i] = "/vthumb/" + wishVO.getPhotoSmallPath();
                buddyNames[i] = wishVO.getUserName();
                wishContent[i] = wishVO.getWishContent();
                buddyProfilePaths[i] = "http://profile."+ ConfigurationHelper.getDomainName() + "/" + buddyNames[i];
    
            }
        }
        String imageString[] = new String[2];
        for(int i=0;i<2;i++)
        {
            if (StringUtils.hasValue(buddyPhotoPaths[i]))
            {  
                imageString[i] = "<p><img src=\"http://" + ConfigurationHelper.getDomainName() +buddyPhotoPaths[i]+"\"><br><a href=\""+buddyProfilePaths[i]+"\" target=\"_blank\">"+buddyNames[i]+"</a>"+formatForTable(wishContent[i])+"</p>";
            }
            output.append(imageString[i]);
        }
        
       
        output.append("View all wishes...");
        return output.toString();
    }
     
    public static String getFriendsterFriendWidget(User user) {
         
        StringBuffer output = new StringBuffer(); 
        
        String myFriendsterProfile = null;
        String myPrimaryPhotoUrl = null;
        String myFriendsterUserName = null;
        
        // Friendster list
        System.out.println("getFriendsterFriendWidget=" + user);
        FriendsterUser friendsterMe = UserFriendsterBO.getFriendsterByUserId(user.getUserId());
        List friendsterList = UserFriendsterBO.getFriendsterFriendsListForWidget(user.getUserId()); 
        if (friendsterMe == null)
        {
            return output.append("Friendster Profile Not Found.").toString();
        }
        else
        {
            myFriendsterProfile = friendsterMe.getProfileUrl();
            myPrimaryPhotoUrl = friendsterMe.getPrimaryPhotoUrl();
            myFriendsterUserName = friendsterMe.getFriendsterUserName();
        } 
        
        String friendPhotoPaths[] = new String[8];
        String friendNames[] = new String[8];
        String friendProfilePaths[] = new String[8];
        String emptyFriend = "&nbsp;";
        String transparentGif = "http://" + ConfigurationHelper.getDomainName() + "/images/common/tp.gif";
        String allFriendsLink = "http://www.friendster.com/friends/" + friendsterMe.getFriendsterUserId();
        
        //output.append("<a href=\""+ myFriendsterProfile + "\" target=\"_blank\"><img src=\"" + myPrimaryPhotoUrl + "\"></a><br>");
        //output.append(myFriendsterUserName + "&#39;s Friends in Friendster<br>");
        if (friendsterList != null && !friendsterList.isEmpty())
        {
            for(int i=0;i<friendsterList.size();i++)
            {
                FriendsterUser friendsterUser = (FriendsterUser)friendsterList.get(i);
                friendPhotoPaths[i] = friendsterUser.getPrimaryPhotoUrl();
                friendNames[i] = friendsterUser.getFriendsterUserName();
                friendProfilePaths[i] = friendsterUser.getProfileUrl();
                //output.append("<a href=\""+ friendsterProfilePath + "\" target=\"_blank\"><img src=\""+ friendsterPhotoPath + "\"></a><br>");
                //output.append(friendsterName + "<br>");
            }
        }
        
        String imageString[] = new String[8];
        String fsterFLoad="var fsterFLoad=new Array(); ";
        String resizeCall="";
        for(int i=0;i<8;i++)
        {
            if (!StringUtils.hasValue(friendPhotoPaths[i]))
            {
                imageString[i] = emptyFriend;
            }
            else
            {
                //<div class=\"divPic\"><a href=\"#\" target=\"_blank\"><img src=\"1b.jpg\" height=\"100\" ></a></div>
                //imageString[i] = "<div class=\"divPic\"><center><a href=\""+friendProfilePaths[i]+"\" target=\"_blank\"><img src=\""+friendPhotoPaths[i]+"\" height=\"100\" ></a><br>"+friendNames[i]+"</center></div>";                
                //imageString[i] = "<div class=\"divPic\"><center><a href=\""+friendProfilePaths[i]+"\" target=\"_blank\"><img src=\""+friendPhotoPaths[i]+"\" onMouseOver=\"resizeFrenImg(this)\" onLoad=\"resizeFrenImg(this)\" ></a><br>"+friendNames[i]+"</center></div>";                
                 imageString[i] = "<div class=\"divPic\"><center><a href=\""+friendProfilePaths[i]+"\" target=\"_blank\"><img src=\""+friendPhotoPaths[i]+"\" onMouseOver=resizeFsterFrenImg(\"fsterF"+i+"\","+i+") onLoad=resizeFsterFrenImg(\"fsterF"+i+"\","+i+") id=\"fsterF"+i+"\" width=\"100\"></a><br>"+friendNames[i]+"</center></div>";                
                 fsterFLoad+= "fsterFLoad["+i+"]=false; ";
                 resizeCall+= "resizeFsterFrenImg(\"fsterF"+i+"\","+i+"); ";
            }
        }

output.append("<style type=\"text/css\">");
output.append("<!--");
output.append("img { border: 0; }");
output.append(".widgetLbl {	font-family: tahoma, helvetica, arial, sans-serif;");
output.append("font-size: 14px;");
output.append("font-weight: 700;");
output.append("color: #6f6f6f;}");
output.append(".widgetTxt {	font-family: tahoma, helvetica, arial, sans-serif;");
output.append("font-size: 11px;");
output.append("color: #6f6f6f;}");
output.append(".widgetTxt a {");
output.append("color: #ce7210;}");
output.append(".widgetTxt a:hover {");
output.append("color: #6f3f0b;");
output.append("}");
output.append(".divPic {");
output.append("background-color:white;");
output.append("height: 100px;");
output.append("width: 100px;");
output.append("}");
output.append(".tblFrenImg {");
output.append("	background-image:url(\"http://080808.org.my/img/widget/fren_l.gif\");");
output.append("	background-repeat:no-repeat;");
output.append("}");
output.append("-->");
output.append("</style> ");
/*output.append("<script>");
output.append("function resizeFrenImg(image)");
output.append("{ ");
output.append("var max = 100;");
output.append("var ratio = image.width / image.height;");
output.append("if (image.width > max)");
output.append("{");
output.append("image.width = max;");
output.append("image.height = max / ratio;");
output.append("}");
output.append("if(image.height > max)");
output.append("{");
output.append("image.height = max;");
output.append("image.width = image.height * ratio;");
output.append("}");
output.append("}");
output.append("</script>");*/
// Print the following in global variables 
/*var fsterFLoad=new Array();
fsterFLoad[0]=false;
fsterFLoad[1]=false;
fsterFLoad[2]=false;
fsterFLoad[3]=false;
 *
 *
 *
fsterFLoad[7]=false;
*/
output.append("<script>"+fsterFLoad); 
output.append("</script>"); 
output.append("<script type=\"text/javascript\" src=\"http://" + ConfigurationHelper.getDomainName() + "/scripts/js/fsterF.js\"></script>");
output.append("<center>");
output.append("<table class=\"tblFrenImg\" width=\"500px\" height=\"310px\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">");
output.append("<tr height=\"30px\">"); // For friends widget with name, use 30px, photos with no caption, use 40px
output.append("<td width=\"130px\" colspan=\"2\">&nbsp;");
output.append("</td>");
output.append("<td width=\"360px\" colspan=\"3\" class=\"widgetLbl\"><br>"+myFriendsterUserName+"&#39;s Friendster");
output.append("</td>");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"100px\">");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>"); 
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[0]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[1]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[2]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[3]);
output.append("</td>");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"100px\">");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[4]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[5]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[6]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[7]);
output.append("</td>");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"30px\">");
output.append("<td>&nbsp;");
output.append("</td>");
output.append("<td>&nbsp;");
output.append("</td>");
output.append("<td align=\"center\" class=\"widgetTxt\"><a href=\""+allFriendsLink+"\" target=\"_blank\">Friendster Friends</a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"120px\" height=\"20\"></a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"120px\" height=\"20\"></a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"10px\" height=\"20\"></a>");
output.append("</td>");
output.append("</tr>");
output.append("</table></center>");
// This is the call to resize images for profile page use
output.append("<script>");
//output.append("while (!allFsterFrenLoaded()){"); 
output.append("function onLoadFriendsterFriends(){"); 
output.append(resizeCall); 
output.append("}");
//output.append(" }");
output.append("</script>");
        
        //output.append("<a href=\"" + myFriendsterProfile + "\" target=\"_blank\">Friendster Profile</a>");
        return output.toString();
    }

    public static String getFriendsterPhotoWidget(User user, boolean showMyself) {
           
        StringBuffer output = new StringBuffer(); 
        
        String myFriendsterProfile = null;
        String myPrimaryPhotoUrl = null;
        String myFriendsterUserName = null;
         
        FriendsterUser friendsterMe = UserFriendsterBO.getFriendsterByUserId(user.getUserId()); 
        if (friendsterMe == null)
        {
            return output.append("Friendster Profile Not Found.").toString();
        } 
        
        // Photo list
        List photoList = UserFriendsterBO.getFriendsterPhotosListForWidget(user.getUserId(), friendsterMe); 
        if (showMyself && friendsterMe != null)
        {  
            myFriendsterProfile = friendsterMe.getProfileUrl();
            myPrimaryPhotoUrl = friendsterMe.getPrimaryPhotoUrl();
            myFriendsterUserName = friendsterMe.getFriendsterUserName();
        }
        
        //String photoPath = "/vthumb/" + user.getUserCountry().getPhotoSmallPath();
        String userName = user.getUserName();
        //String profilePath = "http://profile."+ ConfigurationHelper.getDomainName() + "/" + userName; 
         
        /*
        if (showMyself)
        { 
            output.append("<a href=\""+ myFriendsterProfile + "\" target=\"_blank\"><img src=\"" + myPrimaryPhotoUrl + "\"></a><br>");
            output.append(myFriendsterUserName + "&#39;s Photos in Friendster<br>");
        }
        else{
            output.append(userName + "&#39;s Photos in Friendster<br>");
        }*/
        
        
        String photoPaths[] = new String[8];
        String photoLargePaths[] = new String[8];
        String emptyPhoto = "&nbsp;";
        String transparentGif = "http://" + ConfigurationHelper.getDomainName() + "/images/common/tp.gif";
        String allPhotosLink = "http://www.friendster.com/viewphotos.php?uid=" + friendsterMe.getFriendsterUserId() + "&a=0";
        
        if (photoList != null && !photoList.isEmpty())
        {
            for(int i=0;i<photoList.size();i++)
            {
                FriendsterPhoto friendsterPhoto = (FriendsterPhoto)photoList.get(i);
                photoPaths[i] = friendsterPhoto.getPhotoUrl();
                photoLargePaths[i] = friendsterPhoto.getPhotoLargeUrl(); 
                //output.append("<a href=\""+ friendsterPhotoLargePath + "\" target=\"_blank\"><img src=\""+ friendsterPhotoPath + "\"></a><br>");
                //output.append(friendsterPhotoCaption + "<br>");
            }
        }
        
        String imageString[] = new String[8];
        String fsterPLoad="var fsterPLoad=new Array(); ";
        String resizeCall="";
        for(int i=0;i<8;i++)
        {
            if (!StringUtils.hasValue(photoPaths[i]))
            {
                imageString[i] = emptyPhoto;
            }
            else
            {
                //<div class=\"divPic\"><a href=\"#\" target=\"_blank\"><img src=\"1b.jpg\" height=\"100\" ></a></div>
                //imageString[i] = "<div class=\"divPic\"><a href=\""+photoLargePaths[i]+"\" target=\"_blank\"><img src=\""+photoPaths[i]+"\" height=\"100\" ></a></div>";   
                imageString[i] = "<div class=\"divPic\"><a href=\""+photoLargePaths[i]+"\" target=\"_blank\"><img src=\""+photoPaths[i]+"\" onMouseOver=resizeFsterPicImg(\"fsterP"+i+"\","+i+") onLoad=resizeFsterPicImg(\"fsterP"+i+"\","+i+") id=\"fsterP"+i+"\" width=\"100\" ></a></div>";                                 
                fsterPLoad+= "fsterPLoad["+i+"]=false; ";
                resizeCall+= "resizeFsterPicImg(\"fsterP"+i+"\","+i+"); ";
            }
        }
        
output.append("<style type=\"text/css\">");
output.append("<!--");
output.append("img { border: 0; }");
output.append(".widgetLbl {	font-family: tahoma, helvetica, arial, sans-serif;");
output.append("font-size: 14px;");
output.append("font-weight: 700;");
output.append("color: #6f6f6f;}");
output.append(".widgetTxt {	font-family: tahoma, helvetica, arial, sans-serif;");
output.append("font-size: 11px;");
output.append("color: #6f6f6f;}");
output.append(".widgetTxt a {");
output.append("color: #ce7210;}");
output.append(".widgetTxt a:hover {");
output.append("color: #6f3f0b;");
output.append("}");
output.append(".divPic {");
output.append("background-color:white;");
output.append("height: 100px;");
output.append("width: 100px;");
output.append("}");
output.append(".tblPhotoImg {");
output.append("	background-image:url(\"http://080808.org.my/img/widget/photo_l.gif\");");
output.append("	background-repeat:no-repeat;");
output.append("}");
output.append("-->");
output.append("</style> ");
/*output.append("<script>");
output.append("function resizePhotoImg(image)");
output.append("{ ");
output.append("var max = 100;");
output.append("var ratio = image.width / image.height;");
output.append("if (image.width > max)");
output.append("{");
output.append("image.width = max;");
output.append("image.height = max / ratio;");
output.append("}");
output.append("if(image.height > max)");
output.append("{");
output.append("image.height = max;");
output.append("image.width = image.height * ratio;");
output.append("}");
output.append("}");
output.append("</script>");*/
output.append("<script>"+fsterPLoad); 
output.append("</script>"); 
output.append("<script type=\"text/javascript\" src=\"http://" + ConfigurationHelper.getDomainName() + "/scripts/js/fsterP.js\"></script>");
output.append("<center>");
output.append("<table class=\"tblPhotoImg\" width=\"500px\" height=\"310px\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">");
output.append("<tr height=\"40px\">"); // For friends widget with name, use 30px, photos with no caption, use 40px
output.append("<td width=\"130px\" colspan=\"2\">&nbsp;");
output.append("</td>");
output.append("<td width=\"360px\" colspan=\"3\" class=\"widgetLbl\"><br>"+myFriendsterUserName+"&#39;s Friendster");
output.append("</td>");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"100px\">");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>"); 
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[0]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[1]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[2]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[3]);
output.append("</td>");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"100px\">");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[4]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[5]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[6]);
output.append("</td>");
output.append("<td width=\"120px\" valign=\"middle\" align=\"center\">"+imageString[7]);
output.append("</td>");
output.append("<td width=\"10px\">&nbsp;");
output.append("</td>");
output.append("</tr>");
output.append("<tr height=\"30px\">");
output.append("<td>&nbsp;");
output.append("</td>");
output.append("<td>&nbsp;");
output.append("</td>");
output.append("<td align=\"center\" class=\"widgetTxt\"><a href=\""+allPhotosLink+"\" target=\"_blank\">Friendster Photos</a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"120px\" height=\"20\"></a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"120px\" height=\"20\"></a>");
output.append("</td>");
output.append("<td><a href=\"http://" + ConfigurationHelper.getDomainName() + "\" target=\"_blank\"><img src=\""+transparentGif+"\" width=\"10px\" height=\"20\"></a>");
output.append("</td>");
output.append("</tr>");
output.append("</table></center>");
// This is the call to resize images for profile page use
output.append("<script>"); 
output.append("function onLoadFriendsterPhotos(){"); 
output.append(resizeCall); 
output.append("}"); 
output.append("</script>");

        /*if (showMyself)
        { 
            output.append("<a href=\"" + myFriendsterProfile + "\" target=\"_blank\">Friendster Profile</a>");
        }*/
        
        return output.toString();
    }

    /**
     *  Return a list of String, first String is the HTML
     *  Second is optional and will be the oEightId, used for referral tracking
     */
    public static List getFriendsterClimateChangeWidget(String friendsterUserId) {
        List returnList = new ArrayList();
        StringBuffer output = new StringBuffer(); 
       
        EarthBO earthBO = new EarthBO(); 
        OEightPledge pledge = earthBO.getLatestPledgeFromThirdParty( 
                OEightConstants.CAMPAIGN_INVITE_FROM_FRIENDSTER, friendsterUserId);
        if (pledge != null)
        {
            String pledgerName = pledge.getPledgerName();
            String pledgeCategory = OEightPledgeHelper.getPledgeCategoryByCode(""+pledge.getPledgeCode());
            String pledgeTo = OEightPledgeHelper.getPledgeLabelByCode(""+pledge.getPledgeCode());
            boolean showPhoto = false;
            String userId = "" + pledge.getUserId();
            long accepted = 0;
            
            if (StringUtils.hasValue(userId)){
                accepted = CampaignInviteBO.getTotalAccepted(userId, OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE);
            }
                    
            //output.append("<br>Pledge Name :" + pledge.getPledgerName());
            //output.append("<br>Pledge Type :" + OEightPledgeHelper.getPledgeCategoryByCode(""+pledge.getPledgeCode())); 
            //output.append("<br>Pledge To   :" + OEightPledgeHelper.getPledgeLabelByCode(""+pledge.getPledgeCode())); 
            
           
output.append("<style type=\"text/css\"><!--");
output.append(".ccTxt {text-decoration:none; font-family: tahoma, helvetica, arial, sans-serif;font-size: 14px; color: #000000;}");
output.append(".ccTxt a {text-decoration:none; color: #000000;}");
output.append(".ccTxt a:hover {text-decoration:none; color: #999999;}");
output.append("--></style>"); 
output.append("<center><table width=\"380\" height=\"180\"  border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" background=\"http://080808.org.my/earth08/img/"+pledgeCategory+".jpg\" >");
output.append("<tr>");
output.append("<td width=\"131\" height=\"34\">&nbsp;</td>");
output.append("<td width=\"52\">&nbsp;</td>");
output.append("<td width=\"165\">&nbsp;</td>");
output.append("<td width=\"32\">&nbsp;</td>");
output.append("</tr>");
output.append("<tr>");
output.append("<td height=\"38\">&nbsp;</td>");
output.append("<td>");
if (showPhoto){
    output.append("<img src=\"na1.JPG\" width=\"38\" height=\"38\" align=\"top\"></td>");
}else
{
    output.append("&nbsp;</td>");
}
output.append("<td>&nbsp;</td>");
output.append("<td>&nbsp;</td>");
output.append("</tr>");
output.append("<tr>");
output.append("<td height=\"66\">&nbsp;</td>");
output.append("<td colspan=\"2\" align=\"center\" valign=\"middle\" class=\"ccTxt\"><a href=\"http://earth." + ConfigurationHelper.getDomainName() + "\" target=\"_blank\">" + pledgeTo + "</a></td>");
output.append("<td>&nbsp;</td>");
output.append("</tr>");
output.append("<tr>");
output.append("<td height=\"42\">&nbsp;</td>");
output.append("<td>&nbsp;</td>");
output.append("<td>&nbsp;</td>");
output.append("<td>&nbsp;</td>");
output.append("</tr>");
output.append("</table><br><a href=\"http://earth." + ConfigurationHelper.getDomainName() + "\" target=\"_blank\">");
if (accepted == 1){
    output.append("I have a friend who pledged with me.");
}
else if (accepted > 1){
    output.append("I have " + accepted + " friends who pledged with me.");
}
output.append("</a></center>");
            
        } 
        else
        {
            output.append("No pledge record found");
        }
        
        String htmlString = output.toString();
        returnList.add(htmlString);
        if (StringUtils.hasValue(pledge.getUserName()))
        {
            returnList.add(pledge.getUserName());
        }
        return returnList;
    }

    public static String getFacebookClimateChangeWidget(String friendsterUserId) {      
        StringBuffer output = new StringBuffer(); 
       
        EarthBO earthBO = new EarthBO(); 
        OEightPledge pledge = earthBO.getLatestPledgeFromThirdParty( 
                OEightConstants.CAMPAIGN_INVITE_FROM_FACEBOOK, friendsterUserId);
        if (pledge != null)
        {
            String pledgeDescription = OEightPledgeHelper.getPledgeLabelByCode(""+pledge.getPledgeCode());
            pledgeDescription.replaceAll("&quot;","'");
                      
            output.append("<style type=\"text/css\"><!--");
            output.append(".ccTxt {text-decoration:none; font-family: tahoma, helvetica, arial, sans-serif;font-size: 14px; color: #000000;}");
            output.append(".ccTxt a {text-decoration:none; color: #000000;}");
            output.append(".ccTxt a:hover {text-decoration:none; color: #999999;}");
            output.append("--></style>"); 
            output.append("<center><table width=\"380\" height=\"180\"  border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" background=\"http://080808.org.my/earth08/widget/pledge_org.jpg\" >");
            output.append("<tr>");
            output.append("<td width=\"131\" height=\"34\">&nbsp;</td>");
            output.append("<td width=\"52\">&nbsp;</td>");
            output.append("<td width=\"165\">&nbsp;</td>");
            output.append("<td width=\"32\">&nbsp;</td>");
            output.append("</tr>");
            output.append("<tr>");
            output.append("<td height=\"38\">&nbsp;</td>");
            output.append("<td>"); 
            output.append("&nbsp;</td>"); 
            output.append("<td>&nbsp;</td>");
            output.append("<td>&nbsp;</td>");
            output.append("</tr>");
            output.append("<tr>");
            output.append("<td height=\"66\">&nbsp;</td>");
            output.append("<td colspan=\"2\" align=\"center\" valign=\"middle\" class=\"ccTxt\"><a href=\"http://earth." + ConfigurationHelper.getDomainName() + "\" target=\"_blank\">" + pledgeDescription + "</a></td>");
            output.append("<td>&nbsp;</td>");
            output.append("</tr>");
            output.append("<tr>");
            output.append("<td height=\"42\">&nbsp;</td>");
            output.append("<td>&nbsp;</td>");
            output.append("<td>&nbsp;</td>");
            output.append("<td>&nbsp;</td>");
            output.append("</tr>");
            output.append("</table>");
            output.append("<br><a href=\"http://"+ConfigurationHelper.getDomainName()+"/earth.do?act=pledgeRoom\" target=\"_blank\">Pledge Room</a>");
            output.append("<br><a href=\""+PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK6_INSTALL_URL)+"\">Grab This App</a>");
            output.append("</a></center>");
            //Unable to show below in fbml
            /*  THis should be the final FLASH movie, but facebook does not autodisplay without user clicking on it
             *so is better to scrap it and use normal image
            output.append("<fb:swf ");
            output.append("swfbgcolor='000000' ");
            output.append("imgstyle='border-width:3px; border-color:white;' ");
            output.append("swfsrc='http://080808.org.my/earth08/widget/climateChangePledge.swf' ");
            output.append("flashvars='pledge="+pledgeDescription+"' ");
            output.append("width='380' height='180' />");
            output.append("<br><a href=\"http://"+ConfigurationHelper.getDomainName()+"/earth.do?act=pledgeRoom\" target=\"_blank\">Pledge Room</a>");
            output.append("<br><a href=\""+PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK6_INSTALL_URL)+"\">Grab This App</a>");
            */

            /*
            output.append("<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\" width=\"400\" height=\"180\" id=\"Widget\" align=\"middle\">");
            output.append("<PARAM NAME=FlashVars VALUE=\"pledge="+pledgeDescription+"\">");
            output.append("<param name=\"movie\" value=\"http://080808.org.my/earth/widget/climateChangePledge.swf\" /><param name=\"quality\" value=\"high\" /><param name=\"bgcolor\" value=\"#ffffff\" /><embed src=\"http://080808.org.my/earth/widget/climateChangePledge.swf\" quality=\"high\" bgcolor=\"#ffffff\" width=\"400\" height=\"180\" name=\"Widget\" align=\"middle\"");
            output.append("FlashVars=\"pledge="+pledgeDescription+"\" type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />");
            output.append("</object>");
             */
            /*output.append("<br>Pledge Name :" + pledge.getPledgerName());
            output.append("<br>Pledge Type :" + OEightPledgeHelper.getPledgeCategoryByCode(""+pledge.getPledgeCode())); 
            output.append("<br>Pledge To   :" + OEightPledgeHelper.getPledgeLabelByCode(""+pledge.getPledgeCode())); 
            */
        } 
        else
        {
            output.append("No pledge record found");
        }
        return output.toString();
    }
}
