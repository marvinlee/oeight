/*
 * PodiumForm.java
 *
 * Created on October 18, 2007, 4:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class PodiumForm extends AbstractApplicationActionForm { 
    
    /** Creates a new instance of PodiumForm */
    public PodiumForm() {
    }
    
    private HashMap podiumHashMap;
    private List historyList; 
    private List blogList;
    private char snapshotCode;

    public HashMap getPodiumHashMap() {
        return podiumHashMap;
    }

    public void setPodiumHashMap(HashMap podiumHashMap) {
        this.podiumHashMap = podiumHashMap;
    }

    public List getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List historyList) {
        this.historyList = historyList;
    }
    
    public List getBlogList() {
        return blogList;
    }

    public void setBlogList(List blogList) {
        this.blogList = blogList;
    }

    public char getSnapshotCode() {
        return snapshotCode;
    }

    public void setSnapshotCode(char snapshotCode) {
        this.snapshotCode = snapshotCode;
    }
 
    
}
