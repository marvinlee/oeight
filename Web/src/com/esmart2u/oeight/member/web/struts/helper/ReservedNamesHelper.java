/*
 * ReservedNamesHelper.java
 *
 * Created on November 13, 2007, 10:57 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.oeight.data.ReservedNames;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author meauchyuan.lee
 */
public class ReservedNamesHelper {
    
    public static HashSet names = null;
    
    /** Creates a new instance of ReservedNamesHelper */
    public ReservedNamesHelper() {
    }
    
    public static boolean isReservedName(String userName)
    {
        boolean isReserved = true;
    
        if (names == null)
        {
            names = loadNames(); 
        }
        
        boolean checkFound = false;
        if (names.contains(userName.toLowerCase()))
        {
            checkFound = true;
        }
        isReserved = checkFound;
        return isReserved;
    }
    
    private static HashSet loadNames()
    {
            
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            List namesList = session.createQuery("from ReservedNames").list(); 
            if (namesList != null)
            {
                names = new HashSet();
                Iterator namesIterator = namesList.iterator();
                while (namesIterator.hasNext())
                {
                    ReservedNames reservedName = (ReservedNames)namesIterator.next();
                    names.add(reservedName.getNames().toLowerCase()); 
                }

            }
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return names;
    }
    
}
