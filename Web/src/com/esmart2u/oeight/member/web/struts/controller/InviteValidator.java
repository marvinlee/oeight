/*
 * InviteValidator.java
 *
 * Created on May 7, 2008, 1:50 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.helper.contactlist.ContactListHelper;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author meauchyuan.lee
 */
public class InviteValidator {
    
    private static Logger logger = Logger.getLogger(InviteValidator.class);
    
    
    /** Creates a new instance of InviteValidator */
    public InviteValidator() {
    }
     
    public static ActionErrors validateInviteRequest(ActionErrors errors, InviteForm inviteForm) {
         
        String email = inviteForm.getEmailUser() + "@" + inviteForm.getProvider();
        System.out.println("Validating email:" + email);
        
        if (!Validator.validateEmail(email,true)) { 
            errors.add("inviteRequest",new ActionError("common.invalid.emailAddress"));
        }  
        else if (!Validator.validateString(inviteForm.getPassword(),true,0,100)) {
            errors.add("inviteRequest",new ActionError("common.invalid.password"));
        }
        else if(!validProviders(email))
        {
            errors.add("inviteRequest",new ActionError("common.invalid.emailProvider")); 
        }
        
       
        return errors;
    }
     
    protected static boolean validProviders(String fullEmailAddress)
    {
        boolean isProvider = false;
        if (!StringUtils.hasValue(fullEmailAddress))
        {
            return false;
        }
        else
        {
            // Check if able to find importer
            return ContactListHelper.hasValidProvider(fullEmailAddress);
        }
    }

    static ActionErrors validateInvitesSelected(ActionErrors errors, ActionForm actionForm) {
        return null;
    }
 
}
