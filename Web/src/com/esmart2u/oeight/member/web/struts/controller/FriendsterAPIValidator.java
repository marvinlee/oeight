/*
 * FriendsterAPIValidator.java
 *
 * Created on March 22, 2008, 9:35 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserDetails;
import com.esmart2u.oeight.data.UserWidgetFriendster;
import com.esmart2u.oeight.member.bo.LoginBO;
import com.esmart2u.oeight.member.bo.RegisterBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.bo.UserFriendsterBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.logging.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterAPIValidator {
        
    private static Logger logger = Logger.getLogger(FriendsterAPIValidator.class);
    
    /** Creates a new instance of FriendsterAPIValidator */
    public FriendsterAPIValidator() {
    }
        
    public static ActionErrors validateCallback1(ActionErrors errors, FriendsterForm friendsterForm) {
        
        if (StringUtils.hasValue(friendsterForm.getUser_id()))
        {
            // If FriendsterUserId does not exists in db, is first installation
            //if (UserFriendsterBO.getByFriendsterId(friendsterForm.getUser_id(),FriendsterForm.MAIN_TYPE) == null 
            //        && "y".equals(friendsterForm.getFromInstallPage()))
            if ("y".equals(friendsterForm.getFromInstallPage()))
            { 
            
                friendsterForm.setFirstInstall("y");
                // Else do the following as this will be the request to install 
                // Ensure friendster values are available
                if (!StringUtils.hasValue(friendsterForm.getNonce()) || 
                    !StringUtils.hasValue(friendsterForm.getSession_key()) ||
                    !StringUtils.hasValue(friendsterForm.getApi_key()) ||
                    !StringUtils.hasValue(friendsterForm.getSig())) {
                    errors.add("friendsterInput",new ActionError("common.invalid.friendsterAPI.invalidparams"));
                } 
                else{
                    // Check login credentials  
                     LoginBO loginBO = null;

                        if (!Validator.validateEmail(friendsterForm.getLogin(),true)) {
                            errors.add("login",new ActionError("common.mandatory.emailAddress"));
                        }
                        if (!Validator.validateString(friendsterForm.getPassword(),true,4,12)) {
                            errors.add("password",new ActionError("common.mandatory.password"));
                        }

                        if (errors.size() < 1) {
                            // Checks password
                            loginBO = new LoginBO();


                            User userLogin= new User();
                            logger.debug("Login="+friendsterForm.getLogin());
                            userLogin.setEmailAddress(friendsterForm.getLogin()); 
                            userLogin.setSecureCode(friendsterForm.getPassword());

                            // Checks password
                            User dbUserLogin = loginBO.getUserDetails(friendsterForm.getLogin());
                            if (dbUserLogin == null) { 
                                errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                            } else { 

                                // Try use singleton for performance
                                RegisterBO registerBO = new RegisterBO();
                                boolean success = false;
                                try{
                                    success = registerBO.matchSecureCode(userLogin.getEmailAddress(), userLogin.getSecureCode(), dbUserLogin.getSecureCode());
                                } catch(Exception e) {
                                    logger.error("Exception in matching secure code");
                                }
                                registerBO = null;

                                 if (success) { 
                                    // Put user id into session
                                    if (dbUserLogin.getUserId() > 0) {
                                        friendsterForm.setUserId(""+dbUserLogin.getUserId());
                                        friendsterForm.setUserName(dbUserLogin.getUserName()); 
                                    } else {
                                        // Should not happen
                                        errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                    }


                                } else {
                                    errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                }
                            }
                            loginBO = null;

                        }  
                    
                }
                
            }else
            {
                // Already installed
                friendsterForm.setFirstInstall("n");
                // Gets the userId and userName
                UserWidgetFriendster userWidgetFriendster = UserFriendsterBO.getByFriendsterId(friendsterForm.getUser_id(),FriendsterForm.MAIN_TYPE);
                if (userWidgetFriendster != null)
                {
                    friendsterForm.setUserId(""+userWidgetFriendster.getUserId());
                    friendsterForm.setUserName(userWidgetFriendster.getUserName());
                }
                
            }
        }
        else
        { 
            errors.add("friendsterInput",new ActionError("common.invalid.friendsterAPI.invalidparams"));
        } 
        
        return errors;
    }
    
    
    
    public static ActionErrors validateCallback3(ActionErrors errors, FriendsterForm friendsterForm) {
        
        if (StringUtils.hasValue(friendsterForm.getUser_id()))
        {
            // If FriendsterUserId does not exists in db, is first installation
            //if (UserFriendsterBO.getByFriendsterId(friendsterForm.getUser_id(),FriendsterForm.MAIN_TYPE) == null 
            //        && "y".equals(friendsterForm.getFromInstallPage()))
            if ("y".equals(friendsterForm.getFromInstallPage()))
            { 
            
                friendsterForm.setFirstInstall("y");
                // Else do the following as this will be the request to install 
                // Ensure friendster values are available
                if (!StringUtils.hasValue(friendsterForm.getNonce()) || 
                    !StringUtils.hasValue(friendsterForm.getSession_key()) ||
                    !StringUtils.hasValue(friendsterForm.getApi_key()) ||
                    !StringUtils.hasValue(friendsterForm.getSig())) {
                    errors.add("friendsterInput",new ActionError("common.invalid.friendsterAPI.invalidparams"));
                } 
                else{
                    // Check login credentials  
                     LoginBO loginBO = null;

                        if (!Validator.validateEmail(friendsterForm.getLogin(),true)) {
                            errors.add("login",new ActionError("common.mandatory.emailAddress"));
                        }
                        if (!Validator.validateString(friendsterForm.getPassword(),true,4,12)) {
                            errors.add("password",new ActionError("common.mandatory.password"));
                        }

                        if (errors.size() < 1) {
                            // Checks password
                            loginBO = new LoginBO();


                            User userLogin= new User();
                            logger.debug("Login="+friendsterForm.getLogin());
                            userLogin.setEmailAddress(friendsterForm.getLogin()); 
                            userLogin.setSecureCode(friendsterForm.getPassword());

                            // Checks password
                            User dbUserLogin = loginBO.getUserDetails(friendsterForm.getLogin());
                            if (dbUserLogin == null) { 
                                errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                            } else { 

                                // Try use singleton for performance
                                RegisterBO registerBO = new RegisterBO();
                                boolean success = false;
                                try{
                                    success = registerBO.matchSecureCode(userLogin.getEmailAddress(), userLogin.getSecureCode(), dbUserLogin.getSecureCode());
                                } catch(Exception e) {
                                    logger.error("Exception in matching secure code");
                                }
                                registerBO = null;

                                 if (success) { 
                                    // Put user id into session
                                    if (dbUserLogin.getUserId() > 0) {
                                        friendsterForm.setUserId(""+dbUserLogin.getUserId());
                                        friendsterForm.setUserName(dbUserLogin.getUserName()); 
                                        
                                        // Successful login, checks blog information here (if not moderated, it will show in ScriptsAction )
                                        UserBO userBO = new UserBO();
                                        UserDetails userDetails = userBO.getUserContactById(""+dbUserLogin.getUserId());
                                        if (userDetails == null)
                                        {
                                            errors.add("blog",new ActionError("common.invalid.friendsterAPI.blogFeatureNotFound"));
                                        }
                                        else if (userDetails != null && 
                                             (!StringUtils.hasValue(userDetails.getWebBlog()) || !userDetails.getWebBlogFeature()))
                                        {
                                            errors.add("blog",new ActionError("common.invalid.friendsterAPI.blogFeatureNotFound")); 
                                        }
                                          
                                    } else {
                                        // Should not happen
                                        errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                    }


                                } else {
                                    errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                }
                            }
                            loginBO = null;

                        }  
                    
                }
                
            }else
            {
                // Already installed
                friendsterForm.setFirstInstall("n");
                // Gets the userId and userName
                UserWidgetFriendster userWidgetFriendster = UserFriendsterBO.getByFriendsterId(friendsterForm.getUser_id(),FriendsterForm.FEATURED_BLOG_TYPE);
                if (userWidgetFriendster != null)
                {
                    friendsterForm.setUserId(""+userWidgetFriendster.getUserId());
                    friendsterForm.setUserName(userWidgetFriendster.getUserName());
                }
                
            }
        }
        else
        { 
            errors.add("friendsterInput",new ActionError("common.invalid.friendsterAPI.invalidparams"));
        } 
        
        return errors;
    }
    
    
    
    public static ActionErrors validateCallback5(ActionErrors errors, FriendsterForm friendsterForm) {
        
        if (StringUtils.hasValue(friendsterForm.getUser_id()))
        {
            // If FriendsterUserId does not exists in db, is first installation
            //if (UserFriendsterBO.getByFriendsterId(friendsterForm.getUser_id(),FriendsterForm.MAIN_TYPE) == null 
            //        && "y".equals(friendsterForm.getFromInstallPage()))
            if ("y".equals(friendsterForm.getFromInstallPage()))
            { 
            
                friendsterForm.setFirstInstall("y");
                // Else do the following as this will be the request to install 
                // Ensure friendster values are available
                if (!StringUtils.hasValue(friendsterForm.getNonce()) || 
                    !StringUtils.hasValue(friendsterForm.getSession_key()) ||
                    !StringUtils.hasValue(friendsterForm.getApi_key()) ||
                    !StringUtils.hasValue(friendsterForm.getSig())) {
                    errors.add("friendsterInput",new ActionError("common.invalid.friendsterAPI.invalidparams"));
                } 
                else{
                    // Check login credentials  
                     LoginBO loginBO = null;

                        if (!Validator.validateEmail(friendsterForm.getLogin(),true)) {
                            errors.add("login",new ActionError("common.mandatory.emailAddress"));
                        }
                        if (!Validator.validateString(friendsterForm.getPassword(),true,4,12)) {
                            errors.add("password",new ActionError("common.mandatory.password"));
                        }

                        if (errors.size() < 1) {
                            // Checks password
                            loginBO = new LoginBO();


                            User userLogin= new User();
                            logger.debug("Login="+friendsterForm.getLogin());
                            userLogin.setEmailAddress(friendsterForm.getLogin()); 
                            userLogin.setSecureCode(friendsterForm.getPassword());

                            // Checks password
                            User dbUserLogin = loginBO.getUserDetails(friendsterForm.getLogin());
                            if (dbUserLogin == null) { 
                                errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                            } else { 

                                // Try use singleton for performance
                                RegisterBO registerBO = new RegisterBO();
                                boolean success = false;
                                try{
                                    success = registerBO.matchSecureCode(userLogin.getEmailAddress(), userLogin.getSecureCode(), dbUserLogin.getSecureCode());
                                } catch(Exception e) {
                                    logger.error("Exception in matching secure code");
                                }
                                registerBO = null;

                                 if (success) { 
                                    // Put user id into session
                                    if (dbUserLogin.getUserId() > 0) {
                                        friendsterForm.setUserId(""+dbUserLogin.getUserId());
                                        friendsterForm.setUserName(dbUserLogin.getUserName()); 
                                    } else {
                                        // Should not happen
                                        errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                    }


                                } else {
                                    errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                }
                            }
                            loginBO = null;

                        }  
                    
                }
                
            }else
            {
                // Already installed
                friendsterForm.setFirstInstall("n");
                // Gets the userId and userName
                UserWidgetFriendster userWidgetFriendster = UserFriendsterBO.getByFriendsterId(friendsterForm.getUser_id(),FriendsterForm.FRIENDS_XCHANGE_TYPE);
                if (userWidgetFriendster != null)
                {
                    friendsterForm.setUserId(""+userWidgetFriendster.getUserId());
                    friendsterForm.setUserName(userWidgetFriendster.getUserName());
                }
                
            }
        }
        else
        { 
            errors.add("friendsterInput",new ActionError("common.invalid.friendsterAPI.invalidparams"));
        } 
        
        return errors;
    }

    static ActionErrors validateCallback6(ActionErrors errors, FriendsterForm friendsterForm) {  

        // Gets referral from 
        String fromKey = OEightConstants.SESSION_CAMPAIGN_INVITE_FROM + OEightConstants.CAMPAIGN_INVITE_FROM_FRIENDSTER;
        Object referralObject = friendsterForm.getActionContext().getRequest().getSession().getAttribute(fromKey);
        if (referralObject != null)
        {
            String referral = (String)referralObject;
            System.out.println("Referral Session value is not null");
            if (StringUtils.hasValue(referral))
            {
                System.out.println(">>> Obtained session value:" + referral);
                friendsterForm.setNext(referral);
                friendsterForm.setReferral(referral);
            }
        }
        else
        { 
            System.out.println("Referral Session value is NULL");
        }
            
        if (StringUtils.hasValue(friendsterForm.getUser_id()))
        {
            // If FriendsterUserId does not exists in db, is first installation 
            if ("y".equals(friendsterForm.getFromInstallPage()))
            { 
            
                friendsterForm.setFirstInstall("y");
                // Else do the following as this will be the request to install 
                // Ensure friendster values are available
                if (!StringUtils.hasValue(friendsterForm.getNonce()) || 
                    !StringUtils.hasValue(friendsterForm.getSession_key()) ||
                    !StringUtils.hasValue(friendsterForm.getApi_key()) ||
                    !StringUtils.hasValue(friendsterForm.getSig())) {
                    errors.add("friendsterInput",new ActionError("common.invalid.friendsterAPI.invalidparams"));
                } 
                 
                if (!Validator.validateInteger(Integer.toString(friendsterForm.getPledgeCode()),true,1,999)) {
                    errors.add("pledgeCode",new ActionError("common.invalid.earthcampaign.pledgeCode"));
                }
                
                
                //!!! IMPORTANT, only do login if user opted to
                 // Check login credentials  
                if (StringUtils.hasValue(friendsterForm.getLogin()) &&
                    StringUtils.hasValue(friendsterForm.getPassword())){
                        LoginBO loginBO = null;

                        if (!Validator.validateEmail(friendsterForm.getLogin(),true)) {
                            errors.add("login",new ActionError("common.mandatory.emailAddress"));
                        }
                        if (!Validator.validateString(friendsterForm.getPassword(),true,4,12)) {
                            errors.add("password",new ActionError("common.mandatory.password"));
                        }

                        if (errors.size() < 1) {
                            // Checks password
                            loginBO = new LoginBO();


                            User userLogin= new User();
                            logger.debug("Login="+friendsterForm.getLogin());
                            userLogin.setEmailAddress(friendsterForm.getLogin()); 
                            userLogin.setSecureCode(friendsterForm.getPassword());

                            // Checks password
                            User dbUserLogin = loginBO.getUserDetails(friendsterForm.getLogin());
                            if (dbUserLogin == null) { 
                                errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                            } else { 

                                // Try use singleton for performance
                                RegisterBO registerBO = new RegisterBO();
                                boolean success = false;
                                try{
                                    success = registerBO.matchSecureCode(userLogin.getEmailAddress(), userLogin.getSecureCode(), dbUserLogin.getSecureCode());
                                } catch(Exception e) {
                                    logger.error("Exception in matching secure code");
                                }
                                registerBO = null;

                                 if (success) { 
                                    // Put user id into session
                                    if (dbUserLogin.getUserId() > 0) {
                                        friendsterForm.setUserId(""+dbUserLogin.getUserId());
                                        friendsterForm.setUserName(dbUserLogin.getUserName()); 
                                    } else {
                                        // Should not happen
                                        errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                    }


                                } else {
                                    errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                                }
                            }
                            loginBO = null;

                        }  
                    
                }
                
            }else
            {
                // Already installed
                friendsterForm.setFirstInstall("n");  
            }
        }
        else
        { 
            errors.add("friendsterInput",new ActionError("common.invalid.friendsterAPI.invalidparams"));
        } 
        return errors;
    }
    
    
    
}
