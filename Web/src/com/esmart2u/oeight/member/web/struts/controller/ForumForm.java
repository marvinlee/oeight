/*
 * ForumForm.java
 *
 * Created on October 18, 2008, 2:14 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class ForumForm extends AbstractApplicationActionForm {
    
    private long forumTopicId;
    private long forumId;
    private String forumName;
    private long topicId;
    private String topicUrl;
    private String topicPost;
    
    private List forumDropDown;
    private List topicDropDown;
    
    private HashMap forumMap;
    
    /** Creates a new instance of ForumForm */
    public ForumForm() {
    }
    
    public long getForumTopicId() {
        return forumTopicId;
    }

    public void setForumTopicId(long forumTopicId) {
        this.forumTopicId = forumTopicId;
    }

    public long getForumId() {
        return forumId;
    }

    public void setForumId(long forumId) {
        this.forumId = forumId;
    }

    public String getForumName() {
        return forumName;
    }

    public void setForumName(String forumName) {
        this.forumName = forumName;
    }

    public long getTopicId() {
        return topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public String getTopicUrl() {
        return topicUrl;
    }

    public void setTopicUrl(String topicUrl) {
        this.topicUrl = topicUrl;
    }

    public String getTopicPost() {
        return topicPost;
    }

    public void setTopicPost(String topicPost) {
        this.topicPost = topicPost;
    }

    public List getForumDropDown() {
        return forumDropDown;
    }

    public void setForumDropDown(List forumDropDown) {
        this.forumDropDown = forumDropDown;
    }

    public List getTopicDropDown() {
        return topicDropDown;
    }

    public void setTopicDropDown(List topicDropDown) {
        this.topicDropDown = topicDropDown;
    }

    public HashMap getForumMap() {
        return forumMap;
    }

    public void setForumMap(HashMap forumMap) {
        this.forumMap = forumMap;
    }
    
}
