/*
 * LoginForm.java
 *
 * Created on September 19, 2007, 11:11 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.member.bo.RegisterBO;
import com.esmart2u.solution.base.helper.StringUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
/**
 *
 * @author meauchyuan.lee
 */
public class LoginForm  extends AbstractApplicationActionForm {
    private String userId;
    private String login;
    private String password;
    private String confirmPassword;
    private String resetCode;
    private String secureCode;
    
    private String newPassword; 
    
    private String q;
    
    // Forum login usage
    private String username;
    
    /**
     * Clears the object prior to each request.
     *
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        this.clear();
    }
    
    /**
     * getDescription() retrieves <code>password</code> field.
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * getName() retrieves <code>login</code> field.
     */
    public String getLogin() {
        return login;
    }
    
    /**
     * setDescription() sets <code>password</code> field.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Sets <code>login</code> field.
     */
    public void setLogin(String login) {
        if(StringUtils.hasValue(login))
        {
            login = login.toLowerCase().trim();
        }
        this.login = login;
    }
    
    /**
     *
     * clear() sets all the object's fields to <code>null</code>.
     *
     */
    public void clear() {
        this.login = null;
        this.password = null;
        this.userId = null;
    }
    
    public String getUserId() {
        return userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getResetCode() {
        return resetCode;
    }

    public void setResetCode(String resetCode) {
        this.resetCode = resetCode;
    }
      
    public void setSecureCodeFromPassword() throws Exception {
        if (StringUtils.hasValue(login)&&StringUtils.hasValue(password)) {
            RegisterBO registerBO = new RegisterBO();
            secureCode = registerBO.getSecureCode(login, password);
        } else
            throw new Exception("Invalid password");
    }

    public String getSecureCode() {
        return secureCode;
    }

    public void setSecureCode(String secureCode) {
        this.secureCode = secureCode;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        if (StringUtils.hasValue(username))
        {
            setLogin(username);
        }
        this.username = username;
    }
 
    
}
