/*
 * DateObjectHelper.java
 *
 * Created on September 30, 2007, 4:37 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.helper;

import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

/**
 *
 * @author meauchyuan.lee
 */
public class DateObjectHelper {
    
    public static int MINUMUM_AGE_BIRTH_YEAR = PropertyManager.getInt(PropertyConstants.SYSTEM_MIN_AGE_BIRTH_YEAR,1993);
     
    private static String[] days = {"01","02","03","04","05","06","07","08","09","10",
    "11","12","13","14","15","16","17","18","19","20",
    "21","22","23","24","25","26","27","28","29","30","31"};
    private static String[] monthLabels = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    private static String[] monthValues = {"01","02","03","04","05","06","07","08","09","10","11","12"};
    private static String[] years = getAvailableYears();
    
    public static String DISPLAY_TIMESTAMP_FORMAT = "dd/MMM/yyyy hh:mm a";
    public static SimpleDateFormat timestampFormatter = new SimpleDateFormat(DISPLAY_TIMESTAMP_FORMAT);
    
    public static String DISPLAY_STRING_FORMAT = "dd/MMM/yyyy";
    public static SimpleDateFormat dateFormatter = new SimpleDateFormat(DISPLAY_STRING_FORMAT);
    
    public static String CURRENT_MONTH_STRING_FORMAT = "MM/yyyy";
    public static SimpleDateFormat monthFormatter = new SimpleDateFormat(CURRENT_MONTH_STRING_FORMAT);
    
    public static String MYSQL_DATE_STRING_FORMAT = "yyyy-MM-dd 00:00:00";
    public static SimpleDateFormat mysqlDateFormatter = new SimpleDateFormat(MYSQL_DATE_STRING_FORMAT);
            
    public static String DISPLAY_MONTH_STRING_FORMAT = "MMM yyyy";
    public static SimpleDateFormat displayMonthFormatter = new SimpleDateFormat(DISPLAY_MONTH_STRING_FORMAT);
             
    public static String REFERENCE_NO_TIMESTAMP_FORMAT = "yyyyMMddhhmmss";
    public static SimpleDateFormat referenceNumberFormatter = new SimpleDateFormat(REFERENCE_NO_TIMESTAMP_FORMAT);
    
    /** Creates a new instance of DateObjectHelper */
    public DateObjectHelper() {
    }
    
    private static String[] getAvailableYears() {
        int available = MINUMUM_AGE_BIRTH_YEAR-1927;
        String[] years = new String[available];
        
        for(int i=0;i<available;i++) {
            years[i] = Integer.toString(1927 + i);
        }
        return years;
    }
    
    public static Vector getDaysCombo() {
        Vector daysCombo = new Vector(2);
        daysCombo.add(days);
        daysCombo.add(days);
        return daysCombo;
    }
    
    public static Vector getMonthsCombo() {
        Vector monthsCombo = new Vector(2);
        monthsCombo.add(monthLabels);
        monthsCombo.add(monthValues);
        return monthsCombo;
    }
    
    public static Vector getYearsCombo() {
        Vector yearsCombo = new Vector(2);
        yearsCombo.add(years);
        yearsCombo.add(years);
        return yearsCombo;
    }
    
    public static int getAge(long birthday) { 
        Calendar age = Calendar.getInstance(Locale.getDefault());
        age.setTimeInMillis(Math.abs(birthday-System.currentTimeMillis()));
        //System.out.println("Your are "+(age.get(Calendar.YEAR)-1970)+" years, "+age.get(Calendar.MONTH)+" month, "+age.get(Calendar.DAY_OF_MONTH)+" days "+(System.currentTimeMillis()-birthday<0 ?"Not Born Yet" :"Old")+".");
        //System.out.println("Age is " + (age.get(Calendar.YEAR)-1970));
        return (age.get(Calendar.YEAR)-1970);
    }
    
    public static String getPrintedDate(Date date)
    {
        return dateFormatter.format(date);
    }
    
    public static String getPrintedTimestamp(Date date)
    {
        return timestampFormatter.format(date);
    }  
    
    public static String getReferenceNumberTimestamp(Date date)
    {
        return referenceNumberFormatter.format(date);
    }
    
    public static Date getCurrentDayDateObject()
    {
        Date today = null;
        String todayString = dateFormatter.format(new Date()); 
        try{
            today = dateFormatter.parse(todayString);
        }catch (Exception e)
        {
            System.out.println("Error parsing date");
        }
        return today;
    } 
    
    public static Date getYesterdayDateObject()
    {
        Date yesterday = null;
        long oneDayDifference = 1000 * 60 * 60 * 24;
        String yesterdayString = dateFormatter.format(new Date(System.currentTimeMillis() - oneDayDifference)); 
        try{
            yesterday = dateFormatter.parse(yesterdayString);
        }catch (Exception e)
        {
            System.out.println("Error parsing date");
        }
        return yesterday;
    }
    
    public static Date getCurrentMonthDateObject()
    {
        Date currentMonth = null;
        String currentMonthString = monthFormatter.format(new Date()); 
        try{
            currentMonth = monthFormatter.parse(currentMonthString);
        }catch (Exception e)
        {
            System.out.println("Error parsing date");
        }
        return currentMonth;
    }
    
    public static Date getPreviousMonthDateObject()
    {
        Date previousMonth = null;
        long oneMonthDifference = 2855316843l;
        String previousMonthString = monthFormatter.format(new Date(System.currentTimeMillis() - oneMonthDifference)); 
        try{
            previousMonth = monthFormatter.parse(previousMonthString);
            System.out.println("Previous Month=" + previousMonth);
        }catch (Exception e)
        {
            System.out.println("Error parsing date");
        }
        return previousMonth;
    }
    
    public static String getNextMonthFirstDaySQLString()
    { 
        long oneMonthDifference = 2855316843l;
        Date nextMonthDate = new Date(System.currentTimeMillis() + oneMonthDifference); 
        Calendar cal = Calendar.getInstance();
        cal.setTime(nextMonthDate);
        cal.set(Calendar.DAY_OF_MONTH, 1); 
        String nextMonthDateStr = mysqlDateFormatter.format(cal.getTime());  
         
        return nextMonthDateStr; 
    }
    
    public static String getMonthForDisplay(Date date)
    {
        return displayMonthFormatter.format(date);
    }
}
