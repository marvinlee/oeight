/*
 * ScriptsAction.java
 *
 * Created on January 14, 2008, 1:45 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.web.struts.helper.WidgetHelper;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.StringUtils;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class ScriptsAction  extends AbstractApplicationAction {
      
    private static String MAIN = "main";
    private static String IFRAME = "iframe";
    private static String NOT_FOUND = "not_found";
     
    private static String BLOG = "blog"; 
    private static String BUDDIES = "buddies";
    private static String WISHES = "wishes";
    
    private static String FRIENDSTER_FRIEND = "friendsterFriends";
    private static String FRIENDSTER_PHOTOS = "friendsterPhoto"; //Shows only in oEight
    private static String FRIENDSTER_PLEDGE = "friendsterPledge";
    
    private static String FACEBOOK_FRIEND = "facebookFriends";
    private static String FACEBOOK_PHOTOS = "facebookPhoto"; //Shows only in oEight
    private static String FACEBOOK_PLEDGE = "facebookPledge";
    
    /** Creates a new instance of ScriptsAction */
    public ScriptsAction() {
    }
    
     /**
     * Logger.
     */
    
    private static Logger logger = Logger.getLogger(ScriptsAction.class);
    
    
    
    protected boolean isValidationRequired(String event) { 
        
        return false;
    } 
    
     
    
    
    protected String getDefaultActionName() {
                  
        return MAIN;
    }
     
    public String doMain(ScriptsForm actionForm) throws ApplicationException { 
       
        try {
            logger.debug("Generate widget page"); 
            
            // Get User (Id is userName) and get details
            String userId = actionForm.getId();
            UserBO userBO = new UserBO();
            User user = null;
            
            String widgetMainHTML = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(userId)) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                } 
                  
                
            }else {
                    return NOT_FOUND; 
            }
            
            widgetMainHTML = WidgetHelper.getMainWidget(user); 
            userBO = null; 
            
            actionForm.setWidgetMainHTML(widgetMainHTML);
            
            if (IFRAME.equals(actionForm.getType()))
            {
                return IFRAME;
            }
            return MAIN;
        } catch (Exception e) {
            return NOT_FOUND;
        } 
    }  
    
    public String doBlog(ScriptsForm actionForm) throws ApplicationException { 
       
        try {
            logger.debug("Generate widget blog page"); 
            
            // Get User (Id is userName) and get details
            String userId = actionForm.getId();
            UserBO userBO = new UserBO();
            User user = null;
            
            String widgetMainHTML = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(userId)) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                } 
                  
                
            }else {
                    return NOT_FOUND; 
            }
            
            widgetMainHTML = WidgetHelper.getBlogWidget(user); 
            userBO = null; 
            
            actionForm.setWidgetBlogHTML(widgetMainHTML);
            
            if (IFRAME.equals(actionForm.getType()))
            {
                return IFRAME;
            }
            return BLOG;
        } catch (Exception e) {
            return NOT_FOUND;
        } 
    } 
    
    
    
    public String doWishes(ScriptsForm actionForm) throws ApplicationException { 
       
        try {
            logger.debug("Generate widget wishes page"); 
            
            // Get User (Id is userName) and get details
            String userId = actionForm.getId();
            UserBO userBO = new UserBO();
            User user = null;
            
            String widgetMainHTML = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(userId)) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                } 
                  
                
            }else {
                    return NOT_FOUND; 
            }
            
            widgetMainHTML = WidgetHelper.getWishesWidget(user); 
            userBO = null; 
            
            actionForm.setWidgetMainHTML(widgetMainHTML);
            
            if (IFRAME.equals(actionForm.getType()))
            {
                return IFRAME;
            }
            return WISHES;
        } catch (Exception e) {
            return NOT_FOUND;
        } 
    } 
    
    
    public String doBuddies(ScriptsForm actionForm) throws ApplicationException { 
       
        try {
            logger.debug("Generate widget buddies page"); 
            
            // Get User (Id is userName) and get details
            String userId = actionForm.getId();
            UserBO userBO = new UserBO();
            User user = null;
            
            String widgetMainHTML = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(userId)) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                } 
                  
                
            }else {
                    return NOT_FOUND; 
            }
            
            widgetMainHTML = WidgetHelper.getBuddiesWidget(user); 
            userBO = null; 
            
            actionForm.setWidgetMainHTML(widgetMainHTML);
            
            if (IFRAME.equals(actionForm.getType()))
            {
                return IFRAME;
            }
            return BUDDIES;
        } catch (Exception e) {
            return NOT_FOUND;
        } 
    } 
    
    
    public String doFriendsterFriends(ScriptsForm actionForm) throws ApplicationException { 
       
        try {
            logger.debug("Generate widget Friendster Friends page"); 
            
            // Get User (Id is userName) and get details
            String userId = actionForm.getId();
            UserBO userBO = new UserBO();
            User user = null;
            
            String widgetMainHTML = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(userId)) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }  
                
            }else {
                    return NOT_FOUND; 
            }
            
            widgetMainHTML = WidgetHelper.getFriendsterFriendWidget(user); 
            userBO = null; 
            
            actionForm.setWidgetMainHTML(widgetMainHTML);
            
            if (IFRAME.equals(actionForm.getType()))
            {
                return IFRAME;
            }
            return FRIENDSTER_FRIEND;
        } catch (Exception e) {
            return NOT_FOUND;
        } 
    } 
    
    
    public String doFriendsterPhoto(ScriptsForm actionForm) throws ApplicationException { 
       
        try {
            logger.debug("Generate widget Friendster Photo page"); 
            
            // Get User (Id is userName) and get details
            String userId = actionForm.getId();
            UserBO userBO = new UserBO();
            User user = null;
            
            String widgetMainHTML = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(userId)) {
               
                user = userBO.getUserByUserName(actionForm.getUserName());
                if (user == null){
                    return NOT_FOUND;
                }  
                
            }else {
                    return NOT_FOUND; 
            }
            
            widgetMainHTML = WidgetHelper.getFriendsterPhotoWidget(user, actionForm.isShowMyself()); 
            userBO = null; 
            
            actionForm.setWidgetMainHTML(widgetMainHTML);
            
            if (IFRAME.equals(actionForm.getType()))
            {
                return IFRAME;
            }
            return FRIENDSTER_PHOTOS;
        } catch (Exception e) {
            return NOT_FOUND;
        } 
    } 
    
    
    public String doFriendsterPledge(ScriptsForm actionForm) throws ApplicationException { 
       
        try {
            logger.debug("Generate widget page"); 
            
            // Get for this app is FriendsterId
            String userId = actionForm.getId();  
            
            String widgetMainHTML = null;
            // Get id/userName, if available, is view others profile, else view own
            if (StringUtils.hasValue(userId)) { 
            }else {
                    return NOT_FOUND; 
            }
            
            List valueList = WidgetHelper.getFriendsterClimateChangeWidget(userId);  
            widgetMainHTML = (String)valueList.get(0);             
            actionForm.setWidgetMainHTML(widgetMainHTML);
            
            //Optional referral name could be available
            if (valueList.size() > 1)
            {
                actionForm.setSessionValue((String)valueList.get(1));
            }
            
            if (IFRAME.equals(actionForm.getType()))
            {
                return IFRAME;
            }
            return FRIENDSTER_PLEDGE;
        } catch (Exception e) {
            return NOT_FOUND;
        } 
    }  
}
