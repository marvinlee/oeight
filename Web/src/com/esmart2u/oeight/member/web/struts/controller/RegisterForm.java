/*
 * RegisterForm.java
 *
 * Created on September 20, 2007, 11:56 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.member.bo.RegisterBO;
import com.esmart2u.solution.base.helper.BeanUtils;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
/**
 *
 * @author meauchyuan.lee
 */
public class RegisterForm  extends AbstractApplicationActionForm {
    
    
    private static Logger logger = Logger.getLogger(RegisterForm.class);
    
    // First page info
    private long userId;
    private String userName;
    private String emailAddress;
    private String secureCode;
    private String password;
    private String confirmPassword; 
    
    // Second page info
    private String name;
    private String nric;
    private char gender;
    private Date birthDate;
    private String birthDay;
    private String birthMonth;
    private String birthYear;
    private String birthdayString;
    private String city;
    private String state;
    private String nationality;
    private String currentLocation;
    private String schools;
    private String occupation;
    private String company;
    private String publicEmail;
    private FormFile photoFile;
    private String photoPath;
    private String photoSmallPath;
    private String photoLargePath;
    private String photoDescription;
    private char systemStatus;
    private Date systemRegistered;
    private char reportAbuse;
    private char skip2;
    private char skip3;
    private char skip4;
    
    // Page 4
    private String emailFwd1;
    private String emailFwd2;
    private String emailFwd3;
    
    // Invite
    private String invitedBy;
    private String inviteCode;
    private String referralId;
    private String referralName;
    private char inviteError;
    private char fromRequest;
    private char emailValidated = PropertyConstants.BOOLEAN_NO;
    
    // Recaptcha fields
    private String recaptcha_challenge_field;
    private String recaptcha_response_field;
    
    /**
     * Clears the object prior to each request.
     *
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        this.clear();  
    }
    
    
    /**
     *
     * clear() sets all the object's fields to <code>null</code>.
     *
     */
    public void clear() {  
    }
    
    
    /** Creates a new instance of RegisterForm */
    public RegisterForm() {
    }
    
    public long getUserId() {
        return userId;
    }
    
    public void setUserId(long userId) {
        this.userId = userId;
    }
    
    public String getUserName() {
        return userName;
    }
    
    public void setUserName(String userName) {
        if (StringUtils.hasValue(userName))
        {
            userName = userName.toLowerCase().trim();
        }
        this.userName = userName;
    }
    
    public String getEmailAddress() {
        return emailAddress;
    }
    
    public void setEmailAddress(String email) {
        if (StringUtils.hasValue(email))
        {
            email = email.toLowerCase().trim();
        }
        this.emailAddress = email;
    }
    
    public String getSecureCode() {
        return secureCode;
    }
    
    public void setSecureCode(String secureCode) {
        this.secureCode = secureCode;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getNric() {
        return nric;
    }
    
    public void setNric(String nric) {
        this.nric = nric;
    }
    
    public char getGender() {
        return gender;
    }
    
    public void setGender(char gender) {
        this.gender = gender;
    }
    
    public Date getBirthDate() {
        return birthDate;
    }
    
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    public String getBirthdayString() {
        if (StringUtils.hasValue(birthDay) && StringUtils.hasValue(birthMonth) && StringUtils.hasValue(birthYear)) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                birthDate = sdf.parse(birthDay + "/" + birthMonth + "/" + birthYear);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            birthdayString = sdf.format(birthDate);
        }
        
        return birthdayString;
    }
    
    public void setBirthdayString(String birthdayString) {
        this.birthdayString = birthdayString;
    }
    
    public String getCity() {
        return city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getState() {
        return state;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    public String getNationality() {
        return nationality;
    }
    
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
    
    public String getCurrentLocation() {
        return currentLocation;
    }
    
    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }
    
    public String getSchools() {
        return schools;
    }
    
    public void setSchools(String schools) {
        this.schools = schools;
    }
    
    public String getOccupation() {
        return occupation;
    }
    
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
    
    public String getCompany() {
        return company;
    }
    
    public void setCompany(String company) {
        this.company = company;
    }
    
    public String getPublicEmail() {
        return publicEmail;
    }
    
    public void setPublicEmail(String publicEmail) {
        this.publicEmail = publicEmail;
    }
    
    public FormFile getPhotoFile() {
        return photoFile;
    }
    
    public void setPhotoFile(FormFile photoFile) {
        this.photoFile = photoFile;
    }
    
    public String getPhotoPath() {
        return photoPath;
    }
    
    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }
    
    public String getPhotoSmallPath() {
        return photoSmallPath;
    }
    
    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }
    
    public String getPhotoLargePath() {
        return photoLargePath;
    }
    
    public void setPhotoLargePath(String photoLargePath) {
        this.photoLargePath = photoLargePath;
    }
    
    public String getPhotoDescription() {
        return photoDescription;
    }
    
    public void setPhotoDescription(String photoDescription) {
        this.photoDescription = photoDescription;
    }
    
    public char getSystemStatus() {
        return systemStatus;
    }
    
    public void setSystemStatus(char systemStatus) {
        this.systemStatus = systemStatus;
    }
    
    public Date getSystemRegistered() {
        return systemRegistered;
    }
    
    public void setSystemRegistered(Date systemRegistered) {
        this.systemRegistered = systemRegistered;
    }
    
    public char getReportAbuse() {
        return reportAbuse;
    }
    
    public void setReportAbuse(char reportAbuse) {
        this.reportAbuse = reportAbuse;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getConfirmPassword() {
        return confirmPassword;
    }
    
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
    
    public String getEmailFwd1() {
        return emailFwd1;
    }
    
    public void setEmailFwd1(String emailFwd1) {
        this.emailFwd1 = emailFwd1;
    }
    
    public String getEmailFwd2() {
        return emailFwd2;
    }
    
    public void setEmailFwd2(String emailFwd2) {
        this.emailFwd2 = emailFwd2;
    }
    
    public String getEmailFwd3() {
        return emailFwd3;
    }
    
    public void setEmailFwd3(String emailFwd3) {
        this.emailFwd3 = emailFwd3;
    }
    
    public String printValues() {
        HttpSession session = getActionContext().getRequest().getSession();
        StringBuffer output = new StringBuffer();
        output.append("-----------------------  System Info -------------------\n");
        output.append("User Id=" + userId + "\n");
        output.append("User Name=" + userName + "\n");
        output.append("Email=" + emailAddress + "\n");
        output.append("Secure Code=" + secureCode + "\n");
        output.append("Password=" + password + "\n");
        output.append("Confirm Password=" + confirmPassword + "\n");
        
        // Second page info
        output.append("-----------------------  General Info -------------------\n");
        output.append("Name=" + name + "\n");
        output.append("NRIC=" + nric + "\n");
        output.append("Gender=" + gender + "\n");
        output.append("B'day =" + birthDate + "\n");
        output.append("B'day Str=" + birthdayString + "\n");
        output.append("City=" + city + "\n");
        output.append("State=" + state + "\n");
        output.append("Nationality=" + nationality + "\n");
        output.append("Curr Loc=" + currentLocation + "\n");
        output.append("Schools =" + schools + "\n");
        output.append("Occupation=" + occupation + "\n");
        output.append("Company=" + company + "\n");
        output.append("Pub Email=" + publicEmail + "\n");
        output.append("Photo Pth=" + photoPath + "\n");
        output.append("Photo s Path=" + photoSmallPath + "\n");
        output.append("Photo l Path=" + photoLargePath + "\n");
        output.append("Photo Desc=" + photoDescription + "\n");
        output.append("System status=" + systemStatus + "\n");
        output.append("System Registered=" + systemRegistered + "\n");
        output.append("Report Abuse=" + reportAbuse + "\n");
        output.append("Skip 2=" + skip2 + "\n");
        output.append("Skip 3=" + skip3 + "\n");
        output.append("Skip 4=" + skip4 + "\n");
        
        // Page 4
        output.append("-----------------------  Invites Info -------------------\n");
        output.append("Email 1=" + emailFwd1 + "\n");
        output.append("Email 2=" + emailFwd2 + "\n");
        output.append("Email 3=" + emailFwd3 + "\n");
        
        return output.toString();
    }
    
    public User toUserObject() throws Exception {
        User user = new User();
        BeanUtils.copyBean(this, user);
        return user;
    }
    public UserCountry toUserCountryObject() throws Exception {
        UserCountry userCountry = new UserCountry();
        BeanUtils.copyBean(this, userCountry);
        return userCountry;
    }
    
    
    public void setSecureCodeFromPassword() throws Exception {
        if (StringUtils.hasValue(emailAddress)&&StringUtils.hasValue(password)) {
            RegisterBO registerBO = new RegisterBO();
            secureCode = registerBO.getSecureCode(emailAddress, password);
        } else
            throw new Exception("Invalid password");
    }
    
    public String getBirthDay() {
        return birthDay;
    }
    
    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }
    
    public String getBirthMonth() {
        return birthMonth;
    }
    
    public void setBirthMonth(String birthMonth) {
        this.birthMonth = birthMonth;
    }
    
    public String getBirthYear() {
        return birthYear;
    }
    
    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getInvitedBy() {
        return invitedBy;
    }

    public void setInvitedBy(String invitedBy) {
        this.invitedBy = invitedBy;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getReferralId() {
        return referralId;
    }

    public void setReferralId(String referralId) {
        this.referralId = referralId;
    }

    public String getReferralName() {
        return referralName;
    }

    public void setReferralName(String referralName) {
        this.referralName = referralName;
    }

    public char getInviteError() {
        return inviteError;
    }

    public void setInviteError(char inviteError) {
        this.inviteError = inviteError;
    }

    public String getRecaptcha_challenge_field() {
        return recaptcha_challenge_field;
    }

    public void setRecaptcha_challenge_field(String recaptcha_challenge_field) {
        this.recaptcha_challenge_field = recaptcha_challenge_field;
    }

    public String getRecaptcha_response_field() {
        return recaptcha_response_field;
    }

    public void setRecaptcha_response_field(String recaptcha_response_field) {
        this.recaptcha_response_field = recaptcha_response_field;
    }
 

    public char getSkip2() {
        return skip2;
    }

    public void setSkip2(char skip2) {
        this.skip2 = skip2;
    }

    public char getSkip3() {
        return skip3;
    }

    public void setSkip3(char skip3) {
        this.skip3 = skip3;
    }

    public char getSkip4() {
        return skip4;
    }

    public void setSkip4(char skip4) {
        this.skip4 = skip4;
    }

    public char getFromRequest() {
        return fromRequest;
    }

    public void setFromRequest(char fromRequest) {
        this.fromRequest = fromRequest;
    }

    public char getEmailValidated() {
        return emailValidated;
    }

    public void setEmailValidated(char emailValidated) {
        this.emailValidated = emailValidated;
    }
 
    
    
}
