/*
 * AdvertiseAction.java
 *
 * Created on January 12, 2008, 2:30 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class AdvertiseAction  extends AbstractApplicationAction {
      
    private static String MAIN = "main";
   
    /** Creates a new instance of AdvertiseAction */
    public AdvertiseAction() {
    }
    
       /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(AdvertiseAction.class);
    
    
    
    protected boolean isValidationRequired(String event) { 
        
        return false;
    } 
    
     
    
    
    protected String getDefaultActionName() {
                  
        return MAIN;
    }
     
    public String doMain(CommonForm actionForm){ 
        return MAIN;  
    } 
    
}
