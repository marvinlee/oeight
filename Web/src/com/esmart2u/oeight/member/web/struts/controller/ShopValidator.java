/*
 * ShopValidator.java
 *
 * Created on February 13, 2008, 3:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.solution.base.helper.Validator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

/**
 *
 * @author meauchyuan.lee
 */
public class ShopValidator { 
    
    private static Logger logger = Logger.getLogger(ShopValidator.class);
    
    /** Creates a new instance of ShopValidator */
    public ShopValidator() {
    }
     
    public static ActionErrors validateCoupleTee(ActionErrors errors, ShopForm shopForm) {
      
        if (!Validator.validateString(shopForm.getName(),true,1,100,false)) {
            errors.add("name",new ActionError("common.invalid.shopName"));
        }  
        if (!Validator.validatePhoneNumber(shopForm.getContactNo(),true,null)) { 
            errors.add("contactNo",new ActionError("common.invalid.shopContactNo"));
        } 
        /*if (!Validator.validateEmail(shopForm.getEmail(),true)) {
            errors.add("email",new ActionError("common.invalid.shopEmail"));
        } */
        if (!Validator.validateString(shopForm.getAddress(),true,1,200,false)) {
            errors.add("address",new ActionError("common.invalid.shopAddress"));
        }  
        if (!Validator.validateString(shopForm.getF(),true,1,3,true)) {
            errors.add("girlSize",new ActionError("common.invalid.shopGirlSize"));
        }  
        if (!Validator.validateString(shopForm.getM(),true,1,3,true)) {
            errors.add("guySize",new ActionError("common.invalid.shopGuySize"));
        }  
        
        return errors;
        
    }
}
