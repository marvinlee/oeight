/*
 * FriendsterForm.java
 *
 * Created on March 22, 2008, 9:11 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.member.vo.UserFriendsterVO;
import com.esmart2u.solution.base.helper.BeanUtils;
import com.esmart2u.solution.base.logging.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterForm  extends AbstractApplicationActionForm { 
    
    private static Logger logger = Logger.getLogger(FriendsterForm.class);
    
    public static final String MAIN_TYPE = "1";
    public static final String WISH_BUDDIES_TYPE = "2";
    public static final String FEATURED_BLOG_TYPE = "3";
    public static final String WHAT_SAY_U_TYPE = "4";
    public static final String FRIENDS_XCHANGE_TYPE = "5";
    public static final String CLIMATE_CHANGE_TYPE = "6";
    
    // oEight values  
    private String userId;
    private String userName;
    private String login;
    private String password;
    
    // Friendster values
    private String user_id;
    private String nonce;
    private String session_key;
    private String api_key;
    private String sig;
    
    private String fromInstallPage;
    private String firstInstall;
    
    private String next; //Keeps the referring oEightId
    private String referral;
     
    
    // For climate change pledge App
    private int pledgeCode;
    
    /** Creates a new instance of FriendsterForm */
    public FriendsterForm() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getSession_key() {
        return session_key;
    }

    public void setSession_key(String session_key) {
        this.session_key = session_key;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    void setUserFriendsterVO(UserFriendsterVO userFriendsterVO) { 
        try {
            
            BeanUtils.copyBean(userFriendsterVO, this);  
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }

    UserFriendsterVO getUserFriendsterVO() {
        UserFriendsterVO vo = new UserFriendsterVO();
        vo.setUserId(this.getUserId());
        vo.setUserName(this.getUserName());
        vo.setLogin(this.getLogin());
        vo.setPassword(this.getPassword());
        vo.setUser_id(this.getUser_id());
        vo.setNonce(this.getNonce());
        vo.setSession_key(this.getSession_key());
        vo.setApi_key(this.getApi_key());
        vo.setSig(this.getSig()); 
        vo.setNext(this.getNext());
                
        return vo;
    }

    public String getFirstInstall() {
        return firstInstall;
    }

    public void setFirstInstall(String firstInstall) {
        this.firstInstall = firstInstall;
    }

    public String getFromInstallPage() {
        return fromInstallPage;
    }

    public void setFromInstallPage(String fromInstallPage) {
        this.fromInstallPage = fromInstallPage;
    }

    public int getPledgeCode() {
        return pledgeCode;
    }

    public void setPledgeCode(int pledgeCode) {
        this.pledgeCode = pledgeCode;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    
}
