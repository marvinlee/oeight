/*
 * FacebookForm.java
 *
 * Created on April 2, 2008, 4:16 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.member.vo.UserFacebookVO;
import com.esmart2u.solution.base.helper.BeanUtils;
import com.esmart2u.solution.base.logging.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class FacebookForm extends AbstractApplicationActionForm { 
    
    
    private static Logger logger = Logger.getLogger(FacebookForm.class);
    
    public static final String MAIN_TYPE = "1";
    public static final String WISH_BUDDIES_TYPE = "2";
    public static final String FEATURED_BLOG_TYPE = "3";
    public static final String WHAT_SAY_U_TYPE = "4";
    public static final String FRIENDS_XCHANGE_TYPE = "5";
    public static final String CLIMATE_CHANGE_TYPE = "6";
    
    // oEight values  
    private String userId;
    private String userName;
    private String login;
    private String password;
    
    // Facebook values
    private String fb_user;
    private String api_key; 
    private String auth_token;
    private String fb_session_key; 
    private String fb_call_id;
    private String fb_sig;
    
    private String fb_sig_session_key;
    private String fb_sig_user;
    private String fb_sig_api_key;
    
     
    private String reloginApiKey;
    private String fromInstallPage;
    private String firstInstall;
     
    // For climate change pledge App
    private int pledgeCode; 
    
    
    /** Creates a new instance of FacebookForm */
    public FacebookForm() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
 

    public String getFromInstallPage() {
        return fromInstallPage;
    }

    public void setFromInstallPage(String fromInstallPage) {
        this.fromInstallPage = fromInstallPage;
    }

    public String getFirstInstall() {
        return firstInstall;
    }

    public void setFirstInstall(String firstInstall) {
        this.firstInstall = firstInstall;
    }
       
    void setUserFacebookVO(UserFacebookVO userFacebookVO) { 
        try {
            
            BeanUtils.copyBean(userFacebookVO, this);  
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Error in copy bean properties");
        }
        
    }

    UserFacebookVO getUserFacebookVO() {
        UserFacebookVO vo = new UserFacebookVO();
        vo.setUserId(this.getUserId());
        vo.setUserName(this.getUserName());
        vo.setLogin(this.getLogin());
        vo.setPassword(this.getPassword());
        vo.setUid(this.getFb_user()); 
        vo.setSessionId(this.getFb_session_key());
        vo.setApiKey(this.getApi_key()); 
        vo.setAuthToken(this.getAuth_token());
                
        return vo;
    }

    public String getFb_user() {
        return fb_user;
    }

    public void setFb_user(String fb_user) {
        this.fb_user = fb_user;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getFb_session_key() {
        return fb_session_key;
    }

    public void setFb_session_key(String fb_session_key) {
        this.fb_session_key = fb_session_key;
    }

    public String getFb_call_id() {
        return fb_call_id;
    }

    public void setFb_call_id(String fb_call_id) {
        this.fb_call_id = fb_call_id;
    }

    public String getFb_sig() {
        return fb_sig;
    }

    public void setFb_sig(String fb_sig) {
        this.fb_sig = fb_sig;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getReloginApiKey() {
        return reloginApiKey;
    }

    public void setReloginApiKey(String reloginApiKey) {
        this.reloginApiKey = reloginApiKey;
    }

    public String getFb_sig_session_key() {
        return fb_sig_session_key;
    }

    public void setFb_sig_session_key(String fb_sig_session_key) {
        this.fb_sig_session_key = fb_sig_session_key;
        this.fb_session_key = fb_sig_session_key;
    }

    public String getFb_sig_user() {
        return fb_sig_user;
    }

    public void setFb_sig_user(String fb_sig_user) {
        this.fb_sig_user = fb_sig_user;
        this.fb_user = fb_sig_user;
    }

    public String getFb_sig_api_key() {
        return fb_sig_api_key;
    }

    public void setFb_sig_api_key(String fb_sig_api_key) {
        this.fb_sig_api_key = fb_sig_api_key;
        this.api_key = fb_sig_api_key;
    }

    public int getPledgeCode() {
        return pledgeCode;
    }

    public void setPledgeCode(int pledgeCode) {
        this.pledgeCode = pledgeCode;
    }
 

}
