/*
 * LoginAction.java
 *
 * Created on September 19, 2007, 11:09 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.web.struts.controller;

import com.esmart2u.oeight.data.LoginReset;
import com.esmart2u.oeight.data.phpbb.PhpBbUsers;
import com.esmart2u.oeight.member.bo.ForumBO;
import com.esmart2u.oeight.member.bo.ForumBO;
import com.esmart2u.oeight.member.bo.LoginBO;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.RegisterBO;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.helper.smtp.ResetPasswordEmailSender;
import com.esmart2u.solution.base.helper.AppContext;
import com.esmart2u.solution.base.helper.ApplicationException;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import java.util.Date;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author meauchyuan.lee
 */
public class LoginAction extends AbstractApplicationAction {
    
    private static String FORM_PAGE = "form";
    private static String LOGIN_PAGE = "login";
    private static String RELOGIN_PAGE = "relogin";
    private static String LOGOUT_PAGE = "logout";
    private static String RESET = "reset";
    private static String RESET_SUBMITTED = "resetSubmitted";
    private static String RESET_PWD = "resetPassword";
    private static String RESET_PWD_SUBMITTED = "resetPassed";
    private static String CHANGE = "change";
    private static String CHANGE_SUBMITTED = "changeSubmitted";
    
    // Earth Campaign
    private static String EARTH_FORM_PAGE = "earthForm";
    private static String EARTH_LOGIN_PAGE = "earthLogin";
    
    // Wedding Campaign
    private static String WEDDING_FORM_PAGE = "weddingForm";
    private static String WEDDING_LOGIN_PAGE = "weddingLogin";
     
    // Forum 
    private static String FORUM_FORM_PAGE = "forumForm";
    private static String FORUM_LOGIN_PAGE = "forumLogin";
    
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(LoginAction.class);
    
    public String doForm(LoginForm actionForm)
    throws ApplicationException {
        logger.debug("Going back to first page");
        logger.debug("Errors=" + actionForm.getActionContext().getActionErrors().size());
        return FORM_PAGE;
    } 
    
    public String doEarthForm(LoginForm actionForm)
    throws ApplicationException { 
        logger.debug("Errors=" + actionForm.getActionContext().getActionErrors().size());
        return EARTH_FORM_PAGE;
    }
     
    public String doWeddingForm(LoginForm actionForm)
    throws ApplicationException { 
        logger.debug("Errors=" + actionForm.getActionContext().getActionErrors().size());
        return WEDDING_FORM_PAGE;
    }  
    
    public String doForumForm(LoginForm actionForm)
    throws ApplicationException { 
        logger.debug("Errors=" + actionForm.getActionContext().getActionErrors().size());
        return FORUM_FORM_PAGE;
    }
    
    public String doLogin(LoginForm loginForm)
    throws ApplicationException {
        try {
            logger.debug("Running doLogin");
            
            // Login session persists into db for reporting 
            String applicationId = (String)((AppContext)loginForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            logger.debug("Session Id=" + applicationId);
            logger.debug("User Id from form =" + loginForm.getUserId());
            logger.debug("User Id from sess =" + loginForm.getActionContext().getRequest().getSession().
                    getAttribute("userId"));
            logger.debug("User Id from ctx  =" + loginForm.getActionContext().getAppContext().get(OEightConstants.SESSION_USER_ID));
            
            String referrer = loginForm.getActionContext().getRequest().getHeader("Referer");
            logger.debug("Referrer=" + referrer );
            if (referrer != null && 
                    (referrer.startsWith("http://"+ConfigurationHelper.getDomainName()) ||
                    referrer.startsWith("http://www."+ConfigurationHelper.getDomainName()))
                    )
            { 
                referrer = referrer.replace("http://"+ConfigurationHelper.getDomainName()+"/secure/login.jsp","");
                referrer = referrer.replace("http://"+ConfigurationHelper.getDomainName()+"/secure/login.do","");
                loginForm.setQ(referrer);
            }
            
            return LOGIN_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, FORM_PAGE);
        }
    }
    
    public String doEarthLogin(LoginForm loginForm)
    throws ApplicationException {
        try {
            logger.debug("Running doEarthLogin");
            
            // Login session persists into db for reporting 
            String applicationId = (String)((AppContext)loginForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            logger.debug("Session Id=" + applicationId);
            logger.debug("User Id from form =" + loginForm.getUserId());
            logger.debug("User Id from sess =" + loginForm.getActionContext().getRequest().getSession().
                    getAttribute("userId"));
            logger.debug("User Id from ctx  =" + loginForm.getActionContext().getAppContext().get(OEightConstants.SESSION_USER_ID));
            
            String referrer = loginForm.getActionContext().getRequest().getHeader("Referer");
            logger.debug("Referrer=" + referrer );
            if (referrer != null && 
                    (referrer.startsWith("http://"+ConfigurationHelper.getDomainName()) ||
                    referrer.startsWith("http://www."+ConfigurationHelper.getDomainName()))
                    )
            { 
                referrer = referrer.replace("http://"+ConfigurationHelper.getDomainName()+"/secure/earth_login.jsp","");
                referrer = referrer.replace("http://"+ConfigurationHelper.getDomainName()+"/secure/login.do","");
                loginForm.setQ(referrer);
            }
            
            return EARTH_LOGIN_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, EARTH_FORM_PAGE);
        }
    }
   
    public String doWeddingLogin(LoginForm loginForm)
    throws ApplicationException {
        try {
            logger.debug("Running doWeddingLogin");
            
            // Login session persists into db for reporting 
            String applicationId = (String)((AppContext)loginForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            logger.debug("Session Id=" + applicationId);
            logger.debug("User Id from form =" + loginForm.getUserId());
            logger.debug("User Id from sess =" + loginForm.getActionContext().getRequest().getSession().
                    getAttribute("userId"));
            logger.debug("User Id from ctx  =" + loginForm.getActionContext().getAppContext().get(OEightConstants.SESSION_USER_ID));
            
            String referrer = loginForm.getActionContext().getRequest().getHeader("Referer");
            logger.debug("Referrer=" + referrer );
            if (referrer != null && 
                    (referrer.startsWith("http://"+ConfigurationHelper.getDomainName()) ||
                    referrer.startsWith("http://www."+ConfigurationHelper.getDomainName()))
                    )
            { 
                referrer = referrer.replace("http://"+ConfigurationHelper.getDomainName()+"/secure/wedding_login.jsp","");
                referrer = referrer.replace("http://"+ConfigurationHelper.getDomainName()+"/secure/login.do","");
                loginForm.setQ(referrer);
            }
            
            return WEDDING_LOGIN_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, WEDDING_FORM_PAGE);
        }
    }
    
    public String doForumLogin(LoginForm loginForm)
    throws ApplicationException {
        try {
            logger.debug(">>>>>>>>>>>>> Running doForumLogin");
            
            System.out.println("Username:" + loginForm.getUsername()); 
            //System.out.println("Login:" + loginForm.getLogin());
            //System.out.println("Password:" + loginForm.getPassword());
            String sessionId = loginForm.getActionContext().getRequest().getSession().getId();
            System.out.println("SessionId : " + sessionId);
            if (StringUtils.hasValue(sessionId))
            {
                System.out.println("SessionId length : " + sessionId.length());
            } 
             
            String applicationId = (String)((AppContext)loginForm.getActionContext().getAppContext()).
                    get(OEightConstants.SESSION_APPLICATION_ID_KEY);
            logger.debug("Session Id=" + applicationId);
            if (!StringUtils.hasValue(applicationId)){
                loginForm.getActionContext().getAppContext().put(OEightConstants.SESSION_APPLICATION_ID_KEY, sessionId.toUpperCase());
            }
            logger.debug("User Id from form =" + loginForm.getUserId());
            logger.debug("User Id from sess =" + loginForm.getActionContext().getRequest().getSession(false).getAttribute("userId"));
            logger.debug("User Id from ctx  =" + loginForm.getActionContext().getAppContext().get(OEightConstants.SESSION_USER_ID));
            
            String referrer = loginForm.getActionContext().getRequest().getHeader("Referer");
            logger.debug("Referrer=" + referrer );
            if (referrer != null && 
                    (referrer.startsWith("http://"+ConfigurationHelper.getDomainName()) ||
                    referrer.startsWith("http://www."+ConfigurationHelper.getDomainName()))
                    )
            { 
                referrer = referrer.replace("http://forum."+ConfigurationHelper.getDomainName()+"/community/login.php","");
                referrer = referrer.replace("http://"+ConfigurationHelper.getDomainName()+"/secure/login.do","");
                loginForm.setQ(referrer);
            }
            System.out.println(">>>LoginAction returnMapping with sessionId:" + sessionId);
            
            return FORUM_LOGIN_PAGE;
        } catch (Exception e) {
            
            return rethrow(e, FORUM_FORM_PAGE);
        }
    }
    
    public String doLogout(LoginForm loginForm)
    throws ApplicationException {
        try {
            logger.debug("Running doLogout");
            // Reserve login and logout tracking
            
            // Clear session values
            String sessionId = loginForm.getActionContext().getRequest().getSession().getId();
            String userId = (String)loginForm.getActionContext().getRequest().getSession().
                    getAttribute(OEightConstants.SESSION_USER_ID);
            String ipAddress = (String)loginForm.getActionContext().getRequest().getRemoteAddr();
            LoginBO loginBO = new LoginBO();
            loginBO.updateLoginSession(userId, ipAddress);
            loginBO = null;
            
            try {                
                // Logout from Forum
                setForumLogoutCookies(loginForm, sessionId);
            } catch (Exception ex) {
                ex.printStackTrace();
                // Do nothing if error in forum logout
            }
            
            //logger.debug("User Id from sess =" + userId);
            loginForm.getActionContext().getRequest().getSession().invalidate();
            //logger.debug("User Id from sess =" + loginForm.getActionContext().getRequest().getSession().
            //        getAttribute(OEightConstants.SESSION_USER_ID));
            
            return LOGOUT_PAGE;
        } catch (Exception e) {
            logger.error("Error in logout="+e.getMessage());
            return LOGOUT_PAGE;
        }
    } 
    
    
    
    protected boolean isValidationRequired(String event) {
        boolean result = false;
        
        logger.debug("In isValidationRequired for " + event);
        if (LOGIN_PAGE.equals(event)|| EARTH_LOGIN_PAGE.equals(event)|| WEDDING_LOGIN_PAGE.equals(event)
            || FORUM_LOGIN_PAGE.equals(event)
            || RESET_PWD.equals(event) || RESET_PWD_SUBMITTED.equals(event) || CHANGE_SUBMITTED.equals(event))
            return true;
        
        
        return result;
    }
    
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
        ActionErrors errors = new ActionErrors();
        System.out.println("In validateParameter of LoginAction");
        LoginForm loginForm = (LoginForm)actionForm;
        if (LOGIN_PAGE.equals(action) || EARTH_LOGIN_PAGE.equals(action)
            || WEDDING_LOGIN_PAGE.equals(action) || FORUM_LOGIN_PAGE.equals(action)) {   
            String ipAddress = loginForm.getActionContext().getRequest().getRemoteAddr();
            String referrer = loginForm.getActionContext().getRequest().getHeader("referer"); 
            boolean loginSuccessful = false;
                            
            logger.debug("Validate =" + action + "with:" + loginForm.getLogin() + "/" + loginForm.getPassword());
            LoginBO loginBO = null;
            
            if (!Validator.validateEmail(loginForm.getLogin(),true)) {
                errors.add("login",new ActionError("common.mandatory.emailAddress"));
            }
            if (!Validator.validateString(loginForm.getPassword(),true,4,12)) {
                errors.add("password",new ActionError("common.mandatory.password"));
            }
            
            if (errors.size() < 1) {
                // Checks password
                loginBO = new LoginBO();
                
                
                User userLogin= new User();
                logger.debug("Login="+loginForm.getLogin());
                userLogin.setEmailAddress(loginForm.getLogin());
                //logger.debug("Password="+loginForm.getPassword());
                userLogin.setSecureCode(loginForm.getPassword());
                
                // Checks password
                User dbUserLogin = loginBO.getUserDetails(loginForm.getLogin());
                if (dbUserLogin == null) {
                    //System.out.println("User not found");
                    errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                    // Log failure, no such user in system
                    loginBO.insertFailedSession(true,  dbUserLogin.getUserId(), loginForm.getLogin(), loginForm.getPassword(), ipAddress, referrer);
                } else {
                    //System.out.println("User found!");
                    
                    // Try use singleton for performance
                    RegisterBO registerBO = new RegisterBO();
                    boolean success = false;
                    try{
                        success = registerBO.matchSecureCode(userLogin.getEmailAddress(), userLogin.getSecureCode(), dbUserLogin.getSecureCode());
                    } catch(Exception e) {
                        logger.error("Exception in matching secure code");
                    }
                    registerBO = null;
                    
                    //System.out.println("Try to match password " + userLogin.getSecureCode() + " with " + dbUserLogin.getSecureCode());
                    if (success) {
                        //System.out.println("Password match");
                        // Put user id into session
                        if (dbUserLogin.getUserId() > 0) {
                            loginForm.setUserId("" + dbUserLogin.getUserId());
                            loginForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_USER_ID,"" + dbUserLogin.getUserId());
                            loginForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_USER_NAME,"" + dbUserLogin.getUserName());                            
                            String sessionId = loginForm.getActionContext().getRequest().getSession().getId();
                            loginForm.getActionContext().getRequest().getSession().setAttribute(OEightConstants.SESSION_APPLICATION_ID_KEY, sessionId.toUpperCase());
                            try {
                                
                                // Set forum login
                                setForumLoginCookies(loginForm, sessionId);
                            } catch (Exception ex) {
                                ex.printStackTrace(); 
                                errors.add("login",new ActionError("common.error.loginForum"));
                            }
                            
                            // Logs login success into db
                            loginSuccessful = true;
                        } else {
                            // Should not happen
                            errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                        }
                        
                        
                    } else {
                        loginBO.insertFailedSession(true,  dbUserLogin.getUserId(), loginForm.getLogin(), loginForm.getPassword(), ipAddress, referrer);
                        errors.add("passwordMismatch",new ActionError("common.mismatch.userPassword"));
                    }
                }
                loginBO.insertLoginSession(loginSuccessful, loginForm.getLogin(), loginForm.getUserId(), dbUserLogin,  ipAddress, referrer);
                loginBO = null;
                
            } 
            
        } else  if (RESET_PWD.equals(action)) {
            String email = loginForm.getLogin();
            String resetCode = loginForm.getResetCode();
            
            LoginReset loginReset = new LoginReset();
            loginReset.setEmail(email);
            loginReset.setResetCode(resetCode);
            LoginBO loginBO = new LoginBO();
            
            if (!loginBO.validReset(loginReset)) {
                errors.add("resetInvalid",new ActionError("common.invalid.resetPassword"));
            }
            
        } else  if (RESET_PWD_SUBMITTED.equals(action)) {
            String email = loginForm.getLogin();
            String resetCode = loginForm.getResetCode();
            
            LoginReset loginReset = new LoginReset();
            loginReset.setEmail(email);
            loginReset.setResetCode(resetCode);
            LoginBO loginBO = new LoginBO();
            
            if (!loginBO.validReset(loginReset)) {
                errors.add("resetInvalid",new ActionError("common.invalid.resetPassword"));
            }
            
            if (!Validator.validateString(loginForm.getPassword(),true,4,12)) {
                errors.add("password",new ActionError("common.invalid.password"));
            }
            if (!Validator.validateString(loginForm.getConfirmPassword(),true,4,12)) {
                errors.add("confirmPassword",new ActionError("common.invalid.confirmPassword"));
            }
            
            if (errors.size() < 1) { 
                
                // Checks password
                if (!loginForm.getPassword().equals(loginForm.getConfirmPassword())) {
                    errors.add("confirmPasswordMismatch",new ActionError("common.mismatch.confirmPassword"));
                }
                
            }
        } else  if (CHANGE_SUBMITTED.equals(action)) {
           RegisterBO registerBO = null;
            boolean success = false;
            try{ 
                // Get user id, check match current password            
                String userId = checkUserAccess(loginForm); 
                UserBO userBO = new UserBO();
                User dbUserLogin = userBO.getUserById(userId);  
                

                 // Try use singleton for performance
                registerBO = new RegisterBO(); 
                success = registerBO.matchSecureCode(dbUserLogin.getEmailAddress(), loginForm.getPassword(), dbUserLogin.getSecureCode() );
                loginForm.setLogin(dbUserLogin.getEmailAddress());
            } catch(Exception e) {
                logger.error("Exception in matching secure code");
            }
            registerBO = null;

            if (success) {  

                if (!Validator.validateString(loginForm.getNewPassword(),true,4,12)) {
                    errors.add("newPassword",new ActionError("common.invalid.newPassword"));
                }
                if (!Validator.validateString(loginForm.getConfirmPassword(),true,4,12)) {
                    errors.add("confirmPassword",new ActionError("common.invalid.confirmPassword"));
                }

                if (errors.size() < 1) { 

                    // Checks password
                    if (!loginForm.getNewPassword().equals(loginForm.getConfirmPassword())) {
                        errors.add("confirmPasswordMismatch",new ActionError("common.mismatch.confirmPassword"));
                    }
                    else
                    {
                        // No errors
                        loginForm.setSecureCode(null);
                        loginForm.setPassword(loginForm.getNewPassword());
                    }

                }
            
            }
            else
            {
                // Current password does not match
                errors.add("existingPasswordMismatch",new ActionError("common.mismatch.existingPassword"));
            }
        } else {
            logger.debug("Temporary skip");
        }
        
        loginForm.getActionContext().setActionErrors(errors);
        return loginForm;
    }
    
    protected String getDefaultEvent(String action) {
        logger.debug("Going back to previous action");
        if (LOGIN_PAGE.equals(action)) {
            action = FORM_PAGE;
        } else if (RESET_PWD.equals(action)) {
            action = RESET_PWD;
        } else if (RESET_PWD_SUBMITTED.equals(action)) {
            action = RESET_PWD;
        } else if (CHANGE_SUBMITTED.equals(action)) {
            action = CHANGE;
        } else if (EARTH_LOGIN_PAGE.equals(action)) {
            action = EARTH_FORM_PAGE;
        } else if (WEDDING_LOGIN_PAGE.equals(action)) {
            action = WEDDING_FORM_PAGE;
        } else if (FORUM_LOGIN_PAGE.equals(action)) {
            action = FORUM_FORM_PAGE;
        } else {
            logger.debug("Temporary skip");
        }
        return action;
    }
    
    
    public String doReset(LoginForm actionForm)
    throws ApplicationException {
        try {
            
            actionForm.setLogin(null);
            return RESET;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN_PAGE); 
            return RELOGIN_PAGE;
        }
    }
    
    
    public String doResetSubmitted(LoginForm actionForm)
    throws ApplicationException {
        try {
            
            // Save email list to DB for scheduler to send emails
            LoginBO loginBO = new LoginBO();
            // Save a request to db
            logger.debug("Requesting reset password for=" + actionForm.getLogin());
            actionForm.setLogin(loginBO.requestReset(actionForm.getLogin()));
            if (StringUtils.hasValue(actionForm.getLogin())) {
                // Uncomment for actual sending
                ResetPasswordEmailSender.getInstance().invoke();
            }
            loginBO = null;
            
            return RESET_SUBMITTED;
        } catch (Exception e) { 
            //return rethrow(e, RELOGIN_PAGE);
            return RELOGIN_PAGE; 
        }
    }
    
    public String doResetPassword(LoginForm actionForm)
    throws ApplicationException {
        try {
            String email = actionForm.getLogin();
            String resetCode = actionForm.getResetCode();
            
            LoginReset loginReset = new LoginReset();
            loginReset.setEmail(email);
            loginReset.setResetCode(resetCode); 
            
            return RESET_PWD;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN_PAGE); 
            return RELOGIN_PAGE; 
        }
    }
    
    public String doResetPassed(LoginForm actionForm)
    throws ApplicationException {
        try {
            
            if (!StringUtils.hasValue(actionForm.getSecureCode())){
                actionForm.setSecureCodeFromPassword();
                actionForm.setPassword(null);
                actionForm.setConfirmPassword(null);
            }
            // Save and update password
            
            User user = new User();
            user.setEmailAddress(actionForm.getLogin());
            user.setSecureCode(actionForm.getSecureCode());
            LoginBO loginBO = new LoginBO();
            loginBO.updateUserPassword(user);
            
            return RESET_PWD_SUBMITTED;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN_PAGE); 
            return RELOGIN_PAGE; 
        }
    }
    
    
    public String doChange(LoginForm actionForm)
    throws ApplicationException {
        try {
            String userId = checkUserAccess((LoginForm)actionForm); 
            actionForm.setPassword(""); // Current one
            actionForm.setResetCode(""); 
            actionForm.setNewPassword(""); // New Password
            actionForm.setConfirmPassword(""); // Confirm New Password 
            
            
            return CHANGE;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN_PAGE); 
            return RELOGIN_PAGE; 
        }
    }
    
    public String doChangeSubmitted(LoginForm actionForm)
    throws ApplicationException {
        try { 
            String userId = checkUserAccess((LoginForm)actionForm); 
            if (!StringUtils.hasValue(actionForm.getSecureCode())){
                actionForm.setSecureCodeFromPassword();
                actionForm.setPassword(null);
                actionForm.setConfirmPassword(null);
            }
            // Save and update password
            
            User user = new User();
            user.setEmailAddress(actionForm.getLogin());
            user.setSecureCode(actionForm.getSecureCode());
            LoginBO loginBO = new LoginBO();
            loginBO.updateUserPassword(user);
            
            return CHANGE_SUBMITTED;
        } catch (Exception e) {
            //return rethrow(e, RELOGIN_PAGE); 
            return RELOGIN_PAGE; 
        }
    }
    
    private void setForumLoginCookies(LoginForm loginForm, String sessionId) throws Exception
    { 
            Cookie[] cookies = loginForm.getActionContext().getRequest().getCookies();
            System.out.println("Try to print cookies");
            if (cookies != null)
            {
               for(int i=0;i<cookies.length; i++)
               {
                    Cookie cookie = (Cookie)cookies[i];
                    System.out.println("Cookie : " + cookie.getDomain() + ":"+ cookie.getName() + ":" + cookie.getValue());
                    cookie.setDomain("." + ConfigurationHelper.getDomainName());
                    cookie.setPath("/");
                    cookie.setMaxAge(-1);//Not stored persistently, deletes when exited 
                    // Remove phpbb2mysql_sid if found
                    if (cookie.getName().equals(OEightConstants.PHPBB_COOKIE_SESSION_ID_KEY))
                    {
                        System.out.println("Found cookie to delete!!");
                        cookie.setValue(sessionId.toLowerCase());
                        //cookie.setMaxAge(0);// Delete cookie
                    }
                    
                    loginForm.getActionContext().getResponse().addCookie(cookie);
               }
            
            }
            // Sets session id to cookie and save to phpbb
            Cookie phpbbCookie = new Cookie(OEightConstants.PHPBB_COOKIE_SESSION_ID_KEY, sessionId.toLowerCase());
            phpbbCookie.setDomain("." + ConfigurationHelper.getDomainName());
            phpbbCookie.setPath("/");
            phpbbCookie.setMaxAge(-1);//Not stored persistently, deletes when exited
            System.out.println("New Cookie1 Set " + phpbbCookie.getDomain() + ":" + phpbbCookie.getName() + ":" + phpbbCookie.getValue() );
            ForumBO forumBO = new ForumBO();
            PhpBbUsers phpBbUser = forumBO.performForumLogin(loginForm.getLogin(), sessionId.toLowerCase());
            forumBO = null;
            loginForm.getActionContext().getResponse().addCookie(phpbbCookie);
            System.out.println("Cookie1 set!!");
            
            if (phpBbUser != null){
                phpbbCookie = new Cookie(OEightConstants.PHPBB_COOKIE_USER_ID_KEY, "" + phpBbUser.getUserId());
                phpbbCookie.setDomain("." + ConfigurationHelper.getDomainName());
                phpbbCookie.setPath("/");
                phpbbCookie.setMaxAge(-1);//Not stored persistently, deletes when exited
                System.out.println("New Cookie1 Set " + phpbbCookie.getDomain() + ":" + phpbbCookie.getName() + ":" + phpbbCookie.getValue() );

                loginForm.getActionContext().getResponse().addCookie(phpbbCookie);
            }
            
    }
    
    
    private void setForumLogoutCookies(LoginForm loginForm, String sessionId) throws Exception
    { 
        System.out.println("Performing Forum logout");
        Cookie[] cookies = loginForm.getActionContext().getRequest().getCookies();
        System.out.println("Try to print cookies");
        if (cookies != null)
        {
           for(int i=0;i<cookies.length; i++)
           {
                Cookie cookie = (Cookie)cookies[i];
                System.out.println("Cookie : " + cookie.getDomain() + ":"+ cookie.getName() + ":" + cookie.getValue());
                cookie.setDomain("." + ConfigurationHelper.getDomainName());
                cookie.setPath("/");
                cookie.setMaxAge(0);//Deletes cookie

                loginForm.getActionContext().getResponse().addCookie(cookie);
           }

        }

        ForumBO forumBO = new ForumBO();
        PhpBbUsers phpBbUser = forumBO.performForumLogout(sessionId.toLowerCase());
        forumBO = null;  
            
    }
    
    
    protected String getDefaultActionName() {
                  
        return FORM_PAGE;
    }
}
