/*
 * InviteBO.java
 *
 * Created on October 21, 2007, 12:24 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.UserInvites;
import com.esmart2u.oeight.member.helper.EmailAddressHelper;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.WishVO;
import com.esmart2u.oeight.member.web.struts.controller.RegisterForm;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.helper.contactlist.Contact;
import com.esmart2u.solution.base.helper.contactlist.ContactImpl;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

/**
 *
 * @author meauchyuan.lee
 */
public class InviteBO {
    
    private static Logger logger = Logger.getLogger(InviteBO.class);
    
    /** Creates a new instance of InviteBO */
    public InviteBO() {
    }
    
    public static List saveAddresses(String userId, String emailAddresses) {
        HashSet emailSet = new HashSet();
        
        // Forms email address string to a list of valid emails
        /* Previous old email extractor, might have problem
        emailAddresses = emailAddresses.replaceAll(","," ");
        emailAddresses = emailAddresses.replaceAll(";"," ");
        StringTokenizer tokenizer = new StringTokenizer(emailAddresses," ");
        while (tokenizer.hasMoreTokens()) {
            String email = tokenizer.nextToken().trim();
            if (Validator.validateEmail(email,true)) {
                emailSet.add(email); //Set ensures no repeat
            }
        }*/
        // New email extractor
        emailSet = EmailAddressHelper.getEmailAddress(emailAddresses);
        
        List emailList = null;
        if (!emailSet.isEmpty()) {
            emailList = new ArrayList();
            emailList.addAll(emailSet);
        }
        
        // Save into DB, ignore duplicates
        List savedList = insertAddressesIntoDB(userId, emailList);
        
        return savedList;
    }
    
    private static List insertAddressesIntoDB(String userId, List emailList){
        String oeightId = PropertyManager.getValue(PropertyConstants.SYSTEM_USERID_KEY);
        boolean requestFromSystem  = oeightId.equals(userId)? true:false;
        List savedList = null;
        int count = 0;
        
        if (emailList != null && !emailList.isEmpty()){
            // Loop each and insert into db
            logger.debug("Loop to insert into db");
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                savedList = new ArrayList();
                
                Date dateSent = new Date();
                //User user = new UserBO().getUserById(userId);
                for(int i=0;i<emailList.size();i++) {
                    String email = (String)emailList.get(i);
                    UserInvites dbInvite = (UserInvites) session.createCriteria(UserInvites.class)
                    .add(Expression.eq( "userId", Long.parseLong(userId)))
                    .add(Expression.eq( "email", email ))
                    .uniqueResult();
                    
                    if (dbInvite == null) {
                        UserInvites invite = new UserInvites();
                        //user.setUserId(Long.parseLong(userId));
                        //invite.setUser(user);
                        invite.setUserId(Long.parseLong(userId));
                        invite.setEmail(email);
                        invite.setInviteCode(getInviteCode(userId, email));
                        invite.setStatus(OEightConstants.MAIL_STATUS_NEW);
                        invite.setDateSent(dateSent);
                        savedList.add(invite);
                        logger.debug("Adding into db " + invite.getEmail());
                        session.save(invite);
                        count++; // Add count if only if no errors
                    } else // invite already exists
                    {
                        // If user is requesting from the site, we resend invite by updating mail status to New again
                        if (requestFromSystem) {
                            // May 1st, after change to open registration, should not happen to be another invite link
                            dbInvite.setStatus(OEightConstants.MAIL_STATUS_NEW);
                            session.save(dbInvite);
                            savedList.add(dbInvite);
                        }
                    }
                    
                    
                }// end for
                
                if (!requestFromSystem){
                    // Add the counter to current month sent invites
                    long currentCount = RankingBO.increaseInvitesSent(userId, session, count);
                }
                
                
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
        }
        return savedList;
    }
    
    private static String getInviteCode(String userId, String email) {
        String concat = userId + email;
        String hash = null;
        try{
            MessageDigest m=MessageDigest.getInstance("MD5");
            m.update(concat.getBytes(),0,concat.length());
            hash = new BigInteger(1,m.digest()).toString(16);
            m=null;
        } catch (NoSuchAlgorithmException nse) {
            logger.error("Error Creating password for invite to " + email);
            System.out.println("Error Creating for invite to " + email);
        }
        return hash;
    }
    
    public List getUserInvitesList(String userId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        Transaction transaction = null;
        List invitationList = null;
        try {
            
            transaction = session.beginTransaction();
            invitationList = (List) session.createCriteria(UserInvites.class)
            .add(Expression.eq( "userId", Long.parseLong(userId)))
            .addOrder( Order.asc("dateSent") )
            .list();
            
            //session.getTransaction().commit();
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return invitationList;
    }
    
    public boolean validateInvite(RegisterForm registerForm) {
        // Record exists
        String referralUserName = registerForm.getInvitedBy();
        String inviteCode = registerForm.getInviteCode();
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction();
        Transaction transaction = null;
        UserInvites userInvite = null;
        try {
            
            transaction = session.beginTransaction();
            Criteria crit = session.createCriteria(UserInvites.class);
            crit.add(Expression.eq( "inviteCode", inviteCode));
            crit.createAlias("user","usr");
            crit.add(Expression.eq( "usr.userName", referralUserName));
            userInvite = (UserInvites)crit.uniqueResult();
            
            //session.getTransaction().commit();
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        if (userInvite == null) {
            return false;
        }
        
        // Is not previously accepted, must be S status
        long userId = userInvite.getUserId();
        char status = userInvite.getStatus();
        // Set the email address (if only first time, if user edited email, leave it as it is)
        if (!StringUtils.hasValue(registerForm.getEmailAddress())) {
            registerForm.setEmailAddress(userInvite.getEmail());
        }
        
        // If invite is from system, we set it as accepted
        // Form values are required for ReferralId, InviteCode, EmailAddress
        if (PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY) == userId) {
            UserBO userBO = new UserBO();
            User registerUser = userBO.getUserByEmail(userInvite.getEmail());
            registerForm.setReferralId("" +userId);
            registerForm.setInviteCode(userInvite.getInviteCode());
            registerForm.setUserId(registerUser.getUserId());
        }
        
        if ((userId > 0) && (OEightConstants.MAIL_STATUS_SENT == status)) {
            logger.debug("Match found, is a valid invite");
            return true;
        }
        
        return false;
    }
    
    /**
     *  Returns true if registration is from email link, false if is a self registration
     *  Another possiblity, is from email link but user change email address, then we need to validate again.
     */
    public static boolean updateAcceptedInvite(String referralUserId, String inviteCode, String emailAddress) {
        boolean success = false;
        // We might need to think of the condition where referral is in without invite code
        // If referralUserId is system AND invite code is null, gen invite code and save
        if ((PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY) == Integer.parseInt(referralUserId)) &&
                !StringUtils.hasValue(inviteCode)) {
            inviteCode = getInviteCode(referralUserId,emailAddress);
            List emailList = new ArrayList();
            emailList.add(emailAddress);
            insertAddressesIntoDB(referralUserId, emailList);
            return false;
        } else {
            // This is from email link, so we just validate it
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                Criteria crit = session.createCriteria(UserInvites.class);
                crit.add(Expression.eq( "userId", Long.parseLong(referralUserId)));
                crit.add(Expression.eq( "inviteCode", inviteCode));
                UserInvites userInvite = (UserInvites)crit.uniqueResult();
                
                userInvite.setStatus(OEightConstants.MAIL_STATUS_ACCEPTED);
                userInvite.setDateAccepted(new Date());
                
                if (userInvite.getEmail().equals(emailAddress)) {
                    session.update(userInvite);
                /*session.getTransaction().commit();
                if (session.isOpen()){
                    session.close();
                }*/
                    success = true;
                } else {
                    // But if email found is not same with registered
                    String systemReferralId = "" + PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY);
                    inviteCode = getInviteCode(systemReferralId,emailAddress);
                    List emailList = new ArrayList();
                    emailList.add(emailAddress);
                    insertAddressesIntoDB(systemReferralId, emailList);
                    success = false;
                }
                
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
        }
        return success;
    }
    
    /**
     *  Loop through the contactList of ContactImpl objects and check with db.
     *  If found, set relevant fields to display at UI.
     */
    public List getMembersListFromContacts(List contactList) {
        List membersList = new ArrayList();
        
        if (contactList != null && !contactList.isEmpty()) {
            UserBO userBO = new UserBO();
            User user = null;
            // Find
            for(int i=0;i<contactList.size();i++) {
                ContactImpl contact = (ContactImpl)contactList.get(i);
                String contactEmail = contact.getEmailAddress();
                if (StringUtils.hasValue(contactEmail)){
                    user = userBO.getUserByEmail(contactEmail.trim().toLowerCase());
                    if (user != null) {
                        // Indeed is a member
                        UserCountry userCountry = user.getUserCountry();
                        contact.setUserName(user.getUserName());
                        contact.setUserId(user.getUserId());
                        contact.setPhotoSmallPath(userCountry.getPhotoSmallPath());
                        
                        membersList.add(contact);
                    }
                }
            }
            userBO = null;
        }
        
        return membersList;
    }
    
    /**
     *  This method filters out non-members from contactList
     */
    public List getNonMembersListFromContacts(List contactList, List membersList) {
        
        List nonMembersList = new ArrayList();
        
        HashSet memberSet = new HashSet();
        // Find the contacts not in membersList
        
        // Put all members list email in hashset
        if (membersList != null && !membersList.isEmpty()) {
            memberSet = new HashSet();
            for(int i=0;i<membersList.size();i++) {
                ContactImpl memberContact = (ContactImpl)membersList.get(i);
                memberSet.add(memberContact.getEmailAddress());
            }
        }
        
        
        if (contactList != null && !contactList.isEmpty()) {
            // Loop contact list, if email found in members hashset, do not add
            for(int i=0;i<contactList.size();i++) {
                ContactImpl memberContact = (ContactImpl)contactList.get(i);
                
                // else add to result list
                if (!memberSet.contains(memberContact.getEmailAddress())) {
                    nonMembersList.add(memberContact);
                }
            }
            
        }
        
        
        return nonMembersList;
    }
    
    /**
     *  This method is used for invites, so that contacts which appears as members but not buddy yet
     *  can be shown to user to request add as friend.
     *  We will get buddy list first so to save resource of looping each contact to see if already
     *  a buddy.
     *  Loop through the contactList of ContactImpl objects and check with db.
     *  If found, set relevant fields to display at UI.
     */
    public List getMembersNotBuddyListFromContacts(String userId, List contactList) {
        
        List membersList = new ArrayList();
        
        if (contactList != null && !contactList.isEmpty()) {
            UserBO userBO = new UserBO();
            UserWishesBO userWishesBO = new UserWishesBO();
            User user = null;
            
            // Gets buddy list, contains list of WishVO
            List buddyList = userWishesBO.getUserConfirmedFriendsList(userId);
            List buddyUserIdList = new ArrayList();
            // Save buddyUserId to buddyUserIdList
            if (buddyList != null && !buddyList.isEmpty()) {
                for(int i=0;i<buddyList.size();i++) {
                    WishVO wishVO = (WishVO)buddyList.get(i);
                    buddyUserIdList.add(new Long(wishVO.getFriendId()));
                }
            }
            
            
            
            // Find
            for(int i=0;i<contactList.size();i++) {
                ContactImpl contact = (ContactImpl)contactList.get(i);
                String contactEmail = contact.getEmailAddress();
                if (StringUtils.hasValue(contactEmail)){
                    user = userBO.getUserByEmail(contactEmail.trim().toLowerCase());
                    if (user != null) {
                        // but not buddy, so it matched our wanted list
                        if (buddyUserIdList.isEmpty() ||
                                !buddyUserIdList.contains(new Long(user.getUserId()))) {
                            // Indeed is a member
                            UserCountry userCountry = user.getUserCountry();
                            contact.setUserName(user.getUserName());
                            contact.setUserId(user.getUserId());
                            contact.setPhotoSmallPath(userCountry.getPhotoSmallPath());
                            
                            membersList.add(contact);
                        }
                        
                    }
                }
            }
            userBO = null;
        }
        return membersList;
        
    }
    
}
