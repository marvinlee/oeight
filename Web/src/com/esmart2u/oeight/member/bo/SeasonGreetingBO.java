/*
 * SeasonGreetingBO.java
 *
 * Created on November 14, 2007, 3:59 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.SeasonGreeting;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 *
 * @author meauchyuan.lee
 */
public class SeasonGreetingBO {
    
    private static Logger logger = Logger.getLogger(SeasonGreetingBO.class);
    
    /** Creates a new instance of SeasonGreetingBO */
    public SeasonGreetingBO() {
    }
    
              
    public SeasonGreeting sendGreetings(SeasonGreeting seasonGreeting) {  
        return insertGreetingsIntoDB(seasonGreeting);  
    }
    
    private SeasonGreeting insertGreetingsIntoDB(SeasonGreeting seasonGreeting){
        String savedEmail = null;
        int count = 0;
         
        String senderEmail = seasonGreeting.getSenderEmail();
        String receiverEmail = seasonGreeting.getReceiverEmail();
        if (StringUtils.hasValue(senderEmail) && StringUtils.hasValue(receiverEmail)){ 
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction(); 
               
            Date dateSent = new Date();
            seasonGreeting.setGreetingCode(getGreetingCode(senderEmail, receiverEmail, dateSent));
            seasonGreeting.setStatus(OEightConstants.MAIL_STATUS_NEW);
            seasonGreeting.setDateSent(dateSent); 
            logger.debug("Adding into db " + seasonGreeting.getGreetingCode());
            session.save(seasonGreeting); 
            
            session.getTransaction().commit();
            if (session.isOpen()){
                session.close();
            }
        }
        return seasonGreeting;
    }
    
    public String testGetGreetingCode(String senderEmail, String receiverEmail, Date dateSent) {
        String concat = senderEmail + dateSent.getTime() + receiverEmail;
        String hash = null;
        try{
            MessageDigest m=MessageDigest.getInstance("MD5");
            m.update(concat.getBytes(),0,concat.length());
            hash = new BigInteger(1,m.digest()).toString(16);
            m=null;
        } catch (NoSuchAlgorithmException nse) {
            logger.error("Error Creating greetingCode to " + receiverEmail);
            System.out.println("Error Creating greetingCode to " + receiverEmail);
        }
        return hash;
    }  
    
    private String getGreetingCode(String senderEmail, String receiverEmail, Date dateSent) {
        String concat = senderEmail + dateSent.getTime() + receiverEmail;
        String hash = null;
        try{
            MessageDigest m=MessageDigest.getInstance("MD5");
            m.update(concat.getBytes(),0,concat.length());
            hash = new BigInteger(1,m.digest()).toString(16);
            m=null;
        } catch (NoSuchAlgorithmException nse) {
            logger.error("Error Creating greetingCode to " + receiverEmail);
            System.out.println("Error Creating greetingCode to " + receiverEmail);
        }
        return hash;
    }
       
    public SeasonGreeting getGreeting(SeasonGreeting seasonGreeting)
    {
        boolean valid = false;
        Session session = HibernateUtil.getSessionFactory().openSession();
        //session.beginTransaction(); 
        SeasonGreeting dbSeasonGreeting = (SeasonGreeting) session.createCriteria(SeasonGreeting.class)
        .add(Expression.eq( "senderEmail", seasonGreeting.getSenderEmail() ))
        .add(Expression.eq( "greetingCode", seasonGreeting.getGreetingCode() ))
        .uniqueResult();
        
        if (dbSeasonGreeting != null && dbSeasonGreeting.getGreetingId()>0)
        {
            valid = true;
        }
        //session.getTransaction().commit(); 
        if (session.isOpen()){
            session.close();
        }
        return dbSeasonGreeting;
    }
        
    public SeasonGreeting setGreetingRetrieved(SeasonGreeting seasonGreeting)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction(); 
        
        String hql = "update SeasonGreeting set dateAccepted = :newDateAccepted where greetingId = :greetingId";
        Query query = session.createQuery(hql);
        query.setTimestamp("newDateAccepted",new Date());
        query.setLong("greetingId",seasonGreeting.getGreetingId());
        int rowCount = query.executeUpdate();

        /*SeasonGreeting dbSeasonGreeting = (SeasonGreeting) session.createCriteria(SeasonGreeting.class)
        .add(Expression.idEq(new Long(seasonGreeting.getGreetingId())))
        .uniqueResult();
         
        if (dbSeasonGreeting != null && dbSeasonGreeting.getGreetingId()>0)
        {
            dbSeasonGreeting.setDateAccepted(new Date());
            session.update(dbSeasonGreeting);
        }*/
        session.getTransaction().commit(); 
        if (session.isOpen()){
            session.close();
        }
        return seasonGreeting;
    
    }
}
