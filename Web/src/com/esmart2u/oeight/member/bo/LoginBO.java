/*
 * LoginBO.java
 *
 * Created on September 20, 2007, 4:00 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.Login;
import com.esmart2u.oeight.data.LoginFailure;
import com.esmart2u.oeight.data.LoginReset;
import com.esmart2u.oeight.data.MembersUpdate;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.BaseConstants;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.helper.Validator;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.hibernate.*;
import org.hibernate.criterion.Expression;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author meauchyuan.lee
 */
public class LoginBO {
    
    /** Creates a new instance of LoginBO */
    public LoginBO() {
    }
    
    private static Logger logger = Logger.getLogger(LoginBO.class);
    
    /*public List listUsers() {
     
        try  {
            InitialContext initCtx = new InitialContext();
            DataSource ds = (DataSource)initCtx.lookup("java:comp/env/jdbc/firststep");
            Connection conn = ds.getConnection();
     
            System.out.println("Connection from DataSource successfully opened!<br>");
     
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from user_login");
     
            while (rs.next() )  {
                String onerow = "Email " + rs.getString("email_address");
                System.out.println(onerow);
            }
     
            rs.close();
            stmt.close();
            conn.close();
            initCtx.close();
     
            System.out.println("Connection from DataSource successfully closed!<br>");
        } catch(Exception e)  {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
     
        return new ArrayList();
     */
        /*Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
         
        List result = session.createQuery("from UserLogin").list();
         
        session.getTransaction().commit();
         
        return result;
    }*/
    
    public User getUserDetails(String emailAddress) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        User userLogin = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            //List result = session.createQuery("from UserLogin").list();
            userLogin = (User)session.createCriteria(User.class)
            .add( Restrictions.naturalId()
            .set("emailAddress", emailAddress)
            ).setCacheable(true)
            .uniqueResult();
            
            //session.getTransaction().commit();
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userLogin;
    }
    
    
    
    public String requestReset(String emailAddress) {
        String email = null;
        logger.debug("Validate email=" + emailAddress);
        if (Validator.validateEmail(emailAddress,true)) {
            // Save into DB, ignore duplicates
            email = insertEmailIntoDB(emailAddress);
        }
        return email;
    }
    
    private String insertEmailIntoDB(String email){
        String savedEmail = null;
        int count = 0;
        
        User user = getUserDetails(email);
        if (StringUtils.hasValue(email) && (user != null)){
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                
                Date dateSent = new Date();
                long userId = user.getUserId();
                LoginReset dbLoginReset = (LoginReset) session.createCriteria(LoginReset.class)
                .add(Expression.eq( "userId", userId))
                .add(Expression.eq( "email", email ))
                .uniqueResult();
                
                if (dbLoginReset == null) {
                    dbLoginReset = new LoginReset();
                    dbLoginReset.setUserId(userId);
                    dbLoginReset.setEmail(email);
                    dbLoginReset.setResetCode(getResetCode(""+userId, email));
                    dbLoginReset.setStatus(OEightConstants.MAIL_STATUS_NEW);
                    dbLoginReset.setDateSent(dateSent);
                    logger.debug("Adding into db " + dbLoginReset.getEmail());
                    session.save(dbLoginReset);
                } else {
                    dbLoginReset.setStatus(OEightConstants.MAIL_STATUS_NEW);
                    dbLoginReset.setDateSent(dateSent);
                    session.update(dbLoginReset);
                }
                savedEmail = dbLoginReset.getEmail();
                
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
        }
        return savedEmail;
    }
    
    private String getResetCode(String userId, String email) {
        String concat = userId + "reset" + email;
        String hash = null;
        try{
            MessageDigest m=MessageDigest.getInstance("MD5");
            m.update(concat.getBytes(),0,concat.length());
            hash = new BigInteger(1,m.digest()).toString(16);
            m=null;
        } catch (NoSuchAlgorithmException nse) {
            logger.error("Error Creating resetCode for reset password to " + email);
            System.out.println("Error Creating resetCode for reset password  to " + email);
        }
        return hash;
    }
    
    public boolean validReset(LoginReset loginReset) {
        boolean valid = false;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            LoginReset dbLoginReset = (LoginReset) session.createCriteria(LoginReset.class)
            .add(Expression.eq( "email", loginReset.getEmail() ))
            .add(Expression.eq( "resetCode", loginReset.getResetCode() ))
            .uniqueResult();
            
            if (dbLoginReset != null && dbLoginReset.getUserId()>0) {
                valid = true;
            }
            //session.getTransaction().commit();
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return valid;
    }
    
    public User updateUserPassword(User user) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        User userLogin = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            userLogin = (User)session.createCriteria(User.class)
            .add( Restrictions.naturalId()
            .set("emailAddress", user.getEmailAddress())
            ).uniqueResult();
            
            if (userLogin != null && userLogin.getUserId()>0) {
                userLogin.setSecureCode(user.getSecureCode());
                session.save(userLogin);
                
                // Update password updated
                LoginReset loginReset = (LoginReset)session.createCriteria(LoginReset.class)
                .add(Expression.eq( "userId", userLogin.getUserId() ))
                .add(Expression.eq( "email", userLogin.getEmailAddress() ))
                .uniqueResult();
                
                // This is only for user reset
                if (loginReset!=null){
                    loginReset.setDateAccepted(new Date());
                    session.save(loginReset);
                }
            }
            
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userLogin;
    }
    
    
    public void insertLoginSession(boolean success, String loginEmail, String userId, User user, String ipAddress, String referrer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            Login loginBean = new Login();
            loginBean.setLoginEmail(loginEmail);
            if (userId != null && !"".equals(userId)){
                loginBean.setUserId(Long.parseLong(userId));
            }
            char loginSuccess = success? PropertyConstants.BOOLEAN_YES: PropertyConstants.BOOLEAN_NO;
            loginBean.setLoginSuccessFlag(loginSuccess);
            loginBean.setIpAddress(ipAddress);
            loginBean.setReferrer(referrer);
            loginBean.setLoginTime(new Date());
            
            session.save(loginBean);
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        // Insert into members update
        if (success && (user!= null)) {
            MembersUpdate member = new MembersUpdate();
            member.setUserId(Long.parseLong(userId));
            member.setUserName(user.getUserName());
            member.setPhotoSmallPath(user.getUserCountry().getPhotoSmallPath());
            member.setPhotoLargePath(user.getUserCountry().getPhotoLargePath());
            member.setUpdateType(OEightConstants.MEMBERS_UPDATE_TYPE_LOGIN);
            member.setDateUpdate(new Date());
            
            
            // To be displayed in latest login, user photo has to be uploaded and email validated
            if (StringUtils.hasValue(member.getPhotoSmallPath()) &&
                    !PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL).equals(member.getPhotoSmallPath())
                    && PropertyConstants.BOOLEAN_YES == user.getUserCountry().getValidated() ){
                
                MembersUpdateBO.getInstance().insertNewLatestLogin(member);
                
            }
        }
    }
    
    public void updateLoginSession(String userId, String ipAddress) {
        List loginList = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            Criteria crit = session.createCriteria(Login.class);
            crit.add( Expression.eq( "userId", Long.parseLong(userId)) );
            crit.add( Expression.eq( "ipAddress", ipAddress) );
            crit.add( Expression.eq( "loginSuccessFlag", PropertyConstants.BOOLEAN_YES) );
            crit.addOrder( Order.desc("loginTime") );
            crit.setMaxResults(1);
            loginList = crit.list();
            
            if (loginList!=null && !loginList.isEmpty()) {
                Login loginBean = (Login)loginList.get(0);
                if (loginBean.getLogoutTime() != null) {
                    logger.error("Error recording logout as logout time is not null");
                } else {
                    loginBean.setLogoutTime(new Date());
                }
                session.update(loginBean);
            } else {
                logger.error("Error recording logout as no login session found for=" + userId + ", ip=" + ipAddress);
            }
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    }

    public void insertFailedSession(boolean failed, long userId, String email, String password, String ipAddress, String referrer) {
       
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction(); 
            LoginFailure loginFailure = new LoginFailure();
            loginFailure.setUserId(userId);
            loginFailure.setLoginEmail(email);
            loginFailure.setLoginCode(password);
            loginFailure.setLastLoginTime(new Date());
            loginFailure.setIpAddress(ipAddress);
            loginFailure.setReferrer(referrer);
            session.save(loginFailure);
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    }
}
