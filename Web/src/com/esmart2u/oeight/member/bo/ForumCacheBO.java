/*
 * ForumCacheBO.java
 *
 * Created on October 19, 2008, 12:45 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.ForumTopic;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;

/**
 *
 * @author meauchyuan.lee
 */
public class ForumCacheBO {
    
    private static Logger logger = Logger.getLogger(ForumCacheBO.class);
    
    private static ForumCacheBO instance;
      
    private static HashMap forumMap;
    
    /** Creates a new instance of FeaturedBO */
   
    public static synchronized ForumCacheBO getInstance(){
        if ( instance == null ){
            instance = new ForumCacheBO(); 
        }
        return instance;
    }
    
    public static HashMap reloadMap()
    { 
        return getForumsTopicFromDB(); 
    }
    
     
    private static HashMap getForumsTopicFromDB()
    {   
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        List forumTopicList = null;
        Transaction transaction = null;
        HashMap resultMap = null;
        try { 
        
              // Retrieve entire ForumTopic 
            forumTopicList = session.createCriteria(ForumTopic.class)
            .addOrder(Order.desc("topicTime"))
            .setMaxResults(100)
            .list();
            logger.debug("Got forumTopicList:" + forumTopicList);
            resultMap = filterForumTopicList(forumTopicList); 
            
            
            if (resultMap != null && !resultMap.isEmpty())
            {
                getInstance().forumMap = resultMap;
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    
        return getInstance().forumMap;  
    
    } 
    
       
    public static List getForumsTopicForRss()
    {   
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        List forumTopicList = null;
        Transaction transaction = null;
        HashMap resultMap = null;
        try { 
        
              // Retrieve entire ForumTopic 
            forumTopicList = session.createCriteria(ForumTopic.class)
            .addOrder(Order.desc("topicTime"))
            .setMaxResults(25)
            .list();
            logger.debug("Got forumTopicList:" + forumTopicList);  
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    
        return forumTopicList;  
    
    } 
    
    private static HashMap filterForumTopicList(List forumTopicList)
    { 
        HashMap forumMap = new HashMap(); // Group by forum
        HashMap forumIdMap = new HashMap(); // Group by forum
        if (forumTopicList !=null && !forumTopicList.isEmpty())
        {
            long count = 0;
            for(int i=0; i<forumTopicList.size();i++)
            {   
                ForumTopic topic = (ForumTopic)forumTopicList.get(i);
                if (!forumIdMap.containsKey(topic.getForumId()))
                {
                    count++;
                    forumIdMap.put(topic.getForumId(),new Long(count));
                    List topicList = new ArrayList();
                    topic.setForumId(count);
                    topicList.add(topic);
                    forumMap.put(count,topicList);
                }  
                else
                {
                    Long countForumId = (Long)forumIdMap.get(topic.getForumId());
                    topic.setForumId(countForumId);
                    List topicList = (ArrayList)forumMap.get(topic.getForumId());
                    topicList.add(topic); 
                }
            } 
        }
    
        return forumMap;
    }
    
    public static HashMap getForumMap()
    {   
        HashMap dataMap = getInstance().forumMap; 
        if (dataMap == null || dataMap.isEmpty())
        {
           reloadMap();
        }
        return getInstance().forumMap;
    }
}