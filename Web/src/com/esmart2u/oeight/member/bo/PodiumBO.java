/*
 * PodiumBO.java
 *
 * Created on October 18, 2007, 4:22 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.RankingPodium;
import com.esmart2u.oeight.data.RankingSnapshot;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class PodiumBO {
    
    private static Logger logger = Logger.getLogger(PodiumBO.class);
    
    private static PodiumBO instance;
    private static final String CURRENT_MINUTE_FORMAT = "hhmm";
    private static final SimpleDateFormat currentMinuteFormatter = new SimpleDateFormat(CURRENT_MINUTE_FORMAT);
    private static String currentMinute = "";
    private static HashMap currentPodium = null;
    private static List latestMostVotes = null;
    private static List latestMostInvites = null;
    private static List latestMostViews = null;
    private static List latestMostGame = null;
    
    private static List previousMostVotes = null;
    private static List previousMostInvites = null;
    private static List previousMostViews = null;
    private static List previousMostGame = null;
    
    private static final String CURRENT_MONTH_FORMAT = "MMyyyy";
    private static final SimpleDateFormat currentMonthFormatter = new SimpleDateFormat(CURRENT_MONTH_FORMAT);
    private static String currentMonth = "";
    
    private static List currentVotesHistory = null;
    private static List currentInvitesHistory = null;
    private static List currentViewsHistory = null;
    private static List currentGameHistory = null;
    
    private static long systemUser = Long.parseLong(""+PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY));
    
    public static synchronized PodiumBO getInstance(){
        if ( instance == null ){
            instance = new PodiumBO();
            instance.currentMonth = getCurrentMonth();
            getLatestHistoryList();
        }
        return instance;
    }
    
    
    public static HashMap getLatestPodiumStand() {
        // Save some database hit
        if (!currentMinute.equals(getCurrentMinute())){
            HashMap hashMap = new HashMap();
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                // Get Ranking snapshot
                Criteria crit = session.createCriteria(RankingSnapshot.class);
                crit.setFetchMode("user", FetchMode.JOIN);
                crit.add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()));
                crit.add(Expression.ne( "userId", systemUser));
                crit.addOrder( Order.desc("pageView") );
                crit.setMaxResults(3);
                latestMostViews = crit.list();
                
                
                crit = session.createCriteria(RankingSnapshot.class);
                crit.setFetchMode("user", FetchMode.JOIN);
                crit.add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()));
                crit.add(Expression.ne( "userId", systemUser));
                crit.addOrder( Order.desc("invitesAccepted") );
                crit.setMaxResults(3);
                latestMostInvites = crit.list();
                
                
                
                crit = session.createCriteria(RankingSnapshot.class);
                crit.setFetchMode("user", FetchMode.JOIN);
                crit.add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()));
                crit.add(Expression.ne( "userId", systemUser));
                crit.addOrder( Order.desc("votes") );
                crit.setMaxResults(3);
                latestMostVotes = crit.list();
                
                
                
                crit = session.createCriteria(RankingSnapshot.class);
                crit.setFetchMode("user", FetchMode.JOIN);
                crit.add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()));
                crit.add(Expression.ne( "userId", systemUser));
                crit.addOrder( Order.desc("gameTopPoints") );
                crit.setMaxResults(3);
                latestMostGame = crit.list();
                
                hashMap.put(OEightConstants.TOP_VIEWS,latestMostViews);
                hashMap.put(OEightConstants.TOP_VOTES,latestMostVotes);
                hashMap.put(OEightConstants.TOP_INVITES,latestMostInvites);
                hashMap.put(OEightConstants.TOP_GAMER,latestMostGame);
                
                //session.getTransaction().commit();
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
            currentPodium = hashMap;
            currentMinute = getCurrentMinute();
        }
        
        return currentPodium;
    }
    
    
    public static HashMap getPreviousPodiumStand() {
        HashMap hashMap = new HashMap();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            // Get Ranking snapshot
            Criteria crit = session.createCriteria(RankingSnapshot.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.eq( "snapshotMonth", DateObjectHelper.getPreviousMonthDateObject()));
            crit.add(Expression.ne( "userId", systemUser));
            crit.addOrder( Order.desc("pageView") );
            crit.setMaxResults(3);
            previousMostViews = crit.list();
            System.out.println("previousMostViews=" + previousMostViews.size());
            
            
            crit = session.createCriteria(RankingSnapshot.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.eq( "snapshotMonth", DateObjectHelper.getPreviousMonthDateObject()));
            crit.add(Expression.ne( "userId", systemUser));
            crit.addOrder( Order.desc("invitesAccepted") );
            crit.setMaxResults(3);
            previousMostInvites = crit.list();
            System.out.println("previousMostInvites=" + previousMostInvites.size());
            
            
            
            crit = session.createCriteria(RankingSnapshot.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.eq( "snapshotMonth", DateObjectHelper.getPreviousMonthDateObject()));
            crit.add(Expression.ne( "userId", systemUser));
            crit.addOrder( Order.desc("votes") );
            crit.setMaxResults(3);
            previousMostVotes = crit.list();
            System.out.println("previousMostVotes=" + previousMostVotes.size());
            
            
            
            crit = session.createCriteria(RankingSnapshot.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.eq( "snapshotMonth", DateObjectHelper.getPreviousMonthDateObject()));
            crit.add(Expression.ne( "userId", systemUser));
            crit.addOrder( Order.desc("gameTopPoints") );
            crit.setMaxResults(3);
            previousMostGame = crit.list();
            System.out.println("previousMostGame=" + previousMostGame.size());
            
            hashMap.put(OEightConstants.TOP_VIEWS,previousMostViews);
            hashMap.put(OEightConstants.TOP_VOTES,previousMostVotes);
            hashMap.put(OEightConstants.TOP_INVITES,previousMostInvites);
            hashMap.put(OEightConstants.TOP_GAMER,previousMostGame);
            
            insertPreviousMonthsPodium(session, hashMap);
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return hashMap;
    }
    
    private static void insertPreviousMonthsPodium(Session session, HashMap hashMap) {
        List previousMostViews = (List) hashMap.get(OEightConstants.TOP_VIEWS);
        if (previousMostViews != null){
            currentViewsHistory = new ArrayList();
        }
        for (int i=0; i<previousMostViews.size();i++) {
            RankingSnapshot rankingSnapshot = (RankingSnapshot)previousMostViews.get(i);
            RankingPodium podium = new RankingPodium();
            podium.setUserId(rankingSnapshot.getUserId());
            podium.setSnapshotMonth(rankingSnapshot.getSnapshotMonth());
            podium.setSnapshotCode(OEightConstants.PODIUM_TYPE_VIEWS);
            podium.setRank(i +1);
            podium.setValue(rankingSnapshot.getPageView());
            podium.setUser(rankingSnapshot.getUser());
            logger.debug("Adding " + podium.getUserId() + " " + podium.getSnapshotMonth() + " " + podium.getSnapshotCode() + ",ranking no " + podium.getRank());
            session.save(podium);
            currentViewsHistory.add(podium);
        }
        
        List previousMostVotes = (List) hashMap.get(OEightConstants.TOP_VOTES);
        if (previousMostVotes != null){
            currentVotesHistory = new ArrayList();
        }
        for (int i=0; i<previousMostVotes.size();i++) {
            RankingSnapshot rankingSnapshot = (RankingSnapshot)previousMostVotes.get(i);
            RankingPodium podium = new RankingPodium();
            podium.setUserId(rankingSnapshot.getUserId());
            podium.setSnapshotMonth(rankingSnapshot.getSnapshotMonth());
            podium.setSnapshotCode(OEightConstants.PODIUM_TYPE_VOTES);
            podium.setRank(i +1);
            podium.setValue(rankingSnapshot.getVotes());
            podium.setUser(rankingSnapshot.getUser());
            logger.debug("Adding " + podium.getUserId() + " " + podium.getSnapshotMonth() + " " + podium.getSnapshotCode() + ",ranking no " + podium.getRank());
            session.save(podium);
            currentVotesHistory.add(podium);
        }
        
        List previousMostInvites = (List) hashMap.get(OEightConstants.TOP_INVITES);
        if (previousMostInvites != null){
            currentInvitesHistory = new ArrayList();
        }
        for (int i=0; i<previousMostInvites.size();i++) {
            RankingSnapshot rankingSnapshot = (RankingSnapshot)previousMostInvites.get(i);
            RankingPodium podium = new RankingPodium();
            podium.setUserId(rankingSnapshot.getUserId());
            podium.setSnapshotMonth(rankingSnapshot.getSnapshotMonth());
            podium.setSnapshotCode(OEightConstants.PODIUM_TYPE_INVITES);
            podium.setRank(i +1);
            podium.setValue(rankingSnapshot.getInvitesAccepted());
            podium.setUser(rankingSnapshot.getUser());
            logger.debug("Adding " + podium.getUserId() + " " + podium.getSnapshotMonth() + " " + podium.getSnapshotCode() + ",ranking no " + podium.getRank());
            session.save(podium);
            currentInvitesHistory.add(podium);
        }
        
        
        List previousMostGame = (List) hashMap.get(OEightConstants.TOP_GAMER);
        if (previousMostGame != null){
            currentGameHistory = new ArrayList();
        }
        for (int i=0; i<previousMostGame.size();i++) {
            RankingSnapshot rankingSnapshot = (RankingSnapshot)previousMostGame.get(i);
            RankingPodium podium = new RankingPodium();
            podium.setUserId(rankingSnapshot.getUserId());
            podium.setSnapshotMonth(rankingSnapshot.getSnapshotMonth());
            podium.setSnapshotCode(OEightConstants.PODIUM_TYPE_GAMER);
            podium.setRank(i +1);
            podium.setValue(rankingSnapshot.getGameTopPoints());
            podium.setUser(rankingSnapshot.getUser());
            logger.debug("Adding " + podium.getUserId() + " " + podium.getSnapshotMonth() + " " + podium.getSnapshotCode() + ",ranking no " + podium.getRank());
            session.save(podium);
            currentGameHistory.add(podium);
        }
    }
    
    private static String getCurrentMinute() {
        logger.debug("Current minute" + currentMinuteFormatter.format(new Date()));
        return currentMinuteFormatter.format(new Date());
    }
    
    private static String getCurrentMonth() {
        logger.debug("Current month" + currentMonthFormatter.format(new Date()));
        return currentMonthFormatter.format(new Date());
    }
    
    
    public static synchronized void getLatestHistoryList() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            Date previousMonth = DateObjectHelper.getPreviousMonthDateObject();
            
            // Get Ranking snapshot
            Criteria crit = session.createCriteria(RankingPodium.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.le( "snapshotMonth", previousMonth ));
            crit.add(Expression.eq( "snapshotCode", OEightConstants.PODIUM_TYPE_VIEWS ));
            crit.add(Expression.ne( "userId", systemUser));
            crit.addOrder( Order.desc("snapshotMonth") );
            crit.addOrder( Order.asc("rank") );
            currentViewsHistory = crit.list();
            
            
            crit = session.createCriteria(RankingPodium.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.le( "snapshotMonth", previousMonth ));
            crit.add(Expression.eq( "snapshotCode", OEightConstants.PODIUM_TYPE_INVITES ));
            crit.add(Expression.ne( "userId", systemUser));
            crit.addOrder( Order.desc("snapshotMonth") );
            crit.addOrder( Order.asc("rank") );
            currentInvitesHistory = crit.list();
            
            
            crit = session.createCriteria(RankingPodium.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.le( "snapshotMonth", previousMonth ));
            crit.add(Expression.eq( "snapshotCode", OEightConstants.PODIUM_TYPE_VOTES ));
            crit.add(Expression.ne( "userId", systemUser));
            crit.addOrder( Order.desc("snapshotMonth") );
            crit.addOrder( Order.asc("rank") );
            currentVotesHistory = crit.list();
            
            
            crit = session.createCriteria(RankingPodium.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.le( "snapshotMonth", previousMonth ));
            crit.add(Expression.eq( "snapshotCode", OEightConstants.PODIUM_TYPE_GAMER ));
            crit.add(Expression.ne( "userId", systemUser));
            crit.addOrder( Order.desc("snapshotMonth") );
            crit.addOrder( Order.asc("rank") );
            currentGameHistory = crit.list();
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        
    }
    
    
    public static synchronized List getMostVotedHistory() {
        logger.debug("Check Most Voted History");
        if (!currentMonth.equals(getCurrentMonth()) && StringUtils.hasValue(currentMonth)){
            
            logger.debug("Change detected");
            // It could be a change to a new month, or a server restart
            // So we check if current months record is available in RankingPodium, if no, we populate
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                // Get Ranking snapshot
                Criteria crit = session.createCriteria(RankingPodium.class);
                crit.setFetchMode("user", FetchMode.JOIN);
                Date lastMonth = DateObjectHelper.getPreviousMonthDateObject();
                crit.add(Expression.eq( "snapshotMonth", lastMonth));
                crit.add(Expression.eq( "snapshotCode", OEightConstants.PODIUM_TYPE_VOTES));
                crit.add(Expression.ne( "userId", systemUser));
                crit.addOrder( Order.asc("rank") );
                crit.setMaxResults(3);
                List votedList = crit.list();
                logger.debug("Got votedList");
                
                if (votedList == null || votedList.isEmpty()) {
                    logger.debug("No previous records found");
                    HashMap hashMap = getPreviousPodiumStand(); // This will set currentVotesHistory into PodiumBO
                    
                    // Renew the list
                    getLatestHistoryList();
                }
                
                //session.getTransaction().commit();
                
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
            //currentVotesHistory = votedList;
            currentMonth = getCurrentMonth();
        }
        
        return currentVotesHistory;
        
        
    }
    
    
    public static synchronized List getMostViewedHistory() {
        logger.debug("Check Most Viewed History");
        if (!currentMonth.equals(getCurrentMonth()) && StringUtils.hasValue(currentMonth)){
            
            logger.debug("Change detected");
            // It could be a change to a new month, or a server restart
            // So we check if current months record is available in RankingPodium, if no, we populate
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                // Get Ranking snapshot
                Criteria crit = session.createCriteria(RankingPodium.class);
                crit.setFetchMode("user", FetchMode.JOIN);
                Date lastMonth = DateObjectHelper.getPreviousMonthDateObject();
                crit.add(Expression.eq( "snapshotMonth", lastMonth));
                crit.add(Expression.eq( "snapshotCode", OEightConstants.PODIUM_TYPE_VIEWS));
                crit.add(Expression.ne( "userId", systemUser));
                crit.addOrder( Order.asc("rank") );
                crit.setMaxResults(3);
                List viewedList = crit.list();
                logger.debug("Got viewedList");
                
                if (viewedList == null || viewedList.isEmpty()) {
                    logger.debug("No previous records found");
                    HashMap hashMap = getPreviousPodiumStand(); // This will set currentViewsHistory into PodiumBO
                    
                    // Renew the list
                    getLatestHistoryList();
                }
                
                //session.getTransaction().commit();
                
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
            //currentVotesHistory = votedList;
            currentMonth = getCurrentMonth();
        }
        
        return currentViewsHistory;
        
        
    }
    
    
    public static synchronized List getMostInvitesHistory() {
        logger.debug("Check Most Invites History");
        if (!currentMonth.equals(getCurrentMonth()) && StringUtils.hasValue(currentMonth)){
            
            logger.debug("Change detected");
            // It could be a change to a new month, or a server restart
            // So we check if current months record is available in RankingPodium, if no, we populate
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                // Get Ranking snapshot
                Criteria crit = session.createCriteria(RankingPodium.class);
                crit.setFetchMode("user", FetchMode.JOIN);
                Date lastMonth = DateObjectHelper.getPreviousMonthDateObject();
                crit.add(Expression.eq( "snapshotMonth", lastMonth));
                crit.add(Expression.eq( "snapshotCode", OEightConstants.PODIUM_TYPE_INVITES));
                crit.add(Expression.ne( "userId", systemUser));
                crit.addOrder( Order.asc("rank") );
                crit.setMaxResults(3);
                List invitesList = crit.list();
                logger.debug("Got invitesList");
                
                if (invitesList == null || invitesList.isEmpty()) {
                    logger.debug("No previous records found");
                    HashMap hashMap = getPreviousPodiumStand(); // This will set currentInvitesHistory into PodiumBO
                    
                    // Renew the list
                    getLatestHistoryList();
                }
                
                //session.getTransaction().commit();
                
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
            //currentVotesHistory = votedList;
            currentMonth = getCurrentMonth();
        }
        
        return currentInvitesHistory;
        
        
    }
    
    
    public static synchronized List getMostGamePointsHistory() {
        logger.debug("Check Most Game Points History");
        if (!currentMonth.equals(getCurrentMonth()) && StringUtils.hasValue(currentMonth)){
            
            logger.debug("Change detected");
            // It could be a change to a new month, or a server restart
            // So we check if current months record is available in RankingPodium, if no, we populate
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                // Get Ranking snapshot
                Criteria crit = session.createCriteria(RankingPodium.class);
                crit.setFetchMode("user", FetchMode.JOIN);
                Date lastMonth = DateObjectHelper.getPreviousMonthDateObject();
                crit.add(Expression.eq( "snapshotMonth", lastMonth));
                crit.add(Expression.eq( "snapshotCode", OEightConstants.PODIUM_TYPE_GAMER));
                crit.add(Expression.ne( "userId", systemUser));
                crit.addOrder( Order.asc("rank") );
                crit.setMaxResults(3);
                List gameList = crit.list();
                logger.debug("Got gameList");
                
                if (gameList == null || gameList.isEmpty()) {
                    logger.debug("No previous records found");
                    HashMap hashMap = getPreviousPodiumStand(); // This will set currentGameHistory into PodiumBO
                    
                    // Renew the list
                    getLatestHistoryList();
                }
                
                //session.getTransaction().commit();
                
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
            //currentVotesHistory = votedList;
            currentMonth = getCurrentMonth();
        }
        
        return currentGameHistory;
        
        
    }
    
    
}