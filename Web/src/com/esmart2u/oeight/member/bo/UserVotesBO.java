/*
 * UserVotesBO.java
 *
 * Created on October 30, 2007, 1:52 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.UserVotes;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class UserVotesBO {
    
    /** Creates a new instance of UserVotesBO */
    public UserVotesBO() {
    }
    
    public static int increaseVotes(String userId, String votedUserId)
    {
        int votesLeft = 0;
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            // Update self votes
            UserVotes userVotes = (UserVotes)session.createCriteria(UserVotes.class) 
            .add( Restrictions.eq("userId", new Long(userId)))
            .add( Restrictions.eq("voteDate",DateObjectHelper.getCurrentDayDateObject()))
            .uniqueResult();

            if (userVotes == null)
            {
                userVotes = new UserVotes();
                userVotes.setUserId(Long.parseLong(userId));
                userVotes.setTotalVotes(0);// voted by others
                userVotes.setVotedToday(1);// already voted
                userVotes.setVotedMyself(false);
                userVotes.setVoteDate(DateObjectHelper.getCurrentDayDateObject());
                session.save(userVotes);
            }
            else
            {
                userVotes.setVotedToday(userVotes.getVotedToday() + 1);
                session.update(userVotes);
            }

            // Update the voted user
            UserVotes votedUser = (UserVotes)session.createCriteria(UserVotes.class) 
            .add( Restrictions.eq("userId", new Long(votedUserId)))
            .add( Restrictions.eq("voteDate",DateObjectHelper.getCurrentDayDateObject()))
            .uniqueResult();

            if (votedUser == null)
            {
                votedUser = new UserVotes();
                votedUser.setUserId(Long.parseLong(votedUserId));
                votedUser.setTotalVotes(1);// voted by others
                votedUser.setVotedToday(0);// already voted
                votedUser.setVotedMyself(false);
                votedUser.setVoteDate(DateObjectHelper.getCurrentDayDateObject());
                session.save(votedUser);
            }
            else
            {
                votedUser.setTotalVotes(votedUser.getTotalVotes() + 1);
                session.update(votedUser);
            }

            votesLeft = OEightConstants.NUMBER_OF_VOTES_ALLOWED - userVotes.getVotedToday();
            if (votesLeft < 0) votesLeft = 0;

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
         
        RankingBO.increaseVotes(votedUserId);
        return votesLeft;
    }
    
    
    public static int increaseOwnVotes(String userId)
    {
        int votesLeft = 0;
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
        
            // Update self votes
            UserVotes userVotes = (UserVotes)session.createCriteria(UserVotes.class) 
            .add( Restrictions.eq("userId", new Long(userId)))
            .add( Restrictions.eq("voteDate",DateObjectHelper.getCurrentDayDateObject()))
            .uniqueResult();

            if (userVotes == null)
            {
                userVotes = new UserVotes();
                userVotes.setUserId(Long.parseLong(userId));
                userVotes.setTotalVotes(1);// voted by others
                userVotes.setVotedToday(1);// already voted
                userVotes.setVotedMyself(true);
                userVotes.setVoteDate(DateObjectHelper.getCurrentDayDateObject());
                session.save(userVotes);
            }
            else
            {
                userVotes.setTotalVotes(userVotes.getTotalVotes() + 1);
                userVotes.setVotedToday(userVotes.getVotedToday() + 1);
                userVotes.setVotedMyself(true);
                session.update(userVotes);
            }

            votesLeft = OEightConstants.NUMBER_OF_VOTES_ALLOWED - userVotes.getVotedToday();
            if (votesLeft < 0) votesLeft = 0;
            
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
                
        RankingBO.increaseVotes(userId);
        return votesLeft;
    }
    
    public static int getVotesLeft(String userId)
    {
        int votesLeft = 0;
      
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();//session.beginTransaction();
            // Update self votes
            UserVotes userVotes = (UserVotes)session.createCriteria(UserVotes.class) 
            .add( Restrictions.eq("userId", new Long(userId)))
            .add( Restrictions.eq("voteDate",DateObjectHelper.getCurrentDayDateObject()))
            .uniqueResult();

            if (userVotes == null)
            { 
                // User has not voted yet
                votesLeft = OEightConstants.NUMBER_OF_VOTES_ALLOWED;
            }
            else
            {  
                votesLeft = OEightConstants.NUMBER_OF_VOTES_ALLOWED - userVotes.getVotedToday();
                if (votesLeft < 0) votesLeft = 0;
            }

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return votesLeft;
    }
    
    public static boolean validVoting(String userId, String votedUserId)
    {
    
        boolean valid = false;
      
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            UserVotes userVotes = (UserVotes)session.createCriteria(UserVotes.class) 
            .add( Restrictions.eq("userId", new Long(userId)))
            .add( Restrictions.eq("voteDate",DateObjectHelper.getCurrentDayDateObject()))
            .uniqueResult();

            if (userVotes == null)
            { 
                // User has not voted yet
                valid = true;
            }
            else
            {  
                // User still has votes and not voting ownself
                if (userVotes.getVotedToday() < OEightConstants.NUMBER_OF_VOTES_ALLOWED &&  !userId.equals(votedUserId))
                {
                    valid = true;
                }
                // User still has votes and voting ownself
                else if (userVotes.getVotedToday() < OEightConstants.NUMBER_OF_VOTES_ALLOWED && userId.equals(votedUserId)&& !userVotes.getVotedMyself() )
                {
                    valid = true;
                }
            }
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return valid;
    
    }
}
