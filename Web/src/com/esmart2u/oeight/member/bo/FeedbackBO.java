/*
 * FeedbackBO.java
 *
 * Created on January 14, 2008, 11:08 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.admin.logging.AdminLogger;
import com.esmart2u.oeight.data.Feedback;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author meauchyuan.lee
 */
public class FeedbackBO {
      
    private static Logger logger = Logger.getLogger(FeedbackBO.class);
    
    /** Creates a new instance of FeedbackBO */
    public FeedbackBO() {
    }
    
              
    public Feedback saveFeedback(Feedback feedback) {  
        return insertFeedbackIntoDB(feedback);  
    }
    
    private Feedback insertFeedbackIntoDB(Feedback feedback){ 
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            session.save(feedback); 

            //session.getTransaction().commit(); 

            AdminLogger.notifyAdmin("New feedback received on " + new Date() + " from " + feedback.getName());

             transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return feedback;
    }
}
