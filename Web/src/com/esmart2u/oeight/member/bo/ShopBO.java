/*
 * ShopBO.java
 *
 * Created on February 13, 2008, 2:53 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.admin.logging.AdminLogger;
import com.esmart2u.oeight.data.UserShop;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

/**
 *
 * @author meauchyuan.lee
 */
public class ShopBO {
    
    private static Logger logger = Logger.getLogger(ShopBO.class);
    
    /** Creates a new instance of ShopBO */
    public ShopBO() {
    }
    
    public UserShop takeNewOrder(UserShop userShop) {
        Session session = HibernateUtil.getSessionFactory().openSession();
         Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            userShop.setDateUpdated(new Date());
            session.save(userShop);
 
            transaction.commit();
            AdminLogger.notifyAdmin("New shopping order taken from :" + userShop.getName() + " (" + userShop.getEmail()+ ")");

            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userShop;
        
    }
    
    public UserShop updateOrder(UserShop userShop) {
        Session session = HibernateUtil.getSessionFactory().openSession();
         Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            UserShop dbUserShop = (UserShop)session.createCriteria(UserShop.class)
            .add(Expression.eq( "userId", userShop.getUserId()))
            .add(Expression.eq( "shopReferenceNo", userShop.getShopReferenceNo()))
            .uniqueResult();

            dbUserShop.setDateUpdated(new Date());
            dbUserShop.setShopStatus(userShop.getShopStatus());
            session.update(dbUserShop);
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userShop;
        
    }
    
    public UserShop getOrderByReferenceNo(UserShop userShop)
    {
    
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        UserShop dbUserShop = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            dbUserShop = (UserShop)session.createCriteria(UserShop.class)
            .add(Expression.eq( "userId", userShop.getUserId()))
            .add(Expression.eq( "shopReferenceNo", userShop.getShopReferenceNo()))
            .uniqueResult(); 
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return dbUserShop;
    }
    
    public List getOrderList(String userId) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        List latestList = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            Criteria crit = session.createCriteria(UserShop.class);
            crit.add(Expression.eq( "userId", Long.parseLong(userId)));
            crit.addOrder( Order.desc("userShopId") );  
            latestList = crit.list();  
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return latestList;
    }
    
}
