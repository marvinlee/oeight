/*
 * RegisterBO.java
 *
 * Created on September 28, 2007, 11:29 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.MembersUpdate;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import java.security.*;
import java.math.*;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class RegisterBO {
    
    private static Logger logger = Logger.getLogger(RegisterBO.class);
    
    /** Creates a new instance of RegisterBO */
    public RegisterBO() {
    }
    
    public Long register(User user, UserCountry userCountry) {
        user = registerUser(user);
        //userCountry.setUserId(user.getUserId());
        userCountry.setUser(user);
        userCountry.setReportAbuse(PropertyConstants.BOOLEAN_NO);
        userCountry.setSystemRegistered(new Date(System.currentTimeMillis()));
        userCountry.setSystemStatus(OEightConstants.SYS_STATUS_NEW);
        userCountry = registerUserCountry(userCountry);
        
 
        // Insert into members update
        if (user!= null)
        {
            MembersUpdate member = new MembersUpdate();
            member.setUserId(user.getUserId());
            member.setUserName(user.getUserName());
            member.setPhotoSmallPath(userCountry.getPhotoSmallPath());
            member.setPhotoLargePath(userCountry.getPhotoLargePath());
            member.setUpdateType(OEightConstants.MEMBERS_UPDATE_TYPE_REGISTER);
            member.setDateUpdate(new Date());
            // If user uploaded photo, even not validated email, we show them
            if (StringUtils.hasValue(member.getPhotoSmallPath()) &&
                !PropertyManager.getValue(PropertyConstants.PHOTO_DEFAULT_SMALL).equals(member.getPhotoSmallPath())){
                MembersUpdateBO.getInstance().insertNewLatestMember(member);
            }
        }
        
        return new Long(userCountry.getUserId());
    }
    
    public User registerUser(User user) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
          Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            Long generatedId = (Long)session.save(user);
            //logger.debug("UserId generated1=" + generatedId);
            //logger.debug("UserId generated2=" + user.getUserId());

       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return user;
    }
    
    public UserCountry registerUserCountry(UserCountry userCountry) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
         Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            userCountry.setUserType(PropertyManager.getValue(PropertyConstants.USER_TYPE_KEY));
            Long generatedId = (Long)session.save(userCountry);
            logger.debug("UserId generated=" + generatedId);
        
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userCountry;
    }
    
    
    public String getSecureCode(String emailAddress, String password) throws Exception{
        String concat = emailAddress + password;
        String hash = null;
        try{
            MessageDigest m=MessageDigest.getInstance("MD5");
            m.update(concat.getBytes(),0,concat.length());
            hash = new BigInteger(1,m.digest()).toString(16);
            //logger.debug("MD5: "+ hash);
            m=null;
        } catch (NoSuchAlgorithmException nse) {
            throw new Exception("Error Creating password");
        }
        return hash;
        
    }
    
    public boolean matchSecureCode(String emailAddress, String password, String dbHash) throws Exception {
        String concat = emailAddress + password;
        boolean match = false;
        try{
            MessageDigest m=MessageDigest.getInstance("MD5");
            m.update(concat.getBytes(),0,concat.length());
            String hash = new BigInteger(1,m.digest()).toString(16);
            if (hash.equals(dbHash)){
                match = true;
            }
            m=null;
        } catch (NoSuchAlgorithmException nse) {
            throw new Exception("Error Creating password");
        }
        return match;
        
    }
    
    public static void main(String[] args) throws Exception{
        System.out.println("code:" + new RegisterBO().getSecureCode("contact@080808.com.my", "pppp"));
    }
  
}
