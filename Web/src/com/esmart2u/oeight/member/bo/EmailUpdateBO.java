/*
 * EmailUpdateBO.java
 *
 * Created on April 15, 2008, 4:41 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.EmailUpdate;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserMessage;
import com.esmart2u.oeight.data.UserWishFriends;
import com.esmart2u.oeight.data.UserWishers;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;

/**
 *
 * @author meauchyuan.lee
 */
public class EmailUpdateBO implements OEightConstants{
    
    private static Logger logger = Logger.getLogger(EmailUpdateBO.class);
    
    
    /** Creates a new instance of EmailUpdateBO */
    public EmailUpdateBO() {
    }
    
    public static EmailUpdate saveEmailUpdate(int updateCode, Object inputObject) {
        EmailUpdate emailUpdate = null;
        
        try{
            switch (updateCode) {
                case NOTIFY_UPDATE_S01_RECEIVED_WISH:
                    emailUpdate = insertReceivedWish(inputObject);
                    break;
                case NOTIFY_UPDATE_S02_RECEIVED_PM:
                    emailUpdate = insertReceivedPrivateMessage(inputObject);
                    break;
                case NOTIFY_UPDATE_S03_RECEIVE_BUDDY_REQ:
                    emailUpdate = insertReceivedBuddyRequest(inputObject);
                    break;
                case NOTIFY_UPDATE_S04_RECEIVE_BUDDY_REQ_CONFIRM:
                    emailUpdate = insertReceivedBuddyRequestConfirmation(inputObject);
                    break;
                case NOTIFY_UPDATE_F01_UPDATED_WISH:
                    break;
                case NOTIFY_UPDATE_F02_UPDATED_PROFILE:
                    break;
                case NOTIFY_UPDATE_F03_UPDATED_PHOTO:
                    break;
                case NOTIFY_UPDATE_F04_UPDATED_APPS:
                    break;
                    
            }
        } catch(Exception e) {
            logger.error("Error inserting notification for:" + updateCode);
        }
        return emailUpdate;
    }
    
    private static EmailUpdate insertReceivedWish(Object object){
        
        List valueList = null;
        UserWishers userWishers = (UserWishers)object;
        
        UserBO userBO = new UserBO();
        User recipient = userBO.getUserById("" + userWishers.getUserId());
        
        // Skip if user opted-out
        if (PropertyConstants.BOOLEAN_NO == recipient.getUserCountry().getSubscribe()) {
            return null;
        }
        // Skip if email not validated
        String emailAddress = recipient.getEmailAddress();
        boolean validated = (PropertyConstants.BOOLEAN_YES == recipient.getUserCountry().getValidated());
        
        if(!validated || emailAddress.endsWith("test.com")) {
            return null;
        }
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        EmailUpdate emailUpdate = null;
        try {
            
            transaction = session.beginTransaction();
            emailUpdate = new EmailUpdate();
            emailUpdate.setUserId(userWishers.getUserId());
            emailUpdate.setEmailAddress(emailAddress);
            emailUpdate.setEmailUpdateCode(NOTIFY_UPDATE_S01_RECEIVED_WISH);
            emailUpdate.setDateEffective(new Date());
            
            // No need to know sender, as we target recipient, as long as there is new wish for same day,
            // only one email will be sent
            //emailUpdate.setCustomField1(""+userWishers.getWisherId());
            
            // Check for duplicates of same key = UserId, S01, date within last 24 hours
            
            Criteria crit = session.createCriteria(EmailUpdate.class);
            crit.add( Expression.eq( "userId", new Long(emailUpdate.getUserId())) );
            crit.add( Expression.eq( "emailUpdateCode", new Integer(NOTIFY_UPDATE_S01_RECEIVED_WISH) ));
            crit.add( Expression.eq( "status", OEightConstants.SYS_STATUS_NEW) );
            crit.add( Expression.lt( "dateEffective", DateObjectHelper.getYesterdayDateObject() ));
            
            valueList = crit.list();
            
            if (valueList == null || valueList.isEmpty()) {
                //session.beginTransaction();
                System.out.println("Insert new update now.");
                session.save(emailUpdate);
                //session.getTransaction().commit();
            } else {
                System.out.println("Skip insert new update, existing found");
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return emailUpdate;
    }
    
    
    
    private static EmailUpdate insertReceivedPrivateMessage(Object object){
        
        List valueList = null;
        UserMessage userMessage = (UserMessage)object;
        
        // Skip if user opted-out
        if (PropertyConstants.BOOLEAN_NO == userMessage.getRecipient().getUserCountry().getSubscribe()) {
            return null;
        }
        // Skip if email not validated
        String emailAddress = userMessage.getRecipient().getEmailAddress();
        boolean validated = (PropertyConstants.BOOLEAN_YES == userMessage.getRecipient().getUserCountry().getValidated());
        
        if(!validated || emailAddress.endsWith("test.com")) {
            return null;
        }
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        EmailUpdate emailUpdate = null;
        try {
            
            transaction = session.beginTransaction();
            emailUpdate = new EmailUpdate();
            emailUpdate.setUserId(userMessage.getRecipientUserId());
            emailUpdate.setEmailAddress(emailAddress);
            emailUpdate.setEmailUpdateCode(NOTIFY_UPDATE_S02_RECEIVED_PM);
            emailUpdate.setDateEffective(new Date());
            
            // No need to know sender, as we target recipient, as long as there is new wish for same day,
            // only one email will be sent
            //emailUpdate.setCustomField1(""+userWishers.getWisherId());
            
            // Check for duplicates of same key = UserId, S01, date within last 24 hours
            
            Criteria crit = session.createCriteria(EmailUpdate.class);
            crit.add( Expression.eq( "userId", new Long(emailUpdate.getUserId())) );
            crit.add( Expression.eq( "status", OEightConstants.SYS_STATUS_NEW) );
            crit.add( Expression.eq( "emailUpdateCode", new Integer(NOTIFY_UPDATE_S02_RECEIVED_PM) ));
            crit.add( Expression.lt( "dateEffective", DateObjectHelper.getYesterdayDateObject() ));
            
            valueList = crit.list();
            
            if (valueList == null || valueList.isEmpty()) {
                //session.beginTransaction();
                System.out.println("Insert new update now.");
                session.save(emailUpdate);
                //session.getTransaction().commit();
            } else {
                System.out.println("Skip insert new update, existing found");
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return emailUpdate;
    }
    
    
    /**
     *  @todo - Code to be implemented
     */
    private static EmailUpdate insertReceivedBuddyRequest(Object object){
        
        List valueList = null;
        UserWishFriends userWishFriends = (UserWishFriends)object;
        
        UserBO userBO = new UserBO();
        User notifiedUser = userBO.getUserById(""+userWishFriends.getUserId());
        // Skip if user opted-out
        if (PropertyConstants.BOOLEAN_NO == notifiedUser.getUserCountry().getSubscribe()) {
            return null;
        }
        // Skip if email not validated
        String emailAddress = notifiedUser.getEmailAddress();
        boolean validated = (PropertyConstants.BOOLEAN_YES == notifiedUser.getUserCountry().getValidated());
        
        if(!validated || emailAddress.endsWith("test.com")) {
            return null;
        }
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction transaction = null;
        EmailUpdate emailUpdate = null;
        try {
            
            transaction = session.beginTransaction();
            emailUpdate = new EmailUpdate();
            emailUpdate.setUserId(userWishFriends.getUserId());
            emailUpdate.setEmailAddress(emailAddress);
            emailUpdate.setEmailUpdateCode(NOTIFY_UPDATE_S03_RECEIVE_BUDDY_REQ);
            emailUpdate.setDateEffective(new Date());
            
            // Check for duplicates of same key = UserId, S01, date within last 24 hours
            
            Criteria crit = session.createCriteria(EmailUpdate.class);
            crit.add( Expression.eq( "userId", new Long(emailUpdate.getUserId())) );
            crit.add( Expression.eq( "status", OEightConstants.SYS_STATUS_NEW) );
            crit.add( Expression.eq( "emailUpdateCode", new Integer(NOTIFY_UPDATE_S03_RECEIVE_BUDDY_REQ) ));
            crit.add( Expression.lt( "dateEffective", DateObjectHelper.getYesterdayDateObject() ));
            
            valueList = crit.list();
            
            if (valueList == null || valueList.isEmpty()) {
                //session.beginTransaction();
                System.out.println("Insert new update now.");
                session.save(emailUpdate);
                //session.getTransaction().commit();
            } else {
                System.out.println("Skip insert new update, existing found");
            }
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return emailUpdate;
    }
    
    /**
     *  @todo - Code to be implemented
     */
    private static EmailUpdate insertReceivedBuddyRequestConfirmation(Object object){
        
        List valueList = null;
        UserWishFriends userWishFriends = (UserWishFriends)object;
        
        UserBO userBO = new UserBO();
        User notifiedUser = userBO.getUserById(""+userWishFriends.getUserId());
        // Skip if user opted-out
        if (PropertyConstants.BOOLEAN_NO == notifiedUser.getUserCountry().getSubscribe()) {
            return null;
        }
        // Skip if email not validated
        String emailAddress = notifiedUser.getEmailAddress();
        boolean validated = (PropertyConstants.BOOLEAN_YES == notifiedUser.getUserCountry().getValidated());
        
        if(!validated || emailAddress.endsWith("test.com")) {
            return null;
        }
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        EmailUpdate emailUpdate = null;
        try {
            
            transaction = session.beginTransaction();
            emailUpdate = new EmailUpdate();
            emailUpdate.setUserId(userWishFriends.getUserId());
            emailUpdate.setEmailAddress(emailAddress);
            emailUpdate.setEmailUpdateCode(NOTIFY_UPDATE_S04_RECEIVE_BUDDY_REQ_CONFIRM);
            emailUpdate.setDateEffective(new Date());
            
            
            // Check for duplicates of same key = UserId, S01, date within last 24 hours
            emailUpdate.setCustomField1("" + userWishFriends.getFriendUserId());
            
            Criteria crit = session.createCriteria(EmailUpdate.class);
            crit.add( Expression.eq( "userId", new Long(emailUpdate.getUserId())) );
            crit.add( Expression.eq( "status", OEightConstants.SYS_STATUS_NEW) );
            crit.add( Expression.eq( "emailUpdateCode", new Integer(NOTIFY_UPDATE_S04_RECEIVE_BUDDY_REQ_CONFIRM) ));
            crit.add( Expression.lt( "dateEffective", DateObjectHelper.getYesterdayDateObject() ));
            
            valueList = crit.list();
            
            if (valueList == null || valueList.isEmpty()) {
                //session.beginTransaction();
                System.out.println("Insert new update now.");
                session.save(emailUpdate);
                //session.getTransaction().commit();
            } else {
                System.out.println("Skip insert new update, existing found");
            }
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return emailUpdate;
    }
}
