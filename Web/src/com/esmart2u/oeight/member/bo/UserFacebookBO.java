/*
 * UserFacebookBO.java
 *
 * Created on April 2, 2008, 4:30 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

//import com.esmart2u.oeight.data.FacebookPhoto;
//import com.esmart2u.oeight.data.FacebookUser;
import com.esmart2u.oeight.data.FacebookPhoto;
import com.esmart2u.oeight.data.FacebookUser;
import com.esmart2u.oeight.data.OEightPledge;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserFacebook;
//import com.esmart2u.oeight.data.UserFacebook;
import com.esmart2u.oeight.data.UserWidgetFacebook;
import com.esmart2u.oeight.exception.FacebookException;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.UserFacebookVO;
import com.esmart2u.oeight.member.web.struts.controller.FacebookForm;
import com.esmart2u.oeight.member.web.struts.helper.WidgetHelper;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import com.facebook.api.FacebookRestClient;
import com.facebook.api.ProfileField;
import com.facebook.api.schema.FriendsGetResponse;
import com.facebook.api.schema.Photo;
import com.facebook.api.schema.PhotosGetResponse;
import java.io.IOException;
import java.util.*;
import javax.xml.xpath.*;
import org.apache.log4j.*;   
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.w3c.dom.*;
import com.esmart2u.oeight.data.UserWidgetFacebook;
import com.esmart2u.solution.base.logging.Logger;
import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class UserFacebookBO {
    
    private static Logger logger = Logger.getLogger(UserFacebookBO.class);
    
    /** Creates a new instance of UserFacebookBO */
    public UserFacebookBO() {
    }
    
    /**********************************************************************
     *
     *  NOTE : 1 Facebook profile can only have 1 OEight Profile, 
     *          but 1 OEight profile can have multiple Facebook profile
     *
     *********************************************************************/
    
    public static UserWidgetFacebook getByFacebookId(String facebookUserId, String widgetType) { 
        List valueList = null;
        Session session = null;
        UserWidgetFacebook userWidgetFacebook = null;
        Transaction transaction = null; 
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            //session.beginTransaction();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWidgetFacebook.class);
            crit.add( Expression.eq( "facebookUserId", Long.parseLong(facebookUserId)) );
            crit.add( Expression.eq( "widgetType", Integer.parseInt(widgetType) ));
              
            crit.addOrder( Order.desc("dateInstalled") );
            valueList = crit.list(); 
         
            
            if (valueList != null && !valueList.isEmpty()) {  
                userWidgetFacebook = (UserWidgetFacebook)valueList.get(0);    
                
                // Clear query list for resource
                valueList = null; 
            } else {
                logger.info("is empty user widget list!");
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userWidgetFacebook;   
        
    }
    
    public UserFacebookVO newCallback1Install(UserFacebookVO userFacebookVO) throws Exception{
        
        //long uid = Long.parseLong(userFacebookVO.getUid());
        String sessionKey = userFacebookVO.getSessionId(); 
        
        String apiKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK1_API_KEY);
        String secretKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK1_SECRET);
        
        String token = userFacebookVO.getAuthToken();

        System.out.println("Going to get client with apiKey=" + apiKey);
        System.out.println("Going to get client with secretKey=" + secretKey);
        System.out.println("token =" + token);
        
        FacebookRestClient client = null;
        try {

            if (StringUtils.hasValue(sessionKey)) {
                System.out.println("Getting with sessionKey");
                client = new FacebookRestClient(apiKey, secretKey, sessionKey); 
                
            } else if (StringUtils.hasValue(token)) {
                System.out.println("Getting with token");
                client = new FacebookRestClient(apiKey, secretKey);
                System.out.println("After client, now get session");
                sessionKey = client.auth_getSession(token);
                /*
                session.setAttribute("facebookSession", sessionKey);
                sessionKey = frc.auth_getSession(token);
                session.setAttribute("facebookSession", sessionKey); */

            } else { 
                System.out.println("Error in initializing Facebook client");
            } 
        } catch (IOException ioe) {
        }  
 
        if (client == null)
        {
            throw new FacebookException("FacebookRestClient is null, unable to get token"); 
        }
        
        long uid = client.users_getLoggedInUser();
        System.out.println("Facebook User id = " + uid);
        userFacebookVO.setUid("" + uid);
        
        Set set = new HashSet();
        set.add(ProfileField.FIRST_NAME);
        set.add(ProfileField.LAST_NAME);  
        set.add(ProfileField.PIC);
        set.add(ProfileField.PIC_SQUARE);
        
        List userList = new ArrayList();
        userList.add(new Long(uid));
        Document doc = client.users_getInfo(userList,set); 
        String firstName = doc.getElementsByTagName(ProfileField.FIRST_NAME.toString()).item(0).getTextContent();
        String lastName = doc.getElementsByTagName(ProfileField.LAST_NAME.toString()).item(0).getTextContent();
        String primaryPhoto = doc.getElementsByTagName(ProfileField.PIC.toString()).item(0).getTextContent();
        String primarySquarePhoto = doc.getElementsByTagName(ProfileField.PIC_SQUARE.toString()).item(0).getTextContent();
  
        System.out.println("first_name: " + firstName);
        System.out.println("last_name: " + lastName);
        System.out.println("primary photo id: " + primaryPhoto); 
        System.out.println("primary photo sqr: " + primarySquarePhoto);
             
        // If able to reach here, facebookUserId is a valid one
        // Set FBML
        System.out.println("Going to set fbml"); 
        
        UserBO userBO = new UserBO();
        User user = userBO.getUserById("" + userFacebookVO.getUserId());
        String fbml = WidgetHelper.getMainWidgetForFacebook(user); 
        client.profile_setFBML(new Long(uid),fbml,null,null);
        //client.profile_setFBML(new Long(uid),"<fb:visible-to-added-app-users>Showed?</fb:visible-to-added-app-users><fb:ref url=\"" + fbmlPage + "\"/>",null,null) ;
        //client.profile_setProfileFBML("Testing 123 over here, pls show me");
         
        System.out.println("fbml set");
        
        // Insert into db
        userFacebookVO = insertUserWidgetFacebook(userFacebookVO, FacebookForm.MAIN_TYPE);
        
        // Try get primary photo and friends (NOTE: ignore if unable to get)
        /*
        try{  
            List photosIdList = new ArrayList(); 
             
            client.friends_get();
            FriendsGetResponse response = (FriendsGetResponse)client.getResponsePOJO();    
            List<Long> friends = response.getUid();
            
            // This will enable you to get a list of user info
            //client.users_getInfo(friends,set);
             
            client.photos_get(new Long(uid), new Long(null));
            PhotosGetResponse photoResponse = (PhotosGetResponse)client.getResponsePOJO();
            List<Photo> photos = photoResponse.getPhoto();
             
            if ((photos != null) && (!photos.isEmpty()))
            {
                for (int i=0;i<photos.size();i++)
                { 
                    Photo photo = (Photo)photos.get(i);
                    Long photoId = new Long(photo.getPid());
                    photosIdList.add(photoId);
                }
            }
          
            insertUserFacebookInfo(userFacebookVO, primaryPhoto, friends, photosIdList);
        }catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println("Error getting facebook additional info, ignoring it and continue");
        }
        */
        return userFacebookVO; 
        
    }
    
    private UserFacebookVO insertUserWidgetFacebook(UserFacebookVO userFacebookVO, String type)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            UserWidgetFacebook userWidgetFacebook = getByFacebookId(userFacebookVO.getUid(), type); 

            boolean hasUserId = StringUtils.hasValue(userFacebookVO.getUserId())? true : false;
            long userId = hasUserId? Long.parseLong(userFacebookVO.getUserId()) : 0;

            if (userWidgetFacebook == null){
                userWidgetFacebook = new UserWidgetFacebook();
                userWidgetFacebook.setUserId(userId);
                userWidgetFacebook.setUserName(userFacebookVO.getUserName());
                userWidgetFacebook.setFacebookUserId(Long.parseLong(userFacebookVO.getUid()));
                userWidgetFacebook.setWidgetType(Integer.parseInt(type));
                userWidgetFacebook.setDateInstalled(new Date());
                userWidgetFacebook.setViews(0);
                // Set the user object
                if(hasUserId){
                    UserBO userBO = new UserBO();
                    User user = userBO.getUserById(""+userId);
                    userWidgetFacebook.setUser(user);
                }
                session.save(userWidgetFacebook); 
            }
            else
            {
                userWidgetFacebook.setUserId(userId);
                userWidgetFacebook.setUserName(userFacebookVO.getUserName());
                session.saveOrUpdate(userWidgetFacebook); 
            }

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userFacebookVO;
    }
      
    private UserFacebookVO insertUserFacebookInfo(UserFacebookVO userFacebookVO, String primaryPhoto, List friends, List photos )
    {   
            return insertUserFacebookInfo(userFacebookVO, primaryPhoto, friends, photos, null);
    }
    
    private UserFacebookVO insertUserFacebookInfo(UserFacebookVO userFacebookVO, String primaryPhoto, List friends, List photos, Session session )
    {  
        boolean atomicTransaction = false;
        if (session == null){
            session = HibernateUtil.getSessionFactory().openSession();
            atomicTransaction = true;
        }
        
        session.beginTransaction(); 
  
         Criteria crit = session.createCriteria(UserFacebook.class)
            .add( Expression.eq( "userId", new Long(userFacebookVO.getUserId())))
            .add( Expression.eq( "facebookUserId", new Long(userFacebookVO.getUid())));
         UserFacebook userFacebook = (UserFacebook)crit.uniqueResult();
            
        if (userFacebook == null){
            userFacebook = new UserFacebook();
            userFacebook.setUserId(Long.parseLong(userFacebookVO.getUserId())); 
            userFacebook.setFacebookUserId(Long.parseLong(userFacebookVO.getUid())); 
            System.out.println("Primary Photo=" + primaryPhoto);
            userFacebook.setPrimaryPhoto(primaryPhoto);
            System.out.println("Setting friends String");
            userFacebook.setFriendsString(getDelimitedString(friends));
            System.out.println("Setting photos String");
            userFacebook.setPhotosString(getDelimitedString(photos));
            session.save(userFacebook); 
        }
        else
        { 
             
            userFacebook.setPrimaryPhoto(primaryPhoto);
            userFacebook.setFriendsString(getDelimitedString(friends));
            userFacebook.setPhotosString(getDelimitedString(photos));
            session.saveOrUpdate(userFacebook); 
        } 
         
        if (atomicTransaction){

            session.getTransaction().commit(); 
            
            if (session.isOpen()){
                session.close();
            }
        }
        
        return userFacebookVO;
    }

    private UserFacebookVO insertUserFacebookInfo(UserFacebookVO userFacebookVO, String primaryPhoto, List friends, List photos, HashMap facebookFriendMap, List facebookPhotoList)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        userFacebookVO = insertUserFacebookInfo(userFacebookVO, primaryPhoto, friends, photos, session);
        Date dateUpdated = new Date();
        
        if (facebookFriendMap != null && !facebookFriendMap.isEmpty())
        {
            Set keys = facebookFriendMap.keySet();
            Iterator iterator = keys.iterator(); 
            List valueList = new ArrayList();
            while (iterator.hasNext())
            {
                valueList.add(facebookFriendMap.get(iterator.next()));
            }
            for(int i=0;i<valueList.size();i++)
            {
                FacebookUser friend = (FacebookUser)valueList.get(i);
                   
                FacebookUser dbFriend = (FacebookUser)session.createCriteria(FacebookUser.class) 
                .add( Restrictions.idEq(new Long(friend.getFacebookUserId())))
                .uniqueResult(); 
                
                if (dbFriend != null)
                {
                    friend = dbFriend;
                }
                
                friend.setDateUpdated(dateUpdated);
                // Find existing
                session.saveOrUpdate(friend);
            }
        }
        
        if (facebookPhotoList != null && !facebookPhotoList.isEmpty())
        {
            for(int i=0;i<facebookPhotoList.size();i++)
            {
                FacebookPhoto photo = (FacebookPhoto)facebookPhotoList.get(i);
                
                FacebookPhoto dbPhoto = (FacebookPhoto)session.createCriteria(FacebookPhoto.class)
                .add( Restrictions.naturalId()
                .set("photoId", photo.getPhotoId()))
                .uniqueResult();
                
                if (dbPhoto != null)
                {
                    photo = dbPhoto;
                }
                
                
                photo.setDateUpdated(dateUpdated); 
                
                session.saveOrUpdate(photo);
            }
        }
                
        session.getTransaction().commit();
        if (session.isOpen()){
            session.close();
        }
        //facebookPhotoList
        return userFacebookVO;
    }
    
    
    public UserFacebookVO updateCallbackViewsCounter(UserFacebookVO userFacebookVO, String type) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            UserWidgetFacebook userWidgetFacebook = getByFacebookId(userFacebookVO.getUid(), type); 

            if (userWidgetFacebook != null){ 
                userWidgetFacebook.setViews(userWidgetFacebook.getViews() + 1);
                // Set the user object
                session.saveOrUpdate(userWidgetFacebook); 
            }  

       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userFacebookVO;
    }

    private String getDelimitedString(List valueList) {
        StringBuffer resultString = new StringBuffer();
        String result = "";
        if (valueList != null && !valueList.isEmpty())
        { 
            System.out.println("valuelist size=" + valueList.size());
            for(int i=0; i<valueList.size();i++)
            {
                long value = ((Long)valueList.get(i)).longValue();
                resultString.append(value + ",");  
            } 
            System.out.println("Result string=" + resultString.toString());
            if (resultString.length() > 999)
            {
                result = resultString.substring(0,999);
            }  
            result = resultString.toString();
            result = result.substring(0, result.lastIndexOf(","));
        } 
        System.out.println("final result=" + result);
        return result; 
    }
 

    /**********************************************************************
     *
     *  NOTE : 1 Facebook profile can only have 1 OEight Profile, 
     *          but 1 OEight profile can have multiple Facebook profile,
     *         SO userId can return multiple rows of different Facebook
     *
     *********************************************************************/
    public static boolean hasFacebookXChange(String userId) {
        boolean hasFacebookXChange = false;
        List valueList;
        Session session = null;
        UserWidgetFacebook userWidgetFacebook = null;
        Transaction transaction = null; 
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            //session.beginTransaction();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWidgetFacebook.class);
            crit.add( Expression.eq( "userId", Long.parseLong(userId)) );
            crit.add( Expression.eq( "widgetType", Integer.parseInt(FacebookForm.FRIENDS_XCHANGE_TYPE) ));
              
            crit.addOrder( Order.desc("dateInstalled") );
            valueList = crit.list(); 
         
            
            if (valueList != null && !valueList.isEmpty()) { 
               hasFacebookXChange = true;
            } else {
                logger.info("is empty user widget list!");
            } 
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return hasFacebookXChange;
    }

    public UserFacebookVO newCallback3Install(UserFacebookVO userFacebookVO) throws Exception {
        
         //long uid = Long.parseLong(userFacebookVO.getUid());
        String sessionKey = userFacebookVO.getSessionId(); 
        
        String apiKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK3_API_KEY);
        String secretKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK3_SECRET);
        
        String token = userFacebookVO.getAuthToken();

        System.out.println("Going to get client with apiKey=" + apiKey);
        System.out.println("Going to get client with secretKey=" + secretKey);
        System.out.println("token =" + token);
        
        FacebookRestClient client = null;
        try {

            if (StringUtils.hasValue(sessionKey)) {
                System.out.println("Getting with sessionKey");
                client = new FacebookRestClient(apiKey, secretKey, sessionKey); 
                
            } else if (StringUtils.hasValue(token)) {
                System.out.println("Getting with token");
                client = new FacebookRestClient(apiKey, secretKey);
                System.out.println("After client, now get session");
                sessionKey = client.auth_getSession(token); 

            } else { 
                System.out.println("Error in initializing Facebook client");
            } 
        } catch (IOException ioe) {
        }  
 
        if (client == null)
        {
            throw new FacebookException("FacebookRestClient is null, unable to get token"); 
        }
        
        long uid = client.users_getLoggedInUser();
        System.out.println("Facebook User id = " + uid);
        userFacebookVO.setUid("" + uid);
        
        Set set = new HashSet();
        set.add(ProfileField.FIRST_NAME);
        set.add(ProfileField.LAST_NAME);  
        set.add(ProfileField.PIC);
        set.add(ProfileField.PIC_SQUARE);
        
        List userList = new ArrayList();
        userList.add(new Long(uid));
        Document doc = client.users_getInfo(userList,set); 
        String firstName = doc.getElementsByTagName(ProfileField.FIRST_NAME.toString()).item(0).getTextContent();
        String lastName = doc.getElementsByTagName(ProfileField.LAST_NAME.toString()).item(0).getTextContent();
        String primaryPhoto = doc.getElementsByTagName(ProfileField.PIC.toString()).item(0).getTextContent();
        String primarySquarePhoto = doc.getElementsByTagName(ProfileField.PIC_SQUARE.toString()).item(0).getTextContent();
  
        System.out.println("first_name: " + firstName);
        System.out.println("last_name: " + lastName);
        System.out.println("primary photo id: " + primaryPhoto); 
        System.out.println("primary photo sqr: " + primarySquarePhoto);
             
        // If able to reach here, facebookUserId is a valid one
        // Set FBML
        System.out.println("Going to set fbml"); 
        
        UserBO userBO = new UserBO();
        User user = userBO.getUserById("" + userFacebookVO.getUserId());
        String fbml = WidgetHelper.getBlogWidgetForFacebook(user);    
        client.profile_setFBML(new Long(uid),fbml,null,null); 
        
        System.out.println("fbml set");
        
        // Insert into db
        userFacebookVO = insertUserWidgetFacebook(userFacebookVO, FacebookForm.FEATURED_BLOG_TYPE);
        
        return userFacebookVO; 
         
    }

    public boolean hasPledge(String facebookUserId) {
        boolean hasPledge = false;
        EarthBO earthBO = new EarthBO();
        OEightPledge pledge = earthBO.getLatestPledgeFromThirdParty( 
                OEightConstants.CAMPAIGN_INVITE_FROM_FACEBOOK, facebookUserId);
        if (pledge != null)
        {
            hasPledge = true;
        } 
        earthBO = null;
        return hasPledge;
    }

    public UserFacebookVO newCallback6Install(UserFacebookVO userFacebookVO, int pledgeCode)  throws Exception{
        
        String sessionKey = userFacebookVO.getSessionId(); 
        
        String apiKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK6_API_KEY);
        String secretKey = PropertyManager.getValue(PropertyConstants.SERVICE_FACEBOOK_CALLBACK6_SECRET);
        
        String token = userFacebookVO.getAuthToken();

        System.out.println("Going to get client with apiKey=" + apiKey);
        System.out.println("Going to get client with secretKey=" + secretKey);
        System.out.println("token =" + token);
        
        FacebookRestClient client = null;
        try {

            if (StringUtils.hasValue(sessionKey)) {
                System.out.println("Getting with sessionKey");
                client = new FacebookRestClient(apiKey, secretKey, sessionKey); 
                
            } else if (StringUtils.hasValue(token)) {
                System.out.println("Getting with token");
                client = new FacebookRestClient(apiKey, secretKey);
                System.out.println("After client, now get session");
                sessionKey = client.auth_getSession(token);
                /*
                session.setAttribute("facebookSession", sessionKey);
                sessionKey = frc.auth_getSession(token);
                session.setAttribute("facebookSession", sessionKey); */

            } else { 
                System.out.println("Error in initializing Facebook client");
            } 
        } catch (IOException ioe) {
        }  
 
        if (client == null)
        {
            throw new FacebookException("FacebookRestClient is null, unable to get token"); 
        }
        
        long uid = client.users_getLoggedInUser();
        System.out.println("Facebook User id = " + uid);
        userFacebookVO.setUid("" + uid);
        
        Set set = new HashSet();
        set.add(ProfileField.FIRST_NAME);
        set.add(ProfileField.LAST_NAME);  
        //set.add(ProfileField.PIC);
        //set.add(ProfileField.PIC_SQUARE);
        
        List userList = new ArrayList();
        userList.add(new Long(uid));
        Document doc = client.users_getInfo(userList,set); 
        String firstName = doc.getElementsByTagName(ProfileField.FIRST_NAME.toString()).item(0).getTextContent();
        String lastName = doc.getElementsByTagName(ProfileField.LAST_NAME.toString()).item(0).getTextContent();
        //String primaryPhoto = doc.getElementsByTagName(ProfileField.PIC.toString()).item(0).getTextContent();
        //String primarySquarePhoto = doc.getElementsByTagName(ProfileField.PIC_SQUARE.toString()).item(0).getTextContent();
  
        System.out.println("first_name: " + firstName);
        System.out.println("last_name: " + lastName);
        //System.out.println("primary photo id: " + primaryPhoto); 
        //System.out.println("primary photo sqr: " + primarySquarePhoto);
             
        // If able to reach here, facebookUserId is a valid one
        // Set FBML
            
        // If able to reach here, friendsterUserId is a valid one
        OEightPledge oeightPledge = new OEightPledge();
        oeightPledge.setPledgerName(firstName + " " + lastName);
        oeightPledge.setPledgeCode(pledgeCode); 
        oeightPledge.setMemberFlag(false);
        oeightPledge.setPledgeFrom(OEightConstants.CAMPAIGN_INVITE_FROM_FACEBOOK);
        oeightPledge.setPledgeFromUserId(uid);
        oeightPledge.setShowPledgeFrom(true);
        
        EarthBO earthBO = new EarthBO();
        earthBO.savePledge(oeightPledge);
        earthBO = null; 
        
        System.out.println("Going to set fbml"); 
         
        String fbml = WidgetHelper.getFacebookClimateChangeWidget(""+uid); 
        System.out.println("Setting fbml:" + fbml + "\n to profile of:" + uid);
        client.profile_setFBML(new Long(uid),fbml,null,null); 
         
        System.out.println("fbml set");
        
        // Insert into db
        //userFacebookVO = insertUserWidgetFacebook(userFacebookVO, FacebookForm.CLIMATE_CHANGE_TYPE);
        
        return userFacebookVO;
    }
}
