/*
 * MembersUpdateBO.java
 *
 * Created on January 18, 2008, 11:41 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.MembersUpdate;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

/**
 *
 * @author meauchyuan.lee
 */
public class MembersUpdateBO {
    
    private static Logger logger = Logger.getLogger(MembersUpdateBO.class);
    
    private static MembersUpdateBO instance;
    private static HashMap latestLogin = null;
    private static int bufferLoginCount = 0;
    private static HashMap latestNewMembers = null;
    private static int bufferNewMembersCount = 0;
    
    private static long currentTime = 0;
    
    /** Creates a new instance of MembersUpdateBO */
    public MembersUpdateBO() {
    }
    
    public static synchronized MembersUpdateBO getInstance(){
        if ( instance == null ){
            instance = new MembersUpdateBO();
            System.out.println("initializing");
            getLatestLoginList();
            getLatestNewMembersList();
            currentTime = new Date().getTime();
        }
        return instance;
    }
    
    
    public static HashMap getLatestLoginList() {
        System.out.println("Login list is null");
        if (getLatestLogin() == null){
            
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                Criteria crit = session.createCriteria(MembersUpdate.class);
                crit.add(Expression.eq( "updateType", OEightConstants.MEMBERS_UPDATE_TYPE_LOGIN));
                crit.addOrder( Order.desc("dateUpdate") );
                crit.setMaxResults(8);
                System.out.println("Got from db");
                List latestList = crit.list();
                System.out.println("Latest list=" + latestList);
                
                setLatestLogin(new HashMap());
                if (latestList != null) {
                    System.out.println("Latest list size=" + latestList.size());
                    for(int i=latestList.size();i>0;i--) {
                        System.out.println("Put into hashmap");
                        getLatestLogin().put(new Integer(i),latestList.get(i-1));
                        System.out.println("Login list size=" + getLatestLogin().size());
                    }
                }
                
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
        }
        System.out.println("returning list" + getLatestLogin().size());
        return getLatestLogin();
    }
    
    public static HashMap getLatestNewMembersList() {
        if (getLatestNewMembers() == null) {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                
                Criteria crit = session.createCriteria(MembersUpdate.class);
                crit.add(Expression.eq( "updateType", OEightConstants.MEMBERS_UPDATE_TYPE_REGISTER));
                crit.addOrder( Order.desc("dateUpdate") );
                crit.setMaxResults(8);
                List latestList = crit.list();
                
                setLatestNewMembers(new HashMap());
                if (latestList != null) {
                    for(int i=latestList.size();i>0;i--) {
                        getLatestNewMembers().put(new Integer(i),latestList.get(i-1));
                    }
                }
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
        }
        return getLatestNewMembers();
    }
    
    public static void insertNewLatestLogin(MembersUpdate membersUpdate) {
        // Check that the user is not already inside
        if (!alreadyInsideList(getLatestLogin(), membersUpdate)){
            getLatestLogin().put(new Integer(getLatestLogin().size()+1), membersUpdate);
            bufferLoginCount+=1;
            
            //if (bufferLoginCount > 8)
            //{
            Session session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = null;
            try {
                
                transaction = session.beginTransaction();
                // Delete all latest login in table
                String hql = "delete MembersUpdate where updateType = :updateType";
                Query query = session.createQuery(hql);
                query.setCharacter("updateType", OEightConstants.MEMBERS_UPDATE_TYPE_LOGIN);
                int rowCount = query.executeUpdate();
                logger.debug("Members Update - Latest Login:" + rowCount + " deleted");
                
                // Insert all into db
                for(int i=0;i<getLatestLogin().size();i++) {
                    MembersUpdate updateItem = (MembersUpdate)getLatestLogin().get(i+1);
                    session.save(updateItem);
                }
                logger.debug("Members Update - Latest Login:" + getLatestLogin().size() + " inserted");
                
                setLatestLogin(null);
                bufferLoginCount = 0;
                transaction.commit();
                
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
            getLatestLoginList();
            //}
        }
    }
    
    private static boolean alreadyInsideList(HashMap lastLoginMap, MembersUpdate membersUpdate) {
        boolean inside = false;
        
        if (lastLoginMap != null && !lastLoginMap.isEmpty()) {
            for(int i=0;i<lastLoginMap.size();i++) {
                MembersUpdate cacheMember = (MembersUpdate)lastLoginMap.get(i+1);
                if (cacheMember.getUserId() == membersUpdate.getUserId()) {
                    cacheMember.setDateUpdate(membersUpdate.getDateUpdate());
                    return true;
                }
            }
            
        }
        return inside;
    }
    
    public static void insertNewLatestMember(MembersUpdate membersUpdate) {
        getLatestNewMembers().put(new Integer(getLatestNewMembers().size()+1), membersUpdate);
        bufferNewMembersCount+=1;
        
        //if (bufferNewMembersCount > 8)
        //{
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            // Delete all latest new member in table
            String hql = "delete MembersUpdate where updateType = :updateType";
            Query query = session.createQuery(hql);
            query.setCharacter("updateType", OEightConstants.MEMBERS_UPDATE_TYPE_REGISTER);
            int rowCount = query.executeUpdate();
            logger.debug("Members Update - Latest New Members:" + rowCount + " deleted");
            
            // Insert all into db
            for(int i=0;i<getLatestNewMembers().size();i++) {
                MembersUpdate updateItem = (MembersUpdate)getLatestNewMembers().get(i+1);
                session.save(updateItem);
            }
            logger.debug("Members Update - Latest New Members:" + getLatestNewMembers().size() + " inserted");
            
            setLatestNewMembers(null);
            bufferNewMembersCount = 0;
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        getLatestNewMembersList();
        //}
        
    }
    
    public static HashMap getLatestLogin() {
        return getInstance().latestLogin;
    }
    
    public static void setLatestLogin(HashMap aLatestLogin) {
        getInstance().latestLogin = aLatestLogin;
    }
    
    public static HashMap getLatestNewMembers() {
        return getInstance().latestNewMembers;
    }
    
    public static void setLatestNewMembers(HashMap aLatestNewMembers) {
        getInstance().latestNewMembers = aLatestNewMembers;
    }
    
}
