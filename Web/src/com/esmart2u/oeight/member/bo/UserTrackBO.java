/*
 * UserTrackBO.java
 *
 * Created on May 6, 2008, 12:06 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.UserTrack;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
/**
 *
 * @author meauchyuan.lee
 */
public class UserTrackBO {
    
    /** Creates a new instance of UserTrackBO */
    public UserTrackBO() {
    }
    
    /**
     *  Gets the tracked value of the track code
     */
    public static int getTrackValue(String userId, int trackCode) {
        int updatedTrackValue = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            // Update userTrack
            UserTrack userTrack = (UserTrack)session.createCriteria(UserTrack.class)
            .add( Restrictions.eq("userId", new Long(userId)))
            .add( Restrictions.eq("trackCode", new Integer(trackCode)))
            .add( Restrictions.eq("trackDate",DateObjectHelper.getCurrentDayDateObject()))
            .uniqueResult();
            
            if (userTrack != null) {
                updatedTrackValue = userTrack.getTrackValue();
            }
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return updatedTrackValue;
    }
    
    /**
     *  Increase the value of tracking from userId and trackCode.
     *  Method will return the trackValue after increase.
     */
    public static int increaseTrackValue(String userId, int trackCode) {
        int updatedTrackValue = 0;
        UserTrack userTrack = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            // Update userTrack
            userTrack = (UserTrack)session.createCriteria(UserTrack.class)
            .add( Restrictions.eq("userId", new Long(userId)))
            .add( Restrictions.eq("trackCode", new Integer(trackCode)))
            .add( Restrictions.eq("trackDate",DateObjectHelper.getCurrentDayDateObject()))
            .uniqueResult();
            
            if (userTrack == null) {
                userTrack = new UserTrack();
                userTrack.setUserId(Long.parseLong(userId));
                userTrack.setTrackCode(trackCode);
                userTrack.setTrackDate(DateObjectHelper.getCurrentDayDateObject());
                userTrack.setTrackValue(1);
                session.save(userTrack);
            } else {
                userTrack.setTrackValue(userTrack.getTrackValue() + 1);
                session.update(userTrack);
            }
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        updatedTrackValue = userTrack.getTrackValue();
        return updatedTrackValue;
    }
    
}
