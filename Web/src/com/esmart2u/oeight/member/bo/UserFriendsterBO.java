/*
 * UserFriendsterBO.java
 *
 * Created on March 22, 2008, 10:01 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.FriendsterAPITest;
import com.esmart2u.oeight.data.FriendsterPhoto;
import com.esmart2u.oeight.data.FriendsterUser;
import com.esmart2u.oeight.data.OEightPledge;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserFriendster;
import com.esmart2u.oeight.data.UserWidgetFriendster;
import com.esmart2u.oeight.exception.FriendsterException;
import com.esmart2u.oeight.member.helper.EmailAddressHelper;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.UserFriendsterVO;
import com.esmart2u.oeight.member.web.struts.controller.FriendsterForm;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import com.tripadvisor.friendster.FriendsterClient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.xml.xpath.*;
import org.apache.log4j.*;   
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.w3c.dom.*; 

/**
 *
 * @author meauchyuan.lee
 */
public class UserFriendsterBO {
    
    private static Logger logger = Logger.getLogger(UserFriendsterBO.class);
    
    /** Creates a new instance of UserFriendsterBO */
    public UserFriendsterBO() {
    }
    
    /**********************************************************************
     *
     *  NOTE : 1 Friendster profile can only have 1 OEight Profile, 
     *          but 1 OEight profile can have multiple Friendster profile
     *
     *********************************************************************/
    
    public static UserWidgetFriendster getByFriendsterId(String friendsterUserId, String widgetType) { 
        List valueList = null;
        Session session = null;
        UserWidgetFriendster userWidgetFriendster = null;
        Transaction transaction = null; 
        
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            //session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWidgetFriendster.class);
            crit.add( Expression.eq( "friendsterUserId", Long.parseLong(friendsterUserId)) );
            crit.add( Expression.eq( "widgetType", Integer.parseInt(widgetType) ));
              
            crit.addOrder( Order.desc("dateInstalled") );
            valueList = crit.list(); 
         
            
            if (valueList != null && !valueList.isEmpty()) { 
                //for(int i=0;i<valueList.size();i++) {
                //    userWidgetFriendster = (UserWidgetFriendster)valueList.get(i);   
                //} 
                userWidgetFriendster = (UserWidgetFriendster)valueList.get(0);   
                
                // Clear query list for resource
                valueList = null; 
            } else {
                logger.info("is empty user widget list!");
            }
            
            //session.getTransaction().commit(); 
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            session.close();
        }
        return userWidgetFriendster;   
        
    }
    
    public UserFriendsterVO newCallback1Install(UserFriendsterVO userFriendsterVO) throws Exception{
        
        int uid = Integer.parseInt(userFriendsterVO.getUser_id());
        String sessionKey = userFriendsterVO.getSession_key();
    
        XPath xpath = XPathFactory.newInstance().newXPath();
        
        String apiKey = userFriendsterVO.getApi_key();
        String secretKey = PropertyManager.getValue(PropertyConstants.SERVICE_FRIENDSTER_CALLBACK1_SECRET);
        FriendsterClient fc = new FriendsterClient(sessionKey, userFriendsterVO.getApi_key(),secretKey); 
        
        if (fc == null || fc.getToken() == null)
        {
            throw new FriendsterException("FriendsterClient is null, unable to get token"); 
        }
        
        String sToken = fc.getToken(); 
        System.out.println("stoken=" + sToken);


        Document doc = fc.getUser(uid);
        //out.println("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
        //out.println("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
        System.out.println("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
        System.out.println("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
        
        String widgetHTML = "<script src='http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=main&id=" + userFriendsterVO.getUserName() +"'></script>" +
                            "<br><center><a href=\"" + PropertyManager.getValue(PropertyConstants.SERVICE_FRIENDSTER_CALLBACK1_INSTALL_URL)+ "\">Grab This App</a></center>";
        fc.updateProfile(widgetHTML,1);
            
        // If able to reach here, friendsterUserId is a valid one
        
        // Insert into db
        userFriendsterVO = insertUserWidgetFriendster(userFriendsterVO, FriendsterForm.MAIN_TYPE);
        
        // Try get primary photo and friends (NOTE: ignore if unable to get)
        try{
            String primaryPhoto = "";
            List friendsInteger = null;
            List friendsString = null;;
            List photos = new ArrayList();
               
            doc = fc.getPrimaryPhoto(uid);
            primaryPhoto =  xpath.evaluate("/primaryphoto_response/photo/pid", doc);
            System.out.println("primary photo id: " + primaryPhoto);
            String primaryPhotoSrc =  xpath.evaluate("/primaryphoto_response/photo/src_small", doc);
            System.out.println("primary photo src: " + primaryPhotoSrc);
            
             
            friendsInteger = fc.getFriends(uid);
            
            System.out.println("friends: " + friendsInteger); 
            if (friendsInteger != null){
                friendsString = new ArrayList();
                int maxCount = friendsInteger.size() < 8? friendsInteger.size() : 8;
                for (int i = 0;i < maxCount;i++)
                {
                    System.out.println("uid[" + i + "]: " + friendsInteger.get(i));
                    String friendsId = ""+((Integer)friendsInteger.get(i)).intValue(); 
                    friendsString.add(friendsId); 
                    Document friendsDoc = fc.getPrimaryPhoto(((Integer)friendsInteger.get(i)).intValue()); 
                    String friendsPrimaryPhoto =  xpath.evaluate("/primaryphoto_response/photo/src_small", friendsDoc);
                    System.out.println("friendsPrimaryPhoto: " + friendsPrimaryPhoto);
                }
            }
             
            doc = fc.getPhotos(uid); 
            NodeList nl = (NodeList) xpath.evaluate("/photos_response/photo", doc, XPathConstants.NODESET);
            int maxCount = nl.getLength() < 8? nl.getLength() : 8;
            for (int i = 0;i < maxCount;i++)
            { 
                //System.out.println("url[" + i + "]: " + xpath.evaluate("src", nl.item(i)));
                long pid = Long.parseLong(xpath.evaluate("pid", nl.item(i)));
                System.out.println("pid[" + i + "]: " + pid);
                photos.add(Long.toString(pid));
            } 
          
            insertUserFriendsterInfo(userFriendsterVO, primaryPhoto, friendsString, photos);
        }catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println("Error getting friendster additional info, ignoring it and continue");
        }
        
        return userFriendsterVO; 
        
    }
    
    private UserFriendsterVO insertUserWidgetFriendster(UserFriendsterVO userFriendsterVO, String type)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            UserWidgetFriendster userWidgetFriendster = getByFriendsterId(userFriendsterVO.getUser_id(), type); 

            boolean hasUserId = StringUtils.hasValue(userFriendsterVO.getUserId())? true : false;
            long userId = hasUserId? Long.parseLong(userFriendsterVO.getUserId()) : 0;

            if (userWidgetFriendster == null){
                userWidgetFriendster = new UserWidgetFriendster();
                userWidgetFriendster.setUserId(userId);
                userWidgetFriendster.setUserName(userFriendsterVO.getUserName());
                userWidgetFriendster.setFriendsterUserId(Long.parseLong(userFriendsterVO.getUser_id()));
                userWidgetFriendster.setWidgetType(Integer.parseInt(type));
                userWidgetFriendster.setDateInstalled(new Date());
                userWidgetFriendster.setViews(0);
                // Set the user object
                if(hasUserId){
                    UserBO userBO = new UserBO();
                    User user = userBO.getUserById(""+userId);
                    userWidgetFriendster.setUser(user);
                }
                session.save(userWidgetFriendster); 
            }
            else
            {
                userWidgetFriendster.setUserId(userId);
                userWidgetFriendster.setUserName(userFriendsterVO.getUserName());
                session.saveOrUpdate(userWidgetFriendster); 
            }

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userFriendsterVO;
    }
      
    private UserFriendsterVO insertUserFriendsterInfo(UserFriendsterVO userFriendsterVO, String primaryPhoto, List friends, List photos )
    {   
            return insertUserFriendsterInfo(userFriendsterVO, primaryPhoto, friends, photos, null);
    }
    
    private UserFriendsterVO insertUserFriendsterInfo(UserFriendsterVO userFriendsterVO, String primaryPhoto, List friends, List photos, Session session )
    {  
        boolean atomicTransaction = false;
        if (session == null){
            session = HibernateUtil.getSessionFactory().openSession();
            atomicTransaction = true;
        }
        
        Date now = new Date();
        session.beginTransaction(); 
  
         Criteria crit = session.createCriteria(UserFriendster.class)
            .add( Expression.eq( "userId", new Long(userFriendsterVO.getUserId())))
            .add( Expression.eq( "friendsterUserId", new Long(userFriendsterVO.getUser_id())));
         UserFriendster userFriendster = (UserFriendster)crit.uniqueResult();
            
        if (userFriendster == null){
            userFriendster = new UserFriendster();
            userFriendster.setUserId(Long.parseLong(userFriendsterVO.getUserId())); 
            userFriendster.setFriendsterUserId(Long.parseLong(userFriendsterVO.getUser_id())); 
            System.out.println("Primary Photo=" + primaryPhoto);
            userFriendster.setPrimaryPhoto(primaryPhoto);
            System.out.println("Setting friends String");
            userFriendster.setFriendsString(getDelimitedString(friends));
            System.out.println("Setting photos String");
            userFriendster.setPhotosString(getDelimitedString(photos));
            userFriendster.setDateUpdated(now);
            session.save(userFriendster); 
        }
        else
        { 
             
            userFriendster.setPrimaryPhoto(primaryPhoto);
            userFriendster.setFriendsString(getDelimitedString(friends));
            userFriendster.setPhotosString(getDelimitedString(photos));
            userFriendster.setDateUpdated(now);
            session.saveOrUpdate(userFriendster); 
        } 
         
        if (atomicTransaction){

            session.getTransaction().commit();  
            
            if (session.isOpen()){
                session.close();
            }
        }  
        
        return userFriendsterVO;
    }

    private UserFriendsterVO insertUserFriendsterInfo(UserFriendsterVO userFriendsterVO, String primaryPhoto, List friends, List photos, HashMap friendsterFriendMap, List friendsterPhotoList)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            userFriendsterVO = insertUserFriendsterInfo(userFriendsterVO, primaryPhoto, friends, photos, session);
            Date dateUpdated = new Date();

            if (friendsterFriendMap != null && !friendsterFriendMap.isEmpty())
            {
                Set keys = friendsterFriendMap.keySet();
                Iterator iterator = keys.iterator(); 
                List valueList = new ArrayList();
                while (iterator.hasNext())
                {
                    Integer friendIntId = (Integer)iterator.next();
                    System.out.println("Adding key=" +friendIntId);
                    valueList.add(friendsterFriendMap.get(friendIntId));
                }
                for(int i=0;i<valueList.size();i++)
                {
                    FriendsterUser friend = (FriendsterUser)valueList.get(i);

                    System.out.println("checking db with =" + friend.getFriendsterUserId());
                    FriendsterUser dbFriend = (FriendsterUser)session.createCriteria(FriendsterUser.class) 
                    .add( Restrictions.idEq(new Long(friend.getFriendsterUserId())))
                    .uniqueResult(); 

                    if (dbFriend != null)
                    {
                        friend = dbFriend;
                    }

                    friend.setDateUpdated(dateUpdated);
                    // Find existing
                    session.saveOrUpdate(friend);
                }
            }

            if (friendsterPhotoList != null && !friendsterPhotoList.isEmpty())
            {
                for(int i=0;i<friendsterPhotoList.size();i++)
                {
                    FriendsterPhoto photo = (FriendsterPhoto)friendsterPhotoList.get(i);

                    FriendsterPhoto dbPhoto = (FriendsterPhoto)session.createCriteria(FriendsterPhoto.class)
                    .add( Restrictions.naturalId()
                    .set("photoId", photo.getPhotoId()))
                    .uniqueResult();

                    if (dbPhoto != null)
                    {
                        photo = dbPhoto;
                    }


                    photo.setDateUpdated(dateUpdated); 

                    session.saveOrUpdate(photo);
                }
            }

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        //friendsterPhotoList
        return userFriendsterVO;
    }
    
    
    public UserFriendsterVO updateCallbackViewsCounter(UserFriendsterVO userFriendsterVO, String type) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            UserWidgetFriendster userWidgetFriendster = getByFriendsterId(userFriendsterVO.getUser_id(), type); 

            if (userWidgetFriendster != null){ 
                userWidgetFriendster.setViews(userWidgetFriendster.getViews() + 1);
                // Set the user object
                session.saveOrUpdate(userWidgetFriendster); 
            }  

        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userFriendsterVO;
    }

    private String getDelimitedString(List valueList) {
        StringBuffer resultString = new StringBuffer();
        String result = "";
        if (valueList != null && !valueList.isEmpty())
        { 
            System.out.println("valuelist size=" + valueList.size());
            for(int i=0; i<valueList.size();i++)
            {
                String value = (String)valueList.get(i);
                resultString.append(value + ",");  
            }  
            result = resultString.toString();
            if (resultString.length() > 999)
            {
                result = resultString.substring(0,999);
            }  
            result = result.substring(0, result.lastIndexOf(","));
        } 
        System.out.println("final result=" + result);
        return result; 
    }

    public UserFriendsterVO newCallback3Install(UserFriendsterVO userFriendsterVO) throws Exception{
        
        int uid = Integer.parseInt(userFriendsterVO.getUser_id());
        String sessionKey = userFriendsterVO.getSession_key();
    
        XPath xpath = XPathFactory.newInstance().newXPath();
        
        String apiKey = userFriendsterVO.getApi_key();
        String secretKey = PropertyManager.getValue(PropertyConstants.SERVICE_FRIENDSTER_CALLBACK3_SECRET);
        FriendsterClient fc = new FriendsterClient(sessionKey, userFriendsterVO.getApi_key(),secretKey); 
           
        if (fc == null || fc.getToken() == null)
        {
            throw new FriendsterException("FriendsterClient is null, unable to get token"); 
        }
        
        String sToken = fc.getToken(); 
        System.out.println("stoken=" + sToken);


        Document doc = fc.getUser(uid);
        //out.println("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
        //out.println("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
        System.out.println("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
        System.out.println("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
        
        String widgetHTML = "<script src='http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=blog&id=" + userFriendsterVO.getUserName() +"'></script>" +
                            "<br><center><a href=\"" + PropertyManager.getValue(PropertyConstants.SERVICE_FRIENDSTER_CALLBACK3_INSTALL_URL)+ "\">Grab This App</a></center>";
        fc.updateProfile(widgetHTML,1);
            
        // If able to reach here, friendsterUserId is a valid one
        
        // Insert into db
        userFriendsterVO = insertUserWidgetFriendster(userFriendsterVO, FriendsterForm.FEATURED_BLOG_TYPE);
        
        try{
            String primaryPhoto = "";
            List friendsInteger = null;
            List friendsString = null;;
            List photos = new ArrayList();
               
            doc = fc.getPrimaryPhoto(uid);
            primaryPhoto =  xpath.evaluate("/primaryphoto_response/photo/pid", doc);
            System.out.println("primary photo id: " + primaryPhoto);
            String primaryPhotoSrc =  xpath.evaluate("/primaryphoto_response/photo/src_small", doc);
            System.out.println("primary photo src: " + primaryPhotoSrc);
            
             
            friendsInteger = fc.getFriends(uid);
            
            System.out.println("friends: " + friendsInteger); 
            if (friendsInteger != null){
                friendsString = new ArrayList();
                int maxCount = friendsInteger.size() < 8? friendsInteger.size() : 8;
                for (int i = 0;i < maxCount;i++)
                { 
                    System.out.println("uid[" + i + "]: " + friendsInteger.get(i));
                    String friendsId = ""+((Integer)friendsInteger.get(i)).intValue(); 
                    friendsString.add(friendsId); 
                    Document friendsDoc = fc.getPrimaryPhoto(((Integer)friendsInteger.get(i)).intValue()); 
                    String friendsPrimaryPhoto =  xpath.evaluate("/primaryphoto_response/photo/src_small", friendsDoc);
                    System.out.println("friendsPrimaryPhoto: " + friendsPrimaryPhoto);
                }
            }
             
            doc = fc.getPhotos(uid); 
            NodeList nl = (NodeList) xpath.evaluate("/photos_response/photo", doc, XPathConstants.NODESET);
            int maxCount = nl.getLength() < 8? nl.getLength() : 8;
            for (int i = 0;i < maxCount;i++)
            { 
                //System.out.println("url[" + i + "]: " + xpath.evaluate("src", nl.item(i)));
                long pid = Long.parseLong(xpath.evaluate("pid", nl.item(i)));
                System.out.println("pid[" + i + "]: " + pid);
                photos.add(Long.toString(pid));
            } 
          
            insertUserFriendsterInfo(userFriendsterVO, primaryPhoto, friendsString, photos);
        }catch (Exception ex)
        {
            ex.printStackTrace();
            System.out.println("Error getting friendster additional info, ignoring it and continue");
        }
        
        return userFriendsterVO; 
         
    }
     
    
    public UserFriendsterVO newCallback5Install(UserFriendsterVO userFriendsterVO) throws Exception{
        
        int uid = Integer.parseInt(userFriendsterVO.getUser_id());
        String sessionKey = userFriendsterVO.getSession_key();
    
        XPath xpath = XPathFactory.newInstance().newXPath();
        
        String apiKey = userFriendsterVO.getApi_key();
        String secretKey = PropertyManager.getValue(PropertyConstants.SERVICE_FRIENDSTER_CALLBACK5_SECRET);
        FriendsterClient fc = new FriendsterClient(sessionKey, userFriendsterVO.getApi_key(),secretKey); 
           
        if (fc == null || fc.getToken() == null)
        {
            throw new FriendsterException("FriendsterClient is null, unable to get token"); 
        }
        
        String sToken = fc.getToken(); 
        System.out.println("stoken=" + sToken);


        ////////////////////////////////////////
        //  Get User Info
        ////////////////////////////////////////
        Document doc = fc.getUser(uid);
        //out.println("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
        //out.println("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
        String userName = xpath.evaluate("/user_response/user/first_name", doc);
        String userType = xpath.evaluate("/user_response/user/user_type", doc);
        System.out.println("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
        System.out.println("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
        
          
        String primaryPhoto = "";
        List friendsInteger = null;
        List friendsString = null;

        //List friends
        List photos = new ArrayList();
        HashMap friendsMap = new HashMap(); // for Friendster object
        List<Integer> friendsListId = new ArrayList();
        List photoList = new ArrayList(); // for Friendster object

        ////////////////////////////////////////
        //  Get User Primary Photo
        ////////////////////////////////////////
        doc = fc.getPrimaryPhoto(uid);
        primaryPhoto =  xpath.evaluate("/primaryphoto_response/photo/pid", doc);
        System.out.println("primary photo id: " + primaryPhoto);
        String primaryPhotoSrc =  xpath.evaluate("/primaryphoto_response/photo/src_small", doc);
        System.out.println("primary photo src: " + primaryPhotoSrc);
        FriendsterUser friendsterUser = new FriendsterUser();
        friendsterUser.setFriendsterUserId(uid);
        friendsterUser.setPrimaryPhotoUrl(primaryPhotoSrc);
        friendsterUser.setFriendsterUserName(userName);


        ////////////////////////////////////////
        //  Get User's Friends
        ////////////////////////////////////////
         
        if ("Normal".equalsIgnoreCase(userType)){
            logger.debug("Normal user="+uid);
            friendsInteger = fc.getFriends(uid);
        }
        else
        {
            logger.debug("Friendster user profile="+uid);
            friendsInteger = fc.getFans(uid);
        }
 
        // Add ownself into the friends map first
        friendsListId.add(uid);
        FriendsterUser friendsterUserMyself = new FriendsterUser();
        friendsterUserMyself.setFriendsterUserId(uid);
        friendsMap.put(new Integer(uid), friendsterUserMyself);
        
        
        System.out.println("friends: " + friendsInteger); 
        if (friendsInteger != null){
            friendsString = new ArrayList();
            int maxCount = friendsInteger.size() < OEightConstants.APP_MAX_LIST_RESULTS? friendsInteger.size() : OEightConstants.APP_MAX_LIST_RESULTS;
            for (int i = 0;i < maxCount;i++)
            {
                System.out.println("uid[" + i + "]: " + friendsInteger.get(i));
                Integer friendsIntId = (Integer)friendsInteger.get(i);
                String friendsId = ""+friendsIntId.intValue(); 
                
                // FriendsString is used for comma delimited friends, does not include user himself
                friendsString.add(friendsId); 
               
                // Adds FriendsterUser to list 
                FriendsterUser friendsterUserFriend = new FriendsterUser();
                friendsterUserFriend.setFriendsterUserId(friendsIntId);
                
                friendsListId.add(friendsIntId);
                friendsMap.put(friendsIntId, friendsterUserFriend);
                System.out.println("Adding into list " + friendsIntId);
            }
        }

        
        ////////////////////////////////////////
        //  Get User Friends' Info
        ////////////////////////////////////////
        doc = fc.getUsers(friendsListId); 
        NodeList friendsNode = (NodeList) xpath.evaluate("/user_response/user", doc, XPathConstants.NODESET);
         for (int i = 0;i < friendsNode.getLength();i++)
        {  
            long userId = Long.parseLong(xpath.evaluate("uid", friendsNode.item(i))); 
            String friendsName = xpath.evaluate("first_name", friendsNode.item(i)); 
            String friendsProfileUrl = xpath.evaluate("url", friendsNode.item(i)); 
            String friendsPrimaryPhoto = xpath.evaluate("primary_photo_url", friendsNode.item(i));
            System.out.println("Running " + userId);

            // Put username into friends object
            for(int j=0;j<friendsListId.size();j++)
            {
                if (userId == friendsListId.get(j))
                {
                    FriendsterUser friend = (FriendsterUser)friendsMap.get(new Integer(""+userId));
                    friend.setFriendsterUserName(friendsName);
                    friend.setProfileUrl(friendsProfileUrl);
                    friend.setPrimaryPhotoUrl(friendsPrimaryPhoto); 
                    System.out.println("Set profile=" + friendsProfileUrl);
                }
            } 

        }  

        ////////////////////////////////////////
        //  Get User Photos
        ////////////////////////////////////////
        doc = fc.getPhotos(uid); 
        NodeList nl = (NodeList) xpath.evaluate("/photos_response/photo", doc, XPathConstants.NODESET);
        int maxCount = nl.getLength() < OEightConstants.APP_MAX_LIST_RESULTS? nl.getLength() : OEightConstants.APP_MAX_LIST_RESULTS;
        for (int i = 0;i < maxCount;i++)
        { 
            //System.out.println("url[" + i + "]: " + xpath.evaluate("src", nl.item(i)));
            long pid = Long.parseLong(xpath.evaluate("pid", nl.item(i)));
            String photoPath = xpath.evaluate("src_small", nl.item(i));
            String photoLargePath = xpath.evaluate("src_big", nl.item(i));
            String caption = xpath.evaluate("caption", nl.item(i)); 
            long owner = Long.parseLong(xpath.evaluate("owner", nl.item(i)));  
            // If photos is not owned, get another one
            if (owner != uid){
                if (maxCount < nl.getLength()){
                    maxCount++;
                }
                continue;
            }
            photos.add(Long.toString(pid));
            
            FriendsterPhoto friendsterPhoto = new FriendsterPhoto();
            friendsterPhoto.setFriendsterUserId(uid);
            friendsterPhoto.setPhotoId(pid);
            friendsterPhoto.setPhotoUrl(photoPath);
            friendsterPhoto.setPhotoLargeUrl(photoLargePath);
            friendsterPhoto.setPhotoCaption(caption);
            System.out.println("Added photo=" + photoPath);
            photoList.add(friendsterPhoto);
        } 

        insertUserFriendsterInfo(userFriendsterVO, primaryPhoto, friendsString, photos, friendsMap, photoList);




        // Finally step only set widget
        String widgetHTML = "<script src='http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=buddies&id=" + userFriendsterVO.getUserName() +"'></script>" +
                            "<br><center><a href=\"" + PropertyManager.getValue(PropertyConstants.SERVICE_FRIENDSTER_CALLBACK5_INSTALL_URL)+ "\">Grab This App</a></center>";
        fc.updateProfile(widgetHTML,1);

        // If able to reach here, friendsterUserId is a valid one

        // Insert into db
        userFriendsterVO = insertUserWidgetFriendster(userFriendsterVO, FriendsterForm.FRIENDS_XCHANGE_TYPE);


        
        return userFriendsterVO; 
        
    }

    /**********************************************************************
     *
     *  NOTE : 1 Friendster profile can only have 1 OEight Profile, 
     *          but 1 OEight profile can have multiple Friendster profile,
     *         SO userId can return multiple rows of different Friendster
     *
     *********************************************************************/
    public static boolean hasFriendsterXChange(String userId) {
        boolean hasFriendsterXChange = false;
        List valueList;
        Session session = null;
        UserWidgetFriendster userWidgetFriendster = null;
        Transaction transaction = null;  
        
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            //session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWidgetFriendster.class);
            crit.add( Expression.eq( "userId", Long.parseLong(userId)) );
            crit.add( Expression.eq( "widgetType", Integer.parseInt(FriendsterForm.FRIENDS_XCHANGE_TYPE) ));
              
            crit.addOrder( Order.desc("dateInstalled") );
            valueList = crit.list(); 
         
            
            if (valueList != null && !valueList.isEmpty()) { 
               hasFriendsterXChange = true;
            } else {
                logger.info("is empty user widget list!");
            }
            
            //session.getTransaction().commit();
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return hasFriendsterXChange;
    }
    
    /**********************************************************************
     *
     *  NOTE : 1 Friendster profile can only have 1 OEight Profile, 
     *          but 1 OEight profile can have multiple Friendster profile,
     *        
     *  So, using oEight User Id, you can get multiple Friendster Id, 
     *  This method will get the last record of Friendster Id.
     *
     *********************************************************************/
    private static UserFriendster getLastUserFriendsterById(long userId)
    {
        List valueList = null;
        Session session = null; 
        UserFriendster userFriendster = null;  
        Transaction transaction = null;  
        
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            Criteria crit = session.createCriteria(UserFriendster.class);
            crit.add( Expression.eq( "userId", new Long(userId)) ); 
            crit.addOrder( Order.desc("dateUpdated") );
            valueList = crit.list();
            System.out.println("getLastUserFriendsterById with " + userId);
            if (valueList != null && !valueList.isEmpty())
            {
                System.out.println("value list is not null");
                userFriendster = (UserFriendster)valueList.get(0);
                System.out.println("userFriendster=" + userFriendster);
                System.out.println("userFriendster id=" + userFriendster.getFriendsterUserId());
            }
          
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return userFriendster;
    }

    
    
    /**********************************************************************
     *
     *  Get FRIENDSTER_USER from USER_FRENSTER with oEight User Id
     *
     *********************************************************************/
    public static FriendsterUser getFriendsterByUserId(long userId) {
        List valueList = null;
        Session session = null; 
        UserFriendster userFriendster = null; 
        FriendsterUser friendsterUser = null;
        Transaction transaction = null;  
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            userFriendster = getLastUserFriendsterById(userId);
        
            if (userFriendster != null) {  

                // Gets FriendsterUser
                Criteria crit = session.createCriteria(FriendsterUser.class);
                System.out.println("Retrieving friendsterUser="+ userFriendster.getFriendsterUserId());
                crit.add( Expression.idEq(new Long(userFriendster.getFriendsterUserId())) );  
                friendsterUser = (FriendsterUser) crit.uniqueResult();
                System.out.println("Retrieved friendsterUser="+ friendsterUser);
                System.out.println("Retrieved friendsterUserName="+ friendsterUser.getFriendsterUserName());
            }
           transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        
        return friendsterUser;
    }
    
    
    
    /**********************************************************************
     *
     *  1)  Get FRENS_STR (comma delimited friendster id) from USER_FRENSTER
     *  2)  Get from FRIENDSTER_USER as a list
     *
     *********************************************************************/
    public static List getFriendsterFriendsListForWidget(long userId) { 
        List friendsList = null;
        Session session = null; 
        UserFriendster userFriendster = null;
        
        Transaction transaction = null;  
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            userFriendster = getLastUserFriendsterById(userId);
        
            if (userFriendster != null) {  
                System.out.println("getFriendsterFriendsListForWidget userFriendster not null"); 
                String friendsString = userFriendster.getFriendsString();
                StringTokenizer stringTokenizer = new StringTokenizer(friendsString, ",");
                List idList = new ArrayList();
                while (stringTokenizer.hasMoreTokens())
                {
                    Long idLong = new Long(stringTokenizer.nextToken());
                    System.out.println("Friend of " + idLong);
                    idList.add(idLong);
                }
                
                if (idList != null && !idList.isEmpty())
                {
                    Criteria crit = session.createCriteria(FriendsterUser.class);
                    crit.add( Expression.in("friendsterUserId", idList) ); 

                    crit.addOrder( Order.desc("dateUpdated") ); 
                    crit.setMaxResults(OEightConstants.APP_MAX_LIST_RESULTS);
                    friendsList = crit.list();
                }
                // Clear query list for resource 
            } else {
                logger.info("is empty user widget list!");
            }
            
             transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return friendsList;
    }
 
    /**********************************************************************
     *
     *  1)  Get FRIENDSTER_USER
     *  2)  Get from FRIENDSTER_PHOTO as a list
     *
     *********************************************************************/
    public static List getFriendsterPhotosListForWidget(long userId, FriendsterUser friendsterUser) { 
        List photoList = null;
        Session session = null; 
        UserFriendster userFriendster = null;
        Transaction transaction = null;  
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
                
            Criteria crit = session.createCriteria(FriendsterPhoto.class);
            crit.add( Expression.eq("friendsterUserId", new Long(friendsterUser.getFriendsterUserId())) ); 

            crit.setMaxResults(OEightConstants.APP_MAX_LIST_RESULTS);
            photoList = crit.list(); 
             
             transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return photoList;
    }

    // FOR STAGING USE ONLY, NOT FOR PRODUCTION SERVER USAGE!!!!
    public UserFriendsterVO testFriendsterAPI(UserFriendsterVO userFriendsterVO) throws Exception {
        //int mainUID = 48225160; luckyEight
        int mainUID = 25154061; //Karen Kong
        String testSecretKey = "11a6c37a254a6ec0bc01919c789768c1";
        System.out.println("Testing Friendster API with id=" + mainUID);
           
        String sessionKey = userFriendsterVO.getSession_key();
    
        XPath xpath = XPathFactory.newInstance().newXPath();
        
        String apiKey = userFriendsterVO.getApi_key();
        String secretKey = testSecretKey;
        FriendsterClient fc = new FriendsterClient(sessionKey, userFriendsterVO.getApi_key(),secretKey); 
          
        
        ////////////////////////////////////////
        //  Get User Info
        ////////////////////////////////////////
        Document doc = fc.getUser(mainUID);
        //out.println("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
        //out.println("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
        String userName = xpath.evaluate("/user_response/user/first_name", doc);
        String userType = xpath.evaluate("/user_response/user/user_type", doc);
        System.out.println("first_name: " + xpath.evaluate("/user_response/user/first_name", doc));
        System.out.println("last_name: " + xpath.evaluate("/user_response/user/last_name", doc));
        
          
        String primaryPhoto = "";
        List friendsInteger = null;
        List friendsString = null; 


      
        
        // For max 100 users, get Empty name from DB first, if less than 100, then only insert 500 fans (id and name)
        //int max = friendsInteger.size() > 100? 100: friendsInteger.size();
        HashSet frenSet = getFriendsTestSet(""+mainUID);
        
        
        if (frenSet.size() > 99)
        {
            List toGetList = new ArrayList();
            toGetList.addAll(frenSet);
            String DISPLAY_STRING_FORMAT = "dd/MM/yyyy";
            SimpleDateFormat dateFormatter = new SimpleDateFormat(DISPLAY_STRING_FORMAT);


            Session session = HibernateUtil.getSessionFactory().openSession(); 
            int noName = 0;
            Integer friendsIntId = null;
            for (int i=0;i<toGetList.size();i++)
            {
                if (!session.isOpen())
                {
                    session = HibernateUtil.getSessionFactory().openSession(); 
                }
                session.beginTransaction();
                try {
                    System.out.println(">>>>>>>>>> No " + i);
                    // Get user info 
                    friendsIntId = (Integer)toGetList.get(i); 
                    Document docUser = fc.getUser(friendsIntId.intValue()); 
                    String friendName = xpath.evaluate("/user_response/user/first_name", docUser);
                    if (!StringUtils.hasValue(friendName))
                    {
                        noName++;
                        if (noName >4)
                        {
                            System.out.println("Just end it");
                            i = toGetList.size();
                        }
                        continue;
                    }
                    String friendType = xpath.evaluate("/user_response/user/user_type", docUser);
                    String bdayYear = xpath.evaluate("/user_response/user/birthday/year", docUser);
                    String bdayMonth = xpath.evaluate("/user_response/user/birthday/month", docUser);
                    String bdayDay = xpath.evaluate("/user_response/user/birthday/day", docUser);
                    Date bdayObject = null;
                    try {
                        bdayObject = dateFormatter.parse(bdayDay+"/"+bdayMonth+"/"+bdayYear); 
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                        System.out.println("Error parsing " + bdayDay+"/"+bdayMonth+"/"+bdayYear);
                    } 

                    String aboutMe = xpath.evaluate("/user_response/user/about_me", docUser);
                    String wantToMeet = xpath.evaluate("/user_response/user/want_to_meet", docUser);

                    System.out.println("user_id:" + friendsIntId.intValue());
                    System.out.println("first_name: " + friendName);
                    System.out.println("user_type: " + friendType);
                    System.out.println("bday:" + bdayObject);
                    System.out.println("about_me:" + aboutMe );
                    System.out.println("want_to_meet:" + wantToMeet);
                    HashSet emailAboutMeSet = EmailAddressHelper.getEmailAddress(aboutMe);
                    HashSet emailWantToMeetSet = EmailAddressHelper.getEmailAddress(wantToMeet); 

                    docUser = fc.getShoutout(friendsIntId.intValue());
                    String shoutout = xpath.evaluate("/shoutout_response/shoutouts/shoutout", docUser);
                    HashSet emailShoutOut = EmailAddressHelper.getEmailAddress(shoutout); 

                    String allEmails = "";
                    // This will get unique addresses
                    HashSet allEmailsSet = new HashSet();
                    allEmailsSet.addAll(emailAboutMeSet);
                    allEmailsSet.addAll(emailWantToMeetSet);
                    allEmailsSet.addAll(emailShoutOut);
                    allEmails = EmailAddressHelper.getEmailAddressString(allEmailsSet); 

                    System.out.println("allEmails?=" + allEmails);

                    FriendsterAPITest friendsterTest = new FriendsterAPITest();
                    
                    Criteria crit = session.createCriteria(FriendsterAPITest.class);  
                    crit.add( Expression.eq("userId", new Long("" + friendsIntId)));
                    crit.add( Expression.eq("referralUserId", new Long(mainUID))); 
                    friendsterTest = (FriendsterAPITest)crit.uniqueResult(); 
                    if (friendsterTest != null)
                    {
                        friendsterTest.setUserId(Long.parseLong(""+friendsIntId.intValue()));
                        friendsterTest.setName(friendName);
                        friendsterTest.setBirthday(bdayObject);
                        friendsterTest.setUserType(friendType);
                        friendsterTest.setEmail(allEmails);
                        friendsterTest.setReferralUserId(mainUID);
                        friendsterTest.setReferralName(userName); 

                        session.update(friendsterTest);
                    } 
                    session.getTransaction().commit();

                } catch (Exception ex) {
                          
                    FriendsterAPITest friendsterTest = null;
                    
                    Criteria crit = session.createCriteria(FriendsterAPITest.class);  
                    crit.add( Expression.eq("userId", new Long("" + friendsIntId)));
                    crit.add( Expression.eq("referralUserId", new Long(mainUID))); 
                    friendsterTest = (FriendsterAPITest)crit.uniqueResult(); 
                
                    if (friendsterTest != null)
                    {
                        friendsterTest.setName("Error");
                        session.save(friendsterTest);
                    }
                    session.getTransaction().commit();
                    ex.printStackTrace(); 
                    if (session.isOpen()){
                        session.close();
                    }
                } 

            }  

            if (session.isOpen()){
                session.close();
            }
            
        }
        else
        {
            ////////////////////////////////////////
            //  Get User's Friends
            ////////////////////////////////////////

            if ("Normal".equalsIgnoreCase(userType)){
                logger.debug("Normal user="+mainUID);
                friendsInteger = fc.getFriends(mainUID);
            }
            else
            {
                logger.debug("Friendster user profile="+mainUID);
                friendsInteger = fc.getFans(mainUID);
            }
            
            // Insert those not in db already 
            Session session = HibernateUtil.getSessionFactory().openSession(); 
            int noName = 0;
            for (int i=0;i<friendsInteger.size();i++)
            {
                if (!session.isOpen())
                {
                    session = HibernateUtil.getSessionFactory().openSession(); 
                }
                session.beginTransaction();
                
                  
                Integer friendsIntId = (Integer)friendsInteger.get(i);
                
                FriendsterAPITest friendsterTest = null;
                    
                Criteria crit = session.createCriteria(FriendsterAPITest.class);  
                crit.add( Expression.eq("userId", new Long("" + friendsIntId)));
                crit.add( Expression.eq("referralUserId", new Long(mainUID))); 
                friendsterTest = (FriendsterAPITest)crit.uniqueResult();
                
                
                if (friendsterTest == null)
                {
                    friendsterTest = new FriendsterAPITest();
                    friendsterTest.setUserId(Long.parseLong(""+friendsIntId.intValue())); 
                    friendsterTest.setReferralUserId(mainUID);
                    friendsterTest.setReferralName(userName); 

                    session.save(friendsterTest);
                } 
                session.getTransaction().commit(); 
                
                if (session.isOpen()){
                    session.close();
                }
            
            }
        
        }
        
        /*
        List toGetList = new ArrayList();
        Iterator frenIterator = friendsInteger.iterator();
        int count =0;
        while (frenIterator.hasNext() && count < 90) //Friendster only allows 100 call including ownself
        {
            Integer friendsIntId = (Integer)frenIterator.next(); 
            if (!frenSet.contains(new Long(friendsIntId.toString())))
            {
                toGetList.add(friendsIntId);
                count++;
            }
        }
        
        String DISPLAY_STRING_FORMAT = "dd/MM/yyyy";
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DISPLAY_STRING_FORMAT);
        
        
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        int noName = 0;
        for (int i=0;i<toGetList.size();i++)
        {
            if (!session.isOpen())
            {
                session = HibernateUtil.getSessionFactory().openSession(); 
            }
            session.beginTransaction();
            try {
                System.out.println(">>>>>>>>>> No " + i);
                // Get user info 
                Integer friendsIntId = (Integer)toGetList.get(i); 
                Document docUser = fc.getUser(friendsIntId.intValue()); 
                String friendName = xpath.evaluate("/user_response/user/first_name", docUser);
                if (!StringUtils.hasValue(friendName))
                {
                    noName++;
                    if (noName >4)
                    {
                        System.out.println("Just end it");
                        i = toGetList.size();
                    }
                    continue;
                }
                String friendType = xpath.evaluate("/user_response/user/user_type", docUser);
                String bdayYear = xpath.evaluate("/user_response/user/birthday/year", docUser);
                String bdayMonth = xpath.evaluate("/user_response/user/birthday/month", docUser);
                String bdayDay = xpath.evaluate("/user_response/user/birthday/day", docUser);
                Date bdayObject = null;
                try {
                    bdayObject = dateFormatter.parse(bdayDay+"/"+bdayMonth+"/"+bdayYear); 
                } catch (ParseException ex) {
                    ex.printStackTrace();
                    System.out.println("Error parsing " + bdayDay+"/"+bdayMonth+"/"+bdayYear);
                } 

                String aboutMe = xpath.evaluate("/user_response/user/about_me", docUser);
                String wantToMeet = xpath.evaluate("/user_response/user/want_to_meet", docUser);
                
                System.out.println("user_id:" + friendsIntId.intValue());
                System.out.println("first_name: " + friendName);
                System.out.println("user_type: " + friendType);
                System.out.println("bday:" + bdayObject);
                System.out.println("about_me:" + aboutMe );
                System.out.println("want_to_meet:" + wantToMeet);
                HashSet emailAboutMeSet = EmailAddressHelper.getEmailAddress(aboutMe);
                HashSet emailWantToMeetSet = EmailAddressHelper.getEmailAddress(wantToMeet); 
                
                docUser = fc.getShoutout(friendsIntId.intValue());
                String shoutout = xpath.evaluate("/shoutout_response/shoutouts/shoutout", docUser);
                HashSet emailShoutOut = EmailAddressHelper.getEmailAddress(shoutout); 
                
                String allEmails = "";
                // This will get unique addresses
                HashSet allEmailsSet = new HashSet();
                allEmailsSet.addAll(emailAboutMeSet);
                allEmailsSet.addAll(emailWantToMeetSet);
                allEmailsSet.addAll(emailShoutOut);
                allEmails = EmailAddressHelper.getEmailAddressString(allEmailsSet); 
                
                System.out.println("allEmails?=" + allEmails);
                
                FriendsterAPITest friendsterTest = new FriendsterAPITest();
                if (friendsIntId != null)
                {
                    friendsterTest.setUserId(Long.parseLong(""+friendsIntId.intValue()));
                    friendsterTest.setName(friendName);
                    friendsterTest.setBirthday(bdayObject);
                    friendsterTest.setUserType(friendType);
                    friendsterTest.setEmail(allEmails);
                    friendsterTest.setReferralUserId(mainUID);
                    friendsterTest.setReferralName(userName); 

                    session.save(friendsterTest);
                } 
                session.getTransaction().commit();
            
            } catch (Exception ex) {
                ex.printStackTrace(); 
                if (session.isOpen()){
                    session.close();
                }
            } 
        
        }  
         
        if (session.isOpen()){
            session.close();
        }*/
 
        
        return null;
    }

    public HashSet getFriendsTestSet(String mainId) { 
        List frenList = null;
        Session session = null;  
        HashSet frenSet = new HashSet();
        Transaction transaction = null;
        try{ 
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
                
            Criteria crit = session.createCriteria(FriendsterAPITest.class);
            //crit.add( Expression.ne("name", "Error")); 
            crit.add( Expression.isNull("name")); 
            crit.add( Expression.eq("referralUserId", new Long(mainId))); 
            crit.setMaxResults(110);
            frenList = crit.list(); 
            
            if (frenList != null  && !frenList.isEmpty())
            {
                frenSet = new HashSet();
                for(int i=0;i<frenList.size();i++)
                {
                    FriendsterAPITest fren = (FriendsterAPITest)frenList.get(i);
                    frenSet.add(new Integer("" + fren.getUserId()));
                }
            } 
         
             transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return frenSet;
    }
    
    public void printBirthdays() { 
        List bdayList = null;
        Session session = null;  
       Transaction transaction = null;
        try{ 
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
                
            Criteria crit = session.createCriteria(FriendsterAPITest.class); 
            crit.add( Expression.isNotNull( "birthday") );
            crit.addOrder(Order.desc("birthday"));

            crit.setMaxResults(20000);
            bdayList = crit.list(); 
          
             transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        
        if (bdayList != null)
        {
            // Print in table
            System.out.println("<table>");
            System.out.println("<tr><td><b>No</b></td><td><b>Birthday</b><td><td><b>Name</b><td><td><b>Profile</b><td></tr>");
            for(int i=0;i<bdayList.size();i++)
            {
                System.out.println("<tr>");
                FriendsterAPITest friend = (FriendsterAPITest)bdayList.get(i);
                System.out.println("<td>" + i + "</td>");
                System.out.println("<td>"+ DateObjectHelper.getPrintedDate(friend.getBirthday()) +"</td>");
                System.out.println("<td>"+friend.getName()+"</td>");
                System.out.println("<td><a href=\"http://profiles.friendster.com/"+friend.getUserId()+"\" target=\"_blank\">View</a></td>"); 
            
                System.out.println("</tr>");
            } 
            System.out.println("</table>");
        
        }
        
    }

    public UserFriendsterVO newCallback6Install(UserFriendsterVO userFriendsterVO, int pledgeCode) throws Exception{ 
            
        int uid = Integer.parseInt(userFriendsterVO.getUser_id());
        String sessionKey = userFriendsterVO.getSession_key();
    
        XPath xpath = XPathFactory.newInstance().newXPath();
        
        String apiKey = userFriendsterVO.getApi_key();
        String secretKey = PropertyManager.getValue(PropertyConstants.SERVICE_FRIENDSTER_CALLBACK6_SECRET);
        FriendsterClient fc = new FriendsterClient(sessionKey, userFriendsterVO.getApi_key(),secretKey); 
        
        if (fc == null || fc.getToken() == null)
        {
            throw new FriendsterException("FriendsterClient is null, unable to get token"); 
        }
        
        String sToken = fc.getToken(); 
        //System.out.println("stoken=" + sToken);


        Document doc = fc.getUser(uid);  
        String firstName = xpath.evaluate("/user_response/user/first_name", doc);
        String lastName = xpath.evaluate("/user_response/user/last_name", doc);
        
        // Append the oEightId to "next" param for tracking, if oEightId exists, meaning joined contest 
        String nextParam = "";
        if (StringUtils.hasValue(userFriendsterVO.getUserId()))
        {
            nextParam = "&next=" + userFriendsterVO.getUserName();
        }
        String widgetHTML = "<script src='http://"+ConfigurationHelper.getDomainName()+"/scripts.do?act=friendsterPledge&id=" + uid +"'></script>" +
                            "<br><center><a href=\"" + PropertyManager.getValue(PropertyConstants.SERVICE_FRIENDSTER_CALLBACK6_INSTALL_URL)+ nextParam +"\">Get My Own Pledge</a></center>" +
                            "<br><center><a href=\"http://"+ConfigurationHelper.getDomainName()+"/earth.do?act=pledgeRoom\" target=\"_blank\">Pledge Room</a></center>";
        fc.updateProfile(widgetHTML,1);
            
        // If able to reach here, friendsterUserId is a valid one
        OEightPledge oeightPledge = new OEightPledge();
        oeightPledge.setPledgerName(firstName + " " + lastName);
        oeightPledge.setPledgeCode(pledgeCode); 
        oeightPledge.setMemberFlag(false);
        oeightPledge.setPledgeFrom(OEightConstants.CAMPAIGN_INVITE_FROM_FRIENDSTER);
        oeightPledge.setPledgeFromUserId(uid);
        if (StringUtils.hasValue(userFriendsterVO.getUserId()))
        {
            oeightPledge.setUserName(userFriendsterVO.getUserName());
        }
        oeightPledge.setShowPledgeFrom(true);
        
        EarthBO earthBO = new EarthBO();
        earthBO.savePledge(oeightPledge);
        earthBO = null;
        
        CampaignInviteBO campaignBO = new CampaignInviteBO();
        // If has a user login, we insert into CampaignInvites for the tracking
        if (StringUtils.hasValue(userFriendsterVO.getUserId()))
        {
            campaignBO.updateOwnerCampaignTracker(userFriendsterVO.getUserId(), OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE);
           
        }
        if (StringUtils.hasValue(userFriendsterVO.getNext()))
        {
            System.out.println("Updating referral increase accepted:" + userFriendsterVO.getNext());
            campaignBO.updateAccepted(userFriendsterVO.getNext(),null,OEightConstants.CAMPAIGN_INVITE_OEIGHT_PLEDGE, OEightConstants.CAMPAIGN_INVITE_FROM_FRIENDSTER);
        }
        campaignBO = null;
        
        // Insert into db 
        // What is the impact if not inserted into UserWidgetFriendster?
        // We need to skip at the moment as userId could be null for pledge
        //  and userId has a foreignKey constraint
        //userFriendsterVO = insertUserWidgetFriendster(userFriendsterVO, FriendsterForm.CLIMATE_CHANGE_TYPE);
        
        return userFriendsterVO; 
    }

    public boolean hasPledge(String friendsterUserId) {
        boolean hasPledge = false;
        EarthBO earthBO = new EarthBO();
        OEightPledge pledge = earthBO.getLatestPledgeFromThirdParty( 
                OEightConstants.CAMPAIGN_INVITE_FROM_FRIENDSTER, friendsterUserId);
        if (pledge != null)
        {
            hasPledge = true;
        } 
        earthBO = null;
        return hasPledge;
    }

    
    
    
    
}
