/*
 * UserMessageBO.java
 *
 * Created on March 18, 2008, 5:20 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.UserMessage;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.MessageVO;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class UserMessageBO {
    
    private Logger logger = Logger.getLogger(UserMessageBO.class);
    
    /**
     * Creates a new instance of UserMessageBO
     */
    public UserMessageBO() {
    }

    public List getUserMessageList(String userId, boolean hasNew) { 
        List messageList = null;
        List valueList = null;
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserMessage.class);
            crit.add( Expression.eq( "recipientUserId", Long.parseLong(userId)) ); 
            crit.add( Expression.ne( "isDeleted", true ));
            
            // Other criteria
            //crit.setFetchMode("sender", FetchMode.JOIN); 
            
            crit.addOrder( Order.desc("dateSent") );
            crit.setMaxResults(PropertyManager.getInt(PropertyConstants.MESSAGE_LIST_MAX,30));
            messageList = crit.list(); 
     
            
            
            if (messageList != null && !messageList.isEmpty()) {
                valueList = new ArrayList(); 
                for(int i=0;i<messageList.size();i++) {
                    UserMessage message = (UserMessage)messageList.get(i); 
               
                    MessageVO messageVO = new MessageVO();
                    messageVO.setUserMessageId(message.getUserMessageId());
                    messageVO.setMessageIdString("" + message.getUserMessageId());
                    messageVO.setSenderUserId(message.getSenderUserId());
                    messageVO.setRecipientUserId(message.getRecipientUserId());
                    messageVO.setSubject(message.getSubject());
                    messageVO.setMessage(message.getMessage());
                    messageVO.setDateSent(message.getDateSent());
                    messageVO.setDateRead(message.getDateRead());
                    messageVO.setIsRead(message.getIsRead());
                    messageVO.setIsReplied(message.getIsReplied());
                    messageVO.setIsSaved(message.getIsSaved());
                    messageVO.setIsDeleted(message.getIsDeleted());
                    messageVO.setReferredSourceMessageId(message.getReferredSourceMessageId());
                    messageVO.setSenderUserName(message.getSender().getUserName());
                    messageVO.setPhotoSmallPath(message.getSender().getUserCountry().getPhotoSmallPath());
                    valueList.add(messageVO);
                     
                    
                }
                
                // Clear query list for resource
                messageList = null;
                
                if (hasNew)
                {
                    session.beginTransaction();
                    UserBO userBO = new UserBO();
                    User user = userBO.getUserById(userId); 
                    UserCountry userCountry = user.getUserCountry();
                    userCountry.setHasMessage(false);
                    session.update(userCountry);
                    session.getTransaction().commit();
                }
                
            } else {
                logger.info("is empty message list!");
            }
            
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return valueList;
        
    }
    
    public List getUserSavedMessageList(String userId) {
        List messageList = null;
        List valueList = null;
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserMessage.class);
            crit.add( Expression.eq( "senderUserId", Long.parseLong(userId)) ); 
            crit.add( Expression.eq( "isSaved", true ));
            crit.add( Expression.ne( "isSavedDeleted", true ));
            
            // Other criteria
            //crit.setFetchMode("sender", FetchMode.JOIN); 
            
            crit.addOrder( Order.desc("dateSent") );
            crit.setMaxResults(PropertyManager.getInt(PropertyConstants.MESSAGE_LIST_MAX,200));
            messageList = crit.list(); 
     
            
            
            if (messageList != null && !messageList.isEmpty()) {
                valueList = new ArrayList(); 
                for(int i=0;i<messageList.size();i++) {
                    UserMessage message = (UserMessage)messageList.get(i); 
               
                    MessageVO messageVO = new MessageVO();
                    messageVO.setUserMessageId(message.getUserMessageId());
                    messageVO.setMessageIdString("" + message.getUserMessageId());
                    messageVO.setSenderUserId(message.getSenderUserId());
                    messageVO.setRecipientUserId(message.getRecipientUserId());
                    messageVO.setSubject(message.getSubject());
                    messageVO.setMessage(message.getMessage());
                    messageVO.setDateSent(message.getDateSent());
                    messageVO.setDateRead(message.getDateRead());
                    messageVO.setIsRead(message.getIsRead());
                    messageVO.setIsReplied(message.getIsReplied());
                    messageVO.setIsSaved(message.getIsSaved());
                    messageVO.setIsDeleted(message.getIsDeleted());
                    messageVO.setReferredSourceMessageId(message.getReferredSourceMessageId()); 
                    messageVO.setRecipientUserName(message.getRecipient().getUserName());
                    messageVO.setPhotoSmallPath(message.getRecipient().getUserCountry().getPhotoSmallPath());
                    valueList.add(messageVO);
                     
                    
                }
                
                // Clear query list for resource
                messageList = null; 
                
            } else {
                logger.info("is empty message list!");
            }
            
            
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return valueList;
        
    }
     
    public List getUserSavedMessageDelete(String userId, String[] messageIds) {
           
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            for(int i=0; i<messageIds.length;i++){
                String messageId = messageIds[i];
                Criteria crit = session.createCriteria(UserMessage.class)
                .add( Restrictions.idEq(new Long(messageId)))
                .add( Expression.eq( "senderUserId", new Long(userId)));
                UserMessage userMessage = (UserMessage)crit.uniqueResult();

                userMessage.setIsSavedDeleted(true); 
                session.update(userMessage);
            }

          
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        
        return getUserSavedMessageList(userId);
    }
    
    public MessageVO getUserSavedMessage(String messageId, String userId) {
      
        MessageVO messageVO = null; 
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
               
            UserMessage message = (UserMessage)session.createCriteria(UserMessage.class) 
            .add( Restrictions.idEq(new Long(messageId)))
            .add( Expression.eq( "senderUserId", new Long(userId)))
            .uniqueResult();
            
            if (message != null) {   
                    messageVO = new MessageVO();
                    messageVO.setUserMessageId(message.getUserMessageId());
                    messageVO.setMessageIdString("" + message.getUserMessageId());
                    messageVO.setSenderUserId(message.getSenderUserId());
                    messageVO.setRecipientUserId(message.getRecipientUserId());
                    messageVO.setSubject(message.getSubject());
                    messageVO.setMessage(message.getMessage());
                    messageVO.setDateSent(message.getDateSent());
                    messageVO.setDateRead(message.getDateRead());
                    messageVO.setIsRead(message.getIsRead());
                    messageVO.setIsReplied(message.getIsReplied());
                    messageVO.setIsSaved(message.getIsSaved());
                    messageVO.setIsDeleted(message.getIsDeleted());
                    messageVO.setReferredSourceMessageId(message.getReferredSourceMessageId());
                    messageVO.setRecipientUserName(message.getRecipient().getUserName());
                    messageVO.setPhotoSmallPath(message.getRecipient().getUserCountry().getPhotoSmallPath()); 
                     
                
            } else {
                logger.info("message not found");
            }
             
           
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return messageVO;
    }


    public MessageVO getUserMessage(String messageId, String userId) { 
       return getUserMessage(messageId, userId, false);         
    }
    
    public MessageVO getUserMessage(String messageId, String userId, boolean getReferredMessage) {
      
        MessageVO messageVO = null; 
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
               
            UserMessage message = (UserMessage)session.createCriteria(UserMessage.class) 
            .add( Restrictions.idEq(new Long(messageId)))
            .add( Expression.eq( "recipientUserId", new Long(userId)))
            .uniqueResult();
            
            if (message != null) {   
                    messageVO = new MessageVO();
                    messageVO.setUserMessageId(message.getUserMessageId());
                    messageVO.setMessageIdString("" + message.getUserMessageId());
                    messageVO.setSenderUserId(message.getSenderUserId());
                    messageVO.setRecipientUserId(message.getRecipientUserId());
                    messageVO.setSubject(message.getSubject());
                    messageVO.setMessage(message.getMessage());
                    messageVO.setDateSent(message.getDateSent());
                    messageVO.setDateRead(message.getDateRead());
                    messageVO.setIsRead(message.getIsRead());
                    messageVO.setIsReplied(message.getIsReplied());
                    messageVO.setIsSaved(message.getIsSaved());
                    messageVO.setIsDeleted(message.getIsDeleted());
                    messageVO.setReferredSourceMessageId(message.getReferredSourceMessageId());
                    messageVO.setSenderUserName(message.getSender().getUserName());
                    messageVO.setPhotoSmallPath(message.getSender().getUserCountry().getPhotoSmallPath()); 
                    
                    if (getReferredMessage && message.getReferredSourceMessageId() > 0)
                    { 
                        UserMessage referredMessage = (UserMessage)session.createCriteria(UserMessage.class) 
                        .add( Restrictions.idEq(new Long(message.getReferredSourceMessageId())))
                        .add( Expression.eq( "senderUserId", new Long(userId)))
                        .uniqueResult();
                        
                        MessageVO referredMessageVO = null;
                        if (referredMessage != null)
                        {
                            referredMessageVO = new MessageVO();
                            referredMessageVO.setSubject(referredMessage.getSubject());
                            referredMessageVO.setMessage(referredMessage.getMessage());
                            referredMessageVO.setDateSent(referredMessage.getDateSent()); 
                        }
                        
                        messageVO.setReferredMessageVO(referredMessageVO);
                    }
                    
                    if (!message.getIsRead()){
                        session.beginTransaction();
                        message.setIsRead(true); // set as read 
                        session.update(message);
                        session.getTransaction().commit();
                    }
                
            } else {
                logger.info("message not found");
            }
             
          
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        return messageVO;
    }
    
    
    public List getUserMessageDelete(String userId, String[] messageIds) { 
        
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            for(int i=0; i<messageIds.length;i++){
                String messageId = messageIds[i];
                Criteria crit = session.createCriteria(UserMessage.class)
                .add( Restrictions.idEq(new Long(messageId)))
                .add( Expression.eq( "recipientUserId", new Long(userId)));
                UserMessage userMessage = (UserMessage)crit.uniqueResult();

                userMessage.setIsDeleted(true); 
                session.update(userMessage);
            }

         
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        
        return getUserMessageList(userId, false);
    }

    public MessageVO saveMessage(MessageVO messageVO) { 
        return saveMessage(messageVO, 0);
    }
    
    public MessageVO saveMessage(MessageVO messageVO, long referredMessageId) { 
        
        UserMessage userMessage = null;
        Session session = null;
        Transaction transaction = null; 
        try {
        
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            UserBO userBO = new UserBO();
            User recipient = userBO.getUserByUserName(messageVO.getRecipientUserName());
            messageVO.setPhotoSmallPath(recipient.getUserCountry().getPhotoSmallPath()); 

            userMessage = new UserMessage();
            userMessage.setSenderUserId(messageVO.getSenderUserId()); 
            userMessage.setRecipientUserId(recipient.getUserId());
            userMessage.setSubject(messageVO.getSubject());
            userMessage.setMessage(messageVO.getMessage());
            userMessage.setDateSent(messageVO.getDateSent());
            userMessage.setIsDeleted(false);
            userMessage.setIsRead(false);
            userMessage.setIsReplied(false);
            userMessage.setIsSaved(messageVO.getIsSaved());
            userMessage.setReferredSourceMessageId(referredMessageId);  
            userMessage.setRecipient(recipient);

            UserCountry userCountry = recipient.getUserCountry();
            userCountry.setHasMessage(true);
            session.update(userCountry);

            session.save(userMessage);  
          
            transaction.commit();
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        }
        finally{ 
            if (session.isOpen()){
                session.close();
            }
        }
        
        // Fail save, even if fail to insert, we make sure this transaction is ok 
        // Trigger email update
        EmailUpdateBO.saveEmailUpdate(OEightConstants.NOTIFY_UPDATE_S02_RECEIVED_PM, userMessage); 
       
        return messageVO;
    }

    public static void main(String[] args) {
        
        MessageVO messageVO = new UserMessageBO().getUserMessage("4","16");
        System.out.println("MessageVO = " + messageVO);
        System.out.println("MessageVO Message = " + messageVO.getMessage());
        /*List list = new UserMessageBO().getUserMessageList("16");
        if (list == null || list.isEmpty())
        {
            System.out.println("list is empty");
        }
        else
        {
            System.out.println("something is in the list");
        }*/
    }


}
