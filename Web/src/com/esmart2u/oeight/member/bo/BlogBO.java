package com.esmart2u.oeight.member.bo;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

import com.esmart2u.oeight.data.UserBlog;
import com.esmart2u.oeight.data.UserPost;
import com.esmart2u.oeight.data.UserPostVotes;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;

public class BlogBO {
    
    /** Creates a new instance of BlogBO */
    public BlogBO() {
    }
        
    private static Logger logger = Logger.getLogger(BlogBO.class);
            
    public UserBlog saveBlog(UserBlog userBlog) throws Exception{  
        return insertUserBlogIntoDB(userBlog);  
    }
    
    private UserBlog insertUserBlogIntoDB(UserBlog userBlog) throws Exception{ 
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        	//Get userBlog first
        	UserBlog dbUserBlog = getBlogWithUrl(userBlog.getBlogUrl());
            transaction = session.beginTransaction();
            long userBlogId = 0;
        	if (dbUserBlog == null){
        		
	            session.save(userBlog); 
	            logger.debug("UserBlog Created, id:" + userBlog.getUserBlogId());
	            
        	}
        	else
        	{
        		if (dbUserBlog.getUserBlogId() != userBlog.getUserBlogId())
        		{
        			// Someone else owns this blog url
        			throw new Exception("Existing Blog Url Found");
        		}
        		else
        		{
        			// This is an update
        			session.save(userBlog);
    	            logger.debug("UserBlog Updated, id:" + userBlog.getUserBlogId()); 
        		}
        		
        	}
        	
        	userBlogId = userBlog.getUserBlogId();
        	// Get the list of posts from the blog (if available and do insert)
        	ArrayList postsList = userBlog.getPostsList();
        	
        	if (postsList != null && !postsList.isEmpty())
        	{
        		PostBO postBO = new PostBO();
        		for (int i=0; i<postsList.size(); i++)
        		{
        			UserPost userPost = (UserPost)postsList.get(i);
        			UserPost dupUserPost = postBO.getPostWithUrl(userPost.getPostUrl());
        			if (dupUserPost !=null)
        			{
        				// Do nothing, it's a duplicate so skip it
        			}
        			else
        			{ 	
        				// No duplicates, proceed to insert
        				userPost.setUserBlogId(userBlogId);
        				userPost.setPostedByUserId(userBlog.getUserId());
	        			userPost.setPostedDate(new Date());
	                    userPost.setVotedUp(1);  
	                    userPost.setViewedCount(1); 
	        			session.save(userPost);
	        			
		                // Save userPosts if available and add votes
		                UserPostVotes userPostVotes = new UserPostVotes();
		                userPostVotes.setUserId(userBlog.getUserId());
		                userPostVotes.setDateVoted(new Date());
		                userPostVotes.setVotedUp(1); 
		                session.save(userPostVotes);
        			}
        		}
        		postBO = null;
        	}
        	
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
        return userBlog;
    }

    public UserBlog getBlogWithUrl(String urlString) {
 
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserBlog userBlog = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction(); 
            userBlog = (UserBlog)session.createCriteria(UserBlog.class)
            .add(Expression.eq( "blogUrl", urlString )) 
            .uniqueResult(); 
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userBlog;
    }

	public UserBlog getBlogDetailsByUserId(String userId) { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserBlog userBlog = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction(); 
            userBlog = (UserBlog)session.createCriteria(UserBlog.class)
            .add(Expression.eq( "userId", userId )) 
            .uniqueResult(); 
            
            if (userBlog != null)
            {
            	ArrayList postsList = (ArrayList)session.createCriteria(UserPost.class)
                .add(Expression.eq( "userBlogId", userBlog.getUserBlogId())) 
                .list(); 
            	
            	userBlog.setPostsList(postsList);
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
		return userBlog;
	}

	public UserBlog getFeedWithUrl(String blogFeed) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        UserBlog userBlog = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction(); 
            userBlog = (UserBlog)session.createCriteria(UserBlog.class)
            .add(Expression.eq( "blogFeed", blogFeed )) 
            .uniqueResult(); 
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userBlog;
	}
}
