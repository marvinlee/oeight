/*
 * RankingBO.java
 *
 * Created on October 17, 2007, 4:27 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.RankingSnapshot;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class RankingBO {
    
    private static Logger logger = Logger.getLogger(RankingBO.class);
    
    /** Creates a new instance of RankingBO */
    public RankingBO() {
    }
    
    // Increase page view counter
    public static long increasePageView(String userId)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        long currentCount = 0;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
        // Get Ranking snapshot 
        RankingSnapshot rankingSnapshot = (RankingSnapshot) session.createCriteria(RankingSnapshot.class)
        .add(Expression.eq( "userId", Long.parseLong(userId)))
        .add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()))
        .uniqueResult(); 
        
        boolean newRecord = false;
        if (rankingSnapshot == null)
        {     
            rankingSnapshot = new RankingSnapshot();
            rankingSnapshot.setUserId(Long.parseLong(userId));
            rankingSnapshot.setSnapshotMonth(DateObjectHelper.getCurrentMonthDateObject());
            newRecord = true;
        }  
        currentCount = rankingSnapshot.getPageView() + 1;
        rankingSnapshot.setPageView(currentCount);
        rankingSnapshot.setLastUpdate(new Date());
        // Save 
        if (newRecord)
        { 
            session.save(rankingSnapshot);
        }else
        {  
            session.saveOrUpdate(rankingSnapshot);
        }  
        transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return currentCount;  
    
    }
    
    // Increase invites sent
    
    public static long increaseInvitesSent(String userId, Session session, int invites)
    { 
        boolean subSession = true; 
        if (invites <1) invites = 1;
        if (session == null || !session.isOpen())
        {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            subSession = false;
        }
        
        // Get Ranking snapshot 
        RankingSnapshot rankingSnapshot = (RankingSnapshot) session.createCriteria(RankingSnapshot.class)
        .add(Expression.eq( "userId", Long.parseLong(userId)))
        .add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()))
        .uniqueResult(); 
        
        boolean newRecord = false;
        if (rankingSnapshot == null)
        {     
            rankingSnapshot = new RankingSnapshot();
            rankingSnapshot.setUserId(Long.parseLong(userId));
            rankingSnapshot.setSnapshotMonth(DateObjectHelper.getCurrentMonthDateObject());
            newRecord = true;
        }  
        long currentCount = rankingSnapshot.getInvitesSent() + invites;
        rankingSnapshot.setInvitesSent(currentCount);
        rankingSnapshot.setLastUpdate(new Date());
        // Save 
        if (newRecord)
        { 
            session.save(rankingSnapshot);
        }else
        {  
            session.saveOrUpdate(rankingSnapshot);
        }  
         
        if (!subSession){
            session.getTransaction().commit();
            if (session.isOpen()){
                session.close();
            }
        }
        
        return currentCount;  
    
    }
    
    // Increase invites accepted 
    public static long increaseInvitesAccepted(String userId)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        // Get Ranking snapshot 
        RankingSnapshot rankingSnapshot = (RankingSnapshot) session.createCriteria(RankingSnapshot.class)
        .add(Expression.eq( "userId", Long.parseLong(userId)))
        .add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()))
        .uniqueResult(); 
        
        boolean newRecord = false;
        if (rankingSnapshot == null)
        {     
            rankingSnapshot = new RankingSnapshot();
            rankingSnapshot.setUserId(Long.parseLong(userId));
            rankingSnapshot.setSnapshotMonth(DateObjectHelper.getCurrentMonthDateObject());
            newRecord = true;
        }  
        long currentCount = rankingSnapshot.getInvitesAccepted() + 1;
        rankingSnapshot.setInvitesAccepted(currentCount);
        rankingSnapshot.setLastUpdate(new Date());
        // Save 
        if (newRecord)
        { 
            session.save(rankingSnapshot);
        }else
        {  
            session.saveOrUpdate(rankingSnapshot);
        }  
        
        session.getTransaction().commit();
        if (session.isOpen()){
            session.close();
        }
        
        return currentCount;  
    
    }
    
    
    // Increase votes 
    public static long increaseVotes(String userId)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        // Get Ranking snapshot 
        RankingSnapshot rankingSnapshot = (RankingSnapshot) session.createCriteria(RankingSnapshot.class)
        .add(Expression.eq( "userId", Long.parseLong(userId)))
        .add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()))
        .uniqueResult(); 
        
        boolean newRecord = false;
        if (rankingSnapshot == null)
        {     
            rankingSnapshot = new RankingSnapshot();
            rankingSnapshot.setUserId(Long.parseLong(userId));
            rankingSnapshot.setSnapshotMonth(DateObjectHelper.getCurrentMonthDateObject());
            newRecord = true;
        }  
        long currentCount = rankingSnapshot.getVotes() + 1;
        rankingSnapshot.setVotes(currentCount);
        rankingSnapshot.setLastUpdate(new Date());
        // Save 
        if (newRecord)
        { 
            session.save(rankingSnapshot);
        }else
        {  
            session.saveOrUpdate(rankingSnapshot);
        }  
        
        session.getTransaction().commit();
        if (session.isOpen()){
            session.close();
        }
        
        return currentCount;  
    
    }
    
    // Increase game points 
    public static long increaseGamePoints(String userId)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        // Get Ranking snapshot 
        RankingSnapshot rankingSnapshot = (RankingSnapshot) session.createCriteria(RankingSnapshot.class)
        .add(Expression.eq( "userId", Long.parseLong(userId)))
        .add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()))
        .uniqueResult(); 
        
        boolean newRecord = false;
        if (rankingSnapshot == null)
        {     
            rankingSnapshot = new RankingSnapshot();
            rankingSnapshot.setUserId(Long.parseLong(userId));
            rankingSnapshot.setSnapshotMonth(DateObjectHelper.getCurrentMonthDateObject());
            newRecord = true;
        }  
        long currentCount = rankingSnapshot.getGameTopPoints() + 1;
        rankingSnapshot.setGameTopPoints(currentCount);
        rankingSnapshot.setLastUpdate(new Date());
        // Save 
        if (newRecord)
        { 
            session.save(rankingSnapshot);
        }else
        {  
            session.saveOrUpdate(rankingSnapshot);
        }  
        
        session.getTransaction().commit();
        if (session.isOpen()){
            session.close();
        }
        
        return currentCount;  
    
    }
    
    
    public static RankingSnapshot getUserDetails(String userId)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        // Get Ranking snapshot 
        RankingSnapshot rankingSnapshot = (RankingSnapshot) session.createCriteria(RankingSnapshot.class)
        .add(Expression.eq( "userId", Long.parseLong(userId)))
        .add(Expression.eq( "snapshotMonth", DateObjectHelper.getCurrentMonthDateObject()))
        .uniqueResult(); 
         
        if (rankingSnapshot == null)
        {     
            rankingSnapshot = new RankingSnapshot();  
        }   
        
        session.getTransaction().commit();
        if (session.isOpen()){
            session.close();
        }
        
        return rankingSnapshot;
    }
    
    
     public static void autoCreateNextMonthVotes()
     {
         
     
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {
            
            String realUsersUpdate = "insert into RANKING_SNAPSHOT(user_id, snapshot_month, page_view, invites_sent, invites_accepted, votes, game_top_pts, last_update)" +
                               " SELECT user_id,'" +  DateObjectHelper.getNextMonthFirstDaySQLString() + "',0,0,0,2,0,now() FROM USER "+
                               " where email_address not like '%@test.com'";
            int realUsers = session.createSQLQuery(realUsersUpdate).executeUpdate();
            System.out.println("Inserted " + realUsers + " real users");
            
            String testUsersUpdate = "insert into RANKING_SNAPSHOT(user_id, snapshot_month, page_view, invites_sent, invites_accepted, votes, game_top_pts, last_update)" +
                               " SELECT user_id,'" +  DateObjectHelper.getNextMonthFirstDaySQLString() + "',0,0,0,1,0,now() FROM USER "+
                               " where email_address like '%@test.com'";
            int testUsers = session.createSQLQuery(testUsersUpdate).executeUpdate();
            System.out.println("Inserted " + testUsers + " test users");
            
        } catch (HibernateException ex) {
            ex.printStackTrace();
        }
       
        
            session.getTransaction().commit();
            if (session.isOpen()){
                session.close();
            }
         
        //insert into RANKING_SNAPSHOT(user_id, snapshot_month, page_view, invites_sent, invites_accepted, votes, game_top_pts, last_update)
        //SELECT user_id,'2009-09-01 00:00:00',0,0,0,2,0,now() FROM USER
        //where email_address not like '%@test.com'
         
        //insert into RANKING_SNAPSHOT(user_id, snapshot_month, page_view, invites_sent, invites_accepted, votes, game_top_pts, last_update)
        //SELECT user_id,'2009-09-01 00:00:00',0,0,0,1,0,now() FROM USER
        //where email_address like '%@test.com'
     }
     
     public static void main (String[] args)
     { 
     
        String propertyFile = ConfigurationHelper.getInstance().getPropertyValue("config.startup.path");
        System.setProperty(PropertyConstants.MAIN_CONFIG_FILE_KEY,propertyFile);
        
        RankingBO.autoCreateNextMonthVotes();
        
     } 
     
    
}
