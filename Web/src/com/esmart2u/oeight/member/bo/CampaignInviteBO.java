/*
 * CampaignInviteBO.java
 *
 * Created on May 15, 2008, 4:30 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.CampaignInvite;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.helper.EmailAddressHelper;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;

/**
 *
 * @author meauchyuan.lee
 */
public class CampaignInviteBO { 
    
    private static Logger logger = Logger.getLogger(CampaignInviteBO.class);
    
    /** Creates a new instance of CampaignInviteBO */
    public CampaignInviteBO() {
    }
    
    public List saveCampaignInviteAddresses(String userId, String emailAddresses, int campaignCode) {
        HashSet emailSet = new HashSet();
        
    
        // New email extractor
        emailSet = EmailAddressHelper.getEmailAddress(emailAddresses);
        
        List emailList = null;
        if (!emailSet.isEmpty()) {
            emailList = new ArrayList();
            emailList.addAll(emailSet);
        }
        
        // Save into DB, ignore duplicates
        List savedList = insertCampaignAddressesIntoDB(userId, emailList, campaignCode);
        
        return savedList;
    }   
    
    private static List insertCampaignAddressesIntoDB(String userId, List emailList, int campaignCode){
        //String oeightId = PropertyManager.getValue(PropertyConstants.SYSTEM_USERID_KEY); 
        //boolean requestFromSystem  = oeightId.equals(userId)? true:false;
        UserBO userBO = new UserBO();
        User user = userBO.getUserById(userId);
        String userEmail = user.getEmailAddress();
        List savedList = null;
        int count = 0; 
        
        if (emailList != null && !emailList.isEmpty()){
            // Loop each and insert into db
            logger.debug("Loop to insert into db");
            Session session = HibernateUtil.getSessionFactory().openSession();
            
            Transaction transaction = null;
            try {

                transaction = session.beginTransaction();
                savedList = new ArrayList();
             
            // Find own record and insert one first, for invites tracking
             CampaignInvite dbCampaignInviteOwnRecord = (CampaignInvite) session.createCriteria(CampaignInvite.class)
            .add(Expression.eq( "userId", Long.parseLong(userId)))
            .add(Expression.eq( "campaignCode", campaignCode ))
            .add(Expression.eq( "status", OEightConstants.CAMPAIGN_INVITE_OWNER_RECORD ))
            .uniqueResult(); 
            if (dbCampaignInviteOwnRecord == null) {  
                dbCampaignInviteOwnRecord = new CampaignInvite(); 
                dbCampaignInviteOwnRecord.setUserId(Long.parseLong(userId));
                dbCampaignInviteOwnRecord.setUserEmail(userEmail);
                dbCampaignInviteOwnRecord.setEmailAddress(userEmail); 
                dbCampaignInviteOwnRecord.setCampaignCode(campaignCode); 
                dbCampaignInviteOwnRecord.setStatus(OEightConstants.CAMPAIGN_INVITE_OWNER_RECORD);
                dbCampaignInviteOwnRecord.setDateSent(new Date()); 
                dbCampaignInviteOwnRecord.setTotalAccepted(0);
                session.save(dbCampaignInviteOwnRecord); 
            }
            
            Date dateSent = new Date();
            //User user = new UserBO().getUserById(userId);
            for(int i=0;i<emailList.size();i++) {
                String email = (String)emailList.get(i);
                CampaignInvite dbCampaignInvite = (CampaignInvite) session.createCriteria(CampaignInvite.class)
                .add(Expression.eq( "userId", Long.parseLong(userId)))
                .add(Expression.eq( "campaignCode", campaignCode ))
                .add(Expression.eq( "emailAddress", email ))
                .uniqueResult();
                
                if (dbCampaignInvite == null) {
                    CampaignInvite campaignInvite = new CampaignInvite();
                    //user.setUserId(Long.parseLong(userId));
                    //invite.setUser(user);
                    campaignInvite.setUserId(Long.parseLong(userId));
                    campaignInvite.setUserEmail(userEmail);
                    campaignInvite.setEmailAddress(email); 
                    campaignInvite.setCampaignCode(campaignCode); 
                    campaignInvite.setStatus(OEightConstants.MAIL_STATUS_NEW);
                    campaignInvite.setDateSent(dateSent); 
                    campaignInvite.setTotalAccepted(0);
                    savedList.add(campaignInvite);
                    logger.debug("Adding into db " + campaignInvite.getEmailAddress());
                    session.save(campaignInvite);
                    count++; // Add count if only if no errors
                }
                else // invite already exists
                { 
                    // If user has not accepted before, we resend invite by updating mail status to New again 
                    if (dbCampaignInvite.getStatus() != OEightConstants.MAIL_STATUS_ACCEPTED)
                    {
                        // May 1st, after change to open registration, should not happen to be another invite link
                        dbCampaignInvite.setStatus(OEightConstants.MAIL_STATUS_NEW);
                        session.save(dbCampaignInvite);
                        savedList.add(dbCampaignInvite);
                    }
                } 
                
            }// end for  
             transaction.commit();
            
            } catch (Exception e) {
                if (transaction!=null) transaction.rollback();
                //throw e;
            } finally {
                session.close();
            }
        }
        return savedList;
    }
 

    public void updateAccepted(String fromValue, String toValue, int campaignCode, int fromLink) { 
          
        
        Session session = HibernateUtil.getSessionFactory().openSession();
           
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction();
            if (fromLink == 0){

                UserBO userBO = new UserBO();
                User user = userBO.getUserByEmail(fromValue);
                if (user != null){
                    long userId = user.getUserId();

                        // Set accepted 
                        CampaignInvite dbCampaignInvite = (CampaignInvite) session.createCriteria(CampaignInvite.class)
                        .add(Expression.eq( "userId", userId ))
                        .add(Expression.eq( "campaignCode", campaignCode ))
                        .add(Expression.eq( "emailAddress", toValue ))
                        .uniqueResult();

                        if (dbCampaignInvite != null) {
                            dbCampaignInvite.setStatus(OEightConstants.MAIL_STATUS_ACCEPTED);
                            session.update(dbCampaignInvite);

                            // Increase value
                            CampaignInvite dbCampaignInviteOwnRecord = (CampaignInvite) session.createCriteria(CampaignInvite.class)
                            .add(Expression.eq( "userId", userId))
                            .add(Expression.eq( "campaignCode", campaignCode ))
                            .add(Expression.eq( "status", OEightConstants.CAMPAIGN_INVITE_OWNER_RECORD ))
                            .uniqueResult(); 
                            if (dbCampaignInviteOwnRecord != null) {  
                                dbCampaignInviteOwnRecord.setTotalAccepted(dbCampaignInviteOwnRecord.getTotalAccepted() + 1);
                                session.update(dbCampaignInviteOwnRecord);
                            }

                        }
                    }
            }  
            else if (fromLink == OEightConstants.CAMPAIGN_INVITE_FROM_FRIENDSTER)
            {
                // If from friendster, we do not need to validate toValue
                //! fromValue is userName
                UserBO userBO = new UserBO();
                User user = userBO.getUserByUserName(fromValue);
                long userId = user.getUserId();

                // Increase value
                CampaignInvite dbCampaignInviteOwnRecord = (CampaignInvite) session.createCriteria(CampaignInvite.class)
                .add(Expression.eq( "userId", userId))
                .add(Expression.eq( "campaignCode", campaignCode ))
                .add(Expression.eq( "status", OEightConstants.CAMPAIGN_INVITE_OWNER_RECORD ))
                .uniqueResult(); 
                if (dbCampaignInviteOwnRecord != null) {  
                    dbCampaignInviteOwnRecord.setTotalAccepted(dbCampaignInviteOwnRecord.getTotalAccepted() + 1);
                    session.update(dbCampaignInviteOwnRecord);
                }

            }
      
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    }

    public void updateOwnerCampaignTracker(String userId, int campaignCode) {
        Session session = HibernateUtil.getSessionFactory().openSession();
          
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction();
            
            CampaignInvite dbCampaignInviteOwnRecord = (CampaignInvite) session.createCriteria(CampaignInvite.class)
            .add(Expression.eq( "userId", Long.parseLong(userId)))
            .add(Expression.eq( "campaignCode", campaignCode ))
            .add(Expression.eq( "status", OEightConstants.CAMPAIGN_INVITE_OWNER_RECORD ))
            .uniqueResult(); 

            if (dbCampaignInviteOwnRecord == null)
            {
                //session.beginTransaction();

                UserBO userBO = new UserBO();
                User user = userBO.getUserById(userId);
                String userEmail = user.getEmailAddress();
                dbCampaignInviteOwnRecord = new CampaignInvite(); 
                dbCampaignInviteOwnRecord.setUserId(Long.parseLong(userId));
                dbCampaignInviteOwnRecord.setUserEmail(userEmail);
                dbCampaignInviteOwnRecord.setEmailAddress(userEmail); 
                dbCampaignInviteOwnRecord.setCampaignCode(campaignCode); 
                dbCampaignInviteOwnRecord.setStatus(OEightConstants.CAMPAIGN_INVITE_OWNER_RECORD);
                dbCampaignInviteOwnRecord.setDateSent(new Date()); 
                dbCampaignInviteOwnRecord.setTotalAccepted(0);
                session.save(dbCampaignInviteOwnRecord); 

                //session.getTransaction().commit();
                user = null;
                userBO = null;
            }

         transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    }
    
    public static long getTotalAccepted(String userId, int campaignCode)
    {
        long totalAccepted = 0;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction();
            CampaignInvite dbCampaignInviteOwnRecord = (CampaignInvite) session.createCriteria(CampaignInvite.class)
            .add(Expression.eq( "userId", Long.parseLong(userId)))
            .add(Expression.eq( "campaignCode", campaignCode ))
            .add(Expression.eq( "status", OEightConstants.CAMPAIGN_INVITE_OWNER_RECORD ))
            .uniqueResult();

            if (dbCampaignInviteOwnRecord != null)
            { 
                totalAccepted = dbCampaignInviteOwnRecord.getTotalAccepted(); 
            }
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    
        return totalAccepted;
    }
 
    public static long getMaxTotalAccepted(int campaignCode)
    { 
        long totalAccepted = 0;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction();
            String queryString = "Select max(totalAccepted) from CampaignInvite";
            Query query = session.createQuery(queryString);
            List list = query.list();
            totalAccepted = ((Long)list.get(0)).longValue();
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    
    
        return totalAccepted;
    }
    
}
