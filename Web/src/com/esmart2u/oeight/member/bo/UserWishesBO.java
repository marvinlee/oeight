/*
 * UserWishesBO.java
 *
 * Created on November 30, 2007, 2:57 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.UserWishFriends;
import com.esmart2u.oeight.data.UserWishers;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.vo.WishVO;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class UserWishesBO {
    
    private static Logger logger = Logger.getLogger(UserWishesBO.class);
    
    /** Creates a new instance of UserWishesBO */
    public UserWishesBO() {
    }
    
    public List getUserWishersList(String userId) {
        List wishersList = null;
        List valueList = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWishers.class);
            crit.add( Expression.eq( "userId", Long.parseLong(userId)) );
            crit.add( Expression.eq( "status", OEightConstants.SYS_STATUS_NEW ));
            
            // Other criteria
            crit.setFetchMode("user", FetchMode.JOIN);
            // Other criteria
            //crit.setFetchMode("userCountry", FetchMode.JOIN);
            crit.addOrder( Order.desc("dateSent") );
            crit.setMaxResults(PropertyManager.getInt(PropertyConstants.WISHERS_LIST_MAX,88));
            wishersList = crit.list();
            logger.info("Listed wishersList");
            
        /*List wishersList = (List) session.createCriteria(UserWishers.class)
        .add(Expression.eq( "userId", Long.parseLong(userId)))
        .addOrder( Order.desc("dateSent") )
        .setMaxResults(PropertyManager.getInt(PropertyConstants.WISHERS_LIST_MAX,88))
        .list();*/
            
            
            
            if (wishersList != null && !wishersList.isEmpty()) {
                valueList = new ArrayList();
                for(int i=0;i<wishersList.size();i++) {
                    UserWishers wishers = (UserWishers)wishersList.get(i);
                    //logger.info("id " + wishers.getWishId());
                    //logger.info("wisher id " + wishers.getWisherId());
                    //logger.info("user name " + wishers.getUser().getUserName());
                    //logger.info("gender " + wishers.getUser().getUserCountry().getGender());
                    //logger.info("photo path" + wishers.getUser().getUserCountry().getPhotoLargePath());
                    
                    WishVO wishVO = new WishVO();
                    wishVO.setWishId(wishers.getWishId());
                    wishVO.setUserId(wishers.getUserId());
                    wishVO.setWisherId(wishers.getWisherId());
                    wishVO.setWishContent(wishers.getWishContent());
                    wishVO.setDateSent(wishers.getDateSent());
                    wishVO.setUserName(wishers.getUser().getUserName());
                    wishVO.setGender(wishers.getUser().getUserCountry().getGender());
                    wishVO.setPhotoLargePath(wishers.getUser().getUserCountry().getPhotoLargePath());
                    valueList.add(wishVO);
                    
                }
                
                // Clear query list for resource
                wishersList = null;
                
            } else {
                logger.info("is empty well wishers list!");
            }
            
            //session.getTransaction().commit();
            
        }catch(Throwable e) {
            e.printStackTrace();
        } finally{
            if (session.isOpen()){
                session.close();
            }
        }
        return valueList;
        
    }
   /*
    *
        //String sqlQuery = "SELECT user_wishers.wish_id, user_wishers.user_id, user_wishers.wisher_id, user_wishers.wish_content, user_wishers.date_sent, user_wishers.status, user.user_name, user_country.photo_s_path FROM user_wishers " +
        //String sqlQuery = "SELECT  user_wishers.wish_content, user.user_name, user_country.photo_s_path FROM user_wishers " +
        String sqlQuery = "SELECT user_wishers.wish_id, user_wishers.user_id FROM user_wishers " +
                        " INNER JOIN user on user_wishers.wisher_id = user.user_id " +
                          " INNER JOIN user_country on user_wishers.wisher_id = user_country.user_id " +
                          " WHERE user_wishers.status <> 'D' AND user_wishers.user_id = " + Long.parseLong(userId) +
                          " ORDER BY user_wishers.date_sent DESC ";
                          //" LIMIT " + PropertyManager.getInt(PropertyConstants.WISHERS_LIST_MAX,88);
        SQLQuery query = session.createSQLQuery(sqlQuery);
        query.addScalar("wish_id", Hibernate.LONG);
        query.addScalar("user_id", Hibernate.LONG);
        System.out.println("Query has been set");
        //query.setLong("userId", Long.parseLong(userId));
        //query.setMaxResults(PropertyManager.getInt(PropertyConstants.WISHERS_LIST_MAX,88));
        Iterator results = query.iterate();//list().iterator();
        System.out.println("Results obtained");
    
        while (results.hasNext() )  {
                System.out.println("Iterating");
                Object[] row = (Object[]) results.next();
                Long wishId = (Long)row[0];
                Long wishUserId = (Long)row[1];
                System.out.println("wishId=" + wishId + ", wishUserId=" + wishUserId);
                 String[] values = new String[4];
                values[0] = userId;
                values[1] = photoSmall;
                values[2] = photoLarge;
                values[3] = photoDescription;
                photos.add(values);
            }
    */
    
    public void insertUserWishes(String userName, String wisherId, String wishContent) {
        Session session = null;
        Transaction transaction = null;
        try {
            
            // Gets userId from username
            UserBO userBO = new UserBO();
            User user = userBO.getUserByUserName(userName);
            User wisher = userBO.getUserById(wisherId);
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            UserWishers userWishers = new UserWishers();
            userWishers.setUser(wisher);
            userWishers.setUserId(user.getUserId());
            userWishers.setWisherId(wisher.getUserId());
            userWishers.setWishContent(wishContent);
            userWishers.setDateSent(new Date());
            userWishers.setStatus(OEightConstants.SYS_STATUS_NEW);
            
            session.save(userWishers);
            
            // Add this guy to user's wish friend list
            saveFriend(userWishers, session);
            
            
            session.getTransaction().commit();
            
            // Trigger email update
            EmailUpdateBO.saveEmailUpdate(OEightConstants.NOTIFY_UPDATE_S01_RECEIVED_WISH, userWishers);
            
            user = null;
            userWishers = null;
            
        }catch(Throwable e) {
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
    }
    
    public UserWishers getUserWishersById(long wishId) {
        UserWishers userWishers = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            userWishers = (UserWishers)session.createCriteria(UserWishers.class)
            .add( Restrictions.idEq(new Long(wishId)))
            .uniqueResult();
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userWishers;
    }
    
    public void deleteWish(UserWishers userWishers) {
        
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            userWishers.setStatus(OEightConstants.SYS_STATUS_REMOVED);
            session.update(userWishers);
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
    }
    
    /**
     *  This method get friends list for user viewing. (Friend with PendingConfirmation included)
     *  Use <i>getUserConfirmedFriendsList</i> method if require only buddies that are CONFIRMED.
     */
    public List getUserFriendsList(String userId) {
        List friendsList = null;
        List valueList = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWishFriends.class);
            crit.add( Expression.eq( "userId", Long.parseLong(userId)) );
            
            // Filter status here if needed
            Criterion pendingConfirmation = Restrictions.eq("status", OEightConstants.BUDDY_NEW_REQUEST);
            Criterion friends = Restrictions.eq("status", OEightConstants.BUDDY_IS_FREN);
            //crit.add( Expression.ne( "status", OEightConstants.SYS_STATUS_REMOVED));
            LogicalExpression orExpression = Restrictions.or(pendingConfirmation, friends);
            crit.add(orExpression);
            
            // Other criteria
            crit.setFetchMode("user", FetchMode.JOIN);
            // Other criteria
            //crit.setFetchMode("userCountry", FetchMode.JOIN);
            crit.addOrder( Order.desc("status") );
            crit.addOrder( Order.desc("latestSentDate") );
            crit.setMaxResults(PropertyManager.getInt(PropertyConstants.WISHERS_LIST_MAX,888));
            friendsList = crit.list();
            logger.info("Listed wishersList");
            
        /*List wishersList = (List) session.createCriteria(UserWishers.class)
        .add(Expression.eq( "userId", Long.parseLong(userId)))
        .addOrder( Order.desc("dateSent") )
        .setMaxResults(PropertyManager.getInt(PropertyConstants.WISHERS_LIST_MAX,88))
        .list();*/
            
            
            
            if (friendsList != null && !friendsList.isEmpty()) {
                valueList = new ArrayList();
                for(int i=0;i<friendsList.size();i++) {
                    UserWishFriends friend = (UserWishFriends)friendsList.get(i);
                    //logger.info("id " + wishers.getWishId());
                    //logger.info("wisher id " + wishers.getWisherId());
                    //logger.info("user name " + wishers.getUser().getUserName());
                    //logger.info("gender " + wishers.getUser().getUserCountry().getGender());
                    //logger.info("photo path" + wishers.getUser().getUserCountry().getPhotoLargePath());
                    
                    WishVO wishVO = new WishVO();
                    wishVO.setUserId(friend.getUserId());
                    wishVO.setFriendId(friend.getFriendUserId());
                    wishVO.setStatus(friend.getStatus());
                    wishVO.setDateSent(friend.getLatestSentDate());
                    wishVO.setUserName(friend.getUser().getUserName());
                    wishVO.setGender(friend.getUser().getUserCountry().getGender());
                    wishVO.setPhotoSmallPath(friend.getUser().getUserCountry().getPhotoSmallPath());
                    valueList.add(wishVO);
                    
                }
                
                // Clear query list for resource
                friendsList = null;
                
            } else {
                logger.info("is empty wish friends list!");
            }
            
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        return valueList;
        
    }
    
    /**
     *  This method get friends list that are CONFIRMED only for user viewing.
     */
    public List getUserConfirmedFriendsList(String userId) {
        List friendsList = null;
        List valueList = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWishFriends.class);
            crit.add( Expression.eq( "userId", Long.parseLong(userId)) );
            crit.add( Expression.eq( "status", OEightConstants.BUDDY_IS_FREN ));
            
            // Other criteria
            crit.setFetchMode("user", FetchMode.JOIN);
            
            crit.addOrder( Order.desc("status") );
            crit.addOrder( Order.desc("latestSentDate") );
            friendsList = crit.list();
            logger.info("Listed wishersList");
            
            
            if (friendsList != null && !friendsList.isEmpty()) {
                valueList = new ArrayList();
                for(int i=0;i<friendsList.size();i++) {
                    UserWishFriends friend = (UserWishFriends)friendsList.get(i);
                    
                    WishVO wishVO = new WishVO();
                    wishVO.setUserId(friend.getUserId());
                    wishVO.setFriendId(friend.getFriendUserId());
                    wishVO.setStatus(friend.getStatus());
                    wishVO.setDateSent(friend.getLatestSentDate());
                    wishVO.setUserName(friend.getUser().getUserName());
                    UserCountry friendUserCountry = friend.getUser().getUserCountry();
                    wishVO.setGender(friendUserCountry.getGender());
                    wishVO.setPhotoSmallPath(friendUserCountry.getPhotoSmallPath());
                    valueList.add(wishVO);
                    
                }
                
                // Clear query list for resource
                friendsList = null;
                
            } else {
                logger.info("is empty wish friends list!");
            }
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        return valueList;
        
    }
    
    /**
     * Method was previously used to auto add into buddy list after a wish is given.
     * Now system requires users to request add buddy, but is still used to keep last posted message
     * If is not a fren, it will still be inserted and with status NOT_FREN
     * If already a fren, the status will remain and not updated.
     */
    private void saveFriend(UserWishers userWishers, Session session) {
        UserWishFriends userWishFriend = null;
        // Try to find existing record to update
        UserWishFriends dbUserWishFriends = (UserWishFriends)session.createCriteria(UserWishFriends.class)
        .add( Restrictions.eq("userId",new Long(userWishers.getUserId())))
        .add( Restrictions.eq("friendUserId",new Long(userWishers.getWisherId())))
        .uniqueResult();
        
        // If none, init for insert
        if (dbUserWishFriends == null) {
            userWishFriend = new UserWishFriends();
            userWishFriend.setUser(userWishers.getUser());
            userWishFriend.setUserId(userWishers.getUserId());
            userWishFriend.setFriendUserId(userWishers.getWisherId());
            //userWishFriend.setStatus(OEightConstants.SYS_STATUS_NEW); // New friend added
            userWishFriend.setStatus(OEightConstants.BUDDY_NOT_FREN); // New friend added
        } else {
            userWishFriend = dbUserWishFriends; //status quo
            //userWishFriend.setStatus(OEightConstants.SYS_STATUS_EXISTING); // Not a new wish
        }
        
        // Update latest fields
        userWishFriend.setLatestSentDate(userWishers.getDateSent());
        
        session.saveOrUpdate(userWishFriend);
        
    }
    
    
    public static List getUserFriendsListForWidget(long userId) {
        List friendsList = null;
        List valueList = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWishFriends.class);
            crit.add( Expression.eq( "userId", userId) );
            crit.add( Expression.ne( "friendUserId", userId) );
            
            // Filter status here if needed
            crit.add( Expression.eq( "status", OEightConstants.BUDDY_IS_FREN));
            
            // Other criteria
            crit.setFetchMode("user", FetchMode.JOIN);
            // Other criteria
            //crit.setFetchMode("userCountry", FetchMode.JOIN);
            crit.addOrder( Order.desc("latestSentDate") );
            crit.setMaxResults(8);
            friendsList = crit.list();
            logger.info("Listed friendsList");
            
            
            if (friendsList != null && !friendsList.isEmpty()) {
                valueList = new ArrayList();
                for(int i=0;i<friendsList.size();i++) {
                    UserWishFriends friend = (UserWishFriends)friendsList.get(i);
                    WishVO wishVO = new WishVO();
                    wishVO.setUserId(friend.getUserId());
                    wishVO.setFriendId(friend.getFriendUserId());
                    wishVO.setDateSent(friend.getLatestSentDate());
                    wishVO.setUserName(friend.getUser().getUserName());
                    wishVO.setGender(friend.getUser().getUserCountry().getGender());
                    wishVO.setPhotoSmallPath(friend.getUser().getUserCountry().getPhotoSmallPath());
                    valueList.add(wishVO);
                    
                }
                
                // Clear query list for resource
                friendsList = null;
                
            } else {
                logger.info("is empty wish friends list!");
            }
            
            //session.getTransaction().commit();
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        return valueList;
        
    }
    
    /**
     * Similar with getUserFriendsListForWidget(long userId)
     * Diff:
     *     Limit of records = 2 instead of 8
     *     Getting wish content
     */
    public static List getUserWishesListForWidget(long userId) {
        List wishesList = null;
        List valueList = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWishers.class);
            crit.add( Expression.eq( "userId", userId) );
            crit.add( Expression.ne( "wisherId", userId) );
            
            // Filter status here if needed
            crit.add( Expression.eq( "status", OEightConstants.SYS_STATUS_REMOVED));
            
            // Other criteria
            crit.setFetchMode("user", FetchMode.JOIN);
            // Other criteria
            //crit.setFetchMode("userCountry", FetchMode.JOIN);
            crit.addOrder( Order.desc("dateSent") );
            crit.setMaxResults(2);
            wishesList = crit.list();
            logger.info("Listed wishesList");
            
            
            if (wishesList != null && !wishesList.isEmpty()) {
                valueList = new ArrayList();
                for(int i=0;i<wishesList.size();i++) {
                    UserWishers wishes = (UserWishers)wishesList.get(i);
                    WishVO wishVO = new WishVO();
                    wishVO.setUserId(wishes.getUserId());
                    wishVO.setFriendId(wishes.getWisherId());
                    wishVO.setDateSent(wishes.getDateSent());
                    wishVO.setUserName(wishes.getUser().getUserName());
                    wishVO.setGender(wishes.getUser().getUserCountry().getGender());
                    wishVO.setPhotoSmallPath(wishes.getUser().getUserCountry().getPhotoSmallPath());
                    wishVO.setWishContent(wishes.getWishContent());
                    valueList.add(wishVO);
                    
                }
                
                // Clear query list for resource
                wishesList = null;
                
            } else {
                logger.info("is empty wish friends list!");
            }
            
            //session.getTransaction().commit();
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        return valueList;
    }
    
    public static char getRequestBuddyStatus(long requestorId, String requestedUserName) {
        
        UserWishFriends dbUserWishFriends = null;
        UserBO userBO = new UserBO();
        User requestedUser = userBO.getUserByUserName(requestedUserName);
        long requestedUserId = requestedUser.getUserId();
        
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            dbUserWishFriends = (UserWishFriends)session.createCriteria(UserWishFriends.class)
            .add( Restrictions.eq("userId",requestedUserId))
            .add( Restrictions.eq("friendUserId",requestorId))
            .uniqueResult();
            
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        
        if (dbUserWishFriends == null) {
            return OEightConstants.BUDDY_NOT_FREN;
        } else {
            return dbUserWishFriends.getStatus();
        }
        
    }
    
    /**
     *  The add is to create a record on the other end of friend so that it will appear for confirmation
     *  on the targetted friend.
     */
    public static void addRequestFriend(String loginUserId, String userId) {
        
        UserWishFriends notification = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            boolean added = false;
            
            UserWishFriends userWishFriend = null;
            // Try to find existing record to update
            UserWishFriends dbUserWishFriends = (UserWishFriends)session.createCriteria(UserWishFriends.class)
            .add( Restrictions.eq("userId", new Long(userId)))
            .add( Restrictions.eq("friendUserId", new Long(loginUserId)))
            .uniqueResult();
            
            // If none, init for insert
            if (dbUserWishFriends == null) {
                userWishFriend = new UserWishFriends();
                //Not required userWishFriend.setUser(userWishers.getUser());
                userWishFriend.setUserId(Long.parseLong(userId));
                userWishFriend.setFriendUserId(Long.parseLong(loginUserId));
                userWishFriend.setStatus(OEightConstants.BUDDY_NEW_REQUEST); // New friend added
                
                session.save(userWishFriend);
                added = true;
                notification = userWishFriend;
            } else {
                // it could be a deleted or blocked friend, or already a friend
                // if status is deleted or not fren, we set to NEW again as a resend
                // BUDDY_NOT_FREN is possible because even if is not buddy yet, a record
                //      could have been inserted if the user received wish from the requestor
                if (OEightConstants.BUDDY_DELETED == dbUserWishFriends.getStatus()
                || OEightConstants.BUDDY_NOT_FREN == dbUserWishFriends.getStatus()   ){
                    dbUserWishFriends.setStatus(OEightConstants.BUDDY_NEW_REQUEST); // New friend added
                    added = true;
                    notification = dbUserWishFriends;
                }
                // if status is already a friend, set as request
                else if (OEightConstants.BUDDY_IS_FREN == dbUserWishFriends.getStatus()){
                    dbUserWishFriends.setStatus(OEightConstants.BUDDY_NEW_REQUEST); // New friend added
                    added = true;
                    notification = dbUserWishFriends;
                }
                // but if blocked, then it will never be revived(unless the blocker request add)
                session.update(dbUserWishFriends);
            }
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        
        // Fail save, even if fail to insert, we make sure this transaction is ok
        // Trigger email update
        if (notification!=null) {
            EmailUpdateBO.saveEmailUpdate(OEightConstants.NOTIFY_UPDATE_S03_RECEIVE_BUDDY_REQ, notification);
        }
    }
    
    
    
    /**
     *  The confirm is to update the friend request record and create another record on the other end.
     */
    public static void confirmRequestFriend(String loginUserId, String userId) {
        
        UserWishFriends notification = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            boolean confirmed = false;
            UserWishFriends userWishFriend = null;
            // Try to find existing record to update
            UserWishFriends dbUserWishFriends = (UserWishFriends)session.createCriteria(UserWishFriends.class)
            .add( Restrictions.eq("userId",new Long(loginUserId)))
            .add( Restrictions.eq("friendUserId",new Long(userId)))
            .add( Restrictions.eq("status",OEightConstants.BUDDY_NEW_REQUEST))
            .uniqueResult();
            
            // If none, init for insert
            if (dbUserWishFriends != null) {
                dbUserWishFriends.setStatus(OEightConstants.BUDDY_IS_FREN); // Confirm friend added
                session.saveOrUpdate(dbUserWishFriends);
                
                // Now add a friend record to the requestor
                
                // Get to see if existing record exists, it could be previously a request has been sent
                // and the buddy deleted or blocked the user
                userWishFriend = (UserWishFriends)session.createCriteria(UserWishFriends.class)
                .add( Restrictions.eq("userId",new Long(userId)))
                .add( Restrictions.eq("friendUserId",new Long(loginUserId)))
                .uniqueResult();
                
                if (userWishFriend == null) {
                    userWishFriend = new UserWishFriends();
                    userWishFriend.setUserId(Long.parseLong(userId));
                    userWishFriend.setFriendUserId(Long.parseLong(loginUserId));
                    userWishFriend.setStatus(OEightConstants.BUDDY_IS_FREN); // New friend added
                } else {
                    userWishFriend.setStatus(OEightConstants.BUDDY_IS_FREN); // Confirm friend added
                }
                session.saveOrUpdate(userWishFriend);
                confirmed = true;
                notification = userWishFriend;
            }
            
            
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        
        // Fail save, even if fail to insert, we make sure this transaction is ok
        // Trigger email update
        if (notification!=null) {
            EmailUpdateBO.saveEmailUpdate(OEightConstants.NOTIFY_UPDATE_S04_RECEIVE_BUDDY_REQ_CONFIRM, notification);
        }
    }
    
    /**
     *  Remove both side from being friends
     */
    public static void deleteFriend(String loginUserId, String userId) {
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            UserWishFriends userWishFriend = null;
            // Try to find existing record to update
            UserWishFriends dbUserWishFriends = (UserWishFriends)session.createCriteria(UserWishFriends.class)
            .add( Restrictions.eq("userId",new Long(loginUserId)))
            .add( Restrictions.eq("friendUserId",new Long(userId)))
            //.add( Restrictions.eq("status",OEightConstants.BUDDY_IS_FREN)) - No matter what status
            .uniqueResult();
            
            // If none, init for insert
            if (dbUserWishFriends != null) {
                dbUserWishFriends.setStatus(OEightConstants.BUDDY_DELETED); // Delete friend
                session.saveOrUpdate(dbUserWishFriends);
                
                // Now set status at other end
                
                // Get to see if existing record exists
                userWishFriend = (UserWishFriends)session.createCriteria(UserWishFriends.class)
                .add( Restrictions.eq("userId",new Long(userId)))
                .add( Restrictions.eq("friendUserId",new Long(loginUserId)))
                .uniqueResult();
                
                if (userWishFriend != null) {
                    userWishFriend.setStatus(OEightConstants.BUDDY_DELETED); // Delete friend
                }
                session.saveOrUpdate(userWishFriend);
            }
            
            
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
    }
    
    public static boolean isFriend(String loginUserId, String userId) {
        
        UserWishFriends dbUserWishFriends = null;
        Session session = null;
        Transaction transaction = null;
        try {
            
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Criteria crit = session.createCriteria(UserWishFriends.class);
            /*crit.add( Restrictions.eq("userId", new Long(loginUserId)));
            crit.add( Restrictions.eq("friendUserId",new Long(userId)));*/
            crit.add( Restrictions.eq("userId", new Long(userId)));
            crit.add( Restrictions.eq("friendUserId",new Long(loginUserId)));
            
            // Filter status here if needed
            Criterion pendingConfirmation = Restrictions.eq("status", OEightConstants.BUDDY_NEW_REQUEST);
            Criterion friends = Restrictions.eq("status", OEightConstants.BUDDY_IS_FREN);
            //crit.add( Expression.ne( "status", OEightConstants.SYS_STATUS_REMOVED));
            LogicalExpression orExpression = Restrictions.or(pendingConfirmation, friends);
            crit.add(orExpression);
            
            dbUserWishFriends = (UserWishFriends)crit.uniqueResult();
            
            transaction.commit();
            
            
        }catch(Throwable e) {
            if (transaction!=null) transaction.rollback();
            e.printStackTrace();
        } finally {
            if (session.isOpen()){
                session.close();
            }
        }
        
        return dbUserWishFriends == null? false: true;
    }
}
