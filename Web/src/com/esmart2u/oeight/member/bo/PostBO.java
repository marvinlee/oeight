/*
 * PostBO.java
 *
 * Created on September 21, 2009, 5:09 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.UserPost;
import com.esmart2u.oeight.data.UserPostVotes;
import com.esmart2u.solution.base.logging.Logger;
import java.util.Date;
import org.hibernate.*;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class PostBO {
    
    /** Creates a new instance of PostBO */
    public PostBO() {
    }
        
    private static Logger logger = Logger.getLogger(PostBO.class);
            
    public UserPost savePost(UserPost userPost) {  
        return insertUserPostIntoDB(userPost);  
    }
    
    private UserPost insertUserPostIntoDB(UserPost userPost){ 
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            session.save(userPost); 
            System.out.println("UserPost Object id:" + userPost.getUserPostId());

            UserPostVotes userPostVotes = new UserPostVotes();
            userPostVotes.setUserId(userPost.getPostedByUserId());
            userPostVotes.setDateVoted(new Date());
            userPostVotes.setVotedUp(1); 
            session.save(userPostVotes);
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userPost;
    }

    public UserPost getPostWithUrl(String urlString) {
 
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserPost userPost = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction(); 
            userPost = (UserPost)session.createCriteria(UserPost.class)
            .add( Restrictions.naturalId()
            .set("postUrl", urlString)
            ).setCacheable(true)
            .uniqueResult(); 
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return userPost;
    }
}
