/*
 * ForumBO.java
 *
 * Created on August 14, 2008, 5:45 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.ForumTopic;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.phpbb.PhpBbForums;
import com.esmart2u.oeight.data.phpbb.PhpBbSessions;
import com.esmart2u.oeight.data.phpbb.PhpBbTopics;
import com.esmart2u.oeight.data.phpbb.PhpBbUserGroup;
import com.esmart2u.oeight.data.phpbb.PhpBbUsers;
import com.esmart2u.oeight.member.web.struts.controller.LoginForm;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class ForumBO {
    
    private static Logger logger = Logger.getLogger(ForumBO.class);
    /** Creates a new instance of ForumBO */
    public ForumBO() {
    }
    
    /**
     * Sets forum auto login
     *
     */
    public PhpBbUsers performForumLogin(String loginEmail, String sessionId) throws Exception {
        PhpBbUsers phpBbUser = null;
        System.out.println("ForumBO perform Login");
        // Gets oeight id from email
        UserBO userBO = new UserBO();
        User user = userBO.getUserByEmail(loginEmail);
        
        if (user != null){
            // Get phpbb user id by username
            System.out.println("get PhpBbUser with " + user.getUserName());
            phpBbUser = getPhpBbUser(user.getUserName());
        
            if (phpBbUser != null){
                // Sets session id to phpbb
                System.out.println("Creates session id now");
                createPhpBbSession(phpBbUser.getUserId(), sessionId);
                
                // Update phpbb users table
                System.out.println("Updates user table id :" + phpBbUser.getUserId());
                updatePhpBbUsersLogin(phpBbUser);
            }
            else
            {
                System.out.println("PhpBbUser not found");
            }
        }
        else
        {
            logger.error("User not found!!");
        }
        
        return phpBbUser;
        
    }
    
    private PhpBbUsers getPhpBbUser(String username) {
        System.out.println("Getting phpbb user for :" + username);
        Session session = HibernateUtil.getSessionFactory().openSession();
        PhpBbUsers phpBbUser = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            phpBbUser = (PhpBbUsers)session.createCriteria(PhpBbUsers.class)
            .add( Restrictions.eq("userName",username))
            .uniqueResult();
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return phpBbUser;
    }

    private void createPhpBbSession(long phpBbUserId, String sessionId) throws Exception{
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {

            transaction = session.beginTransaction();

            Date now = new Date(); 
            PhpBbSessions phpBbSession = (PhpBbSessions) session.createCriteria(PhpBbSessions.class)
            .add(Expression.idEq(sessionId)) 
            .uniqueResult();

            if (phpBbSession == null) {
                phpBbSession = new PhpBbSessions();
                phpBbSession.setSessionId(sessionId);
                phpBbSession.setSessionUserId(phpBbUserId);
                phpBbSession.setSessionForumId(0);
                phpBbSession.setSessionLastVisit(now.getTime()/1000);
                phpBbSession.setSessionStart(now.getTime()/1000);
                phpBbSession.setSessionTime(now.getTime()/1000);
                phpBbSession.setSessionIp("");
                phpBbSession.setSessionBrowser("");
                phpBbSession.setSessionForwardedFor("");
                phpBbSession.setSessionPage(""); 
                phpBbSession.setSessionViewOnline(1);
                phpBbSession.setSessionAutoLogin(0);
                phpBbSession.setSessionAdmin(0);
                logger.debug("Adding into db " + phpBbSession.getSessionId());
                session.save(phpBbSession);
            } else {
                System.out.println("Session id already exists!!");
                throw new Exception("Forum Session id already exists!!"); 
                /*dbLoginReset.setStatus(OEightConstants.MAIL_STATUS_NEW);
                dbLoginReset.setDateSent(dateSent);
                session.update(dbLoginReset);
                 */
            } 

            transaction.commit();

        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            throw e;
        } finally {
            session.close();
        } 
        
    }

    private void updatePhpBbUsersLogin(PhpBbUsers phpBbUser) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            phpBbUser = (PhpBbUsers)session.createCriteria(PhpBbUsers.class)
            .add( Expression.idEq(phpBbUser.getUserId()))
            .uniqueResult();
            
            phpBbUser.setUserLastVisit(new Date().getTime()/1000);
            session.saveOrUpdate(phpBbUser);
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Steps according to phpbb session.php - session_kill
     * 1) Delete from SESSIONS table
     * 2) Update user table set user_lastvisit
     */
    public PhpBbUsers performForumLogout(String pbpBbSessionId) throws Exception {
        PhpBbUsers phpBbUser = null;
        System.out.println("ForumBO perform Login"); 
         
        // Sets session id to phpbb
        System.out.println("Creates session id now");
        phpBbUser = killPhpBbSession(pbpBbSessionId);
        if (phpBbUser != null){

            // Update phpbb users table
            System.out.println("Updates user table id :" + phpBbUser.getUserId());
            updatePhpBbUsersLastVisit(phpBbUser);
        }
        else
        {
            System.out.println("PhpBbUser not found");
        } 
        
        return phpBbUser;
        
    }

    private PhpBbUsers killPhpBbSession(String pbpBbSessionId) throws Exception { 
        PhpBbUsers phpBbUser = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {

            transaction = session.beginTransaction();

            Date now = new Date(); 
            PhpBbSessions phpBbSession = (PhpBbSessions) session.createCriteria(PhpBbSessions.class)
            .add(Expression.idEq(pbpBbSessionId)) 
            .uniqueResult();

            if (phpBbSession != null) { 
                phpBbUser = new PhpBbUsers();
                phpBbUser.setUserId(phpBbSession.getSessionUserId());
                session.delete(phpBbSession);
            } else {
                logger.error("Session id does not exists!!");
                throw new Exception("Forum Session id doest not exists!!");  
            } 

            transaction.commit();

        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            throw e;
        } finally {
            session.close();
        } 
        return phpBbUser;
    }

    private void updatePhpBbUsersLastVisit(PhpBbUsers phpBbUser) {
        logger.debug("Going to set last visit for id:" + phpBbUser.getUserId());
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            phpBbUser = (PhpBbUsers)session.createCriteria(PhpBbUsers.class)
            .add( Expression.idEq(phpBbUser.getUserId()))
            .uniqueResult();
            
            phpBbUser.setUserLastVisit(new Date().getTime()/1000);
            session.update(phpBbUser);
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    }

    public void registerForumUser(User user, UserCountry userCountry) throws Exception{
        
        PhpBbUsers templatePhpBbUser = new PhpBbUsers(); 
        PhpBbUsers newPhpBbUser = new PhpBbUsers(); 
        long templateUserId = PropertyManager.getLong(PropertyConstants.PHPBB_USER_TEMPLATE_ID);
        logger.debug("Get user template with id:" );
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            templatePhpBbUser = (PhpBbUsers)session.createCriteria(PhpBbUsers.class)
            .add( Expression.idEq(templateUserId))
            .uniqueResult();
            
            if (templatePhpBbUser != null){
                // Create User
                templatePhpBbUser.cloneNewUser(newPhpBbUser);
                newPhpBbUser.setUserId(0);                
                //newPhpBbUser.setUserRegisteredDate(new Date().getTime()/1000);
                newPhpBbUser.setUserRegisteredDate(userCountry.getSystemRegistered().getTime()/1000);                
                newPhpBbUser.setUserName(user.getUserName());
                newPhpBbUser.setUserNameClean(user.getUserName());
                newPhpBbUser.setUserPasswordChange(0);
                newPhpBbUser.setUserEmail(user.getEmailAddress()); 
                newPhpBbUser.setUserLastVisit(new Date().getTime()/1000);
                newPhpBbUser.setUserLastMark(0);
                newPhpBbUser.setUserLastSearch(0);
                newPhpBbUser.setUserPosts(0);
                newPhpBbUser.setUserAllowMassEmail(0);
                newPhpBbUser.setUserSignature("");
                newPhpBbUser.setUserSignatureBBCodeUid("");
                newPhpBbUser.setUserSignatureBBCodeBitField("");
                if (userCountry.getCurrentLocation()!=null){
                    newPhpBbUser.setUserFrom(userCountry.getCurrentLocation());
                }
                if (userCountry.getOccupation()!=null){
                    newPhpBbUser.setUserOcc(userCountry.getOccupation());
                }
                if (userCountry.getUserDetails() != null){
                    if (userCountry.getUserDetails().getContactYahoo()!=null){
                        newPhpBbUser.setUserYim(userCountry.getUserDetails().getContactYahoo());
                    }
                    if (userCountry.getUserDetails().getContactMsn()!=null){
                                newPhpBbUser.setUserMsnm(userCountry.getUserDetails().getContactMsn());
                    }
                    if (userCountry.getUserDetails().getWebBlog()!=null){
                        newPhpBbUser.setUserWebsite(userCountry.getUserDetails().getWebBlog());
                    }
                }
                
                session.save(newPhpBbUser);  
                transaction.commit(); // We need to get the persisted user id, so commit first
                
                transaction.begin();
                PhpBbUsers phpBbUser = getPhpBbUser(user.getUserName());
                // Create User Group
                List groupList = session.createCriteria(PhpBbUserGroup.class)
                .add( Expression.eq("userId", new Long(templateUserId))).list();
                if (groupList != null && !groupList.isEmpty())
                {
                    for (int i=0; i<groupList.size(); i++)
                    {
                        PhpBbUserGroup userGroup = (PhpBbUserGroup)groupList.get(i);
                        PhpBbUserGroup newUserGroup = new PhpBbUserGroup();
                        newUserGroup.setGroupId(userGroup.getGroupId());
                        newUserGroup.setUserId(phpBbUser.getUserId());
                        newUserGroup.setGroupLeader(0);
                        newUserGroup.setUserPending(0);
                        session.save(newUserGroup);
                    } 
                }
                transaction.commit();
                
            }
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
        
    }

    public void updateForumUserPersonalDetails(PhpBbUsers forumUser) {
        // Need to get forum user with username first only perform update on the few particular fields
        String userName = forumUser.getUserName();
        System.out.println("updateForumUserPersonalDetails for :" + userName);
        Session session = HibernateUtil.getSessionFactory().openSession();
        PhpBbUsers phpBbUser = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            phpBbUser = (PhpBbUsers)session.createCriteria(PhpBbUsers.class)
            .add( Restrictions.eq("userName",userName))
            .uniqueResult();
            
            if (phpBbUser != null)
            {
                //phpBbUser.setUserBirthday(forumUser.getUserBirthday()); 
                if (forumUser.getUserFrom() != null){
                    phpBbUser.setUserFrom(forumUser.getUserFrom());
                }
                if (forumUser.getUserOcc() != null){
                    phpBbUser.setUserOcc(forumUser.getUserOcc());
                }
                session.update(phpBbUser);
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    }
    
    public void updateForumUserPersonalContacts(PhpBbUsers forumUser) {
        // Need to get forum user with username first only perform update on the few particular fields
        String userName = forumUser.getUserName();
        //System.out.println("updateForumUserPersonalContacts for :" + userName);
        Session session = HibernateUtil.getSessionFactory().openSession();
        PhpBbUsers phpBbUser = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            phpBbUser = (PhpBbUsers)session.createCriteria(PhpBbUsers.class)
            .add( Restrictions.eq("userName",userName))
            .uniqueResult();
            
            if (phpBbUser != null)
            {
                if (forumUser.getUserYim() != null){
                    phpBbUser.setUserYim(forumUser.getUserYim());
                }
                if (forumUser.getUserMsnm() != null){
                    phpBbUser.setUserMsnm(forumUser.getUserMsnm());
                }
                if (forumUser.getUserWebsite() != null){
                    phpBbUser.setUserWebsite(forumUser.getUserWebsite()); 
                }
                
                session.update(phpBbUser);
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
    }

    public ArrayList createForumUsers()
    {
        ArrayList statsList = new ArrayList();
        ArrayList addedList = new ArrayList();
        ArrayList existList = new ArrayList();
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        PhpBbUsers phpBbUser = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            // Gets current oEight users, to loop
            List userList = (List)session.createCriteria(UserCountry.class).list();
            
            if ((userList != null) && (!userList.isEmpty()))
            {
                for (int i=0; i< userList.size(); i++)
                {
                    // Skip if user already exists in forum
                    UserCountry oEightUserCountry = (UserCountry)userList.get(i);
                    //System.out.println("Checking registration for:"+oEightUserCountry.getUser().getUserName());
                    phpBbUser = (PhpBbUsers)session.createCriteria(PhpBbUsers.class)
                    .add( Restrictions.eq("userName",oEightUserCountry.getUser().getUserName()))
                    .uniqueResult();
                    
                    if (phpBbUser != null)
                    { 
                        //System.out.println("User registered in forum");
                        existList.add(oEightUserCountry.getUser());
                    }
                    else
                    {
                        // Add if user is not in forum yet
                        //System.out.println("Adding new User to forum");
                        registerForumUser(oEightUserCountry.getUser(),oEightUserCountry);
                        addedList.add(oEightUserCountry.getUser());
                    }
                
                } 
            
                statsList.add(existList);
                statsList.add(addedList);
            }
         
            
            transaction.commit();
            
        } catch (Throwable e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
            e.printStackTrace();
        } finally {
            session.close();
        }
        
        
        return statsList;
    
    }

    /**
     *  Get forum list
     */
    public List getForumDropDown() { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        List forumList = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            forumList = session.createCriteria(PhpBbForums.class).list(); 
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return forumList;
    }

    public List getTopicDropDown(long forumId) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        List topicList = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            topicList = session.createCriteria(PhpBbTopics.class)
            .add( Restrictions.eq("forumId", new Long(forumId)))
            .addOrder( Order.desc("topicTime"))
            .setMaxResults(20)
            .list(); 
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return topicList;
    }

    public HashMap addNewForumTopic(ForumTopic forumTopic) {
          
        Session session = HibernateUtil.getSessionFactory().openSession();
        List forumTopicList = null;
        Transaction transaction = null;
        HashMap resultMap = null;
        try {
            List forumList = getForumDropDown();
            String forumName = "";            
            for(int i=0;i<forumList.size();i++)
            {
                PhpBbForums forum = (PhpBbForums)forumList.get(i);
                if (forum.getForumId() == forumTopic.getForumId())
                {
                    forumName = forum.getForumName();
                }
            }
                    
            // Get the topic info from PHPBB db
            transaction = session.beginTransaction();
            PhpBbTopics topic = (PhpBbTopics)session.createCriteria(PhpBbTopics.class)
            .add( Restrictions.eq("topicId", new Long(forumTopic.getTopicId()))) 
            .uniqueResult();  

            // Insert into ForumTopic
            if (topic != null)
            {
                logger.debug("Found topic:" + topic.getTopicTitle());
                forumTopic.setForumName(forumName);
                forumTopic.setTopicTitle(topic.getTopicTitle());
                forumTopic.setTopicPosterName(topic.getTopicFirstPosterName());
                forumTopic.setTopicTime(new Date(topic.getTopicTime()*1000));
                session.saveOrUpdate(forumTopic); 
            }

            // Retrieve entire ForumTopic 
            forumTopicList = session.createCriteria(ForumTopic.class)
            .addOrder(Order.desc("topicTime"))
            .setMaxResults(100)
            .list();
            logger.debug("Got forumTopicList:" + forumTopicList);
            resultMap = filterForumTopicList(forumTopicList);
            
            
            transaction.commit();
            
        } catch (Throwable e) {
            e.printStackTrace();
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        
        return resultMap;
    }
    
    private HashMap filterForumTopicList(List forumTopicList)
    { 
        HashMap forumMap = new HashMap(); // Group by forum
        if (forumTopicList !=null && !forumTopicList.isEmpty())
        {
            for(int i=0; i<forumTopicList.size();i++)
            {   
                ForumTopic topic = (ForumTopic)forumTopicList.get(i);
                if (!forumMap.containsKey(topic.getForumId()))
                {
                    List topicList = new ArrayList();
                    topicList.add(topic);
                    forumMap.put(topic.getForumId(),topicList);
                }  
                else
                {
                    List topicList = (ArrayList)forumMap.get(topic.getForumId());
                    topicList.add(topic); 
                }
            } 
        }
    
        return forumMap;
    }

    public HashMap refreshForumTopic() {
        return ForumCacheBO.getInstance().reloadMap(); 
    }
    
}
