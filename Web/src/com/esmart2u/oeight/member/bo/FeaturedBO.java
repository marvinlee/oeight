/*
 * FeaturedBO.java
 *
 * Created on October 23, 2007, 11:52 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.FeaturedBlog;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;

/**
 *
 * @author meauchyuan.lee
 */
public class FeaturedBO {
    
    private static Logger logger = Logger.getLogger(FeaturedBO.class);
    
    private static FeaturedBO instance;
    
    private static final String CURRENT_DAY_FORMAT = "ddMM";
    private static final SimpleDateFormat currentDayFormatter = new SimpleDateFormat(CURRENT_DAY_FORMAT);
    private static String today = "";
    private static List blogList; // used later to get by date to publish
    
    private static List featuredBlogList;
    
    /** Creates a new instance of FeaturedBO */
   
    public static synchronized FeaturedBO getInstance(){
        if ( instance == null ){
            instance = new FeaturedBO(); 
        }
        return instance;
    }
    
    private static List getFeaturedBlogFromDB()
    {
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            Criteria crit = session.createCriteria(FeaturedBlog.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.eq( "status", OEightConstants.SYS_STATUS_MODERATED));  
            crit.setMaxResults(20);
            List foundList = crit.list();
            // If found, then set to featured, if no, set nothing and reuse
            if (foundList != null && !foundList.isEmpty())
            {
                getInstance().featuredBlogList = foundList;
            }

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    
        return getInstance().featuredBlogList;  
    
    }
    
    
    public static List getFeaturedBlogs()
    { 
        if (getInstance().featuredBlogList == null)
        {
            getFeaturedBlogFromDB();
        }
        return getInstance().featuredBlogList;
    }
    
    public static void reloadList()
    { 
        getFeaturedBlogFromDB(); 
    }
     /*
    public static List getFeaturedBlog()
    { 
        // Save some database hit
        if (!today.equals(getToday())){  
            Session session = HibernateUtil.getSessionFactory().openSession();
            //session.beginTransaction();

            // Get Ranking snapshot 
            Criteria crit = session.createCriteria(FeaturedBlog.class);
            crit.setFetchMode("user", FetchMode.JOIN);
            crit.add(Expression.eq( "dateToList", DateObjectHelper.getCurrentDayDateObject()));  
            crit.setMaxResults(20);
            List foundList = crit.list();
            // If found, then set to featured, if no, set nothing and reuse
            if (foundList != null && !foundList.isEmpty())
            {
                blogList = foundList;
            }

            //session.getTransaction().commit(); 
            
            if (session.isOpen()){
                session.close();
            }
            
            today = getToday();
        } 
    
        return blogList;  
    }
      */
     
    private static String getToday()
    {
        logger.debug("Current day" + currentDayFormatter.format(new Date()));
        return currentDayFormatter.format(new Date());
    }

    public static FeaturedBlog getFeaturedBlog(long userId) {
        FeaturedBlog featuredBlog = null;
        boolean found = false;
        List list = getFeaturedBlogs();
        for (int i=0;i<list.size();i++){
            featuredBlog = (FeaturedBlog)list.get(i);
            if (featuredBlog.getUserId() == userId){
                found = true;
                break;   
            }
        }
        if (!found){
            featuredBlog = null; 
        }
        return featuredBlog;
    }
    
}
