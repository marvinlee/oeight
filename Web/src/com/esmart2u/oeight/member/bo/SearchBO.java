/*
 * SearchBO.java
 *
 * Created on October 15, 2007, 1:33 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.HibernatePage;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class SearchBO {
    
    private Logger logger = Logger.getLogger(SearchBO.class);
    
    /** Creates a new instance of SearchBO */
    public SearchBO() {
    }
    
    public HibernatePage getSearchUserResult(UserCountry userCountry, int pageNumber) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        HibernatePage page = null;
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            logger.debug("Search criteria");
            logger.debug("Name:" + userCountry.getName());
            logger.debug("Email:" + userCountry.getPublicEmail());
            logger.debug("From:" + userCountry.getFromAge());
            logger.debug("To:" + userCountry.getToAge());
            logger.debug("Gender:" + userCountry.getGender());
            logger.debug("City:" + userCountry.getCity());
            logger.debug("State:" + userCountry.getState());
            logger.debug("Nation:" + userCountry.getNationality());
            logger.debug("Cur Loc:" + userCountry.getCurrentLocation());
            
            // List result = session.createQuery("from UserCountry").list();
            Criteria crit = session.createCriteria(UserCountry.class);
            crit.add( Expression.ne( "reportAbuse", OEightConstants.REPORT_ABUSE_CONFIRMED) );
            
            if (StringUtils.hasValue(userCountry.getName())){
                crit.add( Expression.ilike( "name", userCountry.getName(), MatchMode.ANYWHERE));
            }
            if (StringUtils.hasValue(userCountry.getPublicEmail())){
                crit.add( Expression.ilike( "publicEmail", userCountry.getPublicEmail()) );
            }
            
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date fromDate = null;
            Date toDate = null;
            Date currentDate = new Date(System.currentTimeMillis());
            try {
                int currentYear = Integer.parseInt(sdf.format(currentDate).substring(6,10));
                int fromYear = currentYear - userCountry.getFromAge();
                int toYear = currentYear - userCountry.getToAge();
                fromDate = sdf.parse("31/12/"+ fromYear);
                toDate = sdf.parse("01/01/" + toYear);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            
            if (userCountry.getFromAge() > 16){
                crit.add( Expression.le( "birthDate", fromDate) );
            }
            if (userCountry.getToAge()<80 ){
                crit.add( Expression.ge( "birthDate", toDate) );
            }
            if ((userCountry.getGender() == PropertyConstants.GENDER_FEMALE)
            || (userCountry.getGender() == PropertyConstants.GENDER_MALE)){
                crit.add( Expression.eq( "gender", userCountry.getGender()) );
            }
            if (StringUtils.hasValue(userCountry.getCity())){
                crit.add( Expression.ilike( "city", userCountry.getCity()) );
            }
            if (StringUtils.hasValue(userCountry.getState()) &&
                    !"NA".equals(userCountry.getState())){
                crit.add( Expression.eq( "state", userCountry.getState()) );
            }
            if (StringUtils.hasValue(userCountry.getNationality())){
                crit.add( Expression.eq( "nationality", userCountry.getNationality()) );
            }
            if (StringUtils.hasValue(userCountry.getCurrentLocation())){
                crit.add( Expression.ilike( "currentLocation", userCountry.getCurrentLocation()) );
            }
            
            // Other criteria
            crit.setFetchMode("user", FetchMode.JOIN);
            
            String lifestyle = userCountry.getSearchLifestyle().trim();
            String interest = userCountry.getSearchInterest().trim();
            String skills = userCountry.getSearchSkills().trim();
            
            if (StringUtils.hasValue(lifestyle)
            ||StringUtils.hasValue(interest)
            ||StringUtils.hasValue(skills)) {
                //crit.setFetchMode("userDetails", FetchMode.JOIN);
                crit.createAlias("userDetails","details");
                
                if (StringUtils.hasValue(lifestyle)) {
                    lifestyle = "%" + lifestyle + "%";
                    crit.add( Restrictions.disjunction()
                    .add( Restrictions.ilike("details.lifeAboutMe", lifestyle ) )
                    .add( Restrictions.ilike("details.lifeToKnow", lifestyle ) )
                    .add( Restrictions.ilike("details.lifeKnowMe", lifestyle ) )
                    .add( Restrictions.ilike("details.lifeValue1", lifestyle ) )
                    .add( Restrictions.ilike("details.lifeValue2", lifestyle ) )
                    .add( Restrictions.ilike("details.lifeValue3", lifestyle ) )
                    .add( Restrictions.ilike("details.lifeValue4", lifestyle ) )
                    .add( Restrictions.ilike("details.lifeValue5", lifestyle ) ));
                }
                
                if (StringUtils.hasValue(interest)) {
                    interest = "%" + interest + "%";
                    crit.add( Restrictions.disjunction()
                    .add( Restrictions.ilike("details.interestActor", interest ) )
                    .add( Restrictions.ilike("details.interestActress", interest ) )
                    .add( Restrictions.ilike("details.interestSinger", interest ) )
                    .add( Restrictions.ilike("details.interestMusic", interest ) )
                    .add( Restrictions.ilike("details.interestBand", interest ) )
                    .add( Restrictions.ilike("details.interestMovies", interest ) )
                    .add( Restrictions.ilike("details.interestBooks", interest ) )
                    .add( Restrictions.ilike("details.interestSports", interest ) )
                    .add( Restrictions.ilike("details.interestWebsites", interest ) )
                    .add( Restrictions.ilike("details.interestValue1", interest ) )
                    .add( Restrictions.ilike("details.interestValue2", interest ) )
                    .add( Restrictions.ilike("details.interestValue3", interest ) )
                    .add( Restrictions.ilike("details.interestValue4", interest ) )
                    .add( Restrictions.ilike("details.interestValue5", interest ) ));
                }
                
                if (StringUtils.hasValue(skills)) {
                    skills = "%" + skills + "%";
                    crit.add( Restrictions.disjunction()
                    .add( Restrictions.ilike("details.skillsExpert", skills ) )
                    .add( Restrictions.ilike("details.skillsGood", skills ) )
                    .add( Restrictions.ilike("details.skillsLearn", skills ) ));
                }
                
                
            }
            
            
            //    crit.createAlias("user", "user")
            //    .setProjection( Projections.projectionList()
            //    .add( Projections.property("user.userName"), "userName" ));
            
            crit.addOrder( Order.desc("birthDate") );
            
            page = HibernatePage.getHibernatePageInstance(crit, pageNumber, PropertyManager.getInt(PropertyConstants.DEFAULT_PAGE_ITEMS_KEY));
            //List result = crit.list();
            
        /*
        q.setFirstResult(20);
        q.setMaxResults(10);*/
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return page;
    }
}
