/*
 * WeddingBO.java
 *
 * Created on June 26, 2008, 10:49 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.OEightWedding;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.web.struts.helper.HibernatePage;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class WeddingBO {
    
    /** Creates a new instance of WeddingBO */
    public WeddingBO() {
    }
    
    
    public OEightWedding saveRegistration(OEightWedding wedding) {
        return insertRegistrationIntoDB(wedding);
    }
    
    private OEightWedding insertRegistrationIntoDB(OEightWedding wedding){
        
        if (wedding == null || (getWeddingRegistration(""+ wedding.getUserId()) != null)) {
            return wedding;
        }
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
             
            wedding.setStatus(OEightConstants.WEDDING_TYPE_NORMAL);// Normal, Special, Xclusive (S/N by admin)
            wedding.setDateRegistered(new Date());
            wedding.setDateUpdated(new Date());
            wedding.setAllowComments(PropertyConstants.BOOLEAN_YES);
            session.save(wedding);
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return wedding;
    }
    
    public OEightWedding updateRegistration(OEightWedding oeightWedding) {
        return updateRegistrationToDB(oeightWedding);
    }
    
    private OEightWedding updateRegistrationToDB(OEightWedding wedding){
        
        if (wedding == null ) {
            return wedding;
        }
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            // Get the same record with id, and update the rest
            /*OEightWedding weddingDB = (OEightWedding)session.createCriteria(OEightWedding.class)
            .add( Restrictions.idEq(new Long(wedding.getWeddingId())))
            .uniqueResult();
             */
            
            //weddingDB = wedding;
            wedding.setDateUpdated(new Date());
            session.update(wedding);
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return wedding;
    }
    
    /**
     *  Think of the caching! Important!!
     */
    public HibernatePage getWeddingList(int pageNumber) {
        HibernatePage page = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            
            Criteria criteria = session.createCriteria(OEightWedding.class);
            criteria.add(Expression.ne( "status", "C"));
            criteria.addOrder( Order.asc("dateRegistered")) ;
            criteria.setCacheMode(CacheMode.NORMAL);
            criteria.setCacheable(true);
            
            page = HibernatePage.getHibernatePageInstance(criteria, pageNumber, 8);
            
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return page;
    }
    
    public OEightWedding getWeddingRegistration(String userId) {
        OEightWedding weddingRegistration = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            
            Criteria criteria = session.createCriteria(OEightWedding.class);
            criteria.add(Expression.eq( "userId", new Long(userId) ));
            criteria.addOrder( Order.desc("weddingId")) ;
            criteria.setMaxResults(1);
            List resultList = criteria.list();
            
            if (resultList != null && !resultList.isEmpty()) {
                weddingRegistration = (OEightWedding)resultList.get(0);
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return weddingRegistration;
    }

    public List getWeddingListAll() {
        List list = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            
            Criteria criteria = session.createCriteria(OEightWedding.class); 
            criteria.addOrder( Order.desc("dateUpdated")) ;
            list = criteria.list(); 
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return list;
    }

    public List updateAdminWeddingList(String[] weddingIds, String[] weddingStatus) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            
            transaction = session.beginTransaction();
            
            if (weddingIds != null && weddingIds.length > 0)
            {
                for(int i=0;i<weddingIds.length;i++)
                {
                    String weddingId = weddingIds[i];
                    String status = weddingStatus[i];
                    OEightWedding oeightWedding = (OEightWedding)session.createCriteria(OEightWedding.class) 
                    .add( Restrictions.idEq(new Long(weddingId)))
                    .uniqueResult();
                    
                    if (oeightWedding!=null)
                    {
                        oeightWedding.setStatus(status.charAt(0));
                        session.saveOrUpdate(oeightWedding);
                    }
                    
                }
            
            } 
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        
        
        return getWeddingListAll();
    }
    
    
}
