/*
 * UserBO.java
 *
 * Created on September 28, 2007, 3:59 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.User; 
import com.esmart2u.oeight.data.UserCountry;
import com.esmart2u.oeight.data.UserDetails;
import com.esmart2u.oeight.data.UserPictures;
import com.esmart2u.oeight.member.web.struts.helper.ProfileMapper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.math.BigInteger;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author meauchyuan.lee
 */
public class UserBO {
    
    
    private static Logger logger = Logger.getLogger(UserBO.class);
    
    /** Creates a new instance of UserBO */
    public UserBO() {
    }
   
            
    public boolean usernameExists(String userName) {
        
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        User user = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            user = (User)session.createCriteria(User.class)
            .add( Restrictions.naturalId()
            .set("userName", userName)
            ).setCacheable(true)
            .uniqueResult();

      
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return user != null;
    }    
    
    public boolean emailExists(String emailAddress) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            user = (User)session.createCriteria(User.class)
            .add( Restrictions.naturalId()
            .set("emailAddress", emailAddress)
            ).setCacheable(true)
            .uniqueResult();
        
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        
        return user != null;
    } 
    
    public User getUserByUserName(String userName)
    { 
    
        Session session = HibernateUtil.getSessionFactory().openSession();
         User user = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction(); 
            user = (User)session.createCriteria(User.class) 
            .add( Restrictions.eq("userName",userName))
            .uniqueResult();
            
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return user;
    
    } 
    
    public User getUserById(String userId)
    { 
    
        Session session = HibernateUtil.getSessionFactory().openSession();
         User user = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction(); 
            user = (User)session.createCriteria(User.class) 
            .add( Restrictions.idEq(new Long(userId)))
            .uniqueResult();
       
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return user;
    
    } 
    
    public User getUserByEmail(String emailAddress) {
        
        Session session = HibernateUtil.getSessionFactory().openSession();
         User user = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction(); 
            user = (User)session.createCriteria(User.class)
            .add( Restrictions.naturalId()
            .set("emailAddress", emailAddress)
            ).setCacheable(true)
            .uniqueResult();
        
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return user;
    } 
    
    public User getUserPhotosById(User user)
    {
    
        Session session = HibernateUtil.getSessionFactory().openSession();
        List photoList = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction(); 
            photoList = getUserPhotoList(Long.toString(user.getUserId()), session);
            if (photoList != null && !photoList.isEmpty())
            {
                logger.debug("photoList is not null, setting it");
                user.setPhotoList(photoList); 
            }  
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return user; 
    
    }
    
    public User getUserPhotosById(String userId)
    {
    
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null; 
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction(); 
            // Get User
            logger.debug("User BO get user details");
            user = (User)session.createCriteria(User.class) 
            .add( Restrictions.idEq(new Long(userId)))
            .uniqueResult();

            List photoList = getUserPhotoList(userId, session);
            if (photoList != null && !photoList.isEmpty())
            {
                logger.debug("photoList is not null, setting it");
                user.setPhotoList(photoList); 
            }  
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return user; 
    
    }
    
    private List getUserPhotoList(String userId, Session session)
    {
     
        // Get User photo list 
        logger.debug("User BO get photo list");
        List photoList = session.createQuery("from UserPictures where userId = ?")
                        .setLong(0, new Long(userId))
                        .list();
        
        return photoList;
    }
    /*public UserCountry getUserCountryById(String userId)
    { 
    
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        User user = (User)session.createCriteria(User.class)
        .add( Restrictions.naturalId()
        .set("emailAddress", emailAddress)
        ).setCacheable(true)
        .uniqueResult();
        
        
        session.getTransaction().commit();
        
        return user;
    
    }*/

    public User saveUserProfileMain(String userId, User editedUser) {
        // Load from DB
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null; 
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();  
            user = (User)session.createCriteria(User.class) 
            .add( Restrictions.idEq(new Long(userId)))
            .uniqueResult();

            // Map fields
            user = ProfileMapper.updateUserProfileMain(user, editedUser); 
            // Save
            user = saveUserCountryObject(userId, user); 

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return user;
    }
    
    /**
     *  Only saves mosaic photo related values, leave others intact
     **/
    public User saveUserProfileMainPhoto(String userId, User editedUser) {
    
        // Load from DB
        Session session = HibernateUtil.getSessionFactory().openSession();
         User user = null; 
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();  
            user = (User)session.createCriteria(User.class) 
            .add( Restrictions.idEq(new Long(userId)))
            .uniqueResult();

            // Map fields
            UserCountry userCountry = user.getUserCountry();
            UserCountry editedUserCountry = editedUser.getUserCountry();

            if (StringUtils.hasValue(editedUserCountry.getPhotoLargePath())){
                userCountry.setPhotoSmallPath(editedUserCountry.getPhotoSmallPath());
                userCountry.setPhotoLargePath(editedUserCountry.getPhotoLargePath());
            }
            userCountry.setPhotoDescription(editedUserCountry.getPhotoDescription());

            // Save
            user = saveUserCountryObject(userId, user); 

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return user;
    }
    
    private User saveUserCountryObject(String userId, User user)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession(); 
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();  
            session.update(user.getUserCountry());
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return user;
    
    }
    
    
    public List saveUserProfilePhotosList(String userId, List updatedPhotoList)
    {
        // Load from DB
        Session session = HibernateUtil.getSessionFactory().openSession();
        List dbPhotoList = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();   
            dbPhotoList = getUserPhotoList(userId, session); 

            // Map fields
            /*logger.debug("Comparing dbPhoto");
            if (dbPhotoList != null)
            {
                logger.debug("dbPhoto size " + dbPhotoList.size() );    
            }
            if (updatedPhotoList != null)
            {
                logger.debug("updatedPhotoList size " + updatedPhotoList.size() );    
            }*/
            dbPhotoList = ProfileMapper.updateUserProfilePhotos(dbPhotoList, updatedPhotoList); 
            // Save 
            for (int i=0; i<dbPhotoList.size();i++)
            {
                UserPictures photo = (UserPictures)dbPhotoList.get(i);
                //logger.debug("New photo is added type" + photo.getPhotoType());
                //logger.debug("New photo is added large" + photo.getPhotoLarge());
                //logger.debug("New photo is added user" + photo.getUserId());
                session.saveOrUpdate(photo); 
            }
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        return dbPhotoList; 
    
    }

    /*  User Lifestyle
     *
     */
    public UserDetails getUserLifestyleById(String userId) { 
    
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserDetails userDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();   
            userDetails = (UserDetails)session.createCriteria(UserDetails.class) 
            .add( Restrictions.eq("userId",new Long(userId)))
            .uniqueResult();
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userDetails;
    }

    /*  User Lifestyle
     *
     */
    public UserDetails saveUserProfileLifestyle(String userId, UserDetails editedUserDetails) {
      
        // Load from DB
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserDetails dbUserDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();    
            dbUserDetails = (UserDetails)session.createCriteria(UserDetails.class) 
            .add( Restrictions.eq("userId",new Long(userId)))
            .uniqueResult();

            // Map fields
            boolean newRecord = false;
            if (dbUserDetails == null)
            {
                dbUserDetails = new UserDetails();
                dbUserDetails.setUserId(Long.parseLong(userId)); 
                newRecord = true;
            }
            dbUserDetails = ProfileMapper.updateUserProfileLifestyle(dbUserDetails, editedUserDetails);  
            dbUserDetails.setUserCountry((getUserById(userId).getUserCountry()));
            // Save 
            if (newRecord)
            {
                session.save(dbUserDetails);
            }else
            { 
                session.saveOrUpdate(dbUserDetails);
            } 

       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return dbUserDetails;
    }
    
    
    /*  User Interest
     *
     */
    public UserDetails getUserInterestById(String userId) { 
    
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserDetails userDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();    
            userDetails = (UserDetails)session.createCriteria(UserDetails.class) 
            .add( Restrictions.eq("userId",new Long(userId)))
            .uniqueResult();
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        
        return userDetails;
    }

    /*  User Interest
     *
     */
    public UserDetails saveUserProfileInterest(String userId, UserDetails editedUserDetails) {
      
        // Load from DB
        Session session = HibernateUtil.getSessionFactory().openSession();
        UserDetails dbUserDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();    
            dbUserDetails = (UserDetails)session.createCriteria(UserDetails.class) 
            .add( Restrictions.eq("userId",new Long(userId)))
            .uniqueResult();

            // Map fields
            boolean newRecord = false;
            if (dbUserDetails == null)
            {
                dbUserDetails = new UserDetails();
                dbUserDetails.setUserId(Long.parseLong(userId));
                newRecord = true;
            }
            dbUserDetails = ProfileMapper.updateUserProfileInterest(dbUserDetails, editedUserDetails);  
            dbUserDetails.setUserCountry((getUserById(userId).getUserCountry()));
            // Save 
            if (newRecord)
            {
                session.save(dbUserDetails);
            }else
            { 
                session.saveOrUpdate(dbUserDetails);
            } 

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        
        return dbUserDetails;
    }
    
    
    /*  User Skills
     *
     */
    public UserDetails getUserSkillsById(String userId) { 
    
        Session session = HibernateUtil.getSessionFactory().openSession();
         UserDetails userDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();    
            userDetails = (UserDetails)session.createCriteria(UserDetails.class) 
            .add( Restrictions.eq("userId",new Long(userId)))
            .uniqueResult();
            
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userDetails;
    }

    /*  User Skills
     *
     */
    public UserDetails saveUserProfileSkills(String userId, UserDetails editedUserDetails) {
      
        // Load from DB
        Session session = HibernateUtil.getSessionFactory().openSession();
         UserDetails dbUserDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();    
        dbUserDetails = (UserDetails)session.createCriteria(UserDetails.class) 
        .add( Restrictions.eq("userId",new Long(userId)))
        .uniqueResult();
        
        // Map fields
        boolean newRecord = false;
        if (dbUserDetails == null)
        {
            dbUserDetails = new UserDetails();
            dbUserDetails.setUserId(Long.parseLong(userId));
            newRecord = true;
        }
        dbUserDetails = ProfileMapper.updateUserProfileSkills(dbUserDetails, editedUserDetails);   
        dbUserDetails.setUserCountry((getUserById(userId).getUserCountry()));
        // Save 
        if (newRecord)
        {
            session.save(dbUserDetails);
        }else
        { 
            session.saveOrUpdate(dbUserDetails);
        } 
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return dbUserDetails;
    }
    
    
    /*  User Contact
     *
     */
    public UserDetails getUserContactById(String userId) { 
    
        Session session = HibernateUtil.getSessionFactory().openSession();
         UserDetails userDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();     
        userDetails = (UserDetails)session.createCriteria(UserDetails.class) 
        .add( Restrictions.eq("userId",new Long(userId)))
        .uniqueResult();
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userDetails;
    }

    /*  User Contact
     *
     */
    public UserDetails saveUserProfileContact(String userId, UserDetails editedUserDetails) {
      
        // Load from DB
        Session session = HibernateUtil.getSessionFactory().openSession();
         UserDetails dbUserDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();     
        dbUserDetails = (UserDetails)session.createCriteria(UserDetails.class) 
        .add( Restrictions.eq("userId",new Long(userId)))
        .uniqueResult();
        
        // Map fields
        boolean newRecord = false;
        if (dbUserDetails == null)
        {
            dbUserDetails = new UserDetails();
            dbUserDetails.setUserId(Long.parseLong(userId));
            newRecord = true;
        }
        dbUserDetails = ProfileMapper.updateUserProfileContact(dbUserDetails, editedUserDetails);   
        dbUserDetails.setUserCountry((getUserById(userId).getUserCountry()));
        // Save 
        if (newRecord)
        {
            session.save(dbUserDetails);
        }else
        { 
            session.saveOrUpdate(dbUserDetails);
        } 
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return dbUserDetails;
    }
    
     
    /*  User Details for Profile Page
     *
     */
    public UserDetails getUserDetailsById(String userId) { 
    
        Session session = HibernateUtil.getSessionFactory().openSession();
         UserDetails userDetails = null;
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();     
        userDetails = (UserDetails)session.createCriteria(UserDetails.class) 
        .add( Restrictions.eq("userId",new Long(userId)))
        .uniqueResult();
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return userDetails;
    }

    public void logReportedAbuseUser(User user)
    { 
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();    
        UserCountry userCountry = user.getUserCountry();
        userCountry = (UserCountry)session.createCriteria(UserCountry.class) 
        .add( Restrictions.idEq( new Long(userCountry.getUserId())))
        .uniqueResult();
        
        userCountry.setReportAbuse(PropertyConstants.BOOLEAN_YES);
        session.save(userCountry);
        
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    
    }
    
    public void saveFlickerInfo(String userId, String yahooId, String flickrId)
    {
    
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();    
        UserCountry userCountry = null;
        userCountry = (UserCountry)session.createCriteria(UserCountry.class) 
        .add( Restrictions.idEq( new Long(userId)))
        .uniqueResult();
        
        if (userCountry != null)
        {
            if (StringUtils.hasValue(yahooId))
            {
                userCountry.setYahooId(yahooId);
            } 
            if (StringUtils.hasValue(flickrId))
            {
                userCountry.setFlickrId(flickrId);
            } 
            session.save(userCountry);
        }
        
       
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
    
    }
    
    public static void setEmailValidated(Long userId)
    {
         UserBO userBO = new UserBO();
         User user = userBO.getUserById(userId.toString());
         user.getUserCountry().setValidated(PropertyConstants.BOOLEAN_YES);
         userBO.saveUserCountryObject(userId.toString(), user);
         userBO = null;
    }
}
