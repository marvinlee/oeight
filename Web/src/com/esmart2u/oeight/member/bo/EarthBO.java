/*
 * EarthBO.java
 *
 * Created on May 12, 2008, 10:59 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.bo;

import com.esmart2u.oeight.data.OEightBlog;
import com.esmart2u.oeight.data.OEightPledge;
import com.esmart2u.oeight.member.web.struts.helper.HibernatePage;
import com.esmart2u.oeight.member.web.struts.helper.Page;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import java.util.List;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;

/**
 *
 * @author meauchyuan.lee
 */
public class EarthBO {
        
    private static Logger logger = Logger.getLogger(EarthBO.class);
    
    /** Creates a new instance of EarthBO */
    public EarthBO() {
    }
               
    public OEightPledge savePledge(OEightPledge pledge) {  
        return insertPledgeIntoDB(pledge);  
    }
    
    private OEightPledge insertPledgeIntoDB(OEightPledge pledge){ 
        
        if (pledge == null || pledge.getPledgeCode() < 1)
        {
            return pledge;
        }
        
        Session session = HibernateUtil.getSessionFactory().openSession();
         
        Transaction transaction = null;
        try {
        
            transaction = session.beginTransaction();
            pledge.setPledgeDate(new Date());
            session.save(pledge); 

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return pledge;
    }

    /** 
     *  Think of the caching! Important!!
     */
    public HibernatePage getPledgeList(int pageNumber) {  
        
        Session session = HibernateUtil.getSessionFactory().openSession();
          
        Transaction transaction = null;
        HibernatePage page  = null;
        try {
        
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(OEightPledge.class);        
            criteria.addOrder( Order.desc("pledgeId")) ;
            criteria.setCacheMode(CacheMode.NORMAL);
            criteria.setCacheable(true);

            page = HibernatePage.getHibernatePageInstance(criteria, pageNumber, 100); 
        
 
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        
        return page;
    }

    public OEightPledge getLatestPledgeFromThirdParty(int pledgeFrom, String thirdPartyId) {
          
        OEightPledge pledge = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(OEightPledge.class);  
            criteria.add(Expression.eq( "pledgeFrom", pledgeFrom ));
            criteria.add(Expression.eq( "pledgeFromUserId", new Long(thirdPartyId) ));  
            criteria.addOrder( Order.desc("pledgeDate")) ; 
            criteria.setMaxResults(1);
            List resultList = criteria.list();

            if (resultList != null && !resultList.isEmpty())
            {
                pledge = (OEightPledge)resultList.get(0);
            }

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        return pledge;
    }

    public void saveOEightPledge(OEightBlog oeightBlog) { 
         
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
            session.save(oeightBlog); 

            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
        
    }
}
