/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CustomerRelatedSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * {@TODO: Fill in the class description}
 *
 * @author  {@TODO: Fill in your full name}
 * @version $Revision: 1.8 $
 */

public class CustomerRelatedSearchBean  extends SearchBean
{
    private String applicationId;
    private String applicationCustomerId;

    public String getApplicationId()
    {
        return applicationId;
    }

    public void setApplicationId(String applicationId)
    {
        this.applicationId = applicationId;
    }

    public String getApplicationCustomerId()
    {
        return applicationCustomerId;
    }

    public void setApplicationCustomerId(String applicationCustomerId)
    {
        this.applicationCustomerId = applicationCustomerId;
    }
}

// end of CustomerRelatedSearchBean.java