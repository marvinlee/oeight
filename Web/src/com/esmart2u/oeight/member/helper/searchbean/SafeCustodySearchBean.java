/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// SafeCustodySearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Search bean for Safe Custody Listing.
 *
 * @author  Lee Meau Chyuan
 * @version $Revision: 1.8 $
 */

public class SafeCustodySearchBean extends SearchBean
{
    public static String SEARCH_BY_CUSTOMER_ID = "id";
    public static String SEARCH_BY_CUSTOMER_NAME = "nm";
    public static String SEARCH_BY_APPLICATION_REFERENCE = "no";
    public static String SEARCH_BY_BRANCH = "br";

    private String customerId;
    private String customerName;
    private String applicationReferenceNumber;
    private String branchCode;
    private String branchDescription;

    // searching current and total page
    private int incomingCurrentPage;
    private int incomingTotalPage;
    private int incomingTotalDocuments;
    private int availableCurrentPage;
    private int availableTotalPage;
    private int availableTotalDocuments;
    private int checkedOutCurrentPage;
    private int checkedOutTotalPage;
    private int checkedOutTotalDocuments;
    private int redeemedCurrentPage;
    private int redeemedTotalPage;
    private int redeemedTotalDocuments;
    private String documentId;

    public String getCustomerId()
    {
        return customerId;
    }

    public void setCustomerId(String customerId)
    {
        this.customerId = customerId;
    }

    public String getCustomerName()
    {
        return customerName;
    }

    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getApplicationReferenceNumber()
    {
        return applicationReferenceNumber;
    }

    public void setApplicationReferenceNumber(String applicationReferenceNumber)
    {
        this.applicationReferenceNumber = applicationReferenceNumber;
    }

    public String getBranchCode()
    {
        return branchCode;
    }

    public void setBranchCode(String branchCode)
    {
        this.branchCode = branchCode;
    }

    public String getBranchDescription()
    {
        return branchDescription;
    }

    public void setBranchDescription(String branchDescription)
    {
        this.branchDescription = branchDescription;
    }

    public int getIncomingCurrentPage()
    {
        return incomingCurrentPage;
    }

    public void setIncomingCurrentPage(int incomingCurrentPage)
    {
        this.incomingCurrentPage = incomingCurrentPage;
    }

    public int getIncomingTotalPage()
    {
        return incomingTotalPage;
    }

    public void setIncomingTotalPage(int incomingTotalPage)
    {
        this.incomingTotalPage = incomingTotalPage;
    }

    public int getIncomingTotalDocuments()
    {
        return incomingTotalDocuments;
    }

    public void setIncomingTotalDocuments(int incomingTotalDocuments)
    {
        this.incomingTotalDocuments = incomingTotalDocuments;
    }

    public int getAvailableCurrentPage()
    {
        return availableCurrentPage;
    }

    public void setAvailableCurrentPage(int availableCurrentPage)
    {
        this.availableCurrentPage = availableCurrentPage;
    }

    public int getAvailableTotalPage()
    {
        return availableTotalPage;
    }

    public void setAvailableTotalPage(int availableTotalPage)
    {
        this.availableTotalPage = availableTotalPage;
    }

    public int getAvailableTotalDocuments()
    {
        return availableTotalDocuments;
    }

    public void setAvailableTotalDocuments(int availableTotalDocuments)
    {
        this.availableTotalDocuments = availableTotalDocuments;
    }

    public int getCheckedOutCurrentPage()
    {
        return checkedOutCurrentPage;
    }

    public void setCheckedOutCurrentPage(int checkedOutCurrentPage)
    {
        this.checkedOutCurrentPage = checkedOutCurrentPage;
    }

    public int getCheckedOutTotalPage()
    {
        return checkedOutTotalPage;
    }

    public void setCheckedOutTotalPage(int checkedOutTotalPage)
    {
        this.checkedOutTotalPage = checkedOutTotalPage;
    }

    public int getCheckedOutTotalDocuments()
    {
        return checkedOutTotalDocuments;
    }

    public void setCheckedOutTotalDocuments(int checkedOutTotalDocuments)
    {
        this.checkedOutTotalDocuments = checkedOutTotalDocuments;
    }

    public int getRedeemedCurrentPage()
    {
        return redeemedCurrentPage;
    }

    public void setRedeemedCurrentPage(int redeemedCurrentPage)
    {
        this.redeemedCurrentPage = redeemedCurrentPage;
    }

    public int getRedeemedTotalPage()
    {
        return redeemedTotalPage;
    }

    public void setRedeemedTotalPage(int redeemedTotalPage)
    {
        this.redeemedTotalPage = redeemedTotalPage;
    }

    public int getRedeemedTotalDocuments()
    {
        return redeemedTotalDocuments;
    }

    public void setRedeemedTotalDocuments(int redeemedTotalDocuments)
    {
        this.redeemedTotalDocuments = redeemedTotalDocuments;
    }

    public String getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(String documentId)
    {
        this.documentId = documentId;
    }

    /**
     * Pre-validate paging parameter before searching.
     */
    public void beforeSearch()
    {
        if (incomingCurrentPage < 1)
        {
            incomingCurrentPage = 1;
        }
        if (availableCurrentPage < 1)
        {
            availableCurrentPage = 1;
        }
        if (checkedOutCurrentPage < 1)
        {
            checkedOutCurrentPage = 1;
        }
        if (redeemedCurrentPage < 1)
        {
            redeemedCurrentPage = 1;
        }
    }

    /**
     * Post-validate paging parameter after searching.
     */
    public void afterSearch()
    {
        // checking to avoid invalid page number shown on screen; if current page requested is
        // greater than total pages available, set current page count to total page available
        if (incomingCurrentPage > incomingTotalPage)
        {
            incomingCurrentPage = incomingTotalPage;
        }
        if (availableCurrentPage > availableTotalPage)
        {
            availableCurrentPage = availableTotalPage;
        }
        if (checkedOutCurrentPage > checkedOutTotalPage)
        {
            checkedOutCurrentPage = checkedOutTotalPage;
        }
        if (redeemedCurrentPage > redeemedTotalPage)
        {
            redeemedCurrentPage = redeemedTotalPage;
        }
    }

}

// end of SafeCustodySearchBean.java