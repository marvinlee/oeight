/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ApplicationTypeSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Search Bean for Application Type child.
 *
 * @author  Lee Meau Chyuan
 * @version $Revision: 1.8 $
 */

public class ApplicationTypeSearchBean extends SearchBean
{
    private String workflowStatusCode;

    public String getWorkflowStatusCode()
    {
        return workflowStatusCode;
    }

    public void setWorkflowStatusCode(String workflowStatusCode)
    {
        this.workflowStatusCode = workflowStatusCode;
    }
}

// end of ApplicationTypeSearchBean.java