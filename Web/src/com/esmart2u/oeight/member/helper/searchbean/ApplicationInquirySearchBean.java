/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ApplicationInquirySearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

import java.io.Serializable;


/**
 * Search bean for application inquiry module.
 *
 * @author  Chai Kean Hong
 * @version $Revision: 1.8 $
 */

public class ApplicationInquirySearchBean extends SearchBean
        implements Serializable
{
    public static final String SEARCH_BY_REF_NO = "ref_no";
    public static final String SEARCH_BY_ID     = "id";
    public static final String SEARCH_BY_NAME   = "nm";

    private String originationBranchCode;
    private String originationBranchDescription;

    private String criteria = SEARCH_BY_NAME;

    public String getOriginationBranchCode()
    {
        return originationBranchCode;
    }

    public void setOriginationBranchCode(String originationBranchCode)
    {
        this.originationBranchCode = originationBranchCode;
    }

    public String getOriginationBranchDescription()
    {
        return originationBranchDescription;
    }

    public void setOriginationBranchDescription(String originationBranchDescription)
    {
        this.originationBranchDescription = originationBranchDescription;
    }

    public String getCriteria()
    {
        return criteria;
    }

    public void setCriteria(String criteria)
    {
        this.criteria = criteria;
        if(criteria==null || criteria.trim().equals(""))
            criteria = SEARCH_BY_NAME;
    }

}

// end of ApplicationInquirySearchBean.java