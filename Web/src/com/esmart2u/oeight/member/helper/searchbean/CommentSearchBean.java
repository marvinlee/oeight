/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CommentSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Search Bean for Application Comment Module
 *
 * @author  Chow Chun Keong
 * @version $Revision: 1.8 $
 */

public class CommentSearchBean extends SearchBean
{
    private String applicationId;
    private String userId;
    private String workflowStatusCode;
    private String workflowStatusDescription;

    public static String SEARCH_BY_APP_ID = "cs_app_id";

//    public CommentSearchBean()
//    {
//        this.applicationId = "1234567890";
//        this.userId = "ckchow";
//        this.workflowStatusCode = "1";
//        this.workflowStatusDescription = "Created";
//        setCriteria("applicationId");
//        setValue("1234567890");
//    }

    public String getApplicationId()
    {
        return applicationId;
    }

    public void setApplicationId(String applicationId)
    {
        this.applicationId = applicationId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getWorkflowStatusCode()
    {
        return workflowStatusCode;
    }

    public void setWorkflowStatusCode(String workflowStatusCode)
    {
        this.workflowStatusCode = workflowStatusCode;
    }

    public String getWorkflowStatusDescription()
    {
        return workflowStatusDescription;
    }

    public void setWorkflowStatusDescription(String workflowStatusDescription)
    {
        this.workflowStatusDescription = workflowStatusDescription;
    }
}

// end of CommentSearchBean.java