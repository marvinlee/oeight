/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// SafeCustodySearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import java.util.List;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Search bean for Facility and Facility Collateral Link Listing.
 *
 * @author  Lee Meau Chyuan
 * @version $Revision: 1.8.16.1 $
 */

public class FacilitySearchBean extends SearchBean
{
    // searching current and total page
    private int existingFacilityCurrentPage;
    private int existingFacilityTotalPage;
    private int linkCurrentPage;
    private int linkTotalPage;
    private int linkTotalDocuments;
    private List existingFacilityList;

    public int getExistingFacilityCurrentPage()
    {
        return existingFacilityCurrentPage;
    }

    public void setExistingFacilityCurrentPage(int existingFacilityCurrentPage)
    {
        this.existingFacilityCurrentPage = existingFacilityCurrentPage;
    }

    public int getExistingFacilityTotalPage()
    {
        return existingFacilityTotalPage;
    }

    public void setExistingFacilityTotalPage(int existingFacilityTotalPage)
    {
        this.existingFacilityTotalPage = existingFacilityTotalPage;
    }

    public int getLinkCurrentPage()
    {
        return linkCurrentPage;
    }

    public void setLinkCurrentPage(int linkCurrentPage)
    {
        this.linkCurrentPage = linkCurrentPage;
    }

    public int getLinkTotalPage()
    {
        return linkTotalPage;
    }

    public void setLinkTotalPage(int linkTotalPage)
    {
        this.linkTotalPage = linkTotalPage;
    }

    public int getLinkTotalDocuments()
    {
        return linkTotalDocuments;
    }

    public void setLinkTotalDocuments(int linkTotalDocuments)
    {
        this.linkTotalDocuments = linkTotalDocuments;
    }

    public List getExistingFacilityList()
    {
        return existingFacilityList;
    }

    public void setExistingFacilityList(List existingFacilityList)
    {
        this.existingFacilityList = existingFacilityList;
    }

    /**
     * Pre-validate paging parameter before searching.
     */
    public void beforeSearch()
    {
        super.beforeSearch();
        if (linkCurrentPage < 1)
        {
            linkCurrentPage = 1;
        }
        if (existingFacilityCurrentPage < 1)
        {
            existingFacilityCurrentPage = 1;
        }
    }

    /**
     * Post-validate paging parameter after searching.
     */
    public void afterSearch()
    {
        // checking to avoid invalid page number shown on screen; if current page requested is
        // greater than total pages available, set current page count to total page available
        super.afterSearch();
        if (linkCurrentPage > linkTotalPage)
        {
            linkCurrentPage = linkTotalPage;
        }
        if (existingFacilityCurrentPage > existingFacilityTotalPage)
        {
            existingFacilityCurrentPage = existingFacilityTotalPage;
        }
    }

}

// end of SafeCustodySearchBean.java