/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ReportSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * {@TODO: Fill in the class description}
 *
 * @author  Chow Chun Keong
 * @version $Revision: 1.9 $
 */

public class ReportSearchBean extends SearchBean
{
    private String userId;
    private String branchCode;


    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getBranchCode()
    {
        return branchCode;
    }

    public void setBranchCode(String branchCode)
    {
        this.branchCode = branchCode;
    }
}

// end of ReportSearchBean.java