/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// TaskReroutingSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

import java.sql.Date;

/**
 * Task rerouting search bean
 *
 * @author  Gan Kiat Kin
 * @version $Revision: 1.8 $
 */

public class TaskReroutingSearchBean extends SearchBean
{
    private String workflowCode;
    private String applicationStatusCode;
    private String applicationTypeCode;
    private String participantId;
    private String participantName;
    private Date startDate;
    private Date endDate;
    private int routingMode;
    private int recordPerPage;


    public String getWorkflowCode()
    {
        return workflowCode;
    }

    public void setWorkflowCode(String workflowCode)
    {
        this.workflowCode = workflowCode;
    }

    public String getApplicationStatusCode()
    {
        return applicationStatusCode;
    }

    public void setApplicationStatusCode(String applicationStatusCode)
    {
        this.applicationStatusCode = applicationStatusCode;
    }

    public String getApplicationTypeCode()
    {
        return applicationTypeCode;
    }

    public void setApplicationTypeCode(String applicationTypeCode)
    {
        this.applicationTypeCode = applicationTypeCode;
    }

    public String getParticipantId()
    {
        return participantId;
    }

    public void setParticipantId(String participantId)
    {
        this.participantId = participantId;
    }

    public String getParticipantName()
    {
        return participantName;
    }

    public void setParticipantName(String participantName)
    {
        this.participantName = participantName;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public int getRoutingMode()
    {
        return routingMode;
    }

    public void setRoutingMode(int routingMode)
    {
        this.routingMode = routingMode;
    }

    public int getRecordPerPage()
    {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage)
    {
        this.recordPerPage = recordPerPage;
    }
}

// end of TaskReroutingSearchBean.java