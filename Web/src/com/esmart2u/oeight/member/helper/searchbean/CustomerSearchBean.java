/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CifSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;


/**
 * Customized search bean for customer.
 *
 * @author  Goh Siew Chyn
 * @version $Revision: 1.8 $
 */

public class CustomerSearchBean extends SearchBean
{
    public static String SEARCH_BY_ID = "id";
    public static String SEARCH_BY_NAME = "nm";
    public static String SEARCH_BY_CUSTOMER_NO = "no";
    private String branchCode;
    private String branchDescription;

    public String getBranchCode()
    {
        return branchCode;
    }

    public void setBranchCode(String branchCode)
    {
        this.branchCode = branchCode;
    }

    public String getBranchDescription()
    {
        return branchDescription;
    }

    public void setBranchDescription(String branchDescription)
    {
        this.branchDescription = branchDescription;
    }
}

// end of CifSearchBean.java