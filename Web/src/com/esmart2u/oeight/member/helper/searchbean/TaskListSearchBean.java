/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// TaskListSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import java.util.List;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Task list Search Bean
 *
 * @author  Gan Kiat Kin
 * @version $Revision: 1.8.16.1 $
 */

public class TaskListSearchBean extends SearchBean
{
    public static final String SEARCH_BY_REFERENCE_NUMBER = "referenceNumber";
    public static final String SEARCH_BY_CUSTOMER_NAME = "customerName";

    private String applicationTypeCode;
    private String applicationStatusCode;
    private String sortingField;
    private String ascendingSortingFlag;

    // searching current and total page
    private int poolListCurrentPage;
    private int poolListTotalPage;

    private List poolList;

    public String getApplicationTypeCode()
    {
        return applicationTypeCode;
    }

    public void setApplicationTypeCode(String applicationTypeCode)
    {
        this.applicationTypeCode = applicationTypeCode;
    }

    public String getApplicationStatusCode()
    {
        return applicationStatusCode;
    }

    public void setApplicationStatusCode(String applicationStatusCode)
    {
        this.applicationStatusCode = applicationStatusCode;
    }

    public String getSortingField()
    {
        return sortingField;
    }

    public void setSortingField(String sortingField)
    {
        this.sortingField = sortingField;
    }

    public String getAscendingSortingFlag()
    {
        return ascendingSortingFlag;
    }

    public void setAscendingSortingFlag(String ascendingSortingFlag)
    {
        this.ascendingSortingFlag = ascendingSortingFlag;
    }

    public int getPoolListCurrentPage()
    {
        return poolListCurrentPage;
    }

    public void setPoolListCurrentPage(int poolListCurrentPage)
    {
        this.poolListCurrentPage = poolListCurrentPage;
    }

    public int getPoolListTotalPage()
    {
        return poolListTotalPage;
    }

    public void setPoolListTotalPage(int poolListTotalPage)
    {
        this.poolListTotalPage = poolListTotalPage;
    }

    public List getPoolList()
    {
        return poolList;
    }

    public void setPoolList(List poolList)
    {
        this.poolList = poolList;
    }

    public void beforeSearch()
    {
        if (this.getCurrentPage() < 1)
        {
            this.setCurrentPage(1);
        }
        if (poolListCurrentPage < 1)
        {
            poolListCurrentPage = 1;
        }
    }

    public void afterSearch()
    {
        // checking to avoid invalid page number shown on screen; if current page requested is
        // greater than total pages available, set current page count to total page available
        if (this.getCurrentPage() > this.getTotalPage())
        {
            this.setCurrentPage(this.getTotalPage());
        }
        if (poolListCurrentPage > poolListTotalPage)
        {
            poolListCurrentPage = poolListTotalPage;
        }
    }
}

// end of TaskListSearchBean.java