/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// AppealLoanSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Appeal Loan Search Bean
 *
 * @author  Gan Kiat Kin
 * @version $Revision: 1.8 $
 */

public class AppealLoanSearchBean extends SearchBean
{
    public static final String SEARCH_BY_CUSTOMER_NAME = "customerName";
    public static final String SEARCH_BY_REFERENCE_NUMBER = "referenceNumber";
    public static final String SEARCH_BY_CUSTOMER_ID_NUMBER = "customerIdNumber";

}

// end of AppealLoanSearchBean.java