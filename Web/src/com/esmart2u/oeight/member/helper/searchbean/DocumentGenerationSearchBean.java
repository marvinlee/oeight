/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CommentSearchBean.java

package com.esmart2u.oeight.member.helper.searchbean;

import java.sql.SQLException;
import java.util.*;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Search Bean for Application Comment Module
 *
 * @author  Chow Chun Keong
 * @version $Revision: 1.13.2.1 $
 */

public class DocumentGenerationSearchBean extends SearchBean
{
    //  For extra searching criterias
    private String applicationId;
    private String userId;
    private String user;
    private String userExtNo;
    private String workflowStatusCode;
    private String applicationTypeCode;

    //  For multi paging
    private List documentGenerationSavedList;
    private List documentGenerationPrintedList;
    private List documentGenerationList;
    private int savedListCurrentPage = 1;
    private int printedListCurrentPage = 1;
    private int savedListTotalPage = 1;
    private int printedListTotalPage = 1;
    private int documentListCurrentPage = 1;
    private int documentListTotalPage = 1;

    //  For document generation
    private StringBuffer generatedLetter = new StringBuffer("");
    private String letterTemplateCode;
    private String letterTemplateFilePath;
    private String letterTemplateAlternateDescription;
    private String letterTemplateRelatedType;
    private String currentRelatedType;
    //  extra
    //  Use to render save, duplicate and print buttons
    private boolean saveAction;
    private boolean generated;
    private Locale locale;

    private Hashtable htRelatedTypeWithCriteria = new Hashtable();
    private Hashtable htRelatedTypeWithSubType = new Hashtable();
    private List criteriaList = new ArrayList(8);
    private int intInterceptionCounter = 0;

    //  Use to hold related type list eg. CIF Listing, FAC Listing, COLL Listing, ...
    private List documentGenerationRelatedTypeList;

//    public DocumentGenerationSearchBean()
//    {
//        this.applicationId = "1234567890";
//        this.userId = "ckchow";
//        this.workflowStatusCode = "1";
//        this.workflowStatusDescription = "Created";
//    }

    public boolean keyExistsInType(String strKey)
    {
        return htRelatedTypeWithCriteria.containsKey(strKey);
    }

    public boolean keyExistsInSubType(String strKey)
    {
        return htRelatedTypeWithSubType.containsKey(strKey);
    }

    public String getApplicationId()
    {
        return applicationId;
    }

    public void setApplicationId(String applicationId)
    {
        this.applicationId = applicationId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUserExtNo()
    {
        return userExtNo;
    }

    public void setUserExtNo(String userExtNo)
    {
        this.userExtNo = userExtNo;
    }

    public String getWorkflowStatusCode()
    {
        return workflowStatusCode;
    }

    public void setWorkflowStatusCode(String workflowStatusCode)
    {
        this.workflowStatusCode = workflowStatusCode;
    }

    public String getApplicationTypeCode() {
        return applicationTypeCode;
    }

    public void setApplicationTypeCode(String applicationTypeCode) {
        this.applicationTypeCode = applicationTypeCode;
    }

    public List getDocumentGenerationSavedList()
    {
        return documentGenerationSavedList;
    }

    public void setDocumentGenerationSavedList(List documentGenerationSavedList)
    {
        this.documentGenerationSavedList = documentGenerationSavedList;
    }

    public List getDocumentGenerationPrintedList()
    {
        return documentGenerationPrintedList;
    }

    public void setDocumentGenerationPrintedList(List documentGenerationPrintedList)
    {
        this.documentGenerationPrintedList = documentGenerationPrintedList;
    }

    public List getDocumentGenerationList() {
        return documentGenerationList;
    }

    public void setDocumentGenerationList(List documentGenerationList) {
        this.documentGenerationList = documentGenerationList;
    }

    public int getSavedListCurrentPage()
    {
        return savedListCurrentPage;
    }

    public void setSavedListCurrentPage(int savedListCurrentPage)
    {
        this.savedListCurrentPage = savedListCurrentPage;
    }

    public int getPrintedListCurrentPage()
    {
        return printedListCurrentPage;
    }

    public void setPrintedListCurrentPage(int printedListCurrentPage)
    {
        this.printedListCurrentPage = printedListCurrentPage;
    }

    public int getSavedListTotalPage()
    {
        return savedListTotalPage;
    }

    public void setSavedListTotalPage(int savedListTotalPage)
    {
        this.savedListTotalPage = savedListTotalPage;
    }

    public int getPrintedListTotalPage()
    {
        return printedListTotalPage;
    }

    public void setPrintedListTotalPage(int printedListTotalPage)
    {
        this.printedListTotalPage = printedListTotalPage;
    }

    public int getDocumentListCurrentPage() {
        return documentListCurrentPage;
    }

    public void setDocumentListCurrentPage(int documentListCurrentPage) {
        this.documentListCurrentPage = documentListCurrentPage;
    }

    public int getDocumentListTotalPage() {
        return documentListTotalPage;
    }

    public void setDocumentListTotalPage(int documentListTotalPage) {
        this.documentListTotalPage = documentListTotalPage;
    }

    public String getGeneratedLetter()
    {
        return generatedLetter.toString();
    }

    public void setGeneratedLetter(String generatedLetter)
    {
        this.generatedLetter.replace(0, this.generatedLetter.length(), generatedLetter);
    }

    public String getLetterTemplateCode()
    {
        return letterTemplateCode;
    }

    public void setLetterTemplateCode(String letterTemplateCode)
    {
        this.letterTemplateCode = letterTemplateCode;
    }

    public String getLetterTemplateFilePath()
    {
        return letterTemplateFilePath;
    }

    public void setLetterTemplateFilePath(String letterTemplateFilePath)
    {
        this.letterTemplateFilePath = letterTemplateFilePath;
    }

    public String getLetterTemplateAlternateDescription()
    {
        return letterTemplateAlternateDescription;
    }

    public void setLetterTemplateAlternateDescription(String letterTemplateAlternateDescription)
    {
        this.letterTemplateAlternateDescription = letterTemplateAlternateDescription;
    }

    public boolean isSaveAction()
    {
        return saveAction;
    }

    public void setSaveAction(boolean saveAction)
    {
        this.saveAction = saveAction;
    }

    public boolean isGenerated()
    {
        return generated;
    }

    public void setGenerated(boolean generated)
    {
        this.generated = generated;
    }

    public String getLetterTemplateRelatedType()
    {
        return letterTemplateRelatedType;
    }

    public void setLetterTemplateRelatedType(String letterTemplateRelatedType)
    {
        this.letterTemplateRelatedType = letterTemplateRelatedType;
    }

    public List getDocumentGenerationRelatedTypeList()
    {
        return documentGenerationRelatedTypeList;
    }

    public void setDocumentGenerationRelatedTypeList(List documentGenerationRelatedTypeList)
    {
        this.documentGenerationRelatedTypeList = documentGenerationRelatedTypeList;
    }

    public String getCurrentRelatedType()
    {
        return currentRelatedType;
    }

    public void setCurrentRelatedType(String currentRelatedType)
    {
        this.currentRelatedType = currentRelatedType;
    }

    public Locale getLocale()
    {
        return locale;
    }

    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }

    public String getLetterTemplateRelatedTypeCriteria(String strKey)
    {
        if (keyExistsInType(strKey))
            return String.valueOf(htRelatedTypeWithCriteria.get(strKey));

        return "";
    }

    public String getLetterTemplateRelatedSubType(String strKey)
    {
        if (keyExistsInSubType(strKey))
            return String.valueOf(htRelatedTypeWithSubType.get(strKey));

        return "";
    }

    public Hashtable getHtRelatedTypeWithCriteria()
    {
        return htRelatedTypeWithCriteria;
    }

    public void setHtRelatedTypeWithCriteria(Hashtable htRelatedTypeWithCriteria)
    {
        this.htRelatedTypeWithCriteria = htRelatedTypeWithCriteria;
    }

    public Hashtable getHtRelatedTypeWithSubType()
    {
        return htRelatedTypeWithSubType;
    }

    public void setHtRelatedTypeWithSubType(Hashtable htRelatedTypeWithSubType)
    {
        this.htRelatedTypeWithSubType = htRelatedTypeWithSubType;
    }

    public List getCriteriaList()
    {
        return criteriaList;
    }

    public void setCriteriaList(List criteriaList)
    {
        this.criteriaList = criteriaList;
    }

    public int getIntInterceptionCounter()
    {
        return intInterceptionCounter;
    }

    public void setIntInterceptionCounter(int intInterceptionCounter)
    {
        this.intInterceptionCounter = intInterceptionCounter;
    }
    /**
     * Stores new version of the document in target document format.
     *
     * @param docObj The content of the document.
     * @param serialNo The serial number of the document.
     * @param docAccess The document access type.
     * @throws SQLException This is a system exception thrown when any one of the following problems are encountered:
     * <li>Failure in database connection.</li>
     * <li>Error in FileInputStream.</li>
     * <li>Error reading BLOB object.</li>
     * <li>Error in closing FileOutputStream or FileInputStream.</li>
     * <li>Error in closing Connection or PreparedStatement.</li>
     * <li>Invalid serial no.</li>
     */
//    public void storeDocument(Object docObj, String serialNo, int docAccess)
//        throws SQLException
//    {
//        Connection conn = null;
//        PreparedStatement stmt = null;
//        ResultSet rs = null;
//        InputStream fin = null;
//        OutputStream fout = null;
//        OutputStream fout2 = null;
//        int versionNo = 001;
//        String docType = "L001";
//        String docSts = "A";
//        String docAccessField = "";
//        String serialNo1 = "1001";
//        String importpath = "d:/scgoh/may/mcl706_doc_1.doc";
//
//        if (docAccess == Document.DOC_ACCESS_EDITABLE)
//        {
//            docAccessField = "DOC_FILE_ED";
//        }
//        else if (docAccess == Document.DOC_ACCESS_READONLY)
//        {
//            docAccessField = "DOC_FILE_RO";
//        }
//
//        try
//        {
//            conn = JDBCConnector.getConnection();
//            conn.setAutoCommit(false);
//            stmt = conn.prepareStatement("INSERT INTO TBL_DOCS (DOC_SERIAL_NO,DOC_TYPE_ID,VERSION_NO,VER_STATUS,DOC_FILE_ED,DOC_FILE_RO) VALUES (?,?,?,?,empty_blob(),empty_blob())");
//            stmt.setString(1, serialNo1);
//            stmt.setString(2, docType);
//            stmt.setInt(3, versionNo);
//            stmt.setString(4, docSts);
//            stmt.execute();
//            stmt.clearParameters();
//            stmt = conn.prepareStatement("SELECT DOC_FILE_ED,DOC_FILE_RO FROM TBL_DOCS WHERE DOC_SERIAL_NO = ? FOR UPDATE");
//            stmt.setString(1, serialNo1);
//            rs = stmt.executeQuery();
//        }
//        catch (Exception ex)
//        {
//            logger.error("Error with the SQL or Connection" + ex);
//            throw new SQLException("Error with the SQL Statement or JDBC Connection.");
//        }
//
//        if (rs.next())
//        {
//            BLOB blob = ((oracle.jdbc.driver.OracleResultSet)rs).getBLOB("DOC_FILE_ED");
//            BLOB blob2 = ((oracle.jdbc.driver.OracleResultSet)rs).getBLOB("DOC_FILE_RO");
//// Blob blob = rs.getBlob("data");
//            fout = blob.getBinaryOutputStream();
//            fout2 = blob2.getBinaryOutputStream();
//            File f = new File(importpath);
//
//            try
//            {
//                fin = new FileInputStream(f);
//            }
//            catch (IOException ioex)
//            {
//                throw new SQLException("Error in FileInputStream");
//            }
//
//            byte[] buffer = new byte[blob.getBufferSize()];
//            int bytesRead = 0;
//            try
//            {
//                while ((bytesRead = fin.read(buffer)) != -1)
//                {
//                    fout.write(buffer, 0, bytesRead);
//                }
//            }
//            catch (IOException ioex)
//            {
//                throw new SQLException("Error reading BLOB object");
//            }
//
//            byte[] buffer2 = new byte[blob2.getBufferSize()];
//            int bytesRead2 = 0;
//            try
//            {
//                while ((bytesRead2 = fin.read(buffer2)) != -1)
//                {
//                    fout2.write(buffer2, 0, bytesRead2);
//                }
//            }
//            catch (IOException ioex)
//            {
//                throw new SQLException("Error reading BLOB object");
//            }
//
//            blob = null;
//            blob2 = null;
//            f = null;
//            buffer = null;
//            buffer2 = null;
//        }
//        else
//        {
//            throw new InvalidDocumentException(serialNo1);
//        }
//
//        try
//        {
//            fin.close();
//            fout.close();
//            fout2.close();
//        }
//        catch (IOException ioex)
//        {
//            throw new SQLException("Error in closing FileOutputStream or FileInputStream");
//        }
//        finally
//        {
//            try
//            {
//                stmt.clearParameters();
//                conn.commit();
//                stmt.close();
//                conn.close();
//                fin = null;
//                fout = null;
//                rs = null;
//                conn = null;
//                stmt = null;
//            }
//            catch (SQLException s)
//            {
//                throw new SQLException("Error in: Closing Connection or Closing PreparedStatement");
//            }
//        }
//    }

}

// end of CommentSearchBean.java