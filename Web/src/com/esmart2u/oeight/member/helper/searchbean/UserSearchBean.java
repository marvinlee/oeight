package com.esmart2u.oeight.member.helper.searchbean;

import java.util.List;
import java.sql.Date;

import com.esmart2u.solution.base.helper.SearchBean;

/**
 * Customized search bean for user maintenance.
 *
 * @author ericlaw
 * @version $Revision: 1.2 $
 */
public class UserSearchBean  extends SearchBean
{
    public static String SEARCH_BY_ID = "id";
    public static String SEARCH_BY_NAME   = "nm";
    public static String SEARCH_BY_BRANCH_CODE   = "branchCode";

    private String id;
    private String name;
    private String branchCode;
    private String branchDescription;
    private int currentHistPage;
    private List absentHistList;
    private List absentList;
    private int lastPage;
    private boolean hasMorePage;
    private int lastHistPage;
    private boolean hasMoreHistPage;

    private int minPasswordLength;
    private int maxPasswordLength;
    private int passwordValidDays;
    private Date newPasswordExpireDate;
    private Date datePasswordUpdated;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchDescription()
    {
        return branchDescription;
    }

    public void setBranchDescription(String branchDescription)
    {
        this.branchDescription = branchDescription;
    }

    public int getCurrentHistPage() {
        return currentHistPage;
    }

    public void setCurrentHistPage(int currentHistPage) {
        this.currentHistPage = currentHistPage;
    }

    public List getAbsentHistList() {
        return absentHistList;
    }

    public void setAbsentHistList(List absentHistList) {
        this.absentHistList = absentHistList;
    }

    public List getAbsentList() {
        return absentList;
    }

    public void setAbsentList(List absentList) {
        this.absentList = absentList;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public boolean isHasMorePage() {
        return hasMorePage;
    }

    public void setHasMorePage(boolean hasMorePage) {
        this.hasMorePage = hasMorePage;
    }

    public int getLastHistPage() {
        return lastHistPage;
    }

    public void setLastHistPage(int lastHistPage) {
        this.lastHistPage = lastHistPage;
    }

    public boolean isHasMoreHistPage() {
        return hasMoreHistPage;
    }

    public void setHasMoreHistPage(boolean hasMoreHistPage) {
        this.hasMoreHistPage = hasMoreHistPage;
    }

    public int getMinPasswordLength() {
        return minPasswordLength;
    }

    public void setMinPasswordLength(int minPasswordLength) {
        this.minPasswordLength = minPasswordLength;
    }

    public int getMaxPasswordLength() {
        return maxPasswordLength;
    }

    public void setMaxPasswordLength(int maxPasswordLength) {
        this.maxPasswordLength = maxPasswordLength;
    }

    public int getPasswordValidDays() {
        return passwordValidDays;
    }

    public void setPasswordValidDays(int passwordValidDays) {
        this.passwordValidDays = passwordValidDays;
    }

    public Date getNewPasswordExpireDate() {
        return newPasswordExpireDate;
    }

    public void setNewPasswordExpireDate(Date newPasswordExpireDate) {
        this.newPasswordExpireDate = newPasswordExpireDate;
    }

    public Date getDatePasswordUpdated() {
        return datePasswordUpdated;
    }

    public void setDatePasswordUpdated(Date datePasswordUpdated) {
        this.datePasswordUpdated = datePasswordUpdated;
    }
}
