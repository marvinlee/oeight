/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CustomerConstants.java

package com.esmart2u.oeight.member.helper;

/**
 * Customer constants
 *
 * @author  Goh Siew Chyn
 * @version $Revision: 1.8.16.3 $
 */

public interface CustomerConstants
{
    //tbl_mt_addr_typ
    public static final String OFFICE_ADDRESS_TYPE_CODE = "O";
    public static final String LEGAL_ADDRESS_TYPE_CODE = "L";
    public static final String MAILING_ADDRESS_TYPE_CODE = "M";

    //tbl_mt_cif_typ
    public static final String INDIVIDUAL_CUSTOMER_TYPE_CODE = "I";
    public static final String CORPORATE_CUSTOMER_TYPE_CODE = "C";

    //tbl_mt_cif_sts
    public static final String NEW_CUSTOMER_STATUS = "N";
    public static final String EXISTING_CUSTOMER_STATUS = "E";

    //tbl_mt_inc_typ
    public static final String INDIVIDUAL_INCOME_TYPE_CODE = "I";
    public static final String SELF_BUSINESS_INCOME_TYPE_CODE = "S";

    //tbl_mt_emplymt_typ
    //updated by wwtham at 20050316 (add zero to code below to sync wif db)
    public static final String EMPLOYED_PERMANENT_CODE = "01";
    public static final String SELF_EMPLOYED_CODE = "02";

    //tbl_mt_inc_cat
    //Append by wwtham on 20050321 
    public static final String SALARY_INCOME_CATEGORY_CODE = "01";
    public static final String ALLOWANCE_INCOME_CATEGORY_CODE = "02";

    // house hold
    public static final String HOUSE_HOLD_EXPENSES_INCOME_CATEGORY_CODE = "03";
    public static final String OTHER_EXPENSES_HOUSE_HOLD_INCOME_CATEGORY_CODE = "04";
    public static final String LOAN_PAYMENT_WITH_SALARY_DEDUCTION_INCOME_CATEGORY_CODE = "05";
    public static final String LOAN_PAYMENT_WITHOUT_SALARY_DEDUCTION_INCOME_CATEGORY_CODE = "06";
    // other income
    public static final String INTEREST_INCOME_CATEGORY_CODE = "07";
    public static final String BONUS_INCOME_CATEGORY_CODE = "08";
    public static final String OVERTIME_INCOME_CATEGORY_CODE = "09";
    public static final String RENTAL_INCOME_CATEGORY_CODE = "10";
    public static final String DIVIDEND_INCOME_CATEGORY_CODE = "11";
    public static final String COMMISION_INCOME_CATEGORY_CODE = "12";
    public static final String OTHER_INCOME_CATEGORY_CODE = "13";
    // self business
    public static final String REVENUE_INCOME_CATEGORY_CODE = "14";
    public static final String BUSINESS_COST_INCOME_CATEGORY_CODE = "15";
    public static final String OTHER_EXPENSES_SELF_BUSINESS_INCOME_CATEGORY_CODE = "16";
    public static final String INTEREST_PAID_INCOME_CATEGORY_CODE = "17";
    public static final String INCOME_TAX_INCOME_CATEGORY_CODE = "18";
    // Net Salary
    public static final String JAN_INCOME_CATEGORY_CODE = "19";
    public static final String FEB_TAX_INCOME_CATEGORY_CODE = "20";
    public static final String MARCH_TAX_INCOME_CATEGORY_CODE = "21";
    public static final String APRIL_TAX_INCOME_CATEGORY_CODE = "22";
    public static final String MAY_TAX_INCOME_CATEGORY_CODE = "23";
    public static final String JUNE_TAX_INCOME_CATEGORY_CODE = "24";
    public static final String JULY_TAX_INCOME_CATEGORY_CODE = "25";
    public static final String AUG_TAX_INCOME_CATEGORY_CODE = "26";
    public static final String SEP_TAX_INCOME_CATEGORY_CODE = "27";
    public static final String OCT_TAX_INCOME_CATEGORY_CODE = "28";
    public static final String NOV_TAX_INCOME_CATEGORY_CODE = "29";
    public static final String DEC_TAX_INCOME_CATEGORY_CODE = "30";

    // leave reason code
    //tbl_mt_rsn_typ
    //move from mcif customerconstant.java
    public static final String LEAVING_REASON_TYPE = "11";

    //tbl_mt_cif_id_typ
    public static final String IDENTITY_CARD_IDENTIFICATION_TYPE = "I";
    public static final String PASSPORT_IDENTIFICATION_TYPE = "P";
    public static final String TRADE_REGIS_NO = "M";
    public static final String OTHER_TYPE = "O";
    public static final String OFFICIAL_IDCARD_NO_TYPE = "S";
    public static final String TAXID_NO = "T";


}

// end of CustomerConstants.java