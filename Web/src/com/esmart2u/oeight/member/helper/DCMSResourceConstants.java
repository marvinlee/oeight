/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DCMSResourceConstants.java

package com.esmart2u.oeight.member.helper;

/**
 * Resource constants such as keys.
 *
 * @author  Lee Meau Chyuan
 * @version $Revision: 1.14.2.4 $
 */

public interface DCMSResourceConstants
{
    // common
    public static final String RECORD_BEING_REFERENCED = "common.message.general_000000010";

    // Application Maintenance codes
    public static final String APPLICATION_TYPE_MISSING_CODE = "common.message.cs_mt_app_typ_100004200";
    public static final String APPLICATION_CHECKLIST_MISSING_CODE = "common.message.cs_mt_app_chklst_100004300";
    public static final String APPLICATION_ACTIVITY_MISSING_CODE = "common.message.cs_mt_app_actvt_100004400";
    public static final String APPLICATION_DOCGEN_MISSING_CODE = "common.message.cs_mt_app_docgen_100004500";
    public static final String APPLICATION_FACILITY_MISSING_CODE = "common.message.cs_mt_app_fac_100004600";
    public static final String APPLICATION_SUPDOC_MISSING_CODE = "common.message.cs_mt_app_doc_100004700";

    // Facility Tab
    public static final String FACILIY_MISSING_CODE = "common.message.cs_fac_100002200";
    public static final String FACILIY_COLLATERAL_LINK_MISSING_CODE = "common.message.cs_fac_coll_100002300";
    public static final String FACILIY_COLLATERAL_LINK_PLEDGED_CODE = "common.message.cs_fac_coll_100002301";

    //  Condition Tab
    public static final String CONDITION_MISSING_CODE = "common.message.cs_cond_100003300";
    public static final String CHECKLIST_MISSING_CODE = "common.message.cs_chklst_100003600";
    public static final String ACTIVITY_MISSING_CODE = "common.message.cs_actvt_100003700";

    // Change password.
    public static final String UPDATE_USER_PASSWORD_ERROR_CODE = "srn_dcms_00100.message.exception";
    public static final String USER_PASSWORD_MATCH_HISTORY_CODE = "srn_dcms_00098.message.99999";
    public static final String INVALID_OLD_USER_PASSWORD_CODE = "srn_dcms_00098.message.99998";

    // Collateral Tab
    public static final String FACILITY_COLLATERAL_LINK_DELETE_ERROR_CODE = "srn_dcms_00029.message.1000000001";
    public static final String UPDATE_COLLATERAL_APPRAISAL_ISREFER_ERROR_CODE = "srn_dcms_00029.message.1000000002";

    // CIF
    public static final String INVALID_IDENTIFICATION_CODE = "srn_dcms_00002.message.1000002001";
    public static final String MASTER_CUSTOMER_RECORD_NOT_FOUND_ERROR_CODE = "srn_dcms_00002.message.1000002100";

    // External exposure
    public static final String DOWNLOADED_SUCCESS_MESSAGE = "srn_dcms_00079.message.1000002002";
    public static final String HOST_REJECT_MESSAGE = "srn_dcms_00079.message.1000002003";
    public static final String HOST_NOT_FOUND_MESSAGE = "srn_dcms_00079.message.1000002004";
    public static final String HOST_ERROR_DATABASE_CONNECTION_MESSAGE = "srn_dcms_00079.message.1000002005";
    public static final String HOST_ERROR_SERVER_CONNECTION_MESSAGE = "srn_dcms_00079.message.1000002006";
    public static final String HOST_ERROR_TIME_OUT_MESSAGE = "srn_dcms_00079.message.1000002007";
    public static final String HOST_REVOKED_CONSENT_MESSAGE = "srn_dcms_00079.message.1000002008";
    public static final String CONNECTION_ERROR_MESSAGE = "srn_dcms_00079.message.1000002009";
    public static final String UNDEFINED_MESSAGE = "srn_dcms_00079.message.1000002010";

    // phase 1C
    // Credit Application
    public static final String CREDIT_FILE_HAS_BEEN_USED_CODE   = "common.message.cs_app_10001000";
    public static final String CREDIT_FILE_HAS_NO_PRINCIPAL     = "common.message.cs_app_10001001";
    public static final String CREDIT_FILE_IS_CLOSED            = "common.message.cs_app_10001002";


    public static final String CHARGE_VALUE_EXCEED_MORTGAGE_VALUE = "common.message.cs_app_10003001";


}

// end of DCMSResourceConstants.java