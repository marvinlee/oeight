/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DCMSConstants.java

package com.esmart2u.oeight.member.helper;

/**
 * DCMS constants.
 *
 * @author  Chee Weng Keong
 * @version $Revision: 1.20.2.18 $
 */

public interface DCMSConstants
{
    // prescreenning module
    //tbl_mt_prescrn_sts

    public static final String PRE_SCREENING_STATUS_PROCEED = "PC";
    public static final String PRE_SCREENING_STATUS_REJECT = "RJ";
    public static final String PRE_SCREENING_STATUS_PENDING = "PD";
    public static final String PRE_SCREENING_STATUS_FAILED_PROCEED = "FP";
    public static final String FAIL_PROCEED_REASON_TYPE = "01";
    public static final String REJECT_REASON_TYPE = "02";
    public static final String FACILITY_CODE_KEY = "facilityCodeKey";
    public static final String FACILITY_DESCRIPTION_KEY = "facilityDiscriptionKey";
    public static final String SESSION_QUESTION_LIST_KEY = "questionListKey";

    // task rerouting module
    //tbl_mt_rsn_typ
    public static final String REROUTING_REASON_TYPE = "06";

    // application creation module
    public static final String IDENTIFICATION_NUMBER_KEY = "identificationNumberKey";
    public static final String APPLICATION_TYPE_CODE_KEY = "applicationTypeCodeKey";
    public static final String APPLICATION_TYPE_DESCRIPTION_KEY = "applicationTypeDescriptionKey";
    public static final String SESSION_IDENTIFICATION_NUMBER_KEY = "sessionIdentificationNumberKey";
    public static final String SESSION_IDENTIFICATION_TYPE_CODE_KEY = "sessionIdentificationTypeCodeKey";
    public static final String PRE_SCREENING_ID_KEY = "preScreeningIdKey";
    public static final String SESSION_APPLICATION_TYPE_CODE_KEY = "sessionApplicationTypeCodeKey";
    public static final String APPLICATION_PURPOSE_CODE_KEY = "applicationPurposeCodeKey";

    // used across the application flow
    public static final String SESSION_PRE_CREATED_APPLICATION_ID_KEY = "preCreatedApplicationId";
    public static final String SESSION_APPLICATION_ID_KEY = "applicationId";
    public static final String SESSION_APPLICATION_HEADER_KEY = "applicationHeaderVO";
    public static final String SESSION_APPLICATION_WORKFLOW_CODE_KEY = "applicationWorkflowCodeKey";
    public static final String SESSION_APPLICATION_SCORE_CARD_CODE_KEY = "applicationScoreCardCodeKey";
    public static final String APPLICATION_ACTION_FORM_KEY = "applicationActionFormKey";
    public static final String SESSION_APPLICATION_APPLIED_DATE = "applicationAppliedDate";

    public static final String SESSION_MAIN_APPLICANT_ID = "mainApplicantId";

    // used by collateral-related module
    public static final String SESSION_COLLATERAL_ID_KEY  = "collateralId";

    // this key is used to refer collateral VO that temporary stored in session;
    public static final String SESSION_COLLATERAL_VO_KEY = "collateralVO";

    // this key is used to refer type of current collateral record
    public static final String SESSION_COLLATERAL_TYPE_KEY = "collateralTypeKey";

    // Customer
    public static final String CUSTOMER_INFORMATION_MAP_KEY = "customerInformationMap";
    public static final String SESSION_RELATIONSHIP_APPLICATION_CUSTOMER_ID = "relationshipApplicationCustomerId";
    public static final String SESSION_CUSTOMER_VO_KEY  = "customerVO";
    public static final String SESSION_CUSTOMER_ADDRESS_LIST_KEY  = "customerAddressList";
    public static final String SESSION_CUSTOMER_EMPLOYMENT_LIST_KEY  = "customerEmploymentList";
    public static final String SESSION_CUSTOMER_INCOME_LIST_KEY  = "customerIncomeList";

    public static final String CUSTOMER_ACTION_FORM_KEY = "customerActionForm";

    public static final String CUSTOMER_RELATIONSHIP_SPOUSE_CODE = "09";

    // external exposure customer type
    //tbl_mt_acct_typ
    public static final String BORROWING = "B";
    public static final String DEPOSIT = "D";
    public static final String OTHER = "O";

    // used for collateral appraisal module
    //tbl_mt_val_typ
    public static final String PANEL_APPRAISAL = "P";
    public static final String NON_PANEL_APPRAISAL = "N";

    // constants of collateral type
    //tbl_mt_coll_typ    
    public static final String COLLATERAL_TYPE_DEPOSIT = "DP";
    public static final String COLLATERAL_TYPE_GUARANTOR = "GR";
    public static final String COLLATERAL_TYPE_MOTOR_VEHICLE = "MV";
    public static final String COLLATERAL_TYPE_PROPERTY = "PR";

    // constants for solicitor
    public static final String SOLICITOR_CURRENT_PAGE = "SOLICITOR_CURRENT_PAGE";

    // facility
    public static final int FACILITY_MAX_PRICING_TIER = 6;
    public static final int FACILITY_MAX_PRE_INSTALLMENT_PRICING_TIER = 3;

    // facility system code
    //tbl_mt_sys_set
    public static final String SYSTEM_CODE_INFLATION_CLEARANCE_RATE = "CM005";
    public static final String SYSTEM_CODE_NUMBER_OF_TIMES_OF_SALARY = "CM007";
    public static final String SYSTEM_CODE_ADDITIONAL_AMOUNT_START_DAY = "CM008";
    public static final String SYSTEM_CODE_ADDITIONAL_AMOUNT_END_DAY = "CM009";
    public static final String SYSTEM_CODE_DAYS_TO_REPAY = "CM011";
    public static final String SYSTEM_CODE_REMINDER_LETTER_FOR_OUTSTANDING_DOCUMENT = "CM012";
    public static final String SYSTEM_CODE_REMINDER_LETTER_FOR_NON_ACCEPTANCE_LETTER_OFFER = "CM013";
    public static final String SYSTEM_CODE_ROUNDING_SCALE = "CM014";
    public static final String SYSTEM_CODE_MAX_INTEREST_RATE = "CM020";
    public static final String SYSTEM_CODE_KTBCMS_HOSTNAME = "KTBCMSHOSTNAME";

    // facility type
    //tbl_mt_fac_typ
    public static final String FACILITY_TYPE_OVERDRAFT = "OD";
    public static final String FACILITY_TYPE_CIRCLE_LOAN = "CL";
    public static final String FACILITY_TYPE_HOUSING_LOAN = "HL";
    public static final String FACILITY_TYPE_EDUCATION_LOAN = "EL";
    public static final String FACILITY_TYPE_MOTOR_VEHICLE_LOAN = "MV";
    public static final String FACILITY_TYPE_OTHER_LOAN = "OT";
    public static final String FACILITY_TYPE_TERM_LOAN = "TL";

    // facility purpose
    //tbl_mt_fac_pur
    public static final String FACILITY_PURPOSE_LOAN_AMOUNT = "01";
    public static final String FACILITY_PURPOSE_RENOVATION_AMOUNT = "02";
    public static final String FACILITY_PURPOSE_CAR_PARK_AMOUNT = "03";
    public static final String FACILITY_PURPOSE_MRTA_PREMIUM = "04";
    public static final String FACILITY_PURPOSE_LIMIT_AMOUNT = "05";
    public static final String FACILITY_PURPOSE_ADDITIONAL_LIMIT_AMOUNT = "06";
    public static final String FACILITY_PURPOSE_APPLIED_TUITION_OR_COURSE_FEES = "07";
    public static final String FACILITY_PURPOSE_BOARDING_COSTS = "08";
    public static final String FACILITY_PURPOSE_EXPENSES = "09";
    public static final String FACILITY_PURPOSE_COST_OR_EXPENSES = "10";
    public static final String FACILITY_PURPOSE_COMMITMENT_FEES = "11";
    public static final String FACILITY_PURPOSE_FREIGHT_CHARGES = "12";
    public static final String FACILITY_PURPOSE_REGISTRATION_FEES = "13";
    public static final String FACILITY_PURPOSE_INSURANCE_PREMIUM = "14";
    public static final String FACILITY_PURPOSE_OTHER_LOAN_AMOUNT = "15";
    public static final String FACILITY_PURPOSE_APPLIED_AMOUNT = "16";

    public static final String FACILITY_REPAYMENT_METHOD_PNI = "01";
    public static final String FACILITY_REPAYMENT_METHOD_PPI = "02";

    // facility type time
    public static final String DAY = "D";
    public static final String MONTH = "M";
    public static final String YEAR = "Y";

    // relationship
    //tbl_mt_fac_rel
    public static final String RELATIONSHIP_PRINCIPAL_APPLICANT = "P";

    // sign
    //tbl_mt_sign
    public static final String SIGN_PLUS = "01";
    public static final String SIGN_MINUS = "02";

    // tbl_mt_cif_rel_cat static data
    // @todo will put in the correct value.
    public static final String CUSTOMER_RELATED = "C";
    public static final String SHAREHOLDERS = "S";

   //  Document Generation Code
   // tbl_mt_doc_gen
   // Append by wwtham on 20050321
    public static final String LETTER_OF_CANCELLATION_CODE = "L001";
    public static final String LETTER_OF_DECLINE_CODE = "L002";
    public static final String LETTER_OF_REJECT_CODE = "L003";
    public static final String LETTER_OFFER_CODE = "L004";
    public static final String LETTER_INSTRUCTION_CODE = "L005";
    public static final String LETTER_EXECUTED_DOCUMENT_CODE = "L006";
    public static final String LETTER_OF_OUTSTANDING_DOCUMENT_CODE = "L007";
    public static final String LETTER_OF_REMINDER_CODE = "L008";
    public static final String LETTER_BU_ADD_CONTRACT = "L009";
    public static final String LETTER_LNBU_CONTRACT = "L010";    
    public static final String LETTER_BU_CONSENT_FROM_LANDLORD = "L011";
    public static final String LETTER_BU_POA_TA4 = "L012";
    public static final String LETTER_BU_POA = "L013";
    public static final String LETTER_LA_POA = "L017";
    public static final String LETTER_LN_POA = "L020";
    public static final String LETTER_GP_GUARANTEE_CODE = "L022";
    public static final String LETTER_GP_GUARANTEE_WARNING = "L024";
    public static final String LETTER_OF_SAVING_FD_CONSENT_1 = "L025";
    public static final String LETTER_OF_SAVING_FD_CONSENT_2 = "L026";
    public static final String LETTER_OF_SAVING_FD_CONTRACT = "L027";
    public static final String LETTER_OF_SINGLE_CONFIRM = "L038";
    public static final String LETTER_OF_SPOUSE_CONSENT = "L039";
    public static final String LETTER_OF_CL_CONTRACT = "L040";
    public static final String LETTER_OF_CONSUMER_CONTRACT = "L041";
    public static final String LETTER_OF_CL_APPLICATION = "L045";
    public static final String LETTER_CD_ADD_CONTRACT = "L046";
    public static final String LETTER_CD_POA = "L047";
    public static final String LETTER_OF_HL_APPLICATION = "L048";
    public static final String LETTER_OF_PL_APPLICATION = "L049";
    public static final String LETTER_OF_CL_CONFIRMONDEPOSIT = "L053";
    public static final String LETTER_OF_CONSENT = "L055"; //letter to TCB
    public static final String LETTER_LAND_CONTRACT_PARTIAL = "L057"; //letter to TCB
    public static final String LETTER_LEASE_AGREEMENT_GUARANTOR = "L059";
    public static final String LETTER_COT_CANCELLATION = "L060";
    public static final String LETTER_COT_REJECTION = "L061";
    public static final String LETTER_COT_OFFER_TOBRANCH = "L062";
    public static final String LETTER_COT_OFFER_TOCOD = "L063";
    public static final String LETTER_COT_OFFER_TOCUSTOMER = "L064";
    public static final String LETTER_DP_RT_CONTRACT = "L065";
    public static final String LETTER_DP_RT_INFORM = "L066";
    public static final String LETTER_DP_RT_CONFIRM = "L067";


    // facility relation - Guarantor
    //tbl_mt_fac_rel
    public static final String GUARANTOR_CODE = "G";
    public static final String JOIN_CODE = "J";
    public static final String PRINCIPLE_CODE = "P";

    public static final String TEXTAREA_COLS = "COLS";
    public static final String TEXTAREA_ROWS = "ROWS";

    //  Customer Address Type
    //tbl_mt_addr_typ
    public static final String LEGAL_ADRESS_CODE = "L";
    public static final String MAILING_ADRESS_CODE = "M";
    public static final String EMPLOYMENT_ADRESS_CODE = "O";

    // Credit Bureau Type
    //tbl_mt_cdt_bur_typ
    public static final String TCB_CREDIT_BUREAU_TYPE_CODE = "TCB";

    // Loan category code
    //tbl_mt_ln_cat
    public static final String CONSUMER_LOAN_CATEGORY_CODE = "66";
    public static final String REVOLVING_LOAN_CATEGORY_CODE = "67";
    public static final String CREDIT_CARDS_LOAN_CATEGORY_CODE = "68";
    public static final String AUTO_LEASING_LOAN_CATEGORY_CODE = "69";
    public static final String OTHER_LEASING_LOAN_CATEGORY_CODE = "70";

    public static final String HOST_FAILED_REASON_TYPE_CODE = "17";
    public static final String HOST_REJECT_CODE = "03";
    public static final String HOST_NOT_FOUND_CODE = "04";
    public static final String HOST_ERROR_DATABASE_CONNECTION_CODE = "77";
    public static final String HOST_ERROR_SERVER_CONNECTION_CODE = "88";
    public static final String HOST_ERROR_TIME_OUT_CODE = "99";
    public static final String HOST_REVOKED_CONSENT_CODE = "05";
    public static final String HOST_TRACING_CODE = "72";

    public static final String BEFORE_APPROVED_CODE = "BEFORE APPROVED";
    public static final String AFTER_APPROVED_CODE = "AFTER APPROVED"; 

    // Facility Rate Type
    //tbl_mt_rate_typ
    public static final String MLR_CODE = "MLR";
    public static final String MRR_CODE = "MRR";
    public static final String FD_CODE = "FD";
    public static final String FR_CODE = "Fixed Rate";

    // constant variable for Collateral Code (BOT Security Code)
    //tbl_mt_coll
    public static final String COLLATERAL_CODE_LAND = "11100";
    public static final String COLLATERAL_CODE_BUILDING = "11200";
    public static final String COLLATERAL_CODE_LEASEHOLD = "11300";
    public static final String COLLATERAL_CODE_LAND_AND_BUILDING = "11400";
    public static final String COLLATERAL_CODE_CONDO = "91100";

    // constant variable for Title Deed Type
    public static final String TITLE_DEED_TYPE_BLANK = "";
    public static final String TITLE_DEED_TYPE_TITLE_DEED = "01";
    public static final String TITLE_DEED_TYPE_NOR_SOR_3_KOR = "03";
    public static final String TITLE_DEED_TYPE_NOR_SOR_3 = "04";

    //------------- Phase 1C ---------------------------------------------------------------------------------------
    // constant variable for Application Type
    public static final String APPLICATION_TYPE_NEW = "01";
    public static final String APPLICATION_TYPE_TERM_AND_CONDITION_AMENDMENT = "02";
    public static final String APPLICATION_TYPE_CREDIT_OPERATION_TRANSACTION = "03";

    // continue from application creation module
    public static final String CREDIT_FILE_ID_KEY = "creditFileIDKey";
    public static final String CREDIT_FILE_NUMBER_KEY = "creditFileNumberKey";
    public static final String CREDIT_FILE_NAME_KEY = "creditFileNameKey";
    public static final String IDENTIFICATION_TYPE_CODE_KEY = "identificationTypeCodeKey";

    // application purpose
    public static final String APPLICATION_PURPOSE_NEW = "01"; 
    public static final String APPLICATION_PURPOSE_REFINANCE = "02";
    public static final String APPLICATION_PURPOSE_TERM_AND_CONDITION_AMENDMENT = "03";
    public static final String APPLICATION_PURPOSE_CREDIT_TRANSACTION = "04";
    public static final String APPLICATION_PURPOSE_OPERATION_TRANSACTION = "05";
    public static final String APPLICATION_PURPOSE_CREDIT_REVIEW = "06";

    // worksheet tab session
    public static final String SESSION_APPLICATION_STATUS_CODE_KEY = "applicationStatusCodeKey";
    public static final String SESSION_WORKFLOW_TASK_ID_KEY = "workflowTaskIdKey";

    // Term and condition service requests
    public static final String CHANGE_OF_LIMIT                                = "CL";
    public static final String CANCELLATION_CHANGE_OF_PRODUCT                 = "CP";
    public static final String CANCELLATION_OF_FACILITY                       = "CF";
    public static final String EXTENSION_OF_REVIEW_DATE                       = "ER";
    public static final String CHANGE_ON_LOAN_TENURE                          = "CT";
    public static final String CREDIT_REVIEW                                  = "CR";
    public static final String CHANGE_OF_PRICING_INTEREST                     = "CI";
    public static final String CHANGE_OF_PRICING_INTEREST_FOR_TERM_LOAN       = "CI_TERM";
    public static final String CHANGE_OF_PRICING_INTEREST_FOR_LIMIT_LOAN      = "CI_LIMIT";
    public static final String CHANGE_ON_LOAN_REPLAYMENT_TYPE                 = "CRT";
    public static final String CHANGE_ON_LOAN_REPLAYMENT_INSTALMENT_FREQUENCY = "CRF";
    public static final String PARTIAL_OR_FULL_WRITTEN_OFF                    = "WO";
    public static final String ADD_CONDITION                                  = "AC";
    public static final String WAIVE_CONDITION                                = "WC";
    public static final String ADD_COLLATERAL                                 = "NC";
    public static final String RELEASE_COLLATERAL                             = "RC";

    // Variables use to get Section_code
    public static final String SECTION_CODE_APPLIED  = "01";
    public static final String SECTION_CODE_APPROVED = "02";
    public static final String SECTION_CODE_EXISTING = "05";
    public static final String SECTION_CODE_PROPOSED = "06";
    public static final String SECTION_CODE_RECOMMENDATION = "07";
    public static final String SECTION_CODE_APPROVAL = "08";
    public static final String SECTION_CODE_APPROVAL_CONCLUSION = "09";

    // Faciltiy Status Code
    //tbl_cs_mt_app_typ
    public static final String FACILITY_STATUS_APPROVED_CODE = "A";
    public static final String FACILITY_STATUS_PENDING_CODE = "P";
    public static final String FACILITY_STATUS_REJECTED_CODE = "R";
    public static final String FACILITY_STATUS_CANCELLED_CODE = "C";
    public static final String FACILITY_STATUS_APPROVED_VARIATION_CODE = "AV";
    
    // facility pricing opt
   //tbl_mt_app_pur
    public static final String FACILITY_PRICING_OPT_0 = "0";
    public static final String FACILITY_PRICING_OPT_1 = "1";

    // tbl_mt_cif_rel
    public static final String CUSTOMER_RELATIONSHIP_HUSBAND_CODE = "10";

}

// end of DCMSConstants.java