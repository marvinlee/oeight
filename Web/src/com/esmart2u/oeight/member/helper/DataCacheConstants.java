/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DataCacheConstants.java

package com.esmart2u.oeight.member.helper;

/**
 * Constants key for preloaded cache data.
 *
 * @author  Gan Kiat Kin
 * @version $Revision: 1.8 $
 */

public interface DataCacheConstants
{
}

// end of DataCacheConstants.java