/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

package com.esmart2u.oeight.member.helper;

/**
 * @author Goh Siew Chyn
 * @version $Id: HostConstants.java,v 1.2 2004/11/11 03:37:32 hschern Exp $
 */
public class HostConstants
{
    public static final String RECORD_ID                       = "RECORD ID";
    public static final String TRACKING_CODE                   = "TRACKING CODE";
    public static final String ITEM_NO                         = "ITEM NO.";
    public static final String IDENTIFICATION_NUMBER           = "IDENTIFICATION NUMBER";
    public static final String PASSPORT_NUMBER                 = "PASSPORT NUMBER";
    public static final String CUSTOMER_TYPE                   = "CUSTOMER TYPE";
    public static final String TITLE_THAI                      = "TITLE(THAI)";
    public static final String FIRSTNAME_THAI                  = "FIRSTNAME(THAI)";
    public static final String SURNAME_THAI                    = "SURNAME(THAI)";
    public static final String TITLE_ENG                       = "TITLE(ENG)";
    public static final String FIRSTNAME_ENG                   = "FIRSTNAME(ENG)";
    public static final String SURNAME_ENG                     = "SURNAME(ENG)";
    public static final String BIRTH_DATE                      = "BIRTH DATE";
    public static final String ADDRESS_LINE_1                  = "ADDRESS LINE 1";
    public static final String ADDRESS_LINE_2                  = "ADDRESS LINE 2";
    public static final String ADDRESS_LINE_3                  = "ADDRESS LINE 3";
    public static final String POSTAL_CODE                     = "POSTAL CODE";
    public static final String TELEPHONE_NUMBER                = "TELEPHONE NUMBER";
    public static final String OTHER_NAME_THAI_ENG             = "OTHER NAME (THAI/ENG)";
    public static final String DATE_LAST_PAYMENT               = "DATE LAST PAYMENT";
    public static final String RECORD_DATE                     = "RECORD DATE";
    public static final String MAINTENANCE_DATE                = "MAINTENANCE DATE";
    public static final String REMARKS                         = "REMARKS";
    public static final String CONTRACT_DATE                   = "CONTRACT DATE";
    public static final String CONTRACT_LOAN_AMT               = "CONTRACT LOAN AMT";
    public static final String CONTRACT_TERM                   = "CONTRACT TERM";
    public static final String CONTRACT_INSTALLMENT_AMT        = "CONTRACT INSTALLMENT AMT";
    public static final String OS_PRINCIPAL                    = "OS PRINCIPAL";
    public static final String LOAN_CLASS                      = "LOAN CLASS";
    public static final String LOAN_OBJECTIVE                  = "LOAN OBJECTIVE";
    public static final String NUMBER_OF_CO_BORROWER           = "NUMBER OF CO BORROWER";
    public static final String OVERDUE_MONTHS                  = "OVERDUE MONTHS";
    public static final String OVERDUE_MONTHS_HISTORY          = "OVERDUE MONTHS HISTORY";
    public static final String ACCOUNT_STATUS                  = "ACCOUNT STATUS";
    public static final String DATE_OF_LAST_DEBT_RESTRUCTURING = "DATE OF LAST DEBT RESTRUCTURING";
    public static final String NAME_OF_DIRECTOR                = "NAME OF DIRECTOR";
    public static final String INFORMATION_SOURCE              = "INFORMATION SOURCE";
    public static final String DELINQUENT_DATE                 = "DELINQUENT_DATE";
    public static final String COLLATERAL1                     = "COLLATERAL1";
    public static final String COLLATERAL2                     = "COLLATERAL2";
    public static final String COLLATERAL3                     = "COLLATERAL3";
    public static final String ACCOUNT_NAME                    =  "ACCOUNT NAME";
    public static final String CREDIT_LIMIT                    =  "CREDIT LIMIT";
    public static final String CREDIT_USE                      =  "CREDIT USE";
    public static final String DELINQUENT_STATUS               =  "DELINQUENT STATUS";
    public static final String DELINQUENT_STATUS_HISTORY       =  "DELINQUENT STATUS HISTORY";
    public static final String TYPE_OF_CREDIT_CARD             = "TYPE OF CREDIT CARD";
    public static final String OPEN_DATE                       = "OPEN DATE";
    public static final String MINIMUM_PERCENT_PAYMENT         = "MINIMUM PERCENT PAYMENT";
    public static final String NAME_OF_DIRECTOR_THAI_ENG       = "NAME OF DIRECTOR (THAI/ENG)";
    public static final String OS_BALANCE                        = "OS BALANCE";
    public static final String EMPLOYER_NAME                     = "EMPLOYER NAME";
    public static final String UNIT_MAKE                         = "UNIT MAKE";
    public static final String UNIT_MODEL                        = "UNIT MODEL";
    public static final String NO_OF_TIME_TO_PAY_OVERDUE_10_DAY  = "NO. OF TIME TO PAY OVERDUE 10 DAY";
    public static final String NO_OF_TIME_TO_PAY_OVERDUE_30_DAY  = "NO. OF TIME TO PAY OVERDUE 30 DAY";
    public static final String NO_OF_TIME_TO_PAY_OVERDUE_60_DAY  = "NO. OF TIME TO PAY OVERDUE 60 DAY";
    public static final String NO_OF_TIME_TO_PAY_OVERDUE_90_DAY  = "NO. OF TIME TO PAY OVERDUE 90 DAY";

    public static final String SEARCH_PERSONAL_CUSTOMER_TYPE  = "P";
    public static final String SEARCH_COMPANY_CUSTOMER_TYPE  = "C";
}
