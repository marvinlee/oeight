/*
 * HTMLPageRequestor.java
 *
 * Created on September 21, 2009, 3:10 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;

/**
 *
 * @author meauchyuan.lee
 */
public class HTMLPageRequestor {
    
    /** Creates a new instance of HTMLPageRequestor */
    public HTMLPageRequestor() {
    }
    
    public static ArrayList fetchPage(URL serverAddress) {
        ArrayList dataList = new ArrayList();
        HttpURLConnection connection = null;
        OutputStreamWriter wr = null;
        BufferedReader rd  = null;
        StringBuilder sb = null;
        String line = null;
         
        try { 
            //set up out communications stuff
            connection = null;
            
            //Set up the initial connection
            connection = (HttpURLConnection)serverAddress.openConnection(); 
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setReadTimeout(10000);
            
            connection.connect();
            
            //get the output stream writer and write the output to the server
            //not needed in this example
            //wr = new OutputStreamWriter(connection.getOutputStream());
            //wr.write("");
            //wr.flush();
            
            //read the result from the server
            rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
            
            boolean foundTitleEnd = false;
            while (((line = rd.readLine()) != null) && !foundTitleEnd) {
                if (line.toUpperCase().indexOf("</TITLE>") > 0)
                {
                   foundTitleEnd = true;
                }
                sb.append(line + '\n');
            }
            
            String htmlString = sb.toString();
            String htmlUcaseString = sb.toString().toUpperCase();
            //System.out.println(htmlString);
            String title = htmlString.substring(htmlUcaseString.indexOf("<TITLE>")+7, htmlUcaseString.indexOf("</TITLE>"));
            dataList.add(title);
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //close the connection, set all objects to null
            connection.disconnect();
            rd = null;
            sb = null;
            wr = null;
            connection = null;
        }
        return dataList;
    }
    
    public static void main(String[] args)
    {
        try {
            System.out.println(" >> Test page: " + fetchPage( new URL("http://www.google1.com.my")).get(0).toString());
            System.out.println(" >> Test page: " + fetchPage( new URL("http://digiassn.blogspot.com/2008/10/java-simple-httpurlconnection-example.html")).get(0).toString());
            System.out.println(" >> Marvin lee's blog: " + fetchPage( new URL("http://android.marvinlee.net/2009/09/download-android-1-6-sdk-donut/")).get(0).toString());       
            System.out.println(" >> Kenny Sia's blog: " + fetchPage( new URL("http://www.kennysia.com/archives/2009/09/adv-i-wanna-i-w.php")).get(0).toString());
            System.out.println(" >> Kyspeaks's blog: " + fetchPage( new URL("http://kyspeaks.com/2009/09/20/pm-najib-razaks-open-house-with-the-best-milo/")).get(0).toString());
            System.out.println(" >> Archie chi's blog: " + fetchPage( new URL("http://archiechi.com/simple-life/mount-kinabalu-gunung-kinabalu/")).get(0).toString());
            System.out.println(" >> Blogspot: " + fetchPage(new URL("http://glamoursecret.blogspot.com/search/label/Lingerie")).get(0).toString());
             } catch (MalformedURLException ex) {
            ex.printStackTrace();
            System.err.println("Error with URL!!");
        }
    }
    
}
