/*
 * EmailAddressHelper.java
 *
 * Created on April 17, 2008, 11:49 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
/**
 *
 * @author meauchyuan.lee
 */
public class EmailAddressHelper {
    private static Pattern emailPattern = Pattern.compile(
            "^[0-9a-z]([-_.~]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-z]{2,4}$"
            );
    
    /** Creates a new instance of EmailAddressHelper */
    public EmailAddressHelper() {
    }
    
    public static String getEmailAddressString(String input) {
        HashSet emailSet = getEmailAddress(input);
        System.out.println("Size = " + emailSet.size());
        String emailsFound = "";
        if (!emailSet.isEmpty()){  
            Iterator iterator = emailSet.iterator();
            while (iterator.hasNext())
            {
                emailsFound+= (String)iterator.next() + ",";                
            }
            emailsFound = emailsFound.substring(0,emailsFound.length()-1);
        }
        System.out.println("emailsFound=" + emailsFound);
        return emailsFound;
    }
      
    public static String getEmailAddressString(HashSet inputSet) { 
        System.out.println("Size = " + inputSet.size());
        String emailsFound = "";
        if (!inputSet.isEmpty()){  
            Iterator iterator = inputSet.iterator();
            while (iterator.hasNext())
            {
                emailsFound+= (String)iterator.next() + ",";                
            }
            emailsFound = emailsFound.substring(0,emailsFound.length()-1);
        }
        System.out.println("emailsFound=" + emailsFound);
        return emailsFound;
    }
    
    public static HashSet getEmailAddress(String input) {
        
        // Truncate if string is too long, more than 6000?
        if (input == null) return null;
        //int maxLength = 6000;
        //if (input.length() > 6000) input = input.substring(0,6000);
        
        HashSet emailSet = new HashSet();
        String emailAddresses = input.toLowerCase(); 
       
        //String[] tokens = new String[]{"~","`","!","#","$","%","^","&","*","-","+","=","{","}","[","]","|","\\",":",";","\"","'","<",">",",","?","/"};
        String[] tokens = new String[]  {"~","`","!","#","$","%","^","&",    "-",    "=",                         ":",";","\"","'","<",">",",",    "/","\n"};
        
        for (int i=0;i<tokens.length;i++)
        {
            emailAddresses = emailAddresses.replaceAll(tokens[i]," ");
        }
        StringTokenizer tokenizer = new StringTokenizer(emailAddresses," ");
        while (tokenizer.hasMoreTokens()) {
            String email = tokenizer.nextToken().trim();
            if (email.indexOf("@") >=0){
                // Other necessary filtering
                email = extraFilter(email);
               
                if (isEmailAddress(email)) { 
                    System.out.println("Found:" + email);
                    emailSet.add(email); //Set ensures no repeat 
                } 
                // if fail second, just ignore the email
            }
        }
        
        return emailSet;
    }
    
    private static String extraFilter(String email)
    {
        String[] tokens = new String[] {"*","+","{","}","[","]","|","\\","?","(",")","."};
        for (int i=0;i<tokens.length;i++)
        {
            boolean foundPrefix = true;
            while (foundPrefix)
            {
                boolean startsWith = email.startsWith(tokens[i]);
                if (startsWith) {
                    email = email.substring(1,email.length());
                }
                else
                {
                    // Exit
                    foundPrefix = false;
                }
            }
            
            boolean foundPostfix = true;
            while (foundPostfix)
            {
                boolean endsWith = email.endsWith(tokens[i]);
                if (endsWith) {
                    email = email.substring(0,email.length()-1);
                }
                else
                {
                    // Exit
                    foundPostfix = false;
                }
            }
        
        } 
        return email;
    }
    
    public static boolean isEmailAddress(String email) {
        return emailPattern.matcher(email).matches();
    }
    
    public static void main2(String[] args) {
        String test="Read the official blog at http://my080808.blogs.friendster.com/" +
"Intro:" +
"This is the official friendster profile for 080808.com.my " +

"The site is a very \"special project\" to commemorate 080808." +

"To friends in this friendster profile, you will get first-hand updated news about the site and are the first level friends...." +
"Just add this profile and we strive to make the best out of this relationship. =)" +
"To add : founder@080808.com.my... " +

"Comments images can be found under the \"Shop -> Free E-cards\" MSN:test1@hotmail.com\ntest1@hotmail.com\ntest1@hotmail.com\nMSN:test2@hotmail.com\r\nMSN:test3@hotmail.com\n" +

"Remember to make your \"((~asdad@live.com!))\" wish at 080808.com.my" +

"Enjoy! <dfsdf@est.com> [ajhdfjd@ac.com] (2222@ac.com)  (XSDFRSDR@xxer.xxet.xx.xxdegh.cc)    sdfs sdfs sdfsdf " +

"Who I Want to Meet:" + 

"Anybody who would like to make more friends!" +

"Since you're here, it's definately a U!";
        HashSet emailSet = EmailAddressHelper.getEmailAddress(test);
        System.out.println("All Unique Emails = " + EmailAddressHelper.getEmailAddressString(emailSet) );
    }
    
    public static void main(String[] args)
    {
    	File folder = new File("G:\\Projects\\Mabbel\\email\\");
    	File[] rawFiles = folder.listFiles();
    	BufferedWriter bufferedWriter = null;
    	BufferedReader br = null;
    	int totalEmails = 0;
    	for (File rawFile : rawFiles)
    	{
	    	try {    
	    		String line;
	    		StringBuilder fileContent = new StringBuilder();
	    		br = new BufferedReader(new FileReader(rawFile));
	    		while ((line = br.readLine()) != null) { // while loop begins here
	    			//System.out.println(line);
	    			fileContent.append(line);
	    		} // end while 
	    		
	    		//Get Emails
	    		HashSet<String> emailSet = EmailAddressHelper.getEmailAddress(fileContent.toString());
	    		System.out.println("File:" + rawFile.getName() + " count:" + emailSet.size());
	    		totalEmails += emailSet.size();
	    		
	    		//Print to file
	        	File processedFolder = new File("G:\\Projects\\Mabbel\\email\\processed\\" + rawFile.getName());
	            
	            bufferedWriter = new BufferedWriter(new FileWriter(processedFolder));
	            for(String email : emailSet)
	            {
	            	bufferedWriter.write(email+"\n");
	            }
	    	} // end try
	    	catch (IOException e) {
	    		System.err.println("Error: " + e);
	    	 } finally {
	             //Close the BufferedWriter
	             try {
	            	 br.close();
	                 if (bufferedWriter != null) {
	                     bufferedWriter.flush();
	                     bufferedWriter.close();
	                 }
	             } catch (IOException ex) {
	                 ex.printStackTrace();
	             }
	         }
    	}
    	System.out.println("Grand Total Emails: " + totalEmails);

    }
}
