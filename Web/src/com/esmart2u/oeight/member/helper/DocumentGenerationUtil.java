/**
 * � 2001 - 2004 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DocumentGenerationUtil.java

package com.esmart2u.oeight.member.helper;

import com.esmart2u.solution.base.logging.Logger;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: kkku
 * Date: Aug 16, 2004
 * Time: 3:09:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class DocumentGenerationUtil
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(DocumentGenerationUtil.class);

    /**
     * Encrypt a String.
     *
     * @param strParam
     * @return Encrypted String
     * @throws Exception
     */
    public static String encrypt(String strParam) throws Exception
    {
        // If encrypt algorithm is changed, please also update the VB program
        try
        {
            String[] strKeyArray = {"4", "21", "30", "49", "52", "68", "72", "98", "3", "8", "12", "29", "36", "48", "55", "62"};
            int intKeyArraySize = strKeyArray.length;

            int intParamLength = strParam.length();

            int intMultiplier = intParamLength / intKeyArraySize;
            int intMod = intParamLength % intKeyArraySize;

            Vector vecKeyVector = new Vector();

            for(int i = 1; i <= intMultiplier; i++)
            {
                for(int j = 0; j < intKeyArraySize; j++)
                {
                    vecKeyVector.addElement(strKeyArray[j]);
                }
            }

            for(int k = 0; k < intMod; k++)
            {
                vecKeyVector.addElement(strKeyArray[k]);
            }

            StringBuffer sbResult = new StringBuffer();

            for (int counter=0; counter < vecKeyVector.size(); counter++)
            {
                // convert the character to ASCII
                int intAsciiCode = (int)strParam.charAt(counter);

                // Character XOR Key
                int intXOR = intAsciiCode ^ Integer.parseInt((String)vecKeyVector.elementAt(counter));

                // Convert to Hex
                String strHex = Integer.toHexString(intXOR);

                if (strHex.length() < 2)
                {
                    strHex = "0" + strHex;
                }

                sbResult = sbResult.append(strHex);


//                logger.debug("============  " + counter + "  ===========");
//                logger.debug("original char -- " + strParam.charAt(counter));
//                logger.debug("ASCII code    -- " + intAsciiCode);
//                logger.debug("strKey        -- " + (String)vecKeyVector.elementAt(counter));
//                logger.debug("XOR result    -- " + intXOR);
//                logger.debug("Hex result    -- " + strHex);
//                logger.debug("===============================");

            }

            return sbResult.toString();
        }
        catch(Exception e)
        {
            return "";
        }
    }

    /**
     * Decrypt a String.
     *
     * @param strParam
     * @return Decrypted String
     * @throws Exception
     */
    public static String decrypt(String strParam) throws Exception
    {
        try
        {
            strParam = strParam.trim();

            String[] strKeyArray = {"4", "21", "30", "49", "52", "68", "72", "98", "3", "8", "12", "29", "36", "48", "55", "62"};
            int intKeyArraySize = strKeyArray.length;

            int strParamLength = strParam.length();

            if (strParamLength < 2 || strParamLength % 2 == 1)
                throw new IllegalArgumentException("Illegal Hex String");

            StringBuffer sbXOR = new StringBuffer();

            for (int i = 0; i < strParamLength; i += 2)
            {
                String s = strParam.substring(i, i + 2);
                int intHex = Integer.parseInt(s, 16);
                sbXOR.append((char) intHex);
            }

            String strXOR = sbXOR.toString();

            int intStrXORLength = strXOR.length();

            int intMultiplier = intStrXORLength / intKeyArraySize;
            int intMod = intStrXORLength % intKeyArraySize;

            Vector vecKeyVector = new Vector();

            for(int i = 1; i <= intMultiplier; i++)
            {
                for(int j = 0; j < intKeyArraySize; j++)
                {
                    vecKeyVector.addElement(strKeyArray[j]);
                }
            }

            for(int k = 0; k < intMod; k++)
            {
                vecKeyVector.addElement(strKeyArray[k]);
            }

            StringBuffer sbResult = new StringBuffer();

            for (int counter=0; counter < vecKeyVector.size(); counter++)
            {
                // XOR back the original ASCII code
                int intAsciiCode = strXOR.charAt(counter) ^ Integer.parseInt((String)vecKeyVector.elementAt(counter));

                // Convery ASCII code to character
                String strOriginal = String.valueOf((char)intAsciiCode);

                sbResult = sbResult.append(strOriginal);

            }

            return sbResult.toString();
        }
        catch(Exception e)
        {
            return "";
        }
    }
}