/*
 * SeasonsGreetingsEmailSender.java
 *
 * Created on November 14, 2007, 4:31 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp;

import com.esmart2u.oeight.data.SeasonGreeting;
import com.esmart2u.oeight.member.helper.*;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;
import org.apache.tools.ant.taskdefs.Sleep;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author meauchyuan.lee
 */
public class SeasonsGreetingsEmailSender {
      
    private static Logger logger = Logger.getLogger(SeasonsGreetingsEmailSender.class);
    private static SeasonsGreetingsEmailSender instance = null;
    private static boolean working = false;
    private static String userId = PropertyManager.getValue(PropertyConstants.SMTP_USER_KEY);
    private static String password = PropertyManager.getValue(PropertyConstants.SMTP_PASS_KEY);
    private static String systemSender = PropertyManager.getValue(PropertyConstants.SMTP_DISPLAY_SENDER);
    private static String domainName = ConfigurationHelper.getDomainName();
    private static Properties mailServerConfig = new Properties(); 

    private final Timer timer = new Timer();
     
    private static boolean fromStartup = false;
    private boolean firstTime = true;
    private static boolean enabled = PropertyManager.getBoolean(PropertyConstants.SEASONS_GREETINGS_SENDER_ENABLE,true); 
    // Defaults 10 mins
    private final int minutes = PropertyManager.getInt(PropertyConstants.SEASONS_GREETINGS_SENDER_FREQUENCY,10); 

    public static synchronized SeasonsGreetingsEmailSender getInstance(){
        if ( instance == null ){
            instance = new SeasonsGreetingsEmailSender();
            instance.mailServerConfig = fetchConfig(); 
            if (enabled){
                instance.start();
            }
        }
        return instance;
    }
    
    
    public static void refreshConfig() {
        mailServerConfig.clear();
        fetchConfig();
    }
    
    /**
     * Open a specific text file containing mail server
     * parameters, and populate a corresponding Properties object.
     */
    private static Properties fetchConfig() { 
        try{
            //If possible, one should try to avoid hard-coding a path in this
            //manner; in a web application, one should place such a file in
            //WEB-INF, and access it using ServletContext.getResourceAsStream.
            //Another alternative is Class.getResourceAsStream.
            //This file contains the javax.mail config properties mentioned above.
            String host = PropertyManager.getValue(PropertyConstants.SMTP_HOST_KEY);
            String fromAddress = PropertyManager.getValue(PropertyConstants.SMTP_REPLY_TO_KEY);
            String port = PropertyManager.getValue(PropertyConstants.SMTP_PORT_KEY);
            mailServerConfig = new Properties();       
            mailServerConfig.put("mail.smtp.auth", "true"); 
            mailServerConfig.put("mail.smtp.port", port); 
            mailServerConfig.put("mail.transport.protocol", "smtp"); 
            mailServerConfig.put("mail.host", host);
            mailServerConfig.put("mail.from", fromAddress);         
            mailServerConfig.setProperty("mail.user", userId);
            mailServerConfig.setProperty("mail.password", password);

        } catch(Exception e) {
            logger.error("error at fetchConfig method: "+e);
        }
        return mailServerConfig;
    }
    
    public void invoke(boolean fromStart) {
        //static boolean initalizer = fromStart;
        this.fromStartup = fromStart; 
    }
    
    public static void invoke() {
        logger.info("GreetingsEmailSender invoke called");
        if (!working) {
            logger.info("Going to start work");
            working = true; 
            sendMails(null); 
            logger.info("Done working, setting to rest");
            working = false;
        }
    }
    
    private static void sendMails(org.hibernate.Session session)
    {
            if (session == null){
                session = HibernateUtil.getSessionFactory().openSession();
                //session.beginTransaction();
            }
    
            List seasonGreetingList = (List) session.createCriteria(SeasonGreeting.class) 
            .add(Expression.eq( "status", OEightConstants.MAIL_STATUS_NEW)).setMaxResults(90).list();
    
            if (seasonGreetingList != null && !seasonGreetingList.isEmpty())
            {
                Date now = new Date();
                for(int i=0;i<seasonGreetingList.size();i++)
                {
                    // Transaction for each record
                    session.beginTransaction();
                    SeasonGreeting seasonGreeting = (SeasonGreeting)seasonGreetingList.get(i);
                    String receiverEmail = seasonGreeting.getReceiverEmail();
                    String receiverName =  seasonGreeting.getReceiverName();
                    String senderEmail = seasonGreeting.getSenderEmail();
                    String senderName = seasonGreeting.getSenderName();
                    String greetingCode = seasonGreeting.getGreetingCode();
                    
                    try {

                        // Send email
                        // URL is something like this http://dummy-host3.acskl.com/invite.do?act=invited&invitedBy=test1&inviteCode=997e56d3fb8fe13a75ae4c1b62e80e95
                        
                        String mailContent = "Dear " + receiverName + ",<br><br> Your friend " + senderName + "("+senderEmail+") has sent you a greeting card! <br> Please follow the link below to retrieve your greeting card.";
                        String greetingString = "http://"+domainName+"/seasons_greetings.do?act=retrieve&senderEmail="+ senderEmail + "&greetingCode=" + greetingCode;
                        mailContent += "<br><br><a href='" + greetingString + "'>" + greetingString + "</a>";
                        mailContent += "<br><br>This is a system generated email, please do not reply.";
                        
                        mailContent += "<br><br>Note: We do not add your email address to any lists, nor will we ever share it";
                        mailContent += " with anyone at any time.";
                        mailContent += "<br><br>* You will have to register yourself to receive any subsequent updates from us.";

                        //SeasonsGreetingsMailThread thread = new SeasonsGreetingsMailThread();
                        //thread.setMailSessionValues(userId, password, systemSender, domainName, mailServerConfig);
                        //thread.setIndividualEmailValues(senderName, senderEmail,receiverEmail,"Seasons Greetings from "+domainName+" on behalf of " + senderName,mailContent);
                        //thread.start();
                        
                        sendIndividualEmail(senderName, senderEmail,receiverEmail,"Seasons Greetings from "+domainName+" on behalf of " + senderName,mailContent);
                        
                        seasonGreeting.setStatus(OEightConstants.MAIL_STATUS_SENT); 
                        
                    } catch (Exception ex) {
                        logger.error("Error sending invitation to " + receiverEmail); 
                        seasonGreeting.setStatus(OEightConstants.MAIL_STATUS_SEND_FAILED);
                    }

                    // Update db
                    seasonGreeting.setDateMailed(now);
                    session.save(seasonGreeting); 
                    session.getTransaction().commit();
                    //commitSession(session);
                    logger.debug("Greeting card sent to " + receiverEmail);
                    
                    // Sleep for 5 second for every 10 mails/threads
                    // Check committing
                    if (i%10 == 0)
                    {
                        try { 
                            logger.debug("Sleeping for 5 second");
                            Thread.sleep(5 * 1000);
                        } catch (InterruptedException ex) {
                            //ex.printStackTrace();
                            logger.debug("Error while sleeping : " + ex);
                        }
                    }
                }
                
              // Sleep for 5 minutes for every bulk of 90 mails sent  - SKIP if from startup, so no wait
                if (!fromStartup){
                    try { 
                        logger.debug("Sleeping for 5 minutes");
                        Thread.sleep(5 * 60 * 1000);
                    } catch (InterruptedException ex) {
                        //ex.printStackTrace();
                        logger.debug("Error while sleeping : " + ex);
                    } 
                }
                else
                {
                    fromStartup = false;
                }
                
                // Do a send mail to continue to no mails left
                sendMails(session);
            }
            else
            {
                logger.info("Done sending, rest a while....");
                working = false;
            }
    
            /*
             try {
                if (!session.getTransaction().wasCommitted() && session.getTransaction().isActive())
                {
                    session.getTransaction().commit();
                    logger.info("sendMails - committed"); 
                }
            } catch (HibernateException ex) {
                ex.printStackTrace();
                logger.error("Error in committing");
            }
             */
            
    }
    
    /**
     * Send a single email.
     */
    private static void sendIndividualEmail(String fromName, String fromEmailAddr, String toEmailAddr, String subject, String mailBody) throws Exception {
         
        logger.info( "Getting authentication presetting");
        EmailAuthentication auth = new EmailAuthentication(userId,password); 
        logger.info( "Getting mail server config");
        Session session = Session.getDefaultInstance( mailServerConfig, auth); 
        //Session session = Session.getDefaultInstance( mailServerConfig, null);         
        session.setDebug(true); 
        logger.info( "Creating Message");
        MimeMessage message = new MimeMessage( session );  
        
        try {
            //the "from" address may be set in code, or set in the
            //config file under "mail.from" ; here, the latter style is used
            logger.info( "Preparing email");
            //message.setFrom( new InternetAddress(fromEmailAddr) );
            message.setFrom( new InternetAddress(systemSender, domainName) );        
            InternetAddress[] replyToList = new InternetAddress[1];
            replyToList[0] = new InternetAddress(fromEmailAddr, fromName); 
            message.setReplyTo(replyToList);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmailAddr));
            message.setSubject(subject);
            message.setContent(mailBody, "text/html");
            logger.info( "Sending email");
            Transport.send( message );
            logger.info( "Email sent!");
        } catch(AddressException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Address Exception Cannot send email"+ex);
            throw new MessagingException("Address Exception: "+ex);
        } catch(javax.mail.MessagingException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Messaging Exception Cannot send email"+ex);
            throw new MessagingException("Messaging Exception: "+ex);
        }
        
    }
    
      
    private void start() {
        if (enabled){
            long runningTime = minutes * 60 * 1000;
            if (firstTime){
                runningTime = 100;
                firstTime = false; 
            }

            timer.schedule(new TimerTask() {
                public void run() {
                    logger.info("Timer task starting at " + new Date());
                    getInstance().invoke();
                    logger.debug("Sent emails at " + new Date()); 
                    getInstance().start();
                } 
            }, runningTime);
        }
    }
    
    private static void commitSession(org.hibernate.Session session)
    { 
             try {
                if (!session.getTransaction().wasCommitted() && session.getTransaction().isActive())
                {
                    session.getTransaction().commit();
                    logger.info("sendMails - committed"); 
                }
            } catch (HibernateException ex) {
                ex.printStackTrace();
                logger.error("Error in committing");
            } /*finally
             {
             
                if (session.isOpen()){
                    session.close();
                }
             }*/
    }
}
