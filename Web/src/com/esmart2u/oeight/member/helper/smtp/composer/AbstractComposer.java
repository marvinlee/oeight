/*
 * AbstractComposer.java
 *
 * Created on April 16, 2008, 2:58 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp.composer;

import com.esmart2u.oeight.data.EmailUpdate;

/**
 *
 * @author meauchyuan.lee
 */
public interface AbstractComposer {
    
    // Get email subject
    String getEmailSubject(EmailUpdate emailUpdate);
    
    // Get email content
    String createEmailContent(EmailUpdate emailUpdate);
    
    // Flag to determine if use system email (in REPLY-TO)
    boolean useSystemEmailForReply();
    
    // Send to friends?
    boolean sendToFriends();
    
}
