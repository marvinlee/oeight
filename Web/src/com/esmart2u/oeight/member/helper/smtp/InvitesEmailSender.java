/*
 * InvitesEmailSender.java
 *
 * Created on October 21, 2007, 12:24 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp;

import com.esmart2u.oeight.data.UserInvites;
import com.esmart2u.oeight.member.helper.*;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import java.util.Timer;
import java.util.TimerTask;


/**
 *
 * @author meauchyuan.lee
 */
public class InvitesEmailSender {
    
    private static Logger logger = Logger.getLogger(InvitesEmailSender.class);
    private static InvitesEmailSender instance = null;
    private static boolean working = false;
    private static String userId = PropertyManager.getValue(PropertyConstants.SMTP_USER_KEY);
    private static String password = PropertyManager.getValue(PropertyConstants.SMTP_PASS_KEY);
    private static String homeSite = "http://" + ConfigurationHelper.getDomainName();
    
    private static Properties mailServerConfig = new Properties(); 

    private final Timer timer = new Timer();
     
    private static boolean fromStartup = false;
    private boolean firstTime = true;
    private static boolean enabled = PropertyManager.getBoolean(PropertyConstants.INVITES_SENDER_ENABLE,true); 
    // Defaults 1 minute
    private final int minutes = PropertyManager.getInt(PropertyConstants.INVITES_SENDER_FREQUENCY,1); 

    public static synchronized InvitesEmailSender getInstance(){
        if ( instance == null ){
            instance = new InvitesEmailSender();
            instance.mailServerConfig = fetchConfig();
            //instance.currentMonth = getCurrentMonth();
            //getLatestHistoryList();
            if (enabled){
                instance.start();
            }
        }
        return instance;
    }
    
    
    public static void refreshConfig() {
        mailServerConfig.clear();
        fetchConfig();
    }
    
    /**
     * Open a specific text file containing mail server
     * parameters, and populate a corresponding Properties object.
     */
    private static Properties fetchConfig() { 
        try{
            //If possible, one should try to avoid hard-coding a path in this
            //manner; in a web application, one should place such a file in
            //WEB-INF, and access it using ServletContext.getResourceAsStream.
            //Another alternative is Class.getResourceAsStream.
            //This file contains the javax.mail config properties mentioned above.
            String host = PropertyManager.getValue(PropertyConstants.SMTP_HOST_KEY);
            String fromAddress = PropertyManager.getValue(PropertyConstants.SMTP_REPLY_TO_KEY);  
            String port = PropertyManager.getValue(PropertyConstants.SMTP_PORT_KEY);
            mailServerConfig = new Properties();       
            mailServerConfig.put("mail.smtp.auth", "true"); 
            mailServerConfig.put("mail.smtp.port", port); 
            mailServerConfig.put("mail.transport.protocol", "smtp"); 
            mailServerConfig.put("mail.host", host);
            mailServerConfig.put("mail.from", fromAddress);         
            mailServerConfig.setProperty("mail.user", userId);
            mailServerConfig.setProperty("mail.password", password);

        } catch(Exception e) {
            logger.error("error at fetchConfig method: "+e);
        }
        return mailServerConfig;
    }
    
    public void invoke(boolean fromStart) {
        //static boolean initalizer = fromStart;
        this.fromStartup = fromStart; 
    }
    
    public static void invoke() {
        logger.debug("EmailSender invoke called");
        if (!working) {
            logger.debug("Going to start work");
            working = true; 
            sendMails(null); 
            logger.debug("Done working, setting to rest");
            working = false;
        }
    }
    
    private static void sendMails(org.hibernate.Session session)
    {
            if (session == null){
                session = HibernateUtil.getSessionFactory().openSession();
            }
    
            List invitationList = (List) session.createCriteria(UserInvites.class) 
            .add(Expression.eq( "status", OEightConstants.MAIL_STATUS_NEW)).setMaxResults(10).list();
    
            if (invitationList != null && !invitationList.isEmpty())
            {
                Date now = new Date();
                for(int i=0;i<invitationList.size();i++)
                {
                    UserInvites invite = (UserInvites)invitationList.get(i);
                    String email = invite.getEmail();
                    try {

                        session.beginTransaction();
                        // Send email
                        // URL is something like this http://dummy-host3.acskl.com/invite.do?act=invited&invitedBy=test1&inviteCode=997e56d3fb8fe13a75ae4c1b62e80e95
                        long userId = invite.getUserId();
                        String senderUserName = invite.getUser().getUserName();
                        String senderFullName = invite.getUser().getUserCountry().getName();
                        StringBuffer mailOutput = new StringBuffer();
                        String inviteString = "";
                        
                        // Requested from system
                        if (userId == PropertyManager.getInt(PropertyConstants.SYSTEM_USERID_KEY))
                        {
                            mailOutput.append("Dear " + email +",<br><br>");
                            //mailOutput.append("This invitation email is sent to you because you have requested it through <a href='" + homeSite + "'>" + homeSite + "</a>.<br><br>");
                            //mailOutput.append("If you have not requested this invite, please ignore this email.<br><br>");
                            mailOutput.append("This email is sent to you because the email address of <b>" + email + "</b> was used to register at  <a href='" + homeSite + "'>" + homeSite + "</a>.<br><br>");
                            mailOutput.append("If you have not joined  <a href='" + homeSite + "'>" + homeSite + "</a>, please ignore this email.<br><br>");
                            inviteString = "http://"+ ConfigurationHelper.getDomainName()+"/register.do?act=validated&invitedBy="+ senderUserName + "&inviteCode=" + invite.getInviteCode();
                            mailOutput.append("Please click or follow the link below to validate your profile at "+ ConfigurationHelper.getDomainName()+".");
                        }else
                        {   // Invites sent by other members
                            mailOutput.append("Dear " + email +",<br><br>");
                            mailOutput.append("Your friend, <b>"+senderFullName+"</b> has invited you to join "+ ConfigurationHelper.getDomainName()+".<br>");
                            mailOutput.append("If <b>"+senderFullName+"</b> is someone known to you, you have just received a valuable invitation to join a wonderful project with <b>"+senderFullName+"</b>.<br>");
                            mailOutput.append("You can learn more about the project at <a href='" + homeSite + "'>" + homeSite + "</a><br><br>");
                            inviteString = "http://"+ ConfigurationHelper.getDomainName()+"/register.do?act=invited&invitedBy="+ senderUserName + "&inviteCode=" + invite.getInviteCode();
                            mailOutput.append("Please click or follow the link below to register and \"make a mark\" at "+ ConfigurationHelper.getDomainName()+".");
                        }
                        
                        String mailContent = mailOutput.toString();
                        mailContent += "<br><br><a href='" + inviteString + "'>" + inviteString + "</a>";
                        mailContent += "<br><br>Best regards and wishes,<br>The "+ ConfigurationHelper.getDomainName()+" Team<br>";
                        mailContent += "<br><br>This is a system generated email, please do not reply to this email address.";
                        sendIndividualEmail(PropertyManager.getValue(PropertyConstants.SMTP_REPLY_TO_KEY),email,"080808.com.my invites you to 'make a mark'!",mailContent);
                        
                        invite.setStatus(OEightConstants.MAIL_STATUS_SENT);
                    } catch (Exception ex) {
                        logger.error("Error sending invitation to " + email); 
                        invite.setStatus(OEightConstants.MAIL_STATUS_SEND_FAILED);
                    }

                    // Update db
                    invite.setDateMailed(now);
                    session.save(invite);  
                    session.getTransaction().commit();
                    //commitSession(session);
                    logger.debug("Invitation sent to " + email);
                    
                    // Sleep for 5 second for every 10 mails/threads
                    // Check committing
                    if (i%10 == 0)
                    {
                        try { 
                            logger.debug("Sleeping for 10 second");
                            Thread.sleep(10 * 1000);
                        } catch (InterruptedException ex) {
                            //ex.printStackTrace();
                            logger.debug("Error while sleeping : " + ex);
                        }
                    }
                }
                
                
              // Sleep for 2 minutes for every bulk of 10 mails sent  - SKIP if from startup, so no wait
                if (!fromStartup){
                    try { 
                        logger.debug("Sleeping for 2 minutes");
                        Thread.sleep(2 * 60 * 1000);
                    } catch (InterruptedException ex) {
                        //ex.printStackTrace();
                        logger.debug("Error while sleeping : " + ex);
                    } 
                }
                else
                {
                    fromStartup = false;
                }
                
                
                // Do a send mail to continue to no mails left
                sendMails(session);
            }
            else
            {
                logger.debug("Done sending, rest a while....");
                working = false;
            }
    
            // Commit is done individually above
            /*try {
                if (!session.getTransaction().wasCommitted() && session.getTransaction().isActive())
                {
                    session.getTransaction().commit();
                    logger.debug("sendMails - committed"); 
                }
            } catch (HibernateException ex) {
                ex.printStackTrace();
                logger.error("Error in committing");
            }finally{ 
                if (session.isOpen()){
                    session.close();
                }
            }*/
           
            
    }
    
    /**
     * Send a single email.
     */
    private static void sendIndividualEmail(String fromEmailAddr, String toEmailAddr, String subject, String mailBody) throws Exception {
         
        logger.debug( "Getting authentication presetting");
        EmailAuthentication auth = new EmailAuthentication(userId,password); 
        //logger.debug( "Getting mail server config");
        Session session = Session.getDefaultInstance( mailServerConfig, auth); 
        
        //Session session = Session.getDefaultInstance( mailServerConfig, null);         
        //session.setDebug(true); 
        
        //logger.debug( "Creating Message");
        MimeMessage message = new MimeMessage( session );  
        
        try {
            //the "from" address may be set in code, or set in the
            //config file under "mail.from" ; here, the latter style is used
            //logger.debug( "Preparing email");
            message.setFrom( new InternetAddress(fromEmailAddr) );
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmailAddr));
            message.setSubject(subject);
            message.setContent(mailBody, "text/html");
            logger.debug( "Sending email");
            Transport.send( message );
            logger.debug( "Email sent!");
        } catch(AddressException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Address Exception Cannot send email"+ex);
            throw new MessagingException("Address Exception: "+ex);
        } catch(javax.mail.MessagingException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Messaging Exception Cannot send email"+ex);
            throw new MessagingException("Messaging Exception: "+ex);
        }
        
    }
    
    private static void commitSession(org.hibernate.Session session)
    { 
             try {
                if (!session.getTransaction().wasCommitted() && session.getTransaction().isActive())
                {
                    session.getTransaction().commit();
                    logger.info("sendMails - committed"); 
                }
            } catch (HibernateException ex) {
                ex.printStackTrace();
                logger.error("Error in committing");
            } /*finally
             {
             
                if (session.isOpen()){
                    session.close();
                }
             }*/
    }
       
    private void start() {
        if (enabled){
            long runningTime = minutes * 60 * 1000;
            if (firstTime){
                runningTime = 100;
                firstTime = false; 
            }

            timer.schedule(new TimerTask() {
                public void run() {
                    logger.debug("Timer task starting at " + new Date());
                    getInstance().invoke();
                    logger.debug("Sent emails at " + new Date()); 
                    getInstance().start();
                } 
            }, runningTime);
        }
    }

}
