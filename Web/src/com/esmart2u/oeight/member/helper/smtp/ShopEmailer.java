/*
 * ShopEmailer.java
 *
 * Created on February 13, 2008, 9:22 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp;

import com.esmart2u.oeight.data.UserShop;
import com.esmart2u.oeight.member.web.struts.helper.DateObjectHelper;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class ShopEmailer {
    
    private static Logger logger = Logger.getLogger(ShopEmailer.class);
    
    private static String userId = PropertyManager.getValue(PropertyConstants.SMTP_USER_KEY);
    private static String password = PropertyManager.getValue(PropertyConstants.SMTP_PASS_KEY);
    private static String systemSender = PropertyManager.getValue(PropertyConstants.SMTP_DISPLAY_SENDER);
    private static String domainName = ConfigurationHelper.getDomainName();
    private static Properties mailServerConfig = fetchConfig(); 
    
    /** Creates a new instance of ShopEmailer */
    public ShopEmailer() {
            mailServerConfig = fetchConfig(); 
    }
     /**
     * Open a specific text file containing mail server
     * parameters, and populate a corresponding Properties object.
     */
    private static Properties fetchConfig() { 
        try{
            //If possible, one should try to avoid hard-coding a path in this
            //manner; in a web application, one should place such a file in
            //WEB-INF, and access it using ServletContext.getResourceAsStream.
            //Another alternative is Class.getResourceAsStream.
            //This file contains the javax.mail config properties mentioned above.
            String host = PropertyManager.getValue(PropertyConstants.SMTP_HOST_KEY);
            String fromAddress = PropertyManager.getValue(PropertyConstants.SMTP_REPLY_TO_KEY);
            String port = PropertyManager.getValue(PropertyConstants.SMTP_PORT_KEY);
            mailServerConfig = new Properties();       
            mailServerConfig.put("mail.smtp.auth", "true"); 
            mailServerConfig.put("mail.smtp.port", port); 
            mailServerConfig.put("mail.transport.protocol", "smtp"); 
            mailServerConfig.put("mail.host", host);
            mailServerConfig.put("mail.from", fromAddress);         
            mailServerConfig.setProperty("mail.user", userId);
            mailServerConfig.setProperty("mail.password", password);

        } catch(Exception e) {
            logger.error("error at fetchConfig method: "+e);
        }
        return mailServerConfig;
    }
    
    public static boolean sendOrderNotification(UserShop userShop)  
    {
        String email = userShop.getEmail();
        String receiverEmail = email;
        long orderNumber = userShop.getShopReferenceNo();
        String name = userShop.getName();
        String address = userShop.getAddress();
        String contactNo = userShop.getContactNo();
        String orderDate = DateObjectHelper.getPrintedDate(new Date());
        
        StringBuffer mailContentBuffer = new StringBuffer();
        //mailContentBuffer.append("");
mailContentBuffer.append("<table width='800' border='0' cellpadding='0'>");
mailContentBuffer.append("<tr>");
mailContentBuffer.append("<td width='635'><a href='080808.com.my'>080808.com.my</a><br>Suite #337,MBE Sri Hartamas,<br>Desa Sri Hartamas,<br>50480 Kuala Lumpur,<br>Malaysia");
mailContentBuffer.append("<br>");
mailContentBuffer.append("E-Mail: <a href='mailto:sales@080808.com.my'>sales@080808.com.my </a><br>");
mailContentBuffer.append("Phone: +60 (17) 360 4337 <br>");
mailContentBuffer.append("Fax: +60 (3) 780 63194<br>");
mailContentBuffer.append("<br><br>");
mailContentBuffer.append("Invoice To:<br>");
mailContentBuffer.append(name+"<br>");
mailContentBuffer.append(address+"<br>");
mailContentBuffer.append("Phone: "+contactNo+"<br><br></td>");
mailContentBuffer.append("<td width='159' valign='top'><p>Order Date : "+orderDate+" </p>");
mailContentBuffer.append("<p>Order Reference No: "+orderNumber+"</p>");
mailContentBuffer.append("<p>Due Date : "+orderDate+"</p>");
mailContentBuffer.append("<p>Total Due : RM 68.00   </p></td>");
mailContentBuffer.append("</tr>");
mailContentBuffer.append("<tr>");
mailContentBuffer.append("<td><table width='613' border='1' cellpadding='1' cellspacing='1' >");
mailContentBuffer.append("<tr>");
mailContentBuffer.append("<td width='170'>Item:</td>");
mailContentBuffer.append("<td colspan='2'>couple08<br>Lovers Tee<br>");
 String description = userShop.getShopDescription();
StringTokenizer tokenizer = new StringTokenizer(description,",");
while (tokenizer.hasMoreTokens())
{
    String designSize = tokenizer.nextToken();
    designSize = designSize.replaceAll("f=","Girl Size :");
    designSize = designSize.replaceAll("m=","Guy Size :");
    mailContentBuffer.append(designSize+"<br>");
}
mailContentBuffer.append("</td>");                
//mailContentBuffer.append("Girl Size: girl_size<br>");
//mailContentBuffer.append("Guy Size: guy_size<br>		 </td>");
mailContentBuffer.append("</tr>");
mailContentBuffer.append("<tr>");
mailContentBuffer.append("<td>Price (RM):</td>");
mailContentBuffer.append("<td width='314' align='right'>Lovers Tee<br>");
mailContentBuffer.append("Delivery</td>");
mailContentBuffer.append("<td width='111' align='right'>68.00<br>");
mailContentBuffer.append("0.00</td>");
mailContentBuffer.append("</tr>");
mailContentBuffer.append("<tr>");
mailContentBuffer.append("<td>Total Due (RM): </td>");
mailContentBuffer.append("<td colspan='2' align='right'><strong> 68.00</strong></td>");
mailContentBuffer.append("</tr>");
mailContentBuffer.append("</table></td>");
mailContentBuffer.append("<td>&nbsp;</td>");
mailContentBuffer.append("</tr>");
mailContentBuffer.append("<tr>");
mailContentBuffer.append("<td>&nbsp;</td>");
mailContentBuffer.append("<td>&nbsp;</td>");
mailContentBuffer.append("</tr>");
mailContentBuffer.append("<tr>");
mailContentBuffer.append("<td><p><strong>NOTES: </strong></p>");
mailContentBuffer.append("<p>We  accept Funds Transfer via Maybank2u.com or Direct Deposit via Kawanku ATM to our Maybank account. </p>");
mailContentBuffer.append("<p><u>Funds Transfer via Maybank2u.com </u><br>Pay to: E SMART SOLUTIONS  <br>");
mailContentBuffer.append("Account Number : 5122 3132 8102<br>");
mailContentBuffer.append("Email : sales@080808.com.my <br>");
mailContentBuffer.append("Kindly use the email address above for confirmation of your payment. </p>");
mailContentBuffer.append("<p><u>Direct Deposit via Maybank Kawanku ATM</u><br>Pay to: E SMART SOLUTIONS <br>Account Number : 5122 3132 8102 <br>Please fax ( 03-78063194 ) or email ( <a href='mailto:sales@080808.com.my'>sales@080808.com.my </a> ) us the transaction slip together with your order reference number for payment confirmation. </p>");
mailContentBuffer.append("<p> For more information, please refer to <a href='http://080808.com.my/shop.do?act=guide'>http://080808.com.my/shop.do?act=guide</a></p></td>");
mailContentBuffer.append("<td>&nbsp;</td>");
mailContentBuffer.append("</tr>");
mailContentBuffer.append("<tr>");
mailContentBuffer.append("<td><p>&nbsp;</p>");
mailContentBuffer.append("<p>If we can be of any assistance, please do not hesitate to contact us by sending an email to <a href='mailto:sales@080808.com.my'>sales@080808.com.my</a>. We wish to thank you for your continued support. </p>");
mailContentBuffer.append("<p align='left'>Thank you for shopping with 080808.com.my</p>");
mailContentBuffer.append("<p align='left'><a href='080808.com.my'> </a> </p>    </td>");
mailContentBuffer.append("<td>&nbsp;</td>");
mailContentBuffer.append("</tr>");
mailContentBuffer.append("</table> ");
        
        String senderName = "080808.com.my";
        String senderEmail = "sales@080808.com.my";
        try{
        sendIndividualEmail(senderName, senderEmail,receiverEmail," Your order confirmation from "+domainName+" ",mailContentBuffer.toString());
        }catch (Exception e)
        {
            logger.error("Error in sending order confirmation email to " + receiverEmail);
            return false;
        }
        return true;
                        
    }
    
       /**
     * Send a single email.
     */
    private static void sendIndividualEmail(String fromName, String fromEmailAddr, String toEmailAddr, String subject, String mailBody) throws Exception {
         
        logger.info( "Getting authentication presetting");
        EmailAuthentication auth = new EmailAuthentication(userId,password); 
        logger.info( "Getting mail server config");
        Session session = Session.getDefaultInstance( mailServerConfig, auth); 
        //Session session = Session.getDefaultInstance( mailServerConfig, null);         
        session.setDebug(true); 
        logger.info( "Creating Message");
        MimeMessage message = new MimeMessage( session );  
        
        try {
            //the "from" address may be set in code, or set in the
            //config file under "mail.from" ; here, the latter style is used
            logger.info( "Preparing email");
            message.setFrom( new InternetAddress(fromEmailAddr) );
            //message.setFrom( new InternetAddress(systemSender, domainName) );        
            InternetAddress[] replyToList = new InternetAddress[1];
            replyToList[0] = new InternetAddress(fromEmailAddr, fromName); 
            message.setReplyTo(replyToList);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmailAddr));
            message.addRecipient(Message.RecipientType.BCC, new InternetAddress("sales@080808.com.my"));
            message.setSubject(subject);
            message.setContent(mailBody, "text/html");
            logger.info( "Sending email");
            Transport.send( message );
            logger.info( "Email sent!");
        } catch(AddressException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Address Exception Cannot send email"+ex);
            throw new MessagingException("Address Exception: "+ex);
        } catch(javax.mail.MessagingException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Messaging Exception Cannot send email"+ex);
            throw new MessagingException("Messaging Exception: "+ex);
        }
        
    }
    
}
