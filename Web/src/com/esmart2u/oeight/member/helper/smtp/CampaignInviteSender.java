/*
 * CampaignInviteSender.java
 *
 * Created on May 15, 2008, 5:18 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp;

import com.esmart2u.oeight.data.CampaignInvite;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.oeight.member.helper.smtp.composer.AbstractCampaignComposer;
import com.esmart2u.oeight.member.helper.smtp.composer.AbstractComposer;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer; 
import java.util.TimerTask;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;

/**
 *
 * @author meauchyuan.lee
 */
public class CampaignInviteSender {
    
    /** Creates a new instance of CampaignInviteSender */
    public CampaignInviteSender() {
    }
    
    private static Logger logger = Logger.getLogger(CampaignInviteSender.class);
    private static CampaignInviteSender instance = null;
    private static boolean working = false;
    private static String systemSenderAddress = PropertyManager.getValue(PropertyConstants.CAMPAIGN_INVITE_SYSTEM_SENDER_EMAIL);
    private static String systemSenderAlias = PropertyManager.getValue(PropertyConstants.CAMPAIGN_INVITE_SYSTEM_SENDER_ALIAS);
    private static String userId = PropertyManager.getValue(PropertyConstants.SMTP_USER_KEY);
    private static String password = PropertyManager.getValue(PropertyConstants.SMTP_PASS_KEY);
    private static String systemReplyTo = PropertyManager.getValue(PropertyConstants.SMTP_DISPLAY_SENDER);
    private static String homeSite = "http://" + ConfigurationHelper.getDomainName();
    
    private static Properties mailServerConfig = new Properties(); 

    private final Timer timer = new Timer();
     
    private static boolean fromStartup = false;
    private boolean firstTime = true;
    private static boolean enabled = PropertyManager.getBoolean(PropertyConstants.CAMPAIGN_INVITE_ENABLE,false); 
    // Defaults 1 minute
    private final int minutes = PropertyManager.getInt(PropertyConstants.CAMPAIGN_INVITE_FREQUENCY,10); 

    public static synchronized CampaignInviteSender getInstance(){
        if ( instance == null ){
            instance = new CampaignInviteSender();
            instance.mailServerConfig = fetchConfig();
            
            if (enabled){
                instance.start();
            }
        }
        return instance;
    }
    
    
    public static void refreshConfig() {
        mailServerConfig.clear();
        fetchConfig();
    }
    
    /**
     * Open a specific text file containing mail server
     * parameters, and populate a corresponding Properties object.
     */
    private static Properties fetchConfig() { 
        try{
            //If possible, one should try to avoid hard-coding a path in this
            //manner; in a web application, one should place such a file in
            //WEB-INF, and access it using ServletContext.getResourceAsStream.
            //Another alternative is Class.getResourceAsStream.
            //This file contains the javax.mail config properties mentioned above.
            String host = PropertyManager.getValue(PropertyConstants.SMTP_HOST_KEY);
            String fromAddress = PropertyManager.getValue(PropertyConstants.SMTP_REPLY_TO_KEY);
            String port = PropertyManager.getValue(PropertyConstants.SMTP_PORT_KEY);
            mailServerConfig = new Properties();       
            mailServerConfig.put("mail.smtp.auth", "true"); 
            mailServerConfig.put("mail.smtp.port", port); 
            mailServerConfig.put("mail.transport.protocol", "smtp"); 
            mailServerConfig.put("mail.host", host);
            mailServerConfig.put("mail.from", fromAddress);         
            mailServerConfig.setProperty("mail.user", userId);
            mailServerConfig.setProperty("mail.password", password);

        } catch(Exception e) {
            logger.error("error at fetchConfig method: "+e);
        }
        return mailServerConfig;
    }
    
    public void invoke(boolean fromStart) {
        //static boolean initalizer = fromStart;
        this.fromStartup = fromStart; 
    }
    
    public static void invoke() {
        logger.debug("CampaignInviteSender invoke called");
        if (!working) {
            logger.debug("Going to start work");
            working = true; 
            sendMails(null); 
            logger.debug("Done working, setting to rest");
            working = false;
        }
    }
    
    private static void sendMails(org.hibernate.Session session)
    {
            if (session == null || !session.isOpen()){
                session = HibernateUtil.getSessionFactory().openSession();
            }
    
            // Get email updates list
            List emailUpdateList = (List) session.createCriteria(CampaignInvite.class) 
            .add(Expression.eq( "status", OEightConstants.MAIL_STATUS_NEW)).setMaxResults(10).list();
    
            if (emailUpdateList != null && !emailUpdateList.isEmpty())
            {
                Date now = new Date();
                for(int i=0;i<emailUpdateList.size();i++)
                {
                    CampaignInvite campaignInvite = (CampaignInvite)emailUpdateList.get(i);
                    String fromEmail = campaignInvite.getUserEmail();
                    String toEmail = campaignInvite.getEmailAddress();
                    int updateCode = campaignInvite.getCampaignCode();
                    Class abstractClass = null;
                    AbstractCampaignComposer composer = null; 
                    String senderEmail = null; 
                    String recipientEmail = toEmail;
                    
                    try {

                        session.beginTransaction();
                        // Try get the content of the email from individual sender
                        String className = PropertyManager.getValue(PropertyConstants.CAMPAIGN_INVITE_COMPOSER_PREFIX + updateCode + PropertyConstants.CAMPAIGN_INVITE_COMPOSER_POSTFIX);
                        abstractClass = Class.forName(className);
                        composer = (AbstractCampaignComposer)abstractClass.newInstance(); 
                        
                        // Get email related values
                        String subject = composer.getEmailSubject(campaignInvite);
                        String content = composer.createEmailContent(campaignInvite);
                        
                        senderEmail = fromEmail;
                        // recipient email should be friends email, (to be implemented) 
                        
                        
                        // Send email  
                        String mailContent = "";
                        mailContent += "<br><br>" + content;
                        mailContent += "<br><br>Best regards and wishes,<br>The "+ ConfigurationHelper.getDomainName()+" Team<br>";
                        mailContent += "<br><br>This is a system generated email, please do not reply to this email address.";
                        // Opt out indication
                        //"To ensure you receive the latest updates from 080808, add pr@080808.com.my to your safe-sender list."
                        //"To unsubscribe/subscribe to this newsletter, login to http://080808.com.my first, then edit your preference at Profile Menu -> Personal Details.";

                        //mailContent += "Login at <a href=\"" + ConfigurationHelper.getDomainName() + "\">" + ConfigurationHelper.getDomainName() + "</a> and go to Profile Menu -> Wishers.";
        
                        sendIndividualEmail(senderEmail,recipientEmail,subject,mailContent);
                        
                        campaignInvite.setStatus(OEightConstants.MAIL_STATUS_SENT);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        logger.error("Error sending campaign invite to " + toEmail); 
                        campaignInvite.setStatus(OEightConstants.MAIL_STATUS_SEND_FAILED);
                    }
                    
                    // Cleanup
                    abstractClass = null;
                    composer = null;
                    

                    // Update db
                    campaignInvite.setDateEmailed(now);
                    session.save(campaignInvite);  
                    session.getTransaction().commit();
                    //commitSession(session);
                    logger.debug("Email Update sent to " + toEmail);
                    
                    // Sleep for 5 second for every 10 mails/threads
                    // Check committing
                    if (i%10 == 0)
                    {
                        try { 
                            logger.debug("Sleeping for 10 second");
                            Thread.sleep(10 * 1000);
                        } catch (InterruptedException ex) {
                            //ex.printStackTrace();
                            logger.debug("Error while sleeping : " + ex);
                        }
                    }
                }
                
                
              // Sleep for 2 minutes for every bulk of 10 mails sent  - SKIP if from startup, so no wait
                if (!fromStartup){
                    try { 
                        logger.debug("Sleeping for 2 minutes");
                        Thread.sleep(2 * 60 * 1000);
                    } catch (InterruptedException ex) {
                        //ex.printStackTrace();
                        logger.debug("Error while sleeping : " + ex);
                    } 
                }
                else
                {
                    fromStartup = false;
                }
                
                
                // Do a send mail to continue to no mails left
                sendMails(session);
            }
            else
            {
                logger.debug("Done sending, no more campaign invites left on " + new Date());
                logger.debug("Stopping campaign invites sender daemon...");
                working = false;
            } 
           
            
    }
    
    /**
     * Send a single email.
     */
    private static void sendIndividualEmail(String fromEmailAddr, String toEmailAddr, String subject, String mailBody) throws Exception {
         
        //logger.debug( "Getting authentication presetting");
        EmailAuthentication auth = new EmailAuthentication(userId,password); 
        //logger.debug( "Getting mail server config");
        Session session = Session.getDefaultInstance( mailServerConfig, auth); 
        
        //Session session = Session.getDefaultInstance( mailServerConfig, null);         
        session.setDebug(true); 
        //logger.debug( "Creating Message");
        MimeMessage message = new MimeMessage( session );  
        
        try {
            //the "from" address may be set in code, or set in the
            //config file under "mail.from" ; here, the latter style is used
            //logger.debug( "Preparing email"); 
            
            message.setFrom( new InternetAddress(systemSenderAddress,systemSenderAlias) );
            InternetAddress[] replyTo = new InternetAddress[1];
            replyTo[0] = new InternetAddress(fromEmailAddr);
            message.setReplyTo( replyTo );
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmailAddr));
            message.setSubject(subject);
            message.setContent(mailBody, "text/html");
            logger.debug( "Sending email");
            Transport.send( message );
            logger.debug( "Email sent!");
        } catch(AddressException ex) {
            ex.printStackTrace();
            System.err.println("Cannot send email. " + ex);
            logger.error("Address Exception Cannot send email"+ex);
            throw new MessagingException("Address Exception: "+ex);
        } catch(javax.mail.MessagingException ex) {
            ex.printStackTrace();
            System.err.println("Cannot send email. " + ex);
            logger.error("Messaging Exception Cannot send email"+ex);
            throw new MessagingException("Messaging Exception: "+ex);
        }
        
    }
    
    private static void commitSession(org.hibernate.Session session)
    { 
             try {
                if (!session.getTransaction().wasCommitted() && session.getTransaction().isActive())
                {
                    session.getTransaction().commit();
                    logger.info("sendMails - committed"); 
                }
            } catch (HibernateException ex) {
                ex.printStackTrace();
                logger.error("Error in committing");
            } 
    }
       
    /**
     * Invocation of the daemon, place the scheduling inside (firstTime) condition
     * if required to be run once only - for future resource saving
     */
    private void start() {
        if (enabled){
            long runningTime = minutes * 60 * 1000;
            if (firstTime){
                runningTime = 100;
                firstTime = false; 
            }

            // Schedule the run
            timer.schedule(new TimerTask() {
                public void run() {
                    logger.debug("Timer task starting at " + new Date());
                    getInstance().invoke();
                    logger.debug("Sent emails at " + new Date()); 
                    getInstance().start();
                } 
            }, runningTime);
        }
    }

}
