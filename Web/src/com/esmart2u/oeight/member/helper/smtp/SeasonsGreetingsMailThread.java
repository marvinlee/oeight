/*
 * SeasonsGreetingsMailThread.java
 *
 * Created on December 9, 2007, 12:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author meauchyuan.lee
 */
public class SeasonsGreetingsMailThread extends Thread {
     
    private static Logger logger = Logger.getLogger(SeasonsGreetingsMailThread.class);
    private String userId = null;
    private String password = null;
    private String systemSender = null;
    private String domainName = null;
    private Properties mailServerConfig = null;
    private String fromName = null;
    private String fromEmailAddr = null;
    private String toEmailAddr = null;
    private String subject = null;
    private String mailBody = null;
    
    /** Creates a new instance of SeasonsGreetingsMailThread */
    public SeasonsGreetingsMailThread() {
    }
    
    // This method is called when the thread runs
    public void run() {
        
        logger.info( "Getting authentication presetting");
        EmailAuthentication auth = new EmailAuthentication(userId,password); 
        logger.info( "Getting mail server config");
        Session session = Session.getDefaultInstance( mailServerConfig, auth); 
        //Session session = Session.getDefaultInstance( mailServerConfig, null);         
        session.setDebug(true); 
        logger.info( "Creating Message");
        MimeMessage message = new MimeMessage( session );  
        
        try {
            //the "from" address may be set in code, or set in the
            //config file under "mail.from" ; here, the latter style is used
            logger.info( "Preparing email");
            //message.setFrom( new InternetAddress(fromEmailAddr) );
            message.setFrom( new InternetAddress(systemSender, domainName) );        
            InternetAddress[] replyToList = new InternetAddress[1]; 
            replyToList[0] = new InternetAddress(fromEmailAddr, fromName); 
            message.setReplyTo(replyToList);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmailAddr));
            message.setSubject(subject);
            message.setContent(mailBody, "text/html");
            logger.info( "Sending email");
            Transport.send( message );
            logger.info( "Email sent!");
        } catch(AddressException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Address Exception Cannot send email"+ex);
            //throw new MessagingException("Address Exception: "+ex);
        } catch(MessagingException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Messaging Exception Cannot send email"+ex);
            //throw new MessagingException("Messaging Exception: "+ex);
        } catch (UnsupportedEncodingException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("UnsupportedEncodingException Cannot send email"+ex);
        } 
        
    } 
    
    public void setMailSessionValues(String userId, String password, String systemSender, String domainName, Properties mailServerConfig)
    { 
        this.userId = userId;
        this.password = password;
        this.systemSender = systemSender;
        this.domainName = domainName;
        this.mailServerConfig =  mailServerConfig; 
    }
    
    public void setIndividualEmailValues(String fromName, String fromEmailAddr, String toEmailAddr, String subject, String mailBody) throws Exception 
    {
        this.fromName = fromName;
        this.fromEmailAddr = fromEmailAddr;
        this.toEmailAddr = toEmailAddr;
        this.subject = subject;
        this.mailBody = mailBody;
        
    }    
   
    
}
