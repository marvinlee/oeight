/*
 * EmailAuthentication.java
 *
 * Created on October 21, 2007, 6:35 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp;

import com.esmart2u.oeight.member.helper.*;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
/**
 *
 * @author meauchyuan.lee
 */
public class EmailAuthentication  extends Authenticator {
    private String name;
    private String password;
    
    public EmailAuthentication(String name, String pwd) {
        this.name = name;
        this.password = pwd;
    }
    
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(name,password);
    }
    
}
