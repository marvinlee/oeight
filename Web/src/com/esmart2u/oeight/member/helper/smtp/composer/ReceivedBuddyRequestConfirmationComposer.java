/*
 * ReceivedBuddyRequestConfirmationComposer.java
 *
 * Created on April 28, 2008, 3:11 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp.composer;

import com.esmart2u.oeight.data.EmailUpdate;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.StringUtils;
/**
 *
 * @author meauchyuan.lee
 */
public class ReceivedBuddyRequestConfirmationComposer  implements AbstractComposer {
    
    
    /** Creates a new instance of ReceivedBuddyRequestConfirmationComposer */
    public ReceivedBuddyRequestConfirmationComposer() {
    }
    
    public String getEmailSubject(EmailUpdate emailUpdate) {
        return "Buddy request confirmed";
    }

    public String createEmailContent(EmailUpdate emailUpdate) {
        String content = "";
        UserBO userBO = new UserBO();
        User user = userBO.getUserById(""+emailUpdate.getUserId());
        if (user != null){
            String userName = user.getUserCountry().getName();
            if (StringUtils.hasValue(userName)) {
                content+= "Dear " + userName + ",<br><br>";
            }
        }
        user = null;
        
        String acceptingBuddyId = emailUpdate.getCustomField1();
        User buddy = userBO.getUserById(acceptingBuddyId);
        
        String userName = "Your friend";
        if (buddy != null)
        {
            userName = buddy.getUserCountry().getName();
        }
        
        
        content+= userName + " has confirmed your buddy request and joined your list of buddies.<br>";
        content+= "Login to <a href=\"" + ConfigurationHelper.getDomainName() + "\">" + ConfigurationHelper.getDomainName() + "</a> and go to Profile Menu -> My Buddies.";
         
        userBO = null;
        
        return content;
    }

    public boolean useSystemEmailForReply() {
        return true; // Sender is System
    }

    public boolean sendToFriends() {
        return false; // Send to single person
    }
}
