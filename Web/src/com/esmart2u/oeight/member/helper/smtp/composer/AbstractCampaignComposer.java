/*
 * AbstractCampaignComposer.java
 *
 * Created on May 15, 2008, 5:33 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp.composer;

import com.esmart2u.oeight.data.CampaignInvite;

/**
 *
 * @author meauchyuan.lee
 */
public interface AbstractCampaignComposer {
    
   
    // Get email subject
    String getEmailSubject(CampaignInvite campaignInvite);
    
    // Get email content
    String createEmailContent(CampaignInvite campaignInvite);
    
    // Flag to determine if use system email (in REPLY-TO)
    boolean useSystemEmailForReply();
     
}
