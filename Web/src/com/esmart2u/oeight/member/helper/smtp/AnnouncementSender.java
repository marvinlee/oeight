/*
 * AnnouncementSender.java
 *
 * Created on 26 January 2008, 01:11
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp;

import com.esmart2u.oeight.data.Mail1;
import com.esmart2u.oeight.member.helper.OEightConstants;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;

/**
 *
 * @author marvin
 */
public class AnnouncementSender {
    private static Logger logger = Logger.getLogger(AnnouncementSender.class);
    private static AnnouncementSender instance = null;
    private static boolean working = false;
    private static String userId = PropertyManager.getValue(PropertyConstants.SMTP_USER_KEY);
    private static String password = PropertyManager.getValue(PropertyConstants.SMTP_PASS_KEY);
    private static String systemSender = PropertyManager.getValue(PropertyConstants.SMTP_DISPLAY_SENDER);
    private static String domainName = ConfigurationHelper.getDomainName();
    private static Properties mailServerConfig = new Properties(); 

    private final Timer timer = new Timer();
     
    private static boolean fromStartup = false;
    private boolean firstTime = true;
    private static boolean enabled = true; 
    // Defaults 10 mins
    private final int minutes = 1; 
    
    // Read from file or coded
    private static boolean readFromFile = true;//false;
    private static String inputFile = "D:/Temp/marvin/site/mailing_list/nov_ecoknights.html";

    public static synchronized AnnouncementSender getInstance(){
        if ( instance == null ){
            instance = new AnnouncementSender();
            instance.mailServerConfig = fetchConfig(); 
            if (enabled){
                instance.start();
            }
        }
        return instance;
    }
    
    
    public static void refreshConfig() {
        mailServerConfig.clear();
        fetchConfig();
    }
    
    /**
     * Open a specific text file containing mail server
     * parameters, and populate a corresponding Properties object.
     */
    private static Properties fetchConfig() { 
        try{
            //If possible, one should try to avoid hard-coding a path in this
            //manner; in a web application, one should place such a file in
            //WEB-INF, and access it using ServletContext.getResourceAsStream.
            //Another alternative is Class.getResourceAsStream.
            //This file contains the javax.mail config properties mentioned above.
            String host = PropertyManager.getValue(PropertyConstants.SMTP_HOST_KEY);
            String fromAddress = PropertyManager.getValue(PropertyConstants.SMTP_REPLY_TO_KEY);
            String port = PropertyManager.getValue(PropertyConstants.SMTP_PORT_KEY);
            mailServerConfig = new Properties();       
            mailServerConfig.put("mail.smtp.auth", "true"); 
            mailServerConfig.put("mail.smtp.port", port); 
            mailServerConfig.put("mail.transport.protocol", "smtp"); 
            mailServerConfig.put("mail.host", host);
            mailServerConfig.put("mail.from", fromAddress);         
            mailServerConfig.setProperty("mail.user", userId);
            mailServerConfig.setProperty("mail.password", password);

        } catch(Exception e) {
            logger.error("error at fetchConfig method: "+e);
        }
        return mailServerConfig;
    }
    
    public void invoke(boolean fromStart) {
        //static boolean initalizer = fromStart;
        this.fromStartup = fromStart; 
    }
    
    public static void invoke() {
        logger.info("AnnouncementSender invoke called");
        if (!working) {
            logger.info("Going to start work");
            working = true; 
            sendMails(null); 
            logger.info("Done working, setting to rest");
            working = false;
        }
    }
    
    private static void sendMails(org.hibernate.Session session)
    {
            if (session == null || !session.isOpen()){
                // Old
                session = HibernateUtil.getSessionFactory().openSession();
                //session.beginTransaction();
                 
            }
    
            List mailOneList = (List) session.createCriteria(Mail1.class) 
            .add(Expression.eq( "status", OEightConstants.MAIL_STATUS_NEW)).setMaxResults(90).list();
    
            if (mailOneList != null && !mailOneList.isEmpty())
            {
                Date now = new Date();
                for(int i=0;i<mailOneList.size();i++)
                {
                    String mailContent = "";
                    // Transaction for each record
                    session.beginTransaction();
                    Mail1 mailOne = (Mail1)mailOneList.get(i);
                    String receiverEmail = mailOne.getEmail();
                    
                    try {

                        // Send email 
                        
                        if (readFromFile){
                            BufferedReader in = new BufferedReader(new FileReader(inputFile));
                            String str;
                            while ((str = in.readLine()) != null) {  
                                mailContent += str;
                            }
                            in.close(); 
                        }
                        else{
         
                        }         


                        String senderName = "Public Relations";
                        String senderEmail = "pr@080808.com.my";
                        //sendIndividualEmail(senderName, senderEmail,receiverEmail," Friendster App - "+domainName+" newsletter",mailContent);
                        sendIndividualEmail(senderName, senderEmail,receiverEmail,"Events from 080808",mailContent);                        
                        
                        mailOne.setStatus(OEightConstants.MAIL_STATUS_SENT); 
                        
                    } catch (Exception ex) {
                        logger.error("Error sending invitation to " + receiverEmail); 
                        mailOne.setStatus(OEightConstants.MAIL_STATUS_SEND_FAILED);
                    }

                    // Update db
                    mailOne.setDateMailed(now);
                    session.save(mailOne);  
                    commitSession(session);
                    logger.debug("Mail sent to " + receiverEmail);
                    
                    // Sleep for 5 second for every 10 mails/threads
                    // Check committing
                    if (i%2 == 0)
                    {
                        try { 
                            logger.debug("Sleeping for 10 second");
                            Thread.sleep(20 * 1000);
                        } catch (InterruptedException ex) {
                            //ex.printStackTrace();
                            logger.debug("Error while sleeping : " + ex);
                        }
                    }
                }
                
              // Sleep for 5 minutes for every bulk of 90 mails sent  - SKIP if from startup, so no wait
                if (!fromStartup){
                    try { 
                        System.out.println("Sleeping for 5 minutes");
                        logger.debug("Sleeping for 5 minutes");
                        Thread.sleep(5 * 60 * 1000);
                    } catch (InterruptedException ex) {
                        //ex.printStackTrace();
                        logger.debug("Error while sleeping : " + ex);
                    } 
                }
                else
                {
                    fromStartup = false;
                }
                
                // Do a send mail to continue to no mails left
                sendMails(session);
            }
            else
            {
                logger.info("Done sending, rest a while....");
                working = false;
            }
    
            /*
             try {
                if (!session.getTransaction().wasCommitted() && session.getTransaction().isActive())
                {
                    session.getTransaction().commit();
                    logger.info("sendMails - committed"); 
                }
            } catch (HibernateException ex) {
                ex.printStackTrace();
                logger.error("Error in committing");
            }
             */
            
    }
    
    /**
     * Send a single email.
     */
    private static void sendIndividualEmail(String fromName, String fromEmailAddr, String toEmailAddr, String subject, String mailBody) throws Exception {
         
        logger.info( "Getting authentication presetting");
        EmailAuthentication auth = new EmailAuthentication(userId,password); 
        logger.info( "Getting mail server config");
        Session session = Session.getDefaultInstance( mailServerConfig, auth); 
        //Session session = Session.getDefaultInstance( mailServerConfig, null);         
        session.setDebug(true); 
        logger.info( "Creating Message");
        MimeMessage message = new MimeMessage( session );  
        
        try {
            //the "from" address may be set in code, or set in the
            //config file under "mail.from" ; here, the latter style is used
            logger.info( "Preparing email");
            message.setFrom( new InternetAddress(fromEmailAddr) );
            //message.setFrom( new InternetAddress(systemSender, domainName) );        
            InternetAddress[] replyToList = new InternetAddress[1];
            replyToList[0] = new InternetAddress(fromEmailAddr, fromName); 
            message.setReplyTo(replyToList);
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmailAddr));
            message.setSubject(subject);
            message.setContent(mailBody, "text/html");
            logger.info( "Sending email");
            Transport.send( message );
            logger.info( "Email sent!");
        } catch(AddressException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Address Exception Cannot send email"+ex);
            throw new MessagingException("Address Exception: "+ex);
        } catch(javax.mail.MessagingException ex) {
            System.err.println("Cannot send email. " + ex);
            logger.error("Messaging Exception Cannot send email"+ex);
            throw new MessagingException("Messaging Exception: "+ex);
        }
        
    }
    
      
    private void start() {
        if (enabled){
            long runningTime = minutes * 60 * 1000;
            if (firstTime){
                runningTime = 100;
                firstTime = false; 
            }

            timer.schedule(new TimerTask() {
                public void run() {
                    logger.info("Timer task starting at " + new Date());
                    getInstance().invoke();
                    logger.debug("Sent emails at " + new Date()); 
                    getInstance().start();
                } 
            }, runningTime);
        }
    }
    
    private static void commitSession(org.hibernate.Session session)
    { 
             try {
                if (!session.getTransaction().wasCommitted() && session.getTransaction().isActive())
                {
                    session.getTransaction().commit();
                    logger.info("sendMails - committed"); 
                }
            } catch (HibernateException ex) { 
                session.getTransaction().rollback();
                ex.printStackTrace();
                logger.error("Error in committing"); 
            } finally {
                //session.close();
            }
    }
    
    public static void main(String[] args) {
        String propertyFile = ConfigurationHelper.getInstance().getPropertyValue("config.startup.path");
        System.setProperty(PropertyConstants.MAIN_CONFIG_FILE_KEY,propertyFile);
                           //PropertyConstants.MAIN_CONFIG_FILE_VALUE);
            
        AnnouncementSender.getInstance().invoke(true);
    }
    
    /*
Previous mails
Valentine
     
String mailContent = "<p><center><a href=\"http://080808.com.my/shop.do\" target=\"#\"><img src=\"http://080808.org.my/shop/val08/valentine1t.gif\" border=\"0\"></a></center></p>";
mailContent += "<p>Fancy some nice Valentine gift for your loved one?</p>";
mailContent += "<p>Ever thought of getting a special HIM and HER lovers' tee?</p>";
mailContent += "<p>This Valentine's Day, 080808.com.my is launching <a onclick=\"window.open(this.href, '_blank', 'width=800,height=600,scrollbars=no,resizable=no,toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0'); return false\" href=\"http://080808.com.my/images/shop/lovers/lovers_tee.jpg\"><img title=\"Lovers_tee\" height=\"75\" alt=\"Lovers_tee\" src=\"http://080808.com.my/images/shop/lovers/lovers_tee_t.jpg\" width=\"100\" border=\"0\" style=\"FLOAT: left; MARGIN: 0px 5px 5px 0px\" /></a>something special.</p>";
mailContent += "<p></p>";
mailContent += "<p></p>";
mailContent += "<p>Presenting the Lovers' Tee, a pair of couple t-shirts with attractive guy and girl design thinking of each other.</p>";
mailContent += "<p>The shirts are of limited numbers so that you can be sure it is truly unique for you to &quot;make a mark&quot; this Valentine's day.&nbsp; </p>";
mailContent += "<p>For only RM 49.90, you get 2 professionally designed t-shirts so that you can show your loved ones how special they are to you.</p>";
mailContent += "<p>Bear in mind though, the shirts are very limited, and they are selling fast. Check out more photos at <a href=\"http://080808.com.my/shop.do?act=lovers\">http://080808.com.my/shop.do?act=lovers</a></p>";
mailContent += "<p>Happy Valentine's Day!</p>";
mailContent += "<p>The <a href=\"http://080808.com.my\">080808.com.my</a> Team</p>";     
     
     
//     URL is something like this http://dummy-host3.acskl.com/invite.do?act=invited&invitedBy=test1&inviteCode=997e56d3fb8fe13a75ae4c1b62e80e95

/* Was for invitation email
String inviteString = "http://"+ ConfigurationHelper.getDomainName();
String mosaicString = "http://"+ ConfigurationHelper.getDomainName() + "/home.jsp";
String domainLink = "<a href='" + inviteString + "'>" + inviteString + "</a>";
String mosaicLink = "<a href='" + mosaicString + "'>080808.com.my Mosaic</a>";

String mailContent = "Greetings from 080808.com.my,<br><br> Are you ready for the next big thing in 2008?<br><br>";
mailContent += "<br><br>We are very glad to announce the soft-launching of our website specially to you.";
mailContent += "<br><br>Please visit our website at "+domainLink+" to participate and submit the your best photo for the mosaic.";
mailContent += "<br><br>Our objective is to fill the mosaic ("+mosaicLink+") with many happy faces :)";
mailContent += "<br><br>We're looking forward to your participation in this meaningful 1st in Malaysia project!";


mailContent += "<br><br>Best regards and wishes,<br>The "+ ConfigurationHelper.getDomainName()+" Team<br>";
mailContent += "<br><br>This is a system generated email, you are not required to reply to this email address.";
  
*/
}
