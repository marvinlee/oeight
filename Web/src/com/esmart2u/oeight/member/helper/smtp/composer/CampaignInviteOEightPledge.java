/*
 * CampaignInviteOEightPledge.java
 *
 * Created on May 15, 2008, 7:37 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.helper.smtp.composer;

import com.esmart2u.oeight.data.CampaignInvite;
import com.esmart2u.oeight.data.User;
import com.esmart2u.oeight.member.bo.UserBO;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.StringUtils;

/**
 *
 * @author meauchyuan.lee
 */
public class CampaignInviteOEightPledge implements AbstractCampaignComposer {
    
    /** Creates a new instance of CampaignInviteOEightPledge */
    public CampaignInviteOEightPledge() {
    }
    
    public String createEmailContent(CampaignInvite campaignInvite) {  
        String content = "";
        String inviteLink = "";
        String inviteURL = "";
        UserBO userBO = new UserBO();
        User user = userBO.getUserById(""+campaignInvite.getUserId());
        if (user != null){
            String userName = user.getUserCountry().getName();
            if (StringUtils.hasValue(userName)) {
                inviteLink+= userName + " made a pledge to help address Climate Change.<br><br>";
                inviteLink+= userName + " would like you to join in this effort too.<br><br>";
                inviteURL = "http://" +  ConfigurationHelper.getDomainName() + "/earth.do?act=pledgeMake&from=" + campaignInvite.getUserEmail() + "&to="+ campaignInvite.getEmailAddress();
            }
            else
            {
                inviteURL = "http://" +  ConfigurationHelper.getDomainName() + "/earth.do?act=pledgeMake";
            }
        }
        user = null;
        userBO = null;
        //content+= "Enough said.<br><br>Enough said about global warming.<br>Enough said about climate change.<br><br>";
        //content+= "You've read it from papers.<br>You've seen it in the news.<br><br>";
        //content+= "What's the real deal?<br>It's not about EARTH.<br>It's about us!<br><br>";
        //content+= "Help ourselves, help ourselves fast.<br><br>";
        content+= "Be a Hero, make a mark for Climate Change.<br><br>";
        
        content+= inviteLink;
        content+= "Click on the link below to make your pledge:<br><br>";
        content+= "<a href=\""+inviteURL+"\">" + inviteURL + "</a><br><br>";
        content+= "By the way, we also are giving out ecobuttons to selected winners.<br>For more details, go to <a href=\"http://earth.080808.com.my\">http://earth.080808.com.my</a><br><br>";
        //content+= "<br><br>Best regards and wishes,<br>The "+ ConfigurationHelper.getDomainName()+" Team<br>";
        return content;
    }

    public String getEmailSubject(CampaignInvite campaignInvite) {
        String from = campaignInvite.getUserEmail();
        return from + " invites you to make a mark for Climate Change";
    }
 
    public boolean useSystemEmailForReply() {
        return false;
    }
}
