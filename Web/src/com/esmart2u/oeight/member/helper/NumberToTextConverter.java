package com.esmart2u.oeight.member.helper;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;

import com.esmart2u.solution.base.logging.Logger;

/**
 * com.esmart2u.bca.member.consumer.service
 *
 * @author Chow Chun Keong
 * @version $Id: NumberToTextConverter.java,v 1.3.2.1 2005/05/09 10:08:04 cclim Exp $
 */
public class NumberToTextConverter {
    private Logger logger = Logger.getLogger(NumberToTextConverter.class);

    // Editable. You can change the mapping to any language.
    public static final String One = "Neng";
    public static final String Two = "Song";
    public static final String Three = "Sam";
    public static final String Four = "Si";
    public static final String Five = "Ha";
    public static final String Six = "Hok";
    public static final String Seven = "Jiet";
    public static final String Eight = "Pat";
    public static final String Nine = "Kao";
    public static final String Twenty = "Yi Sip";
    public static final String Thirty = "Sam Sip";
    public static final String Forty = "Si Sip";
    public static final String Fifty = "Ha Sip";
    public static final String Sixty = "Hok Sip";
    public static final String Seventy = "Jiet Sip";
    public static final String Eighty = "Pat Sip";
    public static final String Ninety = "Kao Sip";
    public static final String Ten = "Sip";
    public static final String Eleven = "Sip Aik";
    public static final String Twelve = "Sip Song";
    public static final String Thirteen = "Sip Sam";
    public static final String Fourteen = "Sip Si";
    public static final String Fifteen = "Sip Ha";
    public static final String Sixteen = "Sip Hok";
    public static final String Seventeen = "Sip Jiet";
    public static final String Eighteen = "Sip Pat";
    public static final String Nineteen = "Sip Kao";
    public static final String One_Hundred = "Neng Roi";
    public static final String Hundred = "Pan";
    public static final String Thousand = "Thousand";
    public static final String Million = "Million";
    public static final String Billion = "Billion";
    public static final String Trillion = "Trillion";
    public static final String Currency = "Baht";
    public static final String Cent = "Cent";
    public static final String And = "And";
    public static final String No_Value = "No Value";
    // End of Editable

    //Thai Amount
//    private final String UNIT[] = {"San","","Sib","Roy","Pan","Muan","Lan"};
//    private final String DIGIT[] = {"Zero","One","Two","Three","Four","Five","Six","Seven","Eight","Nine"};
//    private final String EXTRA_DIGIT[] = {"Ed","Yee"};

//    public final String BAHT = "Baht";
//    public final String SATANG = "Satang";
    private final String THAI_UNIT[] = {"&#3649;&#3626;&#3609;",
                                        "",
                                        "&#3626;&#3636;&#3610;",
                                        "&#3619;&#3657;&#3629;&#3618;",
                                        "&#3614;&#3633;&#3609;",
                                        "&#3627;&#3617;&#3639;&#3656;&#3609;",
                                        "&#3621;&#3657;&#3634;&#3609;"};
    private final String THAI_DIGIT[] = {"&#3624;&#3641;&#3609;&#3618;&#3660;",
                                        "&#3627;&#3609;&#3638;&#3656;&#3591;",
                                        "&#3626;&#3629;&#3591;",
                                        "&#3626;&#3634;&#3617;",
                                        "&#3626;&#3637;&#3656;",
                                        "&#3627;&#3657;&#3634;",
                                        "&#3627;&#3585;",
                                        "&#3648;&#3592;&#3655;&#3604;",
                                        "&#3649;&#3611;&#3604;",
                                        "&#3648;&#3585;&#3657;&#3634;"};
    private final String THAI_EXTRA_DIGIT[] = {"&#3648;&#3629;&#3655;&#3604;",
                                                "&#3618;&#3637;&#3656;"};

    public final String BAHT = "&#3610;&#3634;&#3607;";
    public final String SATANG = "&#3626;&#3605;&#3634;&#3591;&#3588;&#3660;";
    public final String Only = "&#3606;&#3657;&#3623;&#3609;";

    //

    // Main class
    // For testing purposes only
    public static void main(String argsp[])
        throws Exception {
        NumberToTextConverter numToTxtConv = new NumberToTextConverter();
//        String i = numToTxtConv.getTextNumberWithoutCurrencyDescription("121,910,801,194,908");
        String i = numToTxtConv.getThaiTextNumber("1000.50");
        System.out.println("[getTextNumber] = " + i);
    }

    // Default Constructor
//    public void NumberToTextConverter()
//    {
//    }

    private String d1(char x) { // single digit terms
        String n = "";
        switch (Integer.parseInt(String.valueOf(x))) {
            case 0:
                n = "";
                break;
            case 1:
                n = One;
                break;
            case 2:
                n = Two;
                break;
            case 3:
                n = Three;
                break;
            case 4:
                n = Four;
                break;
            case 5:
                n = Five;
                break;
            case 6:
                n = Six;
                break;
            case 7:
                n = Seven;
                break;
            case 8:
                n = Eight;
                break;
            case 9:
                n = Nine;
                break;
            default:
                n = "Not a Number";
        }
        return n;
    }

    private String d2(char x) { // 10x digit terms
        String n = "";
        switch (Integer.parseInt(String.valueOf(x))) {
            case 0:
                n = "";
                break;
            case 1:
                n = "";
                break;
            case 2:
                n = Twenty;
                break;
            case 3:
                n = Thirty;
                break;
            case 4:
                n = Forty;
                break;
            case 5:
                n = Fifty;
                break;
            case 6:
                n = Sixty;
                break;
            case 7:
                n = Seventy;
                break;
            case 8:
                n = Eighty;
                break;
            case 9:
                n = Ninety;
                break;
            default:
                n = "Not a Number";
        }
        return n;
    }

    private String d3(char x) { // teen digit terms
        String n = "";
        switch (Integer.parseInt(String.valueOf(x))) {
            case 0:
                n = Ten;
                break;
            case 1:
                n = Eleven;
                break;
            case 2:
                n = Twelve;
                break;
            case 3:
                n = Thirteen;
                break;
            case 4:
                n = Fourteen;
                break;
            case 5:
                n = Fifteen;
                break;
            case 6:
                n = Sixteen;
                break;
            case 7:
                n = Seventeen;
                break;
            case 8:
                n = Eighteen;
                break;
            case 9:
                n = Nineteen;
                break;
            default:
                n = "Not a Number";
        }
        return n;
    }

    private String ConvertNumberBeforeDot(String input) {
        System.out.println("[ConvertNumberBeforeDot] input = " + input);
        int inputlength = input.length();
        int x = 0;
        String teen1 = "";
        String teen2 = "";
        String teen3 = "";
        String teen4 = "";
        String teen5 = "";
        String numName = "";
        String invalidNum = "";
        String a1 = ""; // for insertion of trillion, billion, million, thousand, hundred
        String a2 = "";
        String a3 = "";
        String a4 = "";
        String a5 = "";
        String a6 = "";
        String a7 = "";
        String a8 = "";
        String a9 = "";
        char digit[] = new char[inputlength]; // stores output
        int value = 0;
        for (int i = 0; i < inputlength; i++) {
            value = inputlength - i - 1;
            // puts digits into char array
            digit[value] = input.charAt(i);
        }
        String store[] = new String[15]; // store output
        for (int i = 0; i < inputlength; i++) {
            value = inputlength - i - 1;
            x = inputlength - i;
            switch (x) { // assign text to each digit
                case 15:
                    store[value] = d1(digit[value]);
                    break;
                case 14:
                    if (digit[value] == '1')
                        teen5 = "yes";
                    else
                        teen5 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 13:
                    if (teen5 == "yes") {
                        teen5 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
                case 12:
                    store[value] = d1(digit[value]);
                    break;
                case 11:
                    if (digit[value] == '1')
                        teen4 = "yes";
                    else
                        teen4 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 10:
                    if (teen4 == "yes") {
                        teen4 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
                case 9:
                    store[value] = d1(digit[value]);
                    break;
                case 8:
                    if (digit[value] == '1')
                        teen3 = "yes";
                    else
                        teen3 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 7:
                    if (teen3 == "yes") {
                        teen3 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
                case 6:
                    store[value] = d1(digit[value]);
                    break;
                case 5:
                    if (digit[value] == '1')
                        teen2 = "yes";
                    else
                        teen2 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 4:
                    if (teen2 == "yes") {
                        teen2 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
                case 3:
                    store[value] = d1(digit[value]);
                    ;
                    break;
                case 2:
                    if (digit[value] == '1')
                        teen1 = "yes";
                    else
                        teen1 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 1:
                    if (teen1 == "yes") {
                        teen1 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
            }
            if (store[value] == "Not a Number")
                invalidNum = "yes";
            switch (inputlength) {
                case 1:
                    store[2 - 1] = "";
                case 2:
                    store[3 - 1] = "";
                case 3:
                    store[4 - 1] = "";
                case 4:
                    store[5 - 1] = "";
                case 5:
                    store[6 - 1] = "";
                case 6:
                    store[7 - 1] = "";
                case 7:
                    store[8 - 1] = "";
                case 8:
                    store[9 - 1] = "";
                case 9:
                    store[10 - 1] = "";
                case 10:
                    store[11 - 1] = "";
                case 11:
                    store[12 - 1] = "";
                case 12:
                    store[13 - 1] = "";
                case 13:
                    store[14 - 1] = "";
                case 14:
                    store[15 - 1] = "";
            }
            if (store[15 - 1] != "")
                a9 = Hundred;
            else
                a9 = "";
            if ((store[15 - 1] != "") || (store[14 - 1] != "") || (store[13 - 1] != ""))
                a8 = Trillion;
            else
                a8 = "";
            if (store[12 - 1] != "")
                a7 = Hundred;
            else
                a7 = "";
            if ((store[12 - 1] != "") || (store[11 - 1] != "") || (store[10 - 1] != ""))
                a6 = Billion;
            else
                a6 = "";
            if (store[9 - 1] != "")
                a5 = Hundred;
            else
                a5 = "";
            if ((store[9 - 1] != "") || (store[8 - 1] != "") || (store[7 - 1] != ""))
                a4 = Million;
            else
                a4 = "";
            if (store[6 - 1] != "")
                a3 = Hundred;
            else
                a3 = "";
            if ((store[6 - 1] != "") || (store[5 - 1] != "") || (store[4 - 1] != ""))
                a2 = Thousand;
            else
                a2 = "";
            if (store[3 - 1] != "")
                a1 = Hundred;
            else
                a1 = "";
        }
        // add up text, cancel if invalid input found
        if (invalidNum == "yes") {
            numName = "Invalid Input";
        } else {
            String hundredTrillion = store[15 - 1] + a9;
            String hundredBillion = store[12 - 1] + a7;
            String hundredMillion = store[9 - 1] + a5;
            String hundredThousand = store[6 - 1] + a3;
            String hundred = store[3 - 1] + a1;

            numName = hundredTrillion + store[14 - 1] + store[13 - 1]
                    + a8 + hundredBillion + store[11 - 1] + store[10 - 1]
                    + a6 + hundredMillion + store[8 - 1] + store[7 - 1]
                    + a4 + hundredThousand + store[5 - 1] + store[4 - 1]
                    + a2 + hundred + store[2 - 1] + store[1 - 1] + Currency;
        }
        store[1 - 1] = "";
        store[2 - 1] = "";
        store[3 - 1] = "";
        store[4 - 1] = "";
        store[5 - 1] = "";
        store[6 - 1] = "";
        store[7 - 1] = "";
        store[8 - 1] = "";
        store[9 - 1] = "";
        store[10 - 1] = "";
        store[11 - 1] = "";
        store[12 - 1] = "";
        store[13 - 1] = "";
        store[14 - 1] = "";
        store[15 - 1] = "";
        if (numName == "")
            numName = "Zero";
        return numName;
    }

    private String ConvertNumberAfterDot(String input) {
        System.out.println("[ConvertNumberAfterDot] input = " + input);
        int inputlength = input.length();
        int x = 0;
        String teen1 = "";
        String teen2 = "";
        String teen3 = "";
        String teen4 = "";
        String teen5 = "";
        String numName = "";
        String invalidNum = "";
        String a1 = ""; // for insertion of trillion, billion, million, thousand, hundred
        String a2 = "";
        String a3 = "";
        String a4 = "";
        String a5 = "";
        String a6 = "";
        String a7 = "";
        String a8 = "";
        String a9 = "";
        char digit[] = new char[inputlength]; // stores output
        int value = 0;
        for (int i = 0; i < inputlength; i++) {
            value = inputlength - i - 1;
            // puts digits into char array
            digit[value] = input.charAt(i);
        }
        String store[] = new String[15]; // store output
        for (int i = 0; i < inputlength; i++) {
            value = inputlength - i - 1;
            x = inputlength - i;
            switch (x) { // assign text to each digit
                case 15:
                    store[value] = d1(digit[value]);
                    break;
                case 14:
                    if (digit[value] == '1')
                        teen5 = "yes";
                    else
                        teen5 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 13:
                    if (teen5 == "yes") {
                        teen5 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
                case 12:
                    store[value] = d1(digit[value]);
                    break;
                case 11:
                    if (digit[value] == '1')
                        teen4 = "yes";
                    else
                        teen4 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 10:
                    if (teen4 == "yes") {
                        teen4 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
                case 9:
                    store[value] = d1(digit[value]);
                    break;
                case 8:
                    if (digit[value] == '1')
                        teen3 = "yes";
                    else
                        teen3 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 7:
                    if (teen3 == "yes") {
                        teen3 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
                case 6:
                    store[value] = d1(digit[value]);
                    break;
                case 5:
                    if (digit[value] == '1')
                        teen2 = "yes";
                    else
                        teen2 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 4:
                    if (teen2 == "yes") {
                        teen2 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
                case 3:
                    store[value] = d1(digit[value]);
                    ;
                    break;
                case 2:
                    if (digit[value] == '1')
                        teen1 = "yes";
                    else
                        teen1 = "";
                    store[value] = d2(digit[value]);
                    break;
                case 1:
                    if (teen1 == "yes") {
                        teen1 = "";
                        store[value] = d3(digit[value]);
                    } else
                        store[value] = d1(digit[value]);
                    break;
            }
            if (store[value] == "Not a Number")
                invalidNum = "yes";
            switch (inputlength) {
                case 1:
                    store[2 - 1] = "";
                case 2:
                    store[3 - 1] = "";
                case 3:
                    store[4 - 1] = "";
                case 4:
                    store[5 - 1] = "";
                case 5:
                    store[6 - 1] = "";
                case 6:
                    store[7 - 1] = "";
                case 7:
                    store[8 - 1] = "";
                case 8:
                    store[9 - 1] = "";
                case 9:
                    store[10 - 1] = "";
                case 10:
                    store[11 - 1] = "";
                case 11:
                    store[12 - 1] = "";
                case 12:
                    store[13 - 1] = "";
                case 13:
                    store[14 - 1] = "";
                case 14:
                    store[15 - 1] = "";
            }
            if (store[15 - 1] != "")
                a9 = Hundred;
            else
                a9 = "";
            if ((store[15 - 1] != "") || (store[14 - 1] != "") || (store[13 - 1] != ""))
                a8 = Trillion;
            else
                a8 = "";
            if (store[12 - 1] != "")
                a7 = Hundred;
            else
                a7 = "";
            if ((store[12 - 1] != "") || (store[11 - 1] != "") || (store[10 - 1] != ""))
                a6 = Billion;
            else
                a6 = "";
            if (store[9 - 1] != "")
                a5 = Hundred;
            else
                a5 = "";
            if ((store[9 - 1] != "") || (store[8 - 1] != "") || (store[7 - 1] != ""))
                a4 = Million;
            else
                a4 = "";
            if (store[6 - 1] != "")
                a3 = Hundred;
            else
                a3 = "";
            if ((store[6 - 1] != "") || (store[5 - 1] != "") || (store[4 - 1] != ""))
                a2 = Thousand;
            else
                a2 = "";
            if (store[3 - 1] != "")
                a1 = Hundred;
            else
                a1 = "";
        }
        // add up text, cancel if invalid input found
        if (invalidNum == "yes")
            numName = "Invalid Input";
        else {
            numName = store[15 - 1] + a9 + store[14 - 1] + store[13 - 1]
                    + a8 + store[12 - 1] + a7 + store[11 - 1] + store[10 - 1]
                    + a6 + store[9 - 1] + a5 + store[8 - 1] + store[7 - 1]
                    + a4 + store[6 - 1] + a3 + store[5 - 1] + store[4 - 1]
                    + a2 + store[3 - 1] + a1 + store[2 - 1] + store[1 - 1] + Cent;
        }
        store[1 - 1] = "";
        store[2 - 1] = "";
        store[3 - 1] = "";
        store[4 - 1] = "";
        store[5 - 1] = "";
        store[6 - 1] = "";
        store[7 - 1] = "";
        store[8 - 1] = "";
        store[9 - 1] = "";
        store[10 - 1] = "";
        store[11 - 1] = "";
        store[12 - 1] = "";
        store[13 - 1] = "";
        store[14 - 1] = "";
        store[15 - 1] = "";
        if (numName == "") {
            numName = "Zero";
        }
        return numName;
    }

    private String[] ValidateNumber(String number) {
        System.out.println("[ValidateNumber] number = " + number);
        String validatedNumber[] = new String[2];
        char dot = '.';
        char afterDotFlag = 'N';
        StringBuffer beforeDot = new StringBuffer();
        StringBuffer afterDot = new StringBuffer();

//        logger.debug("\n number.length() : " + number.length());
        for (int i = 0; i < number.length(); i++) {
            if (afterDotFlag == 'N' && Character.isDigit(number.charAt(i)))
            {
                beforeDot.append(number.charAt(i));
//                logger.debug("beforeDot : " + beforeDot);
            }
            else if ((number.charAt(i) == dot))
                afterDotFlag = 'Y';
            else if (afterDotFlag == 'Y' && Character.isDigit(number.charAt(i)))
            {
                afterDot.append(number.charAt(i));
//                logger.debug("afterDot : " + afterDot);
            }
        }

//        logger.debug("\n beforeDot.length() : " + beforeDot.length());
//        logger.debug("\n afterDot.length() : " + afterDot.length());

        if (beforeDot.length() == 0)
            validatedNumber[0] = null;
        else {
            BigInteger isZero = new BigInteger(beforeDot.toString());
            for (int i = 0; i < beforeDot.length(); i++) {
                isZero = isZero.add(isZero);
//                logger.debug("\n isZero beforeDot : " + isZero);
            }

//            logger.debug("\n isZero.longValue() beforeDot : "+ isZero.longValue());
            if (isZero.longValue() == 0)
            {
                validatedNumber[0] = null;
//                logger.debug("\n validatedNumber[0] is null");
            }
            else
            {
                validatedNumber[0] = beforeDot.toString();
//                logger.debug("\n validatedNumber[0] : " + validatedNumber[0]);
            }
        }

        if (afterDot.length() == 0)
            validatedNumber[1] = null;
        else {
            BigInteger isZero = new BigInteger(afterDot.toString());
            for (int i = 0; i < afterDot.length(); i++) {
                isZero = isZero.add(isZero);
//                logger.debug("\n isZero afterDot : " + isZero);
            }

//            logger.debug("\n isZero.longValue() afterDot : "+ isZero.longValue());
            if (isZero.longValue() == 0)
            {
//                validatedNumber[1] = null;
                validatedNumber[1] = "0";
//                logger.debug("\n validatedNumber[1] is 0");
            }
            else
            {
                validatedNumber[1] = afterDot.toString();
//                logger.debug("\n validatedNumber[1] : " + validatedNumber[1]);
            }
        }
//        logger.debug("\n validatedNumber before return : " + validatedNumber.toString());
        return validatedNumber;
    }

    public String getTextNumber(String value) {
        String validatedNumberBeforeDot = null;
        String validatedNumberAfterDot = null;
        String textNumber = null;
        String textNumberBeforeDot = null;
        String textNumberAfterDot = null;

        validatedNumberBeforeDot = ValidateNumber(value.toString())[0];
        validatedNumberAfterDot = ValidateNumber(value.toString())[1];

        if (validatedNumberBeforeDot != null && validatedNumberBeforeDot.length() <= 15)
            textNumberBeforeDot = ConvertNumberBeforeDot(validatedNumberBeforeDot);

        if (validatedNumberAfterDot != null && validatedNumberAfterDot.length() <= 2)
            textNumberAfterDot = ConvertNumberAfterDot(validatedNumberAfterDot);

        if (textNumberBeforeDot != null && textNumberAfterDot != null) {
            textNumber = textNumberBeforeDot + And + textNumberAfterDot;
            textNumber = FormatTextNumber(textNumber);
        } else if (textNumberBeforeDot != null && textNumberAfterDot == null) {
            textNumber = textNumberBeforeDot;
            textNumber = FormatTextNumber(textNumber);
        } else if (textNumberBeforeDot == null && textNumberAfterDot != null) {
            textNumber = textNumberAfterDot;
            textNumber = FormatTextNumber(textNumber);
        } else {
            textNumber = No_Value;
            textNumber = FormatTextNumber(textNumber);
        }

        return textNumber;
    }

    public String getTextNumber(BigDecimal value) {
        String textNumber = null;
        textNumber = getTextNumber(value.toString());
        return textNumber;
    }

    private String convertThaiAmountToText(String input, String amountUnit)
    {
        String result="";
        int numLen = input.length();

        for (int i=0;i<numLen;i++)
        {
            int iUnit = (numLen-i)%6;
            String sUnit= THAI_UNIT[iUnit];

            int iDigit = Integer.parseInt(input.substring(i,i+1));

            String sDigit=THAI_DIGIT[iDigit];

            //Check Unit = Ten and Digit = 1 ?
            if ((sUnit.equals(THAI_UNIT[2])) && (sDigit.equals(THAI_DIGIT[1])))
            {
                sDigit="";
            }

            //Check One or Ed?
            if ((sUnit.equals("")) && (sDigit.equals(THAI_DIGIT[1])) && (numLen!=1))
            {
                sDigit = THAI_EXTRA_DIGIT[0];
            }

            if ((i==0) && (sDigit.equals(THAI_EXTRA_DIGIT[0])) && (numLen>1))
            {
                sDigit = THAI_DIGIT[1];
            }

            //Check Twenty?
            if ((sUnit.equals(THAI_UNIT[2])) && (sDigit.equals(THAI_DIGIT[2])))
            {
                sDigit = THAI_EXTRA_DIGIT[1];
            }

            //Check Million?
            if ((sUnit.equals("")) && (numLen-i>6))
            {
                sUnit = THAI_UNIT[6];
            }

            //Generate Text
            if (!sDigit.equals(THAI_DIGIT[0]))
            {
                result += sDigit + sUnit;
            }
            else
            {
                if (sUnit.equals(THAI_UNIT[6]))
                {
                    result += sUnit;
                }
            }
        }

        if (!result.equals(""))
        {
            result += amountUnit;
        }

        return result;
    }

    public String getThaiTextNumber(String value) {
        String validatedNumberBeforeDot = null;
        String validatedNumberAfterDot = null;
        String textNumber = null;
        String textNumberBeforeDot = "";
        String textNumberAfterDot = "";

//        logger.debug("\n In getThaiTextNumber() ");
        logger.debug("\n Value in getThaiTextNumber : " + value);
        validatedNumberBeforeDot = ValidateNumber(value.toString())[0];
        validatedNumberAfterDot = ValidateNumber(value.toString())[1];

//        logger.debug("\n validatedNumberBeforeDot.length() " + validatedNumberBeforeDot.length());
//        logger.debug("\n validatedNumberAfterDot.length() " + validatedNumberAfterDot.length());
        //Format NumberAfterDot
        if (validatedNumberAfterDot.length()==1)
        {
            validatedNumberAfterDot = validatedNumberAfterDot + "0";
        }
        else if (validatedNumberAfterDot.length()>2)
        {
            validatedNumberAfterDot = validatedNumberAfterDot.substring(0,2);
        }


        //Start Convert
        //logger.debug("validatedNumberBeforeDot : "+validatedNumberBeforeDot);
        //logger.debug("validatedNumberAfterDot : "+validatedNumberAfterDot);
        if (validatedNumberBeforeDot!=null)
        {
            textNumberBeforeDot = convertThaiAmountToText(validatedNumberBeforeDot,BAHT);
        }
        if (validatedNumberAfterDot!=null)
        {
            textNumberAfterDot = convertThaiAmountToText(validatedNumberAfterDot,SATANG);
           // logger.debug("textNumberAfterDot : "+textNumberAfterDot);
            if (textNumberAfterDot.equals(""))
            {
                textNumberAfterDot = Only;
            }
        }

        textNumber = textNumberBeforeDot + textNumberAfterDot;

        return textNumber;
    }

    public String getTextNumberWithCurrencyDescription(String value, Locale locale) {
        String textNumber = null;

        if (locale.getCountry().equalsIgnoreCase("th"))
        {
            textNumber = getThaiTextNumber(value);
        }
        else
        {
            textNumber = getTextNumber(value);
        }

        return textNumber;
    }

    public String getTextNumberWithoutCurrencyDescription(String value) {
        StringBuffer valueBuffer = new StringBuffer();
        String textNumber = null;
        textNumber = getTextNumber(value);
        if (textNumber.regionMatches(textNumber.length() - Currency.length(), Currency, 0, Currency.length())) {
            System.out.println("[getTextNumberWithoutCurrencyDescription] regionMatches = " + textNumber.regionMatches(textNumber.length() - Currency.length(), Currency, 0, Currency.length()));
            for (int i = 0; i < textNumber.length() - Currency.length() - 1; i++) {
                valueBuffer.append(textNumber.charAt(i));
            }
            textNumber = valueBuffer.toString();
        }

        return textNumber;
    }

    private String FormatTextNumber(String value) {
        StringBuffer valueBuffer = new StringBuffer();

        for (int i = 0; i < value.length(); i++) {
            if (Character.isLetter(value.charAt(i))) {
                if (Character.isUpperCase(value.charAt(i)) && i != 0)   // Avoid to add the space character before the first uppercase character
                    valueBuffer.append(' ').append(value.charAt(i));    // Append a single space before each uppercase character
                else
                    valueBuffer.append(value.charAt(i));
            }
        }
        value = valueBuffer.toString();

        return value;
    }
}
