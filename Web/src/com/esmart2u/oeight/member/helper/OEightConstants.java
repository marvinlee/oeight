

package com.esmart2u.oeight.member.helper;


public interface OEightConstants {
    
    public static final String SESSION_USER_ID = "userId";
    public static final String SESSION_USER_NAME = "userName";
    
    // used across the application flow
    public static final String SESSION_PRE_CREATED_APPLICATION_ID_KEY = "preCreatedApplicationId";
    public static final String SESSION_APPLICATION_ID_KEY = "applicationId";
    public static final String SESSION_APPLICATION_HEADER_KEY = "applicationHeaderVO";
    public static final String SESSION_APPLICATION_WORKFLOW_CODE_KEY = "applicationWorkflowCodeKey";
    public static final String SESSION_APPLICATION_SCORE_CARD_CODE_KEY = "applicationScoreCardCodeKey";
    public static final String APPLICATION_ACTION_FORM_KEY = "applicationActionFormKey";
    public static final String SESSION_APPLICATION_APPLIED_DATE = "applicationAppliedDate";
    
    public static final String SESSION_MAIN_APPLICANT_ID = "mainApplicantId";
    public static final String SESSION_NONCE_TOKEN = "nonceToken";
    
    public static final String DAY = "D";
    public static final String MONTH = "M";
    public static final String YEAR = "Y";
    
    
    public static final String TEXTAREA_COLS = "COLS";
    public static final String TEXTAREA_ROWS = "ROWS";

    public static final String COUNTRY_DEFAULT_KEY = "country.default";
    public static final String COUNTRY_MALAYSIA = "MY";
    public static final String COUNTRY_SINGAPORE = "SG";
    public static final String COUNTRY_UNITED_KINGDOM =  "UK";
    
    public static final String PROFILE_IMAGE_EXT = ".jpg";
    public static final String LARGE_IMAGE_EXT = "l.jpg";
    public static final String THUMBNAIL_IMAGE_EXT = "m.jpg";
    
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    
    public static final String GENDER_MALE_DISPLAY = "Male";
    public static final String GENDER_FEMALE_DISPLAY = "Female";
    
    public static final char SYS_STATUS_NEW = 'N';
    public static final char SYS_STATUS_EXISTING = 'E';
    public static final char SYS_STATUS_MODERATED = 'M';
    public static final char SYS_STATUS_PENDING_MODERATION = 'P';
    public static final char SYS_STATUS_REMOVED = 'D';
    
    public static final char PHOTO_TYPE_MOSAIC  = 'M';
    public static final char PHOTO_TYPE_PROFILE = 'P';
    public static final char PHOTO_TYPE_SHIRT   = 'S'; 
    
    public static final String TOP_VIEWS = "topViews";
    public static final String TOP_VOTES = "topVotes";
    public static final String TOP_INVITES = "topInvites";
    public static final String TOP_GAMER = "topGamer";
     
    public static final char PODIUM_TYPE_VIEWS = '1';
    public static final char PODIUM_TYPE_VOTES = '2';
    public static final char PODIUM_TYPE_INVITES = '3';
    public static final char PODIUM_TYPE_GAMER = '4';
    
    public static final char MAIL_STATUS_NEW ='N';
    public static final char MAIL_STATUS_SENT = 'S';
    public static final char MAIL_STATUS_SEND_FAILED = 'F';
    public static final char MAIL_STATUS_ACCEPTED = 'A';
    
    public static final char REPORT_ABUSE_CONFIRMED = 'C';
    
    public static final int PROFILE_PREFERENCE_SHOW = 1;
    public static final int PROFILE_PREFERENCE_HIDE = 2;
    public static final int PROFILE_PREFERENCE_CONTACT = 3;
    
    public static final String USER_TYPE_PIONEER = "P";
    public static final String USER_TYPE_MEMBER = "M";
    public static final String USER_TYPE_VIP = "V";
    public static final String USER_TYPE_SPONSOR = "S"; 
    
    public static final char MEMBERS_UPDATE_TYPE_LOGIN = 'L';
    public static final char MEMBERS_UPDATE_TYPE_REGISTER = 'R';
    
    public static final int NUMBER_OF_VOTES_ALLOWED = 10;
    public static final int APP_MAX_LIST_RESULTS = 8;
    
    // Z - New Request
    // N - New/Currently friend
    // D - Deleted
    // B - Blocked forever
    // A - Not friend yet
    public static final char BUDDY_NEW_REQUEST = 'Z';
    public static final char BUDDY_IS_FREN = 'N';
    public static final char BUDDY_DELETED = 'D';
    public static final char BUDDY_BLOCKED = 'B';
    public static final char BUDDY_NOT_FREN = 'A';
    
    /******************
     * Email Updates
     ******************/
    //Received wishes (ownself)
    public static final int NOTIFY_UPDATE_S01_RECEIVED_WISH = -1;
    //Received private message (ownself)
    public static final int NOTIFY_UPDATE_S02_RECEIVED_PM = -2;
    //Added you as buddy(New, required auto add buddy with contact grabber)
    public static final int NOTIFY_UPDATE_S03_RECEIVE_BUDDY_REQ = -3;
    //Added you as buddy(New, required auto add buddy with contact grabber)
    public static final int NOTIFY_UPDATE_S04_RECEIVE_BUDDY_REQ_CONFIRM = -4;
    //Updated wish (friends)
    public static final int NOTIFY_UPDATE_F01_UPDATED_WISH = 1;
    //Updated profile (friends) + including contact information
    public static final int NOTIFY_UPDATE_F02_UPDATED_PROFILE = 2;
    //Updated photo (friends)
    public static final int NOTIFY_UPDATE_F03_UPDATED_PHOTO = 3;
    //Updated apps (friends)
    public static final int NOTIFY_UPDATE_F04_UPDATED_APPS = 4;
    
    /******************
     * User Requests Tracking
     ******************/
    public static final int TRACK_ADD_BUDDY = 1;
    public static final int TRACK_WISHES = 2;// not implemented
    public static final int TRACK_PRIVATE_MESSAGE = 3;//not implemented
    
    /******************
     * Campaign Invite
     ******************/
    public static final char CAMPAIGN_INVITE_OWNER_RECORD = 'O';
    public static final int CAMPAIGN_INVITE_OEIGHT_PLEDGE = 1; 
    
    public static final int CAMPAIGN_INVITE_FROM_FRIENDSTER = 1;
    public static final int CAMPAIGN_INVITE_FROM_FACEBOOK = 2;
    
    // Action class is advised to append campaign code behind session keys below
    public static final String SESSION_CAMPAIGN_INVITE_FROM = "campaignFrom"; 
    public static final String SESSION_CAMPAIGN_INVITE_TO = "campaignTo"; 
    public static final String SESSION_CAMPAIGN_ACCEPTED = "userAccepted"; 
    
   
    /******************
     * OEight Wedding Constants
     ******************/ 
    public static final char WEDDING_TYPE_CANCEL = 'C';
    public static final char WEDDING_TYPE_NORMAL = 'N';
    public static final char WEDDING_TYPE_SPECIAL = 'S';
    public static final char WEDDING_TYPE_XCLUSIVE = 'X';
    
    
    /******************
     * PhpBB Forum Constants
     ******************/ 
    //public static final String PHPBB_COOKIE_SESSION_ID_KEY = "phpbb2mysql_sid";
    public static final String PHPBB_COOKIE_SESSION_ID_KEY = "phpbb3_bholl_sid";
    public static final String PHPBB_COOKIE_USER_ID_KEY = "phpbb3_bholl_u"; 
    
    
}
