/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ActivityVO.java

package com.esmart2u.oeight.member.vo;

import java.lang.String;
import java.sql.Date;
import java.util.Map;
import java.io.Serializable;
import java.lang.Cloneable;

import com.esmart2u.solution.base.helper.ValueObject;

/**
 * Value object to represent table tbl_cs_actvt.
 *
 * @author  Lee Meau Chyuan
 * @version $Revision: 1.8 $
 */

public class ActivityVO extends ValueObject
    implements Serializable, Cloneable
{
    private static final long serialVersionUID = -8811881188118811881L;

    public static final String SP_SELECT = "DcmsCsActivitySelect";
    public static final String SP_INSERT = "DcmsCsActivityInsert";
    public static final String SP_UPDATE = "DcmsCsActivityUpdate";
    public static final String SP_DELETE = "DcmsCsActivityDelete";

    public static final Map COLUMN_MAP = toColumnMap(new String[][]
    {
        // properties direct mapping with value object table
        {"id", "id"},
        {"cs_app_id", "applicationId"},
        {"ref_id", "referenceId"},
        {"wf_sts_cd", "workflowStatusCode"},
        {"is_mandatory", "mandatoryFlag"},
        {"mt_actvt_cd", "activityCode"},
        {"other_actvt_dscp", "otherActivity"},
        {"det", "detail"},
        {"mt_actvt_sts_cd", "activityStatusCode"},
        {"dt_req", "expectedRequireDate"},
        {"is_verified", "verifiedFlag"},
        {"is_sys", "systemDataFlag"},
        {"created_by", "createdBy"},
        {"updated_by", "updatedBy"},
        {"completed_by", "completedBy"},
        {"dt_created", "createdDate"},
        {"dt_updated", "updatedDate"},
        {"dt_completed", "completedDate"},
        {"version", "currentRecordVersion"},

        // extra properties
        {"mt_actvt_dscp", "activityDescription"}
    });


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS PROPERTIES DIRECT MAPPING WITH VALUE OBJECT TABLE.
    //******************************************************************************************************************
    private String id;
    private String applicationId;
    private String referenceId;
    private String workflowStatusCode;
    private String mandatoryFlag;
    private String activityCode;
    private String otherActivity;
    private String detail;
    private String activityStatusCode;
    private Date expectedRequireDate;
    private String verifiedFlag;
    private String systemDataFlag;
    private String createdBy;
    private String updatedBy;
    private String completedBy;
    private Date createdDate;
    private Date updatedDate;
    private Date completedDate;
    private int currentRecordVersion;


    //******************************************************************************************************************
    // EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************

    private String activityDescription;

    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS METHODS TO ACCESS THE VALUE OBJECT TABLE.
    //******************************************************************************************************************
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getApplicationId()
    {
        return applicationId;
    }

    public void setApplicationId(String applicationId)
    {
        this.applicationId = applicationId;
    }

    public String getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(String referenceId)
    {
        this.referenceId = referenceId;
    }

    public String getWorkflowStatusCode()
    {
        return workflowStatusCode;
    }

    public void setWorkflowStatusCode(String workflowStatusCode)
    {
        this.workflowStatusCode = workflowStatusCode;
    }

    public String getMandatoryFlag()
    {
        return mandatoryFlag;
    }

    public void setMandatoryFlag(String mandatoryFlag)
    {
        this.mandatoryFlag = mandatoryFlag;
    }

    public String getActivityCode()
    {
        return activityCode;
    }

    public void setActivityCode(String activityCode)
    {
        this.activityCode = activityCode;
    }

    public String getOtherActivity()
    {
        return otherActivity;
    }

    public void setOtherActivity(String otherActivity)
    {
        this.otherActivity = otherActivity;
    }

    public String getDetail()
    {
        return detail;
    }

    public void setDetail(String detail)
    {
        this.detail = detail;
    }

    public String getActivityStatusCode()
    {
        return activityStatusCode;
    }

    public void setActivityStatusCode(String activityStatusCode)
    {
        this.activityStatusCode = activityStatusCode;
    }

    public Date getExpectedRequireDate()
    {
        return expectedRequireDate;
    }

    public void setExpectedRequireDate(Date expectedRequireDate)
    {
        this.expectedRequireDate = expectedRequireDate;
    }

    public String getVerifiedFlag()
    {
        return verifiedFlag;
    }

    public void setVerifiedFlag(String verifiedFlag)
    {
        this.verifiedFlag = verifiedFlag;
    }

    public String getSystemDataFlag()
    {
        return systemDataFlag;
    }

    public void setSystemDataFlag(String systemDataFlag)
    {
        this.systemDataFlag = systemDataFlag;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getCompletedBy()
    {
        return completedBy;
    }

    public void setCompletedBy(String completedBy)
    {
        this.completedBy = completedBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public Date getCompletedDate()
    {
        return completedDate;
    }

    public void setCompletedDate(Date completedDate)
    {
        this.completedDate = completedDate;
    }

    public int getCurrentRecordVersion()
    {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion)
    {
        this.currentRecordVersion = currentRecordVersion;
    }


    //******************************************************************************************************************
    // METHODS FOR EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    public String getActivityDescription()
    {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription)
    {
        this.activityDescription = activityDescription;
    }

    //******************************************************************************************************************
    // MISCELLANEOUS.
    //******************************************************************************************************************
    public Object clone() throws CloneNotSupportedException
    {
        ActivityVO activityVO = (ActivityVO)super.clone();
        if (getExpectedRequireDate() != null) activityVO.setExpectedRequireDate((Date)getExpectedRequireDate().clone());
        if (getCreatedDate() != null) activityVO.setCreatedDate((Date)getCreatedDate().clone());
        if (getUpdatedDate() != null) activityVO.setUpdatedDate((Date)getUpdatedDate().clone());
        if (getCompletedDate() != null) activityVO.setCompletedDate((Date)getCompletedDate().clone());
        return activityVO;
    }
}

// end of ActivityVO.java