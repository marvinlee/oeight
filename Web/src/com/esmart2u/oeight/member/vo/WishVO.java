/*
 * WishVO.java
 *
 * Created on December 2, 2007, 2:25 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.vo;

import com.esmart2u.oeight.data.UserWishers;

/**
 *
 * @author meauchyuan.lee
 */
public class WishVO extends UserWishers {
    
    private String wishIdString;
    private String userName;
    private char gender;
    private String genderString;
    private String photoLargePath;
    
    // For friendsList
    private long friendId;
    private String photoSmallPath; 
    
    /** Creates a new instance of WishVO */
    public WishVO() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getPhotoLargePath() {
        return photoLargePath;
    }

    public void setPhotoLargePath(String photoLargePath) {
        this.photoLargePath = photoLargePath;
    }

    public String getGenderString() {
        genderString = Character.toString(gender);
        return genderString;
    }

    public void setGenderString(String genderString) {
        this.genderString = genderString;
    } 

    public String getWishIdString() {
        wishIdString = Long.toString(super.getWishId());
        return wishIdString;
    }

    public void setWishIdString(String wishIdString) {
        this.wishIdString = wishIdString;
    }

    public long getFriendId() {
        return friendId;
    }

    public void setFriendId(long friendId) {
        this.friendId = friendId;
    }

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }
}
