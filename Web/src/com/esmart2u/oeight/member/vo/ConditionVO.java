/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ConditionVO.java

package com.esmart2u.oeight.member.vo;

import java.lang.String;
import java.sql.Date;
import java.util.Map;
import java.io.Serializable;
import java.lang.Cloneable;

import com.esmart2u.solution.base.helper.ValueObject;
import com.esmart2u.solution.base.helper.Converter;

/**
 * Value object to represent table tbl_cs_cond.
 *
 * @author  Lee Meau Chyuan
 * @version $Revision: 1.8.16.1 $
 */

public class ConditionVO extends ValueObject
    implements Serializable, Cloneable
{
    private static final long serialVersionUID = -8811881188118811881L;

    public static final String SP_SELECT = "DcmsCsConditionSelect";
    public static final String SP_INSERT = "DcmsCsConditionInsert";
    public static final String SP_UPDATE = "DcmsCsConditionUpdate";
    public static final String SP_DELETE = "DcmsCsConditionDelete";

    public static final Map COLUMN_MAP = toColumnMap(new String[][]
    {
        // properties direct mapping with value object table
        {"id", "id"},
        {"cs_app_id", "applicationId"},
        {"ref_id", "referenceId"},
        {"mt_cond_cat_cd", "conditionCategoryCode"},
        {"mt_cond_cd", "conditionCode"},
        {"condition", "condition"},
        {"seq_no", "sequenceNumber"},
        {"due_wf_sts_cd", "dueInWorkflowStatusCode"},
        {"due_days", "dueInDays"},
        {"dt_due", "dueDate"},
        {"freq_mt_prd_cd", "frequencyCheckingPeriodCode"},
        {"is_new_cond", "newConditionFlag"},
        {"is_adhoc", "adhocCheckedFlag"},
        {"is_waive_allow", "allowWaiveFlag"},
        {"is_sys", "systemDataFlag"},
        {"is_waived", "waivedFlag"},
        {"waived_mt_rsn_cd", "waivedReasonCode"},
        {"waived_remark", "waivedRemark"},
        {"created_by", "createdBy"},
        {"updated_by", "updatedBy"},
        {"verified_by", "verifiedBy"},
        {"waived_by", "waivedBy"},
        {"dt_created", "createdDate"},
        {"dt_updated", "updatedDate"},
        {"dt_verified", "verifiedDate"},
        {"dt_waived", "waivedDate"},
        {"version", "currentRecordVersion"},

        // extra properties
        {"mt_fac_cd", "facilityCode"},
        {"mt_fac_dscp", "facilityDescription"},
        {"mt_cond_cat_dscp", "conditionCategoryDescription"},
        {"waived_mt_rsn_dscp", "waivedReasonDescription"},
        {"is_new_fac", "newFacilityFlag"}
    });


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS PROPERTIES DIRECT MAPPING WITH VALUE OBJECT TABLE.
    //******************************************************************************************************************
    private String id;
    private String applicationId;
    private String referenceId;
    private String conditionCategoryCode;
    private String conditionCode;
    private String condition;
    private int sequenceNumber;
    private String dueInWorkflowStatusCode;
    private int dueInDays;
    private Date dueDate;
    private String frequencyCheckingPeriodCode;
    private String newConditionFlag;
    private String adhocCheckedFlag;
    private String allowWaiveFlag;
    private String systemDataFlag;
    private String waivedFlag;
    private String waivedReasonCode;
    private String waivedRemark;
    private String createdBy;
    private String updatedBy;
    private String verifiedBy;
    private String waivedBy;
    private Date createdDate;
    private Date updatedDate;
    private Date verifiedDate;
    private Date waivedDate;
    private int currentRecordVersion;


    //******************************************************************************************************************
    // EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    private String conditionCategoryDescription;
    private String facilityCode;
    private String facilityDescription;
    private String waivedReasonDescription;
    private String newFacilityFlag;


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS METHODS TO ACCESS THE VALUE OBJECT TABLE.
    //******************************************************************************************************************
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getApplicationId()
    {
        return applicationId;
    }

    public void setApplicationId(String applicationId)
    {
        this.applicationId = applicationId;
    }

    public String getReferenceId()
    {
        return referenceId;
    }

    public void setReferenceId(String referenceId)
    {
        this.referenceId = referenceId;
    }

    public String getConditionCategoryCode()
    {
        return conditionCategoryCode;
    }

    public void setConditionCategoryCode(String conditionCategoryCode)
    {
        this.conditionCategoryCode = conditionCategoryCode;
    }

    public String getConditionCode()
    {
        return conditionCode;
    }

    public void setConditionCode(String conditionCode)
    {
        this.conditionCode = conditionCode;
    }

    public String getCondition()
    {
        return condition;
    }

    public void setCondition(String condition)
    {
        this.condition = condition;
    }

    public int getSequenceNumber()
    {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public String getDueInWorkflowStatusCode()
    {
        return dueInWorkflowStatusCode;
    }

    public void setDueInWorkflowStatusCode(String dueInWorkflowStatusCode)
    {
        this.dueInWorkflowStatusCode = dueInWorkflowStatusCode;
    }

    public int getDueInDays()
    {
        return dueInDays;
    }

    public void setDueInDays(int dueInDays)
    {
        this.dueInDays = dueInDays;
    }

    public Date getDueDate()
    {
        return dueDate;
    }

    public void setDueDate(Date dueDate)
    {
        this.dueDate = dueDate;
    }

    public String getFrequencyCheckingPeriodCode()
    {
        return frequencyCheckingPeriodCode;
    }

    public void setFrequencyCheckingPeriodCode(String frequencyCheckingPeriodCode)
    {
        this.frequencyCheckingPeriodCode = frequencyCheckingPeriodCode;
    }

    public String getNewConditionFlag() {
        return newConditionFlag;
    }

    public void setNewConditionFlag(String newConditionFlag) {
        this.newConditionFlag = newConditionFlag;
    }

    public String getAdhocCheckedFlag()
    {
        return adhocCheckedFlag;
    }

    public void setAdhocCheckedFlag(String adhocCheckedFlag)
    {
        this.adhocCheckedFlag = Converter.toBooleanString(adhocCheckedFlag);
    }

    public String getAllowWaiveFlag()
    {
        return Converter.toBooleanString(allowWaiveFlag);
    }

    public void setAllowWaiveFlag(String allowWaiveFlag)
    {
        this.allowWaiveFlag = Converter.toBooleanString(allowWaiveFlag);
    }

    public String getSystemDataFlag()
    {
        return Converter.toBooleanString(systemDataFlag);
    }

    public void setSystemDataFlag(String systemDataFlag)
    {
        this.systemDataFlag = Converter.toBooleanString(systemDataFlag);
    }

    public String getWaivedFlag()
    {
        return Converter.toBooleanString(waivedFlag);
    }

    public void setWaivedFlag(String waivedFlag)
    {
        this.waivedFlag = Converter.toBooleanString(waivedFlag);
    }

    public String getWaivedReasonCode()
    {
        return waivedReasonCode;
    }

    public void setWaivedReasonCode(String waivedReasonCode)
    {
        this.waivedReasonCode = waivedReasonCode;
    }

    public String getWaivedRemark()
    {
        return waivedRemark;
    }

    public void setWaivedRemark(String waivedRemark)
    {
        this.waivedRemark = waivedRemark;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }

    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getVerifiedBy()
    {
        return verifiedBy;
    }

    public void setVerifiedBy(String verifiedBy)
    {
        this.verifiedBy = verifiedBy;
    }

    public String getWaivedBy()
    {
        return waivedBy;
    }

    public void setWaivedBy(String waivedBy)
    {
        this.waivedBy = waivedBy;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public Date getVerifiedDate()
    {
        return verifiedDate;
    }

    public void setVerifiedDate(Date verifiedDate)
    {
        this.verifiedDate = verifiedDate;
    }

    public Date getWaivedDate()
    {
        return waivedDate;
    }

    public void setWaivedDate(Date waivedDate)
    {
        this.waivedDate = waivedDate;
    }

    public int getCurrentRecordVersion()
    {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion)
    {
        this.currentRecordVersion = currentRecordVersion;
    }


    //******************************************************************************************************************
    // METHODS FOR EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    public String getConditionCategoryDescription()
    {
        return conditionCategoryDescription;
    }

    public void setConditionCategoryDescription(String conditionCategoryDescription)
    {
        this.conditionCategoryDescription = conditionCategoryDescription;
    }

    public String getFacilityCode()
    {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode)
    {
        this.facilityCode = facilityCode;
    }

    public String getFacilityDescription()
    {
        return facilityDescription;
    }

    public void setFacilityDescription(String facilityDescription)
    {
        this.facilityDescription = facilityDescription;
    }

    public String getWaivedReasonDescription()
    {
        return waivedReasonDescription;
    }

    public void setWaivedReasonDescription(String waivedReasonDescription)
    {
        this.waivedReasonDescription = waivedReasonDescription;
    }

    public String getNewFacilityFlag() {
        return newFacilityFlag;
    }

    public void setNewFacilityFlag(String newFacilityFlag) {
        this.newFacilityFlag = newFacilityFlag;
    }

    //******************************************************************************************************************
    // MISCELLANEOUS.
    //******************************************************************************************************************
    public Object clone() throws CloneNotSupportedException
    {
        ConditionVO conditionVO = (ConditionVO)super.clone();
        if (getDueDate() != null) conditionVO.setDueDate((Date)getDueDate().clone());
        if (getCreatedDate() != null) conditionVO.setCreatedDate((Date)getCreatedDate().clone());
        if (getUpdatedDate() != null) conditionVO.setUpdatedDate((Date)getUpdatedDate().clone());
        if (getWaivedDate() != null) conditionVO.setWaivedDate((Date)getWaivedDate().clone());
        if (getVerifiedDate() != null) conditionVO.setVerifiedDate((Date)getVerifiedDate().clone());
        return conditionVO;
    }
}

// end of ConditionVO.java