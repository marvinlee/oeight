/*
 * MessageVO.java
 *
 * Created on March 19, 2008, 3:40 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.oeight.member.vo;

import com.esmart2u.oeight.data.UserMessage;

/**
 *
 * @author meauchyuan.lee
 */
public class MessageVO extends UserMessage {
    
    private String messageIdString;
    private String senderUserName;   
    private String recipientUserName;   
    private String photoSmallPath; 
    
    private MessageVO referredMessageVO;
    
    /** Creates a new instance of MessageVO */
    public MessageVO() {
    }

    public String getMessageIdString() {
        return messageIdString;
    }

    public void setMessageIdString(String messageIdString) {
        this.messageIdString = messageIdString;
    }

    public String getSenderUserName() {
        return senderUserName;
    }

    public void setSenderUserName(String senderUserName) {
        this.senderUserName = senderUserName;
    }

    public String getRecipientUserName() {
        return recipientUserName;
    }

    public void setRecipientUserName(String recipientUserName) {
        this.recipientUserName = recipientUserName;
    }

    public String getPhotoSmallPath() {
        return photoSmallPath;
    }

    public void setPhotoSmallPath(String photoSmallPath) {
        this.photoSmallPath = photoSmallPath;
    }

    public MessageVO getReferredMessageVO() {
        return referredMessageVO;
    }

    public void setReferredMessageVO(MessageVO referredMessageVO) {
        this.referredMessageVO = referredMessageVO;
    }
    
}
