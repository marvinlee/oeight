

package com.esmart2u.oeight.member.service;

import java.util.*;
import java.io.FileInputStream;

import com.esmart2u.solution.base.logging.Logger;

/**
 * @author  Siripong Visasmongkolchai, esmart2u Berhad
 * @version $Revision: 1.8 $
 */

public class ApplicationResourceManager
{
    private static final Logger logger = Logger.getLogger(ApplicationResourceManager.class);
    private static final String APPLICATION_RESOURCE_PROPERTIES_en_US = "MessageResources_en_US.properties";
    private static Map propertiesMap = new HashMap();

    static
    {
        loadConfigurations();
    }

    public static void loadConfigurations()
        throws RuntimeException
    {
        try
        {
            logger.info("Attempting to load " + APPLICATION_RESOURCE_PROPERTIES_en_US);
            logger.debug(APPLICATION_RESOURCE_PROPERTIES_en_US + " located in classpath. Loading properties");
            Properties properties = new Properties();
            properties.load(new FileInputStream(APPLICATION_RESOURCE_PROPERTIES_en_US));
            propertiesMap.put("en_US", properties);
            logger.info(APPLICATION_RESOURCE_PROPERTIES_en_US + " successfully loaded");
        }
        catch (Exception e)
        {
            logger.fatal("Unable to load " + APPLICATION_RESOURCE_PROPERTIES_en_US);
            throw new RuntimeException(e.getMessage());
        }
    }

    public static String getApplicationResourceProperty(String key, Locale locale)
    {
        Properties properties = null;

        Object object = propertiesMap.get(locale.toString());

        if (object == null)
        {
            properties = (Properties)propertiesMap.get("en_US");
        }
        else
        {
            properties = (Properties) object;
        }

        return properties.getProperty(key);
    }
}

// end of ApplicationResourceManager.java