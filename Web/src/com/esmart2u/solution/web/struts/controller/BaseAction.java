// BaseAction.java

package com.esmart2u.solution.web.struts.controller;

import com.esmart2u.oeight.member.helper.OEightConstants;
import java.io.ObjectOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.*;

import com.esmart2u.solution.base.helper.*;
import com.esmart2u.solution.base.logging.Logger; 

/**
 * Base Action.
 * 
 */

public abstract class BaseAction extends Action {
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(BaseAction.class);
    
    // default pages
    public static final String LOGIN_PAGE = "login";
    public static final String RELOGIN_PAGE = "relogin";
    public static final String ERROR_PAGE = "error";
    public static final String MESSAGE_PAGE = "message";
    public static final String INVALID_TOKEN_PAGE = "invalid_token";
    public static final String INVALID_SCREEN_ACCESS = "invalid_screen_access";
    
    // TOKEN is here due to we are purposely not to use Synchronized object
    // idea from Struts' TokenProcessor
    private static final Object TOKEN = new Object();
    private static final String TOKEN_KEY = "jspToken";
    
    /**
     * Overwrite struts's execute method.
     *
     * @param actionMapping Action mapping.
     * @param actionForm    Action form.
     * @param request       HTTP Servlet request.
     * @param response      HTTP Servlet response.
     * @return Action forward.
     * @throws Exception If exception is caught.
     */
    public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        try {
              
            // check action form type
            if (!BaseActionForm.class.isInstance(actionForm)) {
                throw new ClassCastException("ActionForm is not derived from BaseActionForm!");
            }
            
            //Added reason for refreshing a page problem
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "no-cache");
            response.addIntHeader("Expires", -1);
            response.setContentType("text/html;charset=UTF-8");
            
            // setting context values
            BaseActionForm baseActionForm = (BaseActionForm)actionForm;
            /*if (baseActionForm.getActionContext() == null)
            {
                System.out.println("Base actionContext is null, try initialize here");
                logger.debug("Base actionContext is null, try initialize here");
                baseActionForm.setActionContext(new BaseActionContext());
            }
            else
            {
                System.out.println("Base actionContext is not null, do nothing");
                logger.debug("Base actionContext is not null, do nothing");
            
            }*/
            baseActionForm.getActionContext().setMessageResources(getResources(request));
            baseActionForm.getActionContext().setActionMapping(actionMapping);
            baseActionForm.getActionContext().setActionMessages((ActionMessages)request.getAttribute(ActionMessages.GLOBAL_MESSAGE));
            baseActionForm.getActionContext().setActionErrors((ActionErrors)request.getAttribute(ActionErrors.GLOBAL_ERROR));
            baseActionForm.getActionContext().setRequest(request);
            baseActionForm.getActionContext().setResponse(response);
            baseActionForm.getActionContext().setLocale(getLocale(request));
            
            
            
            //Added to validate the session 
            String action = request.getParameter("act");
            if (action == null) action = "";
            if (!action.equalsIgnoreCase(INVALID_SCREEN_ACCESS)){
                if(! validateSession(request)){
                    return actionMapping.findForward(INVALID_SCREEN_ACCESS);
                }
            }
            //Ignore session validation as already validated
            
            // access validation, prepare session variables
            //validateAccess(actionMapping, baseActionForm, request, response);
            /*ActionForward nextPage = validateAccess(actionMapping, baseActionForm, request, response);
            if (nextPage != null)
            {
                return nextPage;
            }*/
            
            
            ActionErrors errors = null;
            if (isValidationRequired(action.trim())) {
                
                HashMap paramsMap = new HashMap();
                baseActionForm = (BaseActionForm)validateParameter(action, baseActionForm);
                errors = baseActionForm.getActionContext().getActionErrors();
                //HashMap map = constructParameter(params, params, paramsMap, request, cForm);
                //errors = validateInput(actionForm, event, map, locale);
            }
            
            
            
            // To display errors
            if (errors != null && errors.size() > 0) {
                //DefaultLogger.debug(CLASSNAME, "inside common action Got error"+ errors.toString());
                saveErrors(request, errors);
                action = getDefaultEvent(action);
            }
            
            
            // process request
            ActionForward nextPage = processRequest(actionMapping, baseActionForm, request, response, action);
            
            // Debug show cookie now
            
            Cookie[] cookies = request.getCookies();
            //System.out.println(">>>> Try to print cookies");
            if (cookies != null)
            {
               for(int i=0;i<cookies.length; i++)
               {
                    Cookie cookie = (Cookie)cookies[i];
                    //System.out.println(">>>>>>> BaseAction Cookie : " + cookie.getDomain()+ ":" + cookie.getName() + ":" + cookie.getValue() + ":" + cookie.getMaxAge());
                    cookie.setDomain("." + ConfigurationHelper.getDomainName());
                    cookie.setPath("/");
                    if (cookie.getName().equals(OEightConstants.PHPBB_COOKIE_SESSION_ID_KEY))
                    {
                        //System.out.println("Found cookie to set session Id!");
                        if (request.getSession(false) != null){ 
                            //System.out.println("Set session Id :" + request.getSession().getId().toLowerCase());
                            cookie.setValue(request.getSession().getId().toLowerCase());// Set session id
                        }
                    } 
                    response.addCookie(cookie);
               }
            
            }
            
            
            if (nextPage != null) {
                return nextPage;
            }
            
            throw new BaseActionException("Return-mapping is missing");
        } catch (Exception e) {
            if (BaseActionForm.class.isInstance(actionForm)) {
                BaseActionContext baseActionContext = ((BaseActionForm)actionForm).getActionContext();
                if (e instanceof ApplicationException) {
                    baseActionContext.getActionErrors().clear();
                    baseActionContext.addErrorCode(((ApplicationException)e).getReferenceNo());
                    saveErrors(request, baseActionContext.getActionErrors());
                    return actionMapping.findForward(ERROR_PAGE);
                } else {
                    baseActionContext.getActionErrors().clear();
                    baseActionContext.addErrorCode(new ApplicationException(e).getReferenceNo());
                    saveErrors(request, baseActionContext.getActionErrors());
                    return actionMapping.findForward(ERROR_PAGE);
                }
            } else {
                ActionErrors actionErrors = (ActionErrors)request.getAttribute(ActionErrors.GLOBAL_ERROR);
                actionErrors.clear();
                ApplicationException applicationException = new ApplicationException(e);
                actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("common.error.referenceNo", applicationException.getReferenceNo()));
                saveErrors(request, actionErrors);
                return actionMapping.findForward(ERROR_PAGE);
            }
        }
    }
    /**
     *Validate that the session is correct. Return true if it is ok. false if it is otherwise
     *@param request  The servlet request we are processing
     */
    
    public static boolean validateSession(HttpServletRequest request) {
        
        // Session is available, already login and user id available
        if ((request.getSession(false) != null) &&
                (!"".equals(request.getSession(false).getId())))
        {
            
            return true;
        }
        
        // Need to login
        return false;
    }
    
    /**
     * Validate access via SecurityManager.
     *
     * @param actionMapping  Action mapping.
     * @param baseActionForm Base action form.
     * @param request        HTTP Servlet request.
     * @param response       HTTP Servlet response.
     * @return Action forward.
     * @throws Exception If exception is caught.
     */
    protected ActionForward validateAccess(ActionMapping actionMapping, BaseActionForm baseActionForm,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        BaseActionContext baseActionContext = baseActionForm.getActionContext();
        
        // getting session
        HttpSession session = request.getSession(false);
                     
        if (session == null) {  
            //logger.debug("validateAccess session is null, set null appContext");
            baseActionContext.setAppContext(null);
        } else {
            // getting appContext 
            //logger.debug("validateAccess session available");
            baseActionContext.setAppContext((AppContext)session.getAttribute(AppConstants.APP_CONTEXT));
        }
        
        // check if require relogin or not when no appContext not found
        if (baseActionContext.getAppContext() == null) {
            // redirect immediately to relogin page
            logger.debug("Invalid session");
            baseActionContext.addMessageResource("common.message.sec_login_100001007");
            saveMessages(request, baseActionContext.getActionMessages());
            return actionMapping.findForward(RELOGIN_PAGE);
        } else {
            // set in module code
            baseActionContext.getAppContext().put(AppConstants.MODULE, getModuleCode());
        }
        
        // check login with security manager
        //todo
       /* if (SecurityManager.isSessionTimeout(baseActionContext.getAppContext()))
        {
            baseActionContext.addMessageResource("common.message.sec_login_100001008");
            saveMessages(request, baseActionContext.getActionMessages());
            return actionMapping.findForward(RELOGIN_PAGE);
        }*/
        
        return null;
    }
    
    /**
     * Process the action request.
     *
     * @param actionMapping  Action mapping.
     * @param baseActionForm Base action form.
     * @param request        HTTP Servlet request.
     * @param response       HTTP Servlet response.
     * @return Action forward.
     * @throws Exception If exception is caught.
     */
    protected ActionForward processRequest(ActionMapping actionMapping, BaseActionForm baseActionForm,
            HttpServletRequest request, HttpServletResponse response, String action)
            throws Exception {
        //System.out.println("Checking action Parameter");
        if (action == null && "".equals(action)){
            action = request.getParameter("act");
        }
        String doAction = parseActionName(action);
        //System.out.println("Action=" + doAction);
        if (StringUtils.hasValue(doAction)) {
            //System.out.println("Processing action [" + doAction + "]...");
            logger.info("Processing action [" + doAction + "]...");
            
            // prepare base action form
            prepareActionForm(baseActionForm);
            
            // get the 'do' method
            Method doMethod = null;
            try {
                doMethod = getClass().getMethod(doAction, new Class[]{baseActionForm.getClass()});
            } catch (NoSuchMethodException e) {
                throw new BaseActionException("Unrecognized action [" + doAction + "]");
            } catch (SecurityException e) {
                throw new BaseActionException("Security error in accessing action [" + doAction + "]");
            }
            
            // calling the 'do' method
            logger.debug(getClass().getName() + " accessed, calling action [" + doAction + "] now...");
            Object returnMapping = doMethod.invoke(this, new Object[]{baseActionForm});
            
            BaseActionContext baseActionContext = baseActionForm.getActionContext();
            
            // get screen action list
            //todo
            /*if (StringUtils.hasValue(baseActionForm.getScreenCode()))
            {
                baseActionForm.setActionList(SecurityManager.getScreenActionList(baseActionContext.getAppContext(), baseActionForm.getScreenCode()));
            }*/
            
            // set system date time
            baseActionForm.setSystemDateTime(DateUtils.getCurrentDateTime());
            
            // save messages
            logger.debug("Number of messages after action invocation = " + baseActionContext.getActionMessages().size());
            saveMessages(request, baseActionContext.getActionMessages());
            
            //System.out.println("ReturnMapping? " + returnMapping);
            // return to page
            if (returnMapping instanceof String) {
                return (actionMapping.findForward((String)returnMapping));
            } else {
                throw new BaseActionException("Unrecognized return-mapping of [" + returnMapping + "]");
            }
        } else {
            throw new BaseActionException("Action not defined");
        }
    }
    
    /**
     * Parse action method name.
     * <p/>
     * For eg. '?do=Login' -> 'doLogin'
     *
     * @param actionName Action name.
     * @return Action method name.
     */
    protected String parseActionName(String actionName) {
        if (!StringUtils.hasValue(actionName)) {
            actionName = getDefaultActionName();
            //return "";
        }
        //} else {
            //System.out.println("ActionName for substring = [" + actionName + "]");
            return "do" + actionName.substring(0,1).toUpperCase() + actionName.substring(1,actionName.length());
        //}
    }
    
    /**
     * Gets module code. To be overloaded to provide actual module code. Default is 'UNDEFINE'.
     *
     * @return Module code.
     */
    protected String getModuleCode() {
        return "undefined";
    }
    
    /**
     * Special handling in preparing the ActionForm before passed to respective Action to process.
     * Do nothing by default. To be overloaded.
     *
     * @param baseActionForm Base action form.
     * @throws Exception
     */
    protected void prepareActionForm(BaseActionForm baseActionForm)
    throws Exception {
        // do nothing now
    }
    
    /**
     * To map the ActionForm instance to the newly created ValueObject instance.
     *
     * @param actionForm       ActionForm instance.
     * @param valueObjectClass ValueObject class.
     * @return Mapped ValueObject instance.
     * @throws ValueObjectMappingException If error occurs during mapping.
     */
    protected ValueObject toValueObject(ActionForm actionForm, Class valueObjectClass)
    throws ValueObjectMappingException {
        if (actionForm == null || valueObjectClass == null) {
            return null;
        }
        if (!ValueObject.class.isAssignableFrom(valueObjectClass)) {
            throw new ValueObjectMappingException("Not able to map BaseActionForm to Non-ValueObject class.");
        }
        
        try {
            ValueObject valueObject = (ValueObject)valueObjectClass.newInstance();
            return toValueObject(actionForm, valueObject);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ValueObjectMappingException(e.getMessage());
        }
    }
    
    /**
     * To map the ActionForm instance to the ValueObject instance.
     *
     * @param actionForm  ActionForm instance.
     * @param valueObject ValueObject instance.
     * @return Mapped ValueObject instance.
     * @throws ValueObjectMappingException If error occurs during mapping.
     */
    protected ValueObject toValueObject(ActionForm actionForm, ValueObject valueObject)
    throws ValueObjectMappingException {
        if (actionForm == null || valueObject == null) {
            return null;
        }
        
        try {
            BeanUtils.copyBean(actionForm, valueObject);
            return valueObject;
        } catch (InvocationTargetException e) {
            Throwable t = e.getTargetException();
            if (t == null) t = e;
            logger.error(t.getMessage(), t);
            throw new ValueObjectMappingException(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ValueObjectMappingException(e.getMessage());
        }
    }
    
    /**
     * To map the ValueObject instance to the newly created ActionForm instance.
     *
     * @param valueObject     ValueObject instance.
     * @param actionFormClass ActionForm class.
     * @return Mapped ActionForm instance.
     * @throws ActionFormMappingException If error occurs during mapping.
     */
    protected ActionForm toActionForm(ValueObject valueObject, Class actionFormClass)
    throws ActionFormMappingException {
        if (valueObject == null || actionFormClass == null) {
            return null;
        }
        if (!ActionForm.class.isAssignableFrom(actionFormClass)) {
            throw new ActionFormMappingException("Not able to map ValueObject to Non-ActionForm class.");
        }
        
        try {
            ActionForm actionForm = (ActionForm)actionFormClass.newInstance();
            return toActionForm(valueObject, actionForm);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ActionFormMappingException(e.getMessage());
        }
    }
    
    /**
     * To map the ValueObject instance to the ActionForm instance.
     *
     * @param valueObject ValueObject instance.
     * @param actionForm  ActionForm instance.
     * @return Mapped ActionForm instance.
     * @throws ActionFormMappingException If error occurs during mapping.
     */
    protected ActionForm toActionForm(ValueObject valueObject, ActionForm actionForm)
    throws ActionFormMappingException {
        if (valueObject == null || actionForm == null) {
            return null;
        }
        
        try {
            BeanUtils.copyBean(valueObject, actionForm);
            return actionForm;
        } catch (InvocationTargetException e) {
            Throwable t = e.getTargetException();
            if (t == null) t = e;
            logger.error(t.getMessage(), t);
            throw new ActionFormMappingException(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ActionFormMappingException(e.getMessage());
        }
    }
    
    /**
     * Rethrow application exception.
     *
     * @param e Exception.
     * @return Page to display the business exception message.
     * @throws ApplicationException If Application exception is caught.
     */
    protected String rethrow(Exception e) throws ApplicationException {
        if (e instanceof ApplicationException) {
            throw (ApplicationException)e;
        } else {
            throw new ApplicationException(e);
        }
    }
    
    /**
     * Rethrow application exception. Special handling for business exception which
     * will set the exception code and direct to user-desired page to display the
     * exception message.
     *
     * @param e             Exception.
     * @param exceptionPage Page to display the business exception message.
     * @return Page to display the business exception message.
     * @throws ApplicationException If Application exception is caught.
     */
    protected String rethrow(Exception e, String exceptionPage) throws ApplicationException {
        return rethrow(e, exceptionPage, null, null);
    }
    
    /**
     * Rethrow application exception. Special handling for business exception which
     * will set the exception code and direct to user-desired page to display the
     * exception message.
     *
     * @param e              Exception.
     * @param exceptionPage  Page to display the business exception message.
     * @param screenCode     Screen code for exception page.
     * @param baseActionForm BaseActionForm instance to set the screen code.
     * @return Page to display the business exception message.
     * @throws ApplicationException If Application exception is caught.
     */
    protected String rethrow(Exception e, String exceptionPage, String screenCode, BaseActionForm baseActionForm)
    throws ApplicationException {
        if (e instanceof BusinessException) {
            if (baseActionForm != null) {
                baseActionForm.setScreenCode(screenCode);
            }
            BaseActionContext baseActionContext = baseActionForm.getActionContext();
            baseActionContext.getActionMessages().clear();
            
            if (((BusinessException)e).getArguments() == null) {
                baseActionContext.addMessageResource(((BusinessException)e).getCode());
            } else {
                baseActionContext.addMessageResource(((BusinessException)e).getCode(), ((BusinessException)e).getArguments());
            }
            
            return exceptionPage;
        } else if (e instanceof ApplicationException) {
            throw (ApplicationException)e;
        } else {
            throw new ApplicationException(e);
        }
    }
    
    /**
     * Generates and persists a token into both session and action form object.
     *
     * @param baseActionForm BaseActionForm object.
     */
    protected void generateAndPersistToken(BaseActionForm baseActionForm) {
        Random random = new Random();
        double token = random.nextDouble();
        
        // store token into action form
        baseActionForm.setToken(String.valueOf(token));
        
        // store token in session
        baseActionForm.getActionContext().setCacheObject(TOKEN_KEY, String.valueOf(token));
        
        logger.debug("New token persisted = " + token);
    }
    
    /**
     * Validates the token from user interface to avoid multiple submission.
     *
     * @param baseActionForm BaseActionForm instance.
     * @return boolean flag representing the validity of token.
     */
    protected boolean validateToken(BaseActionForm baseActionForm) {
        logger.debug("Token from JSP   = " + baseActionForm.getToken());
        
        // remove stored token from session
        String storedToken = null;
        synchronized (TOKEN) {
            storedToken = (String)baseActionForm.getActionContext().getCacheObject(TOKEN_KEY);
            logger.debug("Token in session = " + storedToken);
            
            // remove token from session
            if (storedToken != null) {
                baseActionForm.getActionContext().clearCacheObject(TOKEN_KEY);
            }
        }
        
        // if the token is not there, this indicates that it has already been "consumed"
        boolean isValidToken = false;
        if (storedToken == null) {
            logger.debug("Multiple submission detected and disallowed");
            isValidToken = false;
        } else {
            if (storedToken.equals(baseActionForm.getToken())) {
                isValidToken = true;
            } else {
                isValidToken = false;
            }
        }
        logger.debug("Duplicate submission? " + !isValidToken);
        
        return isValidToken;
    }
    
    /**
     * todo: revise design
     * <p/>
     * THE FOLLOWING SECTION IS CONSIDERED TO HAVE NON-GENERIC IMPLEMENTATION IN THIS BASE CLASS.
     * FURTHER EFFORT IS REQUIRED TO ENCAPSULATE THE FOLLOWING LOGICS OUT FROM THIS CLASS (SUBCLASS?).
     * <p/>
     * Comment by wkchee on 20040220.
     */
    
    /**
     * @deprecated Use BaseResourceConstants.ADD_SUCCESS
     */
    protected static final String ADD_SUCCESS_MESSAGE = BaseResourceConstants.ADD_SUCCESS;
    
    /**
     * @deprecated Use BaseResourceConstants.UPDATE_SUCCESS
     */
    protected static final String UPDATE_SUCCESS_MESSAGE = BaseResourceConstants.UPDATE_SUCCESS;
    
    /**
     * @deprecated Use BaseResourceConstants.DELETE_SUCCESS
     */
    protected static final String DELETE_SUCCESS_MESSAGE = BaseResourceConstants.DELETE_SUCCESS;
    
    /**
     * @deprecated Use BaseResourceConstants.DUPLICATE_RECORD
     */
    protected static final String DUPLICATE_RECORD_MESSAGE = BaseResourceConstants.DUPLICATE_RECORD;
    
    /**
     * @deprecated Use BaseResourceConstants.UPDATED_BY_ANOTHER_USER
     */
    protected static final String UPDATED_BY_ANOTHER_USER_MESSAGE = BaseResourceConstants.UPDATED_BY_ANOTHER_USER;
    
    /**
     * @deprecated Use BaseResourceConstants.DELETED_BY_ANOTHER_USER
     */
    protected static final String DELETED_BY_ANOTHER_USER_MESSAGE = BaseResourceConstants.DELETED_BY_ANOTHER_USER;
    
    public static final String SEARCH_PAGE = "search";
    public static final String LISTING_PAGE = "list";
    public static final String DETAIL_PAGE = "detail";
    public static final String ADD_PAGE = "add";
    public static final String EDIT_PAGE = "edit";
    
    /**
     * Ascertains if exception caught refers to concurrent update exception.
     *
     * @param businessException Business Exception object.
     * @return <code>true</code> if it is concurrent update exception;
     *         <code>false</code> otherwise.
     * @since 1.3
     */
    protected boolean isConcurrentUpdate(BusinessException businessException) {
        return businessException.getCode().equals(BaseResourceConstants.UPDATED_BY_ANOTHER_USER);
    }
    
    /**
     * Ascertains if exception caught refers to concurrent delete exception.
     *
     * @param businessException Business Exception object.
     * @return <code>true</code> if it is concurrent delete exception;
     *         <code>false</code> otherwise.
     * @since 1.3
     */
    protected boolean isConcurrentDelete(BusinessException businessException) {
        return businessException.getCode().equals(BaseResourceConstants.DELETED_BY_ANOTHER_USER);
    }
    
    /**
     * Ascertains if exception caught refers to code duplication exception.
     *
     * @param businessException Business Exception object.
     * @return <code>true</code> if it is code duplication exception;
     *         <code>false</code> otherwise.
     * @since 1.9
     */
    protected boolean isDuplicateRecordFound(BusinessException businessException) {
        return businessException.getCode().equals(BaseResourceConstants.DUPLICATE_RECORD);
    }
    
    /**
     * Determines if exception is referring to data validation error.
     *
     * @param businessException Business exception object
     * @return							True if exception object refers to validation error, else, false
     */
    protected boolean isValidationError(BusinessException businessException) {
        return businessException.getCode().equals(BaseResourceConstants.DATA_VALIDATION_ERROR);
    }
    
    /**
     * Processes a List object containing validation error and for populating the details into <code>ActionError</code>
     * object.<br>
     * The List object (errorList) is contained as an arguement in <code>BusinessException</code> object and within it. It
     * represents the list of error message to be presented on screen. For eg:
     * <ul>
     * <li>Code is mandatory
     * <li>Description is mandatory
     * <li>Buy Rate should be more than 0.0
     * </ul>
     * Each member (errorDetails) of this List object in turn, is another List object that contains the details of a single error
     * message, such as the resource key (partial) or any arguement to be embedded in error message display.
     * <br>
     * Refering to above sample, Buy Rate should be more than 0.0 is stored in errorDetails as below:
     * common.message.validation_000030011 (the error message body)
     * sellRate (partial resource key of which when appended with module prefix, shall form a valid message resource key)
     * 0.0 (optional arguement)
     *
     * @param baseActionForm    BaseActionForm object.
     * @param businessException BusinessException object containing validation error details.
     * @param prefix            Module prefix used in message resource file.
     */
    protected void processValidationResult(BaseActionForm baseActionForm, BusinessException businessException, String prefix) {
        ArrayList errorDetails;
        List errorList;
        Object[] temporaryArray = null;
        String messageResourceValue = null;
        StringBuffer messageResourceKey = new StringBuffer();
        int i;
        int j;
        int errorListSize;
        int errorDetailsSize;
        
        // Extract validation error details from arguement of businessException object
        errorList = (List)businessException.getArguments()[0];
        errorListSize = errorList.size();
        
        if (logger.isDebugEnabled()) {
            logger.debug("Number of error message received = " + errorListSize);
        }
        
        if (errorListSize > 0) {
            // Clear ActionMessages object for population of messages from here
            baseActionForm.getActionContext().getActionMessages().clear();
            
            for (i = 0; i < errorListSize; i++) {
                errorDetails = (ArrayList)errorList.get(i);
                errorDetailsSize = errorDetails.size();
                
                if (logger.isDebugEnabled()) {
                    logger.debug("In error message " + i + ", number of content it contains = " + errorDetailsSize);
                }
                
                // If errorDetails object has arguements embedded, extract it and append it to arguements object.
                // By default, size of errorDetails object is 1, that is it contains the error message resource key
                if (errorDetailsSize > 1) {
                    // Size of temporaryArray is always lesser than errorDetailsSize by 1 since the first content
                    // of errorDetails object is the error message resource key
                    temporaryArray = new String[errorDetailsSize - 1];
                    
                    // Start index 0 to skip the error message resource key to extract arguement from index 1 onwards
                    for (j = 1; j < errorDetailsSize; j++) {
                        // Perform following action:
                        // 1) Extract content from errorDetails object (this represents a key in message resource file)
                        // 2) Concatenate it with the module's prefix
                        // 3) Look up for local sensitive message from message resource
                        //
                        // Content from errorDetails object is not necessarily meant to be a resource key, i.e,
                        // an object could be purely an arguement to be embedded in error message, for eg:
                        //          Transaction amount is more than allowable limit: ->3000.00<-
                        //
                        // In above case, the lookup to message resource would produce following result, depending on
                        // the setting:
                        // 1) If the <message-resource/> tag in struts-config.xml declares the attribute null="false",
                        //    the lookup would return the following value:
                        //          ???[locale_key].[message_resource_key]???
                        // 2) If the attribute is not specified, the lookup would return null
                        
                        // Prepare message resource key
                        messageResourceKey.delete(0, messageResourceKey.length());
                        messageResourceKey.append(prefix);
                        messageResourceKey.append(errorDetails.get(j));
                        
                        if (logger.isDebugEnabled()) {
                            logger.debug("Content extract from errorDetails = " + errorDetails.get(j));
                            logger.debug("Message resource key to look for = " + messageResourceKey);
                        }
                        
                        // Retrieve locale sensitive message
                        messageResourceValue = baseActionForm.getActionContext().getMessage(messageResourceKey.toString());
                        
                        if (logger.isDebugEnabled()) {
                            logger.debug("Message mapped from resource file = " + messageResourceValue);
                        }
                        
                        // Perform checking on value returned from message resource lookup (refer to above
                        // remarks for more details)
                        if (messageResourceValue != null && (messageResourceValue.indexOf("???") == -1)) {
                            logger.debug("Message mapped from resource file is to be used");
                            // Index of temporaryArray however, is to start from index 0
                            temporaryArray[j - 1] = messageResourceValue;
                        } else {
                            logger.debug("Original content is to be used");
                            temporaryArray[j - 1] = (String)errorDetails.get(j);
                        }
                    }
                    
                    // Add error details to message resource object with associated arguement(s)
                    baseActionForm.getActionContext().addMessageResource(errorDetails.get(0).toString(), temporaryArray);
                } else {
                    // Add error details to message resource object
                    baseActionForm.getActionContext().addMessageResource(errorDetails.get(0).toString());
                }
                
                // Empty content of errorDetails object for next iteration
                errorDetails.clear();
            }
        }
    }
    
    /**
     * DEPRECATED SECTION.
     */
    
    /**
     * @deprecated No longer use as not thread-safe variable.
     */
    private ActionMapping actionMapping;
    
    /**
     * @deprecated No longer use as not thread-safe variable.
     */
    private ActionMessages actionMessages;
    
    /**
     * @deprecated No longer use as not thread-safe variable.
     */
    private ActionErrors actionErrors;
    
    /**
     * @deprecated No longer use as not thread-safe variable.
     */
    private AppContext appContext;
    
    /**
     * @deprecated No longer use as not thread-safe variable.
     */
    private HttpServletRequest request;
    
    /**
     * Rethrow application exception. Special handling for business exception which
     * will set the exception code and direct to user-desired page to display the
     * exception message.
     *
     * @param e             Exception.
     * @param exceptionPage Page to display the business exception message.
     * @param screenCode    Screen code for exception page.
     * @return Page to display the business exception message.
     * @throws ApplicationException If Application exception is caught.
     * @deprecated Use rethrow(Exception e, String exceptionPage, String screenCode, BaseActionForm actionForm)
     */
    protected String rethrow(Exception e, String exceptionPage, String screenCode)
    throws ApplicationException {
        return rethrow(e, exceptionPage, null, null);
    }
    
    /**
     * Add message resource code.
     *
     * @param messageResource Message resource code.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().addMessageResource(String messageResource)
     */
    protected void addMessageResource(String messageResource) {
        addMessageResource(messageResource, new Object[]{});
    }
    
    /**
     * Add message resource code with customized parameters.
     *
     * @param messageResource Message resource code.
     * @param params          Parameters.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().addMessageResource(String messageResource, Object[] params)
     */
    protected void addMessageResource(String messageResource, Object[] params) {
        if (getActionMessages() == null) {
            setActionMessages(new ActionMessages());
        }
        getActionMessages().add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(messageResource, params));
    }
    
    /**
     * Clear message resource codes that have been set previously.
     *
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getActionMessages().clear()
     */
    protected void clearMessageResource() {
        if (getActionMessages() != null) {
            getActionMessages().clear();
        }
    }
    
    /**
     * Add error code.
     *
     * @param errorCode Error code.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().addErrorCode(String errorCode)
     */
    protected void addErrorCode(String errorCode) {
        if (getActionErrors() == null) {
            setActionErrors(new ActionErrors());
        }
        getActionErrors().add(ActionErrors.GLOBAL_ERROR, new ActionError("common.error.referenceNo", errorCode));
    }
    
    /**
     * Clear error code.
     *
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getActionErrors().clear()
     */
    protected void clearErrorCode() {
        if (getActionErrors() != null) {
            getActionErrors().clear();
        }
    }
    
    /**
     * Gets action messages.
     *
     * @return Action messages.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getActionMessages()
     */
    protected ActionMessages getActionMessages() {
        if (actionMessages == null) {
            actionMessages = new ActionMessages();
        }
        return actionMessages;
    }
    
    /**
     * Sets action messages.
     *
     * @param actionMessages Action messages.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().setActionMessages(ActionMessages actionMessages)
     */
    protected void setActionMessages(ActionMessages actionMessages) {
        this.actionMessages = actionMessages;
    }
    
    /**
     * Gets action errors.
     *
     * @return Action errors.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getActionErrors()
     */
    protected ActionErrors getActionErrors() {
        if (actionErrors == null) {
            actionErrors = new ActionErrors();
        }
        return actionErrors;
    }
    
    /**
     * Sets action errors.
     *
     * @param actionErrors Action errors.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().setActionErrors(ActionErrors actionErrors)
     */
    protected void setActionErrors(ActionErrors actionErrors) {
        this.actionErrors = actionErrors;
    }
    
    /**
     * Gets action mapping.
     *
     * @return Action mapping.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getActionMapping()
     */
    protected ActionMapping getActionMapping() {
        return actionMapping;
    }
    
    /**
     * Sets action mapping.
     *
     * @param actionMapping Action mapping.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().setActionMapping(ActionMapping actionMapping)
     */
    protected void setActionMapping(ActionMapping actionMapping) {
        this.actionMapping = actionMapping;
    }
    
    /**
     * To synchorize the changes to AppContext across multiple servers.
     *
     * @param observable Observable instance.
     * @param o          Object.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().update(Observable observable, Object o)
     */
    public void update(Observable observable, Object o) {
        if (observable instanceof AppContext) {
            setCacheObject(AppConstants.APP_CONTEXT, appContext);
        }
    }
    
    /**
     * Get the HTTP servlet request. You are not advised to deal directly with
     * HTTP Servlet Request as the Action Form is not fully encapsulating the
     * details to be passed over to JSPs.
     *
     * @return HTTP servlet request.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getRequest()
     */
    protected HttpServletRequest getRequest() {
        return request;
    }
    
    /**
     * Set the servlet request.
     *
     * @param request Servlet request.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().setRequest(HttpServletRequest request)
     */
    protected void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    /**
     * Gets application context.
     *
     * @return Application context.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getAppContext()
     */
    protected AppContext getAppContext() {
        return appContext;
    }
    
    /**
     * Set the app context.
     *
     * @param appContext Application context.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().setAppContext(AppContext appContext)
     */
    protected void setAppContext(AppContext appContext) {
        this.appContext = appContext;
    }
    
    /**
     * Adds object to cache.
     *
     * @param object Object to be cached.
     * @return Cache Id.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().setCacheObject(Object object)
     */
    protected boolean setCacheObject(Object object) {
        return setCacheObject("cacheObject", object);
    }
    
    /**
     * Adds object to cache.
     *
     * @param key    Key.
     * @param object Object to be cached.
     * @return Cache Id.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().setCacheObject(String key, Object object)
     */
    protected boolean setCacheObject(String key, Object object) {
        HttpSession session = getRequest().getSession(false);
        if (session == null) {
            return false;
        }
        session.setAttribute(key, object);
        
        if (logger.isDebugEnabled()) {
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(baos);
                os.writeObject(object);
                logger.debug("Cache object size is " + baos.toByteArray().length);
            } catch (IOException e) {
                logger.debug("Cache object size is unknown");
            }
        }
        
        return true;
    }
    
    /**
     * Retrieves cache object.
     *
     * @return Cache object.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getCacheObject()
     */
    protected Object getCacheObject() {
        HttpSession session = getRequest().getSession(false);
        if (session == null) {
            return null;
        }
        return session.getAttribute("cacheObject");
    }
    
    /**
     * Retrieves cache object.
     *
     * @param key Key.
     * @return Cache object.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().getCacheObject(String key)
     */
    protected Object getCacheObject(String key) {
        HttpSession session = getRequest().getSession(false);
        if (session == null) {
            return null;
        }
        return session.getAttribute(key);
    }
    
    /**
     * Clear cache object.
     *
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().clearCacheObject()
     */
    protected void clearCacheObject() {
        HttpSession session = getRequest().getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute("cacheObject");
    }
    
    /**
     * Clear cache object.
     *
     * @param key Key.
     * @deprecated WARNING! This is not thread-safe method. Use BaseActionForm.getActionContext().clearCacheObject(String key)
     */
    protected void clearCacheObject(String key) {
        HttpSession session = getRequest().getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(key);
    }
    
    /**
     * Validates the token from user interface to avoid multiple submission.
     *
     * @param token Token object.
     * @return boolean flag representing the validity of token.
     * @deprecated WARNING! This is not thread-safe method. Use validateToken(BaseActionForm baseActionForm, String token)
     */
    protected boolean validateToken(String token) {
        logger.debug("Token from JSP   = " + token);
        
        // remove stored token from session
        String storedToken = null;
        synchronized (TOKEN) {
            storedToken = (String)getCacheObject(TOKEN_KEY);
            logger.debug("Token in session = " + storedToken);
            
            // remove token from session
            if (storedToken != null) {
                clearCacheObject(TOKEN_KEY);
            }
        }
        
        // if the token is not there, this indicates that it has already been "consumed"
        boolean isValidToken = false;
        if (storedToken == null) {
            logger.debug("Multiple submission detected and disallowed");
            isValidToken = false;
        } else {
            if (storedToken.equals(token)) {
                isValidToken = true;
            } else {
                isValidToken = false;
            }
        }
        logger.debug("Duplicate submission? " + !isValidToken);
        
        return isValidToken;
    }
    
       /**
     * Method which determines whether a particular event has to be validated or not
     *
     * @param event of type String
     * @return boolean
     */

    protected boolean isValidationRequired(String event) {
        boolean result = false;
         
        return result;
    }
    
    protected ActionForm validateParameter(String action, ActionForm actionForm) {
       //ActionErrors errors = null;
         
        return actionForm;
    } 
    
    protected String getDefaultEvent(String action) {
                 
        System.out.println("Going back to SAME action");
        return action;
    }  
    
    protected String getDefaultActionName() {
                 
        System.out.println("Default Action is null!");
        return "";
    }
    
    
}

// end of BaseAction.java