 

// AbstractMaintenanceActionForm.java

package com.esmart2u.solution.web.struts.controller;

import java.util.List;

import com.esmart2u.solution.web.struts.controller.BaseActionForm;

/**
 * Base maintenance action form.
 *
 * @author Chee Weng Keong
 * @version $Id: AbstractMaintenanceActionForm.java,v 1.7 2004/01/16 04:53:32 wkchee Exp $
 */

public abstract class AbstractMaintenanceActionForm extends BaseActionForm
{
    private boolean approvalRequired;
    private boolean isButtonVisible;
    private boolean recordReplacement;
    private boolean mappingToHostFlag;
    private List hostSourceMappingList;

    /**
     * @todo This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    private String backAction;
    /**
     * @todo This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    private String replaceAction;
    /**
     * @todo This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    private String pageTitleKey;
    /**
     * @todo This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    private String action;
    /**
     * @todo This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    private String actionFormName;

    private String codeDataType;
    private int codeDataLength;
    private int descriptionDataLength;

    public boolean isApprovalRequired()
    {
        return approvalRequired;
    }

    public void setApprovalRequired(boolean approvalRequired)
    {
        this.approvalRequired = approvalRequired;
    }

    public boolean isButtonVisible()
    {
        return isButtonVisible;
    }

    public void setButtonVisible(boolean buttonVisible)
    {
        isButtonVisible = buttonVisible;
    }

    public boolean isRecordReplacement()
    {
        return recordReplacement;
    }

    public void setRecordReplacement(boolean recordReplacement)
    {
        this.recordReplacement = recordReplacement;
    }

    public boolean isMappingToHostFlag()
    {
        return mappingToHostFlag;
    }

    public void setMappingToHostFlag(boolean mappingToHostFlag)
    {
        this.mappingToHostFlag = mappingToHostFlag;
    }

    public List getHostSourceMappingList()
    {
        return hostSourceMappingList;
    }

    public void setHostSourceMappingList(List hostSourceMappingList)
    {
        this.hostSourceMappingList = hostSourceMappingList;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public String getBackAction()
    {
        return backAction;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public void setBackAction(String backAction)
    {
        this.backAction = backAction;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public String getReplaceAction()
    {
        return replaceAction;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public void setReplaceAction(String replaceAction)
    {
        this.replaceAction = replaceAction;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public String getPageTitleKey()
    {
        return pageTitleKey;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public void setPageTitleKey(String pageTitleKey)
    {
        this.pageTitleKey = pageTitleKey;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public String getAction()
    {
        return action;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public void setAction(String action)
    {
        this.action = action;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public String getActionFormName()
    {
        return actionFormName;
    }

    /**
     * @deprecated This attribute is no longer applicable as Record Replacement handling is now made simpler.
     */
    public void setActionFormName(String actionFormName)
    {
        this.actionFormName = actionFormName;
    }

    public String getCodeDataType()
    {
        return codeDataType;
    }

    public void setCodeDataType(String codeDataType)
    {
        this.codeDataType = codeDataType;
    }

    public int getCodeDataLength()
    {
        return codeDataLength;
    }

    public void setCodeDataLength(int codeDataLength)
    {
        this.codeDataLength = codeDataLength;
    }

    public int getDescriptionDataLength()
    {
        return descriptionDataLength;
    }

    public void setDescriptionDataLength(int descriptionDataLength)
    {
        this.descriptionDataLength = descriptionDataLength;
    }

    public void reset() throws Exception
    {
        boolean backupApprovalRequired = approvalRequired;
        super.reset();
        approvalRequired = backupApprovalRequired;
    }
}

// end of AbstractMaintenanceActionForm.java