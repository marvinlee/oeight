/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BaseActionContext.java

package com.esmart2u.solution.web.struts.controller;

import java.io.*;
import java.util.Observable;
import java.util.Observer;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.*;
import org.apache.struts.util.MessageResources;

import com.esmart2u.solution.base.helper.AppConstants;
import com.esmart2u.solution.base.helper.AppContext;
import com.esmart2u.solution.base.logging.Logger;

/**
 * Base action context to store each request context information.
 *
 * @author Chee Weng Keong
 * @version $Id: BaseActionContext.java,v 1.2 2004/06/15 10:46:42 wkchee Exp $
 */

public class BaseActionContext
    implements Observer, Serializable
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(BaseActionContext.class);

    private MessageResources messageResources;
    private ActionMapping actionMapping;
    private ActionMessages actionMessages;
    private ActionErrors actionErrors;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private AppContext appContext;
    private Locale locale;

    public MessageResources getMessageResources()
    {
        return messageResources;
    }

    public void setMessageResources(MessageResources messageResources)
    {
        this.messageResources = messageResources;
    }

    public ActionMapping getActionMapping()
    {
        return actionMapping;
    }

    public void setActionMapping(ActionMapping actionMapping)
    {
        this.actionMapping = actionMapping;
    }

    public ActionMessages getActionMessages()
    {
        if (actionMessages == null)
        {
            actionMessages = new ActionMessages();
        }
        return actionMessages;
    }

    public void setActionMessages(ActionMessages actionMessages)
    {
        this.actionMessages = actionMessages;
    }

    public ActionErrors getActionErrors()
    {
        if (actionErrors == null)
        {
            actionErrors = new ActionErrors();
        }
        return actionErrors;
    }

    public void setActionErrors(ActionErrors actionErrors)
    {
        this.actionErrors = actionErrors;
    }

    /**
     * Get the HTTP servlet request. You are not advised to deal directly with
     * HTTP Servlet Request as the Action Form is not fully encapsulating the
     * details to be passed over to JSPs.
     *
     * @return HTTP servlet request.
     */
    public HttpServletRequest getRequest()
    {
        return request;
    }

    public void setRequest(HttpServletRequest request)
    {
        this.request = request;
    }

    public AppContext getAppContext()
    {
        if (appContext == null)
            appContext = new AppContext();
        return appContext;
    }

    public void setAppContext(AppContext appContext)
    {
        this.appContext = appContext;

        if (this.appContext != null)
        {
            // register this action context as the observer to the AppContext instance
            this.appContext.addObserver(this);
        }
    }

    public Locale getLocale()
    {
        return locale;
    }

    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }

    /**
     * To synchorize the changes to AppContext across multiple servers.
     *
     * @param observable Observable instance.
     * @param object     Object.
     */
    public void update(Observable observable, Object object)
    {
        if (observable instanceof AppContext)
        {
            setCacheObject(AppConstants.APP_CONTEXT, appContext);
        }
    }

    /**
     * Add message resource code.
     *
     * @param messageResource Message resource code.
     */
    public void addMessageResource(String messageResource)
    {
        addMessageResource(messageResource, new Object[]{});
    }

    /**
     * Add message resource code with customized parameters.
     *
     * @param messageResource Message resource code.
     * @param params          Parameters.
     */
    public void addMessageResource(String messageResource, Object[] params)
    {
        getActionMessages().add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(messageResource, params));
    }

    /**
     * Add error code.
     *
     * @param errorCode Error code.
     */
    public void addErrorCode(String errorCode)
    {
        getActionErrors().add(ActionErrors.GLOBAL_ERROR, new ActionError("common.error.referenceNo", errorCode));
    }

    /**
     * Adds object to cache.
     *
     * @param object Object to be cached.
     * @return Cache Id.
     */
    public boolean setCacheObject(Object object)
    {
        return setCacheObject("cacheObject", object);
    }

    /**
     * Adds object to cache.
     *
     * @param key    Key.
     * @param object Object to be cached.
     * @return Cache Id.
     */
    public boolean setCacheObject(String key, Object object)
    {
        HttpSession session = getRequest().getSession(false);
        if (session == null)
        {
            return false;
        }
        session.setAttribute(key, object);

        if (logger.isDebugEnabled())
        {
            try
            {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream os = new ObjectOutputStream(baos);
                os.writeObject(object);
                logger.debug("Cache object size is " + baos.toByteArray().length);
            }
            catch (IOException e)
            {
                logger.debug("Cache object size is unknown");
            }
        }

        return true;
    }

    /**
     * Retrieves cache object.
     *
     * @return Cache object.
     */
    public Object getCacheObject()
    {
        HttpSession session = getRequest().getSession(false);
        if (session == null)
        {
            System.out.println("Session is null!");
            return null;
        }
        return session.getAttribute("cacheObject");
    }

    /**
     * Retrieves cache object.
     *
     * @param key Key.
     * @return Cache object.
     */
    public Object getCacheObject(String key)
    {
        HttpSession session = getRequest().getSession(false);
        if (session == null)
        {
            System.out.println("Session is null!");
            return null;
        }
        return session.getAttribute(key);
    }

    /**
     * Clear cache object.
     */
    public void clearCacheObject()
    {
        HttpSession session = getRequest().getSession(false);
        if (session == null)
        {
            return;
        }
        session.removeAttribute("cacheObject");
    }

    /**
     * Clear cache object.
     *
     * @param key Key.
     */
    public void clearCacheObject(String key)
    {
        HttpSession session = getRequest().getSession(false);
        if (session == null)
        {
            return;
        }
        session.removeAttribute(key);
    }

    /**
     * Obtains locale sensitive message from message resource file based on a given key.
     *
     * @param messageKey Message key to look up in message resource file.
     * @return Message from message resource file. If no match is found, return null.
     */
    protected String getMessage(String messageKey)
    {
        String message = null;

        if (messageResources != null)
        {
            logger.debug("Message resources found");
            message = messageResources.getMessage(getLocale(), messageKey);
        }
        else
        {
            logger.debug("Message resources not found");
            message = null;
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("Message to return = " + message);
        }

        return message;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }
    
}

// end of BaseActionContext.java