/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ActionFormMappingException.java

package com.esmart2u.solution.web.struts.controller;

/**
 * Action form mapping exception.
 *
 * @author Chee Weng Keong
 * @version $Id: ActionFormMappingException.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ActionFormMappingException extends Exception
{
    public ActionFormMappingException()
    {
        super("ActionFormMappingException is caught.");
    }

    public ActionFormMappingException(String message)
    {
        super(message);
    }
}

// end of ActionFormMappingException.java