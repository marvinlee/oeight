  
// ActionForm.java

package com.esmart2u.solution.web.struts.controller;

import java.util.Locale;
import java.util.List;
import java.util.ArrayList;
import java.sql.Date;
import org.apache.log4j.Logger;

import org.apache.struts.action.*;

import com.esmart2u.solution.base.helper.*;

/**
 * Base action form.
 *  
 */

public class BaseActionForm extends ActionForm
{
    
    private static Logger logger = Logger.getLogger(BaseActionForm.class);
    private BaseActionContext baseActionContext = new BaseActionContext();

    private String token;
    private String screenCode;
    private List actionList = new ArrayList(0);

    private String systemDateYear;
    private String systemDateMonth;
    private String systemDateDay;
    private String systemDateHour;
    private String systemDateMinute;
    private String systemDateSecond;

    public BaseActionContext getActionContext()
    {
        return baseActionContext;
    }

    public void setActionContext(BaseActionContext baseActionContext)
    {
        this.baseActionContext = baseActionContext;
    }

    public Locale getLocale()
    {
        return getActionContext().getLocale();
    }

    public void setLocale(Locale locale)
    {
        getActionContext().setLocale(locale);
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getScreenCode()
    {
        return screenCode;
    }

    public void setScreenCode(String screenCode)
    {
        this.screenCode = screenCode;
    }

    public List getActionList()
    {
        return actionList;
    }

    public void setActionList(List actionList)
    {
        this.actionList = actionList;
    }

    public String getSystemDateYear()
    {
        return systemDateYear;
    }

    public void setSystemDateYear(String systemDateYear)
    {
        this.systemDateYear = systemDateYear;
    }

    public String getSystemDateMonth()
    {
        return systemDateMonth;
    }

    public void setSystemDateMonth(String systemDateMonth)
    {
        this.systemDateMonth = systemDateMonth;
    }

    public String getSystemDateDay()
    {
        return systemDateDay;
    }

    public void setSystemDateDay(String systemDateDay)
    {
        this.systemDateDay = systemDateDay;
    }

    public String getSystemDateHour()
    {
        return systemDateHour;
    }

    public void setSystemDateHour(String systemDateHour)
    {
        this.systemDateHour = systemDateHour;
    }

    public String getSystemDateMinute()
    {
        return systemDateMinute;
    }

    public void setSystemDateMinute(String systemDateMinute)
    {
        this.systemDateMinute = systemDateMinute;
    }

    public String getSystemDateSecond()
    {
        return systemDateSecond;
    }

    public void setSystemDateSecond(String systemDateSecond)
    {
        this.systemDateSecond = systemDateSecond;
    }

    public Date getSystemDateTime()
    {
        if (getActionContext().getLocale() == null)
        {
            return null;
        }
        else
        {
            return DateUtils.getDateTime(systemDateYear, systemDateMonth, systemDateDay, systemDateHour, systemDateMinute, systemDateSecond, getActionContext().getLocale());
        }
    }

    public void setSystemDateTime(Date systemDateTime)
    {
        if (systemDateTime == null || getActionContext().getLocale() == null)
        {
            systemDateYear = null;
            systemDateMonth = null;
            systemDateDay = null;
            systemDateHour = null;
            systemDateMinute = null;
            systemDateSecond = null;
        }
        else
        {
            String dates[] = DateUtils.convertDateTimeToStrings(systemDateTime, getActionContext().getLocale());
            systemDateYear = dates[0];
            systemDateMonth = dates[1];
            systemDateDay = dates[2];
            systemDateHour = dates[3];
            systemDateMinute = dates[4];
            systemDateSecond = dates[5];
        }
    }

    public void reset() throws Exception
    {
        
        System.out.println("************** BaseActionForm reset method is called");
        logger.debug("************** BaseActionForm reset method is called");
        BaseActionContext backupBaseActionContext = baseActionContext;
        String backupToken = token;
        String backupScreenCode = screenCode;
        List backupActionList = actionList;
        String backupSystemDateYear = systemDateYear;
        String backupSystemDateMonth = systemDateMonth;
        String backupSystemDateDay = systemDateDay;
        String backupSystemDateHour = systemDateHour;
        String backupSystemDateMinute = systemDateMinute;
        String backupSystemDateSecond = systemDateSecond;

        // to avoid system variables to be reset
        baseActionContext = null;
        token = null;
        screenCode = null;
        actionList = null;
        systemDateYear = null;
        systemDateMonth = null;
        systemDateDay = null;
        systemDateHour = null;
        systemDateMinute = null;
        systemDateSecond = null;
 
        ActionMapping backupActionMapping = actionMapping;
        actionMapping = null; 
        
        // perform reset
        BeanUtils.resetBean(this);

        baseActionContext = backupBaseActionContext;
        token = backupToken;
        screenCode = backupScreenCode;
        actionList = backupActionList;
        systemDateYear = backupSystemDateYear;
        systemDateMonth = backupSystemDateMonth;
        systemDateDay = backupSystemDateDay;
        systemDateHour = backupSystemDateHour;
        systemDateMinute = backupSystemDateMinute;
        systemDateSecond = backupSystemDateSecond;
 
        actionMapping = backupActionMapping; 
    }

    /**
     * todo: revise design
     * <p/>
     * THE FOLLOWING SECTION IS CONSIDERED TO HAVE NON-GENERIC IMPLEMENTATION IN THIS BASE CLASS.
     * FURTHER EFFORT IS REQUIRED TO ENCAPSULATE THE FOLLOWING LOGICS OUT FROM THIS CLASS (SUBCLASS?).
     * <p/>
     * Comment by wkchee on 20040220.
     */

    protected SearchBean searchBean;

    public SearchBean getSearchBean()
    {
        if (searchBean == null)
        {
            searchBean = new SearchBean();
        }
        return searchBean;
    }

    public void setSearchBean(SearchBean searchBean)
    {
        this.searchBean = searchBean;
    }

    public String getSearchCriteria()
    {
        return getSearchBean().getCriteria();
    }

    public void setSearchCriteria(String searchCriteria)
    {
        getSearchBean().setCriteria(searchCriteria);
    }

    public String getSearchValue()
    {
        return getSearchBean().getValue();
    }

    public void setSearchValue(String value)
    {
        getSearchBean().setValue(value);
    }

    public int getCurrentPage()
    {
        return getSearchBean().getCurrentPage();
    }

    public void setCurrentPage(int value)
    {
        getSearchBean().setCurrentPage(value);
    }

    public int getTotalPage()
    {
        return getSearchBean().getTotalPage();
    }

    public void setTotalPage(int value)
    {
        getSearchBean().setTotalPage(value);
    }

    public String getResultLinkKey()
    {
        return getSearchBean().getResultLinkKey();
    }

    public void setResultLinkKey(String value)
    {
        getSearchBean().setResultLinkKey(value);
    }

    public boolean isSearchExecuted()
    {
        return getSearchBean().isSearchExecuted();
    }

    public void setSearchExecuted(boolean searchExecuted)
    {
        getSearchBean().setSearchExecuted(searchExecuted);
    }

    /**
     * @deprecated WARNING! This is not thread-safe variable.
     */
    private ActionMapping actionMapping;

    /**
     * @deprecated WARNING! This is not thread-safe method. Use getActionContext().getActionMapping()
     */
    public ActionMapping getActionMapping()
    {
        return actionMapping;
    }

    /**
     * @deprecated WARNING! This is not thread-safe method. Use getActionContext().setActionMapping(ActionMapping actionMapping)
     */
    public void setActionMapping(ActionMapping actionMapping)
    {
        this.actionMapping = actionMapping;
    }
}

// end of ActionForm.java