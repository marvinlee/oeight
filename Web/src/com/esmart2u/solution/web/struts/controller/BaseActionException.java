/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BaseActionException.java

package com.esmart2u.solution.web.struts.controller;

/**
 * Base action exception.
 *
 * @author Chee Weng Keong
 * @version $Id: BaseActionException.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class BaseActionException extends Exception
{
    public BaseActionException()
    {
        super("BaseActionException is caught");
    }

    public BaseActionException(String message)
    {
        super(message);
    }
}

// end of BaseActionException.java