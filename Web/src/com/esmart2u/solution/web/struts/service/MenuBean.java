/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// MenuBean.java

package com.esmart2u.solution.web.struts.service;

import java.io.Serializable;

/**
 * Menu bean to wrap the menu info from SecurityManager (dsms).
 *
 * @author Chee Weng Keong
 * @version $Id: MenuBean.java,v 1.4 2004/06/15 10:46:42 wkchee Exp $
 */

public class MenuBean
    implements Serializable
{
    private String menuId;
    private String parentId;
    private String menuCode;
    private String menuLink;
    private int menuLevel;
    private int totalSubMenu;

    public String getMenuId()
    {
        return menuId;
    }

    public void setMenuId(String menuId)
    {
        this.menuId = menuId;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getMenuCode()
    {
        return menuCode;
    }

    public void setMenuCode(String menuCode)
    {
        this.menuCode = menuCode;
    }

    public String getMenuLink()
    {
        return menuLink;
    }

    public void setMenuLink(String menuLink)
    {
        this.menuLink = menuLink;
    }

    public int getMenuLevel()
    {
        return menuLevel;
    }

    public void setMenuLevel(int menuLevel)
    {
        this.menuLevel = menuLevel;
    }

    public int getTotalSubMenu()
    {
        return totalSubMenu;
    }

    public void setTotalSubMenu(int totalSubMenu)
    {
        this.totalSubMenu = totalSubMenu;
    }
}

// end of MenuBean.java