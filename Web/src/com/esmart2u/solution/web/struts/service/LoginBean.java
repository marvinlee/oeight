/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// LoginBean.java

package com.esmart2u.solution.web.struts.service;

import java.util.List;

/**
 * Login bean to wrap the details returned by SecurityManager (dsms).
 *
 * @author Chee Weng Keong
 * @version $Id: LoginBean.java,v 1.3 2004/01/16 03:13:30 wkchee Exp $
 */

public class LoginBean
{
    private String sessionId;
    private String branchCode;
    private String branchName;
    private String userName;
    private List applicationRoleList;

    public String getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getBranchCode()
    {
        return branchCode;
    }

    public void setBranchCode(String branchCode)
    {
        this.branchCode = branchCode;
    }

    public String getBranchName()
    {
        return branchName;
    }

    public void setBranchName(String branchName)
    {
        this.branchName = branchName;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public List getApplicationRoleList()
    {
        return applicationRoleList;
    }

    public void setApplicationRoleList(List applicationRoleList)
    {
        this.applicationRoleList = applicationRoleList;
    }
}

// end of LoginBean.java