  

package com.esmart2u.solution.web.struts.service;

import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;

import com.esmart2u.solution.base.logging.Logger;
 

public class CharacterEncodingServlet extends HttpServlet
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(CharacterEncodingServlet.class);

    /**
     * Startup initialization.
     *
     * @throws ServletException If servlet exception is caught.
     */
    public void init() throws ServletException
    {
        String encoding = getInitParameter("encoding");
        getServletContext().setAttribute("encoding", encoding);
        logger.debug("Encoding is initialized to " + encoding);
    }
}

// end of CharacterEncodingServlet.java