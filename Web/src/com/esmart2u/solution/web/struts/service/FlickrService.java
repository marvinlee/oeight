/*
 * FlickrService.java
 *
 * Created on October 31, 2007, 11:47 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.web.struts.service;

import com.aetrion.flickr.people.PeopleInterface;
import com.aetrion.flickr.people.User;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;

import org.xml.sax.SAXException;

import com.aetrion.flickr.Flickr;
import com.aetrion.flickr.FlickrException;
import com.aetrion.flickr.REST;
import com.aetrion.flickr.RequestContext;
import com.aetrion.flickr.auth.Permission;
import com.aetrion.flickr.util.IOUtilities;
/**
 *
 * @author meauchyuan.lee
 */
public class FlickrService {
    
    /** Creates a new instance of FlickrTest */
    private static Logger logger = Logger.getLogger(FlickrService.class);
    
    static String restHost = "www.flickr.com";
    Flickr f;
    REST rest;
    RequestContext requestContext;
    User user = null;
    
    public FlickrService()   {
        
        
    }
    
    public String getUserIdByUsername(String username) {
        
        InputStream in = null;
        
        try {
            rest = new REST();
            rest.setHost(restHost);
            f = new Flickr(PropertyManager.getValue(PropertyConstants.SERVICE_FLICKR_KEY),rest);
            Flickr.debugStream = false;
            // Set the shared secret which is used for any calls which require signing.
            requestContext = RequestContext.getRequestContext();
            PeopleInterface peopleInterface = f.getPeopleInterface();
            user = peopleInterface.findByUsername(username); 
            if (user == null){
                return null;
            }
            else
            {
                logger.debug("Flickr User Id:" + user.getId());
                return  user.getId();
            }
        } catch (ParserConfigurationException ex) {
            logger.error("Error getUserIdByUsername:" + ex.getMessage());
            return null;
        } catch (SAXException ex) {
            logger.error("Error getUserIdByUsername:" + ex.getMessage());
            return null;
        } catch (FlickrException ex) {
            logger.error("Error getUserIdByUsername:" + ex.getMessage());
            return null;
        } catch (IOException ex) {
            logger.error("Error getUserIdByUsername:" + ex.getMessage());
            return null;
        }
    }
    
    public boolean validFlickrId(String flickrId) {
        
        InputStream in = null;
        try {
            
            rest = new REST();
            rest.setHost(restHost);
            f = new Flickr(PropertyManager.getValue(PropertyConstants.SERVICE_FLICKR_KEY),rest);
            Flickr.debugStream = false;
            // Set the shared secret which is used for any calls which require signing.
            requestContext = RequestContext.getRequestContext();
            PeopleInterface peopleInterface = f.getPeopleInterface();
            
            user = peopleInterface.getInfo(flickrId);
            
            
            if (user == null){
                System.out.println("Cond 1: User is null....");
                return false;
            }
            else if (flickrId.equalsIgnoreCase(user.getId())){
                System.out.println("Cond 2: User id matched.");
                logger.debug("Flickr User Id matches:" + user.getId());
                return  true;
            }
            else
            { 
                System.out.println("Cond 3: User is ???");
                System.out.println("Flickr Id Input :" + flickrId + "|");
                System.out.println("Flickr User Id  :" + user.getId() + "|");
                return false;
            }
            
        } catch (ParserConfigurationException ex) {
            logger.error("Error validFlickrId:" + ex.getMessage());
            return false;
        } catch (SAXException ex) {
            logger.error("Error validFlickrId:" + ex.getMessage());
            return false;
        } catch (FlickrException ex) {
            logger.error("Error validFlickrId:" + ex.getMessage());
            return false;
        } catch (IOException ex) {
            logger.error("Error validFlickrId:" + ex.getMessage());
            return false;
        }
    }
    
    public static void main(String[] args) {
        
    }
}
