// StartupServlet.java

package com.esmart2u.solution.web.struts.service;

import com.esmart2u.oeight.member.helper.smtp.InvitesEmailSender;
import com.esmart2u.oeight.member.helper.smtp.ResetPasswordEmailSender;
import com.esmart2u.oeight.member.helper.smtp.SeasonsGreetingsEmailSender;
import com.esmart2u.oeight.member.web.struts.helper.MosaicHelper;
import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.beanutils.converters.BigIntegerConverter;
import org.apache.commons.beanutils.converters.SqlDateConverter;

import com.esmart2u.solution.base.logging.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Startup servlet to perform initializations.
 * 
 * @version $Id: StartupServlet.java,v 1.1 2004/03/31 11:26:13 wkchee Exp $
 */

public class StartupServlet extends HttpServlet implements PropertyConstants
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(StartupServlet.class);

    /**
     * Initialization.
     *
     * @throws ServletException If servlet exception is caught.
     */
    public void init() throws ServletException
    {
        super.init();
        try
        {
            System.setProperty("true","true");
            //Using XML, replace this with PropertyConfigurator if using properties file 
            /*System.out.println("STARTUP Servlet is called, going to configure log4j");
            PropertyConfigurator.configure("C:\\webConfig\\log4j.properties");
            System.out.println("Done configuration");*/
            //Using XML, replace this with PropertyConfigurator if using properties file 
            
            // Setting property Manager
            System.setProperty(MAIN_CONFIG_FILE_KEY, MAIN_CONFIG_FILE_VALUE);
            
            
            // Setting log4j
            System.out.println("Configurating log4j.xml");
            String log4jConfig = PropertyManager.getValue(LOG4J_CONFIG_FILE);
            System.out.println("log4j.xml from=" + log4jConfig);
            
            if (log4jConfig.endsWith("xml"))
            {
                DOMConfigurator.configure(log4jConfig);   
            }else
            {
                PropertyConfigurator.configure(log4jConfig);
            }
            logger.debug("Configurating log from " + log4jConfig); 

            logger.info("Startup = init");
            
            logger.debug("Start Mosaic Helper");
            //MosaicHelper.getInstance();
            
            logger.debug("Registering BeanUtils Converter for BigInteger, BigDecimal, SqlDate classes");
            ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
            ConvertUtils.register(new BigIntegerConverter(null), java.math.BigInteger.class);
            ConvertUtils.register(new SqlDateConverter(null), java.sql.Date.class);
            
            // Email daemons
            //SeasonsGreetingsEmailSender.getInstance().invoke(true);
            //InvitesEmailSender.getInstance().invoke(true);
            //ResetPasswordEmailSender.getInstance().invoke(true);
            
            logger.info("Startup = done");
        }
        catch (Exception e)
        {
            logger.error("Error while startup", e);
        }
    }
}

// end of StartupServlet.java