/*
 * Recaptcha4JService.java
 *
 * Created on January 12, 2008, 3:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.web.struts.service;

import com.esmart2u.solution.base.helper.ConfigurationHelper;
import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaFactory;
/**
 *
 * @author meauchyuan.lee
 */
public class Recaptcha4JService {
    
    /** Creates a new instance of Recaptcha4JService */
    public Recaptcha4JService() {
    }
    
    private static boolean enabled = PropertyManager.getBoolean(PropertyConstants.RECAPTCHA_ENABLED,true);
    private static String publickey = PropertyManager.getValue(PropertyConstants.RECAPTCHA_PUBLIC_KEY);
    private static String privatekey = PropertyManager.getValue(PropertyConstants.RECAPTCHA_PRIVATE_KEY);

    private static ReCaptcha instance = ReCaptchaFactory.newReCaptcha(publickey, privatekey, false);

    public static ReCaptcha getReCaptchaInstance() {
            return instance;
    }

    public static boolean isEnabled() {
          
        return enabled;
    }

    public static void setEnabled(boolean aEnabled) {
        enabled = aEnabled;
    }
    
}
