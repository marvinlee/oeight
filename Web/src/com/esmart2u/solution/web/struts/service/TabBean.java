/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// TabBean.java

package com.esmart2u.solution.web.struts.service;

import java.io.Serializable;

/**
 * Tab bean to wrap the details returned by SecurityManager (dsms).
 *
 * @author Goh Siew Chyn
 * @version $Id: TabBean.java,v 1.3 2004/01/16 03:13:30 wkchee Exp $
 */

public class TabBean
    implements Serializable
{
    private String tabGroupCode;
    private String tabCode;
    private String href;

    public String getTabGroupCode()
    {
        return tabGroupCode;
    }

    public void setTabGroupCode(String tabGroupCode)
    {
        this.tabGroupCode = tabGroupCode;
    }

    public String getTabCode()
    {
        return tabCode;
    }

    public void setTabCode(String tabCode)
    {
        this.tabCode = tabCode;
    }

    public String getHref()
    {
        return href;
    }

    public void setHref(String href)
    {
        this.href = href;
    }
}

// end of TabBean.java