 

package com.esmart2u.solution.web.struts.service;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import sun.io.ByteToCharConverter;

import com.esmart2u.solution.base.logging.Logger;

/**
 * This filter sets the character encoding to be used for current
 * request and response.
 *
 * @author Chee Weng Keong
 * @version $Id: CharacterEncodingFilter.java,v 1.5 2004/06/15 10:46:42 wkchee Exp $
 * @deprecated Use com.esmart2u.solution.web.struts.filter.CharacterEncodingFilter
 */

public class CharacterEncodingFilter
    implements Filter
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(CharacterEncodingFilter.class);

    /**
     * The filter configuration object we are associated with. If this value is null,
     * this filter instance is not currently configured.
     */
    protected FilterConfig filterConfig = null;

    /**
     * Place this filter into service.
     *
     * @param filterConfig The filter configuration object.
     * @throws ServletException If Servlet exception is caught.
     */
    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        this.filterConfig = filterConfig;
    }

    /**
     * This filter sets the character encoding to be used for current
     * request and response.
     *
     * @param servletRequest  The servlet request we are processing.
     * @param servletResponse The servlet response we are creating.
     * @param filterChain     The filter chain we are processing.
     * @throws IOException      If an input/output error occurs.
     * @throws ServletException If a servlet error occurs.
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException, ServletException
    {
        String encoding = getUserEncoding();

        if (encoding == null)
        {
            // use character encoding returned by client browser
            encoding = servletRequest.getCharacterEncoding();
            if (!verifyEncoding(encoding)) encoding = null;
        }

        if (encoding == null)
        {
            // use character encoding from filter config
            encoding = filterConfig.getInitParameter("encoding");
            if (!verifyEncoding(encoding)) encoding = null;
        }

        if (encoding == null)
        {
            // use character encoding from application context
            encoding = (String)filterConfig.getServletContext().getAttribute("encoding");
            if (!verifyEncoding(encoding)) encoding = null;
        }

        if (encoding != null)
        {
            logger.debug("servletRequest.setCharacterEncoding -> " + encoding);
            servletRequest.setCharacterEncoding(encoding);
            logger.debug("servletResponse.setContentType -> text/html;charset=" + encoding);
            servletResponse.setContentType("text/html;charset=" + encoding);
        }

        // pass control on to the next filter
        filterChain.doFilter(servletRequest, new HttpServletResponseWrapper((HttpServletResponse)servletResponse)
        {
            public void setContentType(String s)
            {
                // the following statement has been remarked for future trace on who reset the content type
                //System.out.println("CharacterEncodingFilter.setContentType " + s);
                //new Throwable().printStackTrace();

                if (s != null && s.startsWith("text/html"))
                {
                    // do nothing. this call could be trying to set the charset to another charset
                    // this is the case with BEA/Tomcat/Jetty, whose JSP compiler sets the charset, whether
                    // it is specified in the JSP page or not.

                    // NB - this can also be accomplished by setting the charset manually in the
                    // JSP page & the decorator, but this approach allows for run-time flexibility
                    // of choosing the charsets.
                }
                else
                {
                    super.setContentType(s);
                }
            }
        });

        logger.debug("servletResponse.getCharacterEncoding() = " + servletResponse.getCharacterEncoding());
    }

    /**
     * Take this filter out of service.
     */
    public void destroy()
    {
        this.filterConfig = null;
    }

    /**
     * Get encoding from current user's configuration.
     *
     * @return User-defined encoding.
     */
    private String getUserEncoding()
    {
        // you may get the user-defined encoding from session, properties... etc.
        // currently not implemented because we aim to read the encoding from filterConfig
        // which means configured in web.xml
        return null;
    }

    /**
     * Verify the encoding before setting it to the HttpRequest or HttpResponse.
     *
     * @param encoding Encoding.
     * @return True if valid; Else false.
     */
    private boolean verifyEncoding(String encoding)
    {
        try
        {
            ByteToCharConverter.getConverter(encoding);
            logger.debug("Verifying encoding " + encoding + "...OK");
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
 