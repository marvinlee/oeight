/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ScreenActionBean.java

package com.esmart2u.solution.web.struts.service;

import java.io.Serializable;

/**
 * Screen Action bean wrap the actions returned by Security Manager (dsms).
 *
 * @author Chee Weng Keong
 * @version $Id: ScreenActionBean.java,v 1.4 2004/01/16 03:13:30 wkchee Exp $
 */

public class ScreenActionBean
    implements Serializable
{
    private String actionCode;
    private String actionDescription;

    public String getActionCode()
    {
        return actionCode;
    }

    public void setActionCode(String actionCode)
    {
        this.actionCode = actionCode;
    }

    public String getActionDescription()
    {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription)
    {
        this.actionDescription = actionDescription;
    }
}

// end of ScreenActionBean.java