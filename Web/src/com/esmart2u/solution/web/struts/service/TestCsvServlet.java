/*
 * TestCsvServlet.java
 *
 * Created on October 25, 2007, 4:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.web.struts.service;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;

import com.esmart2u.solution.base.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author meauchyuan.lee
 */
public class TestCsvServlet  extends HttpServlet{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(TestCsvServlet.class);
    
    /**
     * Initialization.
     *
     * @throws ServletException If servlet exception is caught.
     */
    public void init() throws ServletException {
        super.init();
    }
    
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
         response.setContentType("application/vnd.ms-excel");
         response.setHeader("Content-Disposition","attachment;filename=game.csv;");
         PrintWriter out = response.getWriter();
         out.print("Header 1,Header 2,Header 3,Header 4\n");
         out.print("test1,test2,test3,test4\n");
         out.print("a,b,c,d\n");
         out.print("testA,testB,testC,testD\n");
            out.close();



    }
     public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
         doGet(request,response);
    }
    /** Creates a new instance of TestCsvServlet */
    public TestCsvServlet() {
    }
    
}
