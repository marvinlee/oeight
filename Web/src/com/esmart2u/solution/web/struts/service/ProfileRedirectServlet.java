/*
 * ProfileRedirectServlet.java
 *
 * Created on October 11, 2007, 1:59 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.web.struts.service;

import com.esmart2u.solution.base.helper.PropertyConstants;
import com.esmart2u.solution.base.helper.PropertyManager;
import com.esmart2u.solution.base.helper.StringUtils;
import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;

import com.esmart2u.solution.base.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author meauchyuan.lee
 */
public class ProfileRedirectServlet extends HttpServlet implements PropertyConstants {
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(ProfileRedirectServlet.class);
    
    /**
     * Initialization.
     *
     * @throws ServletException If servlet exception is caught.
     */
    public void init() throws ServletException {
        super.init();
    }
    
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        if (!StringUtils.hasValue(name))
        {
            response.sendRedirect("/profile/not_found.jsp");
        }
        else
        {
            logger.debug("Profile for [" + name + "] is requested");
            response.sendRedirect("/profile.do?act=view&id=" + name);
        }
    }
     public void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
         doGet(request,response);
    }
    
}

