/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BaseScreenActionConstants.java

package com.esmart2u.solution.web.struts.helper;

/**
 * Screen action code constants.
 *
 * @author Chee Weng Keong
 * @version $Id: BaseScreenActionConstants.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public interface BaseScreenActionConstants
{
    // general action code
    public static final String ADD_ACTION_CODE = "ADD";
    public static final String EDIT_ACTION_CODE = "EDIT";
    public static final String DELETE_ACTION_CODE = "DELETE";
    public static final String HYPERLINK_ACTION_CODE = "HYPERLINK";
    public static final String CREATE_ACTION_CODE = "CREATE";
    public static final String UPDATE_ACTION_CODE = "UPDATE";
}

// end of BaseScreenActionConstants.java