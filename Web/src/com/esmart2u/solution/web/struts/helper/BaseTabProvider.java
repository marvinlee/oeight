/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BaseTabProvider.java

package com.esmart2u.solution.web.struts.helper;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import com.esmart2u.solution.base.helper.StringUtils;

/**
 * Base tab provider.
 *
 * @author Lee Meau Chyuan
 * @version $Id: BaseTabProvider.java,v 1.4 2004/02/04 12:23:05 wkchee Exp $
 */

public class BaseTabProvider
{
    /**
     * Add new tab information.
     *
     * @param tabName Tab name.
     * @param tabDisplayName Tab display name.
     * @param href Href.
     * @return Array of tab information.
     */
    public static String[] newTabInformation(String tabName, String tabDisplayName, String href)
    {
        String tabInfo[] = new String[3];
        tabInfo[0] = tabName;
        tabInfo[1] = tabDisplayName;
        tabInfo[2] = href;
        return tabInfo;
    }

    /**
     * Returns path from action mapping.
     *
     * @param actionMapping Action mapping.
     * @param name Action name.
     * @return Action path.
     */
    public static String getPath(ActionMapping actionMapping, String name)
    {
        if (actionMapping == null || !StringUtils.hasValue(name))
        {
            return "";
        }
        ActionForward actionForward = actionMapping.findForward(name);
        if (actionForward == null)
        {
            return "";
        }
        return actionForward.getPath();
    }
}

// end of BaseTabProvider.java