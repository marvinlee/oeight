/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ActionBeanException.java

package com.esmart2u.solution.web.struts.helper;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

public class ActionBeanException
    extends Exception
{
    private ActionErrors errors;

    public ActionBeanException()
    {
        super();
        errors = new ActionErrors();
    }

    public ActionBeanException(String e)
    {
        super(e);
        errors = new ActionErrors();
    }

    public void setActionErrors(String errorLink, ActionError ae)
    {
        errors.add(errorLink, ae);
    }

    public ActionErrors getActionErrors()
    {
        return errors;
    }
}

// end of ActionBeanException.java