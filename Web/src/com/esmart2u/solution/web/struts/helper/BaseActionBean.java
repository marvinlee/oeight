/**
 * � 2002 esmart2u Malaysia Sdn. Bhd.  All rights reserved.
 * MarvinLee.net
 *  
 *
 * This software is the intellectual property of esmart2u Malaysia Sdn. Bhd.
 * The   program may be used only in accordance with the terms of the license
 * agreement you entered into with esmart2u Malaysia Sdn. Bhd.
 */

// BaseActionBean.java

package com.esmart2u.solution.web.struts.helper;

import com.esmart2u.solution.base.logging.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import javax.servlet.ServletException;

import org.apache.commons.beanutils.BeanUtils;

/**
 * A base class for <code>ActionBean</code>s
 * <p>Contains useful utility methods that normally gets repeated
 * in most <code>ActionBean</code>s
 *
 * @author Peter Govind
 * @version $Id: BaseActionBean.java,v 1.3 2004/03/03 03:53:25 wkchee Exp $
 *
 * @deprecated No longer use in latest framework.
 */

public class BaseActionBean
{
    /**
     * Logger.
     */
    private Logger logger = Logger.getLogger(BaseActionBean.class);

    /**
     * Populates destination object based on properties from source object.
     *
     * @param destination Object to be populated.
     * @param source      Object with data used to populate the destination object
     * @throws ServletException on any error. The caller can catch this
     *                          so as to stop further processing once error(s) encountered.
     */
    public void populate(Object destination, Object source)
        throws ServletException
    {
        // Retrieve values from the ActionForm to the extended ValueObject
        boolean ok = true;
        try
        {
            Map hm = BeanUtils.describe(source);
            BeanUtils.populate(destination, hm);
        }
        catch (IllegalAccessException e)
        {
            ok = false;
            logger.error(e.toString());
            // errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("helper.error.general", t.toString()));
            // throw new ServletException(this.getClass().getName(), t);
        }
        catch (InvocationTargetException e)
        {
            Throwable t = e.getTargetException();
            if (t == null)
                t = e;
            ok = false;
            logger.error(e.toString());
            // errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("helper.error.general", t.toString()));
            // throw new ServletException(this.getClass().getName(), t);
        }
        catch (NoSuchMethodException e)
        {
            ok = false;
            logger.error(e.toString());
            // errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("helper.error.general", t.toString()));
            // throw new ServletException(this.getClass().getName(), t);
        }
        finally
        {
            if (!ok)
            {
                logger.error("Exception while converting properties from " +
                             source.getClass().getName() + " to " +
                             destination.getClass().getName());
                throw new ServletException("Populate error. Pls check app server logs");
            }
        }
    }
}

// end of BaseActionBean.java