 

package com.esmart2u.solution.web.struts.filter;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.IOException;
import javax.servlet.ServletOutputStream;
 

public class TrimResponseStream extends ServletOutputStream
{
    private DataOutputStream stream;

    public TrimResponseStream(OutputStream outputstream)
    {
        stream = new DataOutputStream(outputstream);
    }

    public void write(int b)
        throws IOException
    {
        stream.write(b);
    }

    public void write(byte b[])
        throws IOException
    {
        stream.write(b);
    }

    public void write(byte b[], int off, int len)
        throws IOException
    {
        stream.write(b, off, len);
    }
}

// end of TrimResponseStream.java