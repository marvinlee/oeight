 

package com.esmart2u.solution.web.struts.filter;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.ServletOutputStream;
 

public class TrimResponseWrapper extends HttpServletResponseWrapper
{
    private ByteArrayOutputStream output;
    private String contentType;

    public TrimResponseWrapper(HttpServletResponse response)
    {
        super(response);
        this.output = new ByteArrayOutputStream();
    }

    public void setContentType(String contentType)
    {
        super.setContentType(contentType);
        this.contentType = contentType;
    }

    public String getContentType()
    {
        return contentType;
    }

    public byte[] getData()
    {
        try
        {
            output.flush();
        }
        catch (Exception exception)
        {
            // quietly ignore
        }
        return output.toByteArray();
    }

    public ServletOutputStream getOutputStream()
    {
        return new TrimResponseStream(output);
    }

    public PrintWriter getWriter()
    {
        return new PrintWriter(getOutputStream(), true);
    }
}
 