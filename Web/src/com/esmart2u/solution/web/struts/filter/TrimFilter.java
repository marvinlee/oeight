 

package com.esmart2u.solution.web.struts.filter;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import sun.io.CharToByteConverter;
import sun.io.ByteToCharConverter;

import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.base.helper.Converter;
 

public class TrimFilter
    implements Filter
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(TrimFilter.class);
    private static Logger report = Logger.getLogger("TrimFilterReport");

    /**
     * The filter configuration object we are associated with. If this value is null,
     * this filter instance is not currently configured.
     */
    protected FilterConfig filterConfig = null;

    /**
     * Flag to remove comments.
     */
    protected boolean removeComments = false;

    /**
     * Threshold number to perform trimming.
     */
    protected int threshold = 0;

    /**
     * Place this filter into service.
     *
     * @param filterConfig The filter configuration object.
     * @throws ServletException If Servlet exception is caught.
     */
    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        this.filterConfig = filterConfig;

        // retrieve flag to remove comments
        if (filterConfig != null)
        {
            String s = filterConfig.getInitParameter("removeComments");
            if (s != null && Converter.toBooleanValue(s))
            {
                removeComments = true;
            }
        }

        // retrieve threshold
        String s = filterConfig.getInitParameter("threshold");
        if (s != null)
        {
            try
            {
                threshold = Integer.parseInt(s);
            }
            catch (NumberFormatException e)
            {
                logger.debug("Threshold should be integer or 0 (trim all) or -1 (no trim)");
                logger.debug("Set threshold to 0 (trim all)");
                threshold = 0;
            }
        }
        else
        {
            threshold = 0;
        }
    }

    /**
     * Take this filter out of service.
     */
    public void destroy()
    {
        this.filterConfig = null;
    }

    /**
     * This filter performs trimming.
     *
     * @param request  The servlet request we are processing.
     * @param response The servlet response we are creating.
     * @param chain    The filter chain we are processing.
     * @throws IOException      If an input/output error occurs.
     * @throws ServletException If a servlet error occurs.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        ServletOutputStream output = response.getOutputStream();

        // pass control on to the next filter
        TrimResponseWrapper responseWrapper = new TrimResponseWrapper((HttpServletResponse)response);
        chain.doFilter(request, responseWrapper);

        byte allBytes[] = responseWrapper.getData();

        String contentType = responseWrapper.getContentType();
        if (contentType != null)
        {
            response.setContentType(contentType);
            if (contentType.toLowerCase().startsWith("text/") && allBytes.length > threshold && threshold != -1)
            {
                logger.debug("Performing trim now...");

                long startTime = System.currentTimeMillis();
                int initialSize = allBytes.length;
                logger.debug("Total bytes before trim = " + initialSize);

                char allChars[] = ByteToCharConverter.getConverter(response.getCharacterEncoding()).convertAll(allBytes);

                StringBuffer sb = new StringBuffer("");
                int currentIndex = 0;
                char tempChar = ' ';
                while (currentIndex < allChars.length)
                {
                    char currentChar;
                    if ((currentChar = allChars[currentIndex]) == '\r')
                    {
                        currentChar = '\n';
                    }
                    if (removeComments && currentChar == '<' && currentIndex + 2 < allChars.length && allChars[currentIndex + 1] == 33)
                    {
                        int tempIndex = currentIndex;
                        for (currentIndex += 2; currentIndex < allChars.length; currentIndex++)
                        {
                            currentChar = allChars[currentIndex];
                            if (currentChar != '>' || allChars[currentIndex - 1] != 45 || allChars[currentIndex - 2] != 45)
                            {
                                continue;
                            }
                            if (currentIndex - 4 >= 0 && allChars[currentIndex - 3] == 47 && allChars[currentIndex - 4] == 47)
                            {
                                sb.append('<');
                                currentIndex = tempIndex;
                                tempChar = '<';
                            }
                            else
                            {
                                tempChar = ' ';
                            }
                            break;
                        }

                        currentIndex++;
                        continue;
                    }
                    if (tempChar == 10)
                    {
                        if (currentChar == '\n')
                        {
                            currentIndex++;
                            continue;
                        }
                        if (currentChar == ' ')
                        {
                            currentIndex++;
                            continue;
                        }
                        if (currentChar == '\t')
                        {
                            currentIndex++;
                            continue;
                        }
                    }
                    if ((tempChar == 32 || tempChar == 9) && (currentChar == ' ' || currentChar == '\t'))
                    {
                        currentIndex++;
                    }
                    else
                    {
                        tempChar = currentChar;
                        sb.append(currentChar);
                        currentIndex++;
                    }
                }

                byte[] allTrimmedBytes = CharToByteConverter.getConverter(response.getCharacterEncoding()).convertAll(sb.toString().toCharArray());

                long stopTime = System.currentTimeMillis();
                long trimmedSize = allTrimmedBytes.length;
                logger.debug("Total bytes after trimmed = " + trimmedSize);

                long trimmingTime = stopTime - startTime;
                logger.debug("Total trim time (ms) = " + trimmingTime);

                if (report.isInfoEnabled())
                {
                    // requestUrl, initialSize, trimmedSize, trimmingTime
                    report.info(((HttpServletRequest)request).getRequestURI() + "," + initialSize + "," + trimmedSize + "," + trimmingTime);
                }

                output.write(allTrimmedBytes);
            }
            else
            {
                output.write(allBytes);
            }
        }
        else
        {
            output.write(allBytes);
        }

        output.close();
    }
}

// end of TrimFilter.java