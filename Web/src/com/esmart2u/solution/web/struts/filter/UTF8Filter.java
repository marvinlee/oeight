/*
 * UTF8Filter.java
 *
 * Created on March 27, 2008, 3:08 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.web.struts.filter;

import java.io.IOException;
import javax.servlet.*;
/**
 *
 * @author meauchyuan.lee
 */
public class UTF8Filter implements Filter {
    public void destroy() {}
    
    public void doFilter(ServletRequest request,
            ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        request.setCharacterEncoding("UTF8");
        chain.doFilter(request, response);
    }
    
    public void init(FilterConfig filterConfig)
    throws ServletException {}
}

