 

package com.esmart2u.solution.web.struts.filter;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;

import sun.io.ByteToCharConverter;

import com.esmart2u.solution.base.logging.Logger;
 

public class CharacterEncodingFilter
    implements Filter
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(CharacterEncodingFilter.class);

    /**
     * The filter configuration object we are associated with. If this value is null,
     * this filter instance is not currently configured.
     */
    protected FilterConfig filterConfig = null;

    /**
     * Place this filter into service.
     *
     * @param filterConfig The filter configuration object.
     * @throws ServletException If Servlet exception is caught.
     */
    public void init(FilterConfig filterConfig)
        throws ServletException
    {
        this.filterConfig = filterConfig;
    }

    /**
     * Take this filter out of service.
     */
    public void destroy()
    {
        this.filterConfig = null;
    }

    /**
     * This filter sets the character encoding to be used for current
     * request and response.
     *
     * @param request  The servlet request we are processing.
     * @param response The servlet response we are creating.
     * @param chain    The filter chain we are processing.
     * @throws IOException      If an input/output error occurs.
     * @throws ServletException If a servlet error occurs.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        String encoding = getUserEncoding();
        if (encoding == null)
        {
            // use character encoding returned by client browser
            encoding = request.getCharacterEncoding();
            if (!verifyEncoding(encoding)) encoding = null;
        }
        if (encoding == null)
        {
            // use character encoding from filter config
            encoding = filterConfig.getInitParameter("encoding");
            if (!verifyEncoding(encoding)) encoding = null;
        }
        if (encoding == null)
        {
            // use character encoding from application context
            encoding = (String)filterConfig.getServletContext().getAttribute("encoding");
            if (!verifyEncoding(encoding)) encoding = null;
        }
        if (encoding != null)
        {
            logger.debug("request.setCharacterEncoding -> " + encoding);
            request.setCharacterEncoding(encoding);
            logger.debug("response.setContentType -> text/html;charset=" + encoding);
            response.setContentType("text/html;charset=" + encoding);
        }

        // pass control on to the next filter
        CharacterEncodingResponseWrapper responseWrapper = new CharacterEncodingResponseWrapper((HttpServletResponse)response);
        chain.doFilter(request, responseWrapper);

        logger.debug("response.getCharacterEncoding() = " + response.getCharacterEncoding());
    }

    /**
     * Get encoding from current user's configuration.
     *
     * @return User-defined encoding.
     */
    private String getUserEncoding()
    {
        // you may get the user-defined encoding from session, properties... etc.
        // currently not implemented because we aim to read the encoding from filterConfig
        // which means configured in web.xml
        return null;
    }

    /**
     * Verify the encoding before setting it to the HttpRequest or HttpResponse.
     *
     * @param encoding Encoding.
     * @return True if valid; Else false.
     */
    private boolean verifyEncoding(String encoding)
    {
        try
        {
            if (encoding != null)
            {
                ByteToCharConverter.getConverter(encoding);
                logger.debug("Verifying encoding " + encoding + "...OK");
                return true;
            }
        }
        catch (Exception e)
        {
            // quietly ignore
        }

        return false;
    }
}

// end of CharacterEncodingFilter.java