 

package com.esmart2u.solution.web.struts.filter;

import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpServletResponse;
 
public class CharacterEncodingResponseWrapper extends HttpServletResponseWrapper
{
    public CharacterEncodingResponseWrapper(HttpServletResponse response)
    {
        super(response);
    }

    public void setContentType(String contentType)
    {
        if (contentType != null && contentType.toLowerCase().startsWith("text/"))
        {
            // do nothing. this call could be trying to set the charset to another charset
            // this is the case with BEA/Tomcat/Jetty, whose JSP compiler sets the charset, whether
            // it is specified in the JSP page or not.

            // NB - this can also be accomplished by setting the charset manually in the
            // JSP page & the decorator, but this approach allows for run-time flexibility
            // of choosing the charsets.
        }
        else
        {
            super.setContentType(contentType);
        }
    }
}

// end of CharacterEncodingResponseWrapper.java