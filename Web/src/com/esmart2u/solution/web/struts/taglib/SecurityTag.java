/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// SecurityTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.struts.action.*;

import com.esmart2u.solution.base.helper.AppContext;
import com.esmart2u.solution.base.helper.AppConstants;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.logging.Logger;
import com.esmart2u.solution.web.struts.controller.BaseAction;
//import com.esmart2u.solution.web.struts.service.SecurityManager;

/**
 * Check session time out and valid screen access.
 *
 * @author Goh Siew Chyn
 * @version $Id: SecurityTag.java,v 1.3 2004/02/20 07:14:56 wkchee Exp $
 */

public class SecurityTag extends BodyTagSupport
{
    private static final Logger logger = Logger.getLogger(SecurityTag.class);

    /**
     * Screen code of the page.
     */
    private String screenCode = null;

    public SecurityTag()
    {
        screenCode = null;
    }

    /**
     * Getter for Screen code.
     */
    public String getScreenCode()
    {
        return screenCode;
    }

    /**
     * Setter for Screen code.
     */
    public void setScreenCode(String screenCode)
    {
        this.screenCode = screenCode;
    }

    /**
     * Start of Tag Processing
     */
    public int doStartTag() throws JspException
    {
        return EVAL_BODY_BUFFERED;
    }

    /**
     * End of Tag Processing
     *
     * @throws JspException if a JSP exception occurs
     */
    public int doEndTag() throws JspException
    {
        ActionErrors actionErrors = new ActionErrors();
        AppContext appContext = (AppContext)pageContext.getSession().getAttribute(AppConstants.APP_CONTEXT);
        try
        {
            /*if (SecurityManager.isSessionTimeout(appContext))
            {
                actionErrors.add(ActionErrors.GLOBAL_ERROR, new ActionError("common.message.sec_login_100001008"));
                saveMessages(actionErrors);
                pageContext.forward(BaseAction.LOGIN_PAGE);
                return SKIP_PAGE;
            }*/
        }
        catch (Exception e)
        {
            try
            {
                logger.error(e.getMessage(), e);
                pageContext.forward(BaseAction.ERROR_PAGE);
                return SKIP_PAGE;
            }
            catch (Exception e1)
            {
                throw new JspException(e1);
            }
        }

        return EVAL_PAGE;
    }

    public void saveMessages(ActionMessages actionMessages)
    {
        if (actionMessages == null || actionMessages.isEmpty())
        {
            pageContext.getRequest().removeAttribute("org.apache.struts.action.ACTION_MESSAGE");
        }
        else
        {
            pageContext.getRequest().setAttribute("org.apache.struts.action.ACTION_MESSAGE", actionMessages);
        }
    }

    public void release()
    {
        super.release();
        screenCode = null;
    }
}

// end of SecurityTag.java