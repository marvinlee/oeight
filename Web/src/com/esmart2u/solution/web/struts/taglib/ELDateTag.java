/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELDateTag.java

package com.esmart2u.solution.web.struts.taglib;

import org.apache.strutsel.taglib.utils.EvalHelper;

import javax.servlet.jsp.JspException;

/**
 * Custom tag for date.
 * <p/>
 * This class is a subclass of the class
 * <code>DateTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author Goh Siew Chyn
 * @version $Id: ELDateTag.java,v 1.6 2004/06/15 10:46:42 wkchee Exp $
 */

public class ELDateTag extends DateTag
{
    private String nameExpr;
    private String dayPropertyExpr;
    private String monthPropertyExpr;
    private String yearPropertyExpr;
    private String onChangeExpr;
    private String onBlurExpr;
    private String disabledExpr;
    private String indexedExpr;

    public String getNameExpr()
    {
        return nameExpr;
    }

    public void setNameExpr(String nameExpr)
    {
        this.nameExpr = nameExpr;
    }

    public String getDayPropertyExpr()
    {
        return dayPropertyExpr;
    }

    public void setDayPropertyExpr(String dayPropertyExpr)
    {
        this.dayPropertyExpr = dayPropertyExpr;
    }

    public String getMonthPropertyExpr()
    {
        return monthPropertyExpr;
    }

    public void setMonthPropertyExpr(String monthPropertyExpr)
    {
        this.monthPropertyExpr = monthPropertyExpr;
    }

    public String getYearPropertyExpr()
    {
        return yearPropertyExpr;
    }

    public void setYearPropertyExpr(String yearPropertyExpr)
    {
        this.yearPropertyExpr = yearPropertyExpr;
    }

    public String getOnChangeExpr()
    {
        return onChangeExpr;
    }

    public void setOnChangeExpr(String onChangeExpr)
    {
        this.onChangeExpr = onChangeExpr;
    }

    public String getOnBlurExpr()
    {
        return onBlurExpr;
    }

    public void setOnBlurExpr(String onBlurExpr)
    {
        this.onBlurExpr = onBlurExpr;
    }

    public String getDisabledExpr()
    {
        return disabledExpr;
    }

    public void setDisabledExpr(String disabledExpr)
    {
        this.disabledExpr = disabledExpr;
    }

    public String getIndexedExpr()
    {
        return indexedExpr;
    }

    public void setIndexedExpr(String indexedExpr)
    {
        this.indexedExpr = indexedExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        String string = null;
        Boolean bool= null;

        if ((string = EvalHelper.evalString("name", getNameExpr(), this, pageContext)) != null)
            setName(string);
        if ((string = EvalHelper.evalString("dayProperty", getDayPropertyExpr(), this, pageContext)) != null)
            setDayProperty(string);
        if ((string = EvalHelper.evalString("monthProperty", getMonthPropertyExpr(), this, pageContext)) != null)
            setMonthProperty(string);
        if ((string = EvalHelper.evalString("yearProperty", getYearPropertyExpr(), this, pageContext)) != null)
            setYearProperty(string);
        if ((string = EvalHelper.evalString("onChange", getOnChangeExpr(), this, pageContext)) != null)
            setOnChange(string);
        if ((string = EvalHelper.evalString("onBlur", getOnBlurExpr(), this, pageContext)) != null)
            setOnBlur(string);
        if ((bool = EvalHelper.evalBoolean("disabled", getDisabledExpr(), this, pageContext)) != null)
            setDisabled(bool.booleanValue());
        if ((bool = EvalHelper.evalBoolean("indexed", getIndexedExpr(), this, pageContext)) != null)
            setIndexed(bool.booleanValue());
    }
}

// end of ELDateTag.java