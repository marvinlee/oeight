/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELOptionTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;
import java.beans.SimpleBeanInfo;

/**
 * BeanInfo of ELOptionsTag Class.
 *
 * @author Siripong Visasmongkolchai
 * @version $Id: ELOptionsTagBeanInfo.java,v 1.5 2004/04/02 10:58:23 wkchee Exp $
 */

public class ELOptionsTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("collection", ELOptionsTag.class,
                                                null, "setCollectionExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("filter", ELOptionsTag.class,
                                                null, "setFilterExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("labelName", ELOptionsTag.class,
                                                null, "setLabelNameExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("labelProperty", ELOptionsTag.class,
                                                null, "setLabelPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("name", ELOptionsTag.class,
                                                null, "setNameExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("property", ELOptionsTag.class,
                                                null, "setPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("style", ELOptionsTag.class,
                                                null, "setStyleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("styleClass", ELOptionsTag.class,
                                                null, "setStyleClassExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("isShowDefault", ELOptionsTag.class,
                                                null, "setIsShowDefaultExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("isShowBlank", ELOptionsTag.class,
                                                null, "setIsShowBlankExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("isShowCode", ELOptionsTag.class,
                                                null, "setIsShowCodeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("isShowAlternateLabel", ELOptionsTag.class,
                                                null, "setIsShowAlternateLabelExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("delimiterKey", ELOptionsTag.class,
                                                null, "setDelimiterKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("openBracketKey", ELOptionsTag.class,
                                                null, "setOpenBracketKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("closeBracketKey", ELOptionsTag.class,
                                                null, "setCloseBracketKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELOptionsTagBeanInfo.java