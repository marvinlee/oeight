/**
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 1999-2003 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 */

// ImportTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.*;

import org.apache.taglibs.standard.tag.common.core.*;
import org.apache.taglibs.standard.tag.el.core.ExpressionUtil;

/**
 * <p>A handler for &lt;import&gt; that accepts attributes as Strings
 * and evaluates them as expressions at runtime.</p>
 *
 * @author Goh Siew Chyn
 * @version $Id: ImportTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ImportTag extends ImportSupport
{
    private String contextIn;
    private String charEncodingIn;
    private String urlIn;

    /**
     * Constructs a new ImportTag.  As with TagSupport, subclasses
     * should not provide other constructors and are expected to call
     * the superclass constructor
     */
    public ImportTag()
    {
        super();
        init();
    }

    // evaluates expression and chains to parent
    public int doStartTag() throws JspException
    {

        // evaluate any expressions we were passed, once per invocation
        evaluateExpressions();

        // chain to the parent implementation
        return super.doStartTag();
    }

    // Releases any resources we may have (or inherit)
    public void release()
    {
        super.release();
        init();
    }

    public void setUrl(String urlIn)
    {
        this.urlIn = urlIn;
    }

    public void setContext(String contextIn)
    {
        this.contextIn = contextIn;
    }

    private void init()
    {
        urlIn = contextIn = charEncodingIn = null;
    }

    /**
     * Evaluates expressions as necessary
     */
    private void evaluateExpressions() throws JspException
    {
        charEncodingIn = (String)pageContext.getServletContext().getAttribute("encoding");

        url = (String)ExpressionUtil.evalNotNull("import", "url", urlIn, String.class, this, pageContext);
        if (url == null || url.equals(""))
            throw new NullAttributeException("import", "url");

        context = (String)ExpressionUtil.evalNotNull("import", "context", contextIn, String.class, this, pageContext);
        charEncoding = (String)ExpressionUtil.evalNotNull("import", "charEncoding", charEncodingIn, String.class, this,
                                                          pageContext);
    }
}

// end of ImportTag.java