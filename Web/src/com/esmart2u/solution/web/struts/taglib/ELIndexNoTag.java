/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELIndexNoTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Custom tag for index tag.
 * <p/>
 * This class is a subclass of the class
 * <code>IndexNoTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author Law Tat Yoong.
 * @version $Id: ELIndexNoTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELIndexNoTag extends IndexNoTag
{
    private String indexExpr;
    private String currentPageExpr;
    private String rowsPerPageExpr;

    public String getIndexExpr()
    {
        return indexExpr;
    }

    public void setIndexExpr(String indexExpr)
    {
        this.indexExpr = indexExpr;
    }

    public String getCurrentPageExpr()
    {
        return currentPageExpr;
    }

    public void setCurrentPageExpr(String currentPageExpr)
    {
        this.currentPageExpr = currentPageExpr;
    }

    public String getRowsPerPageExpr()
    {
        return rowsPerPageExpr;
    }

    public void setRowsPerPageExpr(String rowsPerPageExpr)
    {
        this.rowsPerPageExpr = rowsPerPageExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        Object object = null;

        if ((object = EvalHelper.eval("index", getIndexExpr(), this, pageContext)) != null)
            setIndex(Integer.parseInt(object.toString()));
        if ((object = EvalHelper.eval("currentPage", getCurrentPageExpr(), this, pageContext)) != null)
            setCurrentPage(Integer.parseInt(object.toString()));
        if ((object = EvalHelper.eval("rowsPerPage", getRowsPerPageExpr(), this, pageContext)) != null)
            setRowsPerPage(Integer.parseInt(object.toString()));
    }
}

// end of ELIndexNoTag.java