/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ActionButtonTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.List;
import javax.servlet.jsp.JspException;

import org.apache.struts.util.ResponseUtils;
import org.apache.struts.taglib.html.BaseHandlerTag;

import com.esmart2u.solution.web.struts.service.ScreenActionBean;

/**
 * Renders an HTML BUTTON tag with checking user access right.
 *
 * @author Goh Siew Chyn
 * @version $Id: ActionButtonTag.java,v 1.3 2004/01/16 03:13:30 wkchee Exp $
 */

public class ActionButtonTag extends BaseHandlerTag
{
    private final static String CHECKER_MAKER_SYMBOL = "*";

    // Instance Variables

    /**
     * The property name of the generated button.
     */
    protected String property = null;

    /**
     * The body content of this tag (if any).
     */
    protected String text = null;

    /**
     * The value of the button label.
     */
    protected String value = null;

    /**
     * The action code that defined in security.
     */
    protected String actionCode = null;

    /**
     * A list of actions which the user are valid to access.
     */
    protected List actionList = null;

    /**
     * Indicate whether checker maker required.
     */
    protected boolean isApprovalRequired;

    /**
     * Indicate whether button is visible.
     */
    protected boolean isVisible;

    // Properties

    /**
     * Return the property name.
     */
    public String getProperty()
    {
        return (property);
    }

    /**
     * Set the property name.
     *
     * @param property The property name
     */
    public void setProperty(String property)
    {
        this.property = property;
    }

    /**
     * Return the label value.
     */
    public String getValue()
    {
        return (value);
    }

    /**
     * Set the label value.
     *
     * @param value The label value
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    public String getActionCode()
    {
        return actionCode;
    }

    public void setActionCode(String actionCode)
    {
        this.actionCode = actionCode;
    }

    public List getActionList()
    {
        return actionList;
    }

    public void setActionList(List actionList)
    {
        this.actionList = actionList;
    }

    public boolean isApprovalRequired()
    {
        return isApprovalRequired;
    }

    public void setApprovalRequired(boolean approvalRequired)
    {
        isApprovalRequired = approvalRequired;
    }

    public boolean isVisible()
    {
        return isVisible;
    }

    public void setVisible(boolean visible)
    {
        isVisible = visible;
    }

    // Public Methods

    /**
     * Process the start of this tag.
     *
     * @throws JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException
    {
        // Do nothing until doEndTag() is called
        this.text = null;
        return (EVAL_BODY_BUFFERED);
    }

    /**
     * Save the associated label from the body content (if any).
     *
     * @throws JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException
    {
        if (bodyContent != null)
        {
            String value = bodyContent.getString().trim();
            if (value.length() > 0)
                text = value;
        }
        return (SKIP_BODY);
    }

    /**
     * Process the end of this tag.
     * <p/>
     * Support for indexed property since Struts 1.1
     *
     * @throws JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException
    {
        // Acquire the label value we will be generating
        String label = value;
        if ((label == null) && (text != null))
            label = text;
        if ((label == null) || (label.trim().length() < 1))
            label = "Click";

        // Generate an HTML element
        StringBuffer results = new StringBuffer();

        for (int i = 0, size_i = actionList.size(); i < size_i; i++)
        {
            ScreenActionBean screenActionBean = (ScreenActionBean)actionList.get(i);

            if (screenActionBean.getActionCode().equals(actionCode) && isVisible)
            {
                results.append("<input type=\"button\"");
                if (property != null)
                {
                    results.append(" name=\"");
                    results.append(property);
                    // * @since Struts 1.1
                    if (indexed)
                        prepareIndex(results, null);
                    results.append("\"");
                }
                if (accesskey != null)
                {
                    results.append(" accesskey=\"");
                    results.append(accesskey);
                    results.append("\"");
                }
                if (tabindex != null)
                {
                    results.append(" tabindex=\"");
                    results.append(tabindex);
                    results.append("\"");
                }
                if (isApprovalRequired)
                {
                    label = CHECKER_MAKER_SYMBOL + " " + label;
                }
                results.append(" value=\"");
                results.append(label);
                results.append("\"");
                results.append(prepareEventHandlers());
                results.append(prepareStyles());
                results.append(getElementClose());

                // render this element to our writer
                ResponseUtils.write(pageContext, results.toString());
            }
        }

        // evaluate the remainder of this page
        return (EVAL_PAGE);
    }

    /**
     * Release any acquired resources.
     */
    public void release()
    {
        super.release();
        property = null;
        text = null;
        value = null;
        actionCode = null;
        actionList = null;
        isApprovalRequired = false;
        isVisible = false;
    }
}

// end of ActionButtonTag.java