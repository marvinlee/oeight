/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELDateTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.SimpleBeanInfo;
import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the <code>BeanInfo</code> descriptor for the
 * <code>ELDateTag</code> class.  It is
 * needed to override the default mapping of custom tag attribute names to
 * class attribute names.
 * <p/>
 * This is because the value of the unevaluated EL expression has to be kept
 * separately from the evaluated value, which is stored in the controller class. This
 * is related to the fact that the JSP compiler can choose to reuse different
 * tag instances if they received the same original attribute values, and the
 * JSP compiler can choose to not re-call the setter methods, because it can
 * assume the same values are already set.
 *
 * @author Goh Siew Chyn
 * @version $Id: ELDateTagBeanInfo.java,v 1.6 2004/06/15 10:46:42 wkchee Exp $
 */

public class ELDateTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("name", ELDateTag.class,
                                                null, "setNameExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("dayProperty", ELDateTag.class,
                                                null, "setDayPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("monthProperty", ELDateTag.class,
                                                null, "setMonthPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("yearProperty", ELDateTag.class,
                                                null, "setYearPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onChange", ELDateTag.class,
                                                null, "setOnChangeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onBlur", ELDateTag.class,
                                                null, "setOnBlurExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("disabled", ELDateTag.class,
                                                null, "setDisabledExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("indexed", ELDateTag.class,
                                                null, "setIndexedExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELDateTagBeanInfo.java