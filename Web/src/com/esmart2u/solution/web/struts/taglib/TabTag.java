/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// TabTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.*;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.struts.util.RequestUtils;

import com.esmart2u.solution.base.helper.AppConstants;
import com.esmart2u.solution.base.helper.AppContext;
import com.esmart2u.solution.base.logging.Logger;
//import com.esmart2u.solution.web.struts.service.SecurityManager;
import com.esmart2u.solution.web.struts.service.TabBean;

/**
 * Tag used to generate tab.
 *
 * @author Lee Meau Chyuan
 * @version $Id: TabTag.java,v 1.5 2004/03/03 03:53:26 wkchee Exp $
 */

public class TabTag extends BodyTagSupport
{
    private static Logger logger = Logger.getLogger(TabTag.class);

    private String form;
    private List tabList;
    private String tabGroupCode;
    private String currentTab;

    public TabTag()
    {
        form = "";
        tabList = null;
        tabGroupCode = "";
        currentTab = "";
    }

    public String getForm()
    {
        return form;
    }

    public void setForm(String form)
    {
        this.form = form;
    }

    public List getTabList()
    {
        return tabList;
    }

    public void setTabList(List tabList)
    {
        this.tabList = tabList;
    }

    public String getTabGroupCode()
    {
        return tabGroupCode;
    }

    public void setTabGroupCode(String tabGroupCode)
    {
        this.tabGroupCode = tabGroupCode;
    }

    public String getCurrentTab()
    {
        return currentTab;
    }

    public void setCurrentTab(String currentTab)
    {
        this.currentTab = currentTab;
    }

    /**
     * Start of Tag Processing
     */
    public int doStartTag() throws JspException
    {
        try
        {
            boolean dynamicTab = false;
            String tabName, tabNameDisplay = null;
            JspWriter writer = pageContext.getOut();

            if (tabGroupCode != null && !tabGroupCode.equals(""))
            {
                // get the security tab list and populate into hashmap
                AppContext appContext = (AppContext)pageContext.getSession().getAttribute(AppConstants.APP_CONTEXT);
                logger.debug("getting tablist from security manager [" + tabGroupCode
                             + "]  [" + appContext.get(AppConstants.SESSION_ID).toString() + "]");
//                tabList = SecurityManager.getValidTabList(tabGroupCode, appContext.get(AppConstants.SESSION_ID).toString());

                dynamicTab = true;
            }

            if (tabList != null && !tabList.isEmpty())
            {
                writer.println("<table border='1' class='clsForm' width='100%' cellspacing='1'>");
                writer.println("<tr align='center'>");

                Iterator tabIterator = tabList.iterator();
                for (int i = 0; i < tabList.size(); i++)
                {
                    if (dynamicTab)
                    {
                        TabBean tabBean = (TabBean)tabIterator.next();

                        tabName = tabBean.getTabCode();
                        tabNameDisplay = RequestUtils.message(pageContext, null, null, "common.tab." + tabName);
                        tabNameDisplay = tabNameDisplay == null ? "Tab " + (i + 1) : tabNameDisplay;

                        if (currentTab != null && currentTab.equals(tabName))
                        {
                            writer.println("<td class='clsTabListOn'>");
                        }
                        else
                        {
                            writer.println("<td class='clsTabList'>");
                        }

                        // render the path in accordance to web app context path
                        String href = tabBean.getHref() == null ? "" : tabBean.getHref();
                        writer.println("<div onclick=\"javascript:onTabClick('" + form + "','"
                                       + RequestUtils.getActionMappingURL(href, pageContext) + "','"
                                       + tabName + "');\" " +
                                       " onmouseover=\"document.body.style.cursor='pointer'\" " +
                                       " onmouseout=\"document.body.style.cursor='default'\" " +
                                       ">" + tabNameDisplay + "</div></td>");
                    }
                    else
                    {
                        String tab[] = (String[])tabIterator.next();

                        tabName = tab[0];
                        tabNameDisplay = RequestUtils.message(pageContext, null, null, tab[1]);
                        tabNameDisplay = tabNameDisplay == null ? "Tab " + (i + 1) : tabNameDisplay;
                        String tabRequest = tab[2];

                        if (currentTab != null && currentTab.equals(tabName))
                        {
                            writer.println("<td class='clsTabListOn'>");
                        }
                        else
                        {
                            writer.println("<td class='clsTabList'>");
                        }

                        // render the path in accordance to web app context path
                        writer.println("<div onclick=\"javascript:onTabClick('" + form + "','"
                                       + RequestUtils.getActionMappingURL(tabRequest, pageContext) + "','"
                                       + tabName + "');\" " +
                                       " onmouseover=\"document.body.style.cursor='pointer'\" " +
                                       " onmouseout=\"document.body.style.cursor='default'\" " +
                                       ">" + tabNameDisplay + "</div></td>");
                    }

                    writer.println("</td>");
                }
                writer.println("</tr>");
                writer.println("</table>");
            }
        }
        catch (Exception e)
        {
            throw new JspException("IOException while writing to client " + e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
     * End of Tag Processing
     *
     * @throws JspException if a JSP exception occurs
     */
    public int doEndTag() throws JspException
    {
        return EVAL_PAGE;
    }

    public void release()
    {
        super.release();
        form = "";
        tabList = null;
        currentTab = "";
    }
}

// end of TabTag.java