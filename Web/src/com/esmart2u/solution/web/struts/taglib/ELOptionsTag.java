/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELOptionsTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Extends form com.esmart2u.solution.base.web.taglib.OptionsTag
 * to support isShowDefault when in add mode will show Item which isDefault = "Y"
 *
 * @author Siripong Visasmongkolchai
 * @version $Id: ELOptionsTag.java,v 1.5 2004/04/02 10:58:23 wkchee Exp $
 */

public class ELOptionsTag extends OptionsTag
{
    /**
     * Instance variable mapped to "collection" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String collectionExpr;

    /**
     * Instance variable mapped to "filter" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String filterExpr;

    /**
     * Instance variable mapped to "labelName" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String labelNameExpr;

    /**
     * Instance variable mapped to "labelProperty" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String labelPropertyExpr;

    /**
     * Instance variable mapped to "name" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String nameExpr;

    /**
     * Instance variable mapped to "property" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String propertyExpr;

    /**
     * Instance variable mapped to "style" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String styleExpr;

    /**
     * Instance variable mapped to "styleClass" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String styleClassExpr;

    /**
     * Instance variable mapped to "isShowDefault" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String isShowDefaultExpr;

    /**
     * Instance variable mapped to "isShowBlank" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String isShowBlankExpr;

    /**
     * Instance variable mapped to "isShowCode" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String isShowCodeExpr;

    /**
     * Instance variable mapped to "isShowAlternateLabel" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String isShowAlternateLabelExpr;

    /**
     * Instance variable mapped to "delimiterKey" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String delimiterKeyExpr;

    /**
     * Instance variable mapped to "openBracketKey" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String openBracketKeyExpr;

    /**
     * Instance variable mapped to "closeBracketKey" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String closeBracketKeyExpr;

    /**
     * Getter method for "collection" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getCollectionExpr()
    {
        return collectionExpr;
    }

    /**
     * Getter method for "filter" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getFilterExpr()
    {
        return filterExpr;
    }

    /**
     * Getter method for "labelName" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getLabelNameExpr()
    {
        return labelNameExpr;
    }

    /**
     * Getter method for "labelProperty" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getLabelPropertyExpr()
    {
        return labelPropertyExpr;
    }

    /**
     * Getter method for "name" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getNameExpr()
    {
        return nameExpr;
    }

    /**
     * Getter method for "property" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getPropertyExpr()
    {
        return propertyExpr;
    }

    /**
     * Getter method for "style" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getStyleExpr()
    {
        return styleExpr;
    }

    /**
     * Getter method for "styleClass" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getStyleClassExpr()
    {
        return styleClassExpr;
    }

    /**
     * Setter method for "collection" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setCollectionExpr(String collectionExpr)
    {
        this.collectionExpr = collectionExpr;
    }

    /**
     * Setter method for "filter" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setFilterExpr(String filterExpr)
    {
        this.filterExpr = filterExpr;
    }

    /**
     * Setter method for "labelName" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setLabelNameExpr(String labelNameExpr)
    {
        this.labelNameExpr = labelNameExpr;
    }

    /**
     * Setter method for "labelProperty" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setLabelPropertyExpr(String labelPropertyExpr)
    {
        this.labelPropertyExpr = labelPropertyExpr;
    }

    /**
     * Setter method for "name" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setNameExpr(String nameExpr)
    {
        this.nameExpr = nameExpr;
    }

    /**
     * Setter method for "property" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setPropertyExpr(String propertyExpr)
    {
        this.propertyExpr = propertyExpr;
    }

    /**
     * Setter method for "style" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setStyleExpr(String styleExpr)
    {
        this.styleExpr = styleExpr;
    }

    /**
     * Setter method for "styleClass" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setStyleClassExpr(String styleClassExpr)
    {
        this.styleClassExpr = styleClassExpr;
    }

    /**
     * Getter method for "isShowDefault" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getIsShowDefaultExpr()
    {
        return isShowDefaultExpr;
    }

    /**
     * Setter method for "isShowDefault" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setIsShowDefaultExpr(String isShowDefaultExpr)
    {
        this.isShowDefaultExpr = isShowDefaultExpr;
    }

    /**
     * Getter method for "isShowBlank" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getIsShowBlankExpr()
    {
        return isShowBlankExpr;
    }

    /**
     * Setter method for "isShowBlank" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setIsShowBlankExpr(String isShowBlankExpr)
    {
        this.isShowBlankExpr = isShowBlankExpr;
    }

    /**
     * Getter method for "isShowCode" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getIsShowCodeExpr()
    {
        return isShowCodeExpr;
    }

    /**
     * Setter method for "isShowCode" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setIsShowCodeExpr(String isShowCodeExpr)
    {
        this.isShowCodeExpr = isShowCodeExpr;
    }

    /**
     * Getter method for "isShowAlternateLabel" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getIsShowAlternateLabelExpr()
    {
        return isShowAlternateLabelExpr;
    }

    /**
     * Setter method for "isShowAlternateLabel" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setIsShowAlternateLabelExpr(String isShowAlternateLabelExpr)
    {
        this.isShowAlternateLabelExpr = isShowAlternateLabelExpr;
    }

    public String getDelimiterKeyExpr()
    {
        return delimiterKeyExpr;
    }

    public void setDelimiterKeyExpr(String delimiterKeyExpr)
    {
        this.delimiterKeyExpr = delimiterKeyExpr;
    }

    public String getOpenBracketKeyExpr()
    {
        return openBracketKeyExpr;
    }

    public void setOpenBracketKeyExpr(String openBracketKeyExpr)
    {
        this.openBracketKeyExpr = openBracketKeyExpr;
    }

    public String getCloseBracketKeyExpr()
    {
        return closeBracketKeyExpr;
    }

    public void setCloseBracketKeyExpr(String closeBracketKeyExpr)
    {
        this.closeBracketKeyExpr = closeBracketKeyExpr;
    }

    /**
     * Resets attribute values for tag reuse.
     */
    public void release()
    {
        super.release();
        setCollectionExpr(null);
        setFilterExpr(null);
        setLabelNameExpr(null);
        setLabelPropertyExpr(null);
        setNameExpr(null);
        setPropertyExpr(null);
        setStyleExpr(null);
        setStyleClassExpr(null);
        setIsShowCodeExpr(null);
        setIsShowAlternateLabelExpr(null);
        setIsShowBlankExpr(null);
        setIsShowDefaultExpr(null);
        setDelimiterKeyExpr(null);
        setOpenBracketKeyExpr(null);
        setCloseBracketKeyExpr(null);
    }

    /**
     * Process the start tag.
     *
     * @throws JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    /**
     * Processes all attribute values which use the JSTL expression evaluation
     * engine to determine their values.
     *
     * @throws JspException if a JSP exception has occurred
     */
    private void evaluateExpressions() throws JspException
    {
        String string = null;
        Boolean bool = null;

        if ((string = EvalHelper.evalString("collection", getCollectionExpr(),
                                            this, pageContext)) != null)
            setCollection(string);

        if ((bool = EvalHelper.evalBoolean("filter", getFilterExpr(),
                                           this, pageContext)) != null)
            setFilter(bool.booleanValue());

        if ((string = EvalHelper.evalString("labelName", getLabelNameExpr(),
                                            this, pageContext)) != null)
            setLabelName(string);

        if ((string = EvalHelper.evalString("labelProperty", getLabelPropertyExpr(),
                                            this, pageContext)) != null)
            setLabelProperty(string);

        if ((string = EvalHelper.evalString("name", getNameExpr(),
                                            this, pageContext)) != null)
            setName(string);

        if ((string = EvalHelper.evalString("property", getPropertyExpr(),
                                            this, pageContext)) != null)
            setProperty(string);

        if ((string = EvalHelper.evalString("style", getStyleExpr(),
                                            this, pageContext)) != null)
            setStyle(string);

        if ((string = EvalHelper.evalString("styleClass", getStyleClassExpr(),
                                            this, pageContext)) != null)
            setStyleClass(string);
        if ((bool = EvalHelper.evalBoolean("isShowDefault", getIsShowDefaultExpr(),
                                           this, pageContext)) != null)
            setIsShowDefault(bool.booleanValue());
        if ((bool = EvalHelper.evalBoolean("isShowBlank", getIsShowBlankExpr(),
                                           this, pageContext)) != null)
            setIsShowBlank(bool.booleanValue());
        if ((bool = EvalHelper.evalBoolean("isShowCode", getIsShowCodeExpr(),
                                           this, pageContext)) != null)
            setIsShowCode(bool.booleanValue());
        if ((bool = EvalHelper.evalBoolean("isShowAlternateLabel", getIsShowAlternateLabelExpr(),
                                           this, pageContext)) != null)
            setIsShowAlternateLabel(bool.booleanValue());
        if ((string = EvalHelper.evalString("delimiterKey", getDelimiterKeyExpr(),
                                            this, pageContext)) != null)
            setDelimiterKey(string);
        if ((string = EvalHelper.evalString("openBracketKey", getOpenBracketKeyExpr(),
                                            this, pageContext)) != null)
            setOpenBracketKey(string);
        if ((string = EvalHelper.evalString("closeBracketKey", getCloseBracketKeyExpr(),
                                            this, pageContext)) != null)
            setCloseBracketKey(string);

        // Note that in contrast to other elements which have "style" and
        // "styleClass" attributes, this tag does not have a "styleId"
        // attribute.  This is because this produces the "id" attribute, which
        // has to be unique document-wide, but this tag can generate more than
        // one "option" element.  Thus, the base tag, "Options" does not
        // support this attribute.
    }
}

// end of ELOptionsTag.java