/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELMessagesTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Custom tag that iterates the elements of a message collection.
 * It defaults to retrieving the messages from <code>Action.ERROR_KEY</code>,
 * but if the message attribute is set to true then the messages will be
 * retrieved from <code>Action.MESSAGE_KEY</code>. This is an alternative
 * to the default <code>ErrorsTag</code>.
 * <p/>
 * This class is a subclass of the class
 * <code>org.apache.struts.taglib.html.MessagesTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author David M. Karr
 * @version $Revision: 1.2 $
 */

public class ELMessagesTag extends MessagesTag
{
    /**
     * Instance variable mapped to "id" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String idExpr;
    /**
     * Instance variable mapped to "bundle" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String bundleExpr;
    /**
     * Instance variable mapped to "locale" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String localeExpr;
    /**
     * Instance variable mapped to "name" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String nameExpr;
    /**
     * Instance variable mapped to "property" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String propertyExpr;
    /**
     * Instance variable mapped to "header" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String headerExpr;
    /**
     * Instance variable mapped to "footer" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String footerExpr;
    /**
     * Instance variable mapped to "message" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String messageExpr;

    private String styleClassExpr;

    /**
     * Getter method for "id" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getIdExpr()
    {
        return (idExpr);
    }

    /**
     * Getter method for "bundle" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getBundleExpr()
    {
        return (bundleExpr);
    }

    /**
     * Getter method for "locale" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getLocaleExpr()
    {
        return (localeExpr);
    }

    /**
     * Getter method for "name" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getNameExpr()
    {
        return (nameExpr);
    }

    /**
     * Getter method for "property" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getPropertyExpr()
    {
        return (propertyExpr);
    }

    /**
     * Getter method for "header" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getHeaderExpr()
    {
        return (headerExpr);
    }

    /**
     * Getter method for "footer" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getFooterExpr()
    {
        return (footerExpr);
    }

    /**
     * Getter method for "message" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public String getMessageExpr()
    {
        return (messageExpr);
    }

    public String getStyleClassExpr()
    {
        return (styleClassExpr);
    }

    public void setStyleClassExpr(String styleClassExpr)
    {
        this.styleClassExpr = styleClassExpr;
    }

    /**
     * Setter method for "id" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setIdExpr(String idExpr)
    {
        this.idExpr = idExpr;
    }

    /**
     * Setter method for "bundle" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setBundleExpr(String bundleExpr)
    {
        this.bundleExpr = bundleExpr;
    }

    /**
     * Setter method for "locale" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setLocaleExpr(String localeExpr)
    {
        this.localeExpr = localeExpr;
    }

    /**
     * Setter method for "name" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setNameExpr(String nameExpr)
    {
        this.nameExpr = nameExpr;
    }

    /**
     * Setter method for "property" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setPropertyExpr(String propertyExpr)
    {
        this.propertyExpr = propertyExpr;
    }

    /**
     * Setter method for "header" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setHeaderExpr(String headerExpr)
    {
        this.headerExpr = headerExpr;
    }

    /**
     * Setter method for "footer" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setFooterExpr(String footerExpr)
    {
        this.footerExpr = footerExpr;
    }

    /**
     * Setter method for "message" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    public void setMessageExpr(String messageExpr)
    {
        this.messageExpr = messageExpr;
    }

    /**
     * Resets attribute values for tag reuse.
     */
    public void release()
    {
        super.release();
        setIdExpr(null);
        setBundleExpr(null);
        setLocaleExpr(null);
        setNameExpr(null);
        setPropertyExpr(null);
        setHeaderExpr(null);
        setFooterExpr(null);
        setMessageExpr(null);
        setStyleClassExpr(null);
    }

    /**
     * Process the start tag.
     *
     * @throws javax.servlet.jsp.JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException
    {
        evaluateExpressions();
        return (super.doStartTag());
    }

    /**
     * Processes all attribute values which use the JSTL expression evaluation
     * engine to determine their values.
     *
     * @throws javax.servlet.jsp.JspException if a JSP exception has occurred
     */
    private void evaluateExpressions() throws JspException
    {
        String string = null;

        if ((string = EvalHelper.evalString("id", getIdExpr(),
                                            this, pageContext)) != null)
            setId(string);

        if ((string = EvalHelper.evalString("bundle", getBundleExpr(),
                                            this, pageContext)) != null)
            setBundle(string);

        if ((string = EvalHelper.evalString("locale", getLocaleExpr(),
                                            this, pageContext)) != null)
            setLocale(string);

        if ((string = EvalHelper.evalString("name", getNameExpr(),
                                            this, pageContext)) != null)
            setName(string);

        if ((string = EvalHelper.evalString("property", getPropertyExpr(),
                                            this, pageContext)) != null)
            setProperty(string);

        if ((string = EvalHelper.evalString("header", getHeaderExpr(),
                                            this, pageContext)) != null)
            setHeader(string);

        if ((string = EvalHelper.evalString("footer", getFooterExpr(),
                                            this, pageContext)) != null)
            setFooter(string);

        if ((string = EvalHelper.evalString("message", getMessageExpr(),
                                            this, pageContext)) != null)
            setMessage(string);

        if ((string = EvalHelper.evalString("styleClass", getStyleClassExpr(),
                                            this, pageContext)) != null)
            setStyleClass(string);
    }
}

// end of ELMessagesTag.java