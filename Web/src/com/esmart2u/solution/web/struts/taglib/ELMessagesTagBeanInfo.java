/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELMessagesTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;
import java.beans.SimpleBeanInfo;

/**
 * This is the <code>BeanInfo</code> descriptor for the
 * <code>org.apache.strutsel.taglib.html.ELMessagesTag</code> class.  It is
 * needed to override the default mapping of custom tag attribute names to
 * class attribute names.
 *
 * @author Goh Siew Chyn
 * @version $Id: ELMessagesTagBeanInfo.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELMessagesTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("id", ELMessagesTag.class,
                                                null, "setIdExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("bundle", ELMessagesTag.class,
                                                null, "setBundleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("locale", ELMessagesTag.class,
                                                null, "setLocaleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("name", ELMessagesTag.class,
                                                null, "setNameExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("property", ELMessagesTag.class,
                                                null, "setPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("header", ELMessagesTag.class,
                                                null, "setHeaderExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("footer", ELMessagesTag.class,
                                                null, "setFooterExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("message", ELMessagesTag.class,
                                                null, "setMessageExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("styleClass", ELMessagesTag.class,
                                                null, "setStyleClassExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELMessagesTagBeanInfo.java