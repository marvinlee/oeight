/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELIndexNoTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.SimpleBeanInfo;
import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;

/**
 * Bean info for Index No.
 *
 * @author Law Tat Yoong.
 * @version $Id: ELIndexNoTagBeanInfo.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELIndexNoTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("index", ELIndexNoTag.class,
                                                null, "setIndexExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("currentPage", ELIndexNoTag.class,
                                                null, "setCurrentPageExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("rowsPerPage", ELIndexNoTag.class,
                                                null, "setRowsPerPageExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELIndexNoTagBeanInfo.java