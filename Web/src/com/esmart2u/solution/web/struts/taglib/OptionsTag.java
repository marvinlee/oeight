/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// OptionsTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Iterator;
import javax.servlet.jsp.JspException;

import org.apache.struts.util.ResponseUtils;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.taglib.html.SelectTag;
import org.apache.struts.taglib.html.Constants;

import com.esmart2u.solution.base.helper.BaseConstants;
import com.esmart2u.solution.base.service.ConfigurationManager;
import com.esmart2u.solution.base.vo.OptionVO;

/**
 * Extends form org.apache.struts.taglib.html.OptionsTag
 * to support isShowDefault when in add mode will show Item which isDefault = "Y"
 *
 * @author Siripong Visasmongkolchai
 * @version $Id: OptionsTag.java,v 1.11 2004/04/02 10:58:23 wkchee Exp $
 */

public class OptionsTag extends org.apache.struts.taglib.html.OptionsTag
{
    private String delimiter;
    private String openBracket;
    private String closeBracket;

    private String delimiterKey;
    private String openBracketKey;
    private String closeBracketKey;

    /**
     * The flag identify if selectd value is empty String. It show optionVO which isDefault equals "Y"
     * option, if any.
     */
    protected boolean isShowDefault = false;

    /**
     * The flag identify to show blank item.
     */
    protected boolean isShowBlank = false;

    /**
     * The flag identify if selectd value is empty String. It show optionVO which isDefault equals "Y"
     * option, if any.
     */
    protected boolean isShowCode = false;

    /**
     * The flag identify if selectd value is empty String. It show optionVO which isDefault equals "Y"
     * option, if any.
     */
    protected boolean isShowAlternateLabel = false;

    public OptionsTag()
    {
        property = "value";
        labelProperty = "label";
        delimiter = ConfigurationManager.getKey("DROPLIST_DELIMITER");
        openBracket = ConfigurationManager.getKey("DROPLIST_ALT_DESC_OPEN_BRACKET");
        closeBracket = ConfigurationManager.getKey("DROPLIST_ALT_DESC_CLOSE_BRACKET");
        isShowCode = new Boolean(ConfigurationManager.getKey("DROPLIST_SHOW_CODE")).booleanValue();
        isShowAlternateLabel = new Boolean(ConfigurationManager.getKey("DROPLIST_SHOW_ALTERNATE_LABEL")).booleanValue();
    }

    public boolean getIsShowDefault()
    {
        return isShowDefault;
    }

    public void setIsShowDefault(boolean isShowDefault)
    {
        this.isShowDefault = isShowDefault;
    }

    public boolean getIsShowBlank()
    {
        return isShowBlank;
    }

    public void setIsShowBlank(boolean isShowBlank)
    {
        this.isShowBlank = isShowBlank;
    }

    public boolean getIsShowCode()
    {
        return isShowCode;
    }

    public void setIsShowCode(boolean isShowCode)
    {
        this.isShowCode = isShowCode;
    }

    public boolean getIsShowAlternateLabel()
    {
        return isShowAlternateLabel;
    }

    public void setIsShowAlternateLabel(boolean isShowAlternateLabel)
    {
        this.isShowAlternateLabel = isShowAlternateLabel;
    }

    public String getDelimiterKey()
    {
        return delimiterKey;
    }

    public void setDelimiterKey(String delimiterKey)
    {
        this.delimiterKey = delimiterKey;
    }

    public String getOpenBracketKey()
    {
        return openBracketKey;
    }

    public void setOpenBracketKey(String openBracketKey)
    {
        this.openBracketKey = openBracketKey;
    }

    public String getCloseBracketKey()
    {
        return closeBracketKey;
    }

    public void setCloseBracketKey(String closeBracketKey)
    {
        this.closeBracketKey = closeBracketKey;
    }

    /**
     * Process the end of this tag.
     *
     * @throws JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException
    {
        if (delimiterKey != null)
            delimiter = RequestUtils.message(pageContext, null, null, delimiterKey);

        // if there is no matching from the delimiter key. Get the default value.
        if (delimiter == null || delimiter.equals(""))
            delimiter = ConfigurationManager.getKey("DROPLIST_DELIMITER");

        if (openBracketKey != null)
            openBracket = RequestUtils.message(pageContext, null, null, openBracketKey);

        // if there is no matching from the open bracket key. Get the default value.
        if (openBracket == null || openBracket.equals(""))
            openBracket = ConfigurationManager.getKey("DROPLIST_ALT_DESC_OPEN_BRACKET");

        if (closeBracketKey != null)
            closeBracket = RequestUtils.message(pageContext, null, null, closeBracketKey);

        // if there is no matching from the close bracket key. Get the default value.
        if (closeBracket == null || closeBracket.equals(""))
            closeBracket = ConfigurationManager.getKey("DROPLIST_ALT_DESC_CLOSE_BRACKET");

        // Acquire the select tag we are associated with
        SelectTag selectTag = (SelectTag)pageContext.getAttribute(Constants.SELECT_KEY);
        if (selectTag == null)
        {
            throw new JspException(messages.getMessage("optionsTag.select"));
        }
        StringBuffer sb = new StringBuffer();

        // If a collection was specified, use that mode to render options
        if (collection != null)
        {
            if (isShowBlank)
            {
                sb.append("<option value=\"\"></option>");
            }

            Iterator collIterator = getIterator(collection, null);
            while (collIterator.hasNext())
            {
                OptionVO optionVO = (OptionVO)collIterator.next();
                String value = optionVO.getValue();
                String label = optionVO.getLabel();
                String alternateLabel = optionVO.getAlternateLabel();

                boolean match = false;

                if (selectTag.isMatched(value))
                {
                    match = true;
                }
                else if (optionVO.getDefaultFlag() == null || optionVO.getDefaultFlag().equalsIgnoreCase(BaseConstants.BOOLEAN_TYPE_NO))
                {
                    match = false;
                }
                else if (isShowDefault)
                {
                    match = true;
                }

                addOption(sb, value, label, alternateLabel, match);
            }
        }

        // Otherwise, use the separate iterators mode to render options
        else
        {
            // Construct iterators for the values and labels collections
            Iterator valuesIterator = getIterator(name, property);
            Iterator labelsIterator = null;
            if ((labelName == null) && (labelProperty == null))
            {
                labelsIterator = getIterator(name, property); // Same coll.
            }
            else
            {
                labelsIterator = getIterator(labelName, labelProperty);
            }

            // Render the options tags for each element of the values coll.
            while (valuesIterator.hasNext())
            {
                Object valueObject = valuesIterator.next();
                if (valueObject == null)
                {
                    valueObject = "";
                }
                String value = valueObject.toString();
                String label = value;
                if (labelsIterator.hasNext())
                {
                    Object labelObject = labelsIterator.next();
                    if (labelObject == null)
                    {
                        labelObject = "";
                    }
                    label = labelObject.toString();
                }
                addOption(sb, value, label, selectTag.isMatched(value));
            }
        }

        // Render this element to our writer
        ResponseUtils.write(pageContext, sb.toString());

        // Evaluate the remainder of this page
        return EVAL_PAGE;
    }

    /**
     * Release any acquired resources.
     */
    public void release()
    {
        super.release();
        isShowDefault = false;
    }

    /**
     * Add an option element to the specified StringBuffer based on the
     * specified parameters.
     * <p/>
     * Note that this tag specifically does not support the
     * <code>styleId</code> tag attribute, which causes the HTML
     * <code>id</code> attribute to be emitted.  This is because the HTML
     * specification states that all "id" attributes in a document have to be
     * unique.  This tag will likely generate more than one <code>option</code>
     * element element, but it cannot use the same <code>id</code> value.  It's
     * conceivable some sort of mechanism to supply an array of <code>id</code>
     * values could be devised, but that doesn't seem to be worth the trouble.
     *
     * @param sb      StringBuffer accumulating our results
     * @param value   Value to be returned to the server for this option
     * @param label   Value to be shown to the user for this option
     * @param matched Should this value be marked as selected?
     */
    protected void addOption(StringBuffer sb, String value, String label, String alternateLabel, boolean matched)
    {
        if (value == null) value = "";
        if (label == null) label = "";
        if (alternateLabel == null) alternateLabel = "";

        sb.append("<option value=\"");
        sb.append(value);
        sb.append("\"");
        if (matched)
        {
            sb.append(" selected=\"selected\"");
        }
        if (getStyle() != null)
        {
            sb.append(" style=\"");
            sb.append(getStyle());
            sb.append("\"");
        }
        if (getStyleClass() != null)
        {
            sb.append(" class=\"");
            sb.append(getStyleClass());
            sb.append("\"");
        }
        sb.append(">");

        // value
        if (isShowCode)
        {
            sb.append(value);
        }
        if (isShowCode && !label.equals(""))
        {
            sb.append(" ");
            sb.append(delimiter);
        }

        // label
        if (!label.equals(""))
        {
            if (getFilter())
            {
                sb.append(" ");
                sb.append(ResponseUtils.filter(label));
            }
            else
            {
                sb.append(" ");
                sb.append(label);
            }
        }

        // alternate label
        if (isShowAlternateLabel && !alternateLabel.equals(""))
        {
            sb.append(" " + openBracket);
            if (getFilter())
            {
                sb.append(ResponseUtils.filter(alternateLabel));
            }
            else
            {
                sb.append(alternateLabel);
            }
            sb.append(closeBracket);
        }

        sb.append("</option>\r\n");
    }
}

// end of OptionsTag.java