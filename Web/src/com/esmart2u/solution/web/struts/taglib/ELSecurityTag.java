/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELSecurityTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Custom tag for paging information.
 * <p/>
 * This class is a subclass of the class
 * <code>PagingInfoTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author Goh Siew Chyn
 * @version $Id: ELSecurityTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELSecurityTag extends SecurityTag
{
    private String screenCodeExpr = null;

    public String getScreenCodeExpr()
    {
        return screenCodeExpr;
    }

    public void setScreenCodeExpr(String screenCodeExpr)
    {
        this.screenCodeExpr = screenCodeExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        String screenCode;

        if ((screenCode = EvalHelper.evalString("screenCode", getScreenCodeExpr(), this, pageContext)) != null)
            setScreenCode(screenCode);
    }
}

// end of ELSecurityTag.java