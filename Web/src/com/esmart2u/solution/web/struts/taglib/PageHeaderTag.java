/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// PageHeaderTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.struts.taglib.html.BaseHandlerTag;
import org.apache.struts.util.RequestUtils;

/**
 * Render page header information
 *
 * @author Goh Siew Chyn
 * @version $Id: PageHeaderTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class PageHeaderTag extends BaseHandlerTag
{
    private final static String QUOTE = "\"";
    private String key = null;
    private String styleClass = null;

    public PageHeaderTag()
    {
        key = null;
        styleClass = null;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getStyleClass()
    {
        return styleClass;
    }

    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    /**
     * Start of Tag Processing
     */
    public int doStartTag() throws JspException
    {
        return (EVAL_BODY_BUFFERED);
    }

    /**
     * End of Tag Processing
     *
     * @throws javax.servlet.jsp.JspException if a JSP exception occurs
     */
    public int doEndTag() throws JspException
    {
        StringBuffer buffer = new StringBuffer();

        buffer.append("<div");
        prepareAttributes(buffer);
        buffer.append(">");

        String message = RequestUtils.message(pageContext, null, null, key);

        if (message != null)
        {
            buffer.append(message);
        }

        if (bodyContent != null)
            buffer.append(bodyContent.getString().trim());

        buffer.append("</div>");
        prepareStyles();
        JspWriter writer = pageContext.getOut();
        try
        {
            writer.print(buffer.toString());
        }
        catch (IOException e)
        {
            throw new JspException("Exception in PagingInfoTag doEndTag():" + e.toString());
        }

        return EVAL_PAGE;
    }

    public void prepareAttributes(StringBuffer buffer)
    {
        buffer.append(prepareAttribute("class", styleClass));
    }

    /**
     * Format attribute="value" from the specified attribute & value
     */
    public String prepareAttribute(String attribute, String value)
    {
        return value == null ? "" : " " + attribute + "=" + QUOTE + value + QUOTE;
    }

    public void release()
    {
        super.release();
        key = null;
        styleClass = null;
    }
}

// end of PageHeaderTag.java