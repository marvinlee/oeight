/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELNumericSymbolTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Locale;
import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

import com.esmart2u.solution.base.logging.Logger;

/**
 * This is EL implementation for <code>NumericSymbolTag</code> class.
 * <p/>
 * <p/>
 * <i>16 Dec 2003 v1.2 danielang</i>
 * <p/>
 * Removed country code and language code following implementation of retrieving Locale information directly
 * from session
 * <p/>
 *
 * @author Daniel Ang
 * @version $Id: ELNumericSymbolTag.java,v 1.3 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELNumericSymbolTag extends NumericSymbolTag
{
    private static Logger logger = Logger.getLogger(ELNumericSymbolTag.class);

    private String localeExpr;
    private String typeExpr;

    public String getLocaleExpr()
    {
        return localeExpr;
    }

    public void setLocaleExpr(String localeExpr)
    {
        this.localeExpr = localeExpr;
    }

    public String getTypeExpr()
    {
        return typeExpr;
    }

    public void setTypeExpr(String typeExpr)
    {
        this.typeExpr = typeExpr;
    }

    /**
     * Start of Tag Processing
     */
    public int doStartTag() throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    /**
     * Resets attribute values for tag reuse.
     */
    public void release()
    {
        super.release();
        setLocaleExpr(null);
        setTypeExpr(null);
    }

    /**
     * Evaluates the expressions
     *
     * @throws JspException
     */
    private void evaluateExpressions()
        throws JspException
    {
        Object object = null;

        try
        {
            if ((object = EvalHelper.eval("locale", getLocaleExpr(), this, pageContext)) != null)
                setLocale((Locale)object);
            if ((object = EvalHelper.eval("type", getTypeExpr(), this, pageContext)) != null)
                setType(object.toString());
        }
        catch (ClassCastException cce)
        {
            logger.error("Ensure that LOCALE passed in is of Locale object.");
            logger.error("ClassCastException encountered in ELNumericSymbolTag.", cce);
        }
    }
}

// end of ELNumericSymbolTag.java