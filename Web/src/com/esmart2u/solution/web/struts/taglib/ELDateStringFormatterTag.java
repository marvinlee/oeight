/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELDateStringFormatterTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Locale;
import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Formatter for date string.
 *
 * @author Chee Weng Keong
 * @version $Id: ELDateStringFormatterTag.java,v 1.1 2004/03/03 03:53:26 wkchee Exp $
 */

public class ELDateStringFormatterTag extends DateStringFormatterTag
{
    private String nameExpr;
    private String dayPropertyExpr;
    private String monthPropertyExpr;
    private String yearPropertyExpr;
    private String patternExpr;
    private String localeExpr;

    public String getNameExpr()
    {
        return nameExpr;
    }

    public void setNameExpr(String nameExpr)
    {
        this.nameExpr = nameExpr;
    }

    public String getDayPropertyExpr()
    {
        return dayPropertyExpr;
    }

    public void setDayPropertyExpr(String dayPropertyExpr)
    {
        this.dayPropertyExpr = dayPropertyExpr;
    }

    public String getMonthPropertyExpr()
    {
        return monthPropertyExpr;
    }

    public void setMonthPropertyExpr(String monthPropertyExpr)
    {
        this.monthPropertyExpr = monthPropertyExpr;
    }

    public String getYearPropertyExpr()
    {
        return yearPropertyExpr;
    }

    public void setYearPropertyExpr(String yearPropertyExpr)
    {
        this.yearPropertyExpr = yearPropertyExpr;
    }

    public String getPatternExpr()
    {
        return patternExpr;
    }

    public void setPatternExpr(String patternExpr)
    {
        this.patternExpr = patternExpr;
    }

    public String getLocaleExpr()
    {
        return localeExpr;
    }

    public void setLocaleExpr(String localeExpr)
    {
        this.localeExpr = localeExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        String string = null;
        Object object = null;

        if ((string = EvalHelper.evalString("name", getNameExpr(), this, pageContext)) != null)
            setName(string);
        if ((string = EvalHelper.evalString("dayProperty", getDayPropertyExpr(), this, pageContext)) != null)
            setDayProperty(string);
        if ((string = EvalHelper.evalString("monthProperty", getMonthPropertyExpr(), this, pageContext)) != null)
            setMonthProperty(string);
        if ((string = EvalHelper.evalString("yearProperty", getYearPropertyExpr(), this, pageContext)) != null)
            setYearProperty(string);
        if ((string = EvalHelper.evalString("pattern", getPatternExpr(), this, pageContext)) != null)
            setPattern(string);
        if ((object = EvalHelper.eval("locale", getLocaleExpr(), this, pageContext)) != null)
            setLocale((Locale)object);
    }
}

// end of ELDateStringFormatterTag.java