/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// MessagesTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Iterator;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.taglib.html.Constants;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.util.ResponseUtils;

/**
 * Custom tag that iterates the elements of a message collection.
 * It defaults to retrieving the messages from <code>Globals.ERROR_KEY</code>,
 * but if the message attribute is set to true then the messages will be
 * retrieved from <code>Globals.MESSAGE_KEY</code>. This is an alternative
 * to the default <code>ErrorsTag</code>.
 *
 * @author David Winterfeldt
 * @version $Revision: 1.6 $ $Date: 2004/03/25 07:00:13 $
 */

public class MessagesTag extends BodyTagSupport
{
    private final static String QUOTE = "\"";

    protected StringBuffer buffer = new StringBuffer(0);
    protected String styleClass = null;

    /**
     * The message resources for this package.
     */
    protected static MessageResources messageResources =
        MessageResources.getMessageResources(Constants.Package + ".LocalStrings");

    /**
     * Commons Logging instance.
     */
    private static Log log = LogFactory.getLog(MessagesTag.class);

    /**
     * Iterator of the elements of this error collection, while we are actually
     * running.
     */
    protected Iterator iterator = null;

    /**
     * Whether or not any error messages have been processed.
     */
    protected boolean processed = false;

    /**
     * The name of the scripting variable to be exposed.
     */
    protected String id = null;

    /**
     * The servlet context attribute key for our resources.
     */
    protected String bundle = null;

    /**
     * The session attribute key for our locale.
     */
    protected String locale = Globals.LOCALE_KEY;

    /**
     * The request attribute key for our error messages (if any).
     */
    protected String name = Globals.ERROR_KEY;

    /**
     * The name of the property for which error messages should be returned,
     * or <code>null</code> to return all errors.
     */
    protected String property = null;

    /**
     * The message resource key for errors header.
     */
    protected String header = null;

    /**
     * The message resource key for errors footer.
     */
    protected String footer = null;

    /**
     * If this is set to 'true', then the <code>Globals.MESSAGE_KEY</code> will
     * be used to retrieve the messages from scope.
     */
    protected String message = null;

    public String getStyleClass()
    {
        return (this.styleClass);
    }

    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    public String getId()
    {
        return (this.id);
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getBundle()
    {
        return (this.bundle);
    }

    public void setBundle(String bundle)
    {
        this.bundle = bundle;
    }

    public String getLocale()
    {
        return (this.locale);
    }

    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    public String getName()
    {
        return (this.name);
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getProperty()
    {
        return (this.property);
    }

    public void setProperty(String property)
    {
        this.property = property;
    }

    public String getHeader()
    {
        return (this.header);
    }

    public void setHeader(String header)
    {
        this.header = header;
    }

    public String getFooter()
    {
        return (this.footer);
    }

    public void setFooter(String footer)
    {
        this.footer = footer;
    }

    public String getMessage()
    {
        return (this.message);
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * Construct an iterator for the specified collection, and begin
     * looping through the body once per element.
     *
     * @throws JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException
    {
        // Initialize for a new request.
        processed = false;

        // Were any messages specified?
        ActionMessages messages = null;

        // Make a local copy of the name attribute that we can modify.
        String name = this.name;

        if (message != null && "true".equalsIgnoreCase(message))
        {
            name = Globals.MESSAGE_KEY;
        }

        try
        {
            messages = RequestUtils.getActionMessages(pageContext, name);
        }
        catch (JspException e)
        {
            RequestUtils.saveException(pageContext, e);
            throw e;
        }

        // Acquire the collection we are going to iterate over
        this.iterator = (property == null) ? messages.get() : messages.get(property);

        // Store the first value and evaluate, or skip the body if none
        if (!this.iterator.hasNext())
        {
            return SKIP_BODY;
        }

        ActionMessage report = (ActionMessage)this.iterator.next();
        String msg =
            RequestUtils.message(pageContext,
                                 bundle,
                                 locale,
                                 report.getKey(),
                                 report.getValues());

        if (msg != null)
        {
            buffer.append("<table>");
            buffer.append("<tr>");
            buffer.append("<td");
            if (styleClass != null)
            {
                prepareAttributes(buffer);
            }
            buffer.append(">");

            if (msg != null)
            {
                buffer.append(msg);
                buffer.append("<br>");
            }
            pageContext.setAttribute(id, msg);
        }
        else
        {
            pageContext.removeAttribute(id);

            // log missing key to ease debugging
            if (log.isDebugEnabled())
            {
                log.debug(messageResources.getMessage("messageTag.resources",
                                                      report.getKey()));
            }
        }

        if (header != null && header.length() > 0)
        {
            String headerMessage =
                RequestUtils.message(pageContext, bundle, locale, header);

            if (headerMessage != null)
            {
                ResponseUtils.write(pageContext, headerMessage);
            }
        }

        // Set the processed variable to true so the
        // doEndTag() knows processing took place
        processed = true;

        return (EVAL_BODY_BUFFERED);
    }

    /**
     * Make the next collection element available and loop, or
     * finish the iterations if there are no more elements.
     *
     * @throws JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException
    {
        // Render the output from this iteration to the output stream
        if (bodyContent != null)
        {
            ResponseUtils.writePrevious(pageContext, bodyContent.getString());
            bodyContent.clearBody();
        }

        // Decide whether to iterate or quit
        if (iterator.hasNext())
        {
            ActionMessage report = (ActionMessage)iterator.next();
            String msg = RequestUtils.message(pageContext, bundle,
                                              locale, report.getKey(),
                                              report.getValues());

            if (msg != null)
            {
                buffer.append(msg);
                buffer.append("<br>");
            }

            pageContext.setAttribute(id, msg);

            return (EVAL_BODY_BUFFERED);
        }
        else
        {
            return (SKIP_BODY);
        }
    }

    /**
     * Clean up after processing this enumeration.
     *
     * @throws JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException
    {
        if (processed && footer != null && footer.length() > 0)
        {
            String footerMessage = RequestUtils.message(pageContext, bundle,
                                                        locale, footer);
            if (footerMessage != null)
            {
                // Print the results to our output writer
                ResponseUtils.write(pageContext, footerMessage);
            }
        }

        if (bodyContent != null)
        {
            buffer.append(bodyContent.getString().trim());
            buffer.append("</td>");
            buffer.append("</tr>");
            buffer.append("</table>");
            buffer.append("<br>");
        }

        JspWriter writer = pageContext.getOut();

        try
        {
            writer.print(buffer.toString());
        }
        catch (IOException e)
        {
            throw new JspException("Exception in PagingInfoTag doEndTag():" + e.toString());
        }

        // Continue processing this page
        return (EVAL_PAGE);
    }

    public void prepareAttributes(StringBuffer buffer)
    {
        buffer.append(prepareAttribute("class", styleClass));
    }

    public String prepareAttribute(String attribute, String value)
    {
        return value == null ? "" : " " + attribute + "=" + QUOTE + value + QUOTE;
    }

    /**
     * Release all allocated resources.
     */
    public void release()
    {
        super.release();
        iterator = null;
        processed = false;
        id = null;
        bundle = null;
        locale = Globals.LOCALE_KEY;
        name = Globals.ERROR_KEY;
        property = null;
        header = null;
        footer = null;
        message = null;
    }
}

// end of MessagesTag.java