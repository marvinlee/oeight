/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// Date.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Locale;
import javax.servlet.jsp.JspException;

import org.apache.struts.taglib.html.BaseInputTag;
import org.apache.struts.util.RequestUtils;
import org.apache.struts.util.ResponseUtils;

import com.esmart2u.solution.base.helper.DateUtils;
import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.logging.Logger;

/**
 * Displays day and month in selection and year in input text box.
 *
 * @author Goh Siew Chyn
 * @version $Id: DateTag.java,v 1.9 2004/06/15 10:46:42 wkchee Exp $
 */

public class DateTag extends BaseInputTag
{
    private static Logger logger = Logger.getLogger(DateTag.class);

    private String name;
    private String dayProperty;
    private String monthProperty;
    private String yearProperty;
    private String dayValue;
    private String monthValue;
    private String yearValue;
    private String onChange;
    private String onBlur;
    private boolean disabled;

    public DateTag()
    {
        name = null;
        dayProperty = null;
        monthProperty = null;
        yearProperty = null;
        dayValue = null;
        monthValue = null;
        yearValue = null;
        onChange = null;
        onBlur = null;
        disabled = false;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDayProperty(String dayProperty)
    {
        this.dayProperty = dayProperty;
    }

    public String getDayProperty()
    {
        return this.dayProperty;
    }

    public void setMonthProperty(String monthProperty)
    {
        this.monthProperty = monthProperty;
    }

    public String getMonthProperty()
    {
        return this.monthProperty;
    }

    public void setYearProperty(String yearProperty)
    {
        this.yearProperty = yearProperty;
    }

    public String getYearProperty()
    {
        return this.yearProperty;
    }

    public String getOnChange()
    {
        return onChange;
    }

    public void setOnChange(String onChange)
    {
        this.onChange = onChange;
    }

    public String getOnBlur()
    {
        return onBlur;
    }

    public void setOnBlur(String onBlur)
    {
        this.onBlur = onBlur;
    }

    public boolean getDisabled()
    {
        return disabled;
    }

    public void setDisabled(boolean disabled)
    {
        this.disabled = disabled;
    }

    public int doStartTag() throws JspException
    {
        String selectItem;
        String defaultItem;
        StringBuffer results = new StringBuffer();

        logger.debug("Date Properties:");
        logger.debug("  dayProperty = " + dayProperty);
        logger.debug("  monthProperty = " + monthProperty);
        logger.debug("  yearProperty = " + yearProperty);

        // checking for valid combination
        if (StringUtils.hasValue(dayProperty) && !StringUtils.hasValue(monthProperty) && !StringUtils.hasValue(yearProperty))
        {
            // pass for dd only
        }
        else if (!StringUtils.hasValue(dayProperty) && StringUtils.hasValue(monthProperty) && !StringUtils.hasValue(yearProperty))
        {
            // pass for mmm only
        }
        else if (!StringUtils.hasValue(dayProperty) && !StringUtils.hasValue(monthProperty) && StringUtils.hasValue(yearProperty))
        {
            // pass for yyyy only
        }
        else if (StringUtils.hasValue(dayProperty) && StringUtils.hasValue(monthProperty) && !StringUtils.hasValue(yearProperty))
        {
            // pass for dd-mmm only
        }
        else if (!StringUtils.hasValue(dayProperty) && StringUtils.hasValue(monthProperty) && StringUtils.hasValue(yearProperty))
        {
            // pass for mmm-yyyy only
        }
        else if (StringUtils.hasValue(dayProperty) && StringUtils.hasValue(monthProperty) && StringUtils.hasValue(yearProperty))
        {
            // pass for dd-mmm-yyyy
        }
        else
        {
            throw new JspException("Wrong combination of date properties!");
        }

        if (StringUtils.hasValue(dayProperty))
        {
            dayValue = (String)RequestUtils.lookup(pageContext, name, dayProperty, null);

            results.append("<select name=\"");
            if (indexed)
            {
                prepareIndex(results, name);
            }
            results.append(dayProperty);
            results.append("\"");
            if (onChange != null)
                results.append("onchange=\"" + onChange + "\" ");
            if (onBlur != null)
                results.append("onblur=\"" + onBlur + "\" ");
            if (disabled)
                results.append("disabled=\"" + disabled + "\" ");
            results.append(">");
            if (dayValue == null)
            {
                selectItem = null;
                defaultItem = "selected";
            }
            else
            {
                selectItem = "selected";
                defaultItem = "";
            }
            results.append("<option value=\"\" " + defaultItem + ">" + "" + "</option>");
            for (int i = 1; i <= 31; i++)
            {
                String temp;
                if (i < 10)
                    temp = "0" + i;
                else
                    temp = "" + i;
                results.append("<option value=\"" + temp + "\" ");
                String day = temp;
                if (day.equals(dayValue))
                    results.append("" + selectItem + "");
                results.append(">" + temp + "</option>");
            }
            results.append("</select>");
            results.append("&nbsp;&nbsp;");
        }

        if (StringUtils.hasValue(monthProperty))
        {
            monthValue = (String)RequestUtils.lookup(pageContext, name, monthProperty, null);

            results.append("<select name=\"");
            if (indexed)
            {
                prepareIndex(results, name);
            }
            results.append(monthProperty);
            results.append("\"");
            if (onChange != null)
                results.append("onchange=\"" + onChange + "\" ");
            if (onBlur != null)
                results.append("onblur=\"" + onBlur + "\" ");
            if (disabled)
                results.append("disabled=\"" + disabled + "\" ");
            results.append(">");
            if (monthValue == null)
            {
                selectItem = "";
                defaultItem = "selected";
            }
            else
            {
                selectItem = "selected";
                defaultItem = "";
            }
            Locale locale = (Locale)pageContext.getSession().getAttribute("org.apache.struts.action.LOCALE");
            String as[] = DateUtils.getShortMonths(locale);
            results.append("<option value=\"\" " + defaultItem + ">" + "" + "</option>");
            monthValue = monthValue != null ? monthValue.trim() : "";
            for (int j = 1; j <= 12; j++)
            {
                String s4 = getMonthString(j);
                results.append("<option value=\"" + s4 + "\"");
                if (s4.equals(monthValue))
                    results.append("" + selectItem + "");
                String s5 = as[j - 1];
                results.append(">" + s5 + "</option>");
            }
            results.append("</select>");
            results.append("&nbsp;&nbsp;");
        }

        if (StringUtils.hasValue(yearProperty))
        {
            yearValue = (String)RequestUtils.lookup(pageContext, name, yearProperty, null);

            // Create an appropriate "input" element based on our parameters
            results.append("<input type=\"text\" maxlength=\"4\" size=\"4\" ");
            results.append("text");
            results.append("\" name=\"");

            if (indexed)
            {
                prepareIndex(results, name);
            }
            results.append(yearProperty);
            results.append("\"");
            results.append(" value=\"");
            if (yearValue != null)
            {
                results.append(ResponseUtils.filter(yearValue));
            }
            results.append("\"");
            results.append(prepareStyles());
            if (onChange != null)
                results.append("onchange=\"" + onChange + "\" ");
            if (onBlur != null)
                results.append("onblur=\"" + onBlur + "\" ");
            if (disabled)
                results.append("disabled=\"" + disabled + "\" ");
            results.append(getElementClose());
            results.append("&nbsp;&nbsp;");
        }

        // print this field to our output writer
        ResponseUtils.write(pageContext, results.toString());

        return SKIP_BODY;
    }

    public int doEndTag() throws JspException
    {
        return EVAL_PAGE;
    }

    public void release()
    {
        super.release();
        dayProperty = null;
        monthProperty = null;
        yearProperty = null;
        dayValue = null;
        monthValue = null;
        yearValue = null;
        onChange = null;
        onBlur = null;
        disabled = false;
    }

    protected String getMonthString(int i)
    {
        String returnValue = new Integer(i).toString();
        if (returnValue.length() == 1)
        {
            returnValue = "0" + returnValue;
        }
        return returnValue;
    }
}

// end of Date.java