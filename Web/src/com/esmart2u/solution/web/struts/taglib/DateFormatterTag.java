/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DateFormatterTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Locale;
import java.sql.Date;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.Globals;

import com.esmart2u.solution.base.helper.Formatter;
import com.esmart2u.solution.base.helper.BaseConstants;

/**
 * This <code>DateFormatterTag</code> class is used to perform locale-sensitive formatting on date object.
 * This component expects a <code>java.sql.Date</code> object to be passed in as Value attribute and a
 * <code>locale</code> object. Alternatively, recognised country code and language code may be explicitly
 * defined.
 * <p/>
 * <p/>
 * <i>16 Dec 2003 v1.2 danielang</i>
 * <p/>
 * Removed country code and language code following implementation of retrieving Locale information directly
 * from session
 * <p/>
 *
 * @author Daniel Ang
 * @version $Id: DateFormatterTag.java,v 1.4 2004/06/15 10:46:42 wkchee Exp $
 */

public class DateFormatterTag extends TagSupport
{
    private Date value;
    private String pattern;
    private Locale locale;
    private boolean showTime;

    public Date getValue()
    {
        return value;
    }

    public void setValue(Date value)
    {
        this.value = value;
    }

    public String getPattern()
    {
        return pattern;
    }

    public void setPattern(String pattern)
    {
        this.pattern = pattern;
    }

    public Locale getLocale()
    {
        return locale;
    }

    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }

    public boolean isShowTime()
    {
        return showTime;
    }

    public void setShowTime(boolean showTime)
    {
        this.showTime = showTime;
    }

    /**
     * End of Tag Processing.
     *
     * @throws JspException if a JSP exception occurs.
     */
    public int doEndTag() throws JspException
    {
        String output;
        Locale locale;
        JspWriter writer;

        try
        {
            // if locale is not provided by caller, retrieve it from session
            if (getLocale() == null)
            {
                try
                {
                    locale = (Locale)pageContext.getSession().getAttribute(Globals.LOCALE_KEY);
                }
                catch (NullPointerException npe)
                {
                    locale = Locale.getDefault();
                }
            }
            else
            {
                locale = getLocale();
            }

            // check if pattern is specified before formatting value
            if (pattern == null && !showTime)
            {
                output = Formatter.formatDate(value, locale, BaseConstants.DATE_FORMAT);
            }
            else if (pattern == null && showTime)
            {
                output = Formatter.formatDate(value, locale, BaseConstants.DATE_TIME_FORMAT);
            }
            else
            {
                output = Formatter.formatDate(value, locale, pattern);
            }

            // write output to page
            writer = pageContext.getOut();
            writer.print(output);
        }
        catch (IOException e)
        {
            throw new JspException("Exception in DateFormatterTag doEndTag():" + e.toString());
        }

        return SKIP_BODY;
    }

    /**
     * Resets attribute values for tag reuse.
     */
    public void release()
    {
        super.release();
        setValue(null);
        setPattern(null);
        setLocale(null);
        setShowTime(false);
    }
}

// end of DateFormatterTag.java