/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// NumericSymbolTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Locale;
import java.io.IOException;
import java.text.DecimalFormatSymbols;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.Globals;

/**
 * This <code>NumericSymbolTag</code> class is used to determine locale-sensitive formatting symbol
 * for numeric data.
 * This component expects a <code>locale</code> object or recognised countryCode code and languageCode code.
 * <p/>
 * <ul>To request for services, the <code>Type</code> attribute must be specified. Among the formatting
 * symbol that can be requested and the respective keyword to be indicated as <code>Type</code> are:
 * <li>1) monetary decimal separator (type="monetaryDecimalSeparator")
 * <li>2) grouping separator (type="groupingSeparator")
 * </ul>
 * <p/>
 * <p/>
 * <i>16 Dec 2003 v1.2 danielang</i>
 * <p/>
 * Removed country code and language code following implementation of retrieving Locale information directly
 * from session
 * <p/>
 *
 * @author Daniel Ang
 * @version $Id: NumericSymbolTag.java,v 1.3 2004/01/16 03:13:30 wkchee Exp $
 */

public class NumericSymbolTag extends TagSupport
{
    private Locale locale;
    private String type;

    public Locale getLocale()
    {
        return locale;
    }

    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    /**
     * End of Tag Processing
     *
     * @throws JspException if a JSP exception occurs
     */
    public int doEndTag() throws JspException
    {
        String output = "";
        Locale locale;
        JspWriter writer;

        try
        {
            // if locale is not provided by caller, retrieve it from session
            if (getLocale() == null)
            {
                try
                {
                    locale = (Locale)pageContext.getSession().getAttribute(Globals.LOCALE_KEY);
                }
                catch (NullPointerException npe)
                {
                    locale = Locale.getDefault();
                }
            }
            else
            {
                locale = getLocale();
            }

            DecimalFormatSymbols dt = new DecimalFormatSymbols(locale);

            // obtain the expected output
            if (type.equals("groupingSeparator"))
            {
                output = new Character(dt.getGroupingSeparator()).toString();
            }
            else if (type.equals("monetaryDecimalSeparator"))
            {
                output = new Character(dt.getMonetaryDecimalSeparator()).toString();
            }

            // write output to page
            writer = pageContext.getOut();
            writer.print(output);

        }
        catch (IOException e)
        {
            throw new JspException("Exception in NumericSymbolTag doEndTag():" + e.toString());
        }

        return SKIP_BODY;
    }

    /**
     * Resets attribute values for tag reuse.
     */
    public void release()
    {
        super.release();
        setLocale(null);
        setType(null);
    }
}

// end of NumericSymbolTag.java