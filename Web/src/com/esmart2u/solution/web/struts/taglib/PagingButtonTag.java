/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// PagingButtonTag.java

package com.esmart2u.solution.web.struts.taglib;

import org.apache.struts.taglib.html.BaseHandlerTag;
import org.apache.struts.util.ResponseUtils;
import org.apache.struts.util.RequestUtils;

import javax.servlet.jsp.JspException;

/**
 * Displays paging button.
 *
 * @author Goh Siew Chyn
 * @version $Id: PagingButtonTag.java,v 1.3 2004/01/16 03:13:30 wkchee Exp $
 */

public class PagingButtonTag extends BaseHandlerTag
{
    private static String PREV = "common.button.previous";
    private static String NEXT = "common.button.next";
    private int currentPage;
    private int totalPage;
    private String onPrevClick;
    private String onNextClick;
    private String prevProperty;
    private String nextProperty;
    private String styleClass;

    public void PagingButton()
    {
        currentPage = 0;
        totalPage = 0;
        onPrevClick = null;
        onNextClick = null;
        prevProperty = null;
        nextProperty = null;
        styleClass = null;
    }

    public String getPrevProperty()
    {
        return prevProperty;
    }

    public void setPrevProperty(String prevProperty)
    {
        this.prevProperty = prevProperty;
    }

    public String getNextProperty()
    {
        return nextProperty;
    }

    public void setNextProperty(String nextProperty)
    {
        this.nextProperty = nextProperty;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
    }

    public int getTotalPage()
    {
        return totalPage;
    }

    public void setTotalPage(int totalPage)
    {
        this.totalPage = totalPage;
    }

    public String getOnPrevClick()
    {
        return onPrevClick;
    }

    public void setOnPrevClick(String onPrevClick)
    {
        this.onPrevClick = onPrevClick;
    }

    public String getOnNextClick()
    {
        return onNextClick;
    }

    public void setOnNextClick(String onNextClick)
    {
        this.onNextClick = onNextClick;
    }

    public String getStyleClass()
    {
        return styleClass;
    }

    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    public int doStartTag()
        throws JspException
    {
        return EVAL_BODY_BUFFERED;
    }

    public int doEndTag()
        throws JspException
    {
        StringBuffer results = new StringBuffer();

        String prevProperty;
        String nextProperty;

        String prevMessage = RequestUtils.message(pageContext, null, null, PREV);

        if (prevMessage != null)
        {
            prevProperty = prevMessage;
        }
        else
        {
            prevProperty = "<< Prev";
        }

        String nextMessage = RequestUtils.message(pageContext, null, null, NEXT);

        if (nextMessage != null)
        {
            nextProperty = nextMessage;
        }
        else
        {
            nextProperty = "Next >>";
        }

        // if current page more than 1 then display Previous Button
        if (currentPage > 1)
        {
            results.append("<input type=\"button\"");
            if (prevProperty != null)
            {
                results.append(" name=\"");
                results.append(getPrevProperty());
                if (indexed)
                    prepareIndex(results, null);
                results.append("\"");
            }
            if (styleClass != null)
            {
                results.append(" class=\"");
                results.append(styleClass);
                results.append("\"");
            }
            if (accesskey != null)
            {
                results.append(" accesskey=\"");
                results.append(accesskey);
                results.append("\"");
            }
            if (tabindex != null)
            {
                results.append(" tabindex=\"");
                results.append(tabindex);
                results.append("\"");
            }
            if (onPrevClick != null)
            {
                results.append(" onClick=\"");
                results.append(onPrevClick);
                results.append("\"");
            }
            results.append(" value=\"");
            results.append(prevProperty);
            results.append("\"");
            results.append(prepareEventHandlers());
            results.append(prepareStyles());
            results.append(getElementClose());
        }

        if ((currentPage > 0) && (currentPage < totalPage))
        {
            results.append("<input type=\"button\"");
            if (prevProperty != null)
            {
                results.append(" name=\"");
                results.append(getNextProperty());
                if (indexed)
                    prepareIndex(results, null);
                results.append("\"");
            }
            if (styleClass != null)
            {
                results.append(" class=\"");
                results.append(styleClass);
                results.append("\"");
            }
            if (accesskey != null)
            {
                results.append(" accesskey=\"");
                results.append(accesskey);
                results.append("\"");
            }
            if (tabindex != null)
            {
                results.append(" tabindex=\"");
                results.append(tabindex);
                results.append("\"");
            }
            if (onNextClick != null)
            {
                results.append(" onClick=\"");
                results.append(onNextClick);
                results.append("\"");
            }
            results.append(" value=\"");
            results.append(nextProperty);
            results.append("\"");
            results.append(prepareEventHandlers());
            results.append(prepareStyles());
            results.append(getElementClose());
        }
        ResponseUtils.write(pageContext, results.toString());
        return EVAL_PAGE;
    }

    public void release()
    {
        super.release();
        currentPage = 0;
        totalPage = 0;
        onPrevClick = null;
        onNextClick = null;
        prevProperty = null;
        nextProperty = null;
        styleClass = null;
    }
}

// end of PagingButtonTag.java