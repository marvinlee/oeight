/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DateStringFormatterTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Locale;
import java.util.Calendar;
import java.io.IOException;
import java.sql.Date;
import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.struts.Globals;
import org.apache.struts.util.RequestUtils;

import com.esmart2u.solution.base.helper.*;
import com.esmart2u.solution.base.logging.Logger;

/**
 * Formatter for date string.
 *
 * @author Chee Weng Keong
 * @version $Id: DateStringFormatterTag.java,v 1.3 2004/06/15 10:46:42 wkchee Exp $
 */

public class DateStringFormatterTag extends TagSupport
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(DateStringFormatterTag.class);

    private String name;
    private String dayProperty;
    private String monthProperty;
    private String yearProperty;
    private String dayValue;
    private String monthValue;
    private String yearValue;
    private String pattern;
    private Locale locale;

    public DateStringFormatterTag()
    {
        name = null;
        dayProperty = null;
        monthProperty = null;
        yearProperty = null;
        dayValue = null;
        monthValue = null;
        yearValue = null;
        pattern = null;
        locale = null;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDayProperty()
    {
        return dayProperty;
    }

    public void setDayProperty(String dayProperty)
    {
        this.dayProperty = dayProperty;
    }

    public String getMonthProperty()
    {
        return monthProperty;
    }

    public void setMonthProperty(String monthProperty)
    {
        this.monthProperty = monthProperty;
    }

    public String getYearProperty()
    {
        return yearProperty;
    }

    public void setYearProperty(String yearProperty)
    {
        this.yearProperty = yearProperty;
    }

    public String getDayValue()
    {
        return dayValue;
    }

    public void setDayValue(String dayValue)
    {
        this.dayValue = dayValue;
    }

    public String getMonthValue()
    {
        return monthValue;
    }

    public void setMonthValue(String monthValue)
    {
        this.monthValue = monthValue;
    }

    public String getYearValue()
    {
        return yearValue;
    }

    public void setYearValue(String yearValue)
    {
        this.yearValue = yearValue;
    }

    public String getPattern()
    {
        return pattern;
    }

    public void setPattern(String pattern)
    {
        this.pattern = pattern;
    }

    public Locale getLocale()
    {
        return locale;
    }

    public void setLocale(Locale locale)
    {
        this.locale = locale;
    }

    /**
     * End of Tag Processing.
     *
     * @throws JspException if a JSP exception occurs.
     */
    public int doEndTag() throws JspException
    {
        String output;
        Locale locale;
        JspWriter writer;

        try
        {
            // if locale is not provided by caller, retrieve it from session
            if (getLocale() == null)
            {
                try
                {
                    locale = (Locale)pageContext.getSession().getAttribute(Globals.LOCALE_KEY);
                }
                catch (NullPointerException npe)
                {
                    locale = Locale.getDefault();
                }
            }
            else
            {
                locale = getLocale();
            }

            logger.debug("Date Properties:");
            logger.debug("  dayProperty = " + dayProperty);
            logger.debug("  monthProperty = " + monthProperty);
            logger.debug("  yearProperty = " + yearProperty);

            // choose pattern
            if (!StringUtils.hasValue(pattern))
            {
                // checking for valid combination
                if (StringUtils.hasValue(dayProperty) && !StringUtils.hasValue(monthProperty) && !StringUtils.hasValue(yearProperty))
                {
                    pattern = BaseConstants.DATE_FORMAT_DD;
                }
                else if (!StringUtils.hasValue(dayProperty) && StringUtils.hasValue(monthProperty) && !StringUtils.hasValue(yearProperty))
                {
                    pattern = BaseConstants.DATE_FORMAT_MM;
                }
                else if (!StringUtils.hasValue(dayProperty) && !StringUtils.hasValue(monthProperty) && StringUtils.hasValue(yearProperty))
                {
                    pattern = BaseConstants.DATE_FORMAT_YYYY;
                }
                else if (StringUtils.hasValue(dayProperty) && StringUtils.hasValue(monthProperty) && !StringUtils.hasValue(yearProperty))
                {
                    pattern = BaseConstants.DATE_FORMAT_DD_MM;
                }
                else if (!StringUtils.hasValue(dayProperty) && StringUtils.hasValue(monthProperty) && StringUtils.hasValue(yearProperty))
                {
                    pattern = BaseConstants.DATE_FORMAT_MM_YYYY;
                }
                else if (StringUtils.hasValue(dayProperty) && StringUtils.hasValue(monthProperty) && StringUtils.hasValue(yearProperty))
                {
                    pattern = BaseConstants.DATE_FORMAT;
                }
                else
                {
                    throw new JspException("Wrong combination of date properties!");
                }
            }

            // extract value
            if (StringUtils.hasValue(dayProperty))
            {
                dayValue = (String)RequestUtils.lookup(pageContext, name, dayProperty, null);
            }
            if (StringUtils.hasValue(monthProperty))
            {
                monthValue = (String)RequestUtils.lookup(pageContext, name, monthProperty, null);
            }
            if (StringUtils.hasValue(yearProperty))
            {
                yearValue = (String)RequestUtils.lookup(pageContext, name, yearProperty, null);
            }

            // preparing date object
            Date date = null;
            if (locale.getLanguage().equals(LocaleConstants.THAILAND.getLanguage()))
            {
                com.ibm.icu.util.Calendar calendar = new com.ibm.icu.util.BuddhistCalendar();
                if (StringUtils.hasValue(dayProperty) && StringUtils.hasValue(dayValue))
                {
                    calendar.set(com.ibm.icu.util.Calendar.DAY_OF_MONTH, Integer.parseInt(dayValue));
                }
                if (StringUtils.hasValue(monthProperty) && StringUtils.hasValue(monthValue))
                {
                    calendar.set(com.ibm.icu.util.Calendar.MONTH, Integer.parseInt(monthValue) - 1);
                }
                if (StringUtils.hasValue(yearProperty) && StringUtils.hasValue(yearValue))
                {
                    calendar.set(com.ibm.icu.util.Calendar.YEAR, Integer.parseInt(yearValue));
                }
                date = new Date(calendar.getTime().getTime());
            }
            else
            {
                Calendar calendar = Calendar.getInstance(locale);
                if (StringUtils.hasValue(dayProperty) && StringUtils.hasValue(dayValue))
                {
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayValue));
                }
                if (StringUtils.hasValue(monthProperty) && StringUtils.hasValue(monthValue))
                {
                    calendar.set(Calendar.MONTH, Integer.parseInt(monthValue) - 1);
                }
                if (StringUtils.hasValue(yearProperty) && StringUtils.hasValue(yearValue))
                {
                    calendar.set(Calendar.YEAR, Integer.parseInt(yearValue));
                }
                date = new Date(calendar.getTime().getTime());
            }

            // to setup formatter using calendar and locale
            com.ibm.icu.text.SimpleDateFormat formatter =
                new com.ibm.icu.text.SimpleDateFormat(pattern, locale);

            // special handling of locale
            if (locale.getLanguage().equals(LocaleConstants.THAILAND.getLanguage()))
            {
                // only add for ICU, can be removed when upgrade to JDK 1.4.1
                formatter.setCalendar(new com.ibm.icu.util.BuddhistCalendar());
            }

            // display blank when no date value is given
            if (!StringUtils.hasValue(dayValue) && !StringUtils.hasValue(monthValue) && !StringUtils.hasValue(yearValue))
            {
                output = "";
            }
            else
            {
                output = formatter.format(date);
            }

            // write output to page
            writer = pageContext.getOut();
            writer.print(output);
        }
        catch (IOException e)
        {
            throw new JspException("Exception in DateStringFormatterTag doEndTag():" + e.toString());
        }

        return SKIP_BODY;
    }

    /**
     * Resets attribute values for tag reuse.
     */
    public void release()
    {
        super.release();
        dayProperty = null;
        monthProperty = null;
        yearProperty = null;
        dayValue = null;
        monthValue = null;
        yearValue = null;
        pattern = null;
        locale = null;
    }
}

// end of DateStringFormatterTag.java