/*
 * FormTag.java
 *
 * Created on September 24, 2007, 11:21 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.web.struts.taglib;

/**
 *
 * @author meauchyuan.lee
 */

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.TagSupport;
import org.apache.struts.action.*;
import org.apache.struts.config.FormBeanConfig;
import org.apache.struts.config.ModuleConfig;
import org.apache.struts.util.*;

public class FormTag extends TagSupport
{

    public FormTag()
    {
        action = null;
        moduleConfig = null;
        enctype = null;
        focus = null;
        focusIndex = null;
        mapping = null;
        method = null;
        name = null;
        onreset = null;
        onsubmit = null;
        scope = null;
        servlet = null;
        style = null;
        styleClass = null;
        styleId = null;
        target = null;
        type = null;
        beanName = null;
        beanScope = null;
        beanType = null;
        isRelative = null;
    }

    public String getBeanName()
    {
        return beanName;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getEnctype()
    {
        return enctype;
    }

    public void setEnctype(String enctype)
    {
        this.enctype = enctype;
    }

    public String getFocus()
    {
        return focus;
    }

    public void setFocus(String focus)
    {
        this.focus = focus;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getOnreset()
    {
        return onreset;
    }

    public void setOnreset(String onReset)
    {
        onreset = onReset;
    }

    public String getOnsubmit()
    {
        return onsubmit;
    }

    public void setOnsubmit(String onSubmit)
    {
        onsubmit = onSubmit;
    }

    public String getScope()
    {
        return scope;
    }

    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public String getStyle()
    {
        return style;
    }

    public void setStyle(String style)
    {
        this.style = style;
    }

    public String getStyleClass()
    {
        return styleClass;
    }

    public void setStyleClass(String styleClass)
    {
        this.styleClass = styleClass;
    }

    public String getStyleId()
    {
        return styleId;
    }

    public void setStyleId(String styleId)
    {
        this.styleId = styleId;
    }

    public String getTarget()
    {
        return target;
    }

    public void setTarget(String target)
    {
        this.target = target;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
    
    public String getIsRelative()
    {
        return isRelative;
    }

    public void setIsRelative(String isRelative)
    {
        this.isRelative = isRelative;
    }

    public int doStartTag()
        throws JspException
    {
        lookup();
        StringBuffer results = new StringBuffer();
        results.append(renderFormStartElement());
        results.append(renderToken());
        ResponseUtils.write(pageContext, results.toString());
        pageContext.setAttribute("org.apache.struts.taglib.html.FORM", this, 2);
        initFormBean();
        return 1;
    }

    protected void initFormBean()
        throws JspException
    {
        int scope = 3;
        if("request".equalsIgnoreCase(beanScope))
            scope = 2;
        Object bean = pageContext.getAttribute(beanName, scope);
        if(bean == null)
        {
            if(type != null)
                try
                {
                    bean = RequestUtils.applicationInstance(beanType);
                    if(bean != null)
                        ((ActionForm)bean).setServlet(servlet);
                }
                catch(Exception e)
                {
                    throw new JspException(messages.getMessage("formTag.create", type, e.toString()));
                }
            else
                bean = RequestUtils.createActionForm((HttpServletRequest)pageContext.getRequest(), mapping, moduleConfig, servlet);
            if(bean instanceof ActionForm)
                ((ActionForm)bean).reset(mapping, (HttpServletRequest)pageContext.getRequest());
            if(bean == null)
                throw new JspException(messages.getMessage("formTag.create", beanType));
            pageContext.setAttribute(beanName, bean, scope);
        }
        pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, 2);
    }

    protected String renderFormStartElement()
    {
        HttpServletResponse response = (HttpServletResponse)pageContext.getResponse();
        StringBuffer results = new StringBuffer("<form");
        results.append(" name=\"");
        results.append(beanName);
        results.append("\"");
        results.append(" method=\"");
        results.append(method != null ? method : "post");
        results.append("\" action=\"");
        if ("true".equalsIgnoreCase(isRelative ) ) {
            String actionPath = RequestUtils.getActionMappingURL(action,pageContext);
            String output = actionPath;
            if (actionPath.startsWith("/Web/"))
            {
                output = actionPath.substring(4,actionPath.length());
            }
            results.append(response.encodeURL(output));
        } else {
            
            results.append(response.encodeURL(RequestUtils.getActionMappingURL(action,pageContext)));
        }
        //results.append(response.encodeURL(RequestUtils.getActionMappingURL(action, pageContext)));
        results.append("\"");
        if(styleClass != null)
        {
            results.append(" class=\"");
            results.append(styleClass);
            results.append("\"");
        }
        if(enctype != null)
        {
            results.append(" enctype=\"");
            results.append(enctype);
            results.append("\"");
        }
        if(onreset != null)
        {
            results.append(" onreset=\"");
            results.append(onreset);
            results.append("\"");
        }
        if(onsubmit != null)
        {
            results.append(" onsubmit=\"");
            results.append(onsubmit);
            results.append("\"");
        }
        if(style != null)
        {
            results.append(" style=\"");
            results.append(style);
            results.append("\"");
        }
        if(styleId != null)
        {
            results.append(" id=\"");
            results.append(styleId);
            results.append("\"");
        }
        if(target != null)
        {
            results.append(" target=\"");
            results.append(target);
            results.append("\"");
        }
        results.append(">");
        return results.toString();
    }

    protected String renderToken()
    {
        StringBuffer results = new StringBuffer();
        HttpSession session = pageContext.getSession();
        if(session != null)
        {
            String token = (String)session.getAttribute("org.apache.struts.action.TOKEN");
            if(token != null)
            {
                results.append("<input type=\"hidden\" name=\"");
                results.append("org.apache.struts.taglib.html.TOKEN");
                results.append("\" value=\"");
                results.append(token);
                if(isXhtml())
                    results.append("\" />");
                else
                    results.append("\">");
            }
        }
        return results.toString();
    }

    public int doEndTag()
        throws JspException
    {
        pageContext.removeAttribute("org.apache.struts.taglib.html.BEAN", 2);
        pageContext.removeAttribute("org.apache.struts.taglib.html.FORM", 2);
        StringBuffer results = new StringBuffer("</form>");
        if(focus != null)
            results.append(renderFocusJavascript());
        JspWriter writer = pageContext.getOut();
        try
        {
            writer.print(results.toString());
        }
        catch(IOException e)
        {
            throw new JspException(messages.getMessage("common.io", e.toString()));
        }
        return 6;
    }

    protected String renderFocusJavascript()
    {
        StringBuffer results = new StringBuffer();
        results.append(lineEnd);
        results.append("<script type=\"text/javascript\"");
        if(!isXhtml())
            results.append(" language=\"JavaScript\"");
        results.append(">");
        results.append(lineEnd);
        if(!isXhtml())
        {
            results.append("  <!--");
            results.append(lineEnd);
        }
        StringBuffer focusControl = new StringBuffer("document.forms[\"");
        focusControl.append(beanName);
        focusControl.append("\"].elements[\"");
        focusControl.append(focus);
        focusControl.append("\"]");
        results.append("  var focusControl = ");
        results.append(focusControl.toString());
        results.append(";");
        results.append(lineEnd);
        results.append(lineEnd);
        results.append("  if (focusControl.type != \"hidden\") {");
        results.append(lineEnd);
        String index = "";
        if(focusIndex != null)
        {
            StringBuffer sb = new StringBuffer("[");
            sb.append(focusIndex);
            sb.append("]");
            index = sb.toString();
        }
        results.append("     focusControl");
        results.append(index);
        results.append(".focus();");
        results.append(lineEnd);
        results.append("  }");
        results.append(lineEnd);
        if(!isXhtml())
        {
            results.append("  // -->");
            results.append(lineEnd);
        }
        results.append("</script>");
        results.append(lineEnd);
        return results.toString();
    }

    public void release()
    {
        super.release();
        action = null;
        moduleConfig = null;
        enctype = null;
        focus = null;
        focusIndex = null;
        mapping = null;
        method = null;
        name = null;
        onreset = null;
        onsubmit = null;
        scope = null;
        servlet = null;
        style = null;
        styleClass = null;
        styleId = null;
        target = null;
        type = null;
        isRelative = null;
    }

    protected void lookup()
        throws JspException
    {
        moduleConfig = RequestUtils.getModuleConfig(pageContext);
        if(moduleConfig == null)
        {
            JspException e = new JspException(messages.getMessage("formTag.collections"));
            pageContext.setAttribute("org.apache.struts.action.EXCEPTION", e, 2);
            throw e;
        }
        servlet = (ActionServlet)pageContext.getServletContext().getAttribute("org.apache.struts.action.ACTION_SERVLET");
        String mappingName = RequestUtils.getActionMappingName(action);
        mapping = (ActionMapping)moduleConfig.findActionConfig(mappingName);
        if(mapping == null)
        {
            JspException e = new JspException(messages.getMessage("formTag.mapping", mappingName));
            pageContext.setAttribute("org.apache.struts.action.EXCEPTION", e, 2);
            throw e;
        }
        if(name != null)
            if(type == null)
            {
                JspException e = new JspException(messages.getMessage("formTag.nameType"));
                pageContext.setAttribute("org.apache.struts.action.EXCEPTION", e, 2);
                throw e;
            } else
            {
                beanName = name;
                beanScope = scope != null ? scope : "session";
                beanType = type;
                return;
            }
        FormBeanConfig formBeanConfig = moduleConfig.findFormBeanConfig(mapping.getName());
        if(formBeanConfig == null)
        {
            JspException e = new JspException(messages.getMessage("formTag.formBean", mapping.getName()));
            pageContext.setAttribute("org.apache.struts.action.EXCEPTION", e, 2);
            throw e;
        } else
        {
            beanName = mapping.getAttribute();
            beanScope = mapping.getScope();
            beanType = formBeanConfig.getType();
            return;
        }
    }

    private boolean isXhtml()
    {
        return RequestUtils.isXhtml(pageContext);
    }

    public String getFocusIndex()
    {
        return focusIndex;
    }

    public void setFocusIndex(String focusIndex)
    {
        this.focusIndex = focusIndex;
    }

    protected String action;
    protected ModuleConfig moduleConfig;
    protected String enctype;
    protected String focus;
    protected String focusIndex;
    protected static String lineEnd = System.getProperty("line.separator");
    protected ActionMapping mapping;
    protected static MessageResources messages = MessageResources.getMessageResources("org.apache.struts.taglib.html.LocalStrings");
    protected String method;
    protected String name;
    protected String onreset;
    protected String onsubmit;
    protected String scope;
    protected ActionServlet servlet;
    protected String style;
    protected String styleClass;
    protected String styleId;
    protected String target;
    protected String type;
    protected String beanName;
    protected String beanScope;
    protected String beanType;
    protected String isRelative;

}
