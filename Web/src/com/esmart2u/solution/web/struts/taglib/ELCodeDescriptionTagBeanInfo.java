/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELCodeDescriptionTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.SimpleBeanInfo;
import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;

/**
 * Code Description Tag Bean info.
 *
 * @author Goh Siew Chyn
 * @version $Id: ELCodeDescriptionTagBeanInfo.java,v 1.4 2004/01/18 08:15:57 wkchee Exp $
 */

public class ELCodeDescriptionTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("code", ELCodeDescriptionTag.class,
                                                null, "setCodeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("description", ELCodeDescriptionTag.class,
                                                null, "setDescriptionExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("alternateDescription", ELCodeDescriptionTag.class,
                                                null, "setAlternateDescriptionExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("delimiterKey", ELCodeDescriptionTag.class,
                                                null, "setDelimiterKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("openBracketKey", ELCodeDescriptionTag.class,
                                                null, "setOpenBracketKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("closeBracketKey", ELCodeDescriptionTag.class,
                                                null, "setCloseBracketKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELCodeDescriptionTagBeanInfo.java