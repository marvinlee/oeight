/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELPageHeaderTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.SimpleBeanInfo;
import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the <code>BeanInfo</code> descriptor for the
 * <code>ELPageHeaderTag</code> class.  It is
 * needed to override the default mapping of custom tag attribute names to
 * class attribute names.
 * <p/>
 * This is because the value of the unevaluated EL expression has to be kept
 * separately from the evaluated value, which is stored in the controller class. This
 * is related to the fact that the JSP compiler can choose to reuse different
 * tag instances if they received the same original attribute values, and the
 * JSP compiler can choose to not re-call the setter methods, because it can
 * assume the same values are already set.
 *
 * @author Lee Meau Chyuan
 * @version $Id: ELPageHeaderTagBeanInfo.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELPageHeaderTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("key", ELPageHeaderTag.class,
                                                null, "setKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("styleClass", ELPageHeaderTag.class,
                                                null, "setStyleClassExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELPageHeaderTagBeanInfo.java