/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// CodeDescriptionTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.struts.util.RequestUtils;
import org.apache.struts.taglib.html.BaseHandlerTag;

import com.esmart2u.solution.base.helper.StringUtils;
import com.esmart2u.solution.base.service.ConfigurationManager;

/**
 * This <code>CodeDescriptionTag</code> class is the implementation class of the
 * Code Description tag library.
 * This component will receive 2 parameters, code and description as input and get
 * the delimiter from the resource file for display.
 *
 * @author Law Tat Yoong.
 * @version $Id: CodeDescriptionTag.java,v 1.4 2004/01/18 08:15:57 wkchee Exp $
 */

public class CodeDescriptionTag extends BaseHandlerTag
{
    private String delimiter;
    private String openBracket;
    private String closeBracket;

    private String code;
    private String description;
    private String alternateDescription;
    private String delimiterKey;
    private String openBracketKey;
    private String closeBracketKey;

    public CodeDescriptionTag()
    {
        delimiter = ConfigurationManager.getKey("DROPLIST_DELIMITER");
        openBracket = ConfigurationManager.getKey("DROPLIST_ALT_DESC_OPEN_BRACKET");
        closeBracket = ConfigurationManager.getKey("DROPLIST_ALT_DESC_CLOSE_BRACKET");
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getAlternateDescription()
    {
        return alternateDescription;
    }

    public void setAlternateDescription(String alternateDescription)
    {
        this.alternateDescription = alternateDescription;
    }

    public String getDelimiterKey()
    {
        return delimiterKey;
    }

    public void setDelimiterKey(String delimiterKey)
    {
        this.delimiterKey = delimiterKey;
    }

    public String getOpenBracketKey()
    {
        return openBracketKey;
    }

    public void setOpenBracketKey(String openBracketKey)
    {
        this.openBracketKey = openBracketKey;
    }

    public String getCloseBracketKey()
    {
        return closeBracketKey;
    }

    public void setCloseBracketKey(String closeBracketKey)
    {
        this.closeBracketKey = closeBracketKey;
    }

    /**
     * Start of Tag Processing
     */
    public int doStartTag() throws JspException
    {
        return SKIP_BODY;
    }

    /**
     * End of Tag Processing
     *
     * @throws javax.servlet.jsp.JspException if a JSP exception occurs
     */
    public int doEndTag() throws JspException
    {
        if (code == null)
            throw new NullPointerException("The code is null");

        try
        {
            if (delimiterKey != null)
                delimiter = RequestUtils.message(pageContext, null, null, delimiterKey);

            // if there is no matching from the delimiter key. Get the default value.
            if (delimiter == null || delimiter.equals(""))
                delimiter = ConfigurationManager.getKey("DROPLIST_DELIMITER");

            if (openBracketKey != null)
                openBracket = RequestUtils.message(pageContext, null, null, openBracketKey);

            // if there is no matching from the open bracket key. Get the default value.
            if (openBracket == null || openBracket.equals(""))
                openBracket = ConfigurationManager.getKey("DROPLIST_ALT_DESC_OPEN_BRACKET");

            if (closeBracketKey != null)
                closeBracket = RequestUtils.message(pageContext, null, null, closeBracketKey);

            // if there is no matching from the close bracket key. Get the default value.
            if (closeBracket == null || closeBracket.equals(""))
                closeBracket = ConfigurationManager.getKey("DROPLIST_ALT_DESC_CLOSE_BRACKET");

            JspWriter writer = pageContext.getOut();
            if (StringUtils.hasValue(code))
                writer.println(code);
            if (StringUtils.hasValue(code) && StringUtils.hasValue(description))
                writer.println(" " + delimiter + " ");
            if (StringUtils.hasValue(description))
                writer.println(description);
            if (StringUtils.hasValue(alternateDescription))
                writer.println(openBracket + alternateDescription + closeBracket);
        }
        catch (IOException e)
        {
            throw new JspException("Exception in PagingInfoTag doEndTag():" + e.toString());
        }

        return SKIP_BODY;
    }
}

// end of CodeDescriptionTag.java