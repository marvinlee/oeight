/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELDateFormatterTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;
import java.beans.SimpleBeanInfo;

/**
 * This is the <code>BeanInfo</code> descriptor for the
 * <code>com.esmart2u.solution.web.struts.taglib.ELDateFormatterTag</code> class.  It is needed
 * to override the default mapping of custom tag attribute names to class
 * attribute names.
 * <p/>
 * This is because the value of the unevaluated EL expression has to be kept
 * separately from the evaluated value, which is stored in the controller class. This
 * is related to the fact that the JSP compiler can choose to reuse different
 * tag instances if they received the same original attribute values, and the
 * JSP compiler can choose to not re-call the setter methods, because it can
 * assume the same values are already set.
 * <p/>
 * <p/>
 * <i>16 Dec 2003 v1.2 danielang</i>
 * <p/>
 * Removed country code and language code following implementation of retrieving Locale information directly
 * from session
 * <p/>
 *
 * @author Daniel Ang
 * @version $Id: ELDateFormatterTagBeanInfo.java,v 1.4 2004/06/15 10:46:42 wkchee Exp $
 */

public class ELDateFormatterTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("value", ELDateFormatterTag.class, null, "setValueExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("pattern", ELDateFormatterTag.class, null, "setPatternExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("locale", ELDateFormatterTag.class, null, "setLocaleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("showTime", ELDateFormatterTag.class, null, "setShowTimeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELDateFormatterTagBeanInfo.java