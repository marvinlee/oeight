/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELPagingInfoTag.java

package com.esmart2u.solution.web.struts.taglib;

import org.apache.strutsel.taglib.utils.EvalHelper;

import javax.servlet.jsp.JspException;

/**
 * Custom tag for paging information.
 * <p/>
 * This class is a subclass of the class
 * <code>PagingInfoTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author Goh Siew Chyn
 * @version $Id: ELPagingInfoTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELPagingInfoTag extends PagingInfoTag
{
    private String currentPageExpr;
    private String totalPageExpr;

    public String getCurrentPageExpr()
    {
        return currentPageExpr;
    }

    public void setCurrentPageExpr(String currentPageExpr)
    {
        this.currentPageExpr = currentPageExpr;
    }

    public String getTotalPageExpr()
    {
        return totalPageExpr;
    }

    public void setTotalPageExpr(String totalPageExpr)
    {
        this.totalPageExpr = totalPageExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        Object currentPage;
        Object totalPage;

        if ((currentPage = EvalHelper.eval("currentPage", getCurrentPageExpr(), this, pageContext)) != null)
            setCurrentPage(Integer.parseInt(currentPage.toString()));
        if ((totalPage = EvalHelper.eval("totalPage", getTotalPageExpr(), this, pageContext)) != null)
            setTotalPage(Integer.parseInt(totalPage.toString()));
    }
}

// end of ELPagingInfoTag.java