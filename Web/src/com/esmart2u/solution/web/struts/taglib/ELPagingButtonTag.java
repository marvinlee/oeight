/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELPagingButtonTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Custom tag for paging button.
 * <p/>
 * This class is a subclass of the class
 * <code>PagingButtonTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author Goh Siew Chyn
 * @version $Id: ELPagingButtonTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELPagingButtonTag extends PagingButtonTag
{
    private String currentPageExpr;
    private String totalPageExpr;
    private String onPrevClickExpr;
    private String onNextClickExpr;
    private String prevPropertyExpr;
    private String nextPropertyExpr;
    private String styleClassExpr;

    public String getCurrentPageExpr()
    {
        return currentPageExpr;
    }

    public void setCurrentPageExpr(String currentPageExpr)
    {
        this.currentPageExpr = currentPageExpr;
    }

    public String getTotalPageExpr()
    {
        return totalPageExpr;
    }

    public void setTotalPageExpr(String totalPageExpr)
    {
        this.totalPageExpr = totalPageExpr;
    }

    public String getOnPrevClickExpr()
    {
        return onPrevClickExpr;
    }

    public void setOnPrevClickExpr(String onPrevClickExpr)
    {
        this.onPrevClickExpr = onPrevClickExpr;
    }

    public String getOnNextClickExpr()
    {
        return onNextClickExpr;
    }

    public void setOnNextClickExpr(String onNextClickExpr)
    {
        this.onNextClickExpr = onNextClickExpr;
    }

    public String getPrevPropertyExpr()
    {
        return prevPropertyExpr;
    }

    public void setPrevPropertyExpr(String prevPropertyExpr)
    {
        this.prevPropertyExpr = prevPropertyExpr;
    }

    public String getNextPropertyExpr()
    {
        return nextPropertyExpr;
    }

    public void setNextPropertyExpr(String nextPropertyExpr)
    {
        this.nextPropertyExpr = nextPropertyExpr;
    }

    public String getStyleClassExpr()
    {
        return styleClassExpr;
    }

    public void setStyleClassExpr(String styleClassExpr)
    {
        this.styleClassExpr = styleClassExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        String string = null;
        Integer currentPage;
        Integer totalPage;

        if ((string = EvalHelper.evalString("styleClass", getStyleClassExpr(), this, pageContext)) != null)
            setStyleClass(string);
        if ((currentPage = EvalHelper.evalInteger("currentPage", getCurrentPageExpr(), this, pageContext)) != null)
            setCurrentPage(currentPage.intValue());
        if ((totalPage = EvalHelper.evalInteger("totalPage", getTotalPageExpr(), this, pageContext)) != null)
            setTotalPage(totalPage.intValue());
        if ((string = EvalHelper.evalString("onPrevClick", getOnPrevClickExpr(), this, pageContext)) != null)
            setOnPrevClick(string);
        if ((string = EvalHelper.evalString("onNextClick", getOnNextClickExpr(), this, pageContext)) != null)
            setOnNextClick(string);
        if ((string = EvalHelper.evalString("prevProperty", getPrevPropertyExpr(), this, pageContext)) != null)
            setPrevProperty(string);
        if ((string = EvalHelper.evalString("nextProperty", getNextPropertyExpr(), this, pageContext)) != null)
            setNextProperty(string);
    }
}

// end of ELPagingButtonTag.java