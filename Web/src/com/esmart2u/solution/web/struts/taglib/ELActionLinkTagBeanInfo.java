/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELActionLinkTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;
import java.beans.SimpleBeanInfo;

/**
 * This is the <code>BeanInfo</code> descriptor for the
 * <code>com.esmart2u.taglib.ELActionLinkTag</code> class.  It is needed
 * to override the default mapping of custom tag attribute names to class
 * attribute names.
 * <p/>
 * This is because the value of the unevaluated EL expression has to be kept
 * separately from the evaluated value, which is stored in the controller class. This
 * is related to the fact that the JSP compiler can choose to reuse different
 * tag instances if they received the same original attribute values, and the
 * JSP compiler can choose to not re-call the setter methods, because it can
 * assume the same values are already set.
 */

public class ELActionLinkTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("accesskey", ELActionLinkTag.class,
                                                null, "setAccesskeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("action", ELActionLinkTag.class,
                                                null, "setActionExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("anchor", ELActionLinkTag.class,
                                                null, "setAnchorExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("forward", ELActionLinkTag.class,
                                                null, "setForwardExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("href", ELActionLinkTag.class,
                                                null, "setHrefExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("indexed", ELActionLinkTag.class,
                                                null, "setIndexedExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("indexId", ELActionLinkTag.class,
                                                null, "setIndexIdExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("linkName", ELActionLinkTag.class,
                                                null, "setLinkNameExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("name", ELActionLinkTag.class,
                                                null, "setNameExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onblur", ELActionLinkTag.class,
                                                null, "setOnblurExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onclick", ELActionLinkTag.class,
                                                null, "setOnclickExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("ondblclick", ELActionLinkTag.class,
                                                null, "setOndblclickExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onfocus", ELActionLinkTag.class,
                                                null, "setOnfocusExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onkeydown", ELActionLinkTag.class,
                                                null, "setOnkeydownExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onkeypress", ELActionLinkTag.class,
                                                null, "setOnkeypressExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onkeyup", ELActionLinkTag.class,
                                                null, "setOnkeyupExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmousedown", ELActionLinkTag.class,
                                                null, "setOnmousedownExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmousemove", ELActionLinkTag.class,
                                                null, "setOnmousemoveExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmouseout", ELActionLinkTag.class,
                                                null, "setOnmouseoutExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmouseover", ELActionLinkTag.class,
                                                null, "setOnmouseoverExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmouseup", ELActionLinkTag.class,
                                                null, "setOnmouseupExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("page", ELActionLinkTag.class,
                                                null, "setPageExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("paramId", ELActionLinkTag.class,
                                                null, "setParamIdExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("paramName", ELActionLinkTag.class,
                                                null, "setParamNameExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("paramProperty", ELActionLinkTag.class,
                                                null, "setParamPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("paramScope", ELActionLinkTag.class,
                                                null, "setParamScopeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("property", ELActionLinkTag.class,
                                                null, "setPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("scope", ELActionLinkTag.class,
                                                null, "setScopeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("style", ELActionLinkTag.class,
                                                null, "setStyleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("styleClass", ELActionLinkTag.class,
                                                null, "setStyleClassExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("styleId", ELActionLinkTag.class,
                                                null, "setStyleIdExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("tabindex", ELActionLinkTag.class,
                                                null, "setTabindexExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("target", ELActionLinkTag.class,
                                                null, "setTargetExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("title", ELActionLinkTag.class,
                                                null, "setTitleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("titleKey", ELActionLinkTag.class,
                                                null, "setTitleKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("transaction", ELActionLinkTag.class,
                                                null, "setTransactionExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        try
        {
            proplist.add(new PropertyDescriptor("actionCode", ELActionLinkTag.class,
                                                null, "setActionCodeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("actionList", ELActionLinkTag.class,
                                                null, "setActionListExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELActionLinkTagBeanInfo.java