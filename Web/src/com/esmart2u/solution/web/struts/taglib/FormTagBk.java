
package com.esmart2u.solution.web.struts.taglib;

/*
 * $Id: FormTagBk.java 54929 2004-10-16 16:38:42Z germuska $
 *
 * Copyright 1999-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.config.FormBeanConfig;
import org.apache.struts.config.ModuleConfig;
//import org.apache.struts.taglib.TagUtils;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.RequestUtils;

/**
 * Custom tag that represents an input form, associated with a bean whose
 * properties correspond to the various fields of the form.
 *
 * @version $Rev: 54929 $ $Date: 2004-10-16 09:38:42 -0700 (Sat, 16 Oct 2004) $
 */
public class FormTagBk extends org.apache.struts.taglib.html.FormTag {
    
    // ----------------------------------------------------- Instance Variables
    
    /**
     * The action URL to which this form should be submitted, if any.
     */
    protected String action = null;
    
    /**
     * The module configuration for our module.
     */
    protected ModuleConfig moduleConfig = null;
    
    /**
     * The content encoding to be used on a POST submit.
     */
    protected String enctype = null;
    
    /**
     * The name of the field to receive focus, if any.
     */
    protected String focus = null;
    
    /**
     * The index in the focus field array to receive focus.  This only applies if the field
     * given in the focus attribute is actually an array of fields.  This allows a specific
     * field in a radio button array to receive focus while still allowing indexed field
     * names like "myRadioButtonField[1]" to be passed in the focus attribute.
     * @since Struts 1.1
     */
    protected String focusIndex = null;
    
    /**
     * The line ending string.
     */
    protected static String lineEnd = System.getProperty("line.separator");
    
    /**
     * The ActionMapping defining where we will be submitting this form
     */
    protected ActionMapping mapping = null;
    
    /**
     * The message resources for this package.
     */
    //protected static MessageResources messages =
    //    MessageResources.getMessageResources(Constants.Package + ".LocalStrings");
    
    /**
     * The request method used when submitting this form.
     */
    protected String method = null;
    
    /**
     * The onReset event script.
     */
    protected String onreset = null;
    
    /**
     * The onSubmit event script.
     */
    protected String onsubmit = null;
    
    /**
     * Include language attribute in the focus script's &lt;script&gt; element.  This
     * property is ignored in XHTML mode.
     * @since Struts 1.2
     */
    protected boolean scriptLanguage = true;
    
    /**
     * The ActionServlet instance we are associated with (so that we can
     * initialize the <code>servlet</code> property on any form bean that
     * we create).
     */
    protected ActionServlet servlet = null;
    
    /**
     * The style attribute associated with this tag.
     */
    protected String style = null;
    
    /**
     * The style class associated with this tag.
     */
    protected String styleClass = null;
    
    /**
     * The identifier associated with this tag.
     */
    protected String styleId = null;
    
    /**
     * The window target.
     */
    protected String target = null;
    
    /**
     * The name of the form bean to (create and) use. This is either the same
     * as the 'name' attribute, if that was specified, or is obtained from the
     * associated <code>ActionMapping</code> otherwise.
     */
    protected String beanName = null;
    
    /**
     * The scope of the form bean to (create and) use. This is either the same
     * as the 'scope' attribute, if that was specified, or is obtained from the
     * associated <code>ActionMapping</code> otherwise.
     */
    protected String beanScope = null;
    
    /**
     * The type of the form bean to (create and) use. This is either the same
     * as the 'type' attribute, if that was specified, or is obtained from the
     * associated <code>ActionMapping</code> otherwise.
     */
    protected String beanType = null;
    
    /**
     * Whether or not the action="" should be a relative URL. Default is
     * false, to maintain backwards compatibility.
     */
    protected String isRelative = null;
    
    /**
     * The list of character encodings for input data that the server should
     * accept.
     */
    protected String acceptCharset = null;
    
    
    
    /**
     * Renders the action attribute
     */
    protected void renderAction(StringBuffer results) {
        System.out.println("Got into renderAction of esmart2u FormTag");
        HttpServletResponse response =
                (HttpServletResponse) this.pageContext.getResponse();
        
        results.append(" action=\"");
         /*results.append(
             response.encodeURL(
                 TagUtils.getInstance().getActionMappingURL(
                     this.action,
                     this.pageContext)));
          */
        if ("true".equalsIgnoreCase(isRelative ) ) {
            results.append(response.encodeURL(action));
        } else {
            
            results.append(response.encodeURL(RequestUtils.getActionMappingURL(action,
                    pageContext)));
        }
        
        results.append("\"");
        
        System.out.println("FormTag results="+ results);
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() {
        
        super.release();
        action = null;
        moduleConfig = null;
        enctype = null;
        focus = null;
        focusIndex = null;
        mapping = null;
        method = null;
        onreset = null;
        onsubmit = null;
        servlet = null;
        style = null;
        styleClass = null;
        styleId = null;
        target = null;
        acceptCharset = null;
        isRelative = null;
        
    }
    
    // ------------------------------------------------------ Protected Methods
    /**
     * Is the action="" relative? a value of "true" (case insensitive) indicates it is
     *
     * @param value
     */
    public void setIsRelative(String value) {
        
        this.isRelative = value;
        
    }
    
    public String getIsRelative() {
        
        return isRelative;
        
    }
    
    
    
}


