/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELCodeDescriptionTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Custom tag for code description.
 * <p/>
 * This class is a subclass of the class
 * <code>CodeDescriptionTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author Goh Siew Chyn
 * @version $Id: ELCodeDescriptionTag.java,v 1.4 2004/01/18 08:15:57 wkchee Exp $
 */

public class ELCodeDescriptionTag extends CodeDescriptionTag
{
    private String codeExpr;
    private String descriptionExpr;
    private String alternateDescriptionExpr;
    private String delimiterKeyExpr;
    private String openBracketKeyExpr;
    private String closeBracketKeyExpr;

    public String getCodeExpr()
    {
        return codeExpr;
    }

    public void setCodeExpr(String codeExpr)
    {
        this.codeExpr = codeExpr;
    }

    public String getDescriptionExpr()
    {
        return descriptionExpr;
    }

    public void setDescriptionExpr(String descriptionExpr)
    {
        this.descriptionExpr = descriptionExpr;
    }

    public String getAlternateDescriptionExpr()
    {
        return alternateDescriptionExpr;
    }

    public void setAlternateDescriptionExpr(String alternateDescriptionExpr)
    {
        this.alternateDescriptionExpr = alternateDescriptionExpr;
    }

    public String getDelimiterKeyExpr()
    {
        return delimiterKeyExpr;
    }

    public void setDelimiterKeyExpr(String delimiterKeyExpr)
    {
        this.delimiterKeyExpr = delimiterKeyExpr;
    }

    public String getOpenBracketKeyExpr()
    {
        return openBracketKeyExpr;
    }

    public void setOpenBracketKeyExpr(String openBracketKeyExpr)
    {
        this.openBracketKeyExpr = openBracketKeyExpr;
    }

    public String getCloseBracketKeyExpr()
    {
        return closeBracketKeyExpr;
    }

    public void setCloseBracketKeyExpr(String closeBracketKeyExpr)
    {
        this.closeBracketKeyExpr = closeBracketKeyExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        String string = null;

        if ((string = EvalHelper.evalString("code", getCodeExpr(), this, pageContext)) != null)
            setCode(string);
        if ((string = EvalHelper.evalString("description", getDescriptionExpr(), this, pageContext)) != null)
            setDescription(string);
        if ((string = EvalHelper.evalString("alternateDescriptionExpr", getAlternateDescriptionExpr(), this, pageContext)) != null)
            setAlternateDescription(string);
        if ((string = EvalHelper.evalString("delimiterKey", getDelimiterKeyExpr(), this, pageContext)) != null)
            setDelimiterKey(string);
        if ((string = EvalHelper.evalString("openBracketKey", getOpenBracketKeyExpr(), this, pageContext)) != null)
            setOpenBracketKey(string);
        if ((string = EvalHelper.evalString("closeBracketKey", getCloseBracketKeyExpr(), this, pageContext)) != null)
            setCloseBracketKey(string);
    }
}

// end of ELCodeDescriptionTag.java