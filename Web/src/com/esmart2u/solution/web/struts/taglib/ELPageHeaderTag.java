/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELPageHeaderTag.java

package com.esmart2u.solution.web.struts.taglib;

import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Custom tag for paging information.
 * <p/>
 * This class is a subclass of the class
 * <code>PageHeaderTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author Siripong Visasmongkolchai
 * @version $Id: ELPageHeaderTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELPageHeaderTag extends PageHeaderTag
{
    private String keyExpr;
    private String styleClassExpr;

    public String getKeyExpr()
    {
        return keyExpr;
    }

    public void setKeyExpr(String keyExpr)
    {
        this.keyExpr = keyExpr;
    }

    public String getStyleClassExpr()
    {
        return styleClassExpr;
    }

    public void setStyleClassExpr(String styleClassExpr)
    {
        this.styleClassExpr = styleClassExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        String string = null;

        if ((string = EvalHelper.evalString("key", getKeyExpr(), this, pageContext)) != null)
            setKey(string);
        if ((string = EvalHelper.evalString("styleClass", getStyleClassExpr(), this, pageContext)) != null)
            setStyleClass(string);
    }
}

// end of ELPageHeaderTag.java