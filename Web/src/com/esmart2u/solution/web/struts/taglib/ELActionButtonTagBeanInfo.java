/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELActionButtonTagBeanInfo.java

package com.esmart2u.solution.web.struts.taglib;

import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;
import java.beans.SimpleBeanInfo;

/**
 * This is the <code>BeanInfo</code> descriptor for the
 * <code>com.esmart2u.taglib.ELActionButtonTag</code> class.  It is needed
 * to override the default mapping of custom tag attribute names to class
 * attribute names.
 * <p/>
 * This is because the value of the unevaluated EL expression has to be kept
 * separately from the evaluated value, which is stored in the controller class. This
 * is related to the fact that the JSP compiler can choose to reuse different
 * tag instances if they received the same original attribute values, and the
 * JSP compiler can choose to not re-call the setter methods, because it can
 * assume the same values are already set.
 */

public class ELActionButtonTagBeanInfo extends SimpleBeanInfo
{
    public PropertyDescriptor[] getPropertyDescriptors()
    {
        List proplist = new ArrayList(0);

        try
        {
            proplist.add(new PropertyDescriptor("accesskey", ELActionButtonTag.class,
                                                null, "setAccesskeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("alt", ELActionButtonTag.class,
                                                null, "setAltExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("altKey", ELActionButtonTag.class,
                                                null, "setAltKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("disabled", ELActionButtonTag.class,
                                                null, "setDisabledExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("indexed", ELActionButtonTag.class,
                                                null, "setIndexedExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onblur", ELActionButtonTag.class,
                                                null, "setOnblurExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onchange", ELActionButtonTag.class,
                                                null, "setOnchangeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onclick", ELActionButtonTag.class,
                                                null, "setOnclickExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("ondblclick", ELActionButtonTag.class,
                                                null, "setOndblclickExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onfocus", ELActionButtonTag.class,
                                                null, "setOnfocusExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onkeydown", ELActionButtonTag.class,
                                                null, "setOnkeydownExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onkeypress", ELActionButtonTag.class,
                                                null, "setOnkeypressExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onkeyup", ELActionButtonTag.class,
                                                null, "setOnkeyupExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmousedown", ELActionButtonTag.class,
                                                null, "setOnmousedownExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmousemove", ELActionButtonTag.class,
                                                null, "setOnmousemoveExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmouseout", ELActionButtonTag.class,
                                                null, "setOnmouseoutExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmouseover", ELActionButtonTag.class,
                                                null, "setOnmouseoverExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("onmouseup", ELActionButtonTag.class,
                                                null, "setOnmouseupExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("property", ELActionButtonTag.class,
                                                null, "setPropertyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("style", ELActionButtonTag.class,
                                                null, "setStyleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("styleClass", ELActionButtonTag.class,
                                                null, "setStyleClassExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("styleId", ELActionButtonTag.class,
                                                null, "setStyleIdExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("tabindex", ELActionButtonTag.class,
                                                null, "setTabindexExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("title", ELActionButtonTag.class,
                                                null, "setTitleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("titleKey", ELActionButtonTag.class,
                                                null, "setTitleKeyExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("value", ELActionButtonTag.class,
                                                null, "setValueExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("actionCode", ELActionButtonTag.class,
                                                null, "setActionCodeExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("actionList", ELActionButtonTag.class,
                                                null, "setActionListExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("approvalRequired", ELActionButtonTag.class,
                                                null, "setApprovalRequiredExpr"));
        }
        catch (IntrospectionException ex)
        {
        }
        try
        {
            proplist.add(new PropertyDescriptor("visible", ELActionButtonTag.class,
                                                null, "setVisibleExpr"));
        }
        catch (IntrospectionException ex)
        {
        }

        PropertyDescriptor[] result = new PropertyDescriptor[proplist.size()];
        return ((PropertyDescriptor[])proplist.toArray(result));
    }
}

// end of ELActionButtonTagBeanInfo.java