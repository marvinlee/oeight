/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// PagingInfoTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.io.IOException;
import java.text.MessageFormat;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.struts.taglib.html.BaseHandlerTag;
import org.apache.struts.util.RequestUtils;

/**
 * This class used to display paging information.
 *
 * @author Goh Siew Chyn
 * @version $Id: PagingInfoTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class PagingInfoTag extends BaseHandlerTag
{
    private static String PAGING = "common.text.pageOf";
    private int currentPage = 0;
    private int totalPage = 0;

    public void PagingInfo()
    {
        currentPage = 0;
        totalPage = 0;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
    }

    public int getTotalPage()
    {
        return totalPage;
    }

    public void setTotalPage(int totalPage)
    {
        this.totalPage = totalPage;
    }

    /**
     * Start of Tag Processing
     */
    public int doStartTag() throws JspException
    {
        return (EVAL_BODY_BUFFERED);
    }

    /**
     * End of Tag Processing
     *
     * @throws javax.servlet.jsp.JspException if a JSP exception occurs
     */
    public int doEndTag() throws JspException
    {
        StringBuffer buffer = new StringBuffer();
        prepareAttributes(buffer);

        if (bodyContent != null)
            buffer.append(bodyContent.getString().trim());

        JspWriter writer = pageContext.getOut();

        try
        {
            writer.print(buffer.toString());
        }
        catch (IOException e)
        {
            throw new JspException("Exception in PagingInfoTag doEndTag():" + e.toString());
        }

        return EVAL_PAGE;
    }

    public void prepareAttributes(StringBuffer buffer) throws JspException
    {
        Object[] args = new Object[2];

        args[0] = Integer.toString(currentPage);
        args[1] = Integer.toString(totalPage);

        String paging;

        String pagingMessage = RequestUtils.message(pageContext, null, null, PAGING);

        if (pagingMessage != null)
        {
            paging = pagingMessage;
        }
        else
        {
            paging = "Page {0} of {1}";
        }
        MessageFormat messageFormat = new MessageFormat(paging);
        String message = messageFormat.format(args);

        buffer.append(message);
    }

    public void release()
    {
        super.release();
        currentPage = 0;
        totalPage = 0;
    }
}

// end of PagingInfoTag.java