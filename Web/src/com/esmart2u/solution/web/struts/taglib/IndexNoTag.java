/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// IndexNoTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.struts.taglib.html.BaseHandlerTag;

/**
 * This <code>IndexNoTag</code> is the implementation class of the IndexNoTag tag.
 * This tag will render the index according to the iterate index, current page and
 * row per page.
 *
 * @author Law Tat Yoong.
 * @version $Id: IndexNoTag.java,v 1.2 2004/01/16 03:13:30 wkchee Exp $
 */

public class IndexNoTag extends BaseHandlerTag
{
    private int index;
    private int currentPage;
    private int rowsPerPage;

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public int getRowsPerPage()
    {
        return rowsPerPage;
    }

    public void setRowsPerPage(int rowsPerPage)
    {
        this.rowsPerPage = rowsPerPage;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
    }

    /**
     * Start of Tag Processing
     */
    public int doStartTag() throws JspException
    {
        return SKIP_BODY;
    }

    /**
     * End of Tag Processing
     *
     * @throws javax.servlet.jsp.JspException if a JSP exception occurs
     */
    public int doEndTag() throws JspException
    {
        try
        {
            int rowIndex;

            if ((currentPage == 0) && (rowsPerPage == 0))
            {
                rowIndex = index + 1;
            }
            else
            {
                rowIndex = (currentPage * rowsPerPage - rowsPerPage +
                    index + 1);
            }
            JspWriter writer = pageContext.getOut();
            writer.println(rowIndex + ".");
        }
        catch (IOException e)
        {
            throw new JspException("Exception in PagingInfo doEndTag():" + e.toString());
        }

        return SKIP_BODY;
    }
}

// end of IndexNoTag.java