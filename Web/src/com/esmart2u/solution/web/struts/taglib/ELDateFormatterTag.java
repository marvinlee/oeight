/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELDateFormatterTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.Locale;
import java.sql.Date;
import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

import com.esmart2u.solution.base.logging.Logger;

/**
 * This <code>ELDateFormatterTag</code> class is used to perform locale-sensitive formatting on date object.
 * This component expects a <code>java.sql.Date</code> object to be passed in as Value attribute.
 * <p/>
 * <p/>
 * <i>16 Dec 2003 v1.2 danielang</i>
 * <p/>
 * Removed country code and language code following implementation of retrieving Locale information directly
 * from session
 * <p/>
 *
 * @author Daniel Ang
 * @version $Id: ELDateFormatterTag.java,v 1.4 2004/06/15 10:46:42 wkchee Exp $
 */

public class ELDateFormatterTag extends DateFormatterTag
{
    private static Logger logger = Logger.getLogger(ELDateFormatterTag.class);

    private String valueExpr;
    private String patternExpr;
    private String localeExpr;
    private String showTimeExpr;

    public String getValueExpr()
    {
        return valueExpr;
    }

    public void setValueExpr(String valueExpr)
    {
        this.valueExpr = valueExpr;
    }

    public String getPatternExpr()
    {
        return patternExpr;
    }

    public void setPatternExpr(String patternExpr)
    {
        this.patternExpr = patternExpr;
    }

    public String getLocaleExpr()
    {
        return localeExpr;
    }

    public void setLocaleExpr(String localeExpr)
    {
        this.localeExpr = localeExpr;
    }

    public String getShowTimeExpr()
    {
        return showTimeExpr;
    }

    public void setShowTimeExpr(String showTimeExpr)
    {
        this.showTimeExpr = showTimeExpr;
    }

    /**
     * Start of Tag Processing
     */
    public int doStartTag() throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    /**
     * Resets attribute values for tag reuse.
     */
    public void release()
    {
        super.release();
        setValueExpr(null);
        setPatternExpr(null);
        setLocaleExpr(null);
        setShowTimeExpr(null);
    }

    /**
     * Evaluates the expressions
     *
     * @throws JspException
     */
    private void evaluateExpressions()
        throws JspException
    {
        Object object = null;
        String string = null;
        Boolean bool = null;

        try
        {
            if ((object = EvalHelper.eval("value", getValueExpr(), this, pageContext)) != null)
                setValue((Date)object);
            if ((string = EvalHelper.evalString("pattern", getPatternExpr(), this, pageContext)) != null)
                setPattern(string);
            if ((object = EvalHelper.eval("locale", getLocaleExpr(), this, pageContext)) != null)
                setLocale((Locale)object);
            if ((bool = EvalHelper.evalBoolean("showTime", getShowTimeExpr(), this, pageContext)) != null)
                setShowTime(bool.booleanValue());
        }
        catch (ClassCastException cce)
        {
            logger.error("Ensure that VALUE passed in is of Date object and LOCALE is of Locale object.");
            logger.error("ClassCastException encountered in ELDateFormatterTag.", cce);
        }
    }
}

// end of ELDateFormatterTag.java