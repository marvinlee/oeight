/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ELTabTag.java

package com.esmart2u.solution.web.struts.taglib;

import java.util.List;
import javax.servlet.jsp.JspException;

import org.apache.strutsel.taglib.utils.EvalHelper;

/**
 * Custom tag for paging information.
 * <p/>
 * This class is a subclass of the class
 * <code>TabTag</code> which provides most of
 * the described functionality.  This subclass allows all attribute values to
 * be specified as expressions utilizing the JavaServer Pages Standard Library
 * expression language.
 *
 * @author Lee Meau Chyuan
 * @version $Id: ELTabTag.java,v 1.4 2004/01/16 03:13:30 wkchee Exp $
 */

public class ELTabTag extends TabTag
{
    /**
     * Instance variable mapped to "form" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String formExpr;

    /**
     * Instance variable mapped to "tabList" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String tabListExpr;

    /**
     * Instance variable mapped to "tabGroupCode" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String tabGroupCodeExpr;

    /**
     * Instance variable mapped to "currentTab" tag attribute.
     * (Mapping set in associated BeanInfo class.)
     */
    private String currentTabExpr;

    public String getFormExpr()
    {
        return formExpr;
    }

    public void setFormExpr(String formExpr)
    {
        this.formExpr = formExpr;
    }

    public String getTabListExpr()
    {
        return tabListExpr;
    }

    public void setTabListExpr(String tabListExpr)
    {
        this.tabListExpr = tabListExpr;
    }

    public String getTabGroupCodeExpr()
    {
        return tabGroupCodeExpr;
    }

    public void setTabGroupCodeExpr(String tabGroupCodeExpr)
    {
        this.tabGroupCodeExpr = tabGroupCodeExpr;
    }

    public String getCurrentTabExpr()
    {
        return currentTabExpr;
    }

    public void setCurrentTabExpr(String currentTabExpr)
    {
        this.currentTabExpr = currentTabExpr;
    }

    public int doStartTag()
        throws JspException
    {
        evaluateExpressions();
        return super.doStartTag();
    }

    private void evaluateExpressions()
        throws JspException
    {
        String form = null;
        List list = null;
        String code = null;
        String string = null;

        if ((form = EvalHelper.evalString("form", getFormExpr(), this, pageContext)) != null)
            setForm(form);
        if ((list = (List)EvalHelper.eval("tabList", getTabListExpr(), this, pageContext)) != null)
            setTabList(list);
        if ((code = EvalHelper.evalString("tabGroupCode", getTabGroupCodeExpr(), this, pageContext)) != null)
            setTabGroupCode(code);
        if ((string = EvalHelper.evalString("currentTab", getCurrentTabExpr(), this, pageContext)) != null)
            setCurrentTab(string);
    }
}

// end of ELTabTag.java