
package com.esmart2u.solution.base.logging;

import  java.io.Serializable;
import java.io.ObjectInputStream;

import org.apache.log4j.Level;

public class Logger
    implements Serializable
{
    /**
     * The category name.
     */
    private final String name;

    /**
     * The Log4j delegate logger.
     */
    private transient org.apache.log4j.Logger log;

    /**
     * Constructor.
     *
     * @param name The category name.
     */
    protected Logger(final String name)
    {
        this.name = name;
        log = org.apache.log4j.Logger.getLogger(name);
    }

    /**
     * Expose the raw category for this logger.
     */
    public org.apache.log4j.Logger getLogger()
    {
        return log;
    }

    /**
     * Return the category name of this logger.
     *
     * @return The category name of this logger.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Check to see if the TRACE priority is enabled for this category.
     *
     * @return True if a {@link #trace(Object)} method invocation would pass
     *         the msg to the configured appenders, false otherwise.
     */
    public boolean isTraceEnabled()
    {
        if (log.isEnabledFor(XLevel.TRACE) == false)
        {
            return false;
        }
        return XLevel.TRACE.isGreaterOrEqual(log.getEffectiveLevel());
    }

    /**
     * Issue a log message with a priority of TRACE.
     * Invokes log.log(XPriority.TRACE, message);
     */
    public void trace(Object message)
    {
        log.log(XLevel.TRACE, message);
    }

    /**
     * Issue a log message and throwable with a priority of TRACE.
     * Invokes log.log(XPriority.TRACE, message, t);
     */
    public void trace(Object message, Throwable t)
    {
        log.log(XLevel.TRACE, message, t);
    }

    /**
     * Check to see if the TRACE priority is enabled for this category.
     *
     * @return True if a {@link #trace(Object)} method invocation would pass
     *         the msg to the configured appenders, false otherwise.
     */
    public boolean isDebugEnabled()
    {
        Level l = Level.DEBUG;
        if (log.isEnabledFor(l) == false)
        {
            return false;
        }
        return l.isGreaterOrEqual(log.getEffectiveLevel());
    }

    /**
     * Issue a log message with a priority of DEBUG.
     * Invokes log.log(Priority.DEBUG, message);
     */
    public void debug(Object message)
    {
        log.log(Level.DEBUG, message);
    }

    /**
     * Issue a log message and throwable with a priority of DEBUG.
     * Invokes log.log(Priority.DEBUG, message, t);
     */
    public void debug(Object message, Throwable t)
    {
        log.log(Level.DEBUG, message, t);
    }

    /**
     * Check to see if the INFO priority is enabled for this category.
     *
     * @return true if a {@link #info(Object)} method invocation would pass
     *         the msg to the configured appenders, false otherwise.
     */
    public boolean isInfoEnabled()
    {
        Level l = Level.INFO;
        if (log.isEnabledFor(l) == false)
        {
            return false;
        }
        return l.isGreaterOrEqual(log.getEffectiveLevel());
    }

    /**
     * Issue a log message with a priority of INFO.
     * Invokes log.log(Priority.INFO, message);
     */
    public void info(Object message)
    {
        log.log(Level.INFO, message);
    }

    /**
     * Issue a log message and throwable with a priority of INFO.
     * Invokes log.log(Priority.INFO, message, t);
     */
    public void info(Object message, Throwable t)
    {
        log.log(Level.INFO, message, t);
    }

    /**
     * Issue a log message with a priority of WARN.
     * Invokes log.log(Priority.WARN, message);
     */
    public void warn(Object message)
    {
        log.log(Level.WARN, message);
    }

    /**
     * Issue a log message and throwable with a priority of WARN.
     * Invokes log.log(Priority.WARN, message, t);
     */
    public void warn(Object message, Throwable t)
    {
        log.log(Level.WARN, message, t);
    }

    /**
     * Issue a log message with a priority of ERROR.
     * Invokes log.log(Priority.ERROR, message);
     */
    public void error(Object message)
    {
        log.log(Level.ERROR, message);
    }

    /**
     * Issue a log message and throwable with a priority of ERROR.
     * Invokes log.log(Priority.ERROR, message, t);
     */
    public void error(Object message, Throwable t)
    {
        log.log(Level.ERROR, message, t);
    }

    /**
     * Issue a log message with a priority of FATAL.
     * Invokes log.log(Priority.FATAL, message);
     */
    public void fatal(Object message)
    {
        log.log(Level.FATAL, message);
    }

    /**
     * Issue a log message and throwable with a priority of FATAL.
     * Invokes log.log(Priority.FATAL, message, t);
     */
    public void fatal(Object message, Throwable t)
    {
        log.log(Level.FATAL, message, t);
    }

    /**
     * Issue a log messae with the given priority.
     * Invokes log.log(l, message);
     */
    public void log(Level l, Object message)
    {
        log.log(l, message);
    }

    /**
     * Issue a log message with the given priority.
     * Invokes log.log(l, message, t);
     */
    public void log(Level l, Object message, Throwable t)
    {
        log.log(l, message, t);
    }


    /////////////////////////////////////////////////////////////////////////
    //                         Custom Serialization                        //
    /////////////////////////////////////////////////////////////////////////

    private void readObject(ObjectInputStream stream)
        throws java.io.IOException, ClassNotFoundException
    {
        // restore non-transient fields (aka name)
        stream.defaultReadObject();

        // restore logging
        log = org.apache.log4j.Logger.getLogger(name);
    }


    /////////////////////////////////////////////////////////////////////////
    //                            Factory Methods                          //
    /////////////////////////////////////////////////////////////////////////

    /**
     * Create a Logger instance given the category name.
     *
     * @param name The category name.
     */
    public static Logger getLogger(String name)
    {
        return new Logger(name);
    }

    /**
     * Create a Logger instance given the category name with the given suffix.
     * <p/>
     * <p>This will include a category seperator between classname and suffix
     *
     * @param name   The category name.
     * @param suffix A suffix to append to the classname.
     */
    public static Logger getLogger(String name, String suffix)
    {
        return new Logger(name + "." + suffix);
    }

    /**
     * Create a Logger instance given the category class. This simply
     * calls create(clazz.getName()).
     *
     * @param theClass The Class whose name will be used as the category name
     */
    public static Logger getLogger(Class theClass)
    {
        return new Logger(theClass.getName());
    }

    /**
     * Create a Logger instance given the category class with the given suffix.
     * <p/>
     * <p>This will include a category seperator between classname and suffix
     *
     * @param theClass The Class whose name will be used as the category name.
     * @param suffix   A suffix to append to the classname.
     */
    public static Logger getLogger(Class theClass, String suffix)
    {
        return new Logger(theClass.getName() + "." + suffix);
    }
}

// end of Logger.java