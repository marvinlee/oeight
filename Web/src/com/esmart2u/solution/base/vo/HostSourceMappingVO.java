 

// HostSourceMappingVO.java

package com.esmart2u.solution.base.vo;

import java.lang.String;
import java.util.Map;
import java.io.Serializable;
import java.lang.Cloneable;

import com.esmart2u.solution.base.helper.ValueObject;

/**
 * Value object to represent table tbl_mt_src_map.
 *
 * @author Chee Weng Keong
 * @version $Id: HostSourceMappingVO.java,v 1.2 2004/01/16 04:53:32 wkchee Exp $
 */

public class HostSourceMappingVO extends ValueObject
    implements Serializable, Cloneable
{
    private static final long serialVersionUID = -8811881188118811881L;

    public static final String SP_SELECT = "MtHostSourceMappingSelect";
    public static final String SP_INSERT = "MtHostSourceMappingInsert";
    public static final String SP_UPDATE = "MtHostSourceMappingUpdate";
    public static final String SP_DELETE = "MtHostSourceMappingDelete";

    public static final Map COLUMN_MAP = toColumnMap(new String[][]
    {
        // properties direct mapping with value object table
        {"id", "id"},
        {"tbl_nm", "tableName"},
        {"tbl_key", "tableKey"},
        {"mt_host_sys_cd", "backendHostSystemCode"},
        {"cd", "code"},
        {"version", "currentRecordVersion"},

        // extra properties
        {"mt_host_sys_dscp", "backendHostSystemDescription"},
        {"cd_length", "codeDataLength"},
        {"cd_mt_data_typ_cd", "codeDataType"}
    });


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS PROPERTIES DIRECT MAPPING WITH VALUE OBJECT TABLE.
    //******************************************************************************************************************
    private String id;
    private String tableName;
    private String tableKey;
    private String backendHostSystemCode;
    private String code;
    private int currentRecordVersion;


    //******************************************************************************************************************
    // EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    private String backendHostSystemDescription;
    private int codeDataLength;
    private String codeDataType;


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS METHODS TO ACCESS THE VALUE OBJECT TABLE.
    //******************************************************************************************************************
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getTableKey()
    {
        return tableKey;
    }

    public void setTableKey(String tableKey)
    {
        this.tableKey = tableKey;
    }

    public String getBackendHostSystemCode()
    {
        return backendHostSystemCode;
    }

    public void setBackendHostSystemCode(String backendHostSystemCode)
    {
        this.backendHostSystemCode = backendHostSystemCode;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public int getCurrentRecordVersion()
    {
        return currentRecordVersion;
    }

    public void setCurrentRecordVersion(int currentRecordVersion)
    {
        this.currentRecordVersion = currentRecordVersion;
    }


    //******************************************************************************************************************
    // METHODS FOR EXTRA PROPERTIES. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    public String getBackendHostSystemDescription()
    {
        return backendHostSystemDescription;
    }

    public void setBackendHostSystemDescription(String backendHostSystemDescription)
    {
        this.backendHostSystemDescription = backendHostSystemDescription;
    }

    public int getCodeDataLength()
    {
        return codeDataLength;
    }

    public void setCodeDataLength(int codeDataLength)
    {
        this.codeDataLength = codeDataLength;
    }

    public String getCodeDataType()
    {
        return codeDataType;
    }

    public void setCodeDataType(String codeDataType)
    {
        this.codeDataType = codeDataType;
    }


    //******************************************************************************************************************
    // MISCELLANEOUS.
    //******************************************************************************************************************
    public Object clone() throws CloneNotSupportedException
    {
        HostSourceMappingVO hostSourceMappingVO = (HostSourceMappingVO)super.clone();
        return hostSourceMappingVO;
    }
}

// end of HostSourceMappingVO.java