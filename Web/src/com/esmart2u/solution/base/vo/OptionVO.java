/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// OptionVO.java

package com.esmart2u.solution.base.vo;

import java.util.Map;

import com.esmart2u.solution.base.helper.ValueObject;

public class OptionVO extends ValueObject
{
    private static final long serialVersionUID = -8811881188118811881L;

    public static final Map COLUMN_MAP = toColumnMap(new String[][]
    {
        // properties direct mapping with value object table

        // extra properties
        {"value", "value"},
        {"label", "label"},
        {"alt_label", "alternateLabel"},
        {"is_default", "defaultFlag"},
        {"is_deactivated", "deactivatedFlag"},
        {"is_del", "softDeletedFlag"}
    });


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS PROPERTIES DIRECT MAPPING WITH VALUE OBJECT TABLE.
    //******************************************************************************************************************


    //******************************************************************************************************************
    // EXTRA FIELDS. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    private String value;
    private String label;
    private String alternateLabel;
    private String defaultFlag;
    private String deactivatedFlag;
    private String softDeletedFlag;
    private String searchValue;
    private String searchValue2;
    private String searchValue3;
    private String searchValue4;
    private String searchValue5;


    //******************************************************************************************************************
    // DO NOT CHANGE THE FOLLOWING SECTION. THIS SECTION CONTAINS METHODS TO ACCESS THE VALUE OBJECT TABLE.
    //******************************************************************************************************************


    //******************************************************************************************************************
    // METHODS FOR EXTRA FIELD. YOU MAY APPEND TO THE FOLLOWING SECTION.
    //******************************************************************************************************************
    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getAlternateLabel()
    {
        return alternateLabel;
    }

    public void setAlternateLabel(String alternateLabel)
    {
        this.alternateLabel = alternateLabel;
    }

    public String getDefaultFlag()
    {
        return defaultFlag;
    }

    public void setDefaultFlag(String defaultFlag)
    {
        this.defaultFlag = defaultFlag;
    }

    public String getDeactivatedFlag()
    {
        return deactivatedFlag;
    }

    public void setDeactivatedFlag(String deactivatedFlag)
    {
        this.deactivatedFlag = deactivatedFlag;
    }

    public String getSoftDeletedFlag()
    {
        return softDeletedFlag;
    }

    public void setSoftDeletedFlag(String softDeletedFlag)
    {
        this.softDeletedFlag = softDeletedFlag;
    }

    public String getSearchValue()
    {
        return searchValue;
    }

    public void setSearchValue(String searchValue)
    {
        this.searchValue = searchValue;
    }

    public String getSearchValue2()
    {
        return searchValue2;
    }

    public void setSearchValue2(String searchValue2)
    {
        this.searchValue2 = searchValue2;
    }

    public String getSearchValue3()
    {
        return searchValue3;
    }

    public void setSearchValue3(String searchValue3)
    {
        this.searchValue3 = searchValue3;
    }

    public String getSearchValue4()
    {
        return searchValue4;
    }

    public void setSearchValue4(String searchValue4)
    {
        this.searchValue4 = searchValue4;
    }

    public String getSearchValue5()
    {
        return searchValue5;
    }

    public void setSearchValue5(String searchValue5)
    {
        this.searchValue5 = searchValue5;
    }


    //******************************************************************************************************************
    // MISCELLANEOUS.
    //******************************************************************************************************************
}

// end of OptionVO.java