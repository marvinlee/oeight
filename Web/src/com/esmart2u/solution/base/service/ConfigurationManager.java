
package com.esmart2u.solution.base.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.net.URL;

import com.esmart2u.solution.base.logging.Logger;

/**
 * This class is to provide access to common configuration properties
 * like server URL and initial context factory.
 *
 * Make sure you include the following system property in your command line
 * or the app server's batch file (change the path accordingly) :
 *
 * <code>-Desmart2u.system.properties=C:/poc/config/esmart2u.properties</code>
 *
 * @author  Wong Chee Peng
 * @version $Id: ConfigurationManager.java,v 1.5 2004/01/15 07:46:32 wkchee Exp $
 */

public class ConfigurationManager
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(ConfigurationManager.class);

    /**
     * Default properties file name.
     */
    public static final String PROPERTIES_FILE = "esmart2u.properties";

    /**
     * Main property class to load the system properties.
     */
    protected static Properties properties = new Properties();

    /**
     * Default location of the property file.
     */
    protected static String propertyFile = null;

    /**
     * Default server url key name in the property file.
     */
    private static String serverURLKey = "esmart2u.jndi.url";

    /**
     * Default jndi context factory in the property file.
     */
    private static String serverContextKey = "esmart2u.jndi.factory";

    /**
     * Default jndi eai datasource key name in the property file.
     */
    private static String eaiDataSourceKey = "esmart2u.datasource.eai";

    /**
     * EAI transactional datasource key name in the property file.
     */
    private static String eaiTxDataSourceKey = "esmart2u.datasource.tx.eai";

    /**
     * EAI non-transactional datasource key name in the property file.
     */
    private static String eaiNxDataSourceKey = "esmart2u.datasource.nx.eai";

    /**
     * Default jndi auditlog datasource key name in the property file.
     * Added by: Marvin Lee, in order to support AuditLog++
     */
    private static String auditLogDataSourceKey = "esmart2u.datasource.auditlog";

    /**
     * Default jndi host datasource key name in the property file.
     */
    private static String hostDataSourceKey = "esmart2u.datasource.host";

    /**
     * Static initializer to load system properties when it was called for the first time.
     */
    static
    {
        try
        {
            // First try, we get the system property for esmart2u.system.properties (hard code, standard key)
            logger.info("Attempting to load properties file name from system properties");
            propertyFile = System.getProperty("esmart2u.system.properties");

            // Perform a validation check and see if the system property for esmart2u.system.properties has been set
            // (Added by Marvin Lee on 9-Dec-2003 11:47 AM)
            if (propertyFile == null || propertyFile.equals(""))
            {
                // esmart2u.system.properties is not set.
                // (Added by Marvin Lee on 9-Dec-2003 11:47 AM)
                logger.info("System property for esmart2u.system.properties is not set");
                propertyFile = PROPERTIES_FILE;
                loadConfigFromClassPath(properties, propertyFile);
            }
            else
            {
                // esmart2u.system.properties has been set. Try to load the specified path. This might throw an
                // exception, which we should be prepared to catch at any cause
                // (Added by Marvin Lee on 9-Dec-2003 11:47 AM)
                loadConfigFromFile(properties, propertyFile);
            }
        }
        catch (Throwable t)
        {
            logger.error("Failed to load " + propertyFile + " from classpath. Please check your configuration");
        }
    }

    /**
     * To load config from path defined in system environment.
     *
     * @param properties Properties to contain the loaded config.
     * @param propertyFile Properties file name.
     * @throws Exception If exception is caught.
     */
    public static void loadConfigFromFile(Properties properties, String propertyFile) throws Exception
    {
        logger.info("Loading property file = " + propertyFile);
        properties.load(new FileInputStream(propertyFile));
        logger.info(propertyFile + " successfully loaded");
    }

    /**
     * To load config from classpath.
     *
     * @param properties Properties to contain the loaded config.
     * @param propertyFile Properties file name.
     * @throws Exception If exception is caught.
     */
    public static void loadConfigFromClassPath(Properties properties, String propertyFile) throws Exception
    {
        // Upon failure to load from system properties, attempt to load it from classpath (final straw). In order
        // for this feature to work, one must ensure that the properties file is visible to this class
        // for this feature to work, one must ensure that the properties file is visible to this class
        // (via ClassLoader hierarchy concept). In addition to that, it is mandatory for the properties file to
        // be named as default config name (hardcoded default file name, just like log4j.properties). Should the
        // project require any other name, it is up to their descretion, and this method might not apply anymore.
        // (Added by Marvin Lee on 9-Dec-2003 11:47 AM)
        logger.info("Attempting to load " + propertyFile + " from classpath");

        // We use this class's ClassLoader to roam the classpath for the property file. Remember,
        // the default file name is hardcoded as is in respective ConfigurationManager!
        ClassLoader cl = ConfigurationManager.class.getClassLoader();
        URL url = cl.getResource(propertyFile);

        // Validate and check that the returned URL is not null
        if (url != null)
        {
            // URL returned is not null. Try to load it (using the URL class's openStream() method)
            logger.debug("Found " + propertyFile + " within the classpath. Loading it");
            properties.load(url.openStream());
            logger.info(propertyFile + " successfully loaded");
        }
        else
        {
            // ClassLoader is unable to find the properties file. Fail on it.
            throw new FileNotFoundException("Unable to locate " + propertyFile + " within the ClassPath");
        }
    }

    /**
     * Method to retrieve server url system property.
     *
     * @return String Server url system property.
     */
    public static String getServerURL()
    {
        return properties.getProperty(serverURLKey);
    }

    /**
     * Method to retrieve server url property key from the property file.
     *
     * @return String Server url property key.
     */
    public static String getServerURLKey()
    {
        return serverURLKey;
    }

    /**
     * Method to set the server url property key for server url value
     * retrieval.
     *
     * @param serverURLKey Server url property key.
     */
    public static void setServerURLKey(String serverURLKey)
    {
        ConfigurationManager.serverURLKey = serverURLKey;
    }

    /**
     * Method to retrieve server context system property.
     *
     * @return String Server context system property.
     */
    public static String getServerContext()
    {
        return properties.getProperty(serverContextKey);
    }

    /**
     * Method to retrieve context property key from the property file.
     *
     * @return String Server context property key.
     */
    public static String getServerContextKey()
    {
        return serverContextKey;
    }

    /**
     * Method to set the context property key for server url value
     * retrieval.
     *
     * @param serverContextKey Server context property key.
     */
    public static void setServerContextKey(String serverContextKey)
    {
        ConfigurationManager.serverContextKey = serverContextKey;
    }

    /**
     * @deprecated Use ConfigurationManager.getServerContextKey(). Will be removed on 29 Feb 2004.
     */
    public static String getContextKey()
    {
        return ConfigurationManager.getServerContextKey();
    }


    /**
     * @deprecated Use ConfigurationManager.setServerContextKey(String contextKey). Will be removed on 29 Feb 2004.
     */
    public static void setContextKey(String contextKey)
    {
        ConfigurationManager.setServerContextKey(contextKey);
    }

    /**
     * Method to retrieve the EAI datasource.
     *
     * @return String EAI dataSource JNDI name.
     */
    public static String getEaiDataSource()
    {
        return properties.getProperty(eaiDataSourceKey);
    }

    /**
     * Method to retrieve the EAI datasource key
     *
     * @return String EAI datasource key.
     */
    public static String getEaiDataSourceKey()
    {
        return eaiDataSourceKey;
    }

    /**
     * Method to set the EAI datasource key
     *
     * @param eaiDataSourceKey EAI datasource key.
     */
    public static void setEaiDataSourceKey(String eaiDataSourceKey)
    {
        ConfigurationManager.eaiDataSourceKey = eaiDataSourceKey;
    }

    /**
     * @deprecated Use ConfigurationManager.getEaiDataSource(). Will be removed on 29 Feb 2004.
     */
    public static String getEaiDatasource()
    {
        return ConfigurationManager.getEaiDataSource();
    }

    /**
     * @deprecated Use ConfigurationManager.getEaiDataSourceKey(). Will be removed on 29 Feb 2004.
     */
    public static String getEaiDatasourceKey()
    {
        return ConfigurationManager.getEaiDataSourceKey();
    }

    /**
     * @deprecated Use ConfigurationManager.setEaiDataSourceKey(String eaiDatasourceKey). Will be removed on 29 Feb 2004.
     */
    public static void setEaiDatasourceKey(String eaiDatasourceKey)
    {
        ConfigurationManager.setEaiDataSourceKey(eaiDatasourceKey);
    }

    /**
     * Method to retrieve the EAI TX datasource.
     *
     * @return String EAI TX dataSource JNDI name.
     */
    public static String getEaiTxDataSource()
    {
        return properties.getProperty(eaiTxDataSourceKey);
    }

    /**
     * Method to retrieve the EAI TX datasource key
     *
     * @return String EAI TX datasource key.
     */
    public static String getEaiTxDataSourceKey()
    {
        return eaiTxDataSourceKey;
    }

    /**
     * Method to set the EAI TX datasource key
     *
     * @param eaiTxDataSourceKey EAI TX datasource key.
     */
    public static void setEaiTxDataSourceKey(String eaiTxDataSourceKey)
    {
        ConfigurationManager.eaiTxDataSourceKey = eaiTxDataSourceKey;
    }

    /**
     * Method to retrieve the EAI NX datasource.
     *
     * @return String EAI NX datasource JNDI name.
     */
    public static String getEaiNxDataSource()
    {
        return properties.getProperty(eaiNxDataSourceKey);
    }

    /**
     * Method to retrieve the EAI NX datasource key.
     *
     * @return String EAI NX datasource key.
     */
    public static String getEaiNxDataSourceKey()
    {
        return eaiNxDataSourceKey;
    }

    /**
     * Method to set the EAI NX datasource key.
     *
     * @param eaiNxDataSourceKey EAI NX datasource key.
     */
    public static void setEaiNxDataSourceKey(String eaiNxDataSourceKey)
    {
        ConfigurationManager.eaiNxDataSourceKey = eaiNxDataSourceKey;
    }

    /**
     * Method to retrieve the Audit Log datasource.
     *
     * @return String Audit Log datasource JNDI name.
     */
    public static String getAuditLogDataSource()
    {
        return properties.getProperty(auditLogDataSourceKey);
    }

    /**
     * Method to retrieve the Audit Log datasource key.
     *
     * @return String Audit Log datasource key.
     */
    public static String getAuditLogDataSourceKey()
    {
        return auditLogDataSourceKey;
    }

    /**
     * Method to set the Audit Log datasource key.
     *
     * @param auditLogDataSourceKey Audit Log datasource key.
     */
    public static void setAuditLogDataSourceKey(String auditLogDataSourceKey)
    {
        ConfigurationManager.auditLogDataSourceKey = auditLogDataSourceKey;
    }

    /**
     * Method to retrieve the Host datasource.
     *
     * @return String Host dataSource JNDI name.
     */
    public static String getHostDataSource()
    {
        return properties.getProperty(hostDataSourceKey);
    }

    public static String getHostDataSourceKey()
    {
        return hostDataSourceKey;
    }

    public static void setHostDataSourceKey(String hostDataSourceKey)
    {
        ConfigurationManager.hostDataSourceKey = hostDataSourceKey;
    }

    /**
     * Method to retrieve absolute path of the system property file.
     *
     * @return String Absolute path of system property file.
     */
    public static String getPropertyFile()
    {
        return propertyFile;
    }

    /**
     * Method to override the default absolute path of system property file.
     * Upon setting, it will invoke a method to reload the system properties.
     *
     * @param propertyFile Absolute path of the system property file.
     */
    public static void setPropertyFile(String propertyFile)
    {
        ConfigurationManager.propertyFile = propertyFile;
        reload();
    }

    /**
     * Method to retrieve any key
     *
     * @return String Key.
     */
    public static String getKey(String key)
    {
        return properties.getProperty(key);
    }

    /**
     * Method to set any key.
     *
     * @param key Key.
     * @param value Value.
     */
    public static void setKey(String key, String value)
    {
        properties.setProperty(key, value);
    }

    /**
     * Method to reload the system properties values.
     */
    protected static void reload()
    {
        try
        {
            properties.load(new FileInputStream(propertyFile));
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }
    }
}

// end of ConfigurationManager.java
