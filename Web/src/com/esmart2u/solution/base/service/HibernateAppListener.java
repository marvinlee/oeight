/*
 * HibernateAppListener.java
 *
 * Created on September 21, 2007, 11:33 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.service;

import com.esmart2u.solution.web.struts.service.HibernateUtil;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;
import org.apache.log4j.Logger;
import org.hibernate.Session;


/**
 *
 * @author meauchyuan.lee
 */
public class HibernateAppListener  implements ServletContextListener	{
    
    /* Application Startup Event */
    public void	contextInitialized(ServletContextEvent ce) {
        System.out.println("Initializing HibernateAppListener");
        Logger loggerConnect = Logger.getLogger("Connect");
        
        try  {
            loggerConnect.debug("In HibernateAppListener.contextInitialized");
            
            //Class.forName("com.esmart2u.solution.web.struts.service.HibernateUtil").newInstance();
            HibernateUtil.getSessionFactory(); // Just call the static initializer of that class    

            loggerConnect.debug("In HibernateAppListener, Class.forName for tomcatJndi.HibernateUtil successful");
        } catch (Exception e)  {
            loggerConnect.debug("In HibernateAppListener, Class.forName for tomcatJndi.HibernateUtil throws Exception");
        }
    }
    
    /* Application Shutdown	Event */ 
    public void contextDestroyed(ServletContextEvent event) {
        HibernateUtil.getSessionFactory().close(); // Free all resources
    }

}

