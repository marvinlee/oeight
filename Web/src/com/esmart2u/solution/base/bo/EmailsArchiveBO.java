/*
 * EmailsArchiveBO.java
 *
 * Created on May 30, 2008, 12:32 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.bo;

import com.esmart2u.oeight.data.EmailsArchive;
import com.esmart2u.solution.web.struts.service.HibernateUtil;
import com.esmart2u.solution.base.helper.contactlist.Contact;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;

/**
 *
 * @author meauchyuan.lee
 */
public class EmailsArchiveBO {
    
    /** Creates a new instance of EmailsArchiveBO */
    public EmailsArchiveBO() {
    }
    
    /*
     * Save email list
     */
    public static void saveEmailContacts(String emailAddress, List contactList)
    {
        if (contactList == null || contactList.isEmpty()) return;
        
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null; 
        try {
        
            transaction = session.beginTransaction();
 
            // If already exists, do not update
            Criteria crit = session.createCriteria(EmailsArchive.class); 
            crit.add( Expression.eq("emailAddress", emailAddress) );  
            EmailsArchive dbArchive = (EmailsArchive) crit.uniqueResult();

            if (dbArchive != null) return;

            EmailsArchive emailsArchive = new EmailsArchive();
            emailsArchive.setEmailAddress(emailAddress); 
            emailsArchive.setContactList(getDelimitedString(contactList));

            session.save(emailsArchive);  
        
            transaction.commit();
            
        } catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            //throw e;
        } finally {
            session.close();
        }
        
    }
    
    private static String getDelimitedString(List valueList) {
        StringBuffer resultString = new StringBuffer();
        String result = "";
        if (valueList != null && !valueList.isEmpty())
        {  
            for(int i=0; i<valueList.size();i++)
            {
                Contact contact = (Contact)valueList.get(i);
                String value = contact.getEmailAddress();
                resultString.append(value + ",");  
            } 
            result = resultString.toString(); 
            if (resultString.length() > 4999)
            {
                result = resultString.substring(0,4999);
            }  
            result = result.substring(0, result.lastIndexOf(","));
        }  
        return result; 
    }
}
