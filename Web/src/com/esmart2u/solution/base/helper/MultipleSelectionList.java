/**
 * � 2003 esmart2u Malaysia Sdn. Bhd.  All rights reserved.
 * MarvinLee.net
 * 
 * 
 *
 * This software is the intellectual property of esmart2u Malaysia Sdn. Bhd.
 * The program may be used only in accordance with the terms of the license
 * agreement you entered into with esmart2u Malaysia Sdn. Bhd.
 *
 * User: Benjamin
 * Computer: Afinity
 * Date: 05-Sep-2003
 * Time: 16:15:50
 */

package com.esmart2u.solution.base.helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Class <code>com.esmart2u.solution.base.helper.MultipleSelectionList</code> acts as a
 * wrapper for special case ValueObjects that behave in a multiple choice
 * selection manner. Such scenarios are applicable when mutable choices
 * of ValueObjects can be selected.
 *
 * In order to provide support for AuditLogging, ValueObjects that contain
 * nested list collections of mutable ValueObjects of such behaviour should
 * encapsulate them in thin class, rather than other Collection classes such
 * as ArrayList or Vectors. This is needed to notify both AdvancedEAI and
 * AuditLog that the list elements are multiple choice in nature.
 *
 * @author Marvin Lee, esmart2u Malaysia Sdn Bhd
 */
public class MultipleSelectionList extends ArrayList {

    /**
     * The control ValueObject used to define a logical control
     * over this list of ValueObjects
     */
    private ValueObject controlValueObject = null;

    /**
     * The EAIAction for the control ValueObject. Based on the EAIAction
     * variables defined in class ValueObject. Initially set to NO_ACTION
     * @see ValueObject
     */
    private int controlEAIAction = ValueObject.NO_ACTION;

    /**
     * The SP_ALIAS for the control ValueObject
     */
    private String controlSpAlias = null;

    /**
     * Default constructor. Instantiates a new Collection list
     * @see ArrayList()
     */
    public MultipleSelectionList() {
        super();
    }

    /**
     * Overload constructor used to initialise a new Collection list
     * with a List of Objects
     * @param list the list of Objects
     */
    public MultipleSelectionList(List list) {
        super();
        if(list != null && !list.isEmpty()) {
            for(int i = 0; i < list.size(); i++) {
                super.add(i, list.get(i));
            }
        }
    }

    /**
     * Overload constructor used to initialis a new Collection list
     * with an array of ValueObjects
     * @param valueObjects the array of ValueObjects
     */
    public MultipleSelectionList(ValueObject[] valueObjects) {
        super();
        if(valueObjects != null && valueObjects.length > 0) {
            for (int i = 0; i < valueObjects.length; i++) {
                super.add(i, valueObjects[i]);
            }
        }
    }

    /**
     * Sets the list of ValueObjects based on a given list of
     * ValueObjects. This method will re-initialise the Collection,
     * discarding all existing items
     * @param list the list of ValueObjects
     */
    public void setValueObjects(List list) {
        super.clear();
        if(list != null && !list.isEmpty()) {
            for(int i = 0; i < list.size(); i++) {
                super.add(i, list.get(i));
            }
        }
    }

    /**
     * Sets the list of ValueObjects based on a given array of
     * ValueObjects. This method will re-initialise the Collection,
     * discarding all existing items
     * @param valueObjects the array of ValueObjects
     */
    public void setValueObjects(ValueObject[] valueObjects) {
        super.clear();
        if(valueObjects != null && valueObjects.length > 0) {
            for (int i = 0; i < valueObjects.length; i++) {
                super.add(i, valueObjects[i]);
            }
        }
    }

    /**
     * Returns the control ValueObject's EAIAction
     * @return the control ValueObject's EAIAction
     */
    public int getControlEAIAction() {
        return controlEAIAction;
    }

    /**
     * Sets the control ValueObject's EAIAction
     * @param controlEAIAction the control ValueObject's EAIAction
     * @see ValueObject
     */
    public void setControlEAIAction(int controlEAIAction) {
        this.controlEAIAction = controlEAIAction;
    }

    /**
     * Returns the SP_ALIAS of the control ValueObject
     * @return the SP_ALIAS of the control ValueObject
     */
    public String getControlSpAlias() {
        return controlSpAlias;
    }

    /**
     * Sets the SP_ALIAS of the control ValueObject
     * @param controlSpAlias the SP_ALIAS of the control ValueObject
     */
    public void setControlSpAlias(String controlSpAlias) {
        this.controlSpAlias = controlSpAlias;
    }

    /**
     * Returns the control ValueObject
     * @return the control ValueObject
     */
    public ValueObject getControlValueObject() {
        return controlValueObject;
    }

    /**
     * Sets the control ValueObject
     * @param controlValueObject the control ValueObject
     */
    public void setControlValueObject(ValueObject controlValueObject) {
        this.controlValueObject = controlValueObject;
    }

    /**
     * Sets the control ValueObject for this Multiple Selection List. This
     * method also sets the EAIAction and the SP_ALIAS associated with the
     * control ValueObject.
     * @param controlValueObject the control ValueObject
     * @param controlEAIAction the control ValueObject's EAIAction
     * @param controlSpAlias the control ValueObject's SP_ALIAS
     */
    public void setControlValueObject(ValueObject controlValueObject,
                                      int controlEAIAction,
                                      String controlSpAlias) {
        this.controlValueObject = controlValueObject;
        this.controlEAIAction = controlEAIAction;
        this.controlSpAlias = controlSpAlias;
    }
}
