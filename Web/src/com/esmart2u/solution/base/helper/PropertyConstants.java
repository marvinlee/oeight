/*
 * PropertyConstants.java
 *
 * Created on September 29, 2007, 2:48 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper;

/**
 *
 * @author meauchyuan.lee
 */
public interface PropertyConstants {
    
    public static final String SYSTEM_DOMAIN_NAME = "system.domain.name";
    public static final String WORKING_WEBAPP_HOME = "working.webapp.path";
    
    public static final String LOG4J_CONFIG_FILE = "config.log4j.path";
    public static final String MAIN_CONFIG_FILE_KEY = "config.main.path";
    public static final String MAIN_CONFIG_FILE_VALUE = "C:/Work_home/conf/config.properties";
    
    public static final String PHOTO_ACCEPTED_MIMES = "photo.settings.mime";
    public static final String PHOTO_STORE_PATH = "photos.album.path";
    public static final String PHOTO_DEFAULT_LARGE = "photos.default.large";
    public static final String PHOTO_DEFAULT_SMALL = "photos.default.small";
    public static final String THUMBNAIL_STORE_PATH = "thumbnail.album.path";
    public static final String MOSAIC_STORE_PATH = "mosaic.album.path";
    public static final String MOSAIC_MAP_PATH = "mosaic.map.path";
    public static final String MOSAIC_GENERATE_ENABLE = "mosaic.generate.enable";
    public static final String MOSAIC_GENERATE_FREQUENCY = "mosaic.generate.frequency";
    
    // 080808 Wedding 
    public static final String WEDDING_PHOTO_STORE_PATH = "wedding.photo.album.path";
    public static final String WEDDING_PHOTO_DEFAULT_LARGE = "wedding.photo.default.large";
    public static final String WEDDING_PHOTO_DEFAULT_SMALL = "wedding.photo.default.small";
    public static final String WEDDING_THUMBNAIL_STORE_PATH = "wedding.thumbnail.album.path";
    public static final String WEDDING_MOSAIC_STORE_PATH = "wedding.mosaic.album.path";
    public static final String WEDDING_MOSAIC_MAP_PATH = "wedding.mosaic.map.path";
    
    public static final String INVITES_SENDER_ENABLE = "invites.sender.enable";
    public static final String INVITES_SENDER_FREQUENCY = "invites.sender.frequency";
    
    public static final String RESET_PASSWORD_SENDER_ENABLE = "resetpassword.sender.enable";
    public static final String RESET_PASSWORD_SENDER_FREQUENCY = "resetpassword.sender.frequency";
    
    public static final String SEASONS_GREETINGS_SENDER_ENABLE = "seasonsgreetings.sender.enable";
    public static final String SEASONS_GREETINGS_SENDER_FREQUENCY = "seasonsgreetings.sender.frequency"; 
    
    public static final String RECAPTCHA_ENABLED = "recaptcha.service.enable";
    public static final String RECAPTCHA_PUBLIC_KEY = "recaptcha.key.public";
    public static final String RECAPTCHA_PRIVATE_KEY = "recaptcha.key.private";
    
    public static final char BOOLEAN_YES = 'Y';
    public static final char BOOLEAN_NO = 'N';
    
    public static final char GENDER_MALE = 'M';
    public static final char GENDER_FEMALE = 'F';
    
    public static final String DEFAULT_PAGE_ITEMS_KEY = "default.page.items"; 
    
    public static final String SMTP_DISPLAY_SENDER = "system.mail.senderdisplay";
    public static final String SMTP_HOST_KEY = "system.mail.host";
    public static final String SMTP_PORT_KEY = "system.mail.port";
    public static final String SMTP_REPLY_TO_KEY = "system.mail.replyto";
    public static final String SMTP_USER_KEY = "system.mail.user";
    public static final String SMTP_PASS_KEY = "system.mail.pass";
    
    public static final String SERVICE_FLICKR_KEY = "service.flickr.key";
    
    public static final String USER_TYPE_KEY = "user.current.type";
    public static final String SYSTEM_USERID_KEY = "system.userid.key";
    public static final String SYSTEM_FRIENDSTER_PROFILE = "system.friendster.profile";
    
    public static final String SYSTEM_MIN_AGE_BIRTH_YEAR = "system.minimumage.birthyear";
    
    public static final String WISHERS_LIST_MAX = "system.wisherslist.maxitems";
    public static final String MESSAGE_LIST_MAX = "system.messagelist.maxitems";
    
    public static final String SYSTEM_DEFAULT_BLOG_PHOTO = "system.default.blogPhoto";
     
    public static final String SERVICE_FRIENDSTER_CALLBACK1_API_KEY = "service.friendster.app1.apikey";
    public static final String SERVICE_FRIENDSTER_CALLBACK1_SECRET = "service.friendster.app1.secret";
    public static final String SERVICE_FRIENDSTER_CALLBACK1_INSTALL_URL = "service.friendster.app1.installURL";
    public static final String SERVICE_FRIENDSTER_CALLBACK2_API_KEY = "service.friendster.app2.apikey";
    public static final String SERVICE_FRIENDSTER_CALLBACK2_SECRET = "service.friendster.app2.secret";
    public static final String SERVICE_FRIENDSTER_CALLBACK2_INSTALL_URL = "service.friendster.app2.installURL";
    public static final String SERVICE_FRIENDSTER_CALLBACK3_API_KEY = "service.friendster.app3.apikey";
    public static final String SERVICE_FRIENDSTER_CALLBACK3_SECRET = "service.friendster.app3.secret";
    public static final String SERVICE_FRIENDSTER_CALLBACK3_INSTALL_URL = "service.friendster.app3.installURL";
    public static final String SERVICE_FRIENDSTER_CALLBACK4_API_KEY = "service.friendster.app4.apikey";
    public static final String SERVICE_FRIENDSTER_CALLBACK4_SECRET = "service.friendster.app4.secret";
    public static final String SERVICE_FRIENDSTER_CALLBACK4_INSTALL_URL = "service.friendster.app4.installURL";
    public static final String SERVICE_FRIENDSTER_CALLBACK5_API_KEY = "service.friendster.app5.apikey";
    public static final String SERVICE_FRIENDSTER_CALLBACK5_SECRET = "service.friendster.app5.secret";
    public static final String SERVICE_FRIENDSTER_CALLBACK5_INSTALL_URL = "service.friendster.app5.installURL";
    public static final String SERVICE_FRIENDSTER_CALLBACK6_API_KEY = "service.friendster.app6.apikey";
    public static final String SERVICE_FRIENDSTER_CALLBACK6_SECRET = "service.friendster.app6.secret";
    public static final String SERVICE_FRIENDSTER_CALLBACK6_INSTALL_URL = "service.friendster.app6.installURL";
     
     
    public static final String SERVICE_FACEBOOK_CALLBACK1_API_KEY = "service.facebook.app1.apikey";
    public static final String SERVICE_FACEBOOK_CALLBACK1_SECRET = "service.facebook.app1.secret";
    public static final String SERVICE_FACEBOOK_CALLBACK1_INSTALL_URL = "service.facebook.app1.installURL";
    public static final String SERVICE_FACEBOOK_CALLBACK3_API_KEY = "service.facebook.app3.apikey";
    public static final String SERVICE_FACEBOOK_CALLBACK3_SECRET = "service.facebook.app3.secret";
    public static final String SERVICE_FACEBOOK_CALLBACK3_INSTALL_URL = "service.facebook.app3.installURL";
    public static final String SERVICE_FACEBOOK_CALLBACK5_API_KEY = "service.facebook.app5.apikey";
    public static final String SERVICE_FACEBOOK_CALLBACK5_SECRET = "service.facebook.app5.secret";
    public static final String SERVICE_FACEBOOK_CALLBACK5_INSTALL_URL = "service.facebook.app5.installURL";
    public static final String SERVICE_FACEBOOK_CALLBACK6_API_KEY = "service.facebook.app6.apikey";
    public static final String SERVICE_FACEBOOK_CALLBACK6_SECRET = "service.facebook.app6.secret";
    public static final String SERVICE_FACEBOOK_CALLBACK6_INSTALL_URL = "service.facebook.app6.installURL";
    
    
    public static final String EMAIL_UPDATE_ENABLE = "system.emailupdate.enable";
    public static final String EMAIL_UPDATE_SYSTEM_SENDER_EMAIL = "system.emailupdate.systemaddress";
    public static final String EMAIL_UPDATE_SYSTEM_SENDER_ALIAS = "system.emailupdate.systemalias";
    public static final String EMAIL_UPDATE_FREQUENCY = "system.emailupdate.frequency";
    public static final String EMAIL_UPDATE_START_TIME = "system.emailupdate.starttime";
    public static final String EMAIL_UPDATE_END_TIME = "system.emailupdate.endtime";
    
    // This is the key to form composer eg, system.emailupdate.code-1.composer=<className>
    public static final String EMAIL_UPDATE_COMPOSER_PREFIX = "system.emailupdate.code";
    public static final String EMAIL_UPDATE_COMPOSER_POSTFIX = ".composer";
    
    // This is the key to form tracker eg, system.tracker.code1.max=<int_value>
    public static final String TRACKER_MAX_PREFIX = "system.tracker.code";
    public static final String TRACKER_MAX_POSTFIX = ".max";
    
    
    public static final String CAMPAIGN_INVITE_ENABLE = "system.campaigninvite.enable";
    public static final String CAMPAIGN_INVITE_SYSTEM_SENDER_EMAIL = "system.campaigninvite.systemaddress";
    public static final String CAMPAIGN_INVITE_SYSTEM_SENDER_ALIAS = "system.campaigninvite.systemalias";
    public static final String CAMPAIGN_INVITE_FREQUENCY = "system.campaigninvite.frequency";
    public static final String CAMPAIGN_INVITE_START_TIME = "system.campaigninvite.starttime";
    public static final String CAMPAIGN_INVITE_END_TIME = "system.campaigninvite.endtime";
    
    // This is the key to form composer eg, system.emailupdate.code-1.composer=<className>
    public static final String CAMPAIGN_INVITE_COMPOSER_PREFIX = "system.campaigninvite.code";
    public static final String CAMPAIGN_INVITE_COMPOSER_POSTFIX = ".composer";
    
    // PhpBb Forum constants
    public static final String PHPBB_USER_TEMPLATE_ID = "phpbb.templateuser.id";
  
    // RSS constants
    public static final String RSS_MAIN_FEED_TYPE = "rss.forum_main.feedtype";
    public static final String RSS_MAIN_FEED_FILE = "rss.forum_main.feedfile";
    public static final String RSS_MAIN_FEED_TITLE = "rss.forum_main.feedtitle";
    public static final String RSS_MAIN_FEED_LINK = "rss.forum_main.feedlink";
    public static final String RSS_MAIN_FEED_DESC = "rss.forum_main.feeddesc";

    
}
