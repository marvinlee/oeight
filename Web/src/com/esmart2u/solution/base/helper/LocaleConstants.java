/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// LocaleConstants.java

package com.esmart2u.solution.base.helper;

import java.util.Locale;

/**
 * Locale constants. Contains useful constants that are not defined in java.util.Locale.
 *
 * @author  Chee Weng Keong
 * @version $Id: LocaleConstants.java,v 1.1 2003/12/05 13:32:47 wkchee Exp $
 */

public interface LocaleConstants
{
    /**
     * Useful constant for country.
     */
    public static final Locale THAILAND = new Locale("th", "TH");
}

// end of LocaleConstants.java