package com.esmart2u.solution.base.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Calendar;

/**
 * @deprecated No longer support. Will be removed on 29 Feb 2004.
 */

public class CommonBean
implements java.io.Serializable
{
    private static int YEAR_RANGE = 30;

    private Collection day;
    private Collection month;
    private Collection year;
    private Collection hour;
    private Collection minute;
    private Collection second;
    private Collection date;
    private int startYear = Calendar.getInstance().get(Calendar.YEAR);

    public CommonBean()
    {}

    public Collection getDay()
    {
        ArrayList arrayList = new ArrayList();
        for (int i = 1; i <= 9; i++)
        {
            arrayList.add(new KeyValue("0" + i, "0" + i));
        }
        for (int i = 10; i <= 31; i++)
        {
            arrayList.add(new KeyValue("" + i, "" + i));
        }
        this.day = (Collection) arrayList;

        return this.day;
    }

    public Collection getMonth()
    {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new KeyValue("Jan", "01"));
        arrayList.add(new KeyValue("Feb", "02"));
        arrayList.add(new KeyValue("Mar", "03"));
        arrayList.add(new KeyValue("Apr", "04"));
        arrayList.add(new KeyValue("May", "05"));
        arrayList.add(new KeyValue("Jun", "06"));
        arrayList.add(new KeyValue("Jul", "07"));
        arrayList.add(new KeyValue("Aug", "08"));
        arrayList.add(new KeyValue("Sep", "09"));
        arrayList.add(new KeyValue("Oct", "10"));
        arrayList.add(new KeyValue("Nov", "11"));
        arrayList.add(new KeyValue("Dec", "12"));
        this.month = (Collection) arrayList;

        return this.month;
    }

    public Collection getYear()
    {
        int endYear = Calendar.getInstance().get(Calendar.YEAR) + YEAR_RANGE;

        ArrayList arrayList = new ArrayList();
        for (int i = startYear; i <= endYear; i++)
            arrayList.add(new KeyValue("" + i, "" + i));
        this.year = (Collection) arrayList;

        return this.year;
    }

    public void setStartYear(int startYear)
    {
    	this.startYear = startYear;
    }

     public Collection getHour()
    {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i <= 9; i++)
        {
            arrayList.add(new KeyValue("0" + i, "0" + i));
        }
        for (int i = 10; i <= 23; i++)
        {
            arrayList.add(new KeyValue("" + i, "" + i));
        }
        this.hour = (Collection) arrayList;

        return this.hour;
    }

    public Collection getMinute()
    {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i <= 9; i++)
        {
            arrayList.add(new KeyValue("0" + i, "0" + i));
        }
        for (int i = 10; i <= 59; i++)
        {
            arrayList.add(new KeyValue("" + i, "" + i));
        }
        this.minute = (Collection) arrayList;

        return this.minute;
    }

    public Collection getSecond()
    {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i <= 9; i++)
        {
            arrayList.add(new KeyValue("0" + i, "0" + i));
        }
        for (int i = 10; i <= 59; i++)
        {
            arrayList.add(new KeyValue("" + i, "" + i));
        }
        this.second = (Collection) arrayList;

        return this.second;
    }

    public Collection getDate() {
        ArrayList arraylist = new ArrayList();
        for(int i = 1; i <= 9; i++)
            arraylist.add(new KeyValue("0" + i, "" + i));

        for(int j = 10; j <= 31; j++)
            arraylist.add(new KeyValue("" + j, "" + j));

        date = arraylist;
        arraylist = new ArrayList();
        arraylist.add(new KeyValue("Jan", "01"));
        arraylist.add(new KeyValue("Feb", "02"));
        arraylist.add(new KeyValue("Mar", "03"));
        arraylist.add(new KeyValue("Apr", "04"));
        arraylist.add(new KeyValue("May", "05"));
        arraylist.add(new KeyValue("Jun", "06"));
        arraylist.add(new KeyValue("Jul", "07"));
        arraylist.add(new KeyValue("Aug", "08"));
        arraylist.add(new KeyValue("Sep", "09"));
        arraylist.add(new KeyValue("Oct", "10"));
        arraylist.add(new KeyValue("Nov", "11"));
        arraylist.add(new KeyValue("Dec", "12"));

        return date;
    }


}