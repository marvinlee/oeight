/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// AppContext.java

package com.esmart2u.solution.base.helper;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;
import java.util.Observable;

public class AppContext extends Observable
    implements Serializable
{
    private Map map;

    public AppContext()
    {
        map = new Hashtable();
    }

    public void put(Object key, Object value)
    {
        map.put(key, value);
        notifyObservers();
    }

    public Object get(Object key)
    {
        return map.get(key);
    }

    public Object remove(Object key)
    {
        Object object = map.remove(key);
        notifyObservers();
        return object;
    }
}

// end of AppContext.java