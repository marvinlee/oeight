/**
 * � 2001 - 2002 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// AppConstants.java

package com.esmart2u.solution.base.helper;

public interface AppConstants
{
    public static final String SECURITY_SESSION_EVENT = "securitySessionEvent";
    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";
    public static final String APP_CODE = "applCd";
    public static final String WORKSTATION_IP = "workStationIP";
    public static final String SESSION_ID = "sessionId";
    public static final String BRANCH_CODE = "branchCode";
    public static final String BRANCH_NAME = "branchName";
    public static final String APPLICATION_ROLE_LIST = "applicationRoleList";
    public static final String APP_CONTEXT = "appContext";
    public static final String MODULE = "module";
    public static final String FUNCTION = "function";
}

// end of AppConstants.java