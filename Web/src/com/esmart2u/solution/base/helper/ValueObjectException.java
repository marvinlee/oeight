/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ValueObjectException.java

package com.esmart2u.solution.base.helper;

/**
 * Value object exception.
 *
 * @author  Chee Weng Keong
 * @version $Id: ValueObjectException.java,v 1.1 2003/12/05 13:32:47 wkchee Exp $
 */

public class ValueObjectException extends Exception
{
    /**
     * Creates new <code>ValueObjectException</code> without detail message.
     */
    public ValueObjectException()
    {
    }

    /**
     * Constructs an <code>ValueObjectException</code> with the specified detail message.
     *
     * @param message The detail message.
     */
    public ValueObjectException(String message)
    {
        super(message);
    }
}

// end of ValueObjectException.java