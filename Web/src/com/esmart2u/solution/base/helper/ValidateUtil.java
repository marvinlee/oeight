/*
 * ValidateUtil.java
 *
 * Created on 24 September 2007, 00:46
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper;
import java.io.PrintStream;
import java.text.*;
import java.util.*;
import org.apache.oro.text.regex.*;

// Referenced classes of package com.integrosys.base.techinfra.validation:
//            ValidatorConstant

class ValidateUtil
    implements ValidatorConstant
{

    ValidateUtil()
    {
        locale = new Locale("en", "SG");
    }

    static boolean isEmptyString(String aString)
    {
        if(aString != null)
            aString = aString.trim();
        return aString == null || aString.equals("");
    }

    static String checkMandatory(String aString, boolean chk)
    {
        if(isEmptyString(aString))
        {
            if(chk)
                return "mandatory";
            else
                return "noerror";
        } else
        {
            return null;
        }
    }

    static String checkMandatoryTextBox(String aString, boolean chk)
    {
        if(isEmptyTextBox(aString))
        {
            if(chk)
                return "mandatory";
            else
                return "noerror";
        } else
        {
            return null;
        }
    }

    static boolean isEmptyTextBox(String aString)
    {
        return aString == null || aString.equals("");
    }

    static String validateNumericString(String iInteger, boolean chk, int min, int max)
    {
        String ss = null;
        try
        {
            if(iInteger != null)
                iInteger = iInteger.trim();
            if((ss = checkMandatory(iInteger, chk)) == null)
                ss = chkNumericString(iInteger, min, max);
        }
        catch(Exception e)
        {
            ss = "format";
        }
        return ss;
    }

    private static String chkNumericString(String iInteger, int min, int max)
    {
        String methodResult = "noerror";
        try
        {
            input = new PatternMatcherInput(iInteger);
            pattern = compiler.compile("[^0-9]");
            if(matcher.contains(input, pattern))
            {
                //DefaultLogger.debug("ValidateUtil", input + ": input conatins the pattern otherthan 0-9,','");
                methodResult = "format";
            }
        }
        catch(MalformedPatternException e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            methodResult = "format";
        }
        catch(NumberFormatException e)
        {
            methodResult = "format";
        }
        catch(Exception e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            methodResult = "format";
        }
        int len = iInteger.length();
        if(len < min)
            methodResult = "lessthan";
        if(len > max)
            methodResult = "greaterthan";
        return methodResult;
    }

    static String validateInteger(String iInteger, boolean chk, int min, int max)
    {
        String ss = null;
        try
        {
            if(iInteger != null)
                iInteger = iInteger.trim();
            if((ss = checkMandatory(iInteger, chk)) == null)
                ss = chkInteger(iInteger, min, max);
        }
        catch(Exception e)
        {
            ss = "format";
        }
        return ss;
    }

    private static String chkInteger(String iInteger, int min, int max)
    {
        String methodResult = "noerror";
        String sInteger = "";
        try
        {
            input = new PatternMatcherInput(iInteger);
            pattern = compiler.compile("[^0-9,-]");
            if(matcher.contains(input, pattern))
            {
                //DefaultLogger.debug("ValidateUtil", input + ": input contains the pattern otherthan 0-9,',','-'");
                methodResult = "format";
            } else
            {
                for(st = new StringTokenizer(iInteger, ",", false); st.hasMoreTokens();)
                    sInteger = sInteger + st.nextToken();

                int iIntVal = (new Integer(sInteger.trim())).intValue();
                if(iIntVal < min)
                    methodResult = "lessthan";
                if(iIntVal > max)
                    methodResult = "greaterthan";
            }
        }
        catch(MalformedPatternException e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            methodResult = "format";
        }
        catch(NumberFormatException e)
        {
            methodResult = "format";
        }
        catch(Exception e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            methodResult = "format";
        }
        return methodResult;
    }

    static String validateNumber(String iNumber, boolean chk, double min, double max)
    {
        String ss = null;
        try
        {
            if(iNumber != null)
                iNumber = iNumber.trim();
            if((ss = checkMandatory(iNumber, chk)) == null)
                ss = chkNumber(iNumber, min, max, 0x7fffffff);
        }
        catch(Exception e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            ss = "format";
        }
        return ss;
    }

    static String validateNumber(String iNumber, boolean chk, double min, double max, int decimalPlaces, Locale locale)
    {
        String ss = null;
        try
        {
            if(iNumber != null)
                iNumber = iNumber.trim();
            if((ss = checkMandatory(iNumber, chk)) == null)
                ss = chkNumber(iNumber, min, max, decimalPlaces);
        }
        catch(Exception e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            ss = "format";
        }
        return ss;
    }

    private static String chkNumber(String iNumber, double min, double max, int decimalPlaces)
    {
        String ss;
label0:
        {
            String sNumber = "";
            ss = "noerror";
            try
            {
                input = new PatternMatcherInput(iNumber);
                pattern = compiler.compile("[^-0-9,.]");
                if(matcher.contains(input, pattern))
                {
                    //DefaultLogger.debug("ValidateUtil", input + ": input contains pattern other than 0-9,',','.'");
                    ss = "format";
                    break label0;
                }
                int iIndex = iNumber.indexOf('.');
                if(iIndex != -1)
                {
                    if(iIndex != iNumber.lastIndexOf('.'))
                    {
                        ss = "format";
                        break label0;
                    }
                    if(iNumber.indexOf(',', iIndex) != -1)
                    {
                        ss = "format";
                        break label0;
                    }
                }
                if(iNumber.indexOf(',') == 0)
                {
                    ss = "format";
                    break label0;
                }
                for(st = new StringTokenizer(iNumber, ",", false); st.hasMoreTokens();)
                    sNumber = sNumber + st.nextToken();

                double iNumVal = (new Double(sNumber.trim())).doubleValue();
                int ii = sNumber.lastIndexOf('.');
                if(ii != -1)
                {
                    String fraction = sNumber.substring(ii);
                    if(fraction.length() > decimalPlaces)
                    {
                        ss = "decimalexceeded";
                        break label0;
                    }
                }
                if(iNumVal < min)
                    ss = "lessthan";
                if(iNumVal > max)
                    ss = "greaterthan";
            }
            catch(NumberFormatException e)
            {
                ss = "format";
            }
            catch(Exception e)
            {
                //DefaultLogger.error("ValidateUtil", "", e);
                ss = "format";
            }
        }
        return ss;
    }

    static String validateAmount(String iAmount, boolean chk, double min, double max, String currencyCode, Locale locale)
    {
        boolean methodResult = false;
        String ss = null;
        try
        {
            if(iAmount != null)
                iAmount = iAmount.trim();
            //todo
            /*if((ss = checkMandatory(iAmount, chk)) == null)
                ss = chkAmount(iAmount, min, max, currencyCode, locale);*/
        }
        catch(Exception e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            ss = "format";
        }
        return ss;
    }

    /*private static String chkAmount(String amt, double min, double max, String currencyCode, Locale locale)
        throws IllegalArgumentException
    {
        String ss;
label0:
        {
            ss = "noerror";
            if(min > max)
                throw new IllegalArgumentException("Parameter [min] must be <= [max]!");
            if(null == currencyCode)
                throw new IllegalArgumentException("CurrencyCode must not be null!");
            if(null == locale)
                throw new IllegalArgumentException("Locale must not be null!");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            char groupSeparator = symbols.getGroupingSeparator();
            char decSeparator = symbols.getDecimalSeparator();
            CurrencyFormatter formatter = null;
            try
            {
                formatter = CurrencyManager.getCurrencyFormatterFromString(currencyCode);
            }
            catch(ChainedException ce)
            {
                //DefaultLogger.error("ValidateUtil", "unable to find format string", ce);
                ss = "format";
                break label0;
            }
            String displayFormat = formatter.getInputFormatString(locale);
            String displayGroupSeparator = ",";
            String displayDecimalSeparator = ".";
            if(-1 == displayFormat.indexOf(displayDecimalSeparator))
            {
                if(amt.indexOf(decSeparator) != -1)
                {
                    ss = "decimalnotallowed";
                    break label0;
                }
            } else
            if(amt.indexOf(decSeparator) != -1)
            {
                int decimalPos = displayFormat.indexOf(displayDecimalSeparator);
                int decimalCount = displayFormat.length() - decimalPos - 1;
                if(amt.length() - amt.indexOf(decSeparator) - 1 > decimalCount)
                {
                    ss = "decimalexceeded";
                    break label0;
                }
            }
            if(-1 == displayFormat.indexOf(displayGroupSeparator) && amt.indexOf(groupSeparator) != -1)
            {
                ss = "format";
                break label0;
            }
            for(int i = 0; i < amt.length(); i++)
            {
                if(Character.isDigit(amt.charAt(i)) || amt.charAt(i) == groupSeparator || amt.charAt(i) == decSeparator || amt.charAt(i) == '-')
                    continue;
                ss = "format";
                break;
            }

            if(amt.indexOf(decSeparator) != -1)
            {
                for(int i = amt.indexOf(decSeparator) + 1; i < amt.length(); i++)
                {
                    if(Character.isDigit(amt.charAt(i)))
                        continue;
                    ss = "format";
                    break;
                }

            }
            String checkGroup = String.valueOf(groupSeparator) + String.valueOf(groupSeparator);
            String checkGroupDec = String.valueOf(groupSeparator) + String.valueOf(decSeparator);
            if(amt.indexOf(checkGroup) != -1)
            {
                ss = "format";
                break label0;
            }
            if(amt.indexOf(checkGroupDec) != -1)
            {
                ss = "format";
                break label0;
            }
            if(!Character.isDigit(amt.charAt(amt.length() - 1)))
            {
                ss = "format";
                break label0;
            }
            double amtDbl = 0.0D;
            NumberFormat nf = formatter.getDisplayFormat(locale);
            try
            {
                amtDbl = nf.parse(amt).doubleValue();
            }
            catch(ParseException e)
            {
                ss = "format";
                break label0;
            }
            if(amtDbl < min)
                ss = "lessthan";
            if(amtDbl > max)
                ss = "greaterthan";
        }
        return ss;
    }*/

    /**
     * @deprecated Method validateAmount is deprecated
     */

    static boolean validateAmount(String iAmount, boolean chk, double min, double max, Locale locale)
    {
        boolean methodResult;
        try
        {
            if(iAmount != null)
                iAmount = iAmount.trim();
            if(chk)
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("ValidateUtil", " input is either null or empty");
                    methodResult = false;
                }
                if(!chkAmount(iAmount, min, max, locale))
                    methodResult = false;
            } else
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("ValidateUtil", " input is either null or empty");
                    methodResult = true;
                }
                if(!chkAmount(iAmount, min, max, locale))
                    methodResult = false;
            }
            methodResult = true;
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }
        return methodResult;
    }

    /**
     * @deprecated Method chkAmount is deprecated
     */

    private static boolean chkAmount(String iAmount, double min, double max, Locale locale)
    {
        String sAmount = "";
        String pAmount = "";
        boolean methodResult;
        try
        {
            input = new PatternMatcherInput(iAmount);
            pattern = compiler.compile("[^-0-9,.]");
            if(matcher.contains(input, pattern))
            {
                //DefaultLogger.debug("ValidateUtil", input + ": input contains ILLEGAL pattern");
                return methodResult = false;
            }
            int iIndex = iAmount.indexOf('.');
            if(iIndex != -1)
            {
                if(iIndex != iAmount.lastIndexOf('.'))
                    return methodResult = false;
                if(iAmount.indexOf(',', iIndex) != -1)
                    return methodResult = false;
            }
            if(iAmount.indexOf(',') == 0)
                return methodResult = false;
            for(StringTokenizer st = new StringTokenizer(iAmount, ",", false); st.hasMoreTokens();)
                sAmount = sAmount + st.nextToken();

            int sIndex = sAmount.indexOf('.');
            if(sIndex != -1)
            {
                if(sIndex < 1 || sIndex > 12)
                    return methodResult = false;
                int i = sAmount.substring(sIndex, sAmount.length()).length();
                if(i > 4)
                    return methodResult = false;
            }
            double iAmountVal = (new Double(sAmount)).doubleValue();
            if(iAmountVal < min || iAmountVal > max)
            {
                //DefaultLogger.debug("ValidateUtil", "the input value is out of range  ");
                return methodResult = false;
            }
            pAmount = NumberFormat.getNumberInstance(locale).format(iAmountVal);
            pattern = compiler.compile(pAmount);
        }
        catch(MalformedPatternException e)
        {
            return methodResult = false;
        }
        catch(NumberFormatException e)
        {
            System.err.println("Bad pattern.");
            return methodResult = false;
        }
        return methodResult = true;
    }

    /**
     * @deprecated Method validateAmount is deprecated
     */

    static boolean validateAmount(String iAmount, boolean chk, String currencyCode)
    {
        return true;
    }

    /**
     * @deprecated Method validateAmount is deprecated
     */

    static boolean validateAmount(String iAmount, boolean chk, Locale locale)
    {
        boolean methodResult;
        try
        {
            if(iAmount != null)
                iAmount = iAmount.trim();
            if(chk)
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("ValidateUtil", " input is either null or empty");
                    return methodResult = false;
                }
                if(!chkAmount(iAmount, locale))
                    return methodResult = false;
            } else
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("ValidateUtil", " input is either null or empty");
                    return methodResult = true;
                }
                if(!chkAmount(iAmount, locale))
                    return methodResult = false;
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
            return false;
        }
        return methodResult = true;
    }

    /**
     * @deprecated Method chkAmount is deprecated
     */

    private static boolean chkAmount(String iAmount, Locale locale)
    {
        String sAmount = "";
        String pAmount = "";
        boolean methodResult;
        try
        {
            input = new PatternMatcherInput(iAmount);
            pattern = compiler.compile("[^-0-9,.]");
            if(matcher.contains(input, pattern))
            {
                //DefaultLogger.debug("ValidateUtil", input + ": input contains ILLEGAL pattern");
                return methodResult = false;
            }
            int iIndex = iAmount.indexOf('.');
            if(iIndex != -1)
            {
                if(iIndex != iAmount.lastIndexOf('.'))
                    return methodResult = false;
                if(iAmount.indexOf(',', iIndex) != -1)
                    return methodResult = false;
            }
            if(iAmount.indexOf(',') == 0)
                return methodResult = false;
            for(StringTokenizer st = new StringTokenizer(iAmount, ",", false); st.hasMoreTokens();)
                sAmount = sAmount + st.nextToken();

            int sIndex = sAmount.indexOf('.');
            if(sIndex != -1)
            {
                if(sIndex < 1 || sIndex > 12)
                    return methodResult = false;
                int i = sAmount.substring(sIndex, sAmount.length()).length();
                if(i > 4)
                    return methodResult = false;
            }
            double iAmountVal = (new Double(sAmount)).doubleValue();
            pAmount = NumberFormat.getNumberInstance(locale).format(iAmountVal);
            pattern = compiler.compile(pAmount);
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        return methodResult = true;
    }

    static String validateEmail(String iEmail, boolean chk)
    {
        String ss = null;
        try
        {
            if(iEmail != null)
                iEmail = iEmail.trim();
            if((ss = checkMandatory(iEmail, chk)) == null)
            {
                ss = "noerror";
                ss = chkEmail(iEmail);
            }
        }
        catch(Exception e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            ss = "format";
        }
        return ss;
    }

    private static String chkEmail(String iEmail)
    {
        String ss = "noerror";
        try
        {
            input = new PatternMatcherInput(iEmail);
            pattern = compiler.compile("@.*\\.");
            //DefaultLogger.debug("ValidateUtil", pattern.getPattern());
            if(matcher.contains(input, pattern))
                return ss;
            ss = "format";
        }
        catch(MalformedPatternException e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            ss = "format";
        }
        return ss;
    }

    static String validatePhoneNumber(String iPhNumber, boolean chk, Locale locale)
    {
        String ss = null;
        try
        {
            if(iPhNumber != null)
                iPhNumber = iPhNumber.trim();
            if((ss = checkMandatory(iPhNumber, chk)) == null)
            {
                ss = "noerror";
                ss = chkPhoneNumber(iPhNumber, locale);
            }
        }
        catch(Exception e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            ss = "noerror";
        }
        return ss;
    }

    private static String chkPhoneNumber(String iPhNumber, Locale locale)
    {
        String ss = "noerror";
        if(iPhNumber.length() > 20)
            ss = "greaterthan";
        else
        if(iPhNumber.length() < 1)
            ss = "lessthan";
        return ss;
    }

    static String validateString(String iString, boolean chk, int min, int max)
    {
        String ss = null;
        try
        {
            if(iString != null)
                iString = iString.trim();
            if((ss = checkMandatory(iString, chk)) == null)
            {
                ss = "noerror";
                ss = chkString(iString, min, max);
            }
        }
        catch(Exception ex)
        {
            //DefaultLogger.error("ValidateUtil", "", ex);
            ss = "format";
        }
        return ss;
    }

    private static String chkString(String iString, int min, int max)
    {
        String ss = "noerror";
        try
        {
            int len = iString.length();
            if(len < min)
                ss = "lessthan";
            if(len > max)
                ss = "greaterthan";
        }
        catch(Exception ex)
        {
            //DefaultLogger.error("ValidateUtil", "", ex);
            ss = "format";
        }
        return ss;
    }

    static String validateDate(String iDate, boolean chk, Locale locale)
    {
        String ss = null;
        try
        {
            if((ss = checkMandatory(iDate, chk)) == null)
            {
                ss = "noerror";
                ss = chkDate(iDate, locale);
            }
        }
        catch(Exception e)
        {
            //DefaultLogger.error("ValidateUtil", "", e);
            ss = "format";
        }
        return ss;
    }

    private static String chkDate(String iDate, Locale locale)
    {
        String ss;
label0:
        {
            ss = "noerror";
            int day = 0;
            int mth = 0;
            int yr = 0;
            try
            {
                StringTokenizer st = new StringTokenizer(iDate, "-/");
                if(st.countTokens() == 1)
                {
                    //DefaultLogger.debug("ValidateUtil", "Chk Space");
                    st = new StringTokenizer(iDate, " ");
                    if(st.countTokens() == 3)
                    {
                        day = Integer.parseInt(st.nextToken());
                        mth = convertMonth(st.nextToken());
                        if(mth == -1)
                        {
                            ss = "format";
                            break label0;
                        }
                        yr = Integer.parseInt(st.nextToken());
                    } else
                    {
                        ss = "format";
                        break label0;
                    }
                } else
                if(st.countTokens() == 3)
                {
                    day = Integer.parseInt(st.nextToken());
                    String tt = st.nextToken();
                    try
                    {
                        mth = Integer.parseInt(tt) - 1;
                    }
                    catch(NumberFormatException e)
                    {
                        mth = convertMonth(tt);
                        if(mth == -1)
                        {
                            ss = "format";
                            break label0;
                        }
                    }
                    yr = Integer.parseInt(st.nextToken());
                } else
                {
                    ss = "format";
                    break label0;
                }
                //DefaultLogger.debug("ValidateUtil", "Day\t:" + day);
                //DefaultLogger.debug("ValidateUtil", "Mth\t:" + (mth + 1));
                //DefaultLogger.debug("ValidateUtil", "Yr\t:" + yr);
                if(mth > -1 && mth < 13)
                {
                    GregorianCalendar cal = new GregorianCalendar(yr, mth, 1);
                    cal.setLenient(false);
                    if(day <= 0 || day > cal.getActualMaximum(5))
                        ss = "format";
                } else
                {
                    ss = "format";
                }
            }
            catch(Exception ex)
            {
                //DefaultLogger.error("ValidateUtil", "", ex);
                ss = "format";
            }
        }
        return ss;
    }

    private static boolean chkDate(String iDate)
    {
        int day = 0;
        int mth = 0;
        int yr = 0;
        try
        {
            StringTokenizer st = new StringTokenizer(iDate, "-/");
            if(st.countTokens() == 1)
            {
                //DefaultLogger.debug("ValidateUtil", "Chk Space");
                st = new StringTokenizer(iDate, " ");
                if(st.countTokens() == 3)
                {
                    day = Integer.parseInt(st.nextToken());
                    mth = convertMonth(st.nextToken());
                    if(mth == -1)
                        return false;
                    yr = Integer.parseInt(st.nextToken());
                } else
                {
                    return false;
                }
            } else
            if(st.countTokens() == 3)
            {
                day = Integer.parseInt(st.nextToken());
                mth = Integer.parseInt(st.nextToken()) - 1;
                yr = Integer.parseInt(st.nextToken());
            } else
            {
                return false;
            }
            //DefaultLogger.debug("ValidateUtil", "Day\t:" + day);
            //DefaultLogger.debug("ValidateUtil", "Mth\t:" + (mth + 1));
            //DefaultLogger.debug("ValidateUtil", "Yr\t:" + yr);
            if(mth > -1 && mth < 13)
            {
                GregorianCalendar cal = new GregorianCalendar(yr, mth, 1);
                cal.setLenient(false);
                return day > 0 && day <= cal.getActualMaximum(5);
            } else
            {
                return false;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    private static Date convertDate(String input)
        throws Exception
    {
        int day = 0;
        int mth = 0;
        int yr = 0;
        String dateFormat = "dd MM yyyy";
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        StringTokenizer st = new StringTokenizer(input, "-/");
        if(st.countTokens() == 1)
        {
            //DefaultLogger.debug("ValidateUtil", "Chk Space");
            st = new StringTokenizer(input, " ");
            if(st.countTokens() == 3)
            {
                day = Integer.parseInt(st.nextToken());
                mth = convertMonth(st.nextToken());
                if(mth == -1)
                    throw new Exception();
                yr = Integer.parseInt(st.nextToken());
            } else
            {
                throw new Exception();
            }
        } else
        if(st.countTokens() == 3)
        {
            day = Integer.parseInt(st.nextToken());
            mth = Integer.parseInt(st.nextToken()) - 1;
            yr = Integer.parseInt(st.nextToken());
        } else
        {
            throw new Exception();
        }
        //DefaultLogger.debug("ValidateUtil", "Day\t:" + day);
        //DefaultLogger.debug("ValidateUtil", "Mth\t:" + (mth + 1));
        //DefaultLogger.debug("ValidateUtil", "Yr\t:" + yr);
        if(mth > -1 && mth < 13)
        {
            GregorianCalendar cal = new GregorianCalendar(yr, mth, 1);
            cal.setLenient(false);
            if(day > 0 && day <= cal.getActualMaximum(5))
            {
                return format.parse(day + " " + mth + " " + yr);
            } else
            {
                //DefaultLogger.debug("ValidateUtil", "Error in the Month");
                throw new Exception();
            }
        } else
        {
            //DefaultLogger.debug("ValidateUtil", "Error in the Month");
            throw new Exception();
        }
    }

    static int compareDate(String input, boolean chk, String comparedDate)
        throws Exception
    {
        try
        {
            if(chk)
                if(input == null || input.equals(""))
                {
                    //DefaultLogger.debug("ValidateUtil", "Date is null or empty");
                    throw new Exception("Date is null or empty");
                } else
                {
                    return comparingDate(input, comparedDate);
                }
            if(input == null || input.equals(""))
            {
                //DefaultLogger.debug("ValidateUtil", "Date is null or empty");
                return 0;
            } else
            {
                return comparingDate(input, comparedDate);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    private static int comparingDate(String input, String comparedDate)
        throws Exception
    {
        Date inputDate = convertDate(input);
        Date compDate = convertDate(comparedDate);
        return inputDate.compareTo(compDate);
    }

    static int compareDate(String input, boolean chk, Date comparedDate)
        throws Exception
    {
        try
        {
            if(chk)
                if(input == null || input.equals(""))
                {
                    //DefaultLogger.debug("ValidateUtil", "Date is null or empty");
                    throw new Exception("Date is null or empty");
                } else
                {
                    return comparingDate(input, comparedDate);
                }
            if(input == null || input.equals(""))
            {
                //DefaultLogger.debug("ValidateUtil", "Date is null or empty");
                return 0;
            } else
            {
                return comparingDate(input, comparedDate);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    private static int comparingDate(String input, Date compDate)
        throws Exception
    {
        Date inputDate = convertDate(input);
        return inputDate.compareTo(compDate);
    }

    static String validateTextBox(String input, boolean chk, int minLength, int maxLength, int minHeight, int maxHeight)
    {
        String ss = null;
        try
        {
            if((ss = checkMandatoryTextBox(input, chk)) == null)
            {
                ss = "noerror";
                ss = chkTextBox(input, minLength, maxLength, minHeight, maxHeight);
            }
        }
        catch(Exception ex)
        {
            //DefaultLogger.error("ValidateUtil", "", ex);
            ss = "format";
        }
        return ss;
    }

    private static String chkTextBox(String input, int minLength, int maxLength, int minHeight, int maxHeight)
    {
        String ss = "noerror";
        int numCol = 0;
        try
        {
            //DefaultLogger.debug("ValidateUtil", "Validate TextBox Data");
            for(StringTokenizer tokenString = new StringTokenizer(input, "\r"); tokenString.hasMoreTokens();)
            {
                String textBoxString = tokenString.nextToken();
                numCol++;
            }

            if(numCol > 0)
                numCol--;
            //DefaultLogger.debug("", "Number of <CR> :" + numCol);
            ss = chkString(input, minLength, maxLength);
            if(ss.equals("noerror"))
            {
                if(numCol < minHeight - 1)
                    ss = "heightlessthan";
                if(numCol > maxHeight - 1)
                    ss = "heightgreaterthan";
            }
        }
        catch(Exception ex)
        {
            //DefaultLogger.error("ValidateUtil", "", ex);
            ss = "format";
        }
        return ss;
    }

    public static String validateSwiftCode(String input, boolean chk)
    {
        String ss = null;
        if(input != null)
            input = input.trim();
        try
        {
            if((ss = checkMandatory(input, chk)) == null)
                ss = chkSwiftCode(input);
        }
        catch(Exception ex)
        {
            //DefaultLogger.error("ValidateUtil", "", ex);
            ss = "format";
        }
        return ss;
    }

    public static boolean checkPattern(String target, String myPattern)
    {
        //DefaultLogger.debug("ValidateUtil", "<ValidateUtil> Target:" + target + ":Patter:" + myPattern);
        if(target == null || myPattern == null)
            return false;
        try
        {
            input = new PatternMatcherInput(target);
            pattern = compiler.compile(myPattern);
            //DefaultLogger.debug("ValidateUtil", "<ValidateUtil> Pattern:" + pattern + ":Input:" + input);
            if(matcher.matches(input, pattern))
            {
                //DefaultLogger.debug("ValidateUtil", "<ValidateUtil>" + input + ": input conatains the pattern " + pattern);
                return true;
            }
            //DefaultLogger.debug("ValidateUtil", "<ValidateUtil> Does match");
        }
        catch(MalformedPatternException mfpe)
        {
            //DefaultLogger.debug("ValidateUtil", "<ValidateUtil>: Error in forming Patter" + mfpe.getMessage());
            return false;
        }
        return false;
    }

    private static String chkSwiftCode(String inputString)
    {
        String ss = "noerror";
        String input = inputString.trim();
        char characters[] = input.toCharArray();
        for(int i = 0; i < characters.length; i++)
        {
            int v = characters[i];
            if(v >= 39 && v <= 41 || v >= 43 && v <= 58 || v == 63 || v >= 65 && v <= 90 || v >= 97 && v <= 122 || v == 32 || v == 10 || v == 13)
            {
                ss = "noerror";
                continue;
            }
            ss = "nonswift";
            break;
        }

        return ss;
    }

    private static int convertMonth(String mth)
    {
        if(mth.equalsIgnoreCase("jan"))
            return 0;
        if(mth.equalsIgnoreCase("feb"))
            return 1;
        if(mth.equalsIgnoreCase("mar"))
            return 2;
        if(mth.equalsIgnoreCase("apr"))
            return 3;
        if(mth.equalsIgnoreCase("may"))
            return 4;
        if(mth.equalsIgnoreCase("jun"))
            return 5;
        if(mth.equalsIgnoreCase("jul"))
            return 6;
        if(mth.equalsIgnoreCase("AUG"))
            return 7;
        if(mth.equalsIgnoreCase("SEP"))
            return 8;
        if(mth.equalsIgnoreCase("oct"))
            return 9;
        if(mth.equalsIgnoreCase("nov"))
            return 10;
        return !mth.equalsIgnoreCase("dec") ? -1 : 11;
    }

    private static final String xEmail = "@.*\\.";
    private static final String xNumber = "[^-0-9,.]";
    private static final String xNumeric = "[^0-9]";
    private static final String xInteger = "[^0-9,-]";
    private static final String xAmount = "[^-0-9,.]";
    private static final String xPostCode = "";
    private static final String xString = "";
    private static final String xDate = "";
    private static final String xPhNumber = "[^0-9-]";
    private static final String xRegEx = "";
    private static Pattern pattern = null;
    private static PatternMatcherInput input;
    private static PatternCompiler compiler = new Perl5Compiler();
    private static PatternMatcher matcher = new Perl5Matcher();
    private static MatchResult result;
    private static StringTokenizer st;
    private static ResourceBundle resources;
    private String PhDigits[];
    Locale locale;
 

}
