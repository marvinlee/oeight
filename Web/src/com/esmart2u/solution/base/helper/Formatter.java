/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// Formatter.java

package com.esmart2u.solution.base.helper;

import java.sql.Date;
import java.util.Locale;
import java.math.BigDecimal;
import java.math.BigInteger;

import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.DecimalFormat;

/**
 * Formatter.
 *
 * @author  Chee Weng Keong
 * @version $Id: Formatter.java,v 1.8 2004/02/19 06:56:25 wkchee Exp $
 */

public class Formatter
{
    // ----- Date Formatting -----

    /**
     * Formats date using default locale and pattern.
     *
     * @param date Date.
     * @return Formatted date.
     */
    public static String formatDate(Date date)
    {
        return Formatter.formatDate(date, Locale.getDefault());
    }

    /**
     * Formats date to user defined locale.
     *
     * @param date Date.
     * @param locale Locale.
     * @return Formatted date.
     */
    public static String formatDate(Date date, Locale locale)
    {
        return Formatter.formatDate(date, locale, BaseConstants.DATE_FORMAT);
    }

    /**
     * Formats date to user defined pattern.
     *
     * @param date Date.
     * @param pattern Pattern.
     * @return Formatted date.
     */
    public static String formatDate(Date date, String pattern)
    {
        return Formatter.formatDate(date, Locale.getDefault(), pattern);
    }

    /**
     * Formats date to user defined locale and pattern.
     *
     * @param date Date.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted date.
     */
    public static String formatDate(Date date, Locale locale, String pattern)
    {
        if (date == null)
        {
            return "";
        }
        if (locale == null)
        {
            locale = Locale.getDefault();
        }

        if (StringUtils.hasValue(pattern))
        {
            // to setup formatter using calendar and locale
            com.ibm.icu.text.SimpleDateFormat formatter =
                new com.ibm.icu.text.SimpleDateFormat(pattern, locale);

            // special handling of locale
            if (locale.getLanguage().equals(LocaleConstants.THAILAND.getLanguage()))
            {
                // only add for ICU, can be removed when upgrade to JDK 1.4.1
                formatter.setCalendar(new com.ibm.icu.util.BuddhistCalendar());
            }

            return formatter.format(date);
        }
        else
        {
            com.ibm.icu.text.DateFormat formatter =
                com.ibm.icu.text.DateFormat.getDateInstance(
                    com.ibm.icu.text.DateFormat.SHORT, locale);

            // special handling of locale
            if (locale.getLanguage().equals(LocaleConstants.THAILAND.getLanguage()))
            {
                // only add for ICU, can be removed when upgrade to JDK 1.4.1
                formatter.setCalendar(new com.ibm.icu.util.BuddhistCalendar());
            }

            return formatter.format(date);
        }
    }

    // ----- Rate Formatting -----

    /**
     * Formats rate.
     *
     * @param rate Rate.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted rate.
     * @deprecated Use formatRate(BigDecimal rate, boolean isGroupingUsed)
     */
    public static String formatRate(double rate, boolean isGroupingUsed)
    {
        return Formatter.formatRate(new BigDecimal(rate), isGroupingUsed, Locale.getDefault(), BaseConstants.RATE_FORMAT);
    }

    /**
     * Formats rate based on locale.
     *
     * @param rate Rate.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted rate.
     * @deprecated Use formatRate(BigDecimal rate, boolean isGroupingUsed, Locale locale)
     */
    public static String formatRate(double rate, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatRate(new BigDecimal(rate), isGroupingUsed, locale, BaseConstants.RATE_FORMAT);
    }

    /**
     * Formats rate based on locale and user-defined pattern.
     *
     * @param rate Rate.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted rate.
     * @deprecated Use formatRate(BigDecimal rate, boolean isGroupingUsed, Locale locale, String pattern)
     */
    public static String formatRate(double rate, boolean isGroupingUsed, Locale locale, String pattern)
    {
        return Formatter.formatRate(new BigDecimal(rate), isGroupingUsed, locale, pattern);
    }

    /**
     * Formats rate.
     *
     * @param rate Rate.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted rate.
     */
    public static String formatRate(BigDecimal rate, boolean isGroupingUsed)
    {
        return Formatter.formatRate(rate, isGroupingUsed, Locale.getDefault(), BaseConstants.RATE_FORMAT);
    }

    /**
     * Formats rate based on locale.
     *
     * @param rate Rate.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted rate.
     */
    public static String formatRate(BigDecimal rate, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatRate(rate, isGroupingUsed, locale, BaseConstants.RATE_FORMAT);
    }

    /**
     * Formats rate based on locale and user-defined pattern.
     *
     * @param rate Rate.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted rate.
     */
    public static String formatRate(BigDecimal rate, boolean isGroupingUsed, Locale locale, String pattern)
    {
        return Formatter.formatDecimal(rate, isGroupingUsed, locale, pattern);
    }

    /**
     * Formats rate in string.
     *
     * @param rate Rate string.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted rate.
     */
    public static String formatRate(String rate, boolean isGroupingUsed)
    {
        return Formatter.formatRate(rate, isGroupingUsed, Locale.getDefault(), BaseConstants.RATE_FORMAT);
    }

    /**
     * Formats rate in string based on locale.
     *
     * @param rate Rate string.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted rate.
     */
    public static String formatRate(String rate, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatRate(rate, isGroupingUsed, locale, BaseConstants.RATE_FORMAT);
    }

    /**
     * Formats rate in string based on locale and user-defined pattern.
     *
     * @param rate Rate string.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted rate.
     */
    public static String formatRate(String rate, boolean isGroupingUsed, Locale locale, String pattern)
    {
        try
        {
            return formatRate(new BigDecimal(rate), isGroupingUsed, locale, pattern);
        }
        catch (Exception e)
        {
            return "";
        }
    }

    // ----- Money Formatting -----

    /**
     * Formats money.
     *
     * @param money Money.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted money.
     * @deprecated Use formatMoney(BigDecimal money, boolean isGroupingUsed)
     */
    public static String formatMoney(double money, boolean isGroupingUsed)
    {
        return Formatter.formatMoney(new BigDecimal(money), isGroupingUsed, Locale.getDefault(), BaseConstants.MONEY_FORMAT);
    }

    /**
     * Formats money based on locale.
     *
     * @param money Money.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted money.
     * @deprecated Use formatMoney(BigDecimal money, boolean isGroupingUsed, Locale locale)
     */
    public static String formatMoney(double money, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatMoney(new BigDecimal(money), isGroupingUsed, locale, BaseConstants.MONEY_FORMAT);
    }

    /**
     * Formats money based on locale and user-defined pattern.
     *
     * @param money Money.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted money.
     * @deprecated Use formatMoney(BigDecimal money, boolean isGroupingUsed, Locale locale, String pattern)
     */
    public static String formatMoney(double money, boolean isGroupingUsed, Locale locale, String pattern)
    {
        return Formatter.formatMoney(new BigDecimal(money), isGroupingUsed, locale, pattern);
    }

    /**
     * Formats money.
     *
     * @param money Money.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted money.
     */
    public static String formatMoney(BigDecimal money, boolean isGroupingUsed)
    {
        return Formatter.formatMoney(money, isGroupingUsed, Locale.getDefault(), BaseConstants.MONEY_FORMAT);
    }

    /**
     * Formats money based on locale.
     *
     * @param money Money.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted money.
     */
    public static String formatMoney(BigDecimal money, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatMoney(money, isGroupingUsed, locale, BaseConstants.MONEY_FORMAT);
    }

    /**
     * Formats money based on locale and user-defined pattern.
     *
     * @param money Money.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted money.
     */
    public static String formatMoney(BigDecimal money, boolean isGroupingUsed, Locale locale, String pattern)
    {
        return Formatter.formatDecimal(money, isGroupingUsed, locale, pattern);
    }

    /**
     * Formats money in string.
     *
     * @param money Money string.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted money.
     */
    public static String formatMoney(String money, boolean isGroupingUsed)
    {
        return Formatter.formatMoney(money, isGroupingUsed, Locale.getDefault(), BaseConstants.MONEY_FORMAT);
    }

    /**
     * Formats money in string based on locale.
     *
     * @param money Money string.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted money.
     */
    public static String formatMoney(String money, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatMoney(money, isGroupingUsed, locale, BaseConstants.MONEY_FORMAT);
    }

    /**
     * Formats money in string based on locale and user-defined pattern.
     *
     * @param money Money string.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted money.
     */
    public static String formatMoney(String money, boolean isGroupingUsed, Locale locale, String pattern)
    {
        try
        {
            return formatMoney(new BigDecimal(money), isGroupingUsed, locale, pattern);
        }
        catch (Exception e)
        {
            return "";
        }
    }

    // ----- Integer Formatting -----

    /**
     * Formats integer.
     *
     * @param intValue Integer value.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted integer value.
     */
    public static String formatInteger(int intValue, boolean isGroupingUsed)
    {
        return Formatter.formatInteger(new BigInteger(String.valueOf(intValue)), isGroupingUsed);
    }

    /**
     * Formats integer based on locale.
     *
     * @param intValue Integer value.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted integer value.
     */
    public static String formatInteger(int intValue, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatInteger(new BigInteger(String.valueOf(intValue)), isGroupingUsed, locale);
    }

    /**
     * Formats integer based on locale and user-defined pattern.
     *
     * @param intValue Integer value.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted integer value.
     */
    public static String formatInteger(int intValue, boolean isGroupingUsed, Locale locale, String pattern)
    {
        return Formatter.formatInteger(new BigInteger(String.valueOf(intValue)), isGroupingUsed, locale, pattern);
    }

    /**
     * Formats BigInteger.
     *
     * @param intValue BigInteger value.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted BigInteger value.
     */
    public static String formatInteger(BigInteger intValue, boolean isGroupingUsed)
    {
        return Formatter.formatInteger(intValue, isGroupingUsed, Locale.getDefault(), BaseConstants.INTEGER_FORMAT);
    }

    /**
     * Formats BigInteger based on locale.
     *
     * @param intValue BigInteger value.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted BigInteger value.
     */
    public static String formatInteger(BigInteger intValue, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatInteger(intValue, isGroupingUsed, locale, BaseConstants.INTEGER_FORMAT);
    }

    /**
     * Formats BigInteger based on locale and user-defined pattern.
     *
     * @param intValue BigInteger value.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted BigInteger value.
     */
    public static String formatInteger(BigInteger intValue, boolean isGroupingUsed, Locale locale, String pattern)
    {
        if (intValue == null)
        {
            return "";
        }

        if (StringUtils.hasValue(pattern))
        {
            DecimalFormat formatter = new DecimalFormat(pattern);
            formatter.setGroupingUsed(isGroupingUsed);
            return formatter.format(intValue);
        }
        else
        {
            NumberFormat formatter = NumberFormat.getNumberInstance(locale);
            formatter.setGroupingUsed(isGroupingUsed);
            return formatter.format(intValue);
        }
    }

    // ----- Percentage Formatting -----

    /**
     * Formats percentage.
     *
     * @param percentage Percentage.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted percentage.
     */
    public static String formatPercentage(double percentage, boolean isGroupingUsed)
    {
        return Formatter.formatPercentage(new BigDecimal(percentage), isGroupingUsed);
    }

    /**
     * Formats percentage based on locale.
     *
     * @param percentage Percentage.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted percentage.
     */
    public static String formatPercentage(double percentage, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatPercentage(new BigDecimal(percentage), isGroupingUsed, locale);
    }

    /**
     * Formats percentage based on locale and user-defined pattern.
     *
     * @param percentage Percentage.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted percentage.
     */
    public static String formatPercentage(double percentage, boolean isGroupingUsed, Locale locale, String pattern)
    {
        return Formatter.formatPercentage(new BigDecimal(percentage), isGroupingUsed, locale, pattern);
    }

    /**
     * Formats percentage.
     *
     * @param percentage Percentage.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted percentage.
     */
    public static String formatPercentage(BigDecimal percentage, boolean isGroupingUsed)
    {
        return Formatter.formatPercentage(percentage, isGroupingUsed, Locale.getDefault(), BaseConstants.PERCENTAGE_FORMAT);
    }

    /**
     * Formats percentage based on locale.
     *
     * @param percentage Percentage.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted percentage.
     */
    public static String formatPercentage(BigDecimal percentage, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatPercentage(percentage, isGroupingUsed, locale, BaseConstants.PERCENTAGE_FORMAT);
    }

    /**
     * Formats percentage based on locale and user-defined pattern.
     *
     * @param percentage Percentage.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted percentage.
     */
    public static String formatPercentage(BigDecimal percentage, boolean isGroupingUsed, Locale locale, String pattern)
    {
        return Formatter.formatDecimal(percentage, isGroupingUsed, locale, pattern);
    }

    /**
     * Formats percentage in string.
     *
     * @param percentage Percentage string.
     * @param isGroupingUsed Grouping flag.
     * @return Formatted percentage.
     */
    public static String formatPercentage(String percentage, boolean isGroupingUsed)
    {
        return Formatter.formatPercentage(percentage, isGroupingUsed, Locale.getDefault(), BaseConstants.PERCENTAGE_FORMAT);
    }

    /**
     * Formats percentage in string based on locale.
     *
     * @param percentage Percentage string.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @return Formatted percentage.
     */
    public static String formatPercentage(String percentage, boolean isGroupingUsed, Locale locale)
    {
        return Formatter.formatPercentage(percentage, isGroupingUsed, locale, BaseConstants.PERCENTAGE_FORMAT);
    }

    /**
     * Formats percentage in string based on locale and user-defined pattern.
     *
     * @param percentage Percentage string.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted percentage.
     */
    public static String formatPercentage(String percentage, boolean isGroupingUsed, Locale locale, String pattern)
    {
        try
        {
            return formatPercentage(new BigDecimal(percentage), isGroupingUsed, locale, pattern);
        }
        catch (Exception e)
        {
            return "";
        }
    }

    // ----- Decimal Formatting -----

    /**
     * Formats BigDecimal based on locale and user-defined pattern.
     *
     * @param decimalValue BigDecimal value.
     * @param isGroupingUsed Grouping flag.
     * @param locale Locale.
     * @param pattern Pattern.
     * @return Formatted BigDecimal value.
     */
    public static String formatDecimal(BigDecimal decimalValue, boolean isGroupingUsed, Locale locale, String pattern)
    {
        if (decimalValue == null)
        {
            return "";
        }

        if (StringUtils.hasValue(pattern))
        {
            DecimalFormat formatter = new DecimalFormat(pattern);
            NumberFormat numberFormatter = NumberFormat.getNumberInstance(locale);
            numberFormatter.setMaximumFractionDigits(formatter.getMaximumFractionDigits());
            numberFormatter.setMaximumIntegerDigits(formatter.getMaximumIntegerDigits());
            numberFormatter.setMinimumFractionDigits(formatter.getMinimumFractionDigits());
            numberFormatter.setMinimumIntegerDigits(formatter.getMinimumIntegerDigits());
            numberFormatter.setGroupingUsed(isGroupingUsed);
            return numberFormatter.format(decimalValue);
        }
        else
        {
            NumberFormat formatter = NumberFormat.getNumberInstance(locale);
            formatter.setGroupingUsed(isGroupingUsed);
            return formatter.format(decimalValue);
        }
    }
}

// end of Formatter.java