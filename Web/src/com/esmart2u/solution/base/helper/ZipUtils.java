/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ZipUtils.java

package com.esmart2u.solution.base.helper;

import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;

public class ZipUtils
{
    public static long actualSize;
    public static long compressedSize;

    public static double getCompressedRatio()
    {
        return ((double)compressedSize / actualSize * 100.00);
    }

    public static void zip(List filesToZipList, String outputZipFilename)
        throws Exception
    {
        int intAlFilesToZipSize = filesToZipList.size();

        Calendar calendar = new GregorianCalendar();
        java.util.Date date = calendar.getTime();

        ZipOutputStream zop = new ZipOutputStream(new FileOutputStream(new File(outputZipFilename)));
        zop.setMethod(ZipOutputStream.DEFLATED);

        for (int i = 0; i < intAlFilesToZipSize; i++)
        {

            File fileInput = new File((String)filesToZipList.get(i));
            FileInputStream fileinputstream = new FileInputStream(fileInput);

            byte[] rgb = new byte[1000];

            int n;
            // Create a zip entry.
            ZipEntry zipentry = new ZipEntry(fileInput.getName());

            zipentry.setSize(fileInput.length());
            zipentry.setTime(date.getTime());

            // Add the zip entry and associated data.
            zop.putNextEntry(zipentry);

            while ((n = fileinputstream.read(rgb)) > -1)
            {
                zop.write(rgb, 0, n);
            }

            zop.closeEntry();

            actualSize = zipentry.getSize();
            compressedSize = zipentry.getCompressedSize();

            zop.flush();

            zipentry = null;
            //crc32 = null;

        } // end of for

        zop.close();
        zop = null;

        date = null;
        calendar = null;
    }

    public static void unzip(String outputFolder, String inputZipFilename)
        throws Exception
    {
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(inputZipFilename)));
        ZipInputStream zis = new ZipInputStream(bis);

        ZipEntry ze = null;

        String strEntryName = null;

        while ((ze = zis.getNextEntry()) != null)
        {
            if (ze.isDirectory())
            {
                continue;
            }

            int size = 1000;
            strEntryName = ze.getName();

            File fileOutput = new File(outputFolder + "/" + strEntryName);
            FileOutputStream fos = new FileOutputStream(fileOutput);

            byte[] b = new byte[size];

            int chunk = 0;

            while (true)
            {
                chunk = zis.read(b);
                if (chunk == -1)
                {
                    break;
                }
                fos.write(b, 0, chunk);
            }

            fos.flush();
            fos.close();

        }

        ze = null;
        zis.close();
        zis = null;
        bis.close();
        bis = null;
    }
}

// end of ZipUtils.java