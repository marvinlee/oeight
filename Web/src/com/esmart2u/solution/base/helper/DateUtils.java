/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DateUtils.java

package com.esmart2u.solution.base.helper;

import java.sql.Date;
import java.util.Calendar;
import java.util.Locale;
import java.text.DateFormatSymbols;

import com.esmart2u.solution.base.logging.Logger;

/**
 * Date utilities.
 *
 * @author Chee Weng Keong
 * @version $Id: DateUtils.java,v 1.6 2004/04/13 09:10:17 wkchee Exp $
 */

public class DateUtils
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(DateUtils.class);

    /**
     * Gets date difference in days.
     *
     * @param fromDate First date object containing the date.
     * @param toDate   Second date object containing the date.
     * @return Date difference in months.
     */
    public static int getDateDifferenceInMonths(Date fromDate, Date toDate)
    {
        com.ibm.icu.util.Calendar calendar = com.ibm.icu.util.Calendar.getInstance();
        calendar.setTime(fromDate);
        return Math.abs(calendar.fieldDifference(toDate, com.ibm.icu.util.Calendar.MONTH));
    }

    /**
     * Gets date difference in days.
     *
     * @param calendar1 First calendar object containing the date.
     * @param calendar2 Second calendar object containing the date.
     * @return Date difference in days.
     */
    public static int getDateDifferenceInDays(Calendar calendar1, Calendar calendar2)
    {
        Date date1 = new Date(calendar1.getTime().getTime());
        Date date2 = new Date(calendar2.getTime().getTime());
        return getDateDifferenceInDays(date1, date2);
    }

    /**
     * @deprecated Use getDateDifferenceInDays(Calendar calendar1, Calendar calendar2)
     */
    public static long getDateDifferenceInDay(Calendar calendar1, Calendar calendar2)
    {
        return getDateDifferenceInDays(calendar1, calendar2);
    }

    /**
     * Gets date difference in days.
     *
     * @param fromDate First date object containing the date.
     * @param toDate   Second date object containing the date.
     * @return Date difference in days.
     */
    public static int getDateDifferenceInDays(Date fromDate, Date toDate)
    {
        com.ibm.icu.util.Calendar calendar = com.ibm.icu.util.Calendar.getInstance();
        calendar.setTime(fromDate);
        return Math.abs(calendar.fieldDifference(toDate, com.ibm.icu.util.Calendar.DATE));
    }

    /**
     * @deprecated Use getDateDifferenceInDays(Date fromDate, Date toDate)
     */
    public static long getDateDifferenceInDay(Date date1, Date date2)
    {
        return getDateDifferenceInDays(date1, date2);

        // the following logic has been replaced by the above logic to utilise
        // third-party library to perform the calculation

        /*
        // 1 day = 24 * 60 * 60 * 1000 miliseconds
        final long DAY = 86400000;

        long diff = getDateDifferenceInMilisecond(date1, date2);
        double days = (double)diff / (double)DAY;
        if (days < 0)
        {
            return (long)Math.floor(days);
        }
        else
        {
            return (long)days;
        }
        */
    }

    /**
     * Gets date difference in hours.
     *
     * @param fromDate First date object containing the date.
     * @param toDate   Second date object containing the date.
     * @return Date difference in hours.
     */
    public static int getDateDifferenceInHours(Date fromDate, Date toDate)
    {
        com.ibm.icu.util.Calendar calendar = com.ibm.icu.util.Calendar.getInstance();
        calendar.setTime(fromDate);
        return Math.abs(calendar.fieldDifference(toDate, com.ibm.icu.util.Calendar.HOUR_OF_DAY));
    }

    /**
     * @deprecated Use getDateDifferenceInHours(Date fromDate, Date toDate)
     */
    public static long getDateDifferenceInHour(Date date1, Date date2)
    {
        return getDateDifferenceInHours(date1, date2);

        // the following logic has been replaced by the above logic to utilise
        // third-party library to perform the calculation

        /*
        // 1 hour = 60 * 60 * 1000 miliseconds
        final long HOUR = 3600000;

        long diff = getDateDifferenceInMilisecond(date1, date2);
        double hours = (double)diff / (double)HOUR;
        if (hours < 0)
        {
            return (long)Math.floor(hours);
        }
        else
        {
            return (long)hours;
        }
        */
    }

    /**
     * Gets date difference in minutes.
     *
     * @param fromDate First date object containing the date.
     * @param toDate   Second date object containing the date.
     * @return Date difference in minutes.
     */
    public static int getDateDifferenceInMinutes(Date fromDate, Date toDate)
    {
        com.ibm.icu.util.Calendar calendar = com.ibm.icu.util.Calendar.getInstance();
        calendar.setTime(fromDate);
        return Math.abs(calendar.fieldDifference(toDate, com.ibm.icu.util.Calendar.MINUTE));
    }

    /**
     * @deprecated Use getDateDifferenceInMinutes(Date fromDate, Date toDate)
     */
    public static long getDateDifferenceInMinute(Date date1, Date date2)
    {
        return getDateDifferenceInMinutes(date1, date2);

        // the following logic has been replaced by the above logic to utilise
        // third-party library to perform the calculation

        /*
        // 1 minute = 60 * 1000 miliseconds
        final long MINUTE = 60000;

        long diff = getDateDifferenceInMilisecond(date1, date2);
        double minutes = (double)diff / (double)MINUTE;
        if (minutes < 0)
        {
            return (long)Math.floor(minutes);
        }
        else
        {
            return (long)minutes;
        }
        */
    }

    /**
     * Gets date difference in seconds.
     *
     * @param fromDate First date object containing the date.
     * @param toDate   Second date object containing the date.
     * @return Date difference in seconds.
     */
    public static int getDateDifferenceInSeconds(Date fromDate, Date toDate)
    {
        com.ibm.icu.util.Calendar calendar = com.ibm.icu.util.Calendar.getInstance();
        calendar.setTime(fromDate);
        return Math.abs(calendar.fieldDifference(toDate, com.ibm.icu.util.Calendar.SECOND));
    }

    /**
     * @deprecated getDateDifferenceInSeconds(Date fromDate, Date toDate)
     */
    public static long getDateDifferenceInSecond(Date date1, Date date2)
    {
        return getDateDifferenceInSeconds(date1, date2);

        // the following logic has been replaced by the above logic to utilise
        // third-party library to perform the calculation

        /*
        // 1 second = 1000 miliseconds
        final long SECOND = 1000;

        long diff = getDateDifferenceInMilisecond(date1, date2);
        double seconds = (double)diff / (double)SECOND;
        if (seconds < 0)
        {
            return (long)Math.floor(seconds);
        }
        else
        {
            return (long)seconds;
        }
        */
    }

    /**
     * Gets date difference in miliseconds.
     *
     * @param fromDate First date object containing the date.
     * @param toDate   Second date object containing the date.
     * @return Date difference in miliseconds.
     */
    public static long getDateDifferenceInMiliseconds(Date fromDate, Date toDate)
    {
        return Math.abs(fromDate.getTime() - toDate.getTime());
    }

    /**
     * @deprecated Use getDateDifferenceInMiliseconds(Date fromDate, Date toDate)
     */
    public static long getDateDifferenceInMilisecond(Date date1, Date date2)
    {
        return getDateDifferenceInMiliseconds(date1, date2);
    }

    /**
     * Gets current date.
     *
     * @return Current date.
     */
    public static Date getCurrentDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new Date(calendar.getTime().getTime());
    }

    /**
     * Gets current date time.
     *
     * @return Current date time.
     */
    public static Date getCurrentDateTime()
    {
        return new Date(System.currentTimeMillis());
    }

    /**
     * Consolidate Date object from three strings: year, month, date.
     *
     * @param year  Year.
     * @param month Month.
     * @param date  Date.
     * @return Date object.
     */
    public static Date getDate(String year, String month, String date)
    {
        return DateUtils.getDateTime(year, month, date, "0", "0", "0", Locale.getDefault());
    }

    /**
     * Consolide Date object from three strings: year, month, date based on locale.
     *
     * @param year   Year.
     * @param month  Month.
     * @param date   Date.
     * @param locale Current locale.
     * @return Date object.
     */
    public static Date getDate(String year, String month, String date, Locale locale)
    {
        return DateUtils.getDateTime(year, month, date, "0", "0", "0", locale);
    }

    /**
     * Consolide Date object from six strings: year, month, date, hour, minute, second.
     *
     * @param year   Year.
     * @param month  Month.
     * @param date   Date.
     * @param hour   Hour.
     * @param minute Minute.
     * @param second Second.
     * @return Date object.
     */
    public static Date getDateTime(String year, String month, String date,
                                   String hour, String minute, String second)
    {
        return DateUtils.getDateTime(year, month, date, hour, minute, second, Locale.getDefault());
    }

    /**
     * Consolide Date object from six strings: year, month, date, hour, minute, second based on locale.
     *
     * @param year   Year.
     * @param month  Month.
     * @param date   Date.
     * @param hour   Hour.
     * @param minute Minute.
     * @param second Second.
     * @param locale Current locale.
     * @return Date object.
     */
    public static Date getDateTime(String year, String month, String date,
                                   String hour, String minute, String second, Locale locale)
    {
        if (locale == null)
        {
            logger.error("Locale is missing!");
        }

        if (!StringUtils.hasValue(year) || !StringUtils.hasValue(month) || !StringUtils.hasValue(date) ||
            !StringUtils.hasValue(hour) || !StringUtils.hasValue(minute) || !StringUtils.hasValue(second))
        {
            return null;
        }
        else
        {
            if (locale.getLanguage().equals(LocaleConstants.THAILAND.getLanguage()))
            {
                com.ibm.icu.util.Calendar calendar = new com.ibm.icu.util.BuddhistCalendar();
                calendar.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(date),
                             Integer.parseInt(hour), Integer.parseInt(minute), Integer.parseInt(second));
                return new Date(calendar.getTime().getTime());
            }
            else
            {
                Calendar calendar = Calendar.getInstance(locale);
                calendar.set(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(date),
                             Integer.parseInt(hour), Integer.parseInt(minute), Integer.parseInt(second));
                return new Date(calendar.getTime().getTime());
            }
        }
    }

    /**
     * Gets short month strings.
     *
     * @return Short month strings.
     */
    public static String[] getShortMonths()
    {
        return DateUtils.getShortMonths(Locale.getDefault());
    }

    /**
     * Gets short month strings for specific locale.
     *
     * @param locale Locale.
     * @return Short month strings.
     */
    public static String[] getShortMonths(Locale locale)
    {
        if (locale == null)
        {
            logger.error("Locale is missing!");
        }

        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(locale);
        return dateFormatSymbols.getShortMonths();
    }

    /**
     * Converts date compartments into an array of strings..
     *
     * @param date Date.
     * @return Array of strings (year, month, date).
     */
    public static String[] convertDateToStrings(Date date)
    {
        return convertDateToStrings(date, Locale.getDefault());
    }

    /**
     * Converts date compartments into an array of strings..
     *
     * @param date   Date.
     * @param locale Locale.
     * @return Array of strings (year, month, date).
     */
    public static String[] convertDateToStrings(Date date, Locale locale)
    {
        if (locale == null)
        {
            logger.error("Locale is missing!");
        }

        if (date == null)
        {
            return null;
        }

        String[] values = new String[3];
        int yearValue, monthValue, dateValue;

        if (locale.getLanguage().equals(LocaleConstants.THAILAND.getLanguage()))
        {
            com.ibm.icu.util.Calendar calendar = new com.ibm.icu.util.BuddhistCalendar();
            calendar.setTime(date);
            yearValue = calendar.get(Calendar.YEAR);
            monthValue = calendar.get(Calendar.MONTH) + 1;
            dateValue = calendar.get(Calendar.DATE);
        }
        else
        {
            Calendar calendar = Calendar.getInstance(locale);
            calendar.setTime(date);
            yearValue = calendar.get(Calendar.YEAR);
            monthValue = calendar.get(Calendar.MONTH) + 1;
            dateValue = calendar.get(Calendar.DATE);
        }

        values[0] = "" + yearValue;
        values[1] = monthValue >= 10 ? "" + monthValue : "0" + monthValue;
        values[2] = dateValue >= 10 ? "" + dateValue : "0" + dateValue;

        return values;
    }

    /**
     * Converts date time compartments into an array of strings..
     *
     * @param dateTime Date time.
     * @return Array of strings (year, month, date, hour, minute, second).
     */
    public static String[] convertDateTimeToStrings(Date dateTime)
    {
        return convertDateTimeToStrings(dateTime, Locale.getDefault());
    }

    /**
     * Converts date time compartments into an array of strings..
     *
     * @param dateTime Date time.
     * @param locale   Locale.
     * @return Array of strings (year, month, date, hour, minute, second).
     */
    public static String[] convertDateTimeToStrings(Date dateTime, Locale locale)
    {
        if (locale == null)
        {
            logger.error("Locale is missing!");
        }

        if (dateTime == null)
        {
            return null;
        }

        String[] values = new String[6];
        int yearValue, monthValue, dateValue, hourValue, minuteValue, secondValue;

        if (locale.getLanguage().equals(LocaleConstants.THAILAND.getLanguage()))
        {
            com.ibm.icu.util.Calendar calendar = new com.ibm.icu.util.BuddhistCalendar();
            calendar.setTime(dateTime);
            yearValue = calendar.get(Calendar.YEAR);
            monthValue = calendar.get(Calendar.MONTH) + 1;
            dateValue = calendar.get(Calendar.DATE);
            hourValue = calendar.get(Calendar.HOUR_OF_DAY);
            minuteValue = calendar.get(Calendar.MINUTE);
            secondValue = calendar.get(Calendar.SECOND);
        }
        else
        {
            Calendar calendar = Calendar.getInstance(locale);
            calendar.setTime(dateTime);
            yearValue = calendar.get(Calendar.YEAR);
            monthValue = calendar.get(Calendar.MONTH) + 1;
            dateValue = calendar.get(Calendar.DATE);
            hourValue = calendar.get(Calendar.HOUR_OF_DAY);
            minuteValue = calendar.get(Calendar.MINUTE);
            secondValue = calendar.get(Calendar.SECOND);
        }

        values[0] = "" + yearValue;
        values[1] = monthValue >= 10 ? "" + monthValue : "0" + monthValue;
        values[2] = dateValue >= 10 ? "" + dateValue : "0" + dateValue;
        values[3] = hourValue >= 10 ? "" + hourValue : "0" + hourValue;
        values[4] = minuteValue >= 10 ? "" + minuteValue : "0" + minuteValue;
        values[5] = secondValue >= 10 ? "" + secondValue : "0" + secondValue;

        return values;
    }
}

// end of DateUtils.java