/*
 * FriendsterImporter.java
 *
 * Created on April 25, 2008, 3:43 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;
 
import com.esmart2u.oeight.member.helper.EmailAddressHelper;
import com.esmart2u.solution.base.helper.StringUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.apache.http.HttpException;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import com.esmart2u.solution.base.helper.contactlist.*;
import java.util.logging.Logger;
import java.util.regex.*;


/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterImporter  extends ContactListImporterImpl {
    
    // Karen Kong String currentUserId = "25154061";
    String currentUserId = "90965984"; // 90965991 Starts from newest member //Marvin"3303208";
    String christmasWish = "<a href=\"http://080808.com.my/\" target=\"#\"><img src=\"http://080808.org.my/seasons_greetings/img/xmas2008.gif\" width=\"600\" height=\"400\"></a>";
    String referralPage = "http://www.friendster.com/user.php";
    
    private DefaultHttpClient defClient = null;
    
    public FriendsterImporter(String username, String password) {
        super(username, password);
    }
    
    public String getCommentURL() {
        return "http://www.friendster.com/comments.php?uid=";
    }
    
    @Override
    public String getContactListURL() {
        return "http://www.friendster.com/comments.php?uid=" + currentUserId; 
    }
    
    @Override
    public String getLoginURL() {
        return "http://www.friendster.com/login.php";
    }
    
    protected void login(DefaultHttpClient client) throws Exception {
        String loginPageUrl=getLoginURL().toString();
        getLogger().info("Requesting login page");
        
        /*String content=this.readInputStream(
                this.doGet(client, loginPageUrl, null)
                );
         
        String formUrl=getFormUrl(content); */
        
        // security cookie
        /*BasicClientCookie cookie=new BasicClientCookie("CkTst", "G"+System.currentTimeMillis());
        cookie.setDomain("login.live.com");
        cookie.setPath("/ppsecure/");
        client.getCookieStore().addCookie(cookie);*/
        
        
        NameValuePair[] data = {
            new BasicNameValuePair("_submitted", "1"),
            new BasicNameValuePair("next", "/"),
            new BasicNameValuePair("tzoffset", "-480"),
            new BasicNameValuePair("email", this.getUsername()),
            new BasicNameValuePair("password", this.getPassword())
        };
        
        getLogger().info("Performing login");
        String content=this.readInputStream(
                this.doPost(client, loginPageUrl, data, loginPageUrl)
                );
        defClient = client;
        
        if(content.contains("password is incorrect")) {
            getLogger().info("Login failed");
            throw new AuthenticationException("Username and password do not match");
        }
        
        System.out.println("You need to get the user id and set to variable here.");
        //var pageViewerID = "66748125";
        String keyMatchStart = "var pageViewerID = \"";
        String keyMatchEnd = "\";";
        int stringIndex = content.indexOf(keyMatchStart);
        System.out.println("Match? >>" + stringIndex);
        /*if (stringIndex> 0) {
            currentUserId = content.substring(stringIndex + 20,stringIndex + 28); 
            if (currentUserId.trim().endsWith("\"")) currentUserId = currentUserId.substring(0,currentUserId.trim().length()-1);
            System.out.println("Login success for userId=" + currentUserId);
        }*/
        System.out.println(">>>>>>>>>>> content after login ="+content);
        
    }
    
    protected List<Contact> parseContacts(InputStream contactsContent) throws Exception {
        
        // For a test, submit comments for 3 users from the reducing number
        // 1) Get comment page
        // 2) Get variables in form
        // 3) Submit values
        List contacts = new ArrayList();
        String authCode = "";
        
        BufferedReader in=new BufferedReader(new InputStreamReader(contactsContent));
        String keyMatchStart = "<input type=\"hidden\" name=\"authcode\" value=\"";
        String keyMatchEnd = "\"><div class=\"flo466\">";
        String line = "";
        while ((line = in.readLine()) != null) {
                    //System.out.println(line);
                    int lineIndex = line.indexOf(keyMatchStart);
                    if (lineIndex> -1) {
                        int lineEndIndex = line.indexOf(keyMatchEnd,lineIndex);
                        if (lineEndIndex > -1) {
                            authCode = line.substring(lineIndex + keyMatchStart.length(),line.indexOf(keyMatchEnd,lineIndex));
                            System.out.println("Found authCode=" + authCode);

                        }
                    }
                }

        InputStream inputStream = null;
        
        int count = 0;
        Long currentUserIdLong = new Long(currentUserId);
        while (currentUserIdLong > 0)
        {
            try{
 
                System.out.println(">>>> START>>>> Getting comment page of " + count + ") " + currentUserIdLong);
                inputStream = this.doGet(defClient, getCommentURL()+ currentUserIdLong.toString(), referralPage);

                in=new BufferedReader(new InputStreamReader(inputStream));   
                while ((line = in.readLine()) != null) {
                    //System.out.println(line);
                    int lineIndex = line.indexOf(keyMatchStart);
                    if (lineIndex> -1) {
                        int lineEndIndex = line.indexOf(keyMatchEnd,lineIndex);
                        if (lineEndIndex > -1) {
                            authCode = line.substring(lineIndex + keyMatchStart.length(),line.indexOf(keyMatchEnd,lineIndex));
                            System.out.println("Page " + currentUserIdLong + " Found authCode=" + authCode);

                        }
                    }
                }
                System.out.println(">>>>>>> AUTHCODE:" + authCode);
                if (!StringUtils.hasValue(authCode))
                {
                    System.out.println("Unable to get authCode for " + currentUserIdLong);
                }
                else
                {
                    // Post comment

                    NameValuePair[] commentData = { 
                        new BasicNameValuePair("authCode", authCode),
                        new BasicNameValuePair("postaction", "submit"),
                        new BasicNameValuePair("comment", "%3Ca+href%3D%22http%3A%2F%2F080808.com.my%2F%22+target%3D%22%23%22%3E%3Cimg+src%3D%22http%3A%2F%2F080808.org.my%2Fseasons_greetings%2Fimg%2Fxmas2008.gif%22+width%3D%22600%22+height%3D%22400%22%3E%3C%2Fa%3E"),
                        new BasicNameValuePair("inputcount", "861")
                    };

                    getLogger().info("Performing comment submission");
                    /*
                    getLogger().info("Check cookie before submission");
                    CookieStore cookieStore = defClient.getCookieStore();
                    List cookieList = cookieStore.getCookies();
                    for (int i=0; i<cookieList.size(); i++)
                    {
                        Cookie cookie = (Cookie)cookieList.get(i);
                        System.out.println("Cookie " + i + ":"+ cookie.getName() + " " + cookie.getValue());
                    }*/

                    String content=this.readInputStream(
                            this.doPost(defClient, getCommentURL()+ currentUserIdLong.toString(), commentData, getCommentURL()+ currentUserIdLong.toString())
                            );
                    /*
                    getLogger().info("Check cookie after submission");
                    cookieStore = defClient.getCookieStore();
                    cookieList = cookieStore.getCookies();
                    for (int i=0; i<cookieList.size(); i++)
                    {
                        Cookie cookie = (Cookie)cookieList.get(i);
                        System.out.println("Cookie " + i + ":"+ cookie.getName() + " " + cookie.getValue());
                    }*/
                    System.out.println(">>> DONE Content after comment submission for " + count + ") " +currentUserIdLong+" \n" + content);
                }

                long random = Math.round(Math.random()*(200)* 1000);
                System.out.println("Sleep for " + random + " secs");
                Thread.sleep(random);

            } catch (Exception e) {
                System.out.println("Unable to get comment page of " + currentUserIdLong);
            }

            // Loop criteria
            count++;
            currentUserIdLong--;
            
            if (count > 188)
            {
                currentUserIdLong = new Long(0);
            }
        }
        return contacts;  
        
    }
     
}
