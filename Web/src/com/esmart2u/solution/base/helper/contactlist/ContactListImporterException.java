/*
 * ContactListImporterException.java
 *
 * Created on April 7, 2008, 4:47 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;
/**
 * Represents a generic exception that occurs when
 * retrieving contacts. This exception often
 * wraps another exception like an IOException.
 * 
 * @author Tjerk Wolterink
 */
public class ContactListImporterException extends Exception {

	public ContactListImporterException(String message) {
		super(message);
	}
	
	public ContactListImporterException(String message, Throwable t) {
		super(message, t);
	}
}
