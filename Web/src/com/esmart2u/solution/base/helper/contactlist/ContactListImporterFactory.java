/*
 * ContactListImporterFactory.java
 *
 * Created on April 7, 2008, 4:48 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;

/**
 * Factory for creating ContactListImporter objects.
 * Implemented using the factory pattern.
 * 
 * @author Tjerk Wolterink
 */
public class ContactListImporterFactory {

	/**
	 * Guesses which service to use given an input email adress.
	 * Note that this does not work for all e-mail adresses,
	 * gmail for example allow users to use their own domain name,
	 * then the guess will fail.
	 * Returns null if no match could be found
	 * 
	 * @param email
	 * @param password
	 * @return a importer for the email adress or null if no importer could be found
	 */
	public static ContactListImporter guess(String email, String password) {
		if(HotmailImporter.isHotmail(email)) {
			return new HotmailImporter(email, password);
		
		} else if(GmailImporter.isGmail(email)) {
			return new GmailImporter(email, password);
			
		} else if(YahooImporter.isYahoo(email)) {
			return new YahooImporter(email, password);
			
		}
		return null;
	}
	
	public static ContactListImporter hotmail(String email, String password) {
		return new HotmailImporter(email, password);
	}
	
	public static ContactListImporter gmail(String email, String password) {
		return new GmailImporter(email, password);
	}
	
	public static ContactListImporter yahoo(String email, String password) {
		return new YahooImporter(email, password);
	}
	
	/*public static ContactListImporter hyves(String email, String password) {
		return new HyvesImporter(email, password);
	}*/
        
	public static boolean hasProvider(String email) {
            System.out.println("Has provider?" + email);
		if(HotmailImporter.isHotmail(email)|| 
                    GmailImporter.isGmail(email)||
                    YahooImporter.isYahoo(email)) { 
                    return true;
		}
                else{
                    return false;
                } 
	}	
}

