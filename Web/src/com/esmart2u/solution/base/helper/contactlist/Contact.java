/*
 * Contact.java
 *
 * Created on April 7, 2008, 5:06 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;

/**
 *
 * @author meauchyuan.lee
 */
public interface Contact {
	
	/**
	 * Gets the name of this user. This value
	 * is normally used to display this user in the user interface.
	 *
	 * @return the name of this user
	 * @ensure return!=null
	 */
	public String getName();
	
	/**
	 * Gets the e-mail address of the user
	 */
	public String getEmailAddress();
}
