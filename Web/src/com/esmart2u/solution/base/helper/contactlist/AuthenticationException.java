/*
 * AuthenticationException.java
 *
 * Created on April 7, 2008, 4:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;

import com.esmart2u.solution.base.helper.contactlist.ContactListImporterException;

/**
 * When the user credentials (loginname, password) are incorrect when trying to login,
 * the this exception is thrown.
 * 
 * @author Tjerk Wolterink
 */
public class AuthenticationException extends ContactListImporterException {
	
	public AuthenticationException(String message) {
		super(message);
	}
	
	public AuthenticationException(String message, Throwable t) {
		super(message, t);
	}
}
