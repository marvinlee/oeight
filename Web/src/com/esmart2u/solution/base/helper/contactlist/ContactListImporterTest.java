/*
 * ContactListImporterTest.java
 *
 * Created on April 7, 2008, 5:20 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;
import java.util.List;
import java.util.Properties;
import junit.framework.TestCase;

public class ContactListImporterTest extends TestCase {
	
	public void testHotmailImporter() throws Exception {
		ContactListImporter importer=ContactListImporterFactory.guess("my_080808_c@hotmail.com", "6m6u9muk");
		testImporter(importer);
	}
	
	public void testGmailImporter() throws Exception {
		ContactListImporter importer=ContactListImporterFactory.guess("marvinmclee@gmail.com", "");
		testImporter(importer);
	}
        
	public void testYahooImporter() throws Exception {
		ContactListImporter importer=ContactListImporterFactory.guess("lmchyuan@yahoo.com", "marvinyahoo");
		testImporter(importer);
	}
        
	public void testFriendsterImporter() throws Exception {
		 
		//testImporter(new FriendsterImporter("my_080808@hotmail.com", "password"));
            testImporter(new FriendsterImporter("founder@080808.com.my", "acew49"));
	}
	
	public void testImporter(ContactListImporter importer) throws ContactListImporterException, InterruptedException {
		System.out.println("Testing importer: "+importer.getClass().getName());
		List<Contact> contacts=importer.getContactList();
		for(Contact c : contacts) {
			System.out.println(c);
		}
		System.out.println("Number of contacts found: "+contacts.size());
		Thread.currentThread().sleep(2000);
	}
        
        public static void main(String[] args) throws Exception{
            /*Properties systemSettings = System.getProperties();
            systemSettings.put("http.proxyHost", "216.115.166.81");
            systemSettings.put("http.proxyPort", "8080");
            System.setProperties(systemSettings);*/
            
            //new ContactListImporterTest().testYahooImporter();
            //new ContactListImporterTest().testHotmailImporter();
            
            //new ContactListImporterTest().testGmailImporter();
            new ContactListImporterTest().testFriendsterImporter();
        }
}
