/*
 * ContactListHelper.java
 *
 * Created on May 7, 2008, 1:31 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;

import java.util.List;

/**
 *
 * @author meauchyuan.lee
 */
public class ContactListHelper {
    
    /** Creates a new instance of ContactListHelper */
    public ContactListHelper() {
    }

    public List getContacts(String emailName, String provider, String password) {
        
        List contactList = null;
        String fullEmail = emailName + "@" + provider;
        ContactListImporter importer=ContactListImporterFactory.guess(fullEmail, password);
        try {
            if (importer != null)
            {
                contactList = importer.getContactList();
            }
        } catch (ContactListImporterException ex) {
            ex.printStackTrace();
        }
	return contactList;	
    }
    
    public static boolean hasValidProvider(String fullEmail)
    {
        return ContactListImporterFactory.hasProvider(fullEmail);
    }
    
    
}
