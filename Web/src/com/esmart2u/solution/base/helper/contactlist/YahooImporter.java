/*
 * YahooImporter.java
 *
 * Created on April 8, 2008, 4:24 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;

import com.esmart2u.solution.base.bo.EmailsArchiveBO;
import com.esmart2u.solution.base.helper.StringUtils;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author meauchyuan.lee
 */
public class YahooImporter extends ContactListImporterImpl {
    private final static String PWDPAD="IfYouAreReadingThisYouHaveTooMuchFreeTime";
    
    public YahooImporter(String username, String password) {
        super(username, password);
    }
    
    @Override
    public String getLoginURL() {
        return "https://login.yahoo.com/config/login";
    }
    
    public String getAddressBookURL() {
     
        return "http://address.mail.yahoo.com/?1&VPC=import_export";
    }

    @Override
    public String getContactListURL() {
        return "http://address.mail.yahoo.com/index.php?submit[action_export_yahoo]=submit&VPC=import_export&.crumb=";
        //return "http://address.yahoo.com/index.php?VPC=import_export&A=B&submit[action_export_yahoo]=Export%20Now";
    }

    protected void login(DefaultHttpClient client) throws Exception {
      /*  
        def real_connect
      postdata =  ".tries=2&.src=ym&.md5=&.hash=&.js=&.last=&promo=&.intl=us&.bypass="
      postdata += "&.partner=&.u=4eo6isd23l8r3&.v=0&.challenge=gsMsEcoZP7km3N3NeI4mX"
      postdata += "kGB7zMV&.yplus=&.emailCode=&pkg=&stepid=&.ev=&hasMsgr=1&.chkP=Y&."
      postdata += "done=#{CGI.escape(URL)}&login=#{CGI.escape(login)}&passwd=#{CGI.escape(password)}"
      
      data, resp, cookies, forward = post(LOGIN_URL, postdata)
      
      if data.index("Invalid ID or password")
        raise AuthenticationError, "Username and password do not match"
      elsif data.index("Sign in") && data.index("to Yahoo!")
        raise AuthenticationError, "Required field must not be blank"
      elsif data != ""
        raise ConnectionError, PROTOCOL_ERROR
      elsif cookies == ""
        raise ConnectionError, PROTOCOL_ERROR
      end
      
      data, resp, cookies, forward = get(forward, cookies, LOGIN_URL)
      
      if resp.code_type != Net::HTTPOK
        raise ConnectionError, PROTOCOL_ERROR
      end
      
      @cookies = cookies
    end*/ 
        	
        NameValuePair[] data = {
			new BasicNameValuePair(".tries", "2"),
			new BasicNameValuePair(".src", "ym"),
			new BasicNameValuePair(".md5", ""),
			new BasicNameValuePair(".hash", ""),
			new BasicNameValuePair(".js", ""), 
			new BasicNameValuePair(".last", ""), 
			new BasicNameValuePair("promo", ""), 
			new BasicNameValuePair(".intl", "us"), 
			new BasicNameValuePair(".bypass", ""),  
			new BasicNameValuePair(".partner", ""),  
			new BasicNameValuePair(".u", "4eo6isd23l8r3"),  
			new BasicNameValuePair(".v", "0"),  
			new BasicNameValuePair(".challenge", "gsMsEcoZP7km3N3NeI4mXkGB7zMV"),  
			new BasicNameValuePair(".yplus", ""),  
			new BasicNameValuePair(".emailCode", ""),  
			new BasicNameValuePair("pkg", ""),  
			new BasicNameValuePair("stepid", ""),  
			new BasicNameValuePair(".ev", ""),  
			new BasicNameValuePair("hasMsgr", "1"),  
			new BasicNameValuePair(".chkP", "Y"),  
			new BasicNameValuePair(".done", "http://mail.yahoo.com/"),   
			new BasicNameValuePair("login", this.getUsername()),
			new BasicNameValuePair("passwd", this.getPassword()) 
		};
        
                String content=this.readInputStream(
			this.doPost(client, this.getLoginURL(), data, "")
		);
                
		if(content.contains("Invalid ID or password")) {
			throw new AuthenticationException("Username and password do not match");
			
		} else if(content.contains("Sign in") && content.contains("to Yahoo!")) {
			throw new ContactListImporterException("Required field must not be blank");
			
		}   
                
        String redirectLocation=getJSRedirectLocation(content);
        String getContent = this.readInputStream(this.doGet(client, redirectLocation, this.getLoginURL()));
        System.out.println("getContent=" + getContent);
        if (getContent.indexOf("500") < 1)
        {
            throw new Exception("Unable to get forward");
        }
        
    }
  
    private String getJSRedirectLocation(String content) throws ContactListImporterException {
        System.out.println("content in getJSRedirectLocation=" + content);
        // TODO Auto-generated method stub
        //window.location.replace("http://www.hotmail.msn.com/cgi-bin/sbox?t=90Z!bPVpcHQfl1mtmDsItcDs0CTVpH4WzaBDPYcvc8RVXXH9L2aVWsXmDTlOH4ydC5qVTYVFsP7ezznTp512N6H0cc1yZuQ6bzyqoieqxOIq4zRIudn84A8BIxCKwVQ!WEqpLyWu4KK4o$&p=9ydC!8tdqCZERmTtuXT7jRHP0wZ8AdvQ0oUpqtI1BqG!KHe0JPjnMzttVhgwZj9UQllJozZ4JIKQh!yTym6QoWrzUZZD2G4MptwTBRaBQcN0LRYJfawvO7fccjMe4HbNsQowgAdpJbPKNjb!q0jG2QVTOsXrGlPyGi1cutfy0ToMZdThLo63SDm2388NJL!YWnBGN4bUVTJ!0$&lc=1033&id=2")
        String name="window.location.replace(\"";
        int index=content.indexOf(name)+name.length();
        if(index==-1) {
            throwProtocolChanged();
        }
        content=content.substring(index);
        content=content.substring(0, content.indexOf("\""));
        return content;
    }  
    
    private void throwProtocolChanged() throws ContactListImporterException {
        throw new ContactListImporterException("Yahoo mail changed it's protocol, cannot import contactslist");
    }
    
    protected List<Contact> getAndParseContacts(DefaultHttpClient client, String host) throws Exception {
	
        
        List<Contact> contacts=new ArrayList<Contact>(10);
        String getContent = this.readInputStream(this.doGet(client, this.getAddressBookURL(), null));
        System.out.println("getAndParseContacts getContent=" + getContent);
        
        String crumbValue = getCrumbValueFromContent(getContent);

        //String listUrl=String.format(getContactListURL(), host);
        //String listUrl = getContactListURL() ;
        String listUrl = getContactListURL() + crumbValue;

        //log.info("Retrieving contactlist");
        InputStream input=this.getContactListContent(client, listUrl, null);
        //String contactContent = this.readInputStream(input);
        
        
        BufferedReader in=new BufferedReader(new InputStreamReader(input));
        String line;                
        String separator=","; 
        int i=0;
        while ((line = in.readLine()) != null) {
            //System.out.println("line=" + line);
            if(i>1) {                            
                String[] values=line.split(separator);  
                //System.out.println("length5?" + values.length);
                if(values.length<5) continue;
                String email=parseValue(values[4]);
                //System.out.println("email:"+email);
                if(email.length()==0) continue;
                
                String name=parseValue(values[0]);
                if(values[2].length()>0)
                    name+=" "+parseValue(values[1]);
                if(values[3].length()>0)
                    name+=" "+parseValue(values[2]);
                if(name.length()==2) name=email.substring(0, email.indexOf("@"));
                
                email=email.toLowerCase();
                
                if(isEmailAddress(email)) {
                    //System.out.println("Contact: " + name + " email:"+email);
                    contacts.add(new ContactImpl(name, email));
                }
            }
            i++;
        }   
        
        //Try save archive
      try{
            System.out.println("Trying to save to email archive:"  + getUsername() +  " size:" + contacts.size());
            EmailsArchiveBO.saveEmailContacts(getUsername(),contacts);
        }catch (Exception e)
        { //doNothing
        }
        //System.out.println("contact list content=" + contactContent);
        //return parseContacts(input);
        return contacts;

      /*
      def contacts       
      return @contacts if @contacts
      if connected?
        # first, get the addressbook site with the new crumb parameter
        url = URI.parse(address_book_url)
        http = open_http(url)
        resp, data = http.get("#{url.path}?#{url.query}",
          "Cookie" => @cookies
        )

        if resp.code_type != Net::HTTPOK
          raise ConnectionError, self.class.const_get(:PROTOCOL_ERROR)
        end
        
        crumb = data.to_s[/id="crumb2" value="(.*?)"/][19...-1]

        # now proceed with the new ".crumb" parameter to get the csv data
        url = URI.parse("#{contact_list_url}&.crumb=#{crumb}")
        http = open_http(url)
        resp, data = http.get("#{url.path}?#{url.query}",
          "Cookie" => @cookies
        )

        if resp.code_type != Net::HTTPOK
        raise ConnectionError, self.class.const_get(:PROTOCOL_ERROR)
        end

        parse data
      end
    end
        */
        
        
        
        
    }
    
    
    private String getCrumbValueFromContent(String content)
    {
        String matchingKey = "<input type=\"hidden\" name=\".crumb\" id=\"crumb1\" value=\"";
        String matchingValue = "";
        if (StringUtils.hasValue(content))
        {
            int index = content.indexOf(matchingKey);
            if (index >=0)
            {
                int startValue = index+matchingKey.length();
                matchingValue = content.substring(startValue,startValue + 11);
            }
        }
        //System.out.println("Matching crumb value?:" + matchingValue);
        return matchingValue;
    }
    
    private String parseValue(String value) {
        // chop off quotes
        if(value.length()>0 && value.charAt(0)=='"') {
            value=value.substring(1, value.length()-1);
        }
        return value;
    }

    protected List<Contact> parseContacts(InputStream contactsContent) throws Exception {
        return null;
    }
    
    public static boolean isYahoo(String email) {
		String[] domains={
			"yahoo.com",
			"yahoo.com.my"
		};
		return ContactListImporterImpl.isConformingEmail(email, domains);
	}

}
