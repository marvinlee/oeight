/*
 * FriendsterImporter.java
 *
 * Created on April 25, 2008, 3:43 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper.contactlist;

import com.esmart2u.oeight.member.helper.EmailAddressHelper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.apache.http.HttpException;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import com.esmart2u.solution.base.helper.contactlist.*;
import java.util.logging.Logger;
import java.util.regex.*;


/**
 *
 * @author meauchyuan.lee
 */
public class FriendsterImporter_1  extends ContactListImporterImpl {
    
    String currentUserId = "25154061";
    String referralPage = "";
    
    public FriendsterImporter_1(String username, String password) {
        super(username, password);
    }
    
    @Override
    public String getContactListURL() {
        return "http://www.friendster.com/fans/" + currentUserId;
    }
    
    @Override
    public String getLoginURL() {
        return "http://www.friendster.com/login.php";
    }
    
    protected void login(DefaultHttpClient client) throws Exception {
        String loginPageUrl=getLoginURL().toString();
        getLogger().info("Requesting login page");
        
        /*String content=this.readInputStream(
                this.doGet(client, loginPageUrl, null)
                );
         
        String formUrl=getFormUrl(content); */
        
        // security cookie
        /*BasicClientCookie cookie=new BasicClientCookie("CkTst", "G"+System.currentTimeMillis());
        cookie.setDomain("login.live.com");
        cookie.setPath("/ppsecure/");
        client.getCookieStore().addCookie(cookie);*/
        
        
        NameValuePair[] data = {
            new BasicNameValuePair("_submitted", "1"),
            new BasicNameValuePair("next", "/"),
            new BasicNameValuePair("tzoffset", "-480"),
            new BasicNameValuePair("email", this.getUsername()),
            new BasicNameValuePair("password", this.getPassword())
        };
        
        getLogger().info("Performing login");
        String content=this.readInputStream(
                this.doPost(client, loginPageUrl, data, loginPageUrl)
                );
        
        if(content.contains("password is incorrect")) {
            getLogger().info("Login failed");
            throw new AuthenticationException("Username and password do not match");
        }
        
        System.out.println("You need to get the user id and set to variable here.");
        //var pageViewerID = "66748125";
        String keyMatchStart = "var pageViewerID = \"";
        String keyMatchEnd = "\";";
        int stringIndex = content.indexOf(keyMatchStart);
        System.out.println("Match? >>" + stringIndex);
        /*if (stringIndex> 0) {
            currentUserId = content.substring(stringIndex + 20,stringIndex + 28); 
            if (currentUserId.trim().endsWith("\"")) currentUserId = currentUserId.substring(0,currentUserId.trim().length()-1);
            System.out.println("Login success for userId=" + currentUserId);
        }*/
        System.out.println(">>>>>>>>>>> content after login ="+content);
    }
    
    protected List<Contact> parseContacts(InputStream contactsContent) throws Exception {
        // This test is actually run through a fan profile fans list to get all their emails
         
        String keyMatchStart = "\"http://profiles.friendster.com/";
        String keyMatchEnd = "\"><img";
        HashSet firstLevelUserIdSet = new HashSet();
        BufferedReader in=new BufferedReader(new InputStreamReader(contactsContent));
        String line;
        //int i=0;
        String userId = null;
        
        HashSet emailSet = new HashSet();
        
        // 1) Collect all user ids on each page and get their profile page
        while ((line = in.readLine()) != null) {
            //System.out.println("line=" + line);
            int lineIndex = line.indexOf(keyMatchStart);
            if (lineIndex> -1) {
                int lineEndIndex = line.indexOf(keyMatchEnd,lineIndex);
                if (lineEndIndex > -1) {
                    userId = line.substring(lineIndex + keyMatchStart.length(),line.indexOf(keyMatchEnd,lineIndex));
                    System.out.println("Found (1st level) userId=" + userId);
                    // Try get long, might be username
                    if (userId.trim().endsWith("\"")) userId = userId.substring(0,userId.trim().length()-1);
                    Long validUserId;
                    try {
                        validUserId = new Long(userId); 
                        if (validUserId != null && validUserId > 0){
                            firstLevelUserIdSet.add(userId);
                            // Get emails
                            HashSet currentUidEmailSet = getEmailsFromProfilePage(userId);
                            System.out.println(userId + " has " + currentUidEmailSet.size() + " emails");
                            emailSet.addAll(currentUidEmailSet);
                            
                            
            long random = Math.round(Math.random()*(200)* 100);
            System.out.println("Sleep for " + random + " secs");
            Thread.sleep(random);
                        }
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
       
        // 
        int crawlPages = 2;
        for(int i=1;i<= crawlPages; i++)
        {
            
            InputStream inputStream = null;
            try{

                System.out.println("Getting fans page of " + i);
                inputStream = this.doGet(getHttpClient(), getContactListURL() + "/" + i, referralPage);
                referralPage = getContactListURL() + "/" + i;
                
                in=new BufferedReader(new InputStreamReader(inputStream));   
                while ((line = in.readLine()) != null) {
                    int lineIndex = line.indexOf(keyMatchStart);
                    if (lineIndex> -1) {
                        int lineEndIndex = line.indexOf(keyMatchEnd,lineIndex);
                        if (lineEndIndex > -1) {
                            userId = line.substring(lineIndex + keyMatchStart.length(),line.indexOf(keyMatchEnd,lineIndex));
                            System.out.println("Page " + i + " Found userId=" + userId);
                            // Try get long, might be username
                            if (userId.trim().endsWith("\"")) userId = userId.substring(0,userId.trim().length()-1);
                            Long validUserId;
                            try {
                                validUserId = new Long(userId);

                                if (validUserId != null && validUserId > 0){
                                    firstLevelUserIdSet.add(userId);
                                    HashSet currentUidEmailSet = getEmailsFromProfilePage(userId);
                                    System.out.println(userId + " has " + currentUidEmailSet.size() + " emails");
                                    emailSet.addAll(currentUidEmailSet);
                                    
                                    
            long random = Math.round(Math.random()*(200)* 100);
            System.out.println("Sleep for " + random + " secs");
            Thread.sleep(random);
                                } 
                            } catch (NumberFormatException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }

                } catch (Exception e) {
                    System.out.println("Unable to get profiles from fan page of " + i);
                }
        }
        
        // Then now we get their profile pages from firstLevelUserIdSet
        System.out.println("Total profiles?=" + firstLevelUserIdSet.size());
        /*Iterator uidIterator = firstLevelUserIdSet.iterator();
        int uidCount = 0;
        while (uidIterator.hasNext())
        {
            uidCount++;
            String uid = (String)uidIterator.next();
            HashSet currentUidEmailSet = getEmailsFromProfilePage(uid);
            referralPage = "http://www.friendster.com/" + uid;
            System.out.println((uidCount) + ") " + uid + " has " + currentUidEmailSet.size() + " emails");
            emailSet.addAll(currentUidEmailSet);
            
            long random = Math.round(Math.random()*(200)* 100);
            System.out.println("Sleep for " + random + " secs");
            Thread.sleep(random);
        }*/
        
        // Randomly sleeps between 5 - 60 seconds
        List contacts = new ArrayList();
        contacts.addAll(emailSet);
        String foundEmails = EmailAddressHelper.getEmailAddressString(emailSet);
        System.out.println("\n\n\n emails=" +foundEmails);
        return contacts;  
        
    }
    protected List<Contact> parseContactsDeep(InputStream contactsContent) throws Exception {
        
        // Read content for "http://profiles.friendster.com/60621609", cut the user id, and continue
        String keyMatchStart = "\"http://profiles.friendster.com/";
        String keyMatchEnd = "\"><img";
        HashSet firstLevelUserIdSet = new HashSet();
        HashSet secondLevelUserIdSet = new HashSet(); // we always collect minimum 30 persons
        HashSet thirdLevelUserIdSet = new HashSet(); // we always collect minimum 30 persons
        HashSet processedUserIdSet = new HashSet(); // So that do not reprocess same user
        
        HashSet emailSet = new HashSet();
        
        BufferedReader in=new BufferedReader(new InputStreamReader(contactsContent));
        String line;
        int i=0;
        String userId = null;
        while ((line = in.readLine()) != null) {
            //System.out.println("line=" + line);
            int lineIndex = line.indexOf(keyMatchStart);
            if (lineIndex> -1) {
                int lineEndIndex = line.indexOf(keyMatchEnd,lineIndex);
                if (lineEndIndex > -1) {
                    userId = line.substring(lineIndex + keyMatchStart.length(),line.indexOf(keyMatchEnd,lineIndex));
                    System.out.println("Found (1st level) userId=" + userId);
                    // Try get long, might be username
                    if (userId.trim().endsWith("\"")) userId = userId.substring(0,userId.trim().length()-1);
                    Long validUserId;
                    try {
                        validUserId = new Long(userId); 
                        if (validUserId != null && validUserId > 0){
                            firstLevelUserIdSet.add(userId);
                        }
                    } catch (NumberFormatException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        
        // For every user id not in processed, get their profile page and parses for email
        Iterator firstLevelUserIterator = firstLevelUserIdSet.iterator();
        while (firstLevelUserIterator.hasNext()) {
            String uid = (String)firstLevelUserIterator.next();
            if (!processedUserIdSet.contains(uid)) {
                // Get emails
                HashSet currentUidEmailSet = getEmailsFromProfilePage(uid);
                System.out.println(uid + " has " + currentUidEmailSet.size() + " emails");
                emailSet.addAll(currentUidEmailSet);
                
                // Get second level friends of this user and
                // adds to second level for later processing
                HashSet currentUidFriendsSet = getUidFromFriendsPage(uid);
                System.out.println(uid + " has " + currentUidFriendsSet.size() + "friends");
                secondLevelUserIdSet.addAll(currentUidFriendsSet);
                
                // Add into "processed"
                processedUserIdSet.add(uid);
            }
            
            long random = Math.round(Math.random()*(60)* 100);
            System.out.println("Sleep for " + random + " secs");
            Thread.sleep(random);
        }
        
        // Now running second level
        Iterator secondLevelUserIterator = secondLevelUserIdSet.iterator();
        while (secondLevelUserIterator.hasNext()) {
            String uid = (String)secondLevelUserIterator.next();
            if (!processedUserIdSet.contains(uid)) {
                // Get emails
                HashSet currentUidEmailSet = getEmailsFromProfilePage(uid);
                System.out.println(uid + " has " + currentUidEmailSet.size() + " emails");
                emailSet.addAll(currentUidEmailSet);
                
                // Get second level friends of this user and
                // adds to second level for later processing
                HashSet currentUidFriendsSet = getUidFromFriendsPage(uid);
                System.out.println(uid + " has " + currentUidFriendsSet.size() + "friends");
                thirdLevelUserIdSet.addAll(currentUidFriendsSet);
                
                // Add into "processed"
                processedUserIdSet.add(uid);
            }
            
            long random = Math.round(Math.random()*(60)* 10);
            System.out.println("Sleep for " + random + " secs");
            Thread.sleep(random);
        }
        
        System.out.println("Total Emails found?:" + emailSet.size());
        System.out.println("Total 1st level users ?:" + firstLevelUserIdSet.size());
        System.out.println("Total 2nd level users ?:" + secondLevelUserIdSet.size());
        System.out.println("Total 3rd level users ?:" + thirdLevelUserIdSet.size());
        System.out.println("Total Processed users ?:" + processedUserIdSet.size());
        // After done, get list of his friends if available, if not proceed to the next one..
        // We shall only get one by one, we do not need to get all second level friends list
        
        // Randomly sleeps between 5 - 60 seconds
        List contacts = new ArrayList();
        contacts.addAll(emailSet);
        String foundEmails = EmailAddressHelper.getEmailAddressString(emailSet);
        System.out.println("\n\n\n emails=" +foundEmails);
        return contacts;
    }
    
    private HashSet getEmailsFromProfilePage(String uid) {
        String content = null;
        try{
            System.out.println("Getting profile page of " + uid);
            content=this.readInputStream(
                    this.doGet(getHttpClient(), "http://www.friendster.com/" + uid.toString(), referralPage)
                    );
        } catch (Exception e) {
            System.out.println("Unable to get emails from profile page of " + uid);
        }
        
        if (content.indexOf("Log In")>0)
        {
            System.out.println("WARNING!! LOG IN text found!!");
            System.out.println(content);
        }
        
        return EmailAddressHelper.getEmailAddress(content);
    }
    
    private HashSet getUidFromFriendsPage(String uid) {
        HashSet returnUidSet = new HashSet();
        if (uid == null) return returnUidSet;
        // Read content for "http://profiles.friendster.com/60621609", cut the user id, and continue
        String keyMatchStart = "\"http://profiles.friendster.com/";
        String keyMatchEnd = "\"><img";
        
        InputStream inputStream = null;
        try{
            
            System.out.println("Getting friends page of " + uid);
            inputStream = this.doGet(getHttpClient(), "http://www.friendster.com/friends/" + uid.toString(), getContactListURL());
            
            BufferedReader in=new BufferedReader(new InputStreamReader(inputStream));
            String line;
            int i=0;
            String userId = null;
            while ((line = in.readLine()) != null) {
                int lineIndex = line.indexOf(keyMatchStart);
                if (lineIndex> -1) {
                    int lineEndIndex = line.indexOf(keyMatchEnd,lineIndex);
                    if (lineEndIndex > -1) {
                        userId = line.substring(lineIndex + keyMatchStart.length(),line.indexOf(keyMatchEnd,lineIndex));
                        System.out.println("Found (2nd level) userId=" + userId);
                        // Try get long, might be username
                        if (userId.trim().endsWith("\"")) userId = userId.substring(0,userId.trim().length()-1);
                        Long validUserId;
                        try {
                            validUserId = new Long(userId);

                            if (validUserId != null && validUserId > 0){
                                returnUidSet.add(userId);
                            } 
                        } catch (NumberFormatException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
            
            
        } catch (Exception e) {
            System.out.println("Unable to get emails from profile page of " + uid);
        }
        
        return returnUidSet;
        
    }
}
