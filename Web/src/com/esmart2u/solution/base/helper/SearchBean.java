/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// SearchBean.java

package com.esmart2u.solution.base.helper;

import java.util.List;
import java.io.Serializable;

/**
 * Bean for wrapping searching criteria as well as searching result.
 *
 * @author  Cheng Lai Ann
 * @version $Id: SearchBean.java,v 1.2 2003/12/09 04:39:56 wkchee Exp $
 */

public class SearchBean
    implements Serializable, Cloneable
{
    public static final String SEARCH_BY_CODE = "code";
    public static final String SEARCH_BY_DESCRIPTION = "description";

    private String criteria;
    private String value;
    private int currentPage;
    private int totalPage;
    private transient List resultList;
    private String resultLinkKey;
    private boolean searchExecuted;

    public SearchBean()
    {
        init();
    }

    public void init()
    {
        criteria = null;
        value = null;
        currentPage = 1;
        totalPage = 1;
        resultList = null;
        resultLinkKey = null;
        searchExecuted = false;
    }

    public String getCriteria()
    {
        if (criteria != null) criteria = criteria.trim();
        return criteria;
    }

    public void setCriteria(String criteria)
    {
        this.criteria = criteria;
    }

    public String getValue()
    {
        if (value != null) value = value.trim();
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public void setCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
    }

    public int getTotalPage()
    {
        return totalPage;
    }

    public void setTotalPage(int totalPage)
    {
        this.totalPage = totalPage;
    }

    public List getResultList()
    {
        return resultList;
    }

    public void setResultList(List resultList)
    {
        this.resultList = resultList;
    }

    public String getResultLinkKey()
    {
        if (resultLinkKey != null) resultLinkKey = resultLinkKey.trim();
        return resultLinkKey;
    }

    public void setResultLinkKey(String resultLinkKey)
    {
        this.resultLinkKey = resultLinkKey;
    }

    public boolean isSearchExecuted()
    {
        return searchExecuted;
    }

    public void setSearchExecuted(boolean searchExecuted)
    {
        this.searchExecuted = searchExecuted;
    }

    /**
     * Pre-validate paging parameter before searching.
     */
    public void beforeSearch()
    {
		if (currentPage < 1)
        {
            currentPage = 1;
        }
    }

    /**
     * Post-validate paging parameter after searching.
     */
    public void afterSearch()
    {
        // checking to avoid invalid page number shown on screen; if current page requested is
        // greater than total pages available, set current page count to total page available
        if (currentPage > totalPage)
        {
            currentPage = totalPage;
        }
        searchExecuted = true;
    }

    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }
}

// end of SearchBean.java