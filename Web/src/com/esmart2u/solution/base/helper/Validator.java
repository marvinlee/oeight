 
package com.esmart2u.solution.base.helper;

 
import java.io.PrintStream;
import java.util.Date;
import java.util.Locale;

// Referenced classes of package com.integrosys.base.techinfra.validation:
//            ValidatorConstant, ValidateUtil, RegExValidate

public class Validator
    implements ValidatorConstant
{

    private Validator()
    {
    }

    public static boolean checkPattern(String target, String myPattern)
    {
        return ValidateUtil.checkPattern(target, myPattern);
    }

    public static boolean validateInteger(String input, boolean check, int min, int max)
    {
        boolean result = RegExValidate.validateInteger(input, check, min, max);
        return result;
    }

    public static boolean validateAmount(String input, boolean check, double min, double max, String currencyCode, Locale locale)
    {
        boolean result = RegExValidate.validateAmount(input, check, min, max, currencyCode, locale);
        return result;
    }

    /**
     * @deprecated Method validateAmount is deprecated
     */

    public static boolean validateAmount(String input, boolean check, double min, double max, Locale locale)
    {
        boolean result = RegExValidate.validateAmount(input, check, min, max, locale);
        return result;
    }

    /**
     * @deprecated Method validateAmount is deprecated
     */

    public static boolean validateAmount(String input, boolean check, double min, double max, String currencyCode)
    {
        boolean result = RegExValidate.validateAmount(input, check, min, max, currencyCode, Locale.getDefault());
        return result;
    }

    /**
     * @deprecated Method validateAmount is deprecated
     */

    public static boolean validateAmount(String input, boolean check, Locale locale)
    {
        boolean result = RegExValidate.validateAmount(input, check, locale);
        return result;
    }

    /**
     * @deprecated Method validateAmount is deprecated
     */

    public static boolean validateAmount(String input, boolean check, String currencyCode)
    {
        boolean result = RegExValidate.validateAmount(input, check, currencyCode);
        return result;
    }

    public static boolean validateDate(String input, boolean check, Locale locale)
    {
        boolean result = RegExValidate.validateDate(input, check, locale);
        return result;
    }

    public static int compareDate(String input, String comparedDate)
        throws Exception
    {
        return RegExValidate.compareDate(input, true, comparedDate);
    }

    public static int compareDate(String input, Date comparedDate)
        throws Exception
    {
        return RegExValidate.compareDate(input, true, comparedDate);
    }

    public static boolean validatePhoneNumber(String input, boolean check, Locale locale)
    {
        boolean result = RegExValidate.validatePhoneNumber(input, check, locale);
        return result;
    }

    public static boolean validateEmail(String input, boolean check)
    {
        boolean result = RegExValidate.validateEmail(input, check);
        return result;
    }

    public static boolean validateNumber(String input, boolean check, double min, double max)
    {
        boolean result = RegExValidate.validateNumber(input, check, min, max);
        return result;
    }

    /**
     *  This method allows for mandatory check of a field but also allows skipping of checking special characters.
     *  However, front end should make a filter="true" to not display actual HTML code.
     */
    public static boolean validateString(String input, boolean mandatoryCheck, int min, int max, boolean checkSpecialChars)
    {
        if (checkSpecialChars)
        {
            return validateString(input, mandatoryCheck, min, max);
        }
        else
        {
            return RegExValidate.validateString(input, mandatoryCheck, min, max);
        }
            
        /*boolean result1 = RegExValidate.validateString(input, mandatoryCheck, min, max);
        if(result1)
            return validateSwiftCode(input, mandatoryCheck);
        else
            return false;*/
    }
    
    public static boolean validateString(String input, boolean mandatoryCheck, int min, int max)
    {
        boolean result1 = RegExValidate.validateString(input, mandatoryCheck, min, max);
        if(result1)
            return validateSwiftCode(input, mandatoryCheck);
        else
            return false;
    }

    public static boolean validateTextBox(String input, boolean check, int minLength, int maxLength, int minHeight, int maxHeight)
    {
        boolean result1 = RegExValidate.validateTextBox(input, check, minLength, maxLength, minHeight, maxHeight);
        if(result1)
            return validateSwiftCode(input, check);
        else
            return false;
    }

    public static boolean validateMandatoryField(String input)
    {
        boolean result = true;
        if(input != null)
            input = input.trim();
        if(input == null || input.equals(""))
            result = false;
        else
            result = true;
        return result;
    }

    public static boolean validateSwiftCode(String input, boolean check)
    {
        boolean result = RegExValidate.validateSwiftCode(input, check);
        //DefaultLogger.debug("Validator", "SWIFT VALIDATION for :" + input + " ............" + result);
        return result;
    }

    public static String checkInteger(String input, boolean check, int min, int max)
    {
        String result = ValidateUtil.validateInteger(input, check, min, max);
        return result;
    }

    public static String checkAmount(String input, boolean check, double min, double max, String currencyCode, Locale locale)
    {
        String result = ValidateUtil.validateAmount(input, check, min, max, currencyCode, locale);
        //DefaultLogger.debug("", "result from checkAmount of " + input + " is " + result);
        return result;
    }

    public static String checkDate(String input, boolean check, Locale locale)
    {
        String result = ValidateUtil.validateDate(input, check, locale);
        return result;
    }

    public static String checkPhoneNumber(String input, boolean check, Locale locale)
    {
        return checkString(input, check, 1, 20);
    }

    public static String checkEmail(String input, boolean check)
    {
        String result = ValidateUtil.validateEmail(input, check);
        return result;
    }

    public static String checkNumber(String input, boolean check, double min, double max, int decimalPlaces, Locale locale)
    {
        String result = ValidateUtil.validateNumber(input, check, min, max, decimalPlaces, locale);
        return result;
    }

    public static String checkNumber(String input, boolean check, double min, double max)
    {
        String result = ValidateUtil.validateNumber(input, check, min, max);
        return result;
    }

    public static String checkString(String input, boolean check, int min, int max)
    {
        String result1 = ValidateUtil.validateString(input, check, min, max);
        return result1;
    }

    public static String checkSWIFT(String input, boolean check, int min, int max)
    {
        String result1 = ValidateUtil.validateString(input, check, min, max);
        //DefaultLogger.debug("", "check string of " + input + " is " + result1);
        if(result1.equals("noerror"))
        {
            String rr = checkSwiftCode(input, check);
            //DefaultLogger.debug("", "check string of " + input + " is " + rr);
            return rr;
        } else
        {
            return result1;
        }
    }

    public static String checkNumericString(String input, boolean check, int min, int max)
    {
        String result1 = ValidateUtil.validateNumericString(input, check, min, max);
        //DefaultLogger.debug("", "check string of " + input + " is " + result1);
        return result1;
    }

    public static String checkTextBox(String input, boolean check, int minLength, int maxLength, int minHeight, int maxHeight)
    {
        String result1 = ValidateUtil.validateTextBox(input, check, minLength, maxLength, minHeight, maxHeight);
        //DefaultLogger.debug("", "error code is " + result1);
        return result1;
    }

    public static String checkSWIFTTextBox(String input, boolean check, int minLength, int maxLength, int minHeight, int maxHeight)
    {
        String result1 = ValidateUtil.validateTextBox(input, check, minLength, maxLength, minHeight, maxHeight);
        //DefaultLogger.debug("", "error code is " + result1);
        if(result1.equals("noerror"))
            return checkSwiftCode(input, check);
        else
            return result1;
    }

    public static String checkSwiftCode(String input, boolean check)
    {
        String result = ValidateUtil.validateSwiftCode(input, check);
        //DefaultLogger.debug("Validator", "SWIFT VALIDATION for :" + input + " ............" + result);
        return result;
    }

    public static void main(String args[])
    {
        Validator V = new Validator();
        Locale locale = new Locale("en", "SG");
        try
        {
            Date dd = new Date();
            System.out.println("CurrentDate :" + dd);
            System.out.println("Compare Date " + compareDate("11 Jan 1122", new Date()));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        String a = "";
        System.out.println("Validate String with SWIFT");
        if(validateString(a, false, 1, 100))
            System.out.println("true");
        else
            System.out.println("false");
        System.out.println("Validate Text Box");
        if(validateTextBox(a, false, 1, 100, 1, 3))
            System.out.println("true");
        else
            System.out.println("false");
        System.out.println("Check Mandatory field :" + validateMandatoryField(null));
        System.out.println(locale.getVariant());
        Date date = new Date();
    }

    public static boolean checkDoubleDigits(String dValue, int integerDigits, int decimalDigits, boolean negativeAllowed)
    {
        return RegExValidate.checkDoubleDigits(dValue, integerDigits, decimalDigits, negativeAllowed);
    }

    public static String checkStringWithNoSpecialCharsAndSpace(String input, boolean check, int minLength, int maxLength)
    {
        String errorCode = "";
        if(!(errorCode = checkString(input, check, minLength, maxLength)).equals("noerror"))
            return errorCode;
        if(!isValidLoginIdString(input, check))
            return "format";
        else
            return "noerror";
    }

    private static boolean isValidLoginIdString(String str, boolean check)
    {
        if(!check && (str == null || str.trim().equals("")))
            return true;
        if(check && (str == null || str.trim().equals("")))
            return false;
        for(int i = 0; i < str.length(); i++)
        {
            if(Character.isWhitespace(str.charAt(i)))
                return false;
            if(!isValidEnglishCharOrDigit(str.charAt(i)) && str.charAt(i) != '_' && str.charAt(i) != '.')
                return false;
        }

        return true;
    }

    private static boolean isValidEnglishCharOrDigit(char ch)
    {
        return ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' || ch >= '0' && ch <= '9';
    }
}
