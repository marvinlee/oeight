/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// UUIDProvider.java

package com.esmart2u.solution.base.helper;

import java.security.SecureRandom;
import java.net.UnknownHostException;
import java.net.InetAddress;

import com.esmart2u.solution.base.logging.Logger;

/**
 * UUID Generator.
 *
 * @author  Chee Weng Keong
 * @version $Id: UUIDProvider.java,v 1.1 2003/12/05 13:32:47 wkchee Exp $
 */

public class UUIDProvider
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(UUIDProvider.class);

    private static SecureRandom seeder;
    private static String midValue;
    private static String midValueUnformatted;

    static
    {
        try
        {
            StringBuffer formatted = new StringBuffer();
            StringBuffer unformatted = new StringBuffer();
            seeder = new SecureRandom();
            InetAddress inetaddress = InetAddress.getLocalHost();
            byte abyte[] = inetaddress.getAddress();
            String s = hexFormat(getInt(abyte), 8);
            String s1 = hexFormat(new Object().hashCode(), 8);

            formatted.append("-");
            unformatted.append(s.substring(0, 4));
            formatted.append(s.substring(0, 4));
            formatted.append("-");
            unformatted.append(s.substring(4));
            formatted.append(s.substring(4));
            formatted.append("-");
            unformatted.append(s1.substring(0, 4));
            formatted.append(s1.substring(0, 4));
            formatted.append("-");
            unformatted.append(s1.substring(4));
            formatted.append(s1.substring(4));
            midValue = formatted.toString();
            midValueUnformatted = unformatted.toString();
            seeder.nextInt();
        }
        catch (UnknownHostException e)
        {
            logger.error(e.getMessage());
        }
    }

    private static int getInt(byte abyte0[])
    {
        int i = 0;
        int j = 24;
        for (int k = 0; j >= 0; k++)
        {
            int l = abyte0[k] & 0xff;
            i += l << j;
            j -= 8;
        }

        return i;
    }

    private static String hexFormat(int i, int j)
    {
        String s = Integer.toHexString(i);
        return padHex(s, j) + s;
    }

    private static String padHex(String s, int i)
    {
        StringBuffer stringbuffer = new StringBuffer();
        if (s.length() < i)
        {
            for (int j = 0; j < i - s.length(); j++)
                stringbuffer.append("0");
        }
        return stringbuffer.toString();
    }

    private static String getVal(String s)
    {
        long l = System.currentTimeMillis();
        int i = (int)l & -1;
        int j = seeder.nextInt();
        String value = hexFormat(i, 8) + s + hexFormat(j, 8);
        if (logger.isDebugEnabled())
        {
            logger.debug("uuid=" + value);
        }
        return value;
    }

    /**
     * Method getFormattedUUID - return a formatted UUID.
     *
     * @return Formatter UUID.
     */
    public static String getFormattedUUID()
    {
        return getVal(midValue);
    }

    /**
     * Method getUnformattedUUID - return a formatted UUID.
     *
     * @return Raw UUID.
     */
    public static String getRawUUID()
    {
        return getVal(midValueUnformatted);
    }

    /**
     * To generate UUID.
     *
     * @return UUID.
     */
    public static String getUUID()
    {
        return getRawUUID().toUpperCase();
    }
}

// end of UUIDProvider.java