/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// StringUtil.java

package com.esmart2u.solution.base.helper;

/**
 * @deprecated Use StringUtils. Will be removed on 29 Feb 2004.
 */

public class StringUtil
{
    /**
     * @deprecated Use StringUtils.convertNullString(String s).
     */
    public static String convertNullString(String s)
    {
        return StringUtils.convertNullString(s);
    }

    /**
     * @deprecated Use StringUtils.toArrayString(Object[] obj).
     */
    public static String toArrayString(Object[] obj)
    {
        return StringUtils.toArrayString(obj);
    }

    /**
     * @deprecated Use StringUtils.parseCsvLine(String cvsLine).
     */
    public static String[] parseCsvLine(String cvsLine)
        throws Exception
    {
        return StringUtils.parseCsvLine(cvsLine);
    }
}

// end of StringUtil.java