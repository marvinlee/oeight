/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// Converter.java

package com.esmart2u.solution.base.helper;

import java.io.*;
import java.math.BigInteger;
import java.math.BigDecimal;

import com.esmart2u.solution.base.logging.Logger;

/**
 * Converter.
 *
 * @author Chee Weng Keong
 * @version $Id: Converter.java,v 1.4 2004/02/18 10:13:20 wkchee Exp $
 */

public class Converter
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(Converter.class);

    /**
     * Converts string to boolean string.
     *
     * @param booleanString String.
     * @return Boolean string.
     */
    public static String toBooleanString(String booleanString)
    {
        if (booleanString != null)
        {
            booleanString = booleanString.toUpperCase();
        }
        if (booleanString != null && booleanString.equals(BaseConstants.BOOLEAN_TYPE_YES))
        {
            return BaseConstants.BOOLEAN_TYPE_YES;
        }
        else
        {
            return BaseConstants.BOOLEAN_TYPE_NO;
        }
    }

    /**
     * Converts boolean to string.
     *
     * @param booleanValue Boolean value.
     * @return Boolean string.
     */
    public static String toBooleanString(boolean booleanValue)
    {
        return booleanValue ? BaseConstants.BOOLEAN_TYPE_YES : BaseConstants.BOOLEAN_TYPE_NO;
    }

    /**
     * Converts string to boolean.
     *
     * @param booleanString Boolean string.
     * @return Boolean value.
     */
    public static boolean toBooleanValue(String booleanString)
    {
        if (booleanString != null && booleanString.equalsIgnoreCase(BaseConstants.BOOLEAN_TYPE_YES))
        {
            return true;
        }
        return false;
    }

    /**
     * Converts string to integer.
     *
     * @param intString Integer string.
     * @return Integer value.
     */
    public static int toIntValue(String intString)
    {
        try
        {
            return Integer.parseInt(intString);
        }
        catch (NumberFormatException e)
        {
            return 0;
        }
    }

    /**
     * Converts string to big integer.
     *
     * @param bigIntegerValue Big integer string.
     * @return Big integer object. Returns null if conversion errors.
     */
    public static BigInteger toBigInteger(String bigIntegerValue)
    {
        return toBigInteger(bigIntegerValue, "");
    }

    /**
     * Converts string to big integer.
     *
     * @param bigIntegerValue Big integer string.
     * @param defaultValue    Default big integer string when conversion fails.
     * @return Big integer object. Returns null if conversion errors.
     */
    public static BigInteger toBigInteger(String bigIntegerValue, String defaultValue)
    {
        try
        {
            return new BigInteger(bigIntegerValue);
        }
        catch (Exception e)
        {
            try
            {
                return new BigInteger(defaultValue);
            }
            catch (Exception e1)
            {
                return null;
            }
        }
    }

    /**
     * Converts big integer to string.
     *
     * @param bigInteger Big integer object.
     * @return Big integer string.
     */
    public static String toBigIntegerString(BigInteger bigInteger)
    {
        if (bigInteger == null)
        {
            return null;
        }
        else
        {
            return bigInteger.toString();
        }
    }

    /**
     * Converts string to big decimal.
     *
     * @param bigDecimalValue Big decimal string.
     * @return Big decimal object. Returns null if conversion errors.
     */
    public static BigDecimal toBigDecimal(String bigDecimalValue)
    {
        return toBigDecimal(bigDecimalValue, "");
    }

    /**
     * Converts string to big decimal.
     *
     * @param bigDecimalValue Big decimal string.
     * @param defaultValue    Default big decimal string when conversion fails.
     * @return Big decimal object. Returns null if conversion errors.
     */
    public static BigDecimal toBigDecimal(String bigDecimalValue, String defaultValue)
    {
        try
        {
            return new BigDecimal(bigDecimalValue);
        }
        catch (Exception e)
        {
            try
            {
                return new BigDecimal(defaultValue);
            }
            catch (Exception e1)
            {
                return null;
            }
        }
    }

    /**
     * Converts big decimal to string.
     *
     * @param bigDecimal Big decimal object.
     * @return Big decimal string.
     */
    public static String toBigDecimalString(BigDecimal bigDecimal)
    {
        if (bigDecimal == null)
        {
            return null;
        }
        else
        {
            return bigDecimal.toString();
        }
    }

    /**
     * Converts object to bytes.
     *
     * @param object Object
     * @return Bytes.
     * @deprecated Use ObjectUtils.getByteArray(Object). Will be removed on 29 Feb 2004.
     */
    public static byte[] getByteRepresentation(Object object)
    {
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            byte[] byteArray = baos.toByteArray();
            return byteArray;
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }

        return null;
    }

    /**
     * Converts bytes to object.
     *
     * @param byteArray Bytes.
     * @return Object.
     * @deprecated Use ObjectUtils.getObject(byte[]). Will be removed on 29 Feb 2004.
     */
    public static Object getObjectRepresentation(byte[] byteArray)
    {
        try
        {
            ByteArrayInputStream bais = new ByteArrayInputStream(byteArray);
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }

        return null;
    }
}

// end of Converter.java