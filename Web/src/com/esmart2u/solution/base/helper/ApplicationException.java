/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ApplicationException.java

package com.esmart2u.solution.base.helper;

import java.util.StringTokenizer;
//import javax.ejb.EJBException;

import com.esmart2u.solution.base.helper.UUIDProvider;
import com.esmart2u.solution.base.logging.Logger;

/**
 * Application exception.
 *  
 */

public class ApplicationException extends Exception
{
    private static Logger logger = Logger.getLogger(ApplicationException.class);

    private String referenceNo;

    public ApplicationException(Throwable throwable)
    {
        // take the last portion of uuid as reference no
        this.referenceNo = UUIDProvider.getFormattedUUID().toUpperCase();
        StringTokenizer st = new StringTokenizer(this.referenceNo, "-");
        while (st.hasMoreTokens())
        {
            this.referenceNo = st.nextToken();
        }

        if (throwable instanceof ApplicationException)
        {
            this.referenceNo = ((ApplicationException)throwable).getReferenceNo();
        }
        else if (throwable instanceof BusinessException)
        {
            logger.warn("You should not wrap BusinessException in ApplicationException!");
            logger.error(getMessage(), throwable);
        }
       /* else if (throwable instanceof EJBException)
        {
            Exception causedByException = ((EJBException)throwable).getCausedByException();
            logger.error(getMessage(), (causedByException != null)? causedByException : throwable);
        }*/
        else
        {
            logger.error(getMessage(), throwable);
        }
    }

    public String getReferenceNo()
    {
        return referenceNo;
    }

    public String getMessage()
    {
        return "APPLICATION EXCEPTION (" + referenceNo + ")";
    }
}

// end of ApplicationException.java