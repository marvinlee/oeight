/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BusinessException.java

package com.esmart2u.solution.base.helper;

import com.esmart2u.solution.base.logging.Logger;

/**
 * Business exception.
 *
 * @author  Chee Weng Keong
 * @version $Id: BusinessException.java,v 1.1 2003/12/05 13:32:47 wkchee Exp $
 */

public class BusinessException extends Exception
{
    private static Logger logger = Logger.getLogger(BusinessException.class);

    private String code;
    private Object[] arguments;

    public BusinessException(String code)
    {
        this.code = code;
        logger.debug(getMessage());
    }

    public BusinessException(String code, Object[] arguments)
    {
        this.code = code;
        this.arguments = arguments;
        logger.debug(getMessage());
    }

    public String getCode()
    {
        return code;
    }

    public Object[] getArguments()
    {
        return arguments;
    }

    public void setArguments(Object[] arguments)
    {
        this.arguments = arguments;
    }

    public String getMessage()
    {
        return "BUSINESS EXCEPTION (" + code + ")";
    }
}

// end of BusinessException.java