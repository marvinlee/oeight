/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// UUIDGenerator.java

package com.esmart2u.solution.base.helper;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.SecureRandom;

import com.esmart2u.solution.base.logging.Logger;

/**
 * UUIDGenerator based on the UUID for EJB primary key generation algorithm
 * in EJB Design Pattern book by Floyd Marinescu (Wiley)
 *
 * @deprecated Use UUIDProvider. Will be removed on 29 Feb 2004.
 */

public class UUIDGenerator
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(UUIDGenerator.class);

    /**
     * Instance.
     */
    private static final UUIDGenerator uuidgen = new UUIDGenerator();

    private static SecureRandom seeder;
    private static String midValue;
    private static String midValueUnformatted;

    static
    {
        try
        {
            StringBuffer formatted = new StringBuffer();
            StringBuffer unformatted = new StringBuffer();
            seeder = new SecureRandom();
            InetAddress inetaddress = InetAddress.getLocalHost();
            byte abyte[] = inetaddress.getAddress();
            String s = hexFormat(getInt(abyte), 8);
            String s1 = hexFormat(new Object().hashCode(), 8);

            formatted.append("-");
            unformatted.append(s.substring(0, 4));
            formatted.append(s.substring(0, 4));
            formatted.append("-");
            unformatted.append(s.substring(4));
            formatted.append(s.substring(4));
            formatted.append("-");
            unformatted.append(s1.substring(0, 4));
            formatted.append(s1.substring(0, 4));
            formatted.append("-");
            unformatted.append(s1.substring(4));
            formatted.append(s1.substring(4));
            midValue = formatted.toString();
            midValueUnformatted = unformatted.toString();
            seeder.nextInt();
        }
        catch (UnknownHostException e)
        {
            logger.error(e.getMessage());
        }
    }

    /**
     * Constructor for UUIDGenerator.
     */
    private UUIDGenerator()
    {
    }

    public static final UUIDGenerator getInstance()
    {
        return uuidgen;
    }

    private static int getInt(byte abyte0[])
    {
        int i = 0;
        int j = 24;
        for (int k = 0; j >= 0; k++)
        {
            int l = abyte0[k] & 0xff;
            i += l << j;
            j -= 8;
        }

        return i;
    }

    private static String hexFormat(int i, int j)
    {
        String s = Integer.toHexString(i);
        return padHex(s, j) + s;
    }

    private static String padHex(String s, int i)
    {
        StringBuffer stringbuffer = new StringBuffer();
        if (s.length() < i)
        {
            for (int j = 0; j < i - s.length(); j++)
                stringbuffer.append("0");
        }
        return stringbuffer.toString();
    }

    private String getVal(String s)
    {
        long l = System.currentTimeMillis();
        int i = (int)l & -1;
        int j = seeder.nextInt();
        String value = hexFormat(i, 8) + s + hexFormat(j, 8);
        if (logger.isDebugEnabled())
        {
            logger.debug("uuid=" + value);
        }
        return value;
    }

    /**
     * Method getFormattedUUID - return a formatted UUID.
     *
     * @return Formatter UUID.
     */
    public String getFormattedUUID()
    {
        return getVal(midValue);
    }

    /**
     * Method getUnformattedUUID - return a formatted UUID.
     *
     * @return Raw UUID.
     */
    public String getRawUUID()
    {
        return getVal(midValueUnformatted);
    }
}

// end of UUIDGenerator.java