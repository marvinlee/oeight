/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// DateUtil.java

package com.esmart2u.solution.base.helper;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.sql.Date;

import com.esmart2u.solution.base.logging.Logger;

/**
 * @deprecated No longer support. Will be removed on 29 Feb 2004.
 */

public class DateUtil
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(DateUtil.class);

    /**
     * @deprecated Use DateUtils.getDateDifferenceInDay(Calendar calendar1, Calendar calendar2)
     */
    public static long getDateDifferenceInDay(Calendar calendar1, Calendar calendar2)
    {
        return DateUtils.getDateDifferenceInDay(calendar1, calendar2);
    }

    /**
     * @deprecated Use DateUtils.getDateDifferenceInDay(Date date1, Date date2)
     */
    public static long getDateDifferenceInDay(Date date1, Date date2)
    {
        return DateUtils.getDateDifferenceInDay(date1, date2);
    }

    /**
     * @deprecated Use DateUtils.getDateDifferenceInHour(Date date1, Date date2)
     */
    public static long getDateDifferenceInHour(Date date1, Date date2)
    {
        return DateUtils.getDateDifferenceInHour(date1, date2);
    }

    /**
     * @deprecated Use DateUtils.getDateDifferenceInMinute(Date date1, Date date2)
     */
    public static long getDateDifferenceInMinute(Date date1, Date date2)
    {
        return DateUtils.getDateDifferenceInMinute(date1, date2);
    }

    /**
     * @deprecated Use DateUtils.getDateDifferenceInSecond(Date date1, Date date2)
     */
    public static long getDateDifferenceInSecond(Date date1, Date date2)
    {
        return DateUtils.getDateDifferenceInSecond(date1, date2);
    }

    /**
     * @deprecated Use DateUtils.getDateDifferenceInMilisecond(Date date1, Date date2)
     */
    public static long getDateDifferenceInMilisecond(java.sql.Date date1, java.sql.Date date2)
    {
        return DateUtils.getDateDifferenceInMilisecond(date1, date2);
    }

    /**
     * @deprecated DateUtils.getDate(String year, String month, String date)
     */
    public static Date getDate(String year, String month, String date)
    {
        return DateUtils.getDate(year, month, date);
    }

    /**
     * @deprecated Use DateUtils.getDateTime(String year, String month, String date, String hour, String minute, String seconds)
     */
    public static Date getDate(String year, String month, String date, int hour, int minute, int seconds)
    {
        return DateUtils.getDateTime(year, month, date, String.valueOf(hour), String.valueOf(minute), String.valueOf(seconds));
    }

    /**
     * @deprecated Use DateUtils.getDateTime(String year, String month, String date, String hour, String minute, String seconds)
     */
    public static String getDateTime(String year, String month, String date, int hour, int minute, int seconds)
    {
        if (year == null || month == null || date == null || year.length() == 0 || month.length() == 0 || date.length() == 0)
        {
            return null;
        }
        else
        {
            GregorianCalendar gc = new GregorianCalendar(Integer.parseInt(year), Integer.parseInt(month) - 1, Integer.parseInt(date), hour, minute, seconds);
            return new SimpleDateFormat("MMM-dd-yyyy HH:mm a").format(new java.sql.Date(gc.getTime().getTime()));
        }
    }

    /**
     * @deprecated Use DateUtils.convertDateToStrings(Date date)
     */
    public static void setDate(Date value, String year, String month, String date, Object obj)
    {
        try
        {
            Calendar calendar = Calendar.getInstance();
            String calendarYear = "";
            String calendarMonth = "";
            String calendarDate = "";

            if (value != null)
            {
                calendar.setTime(value);
                calendarYear = String.valueOf(calendar.get(Calendar.YEAR));
                if (calendarYear.length() == 1) calendarYear = "0" + calendarYear;
                calendarMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                if (calendarMonth.length() == 1) calendarMonth = "0" + calendarMonth;
                calendarDate = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                if (calendarDate.length() == 1) calendarDate = "0" + calendarDate;
            }

            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(year, obj.getClass());
            Method yearWriteMethod = propertyDescriptor.getWriteMethod();
            Object yearArgs[] = new Object[1];
            yearArgs[0] = calendarYear;
            yearWriteMethod.invoke(obj, yearArgs);
            logger.debug("Setting year for " + year + " : [" + yearArgs[0] + "]");

            propertyDescriptor = new PropertyDescriptor(month, obj.getClass());
            Method monthWriteMethod = propertyDescriptor.getWriteMethod();
            Object monthArgs[] = new Object[1];
            monthArgs[0] = calendarMonth;
            monthWriteMethod.invoke(obj, monthArgs);
            logger.debug("Setting month for " + month + " : [" + monthArgs[0] + "]");

            propertyDescriptor = new PropertyDescriptor(date, obj.getClass());
            Method dateWriteMethod = propertyDescriptor.getWriteMethod();
            Object dateArgs[] = new Object[1];
            dateArgs[0] = calendarDate;
            dateWriteMethod.invoke(obj, dateArgs);
            logger.debug("Setting date for " + date + " : [" + dateArgs[0] + "]");
        }
        catch (Exception e)
        {
            logger.error(e.toString(), e);
        }
    }

    /**
     * @deprecated Use DateUtils.convertDateTimeToStrings(Date dateTime)
     */
    public static void setDateTime(Date value, String year, String month, String date, String hour, String minute, String second, Object obj)
    {
        try
        {
            Calendar calendar = Calendar.getInstance();
            String calendarYear = "";
            String calendarMonth = "";
            String calendarDate = "";
            String calendarHour = "";
            String calendarMinute = "";
            String calendarSecond = "";

            if (value != null)
            {
                calendar.setTime(value);
                calendarYear = String.valueOf(calendar.get(Calendar.YEAR));
                if (calendarYear.length() == 1) calendarYear = "0" + calendarYear;
                calendarMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                if (calendarMonth.length() == 1) calendarMonth = "0" + calendarMonth;
                calendarDate = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                if (calendarDate.length() == 1) calendarDate = "0" + calendarDate;
                calendarHour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
                if (calendarHour.length() == 1) calendarHour = "0" + calendarHour;
                calendarMinute = String.valueOf(calendar.get(Calendar.MINUTE));
                if (calendarMinute.length() == 1) calendarMinute = "0" + calendarMinute;
                calendarSecond = String.valueOf(calendar.get(Calendar.SECOND));
                if (calendarSecond.length() == 1) calendarSecond = "0" + calendarSecond;
            }

            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(year, obj.getClass());
            Method yearWriteMethod = propertyDescriptor.getWriteMethod();
            Object yearArgs[] = new Object[1];
            yearArgs[0] = calendarYear;
            yearWriteMethod.invoke(obj, yearArgs);
            logger.debug("Setting year for " + year + " : [" + yearArgs[0] + "]");

            propertyDescriptor = new PropertyDescriptor(month, obj.getClass());
            Method monthWriteMethod = propertyDescriptor.getWriteMethod();
            Object monthArgs[] = new Object[1];
            monthArgs[0] = calendarMonth;
            monthWriteMethod.invoke(obj, monthArgs);
            logger.debug("Setting month for " + month + " : [" + monthArgs[0] + "]");

            propertyDescriptor = new PropertyDescriptor(date, obj.getClass());
            Method dateWriteMethod = propertyDescriptor.getWriteMethod();
            Object dateArgs[] = new Object[1];
            dateArgs[0] = calendarDate;
            dateWriteMethod.invoke(obj, dateArgs);
            logger.debug("Setting date for " + date + " : [" + dateArgs[0] + "]");

            propertyDescriptor = new PropertyDescriptor(hour, obj.getClass());
            Method hourWriteMethod = propertyDescriptor.getWriteMethod();
            Object hourArgs[] = new Object[1];
            hourArgs[0] = calendarHour;
            hourWriteMethod.invoke(obj, hourArgs);
            logger.debug("Setting hours for " + hour + " : [" + hourArgs[0] + "]");

            propertyDescriptor = new PropertyDescriptor(minute, obj.getClass());
            Method minuteWriteMethod = propertyDescriptor.getWriteMethod();
            Object minuteArgs[] = new Object[1];
            minuteArgs[0] = calendarMinute;
            minuteWriteMethod.invoke(obj, minuteArgs);
            logger.debug("Setting minutes for " + minute + " : [" + minuteArgs[0] + "]");

            propertyDescriptor = new PropertyDescriptor(second, obj.getClass());
            Method secondWriteMethod = propertyDescriptor.getWriteMethod();
            Object secondArgs[] = new Object[1];
            secondArgs[0] = calendarSecond;
            secondWriteMethod.invoke(obj, secondArgs);
            logger.debug("Setting seconds for " + second + " : [" + secondArgs[0] + "]");
        }
        catch (Exception e)
        {
            logger.error(e.toString(), e);
        }
    }

    /**
     * @deprecated Use DateUtils.getCurrentDate().
     */
    public static Date getCurrentDate()
    {
        return DateUtils.getCurrentDate();
    }

    /**
     * @deprecated Use DateUtils.getCurrentDateTime()
     */
    public static String getCurrentTime()
    {
        GregorianCalendar gc = new GregorianCalendar();
        return (new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss z")).format(new java.sql.Date((gc.getTime()).getTime()));
    }

    /**
     * @deprecated Use DateUtils.getCurrentDateTime().
     */
    public static Date getCurrentDateTime()
    {
        return DateUtils.getCurrentDateTime();
    }

    /**
     * @deprecated Use Formatter.formatDate(...).
     */
    public static String formatDateToDDMMYYYY(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;

        String dayText = (day < 10) ? "0" + day : "" + day;
        String monthText = (month < 10) ? "0" + month : "" + month;

        return dayText + "-" + monthText + "-" + calendar.get(Calendar.YEAR);
    }
}

// end of DateUtil.java