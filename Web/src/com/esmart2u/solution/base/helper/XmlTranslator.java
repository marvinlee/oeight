/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// XmlTranslator.java

package com.esmart2u.solution.base.helper;

import java.io.*;

import org.exolab.castor.xml.*;

import com.esmart2u.solution.base.logging.Logger;

/**
 * This static class is to be used to do conversion between object structure
 * and xml file.
 *
 * @author Wong Chee Peng
 * @version $Id: XmlTranslator.java,v 1.2 2004/03/24 04:35:01 wkchee Exp $
 */

public class XmlTranslator
{
    private static Logger logger = Logger.getLogger(XmlTranslator.class);

    /**
     * @deprecated Use convertXmlToObject(File xmlFile, Class rootClass).
     */
    public static Object convertXmlToObj(File xmlFile, Object rootObject)
    {
        return convertXmlToObject(xmlFile, rootObject.getClass());
    }

    /**
     * @deprecated Use convertXmlToObject(File xmlFile, Class rootClass).
     */
    public static Object convertXmlToObject(File xmlFile, Object rootObject)
    {
        return convertXmlToObject(xmlFile, rootObject.getClass());
    }

    /**
     * This method is to convert and return to an object structure from the given
     * xml file.
     *
     * @param xmlFile   Source xml file use to convert to object structure.
     * @param rootClass The root class that the translator based on to re-construct back
     *                  into the original structure.
     * @return Re-constructed object structure by the translator.
     */
    public static Object convertXmlToObject(File xmlFile, Class rootClass)
    {
        Object returnObject = null;
        FileReader reader = null;
        try
        {
            // Create a Reader to the file to unmarshall from
            reader = new FileReader(xmlFile);

            // Unmarshall the passed in xml file
            returnObject = Unmarshaller.unmarshal(rootClass, reader);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                // Close the file reader
                if (reader != null)
                {
                    reader.close();
                }
            }
            catch (IOException e)
            {
                // ignore quietly
            }
        }
        return returnObject;
    }

    /**
     * This method is to convert and return to an object structure from the given
     * xml string.
     *
     * @param xmlString Source xml string use to convert to object structure.
     * @param rootClass The root class that the translator based on to re-construct back
     *                  into the original structure.
     * @return Re-constructed object structure by the translator.
     */
    public static Object convertXmlStringToObject(String xmlString, Class rootClass)
    {
        StringReader reader = null;
        Object returnObject = null;
        try
        {
            // Create reader to the object to unmarshall from
            reader = new StringReader(xmlString);

            // Unmarshall the passed in xml string
            returnObject = Unmarshaller.unmarshal(rootClass, reader);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }
        finally
        {
            // Close the file reader
            if (reader != null)
            {
                reader.close();
            }
        }
        return returnObject;
    }

    /**
     * @deprecated Use convertObjectToXml(Object rootObject, File xmlFile).
     */
    public static void convertObjToXml(Object rootObject, File xmlFile)
    {
        convertObjectToXml(rootObject, xmlFile);
    }

    /**
     * This method is to convert to xml file from a given object structure.
     *
     * @param rootObject Root object that the translator use to construct xml file.
     * @param xmlFile    A given destinated xml file to be created.
     */
    public static void convertObjectToXml(Object rootObject, File xmlFile)
    {
        FileWriter writer = null;
        try
        {
            // Create a File to marshal to
            writer = new FileWriter(xmlFile);

            // Marshall the passed in object
            Marshaller.marshal(rootObject, writer);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                // Close the file writer
                if (writer != null)
                {
                    writer.flush();
                    writer.close();
                }
            }
            catch (IOException e)
            {
                // ignore quietly
            }
        }
    }

    /**
     * This method is to convert to xml string from a given object structure.
     *
     * @param rootObject Root object that the translator use to construct xml file.
     */
    public static String convertObjectToXmlString(Object rootObject)
    {
        StringWriter writer = null;
        String string = null;
        try
        {
            // Create writer to the object marshall to
            writer = new StringWriter();

            // Marshall the passed in object
            Marshaller.marshal(rootObject, writer);
            string = writer.getBuffer().toString();
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }
        finally
        {
            try
            {
                // Cleanup
                if (writer != null)
                {
                    writer.flush();
                    writer.close();
                }
            }
            catch (IOException e)
            {
                // ignore quietly
            }
        }
        return string;
    }
}

// end of XmlTranslator.java