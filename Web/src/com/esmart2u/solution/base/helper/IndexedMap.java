/**
 * � 2003 esmart2u Malaysia Sdn. Bhd.  All rights reserved.
 * MarvinLee.net
 * 
 * 
 *
 * This software is the intellectual property of esmart2u Malaysia Sdn. Bhd.
 * The program may be used only in accordance with the terms of the license
 * agreement you entered into with esmart2u Malaysia Sdn. Bhd.
 *
 * User: Benjamin
 * Computer: Afinity
 * Date: 02-Oct-2003
 * Time: 19:16:18
 */

package com.esmart2u.solution.base.helper;

import java.util.*;

/**
 * @author Marvin Lee, esmart2u Malaysia Sdn Bhd
 */
public class IndexedMap extends HashMap
{
    private int indexCursor = 0;
    private List indexList = new ArrayList();

    public Object put(Object o, Object o1)
    {
        indexList.add(indexCursor, o);
        ++indexCursor;
        return super.put(o, o1);
    }

    public Iterator keyIterator()
    {
        return indexList.iterator();
    }
}
