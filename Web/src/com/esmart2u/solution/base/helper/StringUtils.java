/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// StringUtils.java

package com.esmart2u.solution.base.helper;

import java.util.Vector;
import java.util.Locale;

public class StringUtils
{
    /**
     * Check if the string is not empty and not null.
     *
     * @param s String value.
     * @return True if not empty and not null; Else false.
     */
    public static boolean hasValue(String s)
    {
        return hasValue(s, false);
    }

    /**
     * Check if the string is not empty and not null.
     * When ignoring space, the string will be trimmed before checking.
     *
     * @param s           String value.
     * @param ignoreSpace Flag to ignore space.
     * @return True if not empty and not null; Else false.
     */
    public static boolean hasValue(String s, boolean ignoreSpace)
    {
        if (ignoreSpace)
        {
            return s != null && !s.trim().equals("");
        }
        else
        {
            return s != null && !s.equals("");
        }
    }

    /**
     * Converts the null string to emptry string to avoid NullPointerException
     * during manipulation on string.
     *
     * @param s String value.
     * @return Converted string value.
     */
    public static String convertNullString(String s)
    {
        return (s == null) ? "" : s;
    }

    public static String toArrayString(Object[] obj)
    {
        String arrayString = "";

        if (obj == null || obj.length == 0)
            return arrayString;

        for (int i = 0; i < obj.length; i++)
            arrayString += ", " + obj[i].toString();

        if (arrayString.length() > 2)
            arrayString = arrayString.substring(2);

        return arrayString;
    }

    /**
     * Extract CVS for
     * a,b          ->    a, b
     * "a",b        ->    a, b
     * """a""",b    ->    "a", b
     * "a,",b       ->    a,, b
     * """a,""",b   ->    "a,", b
     * "a""b",c     ->    a"b, c
     */
    public static String[] parseCsvLine(String cvsLine)
        throws Exception
    {
        String cvsLineCopy = cvsLine;
        String singleResult = "";
        Vector resultVector = new Vector();

        char separator = ',';
        boolean isText = false;

        // append a,b -> a,b,
        cvsLine = cvsLine + separator;
        for (int i = 0; i < cvsLine.length();)
        {
            // found " for just begin or just after seperator
            char readChar = cvsLine.charAt(i);
            if (singleResult.length() == 0 && readChar == '"' && !isText)
            {
                i++;
                isText = true;
            }
            else
            {
                // found " after begin flag "
                if (readChar == '"')
                {
                    // check if ""
                    char nextChar = cvsLine.charAt(i + 1);
                    if (nextChar == '"')
                    {
                        singleResult = singleResult + readChar;
                        i++;
                    }
                    // is closing "
                    else
                    {
                        if (!isText)
                            throw new Exception("Unexpected '\"' in position " + i + ". Line=" + cvsLineCopy);
                        if (nextChar != separator)
                            throw new Exception("Expecting " + separator + " in position " + (i + 1) + ". Line=" + cvsLineCopy);
                        resultVector.add(singleResult);
                        singleResult = "";
                        isText = false;
                        i++;
                    }
                }
                // found ,
                else if (readChar == separator)
                {
                    // treat , as text
                    if (isText)
                    {
                        singleResult = singleResult + readChar;
                    }
                    // treat , as seperator
                    else
                    {
                        resultVector.add(singleResult);
                        singleResult = "";
                    }
                }
                // found char
                else
                {
                    singleResult = singleResult + readChar;
                }

                // move to next character
                i++;
            }
        }

        // copy values from vector to an array
        String results[] = new String[resultVector.size()];
        resultVector.copyInto(results);
        return results;
    }

    /**
     * Convert the input into escape unicode string.
     *
     * @param word        Input word.
     * @param escapeAscii Escape ASCII indicator.
     * @return Escape unicode string
     */
    public static String escapeUnicodeString(String word, boolean escapeAscii)
    {
        if (word == null) return null;

        StringBuffer unicodeWord = new StringBuffer();

        int codeSizeDiff = 0;

        for (int i = 0; i < word.length(); i++)
        {
            char ch = word.charAt(i);

            if (!escapeAscii && ((ch >= 0x0020) && (ch <= 0x007e)))
            {
                unicodeWord.append(ch);
            }
            else
            {
                unicodeWord.append("\\u");
                String hex = Integer.toHexString(word.charAt(i) & 0xFFFF);
                codeSizeDiff = 4 - hex.length();

                // pads 0s into the unicode if length is less than 4
                if (codeSizeDiff > 0)
                {
                    for (int diff = 0; diff < codeSizeDiff; diff++)
                    {
                        unicodeWord.append("0");
                    }
                }
                unicodeWord.append(hex.toUpperCase(Locale.ENGLISH));
            }
        }

        return (unicodeWord.toString());
    }
}

// end of StringUtils.java