/*
 * PropertyManager.java
 *
 * Created on September 29, 2007, 11:47 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper;


/**
 * This is a utility class for reading property file.
 *
 * <PRE>
 * The file shall contains key=value pairs.
 *
 * Example:
 * Assume C:\test.txt is a property file contains the following text.
 *
 *  Property file can be got thru two different methods.
 *  1. getInstance() will return the default property in the following order
 *             a) Will look for system property ( -D option ) called PropertyFile if not found,
 * 	      b) Will default to {user.dir}/genesis.property
 *  2. getInstance(fileName) would give an property of that property file name.
 *
 *NOTE: Most of the methods here are static which defaults to getInstance(). If you needs these methods for your own property of property file ( like getInstance(), implement your own !!!.   Anyway basic methods like get are provided which implementor felt is sufficient.
 *
 * Key1 = Key1Value
 * Name = Mr.Smith
 *
 * Below is the sample of getting values from the file
 *
 * ...
 * ...
 * loadEntry()
 * {
 * ...
 *     try
 *     {
 *         PropertyManager pf = PropertyManager.getInstance("C:\\test.txt");
 *         log("Key1 is having a value of " + pf.get("Key1"));
 *         log("Name is having a value of " + pf.get("Name"));
 *     }
 *     catch(Exception e)
 * ...
 * }
 *
 * Note that exception will be thrown from the constructor if error
 * occurred during loading of property file. This is to prevent the
 * caller from using the property in case of error.
 * </PRE>
 *
 * @author $user$
 * @version $Revision: 1.2 $
 * @since $Date: 2004/09/07 10:27:11 $
 */

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

public class PropertyManager  {
    
    private static PropertyManager instance; //= getInstance();
    private static Logger logger = Logger.getLogger(PropertyManager.class);
    
    private LinkedList propertyLoaderList ;
    private Properties props ;
    
    public static Properties getProperty() {
        return getInstance().props;
    }
    
    /**
     * This should be trigger code, some startup class should trigger. But... made as static
     */
    
    
    //static {
    //    instance = getInstance();
    //instance.registerLoader(new DBPropertyLoader(null, "select * from junk"));
    //}
    
    /**
     * This is the method which will load property file that is specified in the /arguement.
     * @params key name of the proeprty file;
     * @return property
     */
    
    public static synchronized PropertyManager getInstance(){
        if ( instance == null ){
            return initInstance();
            //return new PropertyManager();
        }
        return instance;
    }
    
    public PropertyManager() {
        
    }
    
    public static PropertyManager initInstance() {
        String propertyFile="";
        try {
            instance = new PropertyManager();
            System.out.println("Initializing <<<<<<PropertyManager>>>>>>");
            System.out.println("Initializing <<<<<<PropertyManager>>>>>>");
            
            //propertyFile = System.getProperty(PropertyConstants.MAIN_CONFIG_FILE_KEY);
            propertyFile = ConfigurationHelper.getInstance().getPropertyValue("config.startup.path");
            System.out.println("Property File is read from="+ propertyFile);
            instance.props = new Properties();
            System.out.println("Load from the property file");
            File configFile = new File(propertyFile);
            System.out.println("ConfigFile=" + configFile.canRead());
            
            
            
            
            
            BufferedReader br = new BufferedReader(new FileReader(configFile)); 
            
            String str = null;
            
            while ((str = br.readLine()) != null) {
                int equalsIndex = str.indexOf("=");
                if (equalsIndex>0) { 
                    String key = str.substring(0,equalsIndex); 
                    String value = str.substring(equalsIndex+1, str.length());
                    System.out.println("Put key=" + key + ", value=" + value);
                    instance.props.put(key,value);
                }
            }
             
            br.close();
            
             
            System.out.println("PropertyFile loaded =" + instance.props.size());
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error loading propertyFile=" + propertyFile);
        }
        return instance;
    }
    
    public void clearCache() {
        
    }
    
    
    /**
     * This method returns the String value for a key. This version of PropertyManager supports substring search
     * if the property is not found of up to 2 level. The 1st level substring is assumed to use "," as seperators
     * and the 2nd level substring must use ";" as seperator
     * <pre>
     * 	e.g. 1:
     *		jndi.url=t3://localhost:80
     *
     *	get("jndi.url") returns "t3://localhost:80
     *
     *	e.g. 2:
     *		jndi = url=t3://localhost:80,password=collin
     *
     *	get("jndi.url") returns "t3://localhost:80"
     *
     *	e.g. 3:
     *		jndi = url=t3://localhost:80,service=username=collin;password=collinabc
     *
     *	get("jndi.service.password") returns "collinabc"
     * @param key The name of key.
     * @return String The value of key.
     */
    
    public static String getValue(String key) {
        
        String value = getInstance().props.getProperty(key);
        
        if (value == null){
            return "";
            //return getSubKey(key,null);
        } else {
            return value.trim();
        }
    }
    
    
    /**
     * Helper method to recursively find a subkey from the property file
     * @param parentKey the parentkey to search for in the property file
     * @param subKey the subkey embedded in the value of the parentkey's value. Set to null for the first call
     * @return the value of the subkey is found
     */
    protected static String getSubKey(String parentKey, String subKey) {
        log("Getting: parentKey = " + parentKey + " , subkey = " + subKey);
        
        // test if this is a composite key, if not, give up!
        int dotPos = parentKey.lastIndexOf(".");
        if (dotPos == -1){
            log("Cannot find key : " + parentKey);
            return null;
        }
        
        // is a composite key! Try some more!
        
        String newParentKey = parentKey.substring(0,dotPos);
        String newSubKey;
        if (subKey==null)
            newSubKey = parentKey.substring(dotPos + 1);
        else
            newSubKey = parentKey.substring(dotPos + 1) + "." + subKey;
        String parentValue = instance.props.getProperty(newParentKey);
        if (parentValue == null){
            log("Cannot find parent Key : " + newParentKey + "!");
            return getSubKey(newParentKey,newSubKey); // try next level recursively!
        }
        
        log("Found property for parent Key : " + newParentKey);
        
        return findSubKey(parentValue,newSubKey,",");
    }
    
    /**
     * Helper method to find a subkey value. A subkey is considered as something which prepends an
     * "=" sign and the return value is the string after the equal sign and before a "," sign, if any
     * @param valueStr a complex value string which may consists of a few subkeys and their value
     * @param subKey the subkey to look for
     * @return String the value of the subkey is returned
     */
    private  static String findSubKey(String valueStr, String subKey, String seperator) {
        
        int lastCommaPos = 0;
        boolean done = false;
        int equalPos;
        String posiKey;
        
        log("Searching for \nsubkey : " + subKey + "\nstring : " + valueStr);
        
        
        // determine if its a composite subkey
        int dotPos = subKey.indexOf(".");
        
        if (dotPos != -1) {
            // a composite key, need to find value for parent subkey first
            String parentSubKey = subKey.substring(0, dotPos);
            String parentSubKeyStr = findSubKey(valueStr,parentSubKey,",");
            if (parentSubKeyStr == null) // cannot find parentKey's value, abort!
                return null;
            String newSubKey = subKey.substring(dotPos + 1);
            return findSubKey(parentSubKeyStr,newSubKey,";");
        }
        
        // this part is executed if its not a composite subkey
        while(!done) {
            equalPos = valueStr.indexOf("=",lastCommaPos);
            if (equalPos == -1)
                return null;
            posiKey = valueStr.substring(lastCommaPos,equalPos);
            log("Possible Key = " + posiKey);
            
            // find the next commaPos and set lastCommaPos 1 after it
            lastCommaPos = valueStr.indexOf(seperator,equalPos);
            
            if (!posiKey.equals(subKey)) { // not this key!
                if (lastCommaPos == -1) // no more commas!
                    return null;
                lastCommaPos ++;
            } else {
                if (lastCommaPos == -1)
                    lastCommaPos = valueStr.length();
                // key found!
                String retStr = valueStr.substring(equalPos + 1, lastCommaPos);
                log("Found : " + retStr);
                return retStr;
            }
        }
        return null;
    }
    
    /**
     * throw away method till log is ready
     */
    private static void log(String str) {
    }
    
    /**
     * This method returns the String value for key.
     * @param key The name of key.
     * @param defaultValue The default value to be returned in case that key is not found.
     * @return String The value of key.
     */
    
    public static String getValue(String key, String defaultValue) {
        return  getInstance().props.getProperty(key,defaultValue);
    }
    
    /**
     * This method returns the int value for key.
     * @param key The name of key.
     * @return int The value of key.
     */
    public static int getInt(String key) {
        return Integer.parseInt(getValue(key));
    }
    
    /**
     * This method returns the int value for key.
     * @param key The name of key.
     * @param defaultValue The default value to be returned in case that key is not found.
     * @return String The value of key.
     */
    public static int getInt(String key, int defaultValue) {
        return Integer.parseInt(getValue(key, String.valueOf(defaultValue)));
    }
    
    
    /**
     * This method returns the int value for key.
     * @param key The name of key.
     * @return int The value of key.
     */
    public static long getLong(String key) {
        System.out.println("Ing get Long:..."+getValue(key));
        return Long.parseLong(getValue(key));
    }
    
    /**
     * This method returns the int value for key.
     * @param key The name of key.
     * @param defaultValue The default value to be returned in case that key is not found.
     * @return String The value of key.
     */
    public static long getLong(String key, long defaultValue) {
        return Long.parseLong(getValue(key, String.valueOf(defaultValue)));
    }
    
    
    /**
     * /**
     * This method returns the int value for key.
     * @param key The name of key.
     * @return int The value of key.
     */
    public static double getDouble(String key) {
        return Double.parseDouble(getValue(key));
    }
    
    /**
     * This method returns the int value for key.
     * @param key The name of key.
     * @param defaultValue The default value to be returned in case that key is not found.
     * @return String The value of key.
     */
    public static double getDouble(String key, double defaultValue) {
        return Double.parseDouble(getValue(key, String.valueOf(defaultValue)));
    }
    
    
    /**
     * This method returns the int value for key.
     * @param key The name of key.
     * @return int The value of key.
     */
    public static boolean getBoolean(String key) {
        return Boolean.valueOf(getValue(key)).booleanValue();
    }
    
    /**
     * This method returns the int value for key.
     * @param key The name of key.
     * @param defaultValue The default value to be returned in case that key is not found.
     * @return String The value of key.
     */
    public static boolean getBoolean(String key, boolean defaultValue) {
        return Boolean.valueOf(getValue(key, String.valueOf(defaultValue))).booleanValue();
    }
    
    /**
     * This method prints the list of properties into out.
     * @param out PrintStream for output
     */
    public static void list(PrintStream out) {
        instance.props.list(out);
    }
    
    /**
     * This method prints the list of properties into out.
     * @param out PrintWriter for output
     */
    public static void list(PrintWriter out) {
        instance.props.list(out);
    }
    
    /**
     * This method returns all keys in the property file
     */
    public static Enumeration getAllNameList() {
        return instance.props.propertyNames();
    }
    
    /**
     * This method returns all keys start with startStr in the property file
     */
    public static Enumeration getNameList(String startStr) {
        Vector v = new Vector();
        Enumeration e = getAllNameList();
        if (e!=null) {
            while (e.hasMoreElements()) {
                String name = (String)e.nextElement();
                if (name.startsWith(startStr)) {
                    v.add(name);
                }
            }
        }
        return v.elements();
    }
    
}
