/*
 * ConfigurationHelper.java
 *
 * Created on November 22, 2007, 9:54 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper;

import java.util.Properties;

/**
 *
 * @author meauchyuan.lee
 */
public class ConfigurationHelper {
    
    private Properties properties = null;
    private static ConfigurationHelper instance = null;
    
    /** Creates a new instance of ConfigurationHelper */
    public ConfigurationHelper() {
        //this.getClass().getResourceAsStream("config.properties");
        properties = PropertyLoader.loadProperties("startup");
        
    }
    
    
    public static synchronized ConfigurationHelper getInstance(){
        if ( instance == null ){
            instance = new ConfigurationHelper(); 
        }
        return instance;
    }
    
    public String getPropertyValue(String key)
    {   
        String value = null;
        if (properties != null)
        {
            value = properties.getProperty(key);
        }
        return value;
    }  
    
    public String getSystemValue(String key)
    {   
        String value = PropertyManager.getValue(key);
        return value;
    }
    
    public static String getDomainName()
    {   
        return getInstance().getSystemValue(PropertyConstants.SYSTEM_DOMAIN_NAME);  
    }
}
