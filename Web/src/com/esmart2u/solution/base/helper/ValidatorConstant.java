/*
 * ValidatorConstant.java
 *
 * Created on 24 September 2007, 00:45
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper;

public interface ValidatorConstant
{

    public static final String ERROR_NONE = "noerror";
    public static final String ERROR_LESS_THAN = "lessthan";
    public static final String ERROR_GREATER_THAN = "greaterthan";
    public static final String ERROR_MANDATORY = "mandatory";
    public static final String ERROR_FORMAT = "format";
    public static final String ERROR_DECIMAL_EXCEEDED = "decimalexceeded";
    public static final String ERROR_DECIMAL_NOT_ALLOWED = "decimalnotallowed";
    public static final String ERROR_NON_SWIFT = "nonswift";
    public static final String ERROR_HEIGHT_GREATER_THAN = "heightgreaterthan";
    public static final String ERROR_HEIGHT_LESS_THAN = "heightlessthan";
}

