package com.esmart2u.solution.base.helper;
 

import com.esmart2u.solution.base.logging.Logger;
import java.io.PrintStream;
import java.text.*;
import java.util.*;
import javax.swing.text.NumberFormatter;
import org.apache.oro.text.regex.*;

final class RegExValidate
{

    private static Logger logger = Logger.getLogger(RegExValidate.class);
    
    RegExValidate()
    {
        locale = new Locale("en", "SG");
    }

    static boolean validateInteger(String iInteger, boolean chk, int min, int max)
    {
        boolean methodResult;
        try
        {
            if(iInteger != null)
                iInteger = iInteger.trim();
            if(chk)
            {
                if(iInteger.equals(null) || iInteger.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", "chk = true: input is either null or empty ");
                    return methodResult = false;
                }
                if(!chkInteger(iInteger, min, max))
                    return methodResult = false;
            } else
            {
                if(iInteger.equals(null) || iInteger.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " chk = flase: input is either null or empty ");
                    return methodResult = true;
                }
                if(!chkInteger(iInteger, min, max))
                    return methodResult = false;
            }
        }
        catch(Exception e)
        {
            logger.debug(e);
            return methodResult = false;
        }
        return methodResult = true;
    }

    private static boolean chkInteger(String iInteger, int min, int max)
    {
        String sInteger = "";
        boolean methodResult;
        try
        {
            input = new PatternMatcherInput(iInteger);
            pattern = compiler.compile("[^0-9,-]");
            if(matcher.contains(input, pattern))
            {
                //DefaultLogger.debug("RegExValidate", input + ": input conatins the pattern otherthan 0-9,',','-'");
                return methodResult = false;
            }
            for(st = new StringTokenizer(iInteger, ",", false); st.hasMoreTokens();)
                sInteger = sInteger + st.nextToken();

            int iIntVal = (new Integer(sInteger.trim())).intValue();
            if(iIntVal < min || iIntVal > max)
            {
                //DefaultLogger.debug("RegExValidate", "the input value is out of range");
                return methodResult = false;
            }
        }
        catch(MalformedPatternException e)
        {
            //DefaultLogger.debug("RegExValidate", "Bad pattern");
            return methodResult = false;
        }
        catch(NumberFormatException e)
        {
            //DefaultLogger.debug("RegExValidate", "incorrect format chk = true");
            return methodResult = false;
        }
        catch(Exception e)
        {
            logger.debug(e);
            return methodResult = false;
        }
        return methodResult = true;
    }

    static boolean validateNumber(String iNumber, boolean chk, double min, double max)
    {
        boolean methodResult;
        try
        {
            if(iNumber != null)
                iNumber = iNumber.trim();
            if(chk)
            {
                if(iNumber.equals(null) || iNumber.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " Number is either null or empty ");
                    return methodResult = false;
                }
                if(!chkNumber(iNumber, min, max))
                    return methodResult = false;
            } else
            {
                if(iNumber.equals(null) || iNumber.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " Number is either null or empty ");
                    return methodResult = true;
                }
                if(!chkNumber(iNumber, min, max))
                    return methodResult = false;
            }
        }
        catch(Exception e)
        {
            logger.debug(e);
            return methodResult = false;
        }
        return methodResult = true;
    }

    private static boolean chkNumber(String iNumber, double min, double max)
    {
        String sNumber = "";
        boolean methodResult;
        try
        {
            input = new PatternMatcherInput(iNumber);
            pattern = compiler.compile("[^0-9,.]");
            if(matcher.contains(input, pattern))
            {
                //DefaultLogger.debug("RegExValidate", input + ": input conatins pattern other than 0-9,',','-'");
                return methodResult = false;
            }
            int iIndex = iNumber.indexOf('.');
            if(iIndex != -1)
            {
                if(iIndex != iNumber.lastIndexOf('.'))
                    return methodResult = false;
                if(iNumber.indexOf(',', iIndex) != -1)
                    return methodResult = false;
            }
            if(iNumber.indexOf(',') == 0)
                return methodResult = false;
            for(st = new StringTokenizer(iNumber, ",", false); st.hasMoreTokens();)
                sNumber = sNumber + st.nextToken();

            int sIndex = sNumber.indexOf('.');
            if(sIndex != -1)
            {
                if(sIndex < 1 || sIndex > 12)
                    return methodResult = false;
                int i = sNumber.substring(sIndex, sNumber.length()).length();
                if(i > 4)
                    return methodResult = false;
            } else
            if(sNumber.length() > 12)
                return methodResult = false;
            double iNumVal = (new Double(sNumber.trim())).doubleValue();
            if(iNumVal < min || iNumVal > max)
            {
                //DefaultLogger.debug("RegExValidate", "the input value is out of range c ");
                return methodResult = false;
            }
        }
        catch(NumberFormatException e)
        {
            //DefaultLogger.debug("RegExValidate", " incorrect format ");
            return methodResult = false;
        }
        catch(Exception e)
        {
            logger.debug(e);
            return methodResult = false;
        }
        return methodResult = true;
    }

    static boolean validateAmount(String iAmount, boolean chk, double min, double max, String currencyCode, Locale locale)
    {
        boolean methodResult = false;
        try
        {
            if(iAmount != null)
                iAmount = iAmount.trim();
            if(chk)
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " input is either null or empty");
                    methodResult = false;
                } else
                {
                    methodResult = chkAmount(iAmount, min, max, currencyCode, locale);
                }
            } else
            if(iAmount.equals(null) || iAmount.equals(""))
            {
                //DefaultLogger.debug("RegExValidate", " input is either null or empty");
                methodResult = true;
            } else
            {
                methodResult = chkAmount(iAmount, min, max, currencyCode, locale);
            }
        }
        catch(Exception e)
        {
            logger.debug(e);
            return false;
        }
        return methodResult;
    }

    private static boolean chkAmount(String amt, double min, double max, String currencyCode, Locale locale)
        throws IllegalArgumentException
    {
        if(min < 0.0D)
            throw new IllegalArgumentException("Parameter [min] must be > 0!");
        if(max < 0.0D)
            throw new IllegalArgumentException("Parameter [max] must be > 0!");
        if(min > max)
            throw new IllegalArgumentException("Parameter [min] must be <= [max]!");
        if(null == currencyCode)
            throw new IllegalArgumentException("CurrencyCode must not be null!");
        if(null == locale)
            throw new IllegalArgumentException("Locale must not be null!");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
        char groupSeparator = symbols.getGroupingSeparator();
        char decSeparator = symbols.getDecimalSeparator();
        /*CurrencyFormatter formatter = null;
        try
        {
            formatter = CurrencyManager.getCurrencyFormatterFromString(currencyCode);
        }
        catch(ChainedException ce)
        {
            ce.printStackTrace();
            return false;
        }*/
        //String displayFormat = formatter.getInputFormatString(locale);
        String displayFormat = "########.##";
        String displayGroupSeparator = ",";
        String displayDecimalSeparator = ".";
        if(-1 == displayFormat.indexOf(displayDecimalSeparator))
        {
            if(amt.indexOf(decSeparator) != -1)
                return false;
        } else
        if(amt.indexOf(decSeparator) != -1)
        {
            int decimalPos = displayFormat.indexOf(displayDecimalSeparator);
            int decimalCount = displayFormat.length() - decimalPos - 1;
            if(amt.length() - amt.indexOf(decSeparator) - 1 > decimalCount)
                return false;
        }
        if(-1 == displayFormat.indexOf(displayGroupSeparator) && amt.indexOf(groupSeparator) != -1)
            return false;
        for(int i = 0; i < amt.length(); i++)
            if(!Character.isDigit(amt.charAt(i)) && amt.charAt(i) != groupSeparator && amt.charAt(i) != decSeparator)
                return false;

        if(amt.indexOf(decSeparator) != -1)
        {
            for(int i = amt.indexOf(decSeparator) + 1; i < amt.length(); i++)
                if(!Character.isDigit(amt.charAt(i)))
                    return false;

        }
        String checkGroup = String.valueOf(groupSeparator) + String.valueOf(groupSeparator);
        String checkGroupDec = String.valueOf(groupSeparator) + String.valueOf(decSeparator);
        if(amt.indexOf(checkGroup) != -1)
            return false;
        if(amt.indexOf(checkGroupDec) != -1)
            return false;
        if(!Character.isDigit(amt.charAt(0)) || !Character.isDigit(amt.charAt(amt.length() - 1)))
            return false;
        double amtDbl = 0.0D;
        //NumberFormat nf = formatter.getDisplayFormat(locale); 
        NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH); 
        try
        {
            amtDbl = nf.parse(amt).doubleValue();
        }
        catch(ParseException e)
        {
            e.printStackTrace();
            return false;
        }
        return amtDbl >= min && amtDbl <= max;
    }

    static boolean validateAmount(String iAmount, boolean chk, double min, double max, Locale locale)
    {
        boolean methodResult;
        try
        {
            if(iAmount != null)
                iAmount = iAmount.trim();
            if(chk)
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " input is either null or empty");
                    methodResult = false;
                }
                if(!chkAmount(iAmount, min, max, locale))
                    methodResult = false;
            } else
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " input is either null or empty");
                    methodResult = true;
                }
                if(!chkAmount(iAmount, min, max, locale))
                    methodResult = false;
            }
            methodResult = true;
        }
        catch(Exception e)
        {
            logger.debug(e);
            return false;
        }
        return methodResult;
    }

    private static boolean chkAmount(String iAmount, double min, double max, Locale locale)
    {
        String sAmount = "";
        String pAmount = "";
        boolean methodResult;
        try
        {
            input = new PatternMatcherInput(iAmount);
            pattern = compiler.compile("[^0-9,.]");
            if(matcher.contains(input, pattern))
            {
                //DefaultLogger.debug("RegExValidate", input + ": input contains ILLEGAL pattern");
                return methodResult = false;
            }
            int iIndex = iAmount.indexOf('.');
            if(iIndex != -1)
            {
                if(iIndex != iAmount.lastIndexOf('.'))
                    return methodResult = false;
                if(iAmount.indexOf(',', iIndex) != -1)
                    return methodResult = false;
            }
            if(iAmount.indexOf(',') == 0)
                return methodResult = false;
            for(StringTokenizer st = new StringTokenizer(iAmount, ",", false); st.hasMoreTokens();)
                sAmount = sAmount + st.nextToken();

            int sIndex = sAmount.indexOf('.');
            if(sIndex != -1)
            {
                if(sIndex < 1 || sIndex > 12)
                    return methodResult = false;
                int i = sAmount.substring(sIndex, sAmount.length()).length();
                if(i > 4)
                    return methodResult = false;
            }
            double iAmountVal = (new Double(sAmount)).doubleValue();
            if(iAmountVal < min || iAmountVal > max)
            {
                //DefaultLogger.debug("RegExValidate", "the input value is out of range  ");
                return methodResult = false;
            }
            pAmount = NumberFormat.getNumberInstance(locale).format(iAmountVal);
            pattern = compiler.compile(pAmount);
        }
        catch(MalformedPatternException e)
        {
            return methodResult = false;
        }
        catch(NumberFormatException e)
        {
            System.err.println("Bad pattern.");
            return methodResult = false;
        }
        return methodResult = true;
    }

    static boolean validateAmount(String iAmount, boolean chk, String currencyCode)
    {
        return true;
    }

    static boolean validateAmount(String iAmount, boolean chk, Locale locale)
    {
        boolean methodResult;
        try
        {
            if(iAmount != null)
                iAmount = iAmount.trim();
            if(chk)
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " input is either null or empty");
                    return methodResult = false;
                }
                if(!chkAmount(iAmount, locale))
                    return methodResult = false;
            } else
            {
                if(iAmount.equals(null) || iAmount.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " input is either null or empty");
                    return methodResult = true;
                }
                if(!chkAmount(iAmount, locale))
                    return methodResult = false;
            }
        }
        catch(Exception e)
        {
            logger.debug(e);
            return false;
        }
        return methodResult = true;
    }

    private static boolean chkAmount(String iAmount, Locale locale)
    {
        String sAmount = "";
        String pAmount = "";
        boolean methodResult;
        try
        {
            input = new PatternMatcherInput(iAmount);
            pattern = compiler.compile("[^0-9,.]");
            if(matcher.contains(input, pattern))
            {
                //DefaultLogger.debug("RegExValidate", input + ": input contains ILLEGAL pattern");
                return methodResult = false;
            }
            int iIndex = iAmount.indexOf('.');
            if(iIndex != -1)
            {
                if(iIndex != iAmount.lastIndexOf('.'))
                    return methodResult = false;
                if(iAmount.indexOf(',', iIndex) != -1)
                    return methodResult = false;
            }
            if(iAmount.indexOf(',') == 0)
                return methodResult = false;
            for(StringTokenizer st = new StringTokenizer(iAmount, ",", false); st.hasMoreTokens();)
                sAmount = sAmount + st.nextToken();

            int sIndex = sAmount.indexOf('.');
            if(sIndex != -1)
            {
                if(sIndex < 1 || sIndex > 12)
                    return methodResult = false;
                int i = sAmount.substring(sIndex, sAmount.length()).length();
                if(i > 4)
                    return methodResult = false;
            }
            double iAmountVal = (new Double(sAmount)).doubleValue();
            pAmount = NumberFormat.getNumberInstance(locale).format(iAmountVal);
            pattern = compiler.compile(pAmount);
        }
        catch(Exception e)
        {
            logger.debug(e);
        }
        return methodResult = true;
    }

    static boolean validateEmail(String iEmail, boolean chk)
    {
        boolean methodResult;
        try
        {
            if(iEmail != null)
                iEmail = iEmail.trim();
            if(chk)
            {
                if(iEmail.equals(null) || iEmail.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " mail is either null or empty");
                    return methodResult = false;
                }
                if(chkEmail(iEmail))
                    return methodResult = false;
            } else
            {
                if(iEmail.equals(null) || iEmail.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " mail is either null or empty");
                    return methodResult = true;
                }
                if(chkEmail(iEmail))
                    return methodResult = false;
            }
        }
        catch(Exception e)
        {
            logger.debug(e);
            return methodResult = false;
        }
        return methodResult = true;
    }

    private static boolean chkEmail(String iEmail)
    {
        boolean methodResult;
        try
        {
            input = new PatternMatcherInput(iEmail);
            pattern = compiler.compile("@.*\\.");
            //DefaultLogger.debug("RegExValidate", pattern.getPattern());
            if(!matcher.contains(input, pattern))
                return methodResult = true;
            else
                return methodResult = false;
        }
        catch(MalformedPatternException e)
        {
            return methodResult = false;
        }
    }

    static boolean validatePhoneNumber(String iPhNumber, boolean chk, Locale locale)
    {
        boolean methodResult;
        try
        {
            if(iPhNumber != null)
                iPhNumber = iPhNumber.trim();
            if(chk)
            {
                if(iPhNumber.equals(null) || iPhNumber.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", " Phone Number is either null or empty");
                    methodResult = false;
                } else
                {
                    methodResult = chkPhoneNumber(iPhNumber, locale);
                }
            } else
            if(iPhNumber.equals(null) || iPhNumber.equals(""))
            {
                //DefaultLogger.debug("RegExValidate", " Phone Number is neither null nor empty");
                methodResult = true;
            } else
            {
                methodResult = chkPhoneNumber(iPhNumber, locale);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            methodResult = false;
        }
        return methodResult;
    }

    private static boolean chkPhoneNumber(String iPhNumber, Locale locale)
    {
        byte numByte[] = iPhNumber.getBytes();
        int i = numByte.length - 1;
        boolean flag = true;
        for(; i >= 0; i--)
        {
            int decRep = numByte[i];
            if(decRep < 48 || decRep > 57)
            {
                //DefaultLogger.debug("RegExValidate", "Error");
                flag = false;
            }
        }

        //String country = locale.getCountry();
        //DefaultLogger.debug("RegExValidate", "A" + iPhNumber.length());
        //if(country == "SG" && iPhNumber.length() > 20)
        //    flag = false;
        return flag;
    }

    static boolean validateString(String iString, boolean chk, int min, int max)
    {
        try
        {
            String trimString = "";
            if(iString != null)
                trimString = iString.trim();
            if(chk)
                if(iString == null || trimString.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", "String is null or empty");
                    return false;
                } else
                {
                    return chkString(iString, min, max);
                }
            if(iString == null || trimString.equals(""))
                return true;
            else
                return chkString(iString, 0, max);
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    private static boolean chkString(String iString, int min, int max)
    {
        try
        {
            int len = iString.length();
            return len >= min && len <= max;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    static boolean validateDate(String iDate, boolean chk, Locale locale)
    {
        try
        {
            if(chk)
                if(iDate == null || iDate.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", "Date is null");
                    return false;
                } else
                {
                    return chkDate(iDate, locale);
                }
            if(iDate == null || iDate.equals(""))
            {
                //DefaultLogger.debug("RegExValidate", "Date is null");
                return true;
            } else
            {
                return chkDate(iDate, locale);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    private static boolean chkDate(String iDate, Locale locale)
    {
        int day = 0;
        int mth = 0;
        int yr = 0;
        try
        {
            StringTokenizer st = new StringTokenizer(iDate, "-");
            if(st.countTokens() == 1)
            {
                //DefaultLogger.debug("RegExValidate", "Chk Space");
                st = new StringTokenizer(iDate, " ");
                if(st.countTokens() == 3)
                {
                    day = Integer.parseInt(st.nextToken());
                    mth = convertMonth(st.nextToken());
                    yr = Integer.parseInt(st.nextToken());
                } else
                {
                    return false;
                }
            } else
            if(st.countTokens() == 3)
            {
                day = Integer.parseInt(st.nextToken());
                mth = Integer.parseInt(st.nextToken()) - 1;
                yr = Integer.parseInt(st.nextToken());
            } else
            {
                return false;
            }
            //DefaultLogger.debug("RegExValidate", "Day\t:" + day);
            //DefaultLogger.debug("RegExValidate", "Mth\t:" + (mth + 1));
            //DefaultLogger.debug("RegExValidate", "Yr\t:" + yr);
            if(mth > -1 && mth < 13)
            {
                GregorianCalendar cal = new GregorianCalendar(yr, mth, 1);
                cal.setLenient(false);
                return day > 0 && day <= cal.getActualMaximum(5);
            } else
            {
                return false;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    private static boolean chkDate(String iDate)
    {
        int day = 0;
        int mth = 0;
        int yr = 0;
        try
        {
            StringTokenizer st = new StringTokenizer(iDate, "-");
            if(st.countTokens() == 1)
            {
                //DefaultLogger.debug("RegExValidate", "Chk Space");
                st = new StringTokenizer(iDate, " ");
                if(st.countTokens() == 3)
                {
                    day = Integer.parseInt(st.nextToken());
                    mth = convertMonth(st.nextToken());
                    yr = Integer.parseInt(st.nextToken());
                } else
                {
                    return false;
                }
            } else
            if(st.countTokens() == 3)
            {
                day = Integer.parseInt(st.nextToken());
                mth = Integer.parseInt(st.nextToken()) - 1;
                yr = Integer.parseInt(st.nextToken());
            } else
            {
                return false;
            }
            //DefaultLogger.debug("RegExValidate", "Day\t:" + day);
            //DefaultLogger.debug("RegExValidate", "Mth\t:" + (mth + 1));
            //DefaultLogger.debug("RegExValidate", "Yr\t:" + yr);
            if(mth > -1 && mth < 13)
            {
                GregorianCalendar cal = new GregorianCalendar(yr, mth, 1);
                cal.setLenient(false);
                return day > 0 && day <= cal.getActualMaximum(5);
            } else
            {
                return false;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    private static Date convertDate(String input)
        throws Exception
    {
        int day = 0;
        int mth = 0;
        int yr = 0;
        String dateFormat = "dd MM yyyy";
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        StringTokenizer st = new StringTokenizer(input, "-");
        if(st.countTokens() == 1)
        {
            //DefaultLogger.debug("RegExValidate", "Chk Space");
            st = new StringTokenizer(input, " ");
            if(st.countTokens() == 3)
            {
                day = Integer.parseInt(st.nextToken());
                mth = convertMonth(st.nextToken());
                yr = Integer.parseInt(st.nextToken());
            } else
            {
                throw new Exception();
            }
        } else
        if(st.countTokens() == 3)
        {
            day = Integer.parseInt(st.nextToken());
            mth = Integer.parseInt(st.nextToken()) - 1;
            yr = Integer.parseInt(st.nextToken());
        } else
        {
            throw new Exception();
        }
        //DefaultLogger.debug("RegExValidate", "Day\t:" + day);
        //DefaultLogger.debug("RegExValidate", "Mth\t:" + (mth + 1));
        //DefaultLogger.debug("RegExValidate", "Yr\t:" + yr);
        if(mth > -1 && mth < 13)
        {
            GregorianCalendar cal = new GregorianCalendar(yr, mth, 1);
            cal.setLenient(false);
            if(day > 0 && day <= cal.getActualMaximum(5))
            {
                String dayString = "" + day;
                String mthString = "" + (mth + 1);
                if(day < 10)
                    dayString = "0" + day;
                if(mth < 10)
                    mthString = "0" + (mth + 1);
                return format.parse(dayString + " " + mthString + " " + yr);
            } else
            {
                //DefaultLogger.debug("RegExValidate", "Error in the Month");
                throw new Exception();
            }
        } else
        {
            //DefaultLogger.debug("RegExValidate", "Error in the Month");
            throw new Exception();
        }
    }

    static int compareDate(String input, boolean chk, String comparedDate)
        throws Exception
    {
        try
        {
            if(chk)
                if(input == null || input.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", "Date is null or empty");
                    throw new Exception("Date is null or empty");
                } else
                {
                    return comparingDate(input, comparedDate);
                }
            if(input == null || input.equals(""))
            {
                //DefaultLogger.debug("RegExValidate", "Date is null or empty");
                return 0;
            } else
            {
                return comparingDate(input, comparedDate);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    private static int comparingDate(String input, String comparedDate)
        throws Exception
    {
        Date inputDate = convertDate(input);
        Date compDate = convertDate(comparedDate);
        //DefaultLogger.debug("RegExValidate", "Date 1: " + inputDate);
        //DefaultLogger.debug("RegExValidate", "Date 2: " + compDate);
        return inputDate.compareTo(compDate);
    }

    static int compareDate(String input, boolean chk, Date comparedDate)
        throws Exception
    {
        try
        {
            if(chk)
                if(input == null || input.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", "Date is null or empty");
                    throw new Exception("Date is null or empty");
                } else
                {
                    return comparingDate(input, comparedDate);
                }
            if(input == null || input.equals(""))
            {
                //DefaultLogger.debug("RegExValidate", "Date is null or empty");
                return 0;
            } else
            {
                return comparingDate(input, comparedDate);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            throw ex;
        }
    }

    private static int comparingDate(String input, Date compDate)
        throws Exception
    {
        Date inputDate = convertDate(input);
        return inputDate.compareTo(compDate);
    }

    static boolean validateTextBox(String input, boolean chk, int minLength, int maxLength, int minHeight, int maxHeight)
    {
        boolean result = false;
        try
        {
            String trimInput = "";
            if(input != null)
                trimInput = input.trim();
            if(chk)
            {
                if(input == null || trimInput.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", "TextBox is null or empty");
                    result = false;
                } else
                {
                    return chkTextBox(input, minLength, maxLength, minHeight, maxHeight);
                }
            } else
            if(input == null || trimInput.equals(""))
            {
                //DefaultLogger.debug("RegExValidate", "TextBox is null or empty");
                result = true;
            } else
            {
                return chkTextBox(input, minLength, maxLength, minHeight, maxHeight);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return result;
    }

    private static boolean chkTextBox(String input, int minLength, int maxLength, int minHeight, int maxHeight)
    {
        boolean result = false;
        int numCol = 0;
        try
        {
            //DefaultLogger.debug("RegExValidate", "Validate TextBox Data");
            if(!chkString(input, minLength, maxLength))
            {
                //DefaultLogger.debug("RegExValidate", "Error in the Lenght of the Text Box");
                throw new Exception();
            }
            for(StringTokenizer tokenString = new StringTokenizer(input, "\r"); tokenString.hasMoreTokens();)
            {
                String textBoxString = tokenString.nextToken();
                numCol++;
            }

            //DefaultLogger.debug("RegExValidate", "Number of Lines :" + numCol);
            if(numCol >= minHeight && numCol <= maxHeight)
            {
                result = true;
            } else
            {
                //DefaultLogger.debug("RegExValidate", "Error in the Height of the Text Box");
                result = false;
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            result = false;
        }
        return result;
    }

    public static boolean validateSwiftCode(String input, boolean chk)
    {
        try
        {
            if(chk)
                if(input == null || input.equals(""))
                {
                    //DefaultLogger.debug("RegExValidate", "Field is null or empty");
                    throw new Exception("Field is null or empty");
                } else
                {
                    return chkSwiftCode(input);
                }
            if(input == null || input.equals(""))
            {
                //DefaultLogger.debug("RegExValidate", "Date is null or empty");
                return true;
            } else
            {
                return chkSwiftCode(input);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return false;
    }

    private static boolean chkSwiftCode(String inputString)
    {
        boolean status = true;
        String input = inputString.trim();
        byte binary[] = input.getBytes();
        for(int i = 0; i < binary.length; i++)
        {
            Byte ascii = new Byte(binary[i]);
            int v = ascii.intValue();
            if(v >= 39 && v <= 41 || v >= 43 && v <= 58 || v == 63 || v >= 65 && v <= 90 || v >= 97 && v <= 122 || v == 32 || v == 10 || v == 13)
            {
                status = true;
                continue;
            }
            status = false;
            break;
        }

        return status;
    }

    private static int convertMonth(String mth)
    {
        if(mth.equalsIgnoreCase("jan"))
            return 0;
        if(mth.equalsIgnoreCase("feb"))
            return 1;
        if(mth.equalsIgnoreCase("mar"))
            return 2;
        if(mth.equalsIgnoreCase("apr"))
            return 3;
        if(mth.equalsIgnoreCase("may"))
            return 4;
        if(mth.equalsIgnoreCase("jun"))
            return 5;
        if(mth.equalsIgnoreCase("jul"))
            return 6;
        if(mth.equalsIgnoreCase("AUG"))
            return 7;
        if(mth.equalsIgnoreCase("SEP"))
            return 8;
        if(mth.equalsIgnoreCase("oct"))
            return 9;
        if(mth.equalsIgnoreCase("nov"))
            return 10;
        return !mth.equalsIgnoreCase("dec") ? 0 : 11;
    }

    public static boolean checkDoubleDigits(String dValue, int integerDigits, int decimalDigits, boolean negativeAllowed)
    {
        input = new PatternMatcherInput(dValue);
        String regExp = "[0-9]{0," + integerDigits + "}(\\.[0-9]{1," + decimalDigits + "})?$";
        if(negativeAllowed)
            regExp = "(-)?" + regExp;
        regExp = "^" + regExp;
        try
        {
            pattern = compiler.compile(regExp);
            return matcher.contains(input, pattern);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }

    private static final String xEmail = "@.*\\.";
    private static final String xNumber = "[^0-9,.]";
    private static final String xInteger = "[^0-9,-]";
    private static final String xAmount = "[^0-9,.]";
    private static final String xPostCode = "";
    private static final String xString = "";
    private static final String xDate = "";
    private static final String xPhNumber = "[^0-9-]";
    private static final String xRegEx = "";
    private static Pattern pattern = null;
    private static PatternMatcherInput input;
    private static PatternCompiler compiler = new Perl5Compiler();
    private static PatternMatcher matcher = new Perl5Matcher();
    private static MatchResult result;
    private static StringTokenizer st;
    private static ResourceBundle resources;
    private String PhDigits[];
    Locale locale;

}
