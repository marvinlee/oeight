/**
 * � 2002 esmart2u Malaysia Sdn. Bhd.  All rights reserved.
 * MarvinLee.net
 *  
 *
 * This software is the intellectual property of esmart2u Malaysia Sdn. Bhd.
 * The   program may be used only in accordance with the terms of the license
 * agreement you entered into with esmart2u Malaysia Sdn. Bhd.
 */

package com.esmart2u.solution.base.helper;

/**
 * Simple bean to store key & value. Used especially in creating select boxes.
 *
 * @author Peter Govind
 * @version $Revision: 1.1 $ $Date: 2003/12/05 13:32:47 $
 * @deprecated No longer support. Will be removed on 29 Feb 2004.
 */
public class KeyValue extends ValueObject {

	private String key, value;

	// =============================================================
	// This MUST be here or else ....
	public KeyValue() {}

	// =============================================================

    public KeyValue(String key, String value) {
		this.key = key;
		this.value = value;
	}

	// =============================================================

	public void setKey(String value) {
	    this.key = value;
	}

	public String getKey() {
		return (this.key);
	}

	// =============================================================

	public void setValue(String value) {
	    this.value = value;
	}

	public String getValue() {
		return (this.value);
	}

	// =============================================================

	public String toString() {
		return (this.key + ":" + this.value);
	}

	// =============================================================

}
// $Log: KeyValue.java,v $
// Revision 1.1  2003/12/05 13:32:47  wkchee
// Initial check in.
//
// Revision 1.1  2003/12/05 12:49:44  wkchee
// Initial check in.
//
// Revision 1.1  2003/12/05 06:11:57  wkchee
// Initial check in.
//
// Revision 1.1  2003/11/16 08:04:45  benjaminfoo
// Initial check in.
//
// Revision 1.1  2002/05/20 04:14:16  petermg
// *** empty log message ***
//

