/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BaseResourceConstants.java

package com.esmart2u.solution.base.helper;

/**
 * Resource constants such as keys.
 *
 * @author  Chee Weng Keong
 * @version $Id: BaseResourceConstants.java,v 1.3 2004/06/15 09:34:59 wkchee Exp $
 */

public interface BaseResourceConstants
{
    public static final String INVALID_ERROR_CODE = "common.message.general_000000000";
    public static final String RECORD_NOT_FOUND = "common.message.general_000000001";
    public static final String ADD_SUCCESS = "common.message.general_000000002";
    public static final String UPDATE_SUCCESS = "common.message.general_000000003";
    public static final String DELETE_SUCCESS = "common.message.general_000000004";
    public static final String DUPLICATE_RECORD = "common.message.general_000000005";
    public static final String REPLACE_RECORD = "common.message.general_000000006";
    public static final String UPDATED_BY_ANOTHER_USER = "common.message.general_000000007";
    public static final String DELETED_BY_ANOTHER_USER = "common.message.general_000000008";
    public static final String DATA_VALIDATION_ERROR = "common.message.general_000000009";
}

// end of BaseResourceConstants.java