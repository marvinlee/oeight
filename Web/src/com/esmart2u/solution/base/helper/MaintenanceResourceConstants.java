 

// MaintenanceResourceConstants.java

package com.esmart2u.solution.base.helper;

/**
 * Resource constants such as keys.
 *
 * @author Chee Weng Keong
 * @version $Id: MaintenanceResourceConstants.java,v 1.3 2004/01/16 04:53:31 wkchee Exp $
 */

public interface MaintenanceResourceConstants
{
    public static final String ADD_SUCCESS_PENDING_APPROVAL = "common.message.recordCreatedPendingApproval";
    public static final String UPDATE_SUCCESS_PENDING_APPROVAL = "common.message.recordUpdatedPendingApproval";
    public static final String DELETE_SUCCESS_PENDING_APPROVAL = "common.message.recordDeletedPendingApproval";
}

// end of MaintenanceResourceConstants.java