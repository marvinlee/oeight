 

// ValueObject.java

package com.esmart2u.solution.base.helper;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.IOException;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.beanutils.PropertyUtils;
//import org.apache.xml.serialize.OutputFormat;
//import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;

import com.esmart2u.solution.base.logging.Logger;

/**
 * Base value object.
 *
 * @author  Victor Goh
 * $Revision: 1.5 $ updated on $Date: 2004/03/29 08:44:00 $ by $Author: wkchee $
 */

public class ValueObject implements Serializable
{
    private static final long serialVersionUID = -8811881188118811881L;

    private static Logger logger = Logger.getLogger(ValueObject.class);

    public static final int NO_ACTION = 29000;
    public static final int SELECT_ACTION = 29001;
    public static final int INSERT_ACTION = 29002;
    public static final int UPDATE_ACTION = 29003;
    public static final int DELETE_ACTION = 29004;
    // Added to handle batch delete and subsequently call batch insert to
    // add new or update existing records case.  AuditLog module will
    // determine the actual state and performed relative insert or update logging
    public static final int RAW_INSERT_ACTION = 29011;
    // PreValueObject(s) which have been 'batch deleted'
    public static final int RAW_DELETE_ACTION = 29012;
    // Disable audit logging
    public static final int DISABLE_LOGGING = 29013;
    public static final int ENABLE_LOGGING = 29014;


    private static final String DATA = "data";
    private static final String VALUE = "value";
    private static final String TYPE = "type";
    private static final String COLUMN = "column";
    private static final String NAME = "name";

    private String auditInstanceId = null;

    private int eaiAction = NO_ACTION;

    // @cslim Indicate the 'logical' action performed by analyzing/classifying RAW_INSERT_ACTION
    // The 'logical' action for RAW_INSERT_ACTION can be either insert or update
    private int auditLogAction = NO_ACTION;

    private String aliasName = null;
    private Map property;
    private String blobTableName = "";
    private String clobTableName = "";

    // The following is to be used in "SELECT TOP @recordLimit ..."
    // Why String and not int ? Because when we construct the SQL command
    // in the SP, we concatenate Strings/char. Thus if this property is
    // already a String, there's no need to convert them at the SP.
    // In other words, the query overhead is reduced.
    // private String recordLimit;

    // Errr .. screw the last one. Anyway, the original plan was to
    // include this property into the EAIContext. But since the SP
    // needs this parameter & the only way to pass this value is
    // through any ValueObject, an elegant solution is to put it here.
    private int recordLimit;

    // audit log to show approver List of ApprovalDetailUserVO
    private List approverList;
    private List approvalLevelList;

    public String getBlobTableName() {
        return blobTableName;
    }

    public void setBlobTableName(String blobTableName) {
        this.blobTableName = blobTableName;
    }

    public String getClobTableName() {
        return clobTableName;
    }

    public void setClobTableName(String clobTableName) {
        this.clobTableName = clobTableName;
    }

    public String getKeyFieldName() {
        return KeyFieldName;
    }

    public void setKeyFieldName(String keyFieldName) {
        KeyFieldName = keyFieldName;
    }

    private String KeyFieldName = "";

    // ---------------------------------------------------

    public ValueObject()
    {
    }

    public String getAliasName()
    {
        return aliasName;
    }

    public void setAliasName(String aliasName)
    {
        this.aliasName = aliasName;
    }


    public int getAuditLogAction()
    {
        return auditLogAction;
    }

    public void setAuditLogAction(int auditLogAction)
    {
        this.auditLogAction = auditLogAction;
    }

    public int getEAIAction()
    {
        return eaiAction;
    }

    public void setEAIAction(int eaiAction)
    {
        this.eaiAction = eaiAction;
    }

    public String getAuditInstanceId()
    {
        return auditInstanceId;
    }

    public void setAuditInstanceId(String auditInstanceId)
    {
        this.auditInstanceId = auditInstanceId;
    }

    public int getRecordLimit()
    {
        return recordLimit;
    }

    public void setRecordLimit(int recordLimit)
    {
        this.recordLimit = recordLimit;
    }

    public List getApprovalLevelList()
    {
        return approvalLevelList;
    }

    public void setApprovalLevelList(List approvalLevelList)
    {
        this.approvalLevelList = approvalLevelList;
    }

    public List getApproverList()
    {
        return approverList;
    }

    public void setApproverList(List approverList)
    {
        this.approverList = approverList;
    }

    public Map retrieveValues() throws ValueObjectException
    {
        HashMap values = null;
        try
        {
            Map map = PropertyUtils.describe(this);
            values = new HashMap(map);
        }
        catch (IllegalAccessException e)
        {
            logger.error(e.toString());
            throw new ValueObjectException(e.getMessage());
        }
        catch (InvocationTargetException e)
        {
            logger.error(e.toString());
            throw new ValueObjectException(e.getMessage());
        }
        catch (NoSuchMethodException e)
        {
            logger.error(e.toString());
            throw new ValueObjectException(e.getMessage());
        }
        return values;
    }

    // ---------------------------------------------------

    private Map retrieveProperties()
    {
        // Default properties=fieldname retrieval
        //Hashtable property = new Hashtable();
        property = new IndexedMap();
        PropertyDescriptor pd[] = PropertyUtils.getPropertyDescriptors(this);
        for (int i = 0; i < pd.length; i++)
        {
            property.put(pd[i].getName(), pd[i].getName());
        }
        return property;
    }

    // ---------------------------------------------------

    //==========================================================================
    // Changes: Method Deprecated
    // Author: Marvin Lee
    // Date: 25 Aug 2003
    /** Method to generate the XML String for the ValueObject based on the
     * passed in template code. This method is required for
     * AuditLog purpose
     * @param code The template code to retrieve the required template
     * @throws ValueObjectException If transforming the ValueObject to XML String fails
     * @return The XML String generated using the required template.
     * @deprecated Use toAuditXML(Map) instead (changed since AuditLog++)
    
    public String toAuditXML(String code) throws ValueObjectException
    {
        // Get the template to store all the values
        Map template = getAuditTemplate(code);

        // If template is empty, use default, which is all properties
        if (template == null || template.isEmpty())
            retrieveProperties();
        else
            property = template;
        // Set it as the default properties for generating xml

        // generate the xml and return it
        return generateXML();
    } */
    // End changes
    //==========================================================================



    //==========================================================================
    // Changes: New Method Introduced
    // Author: Marvin Lee
    // Date: 25 Aug 2003
    /**
     * Method that generates an XML string representing the audit log
     * properties, based on a given template.
     *
     * <p><b>Important:</b>Do not confuse the <code>auditTemplate</code>
     * Map interface parameter with the class instance <code>auditTemplate</code>.
     * They are totally different in respect to each other. The auditTemplate
     * refered to in the parameter list of this method is the 'formatted' Map
     * that defines fields that are to be logged. This is different from the class
     * instance, where it maps class properties with external attributes such as
     * resource code, mandotary flag etc (refer to class AuditLogTemplate)</p>
     *
     * Introduced by Marvin Lee for integration with the AuditLog++ 1.0 API
     * @param auditTemplate the audit template containing fields that are to be logged
     * @return the XML string representing the log data
     * @throws ValueObjectException
     * @since AuditLog++ 1.0
     
    public String toAuditXML(Map auditTemplate) throws ValueObjectException
    {
        //** Validation check on the audit template
        if (auditTemplate == null || auditTemplate.isEmpty())
        {
            retrieveProperties();
        }
        else
        {
            property = auditTemplate;
        }
        return generateXML();
    }*/
    // End Changes
    //==========================================================================



    //==========================================================================
    // Changes: Method Deprecated
    // Author: Marvin Lee
    // Date: 25 Aug 2003
    /** Method to retrieve the required audit template matching the code.
     * @param code The code for retrieving the required template
     * @return The template matching the code
     * If no matching found, return null.
     * @deprecated Use getAuditTemplate() instead (changed since AuditLog++)
     */
    public Map getAuditTemplate(String code)
    {
        //Return hashtable based on template
        // Key=property name, Value=description
        return null;
    }
    // End Changes
    //==========================================================================




    //==========================================================================
    // Changes: New Method Introduced
    // Author: Marvin Lee
    // Date: 25 Aug 2003
    /**
     * Method to retrieve a map containing audit logging field preferences.
     * Overrides previous method <code>getAuditTemplate(String code)</code>.
     * Since AuditLog++, there is no need to specify which fields should or
     * should not be logged, as these decision will be specified in an external
     * resource.
     *
     * All classes extending this class should override this method to provide
     * better clarity for audit logging.
     *
     * Introduced by Marvin Lee for integration with the AuditLog++ 1.0 API
     * @return a Map collection of audit logging templates
     * @since AuditLog++ 1.0
     */
    public Map getAuditTemplate()
    {
        //** ==========================
        //** This is temporary disabled
        //** ==========================
        /*
        if(property == null) {
            try {
                AuditTemplateManager atm = (AuditTemplateManager)
                        ServiceFactory.getInstance().getEJBService(
                                AuditTemplateManagerHome.JNDI_NAME);
                property = atm.extractAuditTemplate(this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        */
        return property;
    }
    // End Changes
    //==========================================================================



    /**
     * Method to get the XML output for the current ValueObject.
     *
     * @return The XML String representing the ValueObject.
     * @throws ValueObjectException If transforming the ValueObject to XML String fails.
     
    public String toXML() throws ValueObjectException
    {
        // retrieve all values associate with object
        retrieveProperties();
        return generateXML();
    }*/


    //==========================================================================
    // Changes: Method Deprecated
    // Author: Marvin Lee
    // Date: 25 Aug 2003
    /** Method to generate the templates for use with AuditLog purpose.
     * The passed in templateStr must follow the following pattern
     * pattern : "keys||description"
     * eg : "nm,occp||Customer Name, Occupation"
     * Where 'nm' and 'occp' is the field in ValueObject that you wish to log,
     * 'Customer Name' and 'Occupation' is the description you want to replace with.
     * So instead of logging the field as 'nm' and 'occp', it will log it
     * as 'Customer Name' and 'Occupation'.
     * @param templateStr The template String that you want to generate template
     * @return a Map for the generated template where :
     * key = field in ValueObject
     * value = the description for the field
     * @deprecated as of AuditLog++ 1.0. Use getAuditTemplate() : Map to define
     */
    protected Map generateAuditTemplate(String templateStr)
    {
        return generateHash(templateStr);
    }
    // End Changes
    //==========================================================================



    /** Used to generate the AuditTemplate
     * @deprecated <I>Inrrelevant method name used, changed to generateAuditTemplate</I>
     * @param templateStr The template String that u want to used to generate the template
     * @return a Hashtable containing the template
     */
    protected Hashtable generateHash(String templateStr)
    {
        String keys = null;
        String descriptions = null;

        // Seperate the keys and descriptions
        StringTokenizer stk = new StringTokenizer(templateStr, "||");
        if (stk.hasMoreElements())
            keys = stk.nextToken();
        else
            keys = null;
        if (stk.hasMoreElements())
            descriptions = stk.nextToken();
        else
            descriptions = null;

        // Seperates into multiple keys
        StringTokenizer keyTokenizer = new StringTokenizer(keys, ",");

        // Seperates into miultiple descriptions
        StringTokenizer descTokenizer = new StringTokenizer(descriptions, ",");

        Hashtable ht = new Hashtable();
        while (keyTokenizer.hasMoreTokens())
        {
            ht.put(keyTokenizer.nextToken(), descTokenizer.nextToken());
        }
        return ht;

    }

    /**
     * Transform the properties and values of the valueobject into a XML String
     * @return xml representation of the valueobject
     
    private String generateXML() throws ValueObjectException
    {
        Map values = retrieveValues();

        try
        {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            DOMImplementation domImpl = docBuilder.getDOMImplementation();
            Document doc = domImpl.createDocument(null, DATA, null);
            Element root = doc.getDocumentElement();
            Element elm;

            Iterator it = null;
            if (property instanceof IndexedMap)
            {
                it = ((IndexedMap)property).keyIterator();
            }
            else
            {
                it = property.keySet().iterator();
            }
            // iterate the property to build the xml for the ValueObject
            for (; it.hasNext(); root.appendChild(elm))
            {

                String name = (String)it.next();
                // Retrieve the value for the Hashtable, which is the field name to be replaced
                Object fieldName = property.get(name);

                // if fieldname doesn't exist, use the key as the field name, else the value.
                String actualFieldName = fieldName == null ? name : fieldName.toString();

                //Object objValue = values.get(name);


                Object objValue = null;
                try
                {
                    String formattedMethodName = "getFormatted_" + name;
                    //logger.debug("Attempting to introspec method " + formattedMethodName);
                    Class clazz = getClass();
                    Method method = clazz.getMethod(formattedMethodName, null);
                    String val = (String)method.invoke(ValueObject.this, null);
                    objValue = val;
                    //logger.debug("Got the formatted object as " + val + "\n");
                }
                catch (Exception e)
                {
                    //logger.debug("No such method. No formatted Object will be used\n");
                    objValue = values.get(name);
                }

                String value = objValue == null ? "" : objValue.toString();

                Object theObject = values.get(name);
                String clazz = theObject == null ? "" : theObject.getClass().getName();

                elm = doc.createElementNS(null, COLUMN);
                elm.setAttributeNS(null, NAME, actualFieldName);
                elm.setAttributeNS(null, VALUE, value == null ? "" : value);
                elm.setAttributeNS(null, TYPE, clazz);
            }

            // serialize the dom to a xml string
            StringWriter writer = new StringWriter();
            OutputFormat format = new OutputFormat();
            format.setOmitXMLDeclaration(true);
            format.setIndent(3);

            XMLSerializer xmlserializer = new XMLSerializer(writer, format);
            xmlserializer.serialize(root);
            return writer.toString();

        }
        catch (DOMException e)
        {
            logger.error(e.getMessage(), e);
            throw new ValueObjectException(e.getMessage());
        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
            throw new ValueObjectException(e.getMessage());
        }
        catch (ParserConfigurationException e)
        {
            logger.error(e.getMessage(), e);
            throw new ValueObjectException(e.getMessage());
        }
        catch (TransformerFactoryConfigurationError e)
        {
            logger.error(e.getMessage(), e);
            throw new ValueObjectException(e.getMessage());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            throw new ValueObjectException(e.getMessage());
        }
    }*/

    // ---------------------------------------------------

    /**
     * To load the column mappings into a column map. Use to solve
     * the characters limitation of db returned fields.
     *
     * For e.g.,
     *   SP:
     *     select oth_dscp_dscp_dscp "othDscpDscpDscp"
     *
     *   VO:
     *     columnMappings = String[][]
     *     {
     *       "othDscpDscpDscp", "otherDescriptionDescriptionDescription"
     *     };
     *
     *     so now we can name the setter and getter as
     *     <code>setOtherDescriptionDescriptionDescription</code>
     *     and
     *     <code>getOtherDescriptionDescriptionDescription</code>
     *
     * @param columnMappings Column mappings in String arrays.
     * @return Column Map.
     */
    protected static Map toColumnMap(String[][] columnMappings)
    {
        Map columnMap = new HashMap();
        for (int i = 0; i < columnMappings.length; i++)
        {
            String[] columnMapping = columnMappings[i];
            if (columnMapping[0] != null)
            {
                columnMap.put(columnMapping[0].toUpperCase(), columnMapping[1]);
            }
        }
        return columnMap;
    }

    /**
     * iterates through the values in the ValueObject
     * @return String representation of values from the getter methods
     */
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        Map values = null;
        try
        {
            values = retrieveValues();
        }
        catch (ValueObjectException e)
        {
            logger.error(e.toString());
        }
        Iterator key = values.keySet().iterator();
        Object obj;

        while (key.hasNext())
        {
            String str = (String)key.next();
            obj = values.get(str);
            if (obj == null)
            {
                obj = "null";
            }
            sb.append("{" + str + "=" + obj + "(" + obj.getClass().toString() + ")},");
        }
        return "[" + sb.toString().substring(0, sb.length() - 1) + "]";
    }

    // ---------------------------------------------------
}

// end of ValueObject.java

/*************************************************************************************
 * Revision History
 *
 * 25 August 2003, Marvin Lee
 * In general, modifications were made to cater to the AuditLog++ 1.0 API
 * - Deprecated method toAuditXML(String) : String
 * - Deprecated method getAuditTemplate(String) : Map
 * - Deprecated method generateAuditTemplate(String) : Map
 * - Introduced method toAuditXML(Map) : String
 * - Introduced method getAuditTemplate() : Map
 *
 *************************************************************************************/