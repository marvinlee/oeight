/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BaseConstants.java

package com.esmart2u.solution.base.helper;

import com.esmart2u.solution.base.service.ConfigurationManager;

/**
 * Base constants.
 *
 * @author  Chee Weng Keong
 * @version $Id: BaseConstants.java,v 1.7 2004/06/15 09:34:59 wkchee Exp $
 */

public interface BaseConstants
{
    // wildcard search
    public static final String WILDCARD = "%";

    // row per page properties
    public static final int MIN_ROW_PER_PAGE = Converter.toIntValue(ConfigurationManager.getKey("MIN_ROW_PER_PAGE"));
    public static final int MED_ROW_PER_PAGE = Converter.toIntValue(ConfigurationManager.getKey("MED_ROW_PER_PAGE"));
    public static final int MAX_ROW_PER_PAGE = Converter.toIntValue(ConfigurationManager.getKey("MAX_ROW_PER_PAGE"));

    // boolean properties
    public static final String BOOLEAN_TYPE_YES = ConfigurationManager.getKey("BOOLEAN_TYPE_YES");
    public static final String BOOLEAN_TYPE_NO = ConfigurationManager.getKey("BOOLEAN_TYPE_NO");

    // date properties
    public static final String DATE_FORMAT = ConfigurationManager.getKey("DATE_FORMAT");
    public static final String DATE_FORMAT_DD = ConfigurationManager.getKey("DATE_FORMAT_DD");
    public static final String DATE_FORMAT_MM = ConfigurationManager.getKey("DATE_FORMAT_MM");
    public static final String DATE_FORMAT_YYYY = ConfigurationManager.getKey("DATE_FORMAT_YYYY");
    public static final String DATE_FORMAT_DD_MM = ConfigurationManager.getKey("DATE_FORMAT_DD_MM");
    public static final String DATE_FORMAT_MM_YYYY = ConfigurationManager.getKey("DATE_FORMAT_MM_YYYY");
    public static final String DATE_TIME_FORMAT = ConfigurationManager.getKey("DATE_TIME_FORMAT");

    // integer format properties
    public static final String INTEGER_FORMAT = ConfigurationManager.getKey("INTEGER_FORMAT");

    // decimal format properties
    public static final String PERCENTAGE_FORMAT = ConfigurationManager.getKey("PERCENTAGE_FORMAT");
    public static final String PERCENTAGE_DECIMAL_TYPE = ConfigurationManager.getKey("PERCENTAGE_DECIMAL_TYPE");
    public static final String PERCENTAGE_MAX_FRACTION = ConfigurationManager.getKey("PERCENTAGE_MAX_FRACTION");
    public static final String PERCENTAGE_MIN_FRACTION = ConfigurationManager.getKey("PERCENTAGE_MIN_FRACTION");

    // rate format properties
    public static final String RATE_FORMAT = ConfigurationManager.getKey("RATE_FORMAT");
    public static final String RATE_DECIMAL_TYPE = ConfigurationManager.getKey("RATE_DECIMAL_TYPE");
    public static final String RATE_MAX_FRACTION = ConfigurationManager.getKey("RATE_MAX_FRACTION");
    public static final String RATE_MIN_FRACTION = ConfigurationManager.getKey("RATE_MIN_FRACTION");
    public static final String RATE_SIZE = ConfigurationManager.getKey("RATE_SIZE");

    // amount format properties
    public static final String MONEY_FORMAT = ConfigurationManager.getKey("MONEY_FORMAT");
    public static final String MONEY_DECIMAL_TYPE = ConfigurationManager.getKey("MONEY_DECIMAL_TYPE");
    public static final String MONEY_MAX_FRACTION = ConfigurationManager.getKey("MONEY_MAX_FRACTION");
    public static final String MONEY_MIN_FRACTION = ConfigurationManager.getKey("MONEY_MIN_FRACTION");
    public static final String MONEY_SIZE = ConfigurationManager.getKey("MONEY_SIZE");

    // html column width
    public static final String COLUMN_WIDTH_MODEL1_TYPE1 = ConfigurationManager.getKey("COLUMN_WIDTH_MODEL1_TYPE1");
    public static final String COLUMN_WIDTH_MODEL1_TYPE2 = ConfigurationManager.getKey("COLUMN_WIDTH_MODEL1_TYPE2");

    public static final String COLUMN_WIDTH_MODEL2_TYPE1 = ConfigurationManager.getKey("COLUMN_WIDTH_MODEL2_TYPE1");
    public static final String COLUMN_WIDTH_MODEL2_TYPE2 = ConfigurationManager.getKey("COLUMN_WIDTH_MODEL2_TYPE2");

    public static final String COLUMN_WIDTH_MODEL3_TYPE1 = ConfigurationManager.getKey("COLUMN_WIDTH_MODEL3_TYPE1");
    public static final String COLUMN_WIDTH_MODEL3_TYPE2 = ConfigurationManager.getKey("COLUMN_WIDTH_MODEL3_TYPE2");
    public static final String COLUMN_WIDTH_MODEL3_TYPE3 = ConfigurationManager.getKey("COLUMN_WIDTH_MODEL3_TYPE3");
    public static final String COLUMN_WIDTH_MODEL3_TYPE4 = ConfigurationManager.getKey("COLUMN_WIDTH_MODEL3_TYPE4");

    // html drop list
    public static final String DROPLIST_DELIMITER = ConfigurationManager.getKey("DROPLIST_DELIMITER");
    public static final String DROPLIST_ALT_DESC_OPEN_BRACKET  = ConfigurationManager.getKey("DROPLIST_ALT_DESC_OPEN_BRACKET");
    public static final String DROPLIST_ALT_DESC_CLOSE_BRACKET = ConfigurationManager.getKey("DROPLIST_ALT_DESC_CLOSE_BRACKET");
    public static final String DROPLIST_SHOW_CODE = ConfigurationManager.getKey("DROPLIST_SHOW_CODE");

    // html pick list
    public static final String CODE_DESCRIPTION_RENDER_CODE = ConfigurationManager.getKey("CODE_DESCRIPTION_RENDER_CODE");
    public static final String CODE_DESCRIPTION_RENDER_DESCRIPTION = ConfigurationManager.getKey("CODE_DESCRIPTION_RENDER_DESCRIPTION");
    public static final String CODE_DESCRIPTION_RENDER_ALTERNATE_DESCRIPTION = ConfigurationManager.getKey("CODE_DESCRIPTION_RENDER_ALTERNATE_DESCRIPTION");
}

// end of BaseConstants.java