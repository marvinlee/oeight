package com.esmart2u.solution.base.helper;

/**
 * This Class represents a timer that notifies another Object every X minutes.
 * For being notified, the Object must implements the WaitingAlarm Interface.
 *
 * @see WaitingAlarm
 */
public class AlarmClock extends Thread {
    private long milisecondsPeriod;
    private WaitingAlarm objectWaiting;

    /**
     *  Constructor
     *
     *@param  _objWaiting the Object that will be notified
     *@param  _milisecondsPeriod  the ciclic period to ring the alarm
     */
    public AlarmClock(WaitingAlarm _objWaiting, long _milisecondsPeriod)
    {
        objectWaiting = _objWaiting;
        milisecondsPeriod = _milisecondsPeriod;
        // Sets the thread priority to the minimum value
        this.setPriority(Thread.MIN_PRIORITY);
    }

    /**
     *  Main processing method for the AlarmClock object
     */
    public void run()
    {
        // execute until is interrupted
        while (true)
        {
            // Notify the Waiting object
            objectWaiting.alarmRinging();
            try
            {
                // sleep for milisecondsPeriod
                sleep(milisecondsPeriod);
            } catch (InterruptedException e)
            {
                // executiong interrupted
                break;
            }
        }
    }
}
