/*
 * PhotoUploadHelper.java
 *
 * Created on September 29, 2007, 11:49 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.esmart2u.solution.base.helper;

import com.esmart2u.oeight.member.helper.OEightConstants;
import com.sun.jimi.core.*;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.HashSet;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author meauchyuan.lee
 */
public class PhotoUploadHelper {
    
    
    private static PhotoUploadHelper instance;
    private static String acceptedMimes = "";
    private static HashSet mimeSet = null;
    
    private static Logger logger = Logger.getLogger(PhotoUploadHelper.class);
    
    static {
        instance = getInstance();
    }
    
    /**
     * This is the method which will load property file that is specified in the /arguement.
     * @params key name of the proeprty file;
     * @return property
     */
    
    public static synchronized PhotoUploadHelper getInstance(){
        if ( instance == null )
            return initInstance();
        return instance;
    }
    
    
    /** Creates a new instance of PhotoUploadHelper */
    public PhotoUploadHelper() {
        
    }
    
    public static PhotoUploadHelper initInstance()
    {
        logger.debug("Initialize PhotoUploadHelper");
        instance.acceptedMimes = PropertyManager.getInstance().getValue(PropertyConstants.PHOTO_ACCEPTED_MIMES);
        logger.debug("Initialize PhotoUploadHelper:" + instance.acceptedMimes);
        instance.mimeSet = new HashSet();
        StringTokenizer tokenizer = new StringTokenizer(instance.acceptedMimes,",");
        while (tokenizer.hasMoreElements()) {
            instance.mimeSet.add(tokenizer.nextToken());
        }
        logger.debug("mimeType size=" + instance.mimeSet.size()); 
        return instance;
    }
    
    public static boolean isAcceptedMimeType(String mimeType) {
        logger.debug("Check if mimeType is accepted");
        //return instance.mimeSet.contains(mimeType);
        boolean ok = getInstance().mimeSet.contains(mimeType);
        logger.debug("mimeType accepted=" + ok);
        return ok;
    }
    
    public static PhotoFile uploadMosaicPhoto(FormFile formFile, String userName) throws Exception{
        Image image;
        PhotoFile photoFile;
        String photoPath=PropertyManager.getValue(PropertyConstants.PHOTO_STORE_PATH);
        String mosaicPath=PropertyManager.getValue(PropertyConstants.THUMBNAIL_STORE_PATH);
        String fileName="";
        
        try {
            if (formFile.getFileSize() <=0)
            {
                throw new Exception("Empty photo!");
            }
            
            logger.debug("In upload photo");
            image = Jimi.getImage(formFile.getInputStream());
            logger.debug("Got image via Jimi");
            
            // File to save 
            Long longDate = System.currentTimeMillis();
            Date date = new Date(longDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            
            // Save original size to photos
            fileName = dateFormat.format(date) + userName + OEightConstants.LARGE_IMAGE_EXT;
            logger.debug("Will save large image to=" + photoPath + fileName);
            
            // Save File
            Jimi.putImage(image, photoPath + fileName);
            logger.debug("Image is uploaded");
            photoFile = new PhotoFile();
            photoFile.setFileName(fileName); 
            photoFile.setFilePath(photoPath + fileName);
            
            // Save Thumbnail to mosaic  
            fileName = dateFormat.format(date) + userName + OEightConstants.THUMBNAIL_IMAGE_EXT;
            logger.debug("Will save mosaic image to=" + mosaicPath + fileName);
            photoFile.setThumbnailFileName(fileName);
            photoFile.setThumbnailFilePath(mosaicPath + fileName);
            
            
            
            // Save Mosaic file and resize it in between
            //
            int maxDim = 38;
            double scale = (double) maxDim / (double) image.getHeight(null);
            if (image.getWidth(null) < image.getHeight(null))
            {
                    scale = (double) maxDim / (double) image.getWidth(null);
            }
            // Determine size of new image.
            //One of them
            // should equal maxDim.
            int scaledW = (int) (scale * image.getWidth(null));
            int scaledH = (int) (scale * image.getHeight(null));

            //
            //
            System.out.println(">> " 
                    + image.getSource().getClass() 
                    + " aspect ratio = " 
                    + scaledW + " , " + scaledH);
            Image img = image.getScaledInstance(scaledW , scaledH, Image.SCALE_SMOOTH);
            
            /*File outputFile = new File("output.jpg");
            outputFile.delete();
            JimiRasterImage raster = Jimi.createRasterImage(img.getSource());
            FileOutputStream fos = new FileOutputStream(outputFile);
            Jimi.putImage("image/jpeg", raster, fos);
            fos.flush();
            fos.close();*/  
            
            Jimi.putImage(img, mosaicPath + fileName);
            logger.debug("Image is uploaded"); 
            
            
        } catch (Exception ex) { 
            ex.printStackTrace();
            logger.error("Error in uploading file for [" + userName + "] filename = " + fileName);
            throw new Exception("Error in uploading file for [" + userName + "] filename = " + fileName);
        }
        return photoFile;
    }
    
    
    public static PhotoFile uploadProfilePhoto(FormFile formFile, String userName, char photoType) throws Exception{
        Image image;
        PhotoFile photoFile;
        String photoPath=PropertyManager.getValue(PropertyConstants.PHOTO_STORE_PATH); 
        String fileName="";
        
        try {
            if (formFile.getFileSize() <=0)
            {
                throw new Exception("Empty photo!");
            }
            logger.debug("In upload photo");
            image = Jimi.getImage(formFile.getInputStream());
            logger.debug("Got image via Jimi");
            
            // File to save 
            Long longDate = System.currentTimeMillis();
            Date date = new Date(longDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            
            // Save original size to photos
            fileName = dateFormat.format(date) + userName + photoType + OEightConstants.LARGE_IMAGE_EXT;
            logger.debug("Will save large image to=" + photoPath + fileName);
            
            // Save File
            Jimi.putImage(image, photoPath + fileName);
            logger.debug("Image is uploaded");
            photoFile = new PhotoFile();
            photoFile.setFileName(fileName); 
            photoFile.setFilePath(photoPath + fileName);  
            
            
        } catch (Exception ex) { 
            ex.printStackTrace();
            logger.error("Error in uploading file for [" + userName + "] filename = " + fileName);
            throw new Exception("Error in uploading file for [" + userName + "] filename = " + fileName);
        }
        return photoFile;
    }
    
    
    public static PhotoFile uploadWeddingMosaicPhoto(FormFile formFile, String userName) throws Exception{
        Image image;
        PhotoFile photoFile;
        String photoPath=PropertyManager.getValue(PropertyConstants.WEDDING_PHOTO_STORE_PATH);
        String mosaicPath=PropertyManager.getValue(PropertyConstants.WEDDING_THUMBNAIL_STORE_PATH);
        String fileName="";
        
        try {
            if (formFile.getFileSize() <=0)
            {
                throw new Exception("Empty photo!");
            }
            
            logger.debug("In upload photo");
            image = Jimi.getImage(formFile.getInputStream());
            logger.debug("Got image via Jimi");
            
            // File to save 
            Long longDate = System.currentTimeMillis();
            Date date = new Date(longDate);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            
            // Save original size to photos
            fileName = dateFormat.format(date) + userName + OEightConstants.LARGE_IMAGE_EXT;
            logger.debug("Will save large image to=" + photoPath + fileName);
            
            // Save File
            Jimi.putImage(image, photoPath + fileName);
            logger.debug("Image is uploaded");
            photoFile = new PhotoFile();
            photoFile.setFileName(fileName); 
            photoFile.setFilePath(photoPath + fileName);
            
            // Save Thumbnail to mosaic  
            fileName = dateFormat.format(date) + userName + OEightConstants.THUMBNAIL_IMAGE_EXT;
            logger.debug("Will save mosaic image to=" + mosaicPath + fileName);
            photoFile.setThumbnailFileName(fileName);
            photoFile.setThumbnailFilePath(mosaicPath + fileName);
            
            
            
            // Save Mosaic file and resize it in between
            //
            int maxDim = 80;
            double scale = (double) maxDim / (double) image.getHeight(null);
            if (image.getWidth(null) < image.getHeight(null))
            {
                    scale = (double) maxDim / (double) image.getWidth(null);
            }
            // Determine size of new image.
            //One of them
            // should equal maxDim.
            int scaledW = (int) (scale * image.getWidth(null));
            int scaledH = (int) (scale * image.getHeight(null));

            //
            //
            System.out.println(">> " 
                    + image.getSource().getClass() 
                    + " aspect ratio = " 
                    + scaledW + " , " + scaledH);
            Image img = image.getScaledInstance(scaledW , scaledH, Image.SCALE_SMOOTH);
            
            /*File outputFile = new File("output.jpg");
            outputFile.delete();
            JimiRasterImage raster = Jimi.createRasterImage(img.getSource());
            FileOutputStream fos = new FileOutputStream(outputFile);
            Jimi.putImage("image/jpeg", raster, fos);
            fos.flush();
            fos.close();*/  
            
            Jimi.putImage(img, mosaicPath + fileName);
            logger.debug("Image is uploaded"); 
            
            
        } catch (Exception ex) { 
            ex.printStackTrace();
            logger.error("Error in uploading file for [" + userName + "] filename = " + fileName);
            throw new Exception("Error in uploading file for [" + userName + "] filename = " + fileName);
        }
        return photoFile;
    }
    
}
