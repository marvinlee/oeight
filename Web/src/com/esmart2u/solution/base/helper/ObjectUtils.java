/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// ObjectUtils.java

package com.esmart2u.solution.base.helper;

import java.io.*;

import com.esmart2u.solution.base.logging.Logger;

/**
 * Utility class to perform various convertions on object.
 *
 * @author  Lee Meau Chyuan
 * @version $Id: ObjectUtils.java,v 1.2 2003/12/16 09:24:57 wkchee Exp $
 */

public class ObjectUtils
{
    private static Logger logger = Logger.getLogger(ObjectUtils.class);

    /**
     * To convert an object to byte array.
     *
     * @param object An object.
     * @return Byte array.
     */
    public static byte[] getByteArray(Object object)
    {
        ByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;

        try
        {
            if (object == null) return null;

            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            return baos.toByteArray();
        }
        catch (Exception e)
        {
            logger.error("Fail to convert object to byte array.", e);
        }
        finally
        {
            try
            {
                if (baos != null)
                {
                    baos.flush();
                    baos.close();
                }
                baos = null;
            }
            catch (IOException e)
            {
                // quietly ignore
            }

            try
            {
                if (oos != null) oos.close();
                oos = null;
            }
            catch (IOException e)
            {
                // quietly ignore
            }
        }

        return null;
    }

    /**
     * To convert byte array to an object.
     *
     * @param byteArray Byte array.
     * @return An object.
     */
    public static Object getObject(byte byteArray[])
    {
        ByteArrayInputStream bais = null;
        ObjectInputStream ois = null;

        try
        {
            if (byteArray == null || byteArray.length == 0) return null;

            bais = new ByteArrayInputStream(byteArray);
            ois = new ObjectInputStream(bais);
            return ois.readObject();
        }
        catch (Exception e)
        {
            logger.error("Fail to convert byte array to object.", e);
        }
        finally
        {
            try
            {
                if (bais != null) bais.close();
                bais = null;
            }
            catch (IOException e)
            {
                // quietly ignore
            }

            try
            {
                if (ois != null) ois.close();
                ois = null;
            }
            catch (IOException e)
            {
                // quietly ignore
            }
        }

        return null;
    }

    /**
     * @deprecated Use findClass.isAssignableFrom(Class objectClass)
     */
    public static boolean instanceOf(Class objectClass, Class findClass, String findPackage)
    {
        findPackage = null;
        return findClass.isAssignableFrom(objectClass);
    }
}

// end of ObjectUtils.java