/**
 * � 2001 - 2002 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// FileSplitter.java

package com.esmart2u.solution.base.helper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;

import com.esmart2u.solution.base.logging.Logger;

public class FileSplitter
{
    private static Logger logger = Logger.getLogger(FileSplitter.class);

    private List packageList = new ArrayList(0);

    public List getPackageList()
    {
        return packageList;
    }

    public int getTotalPackages()
    {
        return packageList.size();
    }

    public void splitBySize(File sourceFile, int packageSize)
        throws Exception
    {
        split(true, sourceFile, packageSize, 0);
    }

    public void splitByPackages(File sourceFile, int totalPackages)
        throws Exception
    {
        split(false, sourceFile, 0, totalPackages);
    }

    private File split(boolean isSplitBySize, File sourceFile, int packageSize, int totalPackages)
        throws Exception
    {
        if (!sourceFile.exists() || !sourceFile.isFile())
        {
            throw new Exception("Cannot read source file.");
        }

        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        File projectFile = null;
        PrintWriter printWriter = null;
        long fileSize;

        try
        {
            fileInputStream = new FileInputStream(sourceFile);
            fileSize = sourceFile.length();

            // determine size and no of packages
            if (isSplitBySize)
            {
                if ((long)packageSize > fileSize)
                {
                    throw new Exception("Package Size is greater than the file itself.");
                }
                totalPackages = (int)Math.ceil((double)fileSize / (double)packageSize);
            }
            else
            {
                packageSize = (int)(((fileSize + (long)totalPackages) - 1L) / (long)totalPackages);
            }

            byte fileBytes[] = new byte[packageSize];

            // prepare generation of project file
            projectFile = new File(sourceFile.getPath() + ".prj");
            printWriter = new PrintWriter(new BufferedWriter(new FileWriter(projectFile)));
            printWriter.write("#FileSplit Project File. Do not edit.\n");
            printWriter.write("\"" + sourceFile.getName() + "\"" + "\n");
            printWriter.write("" + totalPackages + "\n");

            // split file process
            CRC32 crc32 = new CRC32();
            crc32.reset();
            for (int i = 0; i < totalPackages - 1; i++)
            {
                // copy partial file
                File packageFile = new File(sourceFile.getPath() + "." + (i + 1));
                fileOutputStream = new FileOutputStream(packageFile);
                fileInputStream.read(fileBytes);
                fileOutputStream.write(fileBytes);
                fileOutputStream.close();

                // update crc
                crc32.reset();
                crc32.update(fileBytes);

                // update project file
                printWriter.write("\"" + sourceFile.getName() + "." + (i + 1) + "\"" + "\n" + packageSize + "\n" + crc32.getValue() + "\n");

                packageList.add(packageFile);
                logger.debug("Created Package No. " + (i + 1) + " : " + sourceFile.getName() + "." + (i + 1));
            }

            // process last package
            {
                // copy partial file
                File packageFile = new File(sourceFile.getPath() + "." + totalPackages);
                fileOutputStream = new FileOutputStream(new File(sourceFile.getPath() + "." + totalPackages));
                int lastPackageLength = fileInputStream.read(fileBytes);
                fileOutputStream.write(fileBytes, 0, lastPackageLength);
                fileOutputStream.close();

                // update crc
                crc32.reset();
                crc32.update(fileBytes, 0, lastPackageLength);

                // update project file
                printWriter.write("\"" + sourceFile.getName() + "." + totalPackages + "\"" + "\n" + lastPackageLength + "\n" + crc32.getValue());

                packageList.add(packageFile);
                logger.debug("Created Package No. " + totalPackages + " : " + sourceFile.getName() + "." + totalPackages);
            }

            logger.debug("Completed Splitting File \"" + sourceFile.getPath() + "\" into " + totalPackages + " Packages.\n");
        }
        finally
        {
            if (fileInputStream != null)
                fileInputStream.close();
            if (printWriter != null)
                printWriter.close();
        }

        return projectFile;
    }

    public File merge(File projectFile)
        throws Exception
    {
        if (!projectFile.exists() || !projectFile.isFile())
        {
            throw new Exception("Cannot read project file to be merged.");
        }
        if (!projectFile.getName().endsWith(".prj"))
        {
            throw new Exception("Improper project file. Project file must end with extension \".prj\".");
        }

        FileInputStream projectFileInputStream = null;
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        File targetFile = null;
        String targetFileName;
        int currentTokenType;

        List cleanupList = new ArrayList(0);
        cleanupList.add(projectFile);

        try
        {
            projectFileInputStream = new FileInputStream(projectFile);
            StreamTokenizer streamTokenizer = new StreamTokenizer(new BufferedReader(new InputStreamReader(projectFileInputStream)));
            streamTokenizer.commentChar('#');

            // retrieve target file name
            currentTokenType = streamTokenizer.nextToken();
            if (currentTokenType == StreamTokenizer.TT_WORD || currentTokenType == '"')
                targetFileName = streamTokenizer.sval;
            else
                throw new Exception("Invalid token type.");
            targetFile = new File(projectFile.getParent() + File.separator + targetFileName);
            fileOutputStream = new FileOutputStream(targetFile);

            // retrieve total number of packages
            currentTokenType = streamTokenizer.nextToken();
            int noOfPackages;
            if (currentTokenType == StreamTokenizer.TT_NUMBER)
                noOfPackages = (int)streamTokenizer.nval;
            else
                throw new Exception("Invalid token type.");

            for (int i = 1; i <= noOfPackages; i++)
            {
                // retrieve package name
                currentTokenType = streamTokenizer.nextToken();
                String packageName;
                if (currentTokenType == StreamTokenizer.TT_WORD || currentTokenType == '"')
                    packageName = streamTokenizer.sval;
                else
                    throw new Exception("Invalid token type.");

                // prepare package
                File packageFile;
                try
                {
                    packageFile = new File(projectFile.getParent() + File.separator + packageName);
                    fileInputStream = new FileInputStream(packageFile);
                    cleanupList.add(packageFile);
                    packageList.add(packageFile);
                }
                catch (IOException e)
                {
                    throw new Exception("One of the package file, " + packageName + " could not be located.");
                }

                // retrieve package size
                currentTokenType = streamTokenizer.nextToken();
                int size;
                if (currentTokenType == StreamTokenizer.TT_NUMBER)
                    size = (int)streamTokenizer.nval;
                else
                    throw new Exception("Invalid token type.");

                // check package size
                if ((long)size != packageFile.length())
                {
                    targetFile.delete();
                    throw new Exception("Size of file " + packageName + " does not match with original package. Merging process abandoned.");
                }

                // retrieve checksum
                currentTokenType = streamTokenizer.nextToken();
                long crc;
                if (currentTokenType == StreamTokenizer.TT_NUMBER)
                    crc = (long)streamTokenizer.nval;
                else
                    throw new Exception("Invalid token type.");

                // merging process
                byte fileBytes[] = new byte[size];
                fileInputStream.read(fileBytes);
                fileOutputStream.write(fileBytes);
                fileInputStream.close();

                // check checksum
                CRC32 crc32 = new CRC32();
                crc32.reset();
                crc32.update(fileBytes);
                if (crc32.getValue() != crc)
                {
                    targetFile.delete();
                    throw new Exception("Checksum of file " + packageName + " does not match with original package. Merging process abandoned.");
                }

                logger.debug("Merged Package No. " + i + " : " + packageFile.getName());
            }

            projectFileInputStream.close();
            fileInputStream.close();
            fileOutputStream.close();

            // cleanup process
            for (int i = 0, size_i = cleanupList.size(); i < size_i; i++)
            {
                File cleanupFile = (File)cleanupList.get(i);
                cleanupFile.delete();
            }

            logger.debug("Process Complete! The original file " + targetFileName + " was obtained successfully by merging.");
        }
        catch (Exception e)
        {
            logger.error("The format of the project file is not as expected.", e);
            throw e;
        }
        finally
        {
            if (projectFileInputStream != null)
                projectFileInputStream.close();
            if (fileInputStream != null)
                fileInputStream.close();
            if (fileOutputStream != null)
                fileOutputStream.close();
        }

        return targetFile;
    }
}

// end of FileSplitter.java