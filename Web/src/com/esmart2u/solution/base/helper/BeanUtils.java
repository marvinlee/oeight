/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// BeanUtils.java

package com.esmart2u.solution.base.helper;

import java.beans.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import com.esmart2u.solution.base.logging.Logger;

/**
 * Bean utilities.
 *
 * @author Chee Weng Keong
 * @version $Id: BeanUtils.java,v 1.5 2004/06/15 09:34:59 wkchee Exp $
 */

public class BeanUtils
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(BeanUtils.class);

    /**
     * Copy bean attributes from source to target.
     *
     * @param source Source object.
     * @param target Target object.
     * @throws IntrospectionException    If introspection failed.
     * @throws IllegalAccessException    If access failed.
     * @throws InvocationTargetException If Invocation failed.
     */
    public static void copyBean(Object source, Object target)
        throws IntrospectionException, IllegalAccessException, InvocationTargetException
    {
        if (source == null || target == null)
        {
            logger.debug("Skipped copy. Either source [" + source + "] or target [" + target + "] is null");
            return;
        }
        logger.debug("Source class is " + source.getClass().getName());
        logger.debug("Target class is " + target.getClass().getName());

        BeanInfo sourceBeanInfo = Introspector.getBeanInfo(source.getClass());
        BeanInfo targetBeanInfo = Introspector.getBeanInfo(target.getClass());

        // analyze source
        Map sourceMap = new HashMap();
        PropertyDescriptor[] sourcePropertyDescriptors = sourceBeanInfo.getPropertyDescriptors();
        for (int i = 0; i < sourcePropertyDescriptors.length; i++)
        {
            PropertyDescriptor sourcePropertyDescriptor = sourcePropertyDescriptors[i];
            sourceMap.put(sourcePropertyDescriptor.getName(), sourcePropertyDescriptor);
        }

        // analyze target
        PropertyDescriptor[] targetPropertyDescriptors = targetBeanInfo.getPropertyDescriptors();
        for (int i = 0; i < targetPropertyDescriptors.length; i++)
        {
            PropertyDescriptor targetPropertyDescriptor = targetPropertyDescriptors[i];
            PropertyDescriptor sourcePropertyDescriptor =
                (PropertyDescriptor)sourceMap.get(targetPropertyDescriptor.getName());

            // match the method on source and target
            if (sourcePropertyDescriptor != null)
            {
                logger.debug("---------------------------------------------");
                logger.debug("Analyzing property [" + sourcePropertyDescriptor.getName() + "]");

                Method sourceReadMethod = sourcePropertyDescriptor.getReadMethod();
                Method targetWriteMethod = targetPropertyDescriptor.getWriteMethod();

                if (sourceReadMethod == null)
                {
                    logger.debug("  Source read method not found. Skipped.");
                    continue;
                }
                if (targetWriteMethod == null)
                {
                    logger.debug("  Target writer method not found. Skipped.");
                    continue;
                }

                // check data type must be same on both side
                Class returnClass = sourceReadMethod.getReturnType();
                Class[] parameterClasses = targetWriteMethod.getParameterTypes();
                logger.debug("  Getter : " + sourceReadMethod.getName() + " returns " + returnClass.getName());
                logger.debug("  Setter : " + targetWriteMethod.getName() + " accepts " + parameterClasses[0].getName() +
                             (parameterClasses.length > 1 ? ", ..." : ""));

                // ensure the setter accepts only one parameter
                if (parameterClasses.length == 1)
                {
                    // due to financial value such as rate and money, we need to have special
                    // handling/ability to convert String to BigDecimal or String to BigInteger
                    // or vice vessa (if any). why? simply because struts framework (BeanUtils)
                    // cannot map null value from html back to ActionForm properly

                    if (returnClass == BigInteger.class && parameterClasses[0] == String.class)
                    {
                        // copying
                        BigInteger bigInteger = (BigInteger)sourceReadMethod.invoke(source, new Object[]{});
                        String bigIntegerValue = Converter.toBigIntegerString(bigInteger);
                        logger.debug("Copying value [" + bigIntegerValue + "]");
                        targetWriteMethod.invoke(target, new Object[]{bigIntegerValue});
                    }
                    else if (returnClass == String.class && parameterClasses[0] == BigInteger.class)
                    {
                        // copying
                        String bigIntegerValue = (String)sourceReadMethod.invoke(source, new Object[]{});
                        BigInteger bigInteger = Converter.toBigInteger(bigIntegerValue);
                        logger.debug("Copying value [" + bigInteger + "]");
                        targetWriteMethod.invoke(target, new Object[]{bigInteger});
                    }
                    else if (returnClass == BigDecimal.class && parameterClasses[0] == String.class)
                    {
                        // copying
                        BigDecimal bigDecimal = (BigDecimal)sourceReadMethod.invoke(source, new Object[]{});
                        String bigDecimalValue = Converter.toBigDecimalString(bigDecimal);
                        logger.debug("Copying value [" + bigDecimalValue + "]");
                        targetWriteMethod.invoke(target, new Object[]{bigDecimalValue});
                    }
                    else if (returnClass == String.class && parameterClasses[0] == BigDecimal.class)
                    {
                        // copying
                        String bigDecimalValue = (String)sourceReadMethod.invoke(source, new Object[]{});
                        BigDecimal bigDecimal = Converter.toBigDecimal(bigDecimalValue);
                        logger.debug("Copying value [" + bigDecimal + "]");
                        targetWriteMethod.invoke(target, new Object[]{bigDecimal});
                    }

                    // normal scenario handling
                    else if (returnClass == parameterClasses[0])
                    {
                        // copying
                        Object sourceValue = sourceReadMethod.invoke(source, new Object[]{});
                        logger.debug("Copying value [" + sourceValue + "]");
                        targetWriteMethod.invoke(target, new Object[]{sourceValue});
                    }
                }
                else
                {
                    logger.debug("Getter and setter are not in pair");
                }
            }
        }
    }

    /**
     * Reset all the properties in the object.
     *
     * @param object Object.
     * @throws IntrospectionException    If introspection failed.
     * @throws IllegalAccessException    If access failed.
     * @throws InvocationTargetException If Invocation failed.
     */
    public static void resetBean(Object object)
        throws IntrospectionException, IllegalAccessException, InvocationTargetException
    {
        if (object == null)
        {
            logger.debug("Bean is not being reset as it is null");
            return;
        }
        logger.debug("Resetting object " + object.getClass());

        BeanInfo beanInfo = Introspector.getBeanInfo(object.getClass());
        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
        for (int i = 0; i < propertyDescriptors.length; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptors[i];

            logger.debug("---------------------------------------------");
            logger.debug("Analyzing property [" + propertyDescriptor.getName() + "]");

            Method readMethod = propertyDescriptor.getReadMethod();
            Method writeMethod = propertyDescriptor.getWriteMethod();

            if (readMethod == null)
            {
                logger.debug("  Source read method not found. Skipped.");
                continue;
            }
            if (writeMethod == null)
            {
                logger.debug("  Target writer method not found. Skipped.");
                continue;
            }

            if (readMethod != null && writeMethod != null)
            {
                // check data type must be same on both side
                Class[] parameterClasses = writeMethod.getParameterTypes();
                Class returnClass = readMethod.getReturnType();
                logger.debug("  Getter : " + readMethod.getName() + " returns " + returnClass.getName());
                logger.debug("  Setter : " + writeMethod.getName() + " accepts " + parameterClasses[0].getName() +
                             (parameterClasses.length > 1 ? ", ..." : ""));

                // check is valid setter and getter
                if (parameterClasses.length == 1 && parameterClasses[0] == returnClass)
                {
                    if (parameterClasses[0] == Boolean.TYPE)
                    {
                        boolean value = false;
                        writeMethod.invoke(object, new Object[]{new Boolean(value)});
                        logger.debug("  Resetting value to " + value);
                    }
                    else if (parameterClasses[0] == Byte.TYPE)
                    {
                        byte value = 0;
                        writeMethod.invoke(object, new Object[]{new Byte(value)});
                        logger.debug("  Resetting value to " + value);
                    }
                    else if (parameterClasses[0] == Character.TYPE)
                    {
                        char value = 0;
                        writeMethod.invoke(object, new Object[]{new Character(value)});
                        logger.debug("  Resetting value to " + value);
                    }
                    else if (parameterClasses[0] == Short.TYPE)
                    {
                        short value = 0;
                        writeMethod.invoke(object, new Object[]{new Short(value)});
                        logger.debug("  Resetting value to " + value);
                    }
                    else if (parameterClasses[0] == Integer.TYPE)
                    {
                        int value = 0;
                        writeMethod.invoke(object, new Object[]{new Integer(value)});
                        logger.debug("  Resetting value to " + value);
                    }
                    else if (parameterClasses[0] == Long.TYPE)
                    {
                        long value = 0L;
                        writeMethod.invoke(object, new Object[]{new Long(value)});
                        logger.debug("  Resetting value to " + value);
                    }
                    else if (parameterClasses[0] == Float.TYPE)
                    {
                        float value = 0;
                        writeMethod.invoke(object, new Object[]{new Float(value)});
                        logger.debug("  Resetting value to " + value);
                    }
                    else if (parameterClasses[0] == Double.TYPE)
                    {
                        double value = 0.0;
                        writeMethod.invoke(object, new Object[]{new Double(value)});
                        logger.debug("  Resetting value to " + value);
                    }
                    else
                    {
                        try
                        {
                            Object value = null;
                            writeMethod.invoke(object, new Object[]{value});
                            logger.debug("  Resetting value to " + value);
                        }
                        catch (Exception e)
                        {
                            logger.debug("  Unable to reset the value, give up!");
                        }
                    }
                }
                else
                {
                    logger.debug("Getter and setter are not in pair");
                }
            }
        }
    }

    /**
     * Compare and update bean attributes from source to target.
     *
     * @param source Source object.
     * @param target Target object.
     * @throws IntrospectionException    If introspection failed.
     * @throws IllegalAccessException    If access failed.
     * @throws InvocationTargetException If Invocation failed.
     */
    public static void updateBean(Object source, Object target)
        throws IntrospectionException, IllegalAccessException, InvocationTargetException
    {
        BeanUtils.updateBean(source, target, new String[0]);
    }

    /**
     * Compare and update bean attributes from source to target with exemptions.
     *
     * @param source             Source object.
     * @param target             Target object.
     * @param exemptedProperties Exempted properties from processing.
     * @throws IntrospectionException    If introspection failed.
     * @throws IllegalAccessException    If access failed.
     * @throws InvocationTargetException If Invocation failed.
     */
    public static void updateBean(Object source, Object target, String[] exemptedProperties)
        throws IntrospectionException, IllegalAccessException, InvocationTargetException
    {
        if (source == null || target == null)
        {
            logger.debug("Skipped compare and update. Either source [" + source + "] or target [" + target + "] is null");
            return;
        }
        logger.debug("Source class is " + source.getClass().getName());
        logger.debug("Target class is " + target.getClass().getName());

        BeanInfo sourceBeanInfo = Introspector.getBeanInfo(source.getClass());
        BeanInfo targetBeanInfo = Introspector.getBeanInfo(target.getClass());

        // analyze source
        Map sourceMap = new HashMap();
        PropertyDescriptor[] sourcePropertyDescriptors = sourceBeanInfo.getPropertyDescriptors();
        for (int i = 0; i < sourcePropertyDescriptors.length; i++)
        {
            PropertyDescriptor sourcePropertyDescriptor = sourcePropertyDescriptors[i];
            sourceMap.put(sourcePropertyDescriptor.getName(), sourcePropertyDescriptor);
        }

        // analyze target
        boolean skipped = false;
        PropertyDescriptor[] targetPropertyDescriptors = targetBeanInfo.getPropertyDescriptors();
        for (int i = 0; i < targetPropertyDescriptors.length; i++)
        {
            PropertyDescriptor targetPropertyDescriptor = targetPropertyDescriptors[i];
            PropertyDescriptor sourcePropertyDescriptor =
                (PropertyDescriptor)sourceMap.get(targetPropertyDescriptor.getName());

            skipped = false;

            // match the method on source and target
            if (sourcePropertyDescriptor != null)
            {
                logger.debug("---------------------------------------------");
                logger.debug("Analyzing property [" + sourcePropertyDescriptor.getName() + "]");

                if (exemptedProperties != null)
                {
                    for (int j = 0; j < exemptedProperties.length; j++)
                    {
                        if (exemptedProperties[j].equals(targetPropertyDescriptor.getName()))
                        {
                            logger.debug("Exempted.");
                            skipped = true;
                            break;
                        }
                    }
                }

                if (skipped) continue;

                Method sourceReadMethod = sourcePropertyDescriptor.getReadMethod();
                Method targetReadMethod = targetPropertyDescriptor.getReadMethod();

                if (sourceReadMethod == null)
                {
                    logger.debug("  Source read method not found. Skipped.");
                    continue;
                }
                if (targetReadMethod == null)
                {
                    logger.debug("  Target read method not found. Skipped.");
                    continue;
                }

                Object sourceValue = sourceReadMethod.invoke(source, new Object[0]);
                Object targetValue = targetReadMethod.invoke(target, new Object[0]);

                if ((sourceValue != null && targetValue == null) ||
                    (sourceValue == null && targetValue != null) ||
                    (sourceValue != null && targetValue != null && !sourceValue.equals(targetValue)))
                {
                    Method targetWriteMethod = targetPropertyDescriptor.getWriteMethod();
                    if (targetWriteMethod == null)
                    {
                        logger.debug("  Target writer method not found. Skipped.");
                        continue;
                    }

                    // check data type must be same on both side
                    Class returnClass = sourceReadMethod.getReturnType();
                    Class[] parameterClasses = targetWriteMethod.getParameterTypes();
                    logger.debug("  Getter : " + sourceReadMethod.getName() + " returns " + returnClass.getName());
                    logger.debug("  Setter : " + targetWriteMethod.getName() + " accepts " + parameterClasses[0].getName() +
                                 (parameterClasses.length > 1 ? ", ..." : ""));
                    logger.debug("  Source Value : " + sourceValue);
                    logger.debug("  Target Value : " + targetValue);

                    // ensure the setter accepts only one parameter
                    if (parameterClasses.length == 1 && returnClass == parameterClasses[0])
                    {
                        // updating
                        logger.debug("Updating target value [" + targetValue + " -> " + sourceValue + "]");
                        targetWriteMethod.invoke(target, new Object[]{sourceValue});
                    }
                    else
                    {
                        logger.debug("Getter and setter are not in pair");
                    }
                }
            }
        }
    }

    /**
     * Finds difference between source and target objects.
     * The return result map will store the property name and source value.
     *
     * @param source Source object.
     * @param target Target object.
     * @return Difference map.
     * @throws IntrospectionException    If introspection failed.
     * @throws IllegalAccessException    If access failed.
     * @throws InvocationTargetException If Invocation failed.
     */
    public static Map findDifference(Object source, Object target) throws Exception
    {
        return BeanUtils.findDifference(source, target, new String[0]);
    }

    /**
     * Finds difference between source and target objects with exempted properties.
     * The return result map will store the property name and source value.
     *
     * @param source             Source object.
     * @param target             Target object.
     * @param exemptedProperties Exempted properties from processing.
     * @return Difference map.
     * @throws IntrospectionException    If introspection failed.
     * @throws IllegalAccessException    If access failed.
     * @throws InvocationTargetException If Invocation failed.
     */
    public static Map findDifference(Object source, Object target, String[] exemptedProperties)
        throws IntrospectionException, IllegalAccessException, InvocationTargetException
    {
        if (source == null || target == null)
        {
            logger.debug("Skipped finding difference. Either source [" + source + "] or target [" + target + "] is null");
            return new HashMap(0);
        }
        logger.debug("Source class is " + source.getClass().getName());
        logger.debug("Target class is " + target.getClass().getName());

        BeanInfo sourceBeanInfo = Introspector.getBeanInfo(source.getClass());
        BeanInfo targetBeanInfo = Introspector.getBeanInfo(target.getClass());

        // analyze source
        Map sourceMap = new HashMap();
        PropertyDescriptor[] sourcePropertyDescriptors = sourceBeanInfo.getPropertyDescriptors();
        for (int i = 0; i < sourcePropertyDescriptors.length; i++)
        {
            PropertyDescriptor sourcePropertyDescriptor = sourcePropertyDescriptors[i];
            sourceMap.put(sourcePropertyDescriptor.getName(), sourcePropertyDescriptor);
        }

        // analyze target
        Map differenceMap = new HashMap();
        boolean skipped = false;
        PropertyDescriptor[] targetPropertyDescriptors = targetBeanInfo.getPropertyDescriptors();
        for (int i = 0; i < targetPropertyDescriptors.length; i++)
        {
            PropertyDescriptor targetPropertyDescriptor = targetPropertyDescriptors[i];
            PropertyDescriptor sourcePropertyDescriptor =
                (PropertyDescriptor)sourceMap.get(targetPropertyDescriptor.getName());

            skipped = false;

            // match the method on source and target
            if (sourcePropertyDescriptor != null)
            {
                logger.debug("---------------------------------------------");
                logger.debug("Analyzing property [" + sourcePropertyDescriptor.getName() + "]");

                if (exemptedProperties != null)
                {
                    for (int j = 0; j < exemptedProperties.length; j++)
                    {
                        if (exemptedProperties[j].equals(targetPropertyDescriptor.getName()))
                        {
                            logger.debug("Exempted.");
                            skipped = true;
                            break;
                        }
                    }
                }

                if (skipped) continue;

                Method sourceReadMethod = sourcePropertyDescriptor.getReadMethod();
                Method targetReadMethod = targetPropertyDescriptor.getReadMethod();

                if (sourceReadMethod == null)
                {
                    logger.debug("  Source read method not found. Skipped.");
                    continue;
                }
                if (targetReadMethod == null)
                {
                    logger.debug("  Target read method not found. Skipped.");
                    continue;
                }

                Object sourceValue = sourceReadMethod.invoke(source, new Object[0]);
                Object targetValue = targetReadMethod.invoke(target, new Object[0]);

                if ((sourceValue != null && targetValue == null) ||
                    (sourceValue == null && targetValue != null) ||
                    (sourceValue != null && targetValue != null && !sourceValue.equals(targetValue)))
                {
                    logger.debug("  Source Value : " + sourceValue);
                    logger.debug("  Target Value : " + targetValue);

                    differenceMap.put(sourcePropertyDescriptor.getName(), sourceValue);
                    logger.debug("Different.");
                }
            }
        }

        return differenceMap;
    }

    /**
     * Applies changed value to target object.
     *
     * @param target            Target object.
     * @param changedProperties Changed values.
     * @throws IntrospectionException    If introspection failed.
     * @throws IllegalAccessException    If access failed.
     * @throws InvocationTargetException If Invocation failed.
     */
    public static void applyChanges(Object target, Map changedProperties)
        throws IntrospectionException, IllegalAccessException, InvocationTargetException
    {
        if (target == null)
        {
            logger.debug("Skipped applying changes. Target [" + target + "] is null");
        }
        logger.debug("Target class is " + target.getClass().getName());

        BeanInfo targetBeanInfo = Introspector.getBeanInfo(target.getClass());

        // analyze target
        Set propertiesSet = changedProperties.keySet();
        Object[] propertiesKeys = propertiesSet.toArray();
        PropertyDescriptor[] targetPropertyDescriptors = targetBeanInfo.getPropertyDescriptors();
        for (int i = 0; i < targetPropertyDescriptors.length; i++)
        {
            PropertyDescriptor targetPropertyDescriptor = targetPropertyDescriptors[i];

            logger.debug("---------------------------------------------");
            logger.debug("Analyzing property [" + targetPropertyDescriptor.getName() + "]");

            String propertyName = targetPropertyDescriptor.getName();
            for (int j = 0; j < propertiesKeys.length; j++)
            {
                if (propertyName.equals(propertiesKeys[j]))
                {
                    Object changedValue = changedProperties.get(propertiesKeys[j]);

                    Method targetWriteMethod = targetPropertyDescriptor.getWriteMethod();
                    if (targetWriteMethod == null)
                    {
                        logger.debug("  Target writer method not found. Skipped.");
                        continue;
                    }

                    // check data type must be same on both side
                    Class[] parameterClasses = targetWriteMethod.getParameterTypes();
                    logger.debug("  Setter : " + targetWriteMethod.getName() + " accepts " + parameterClasses[0].getName() +
                                 (parameterClasses.length > 1 ? ", ..." : ""));

                    if (changedValue == null)
                    {
                        logger.debug("  Changed Value class : " + changedValue);
                    }
                    else
                    {
                        logger.debug("  Changed Value class : " + changedValue.getClass().getName());

                        // ensure the setter accepts only one parameter
                        if (parameterClasses.length == 1 && changedValue.getClass() == parameterClasses[0])
                        {
                            logger.debug("Applying value " + changedValue);
                            targetWriteMethod.invoke(target, new Object[]{changedValue});
                        }
                    }
                }
            }
        }
    }
}

// end of BeanUtils.java