/**
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// WaitingAlarm.java

package com.esmart2u.solution.base.helper;

/**
 * A Class that wants to be notified by AlarmClock must implement this interface.
 *
 * @see AlarmClock
 */

public interface WaitingAlarm
{
    public void alarmRinging();
}

// end of WaitingAlarm.java