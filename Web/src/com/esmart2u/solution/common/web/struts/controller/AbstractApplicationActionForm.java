/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// AbstractApplicationActionForm.java

package com.esmart2u.solution.common.web.struts.controller;

import com.esmart2u.solution.web.struts.controller.BaseActionForm;

/**
 * Base application action form.
 *
 * @author Chee Weng Keong
 * @version $Id: AbstractApplicationActionForm.java,v 1.2 2004/01/16 02:31:16 wkchee Exp $
 */

public class AbstractApplicationActionForm extends BaseActionForm
{
}

// end of AbstractApplicationActionForm.java