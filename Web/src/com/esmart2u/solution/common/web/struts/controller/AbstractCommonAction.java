/*
 * � 2007 - 2008 esmart2u Malaysia Sdn Bhd. All rights reserved.
 * MarvinLee.net
 * 
 * 
 * 
 * 
 *
 * This software is the intellectual property of esmart2u. The program
 * may be used only in accordance with the terms of the license agreement you
 * entered into with esmart2u.
 */

// AbstractCommonAction.java

package com.esmart2u.solution.common.web.struts.controller;

import com.esmart2u.solution.web.struts.controller.BaseAction;
//import com.esmart2u.solution.common.bo.AbstractCommonBO;
import com.esmart2u.solution.base.logging.Logger;

/**
 * Base action for common modules.
 *
 * @author Chee Weng Keong
 * @version $Id: AbstractCommonAction.java,v 1.3 2004/06/22 05:44:55 wkchee Exp $
 */

public abstract class AbstractCommonAction extends BaseAction
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(AbstractCommonAction.class);

    /**
     * @deprecated No longer use. Currently return null.
     */
//    public AbstractCommonBO getBusinessObject()
//    {
//        logger.warn("Warning! Accessing deprecated method. Returning NULL.");
//        return null;
//    }
}

// end of AbstractCommonAction.java