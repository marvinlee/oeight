

package com.esmart2u.solution.common.web.struts.controller;
 
import com.esmart2u.solution.web.struts.controller.BaseAction;
import com.esmart2u.solution.base.logging.Logger; 

/**
 * Base action for application modules.
 *
 * @author Chee Weng Keong
 * @version $Id: AbstractApplicationAction.java,v 1.3 2004/06/22 05:44:55 wkchee Exp $
 */

public abstract class AbstractApplicationAction extends BaseAction
{
    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(AbstractApplicationAction.class);
    
    /**
     * @deprecated No longer use. Currently return null.
     */
//    public AbstractApplicationBO getBusinessObject()
//    {
//        logger.warn("Warning! Accessing deprecated method. Returning NULL.");
//        return null;
//    }
}

// end of AbstractApplicationAction.java