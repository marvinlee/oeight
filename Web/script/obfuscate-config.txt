// Sets the classpath that will be used by Zelix KlassMaster.
// Zelix KlassMaster uses its classpath to find all classes.
classpath   "C:/bea/jdk131_08/lib/tools.jar"
            "C:/bea/weblogic700/server/lib/weblogic.jar"
            "../lib/castor-0.9.5.3.jar"
            "../lib/classes12.zip"
            "../lib/commons-beanutils-1.6.1.jar"
            "../lib/commons-collections-3.0.jar"
            "../lib/commons-logging-1.0.3.jar"
            "../lib/icu4j-2.8.jar"
            "../lib/jstl-1.1-contrib.jar"
            "../lib/log4j-1.2.8.jar"
            "../lib/standard-1.1-contrib.jar"
            "../lib/struts-1.1.jar"
            "../lib/struts-el-1.1-contrib.jar"
            "../lib/xercesImpl-2.6.2.jar"
            "../lib/esmart2u/base-2.4.10.2.0011.jar"
            "../lib/esmart2u/base-auditlog-2.0.7.5.jar"
            "../lib/esmart2u/base-auditlog-web-2.0.7.5.jar"
            "../lib/esmart2u/base-contrib-2.4.10.2.0011.jar"
            "../lib/esmart2u/base-contrib-ejb-2.4.10.2.0011.jar"
            "../lib/esmart2u/base-eai-2.4.10.2.0011.jar"
            "../lib/esmart2u/dsms-client-1.4.2.jar"
            "../lib/esmart2u/dsms-ejb-1.4.2.jar"
            "../lib/esmart2u/dsms-web-1.4.2.jar";

// Opens class files for processing by Zelix KlassMaster.
open        "../deploy/base-web.jar";

// Excludes the specified objects from being removed by the Zelix KlassMaster trim statement.
trimExclude *.* public static * and
            *.* public static final * and
            *.* public static *(*) and
            *.* private static *(*) and
            *.* static final *(*) and
            public *.* and
            *.* public * and
            *.* protected * and
            *.* public *(*) and
            *.* protected *(*);

// Removes classes, fields and methods that have not been explicitly or implicitly
// excluded by a preceding trimExclude statement.
trim        deleteSourceFileAttributes=false
            deleteDeprecatedAttributes=false
            deleteUnknownAttributes=false;

// Excludes the names of specified objects from being renamed by the
// Zelix KlassMaster obfuscate or unobfuscate statements.
exclude     *. and
            *.* and
			public *.* and
			public *.^* and
			*.* public * and
			*.* protected * and
			*.* private * and
			*.* final * and
			*.* static * and
			*.* final static * and
			*.* static final * and
			*.* public final * and
			*.* public static * and
			*.* public final static * and
			*.* public static final * and
			*.* protected final * and
			*.* protected static * and
			*.* protected final static * and
			*.* protected static final * and
			*.* private final * and
			*.* private static * and
			*.* private final static * and
			*.* private static final * and
			*.* static *(*) and
			*.* public *(*) and
			*.* protected *(*) and
			*.* public static *(*) and
			*.* public final *(*) and
			*.* protected static *(*) and
			*.* protected final *(*);

// Excludes specified String literals from being encrypted by the Zelix KlassMaster obfuscate statement.
stringEncryptionExclude     *.* final * and
                            *.* static * and
                            *.* final static * and
                            *.* static final * and
                            *.* public final * and
                            *.* public static * and
                            *.* public final static * and
                            *.* public static final * and
                            *.* protected final * and
                            *.* protected static * and
                            *.* protected final static * and
                            *.* protected static final *;

// Ensures that any changes made to the specified classes by the Zelix KlassMaster
// obfuscate statement will not break already existing serialized objects that are based
// upon the non-obfuscated class definition.
existingSerializedClasses   com.esmart2u.solution.web.struts.service.MenuBean and
                            com.esmart2u.solution.web.struts.service.ScreenActionBean and
                            com.esmart2u.solution.web.struts.service.TabBean;

// Excludes the specified methods from being flow obfuscated by the Zelix KlassMaster obfuscate statement.
obfuscateFlowExclude        * and               // exclude all methods in all classes in default package
                            *.* <init>(*) and   // exclude all contructors from flow obfuscation
                            *.* <clinit>(*);    // exclude all static initializers from flow obfuscation

// Obfuscates all opened classes.
obfuscate   changeLogFileIn=""
            changeLogFileOut="./log/change.log"
            obfuscateFlow=aggressive
            encryptStringLiterals=none
            lineNumbers=delete;

// change to the path where do you want to save the obfuscated jar file
saveAll     archiveCompression=all "..";